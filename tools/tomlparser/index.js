/**
 * Converts the IIFE of https://github.com/jakwings/toml-j0.4 toml parser into a plain
 * function for appropriate Helma usage, then checks/minifies the code for Rhino/ES3
 * compatibility by using Google's closure compiler
 * - writes output file to /dist/toml.min.js
 * - use "npm run copy" to copy the minified file to /Global
 *   or  "npm run build" to compile and copy
 */
const ClosureCompiler = require('google-closure-compiler').compiler;
const fs = require('fs');
const tomlLibCode = fs.readFileSync('./node_modules/toml-j0.4/lib/parser.js').toString();

const searchString = '(function()';
const iifeStart = tomlLibCode.indexOf(searchString) + searchString.length;
const tomlHelmaCode = `/*!
 * TOML Parser, minified version from https://github.com/jakwings/toml-j0.4
 * adapted to Helma server side usage (Rhino/ES3)
 * USAGE: var json = TOML().parse(src);
 * whereas src is a valid TOML string according to https://toml.io/en/v0.4.0
*/
function TOML()${tomlLibCode.slice(iifeStart, -3).replace('"use strict";\n', '')}`;

// write intermediate unminified js code
fs.writeFileSync('./toml-server.js', tomlHelmaCode);
fs.writeFileSync('./toml-server-test.js', tomlHelmaCode + '; module.exports = TOML;');

// check/minify intermediate js code and write packed version to /dist/toml.min.js
const closureCompiler = new ClosureCompiler({
  js: './toml-server.js',
  compilation_level: 'WHITESPACE_ONLY',
  language_in: 'ECMASCRIPT3',
  language_out: 'ECMASCRIPT3',
  warning_level: 'VERBOSE',
  js_output_file: './dist/toml.min.js',
});

const compilerProcess = closureCompiler.run((exitCode, _stdOut, _stdErr) => {
  console.log(`>>> TOML Helma version created, exit code was ${exitCode}.`);
});
