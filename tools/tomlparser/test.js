/**
 * Quick test of a truthy and falsy TOML parsing process
 */
const TOML = require('./toml-server-test');

try {
  const json = TOML().parse(`somearray = [
    'a', #just a line comment
    'b',
    'c',
    ]
    # more of a comment
    eins = '1'
    zwei = '2'
    drei = '3'
    `);
  console.log('Test OK:', json);

  TOML().parse('just give me an error, bro!');
} catch (err) {
  console.log(`Error @ line ${err.location.start.line}, column: ${err.location.start.column} (${err.message})`);
}
