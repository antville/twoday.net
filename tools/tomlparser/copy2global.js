/**
 * Copies /dist/toml.min.js to its respective Helma folder /Global
 */
const fs = require('fs');
fs.createReadStream('./dist/toml.min.js').pipe(fs.createWriteStream('../../code/Global/toml.min.js'));
console.log('>>> Minified TOML js copied to /Global.');