interface argv {
  extract: boolean;
  e: boolean;
  generate: boolean;
  g: boolean;
  debug: boolean;
  d: boolean;
}

type tParams = string[];
type tMacro = { [s: string]: tParams };
type tHoptypes = { [s: string]: tMacro };

interface tDocMacros {
  cwd: string;
  dirs: string[];
  fileMatcher: RegExp;
  excludeHoptypes: string[];
  excludeMacros: string[];
  deprecatedMacros: string[];
  deprecatedParams: string[];
  docsPath: string;
  macros: tHoptypes;
  files: string[];
  yamlDoc: null | object;
}

declare class DocMacros implements tDocMacros {
  cwd: string;
  dirs: string[];
  fileMatcher: RegExp;
  excludeHoptypes: string[];
  excludeMacros: string[];
  deprecatedMacros: string[];
  deprecatedParams: string[];
  docsPath: string;
  macros: tHoptypes;
  files: string[];
  yamlDoc: null | object;  
  readRelevantProjectFiles(): string[];
  getCleanCodeFileContent(file: string): string;
  getMacroFunctionCode(text: string, start: number): string;
  extractMacrosFromFiles(): DocMacros;
  checkYamlDocForUpdates(): void;
  useMustacheTemplate(templateName: string, view: object, filePath: string): void;
  generateMarkdownFiles(): void;
}
