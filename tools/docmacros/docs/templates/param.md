## Parameter {{param}}

### Beispiel für die Anwendung des Parameters **{{param}}** im Macro *{{hoptype}}.{{macro}}*

Syntax: `<% {{hoptype}}.{{macro}} {{param}}=? %>`

{{#deprecated}}**Achtung**: Dieses Parameter hat den Status *"deprecated"*, d.h. er existiert zwar noch, wird aber mittelfristg aus dem Programmcode entfernt. Danach würde die Verwendung des Parameters keine Wirkung mehr haben.{{/deprecated}}

