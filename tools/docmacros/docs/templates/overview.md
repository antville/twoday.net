# Twoday-Macros (Übersicht)

Dies ist eine Übersichtsdatei mit einem allgemeinen Einleitungstext.

Folgende relevante Objekttypen stellen Macros für Endbenutzer bereit:

{{#hoptypes}}
  [{{.}}](./{{.}}.md)<br>
{{/hoptypes}}
