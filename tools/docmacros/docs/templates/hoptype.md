## Hoptype "{{hoptype}}"

Der Objekttyp **{{hoptype}}** besitzt folgende Macros:

{{#macros}}[<% {{hoptype}}.{{.}} %>](./{{hoptype}}/{{.}}.md)<br>{{/macros}}
