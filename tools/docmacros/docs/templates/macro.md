## Macro "{{hoptype}}.{{macro}}"

Syntax: `<% {{hoptype}}.{{macro}} {{paramStr}} %>`

Das Macro *{{macro}}* hat folgende mögliche Parameter:

{{#params}}[{{.}}](./{{hoptype}}/{{macro}}/{{.}}.md)<br>{{/params}}

{{#deprecated}}**Achtung**: Dieses Macro hat den Status *"deprecated"*, d.h. es existiert zwar noch, wird aber mittelfristg aus dem Programmcode entfernt. Danach würde die Verwendung des Macros keine Wirkung mehr haben.{{/deprecated}}