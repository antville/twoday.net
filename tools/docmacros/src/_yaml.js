const yaml = require('js-yaml');
const fs = require('fs');
const path = require('path');

/**
 * Reads a hoptype yaml file that holds the respective macro documentation
 * @param {string} hoptype valid Helma hoptype that features macros
 * @returns {object|null} parsed hoptype yaml as object or null if file doesn't exist
 */
const readHoptypeYamlDoc = hoptype => {
  try {
    const filePath = path.resolve(process.cwd(), 'docs', 'text', `${hoptype}.yaml`);
    const fileExists = fs.existsSync(filePath);

    const parsedYaml = fileExists ? yaml.load(fs.readFileSync(filePath, 'utf8')) : null;
    return parsedYaml;
  } catch (err) {
    throw new Error(`Error while reading "${hoptype}.yaml" file -> ${err}.`);
  }
};

/**
 * Writes a hoptype yaml file that holds the respective macro documentation
 * @param {string} hoptype valid Helma hoptype that features macros
 * @param {object} obj documentation object to be transformed to yaml file format
 * @returns {void}
 */
const writeHoptypeYamlDoc = (hoptype, obj) => {
  try {
    if (!Object.keys(obj).length) return;
    const filePath = path.resolve(process.cwd(), 'docs', 'text', `${hoptype}.yaml`);
    fs.writeFileSync(filePath, yaml.dump(obj, { styles: { '!!null': 'empty' } }));
  } catch (err) {
    throw new Error(`Error while writing "${hoptype}.yaml" file -> ${err}.`);
  }
};

/**
 * Reads the docMacros yaml file that holds the control/selection options
 * @returns {object} parsed docMacros yaml as object
 */
const readControlOptions = () => {
  try {
    const defaultOptions = {
      searchDirs: ['code'],
      fileMatcher: '.js',
      excludeHoptypes: [],
      excludeMacros: [],
      sysAdminMacros: '^(sysmgr|sys_)',
      deprecatedMacros: [],
      deprecatedParams: [],
      exampleParamValuesFor: [],
      searchParamValuesFor: []
    };
    const filePath = path.resolve(process.cwd(), 'src', 'docMacros.yaml');
    const controlOptions = yaml.load(fs.readFileSync(filePath, 'utf8'));
    const finalOptions = Object.assign({}, defaultOptions, controlOptions);
    finalOptions.fileMatcher = new RegExp(finalOptions.fileMatcher, 'i');
    finalOptions.sysAdminMacros = new RegExp(finalOptions.sysAdminMacros, 'i');
    return finalOptions;
  } catch (err) {
    throw new Error(`Error while reading "docMacros.yaml" control options file -> ${err}.`);
  }
};

module.exports = { readHoptypeYamlDoc, writeHoptypeYamlDoc, readControlOptions };
