const argv = require('yargs').options({
  extract: {
    alias: 'e',
    default: true,
    description: 'Extract all macros from relevant js sourcefiles',
    type: 'boolean'
  },
  generate: {
    alias: 'g',
    default: false,
    description: 'Update macros and params in yaml docs and (re-)generate markdown files',
    type: 'boolean'
  },
  debug: {
    alias: 'd',
    default: false,
    description: 'Issue extended logging messages during script processing',
    type: 'boolean'
  }
}).argv;
const fsrr = require('fs-readdir-recursive');
const fs = require('fs');
const mustache = require('mustache');
const path = require('path');
const slash = require('slash');
const strip = require('strip-comments');
const { readHoptypeYamlDoc, writeHoptypeYamlDoc, readControlOptions } = require('./_yaml');

class DocMacros {
  constructor() {
    Object.assign(this, readControlOptions());
    this.cwd = __dirname.split(`${path.sep}tools`)[0];
    this.docsPath = path.resolve(this.cwd, 'tools/docmacros/docs');
    this.files = this.readRelevantProjectFiles();
    this.macros = {};
    this.yamlDoc = null;
  }

  readRelevantProjectFiles() {
    let files = [];
    this.searchDirs.forEach(folder => {
      let fullPath = path.resolve(this.cwd, folder);
      files = files.concat(
        fsrr(fullPath, name => {
          return (
            (!name.includes('.') && !this.excludeHoptypes.includes(name)) ||
            this.fileMatcher.test(name)
          );
        }).map(file => `${folder}/${file}`)
      );
    });
    return files;
  }

  getCleanCodeFileContent(file) {
    try {
      // read source js file as string
      const code = fs.readFileSync(path.resolve(this.cwd, file)).toString();
      // strip off all comments
      return (
        strip(code, {
          line: true,
          block: true,
          keepProtected: false,
          preserveNewLines: false
        })
          // remove empty lines
          .split('\n')
          .map(line => line.trim())
          .filter(line => line.length)
          .join('\n')
      );
    } catch (err) {
      throw new Error(`Error while stripping comments off file ${file} -> ${err}`);
    }
  }

  getMacroFunctionCode(text, start) {
    const len = text.length;
    let pos = start;
    // find function opening bracket
    while (pos < len && text[pos] !== '{') pos++;
    let openBrackets = 1;
    pos++;
    // find function ending bracket
    while (pos < len && openBrackets !== 0) {
      switch (text[pos]) {
        case '{':
          openBrackets++;
          break;
        case '}':
          openBrackets--;
          break;
      }
      pos++;
    }
    return text.substr(start, pos - start + 1);
  }

  extractMacrosFromFiles() {
    this.files.forEach(file => {
      const filePath = slash(file);
      const hoptype = filePath.split('/').slice(-2, -1).pop();
      if (!this.macros[hoptype]) this.macros[hoptype] = {};

      const js = this.getCleanCodeFileContent(file);
      const macroFunctionMatcher = js.matchAll(/function (.*)_macro/gi);

      for (let macroFunction of macroFunctionMatcher) {
        const macroName = macroFunction[1];
        if (this.excludeMacros.includes(`${hoptype}.${macroName}`)) continue;

        if (!this.macros[hoptype][macroName]) this.macros[hoptype][macroName] = [];

        const paramSaver = this.macros[hoptype][macroName];
        const macroCommand = this.getMacroFunctionCode(js, macroFunction.index);
        const paramMatcher = macroCommand.matchAll(/param\.(\w*)/gi);

        for (let param of paramMatcher) {
          const paramName = param[1];
          if (!paramSaver.includes(paramName)) {
            let paramValues = [];
            const valueRegEx = new RegExp(`\.${paramName} [!=]=+ "(.+?)"`, 'gi');
            const valueMatcher = macroCommand.matchAll(valueRegEx);
            for (let v of valueMatcher) {
              let value = v[1];
              if (!paramValues.includes(value)) paramValues.push(value);
            }
            if (paramValues.length) console.log(this.getToken(hoptype, macroName), paramName, paramValues);
            paramSaver.push(paramName);
          }
        }
      }
    });
    return this;
  }

  getToken(hoptype, macroName) {
    hoptype = hoptype.toLowerCase();
    return (hoptype === 'global' ? '' : `${hoptype}.`) + `${macroName}`;
  }

  createNewHoptypeDoc(hoptype) {
    this.yamlDoc = {};
    console.log(`Hoptype ${hoptype}.yaml neu erstellt.`);
  }

  createNewMacroEntry(hoptype, macroName) {
    const macroCommand = this.getToken(hoptype, macroName);
    this.yamlDoc[macroName] = {
      use: 'ToDo: Beschreibung des Zwecks',
      code: `<% ${macroCommand} %>`,
      sys: this.sysAdminMacros.test(macroName)
    };
    console.log(`Macro ${macroCommand} neu aufgenommen.`);
  }

  getParamExampleValue(macroName, paramName) {
    // check if there is a macro specific param value
    const macroParamKey = `${macroName}.${paramName}`;
    if (this.exampleParamValuesFor.hasOwnProperty(macroParamKey))
      return this.exampleParamValuesFor[macroParamKey];

    // check if there is a generic (cross-macro) param value
    if (this.exampleParamValuesFor.hasOwnProperty(paramName))
      return this.exampleParamValuesFor[paramName];

    // no predefined example values; then return dummy literal
    return '{beispielinhalt}';
  }

  createNewParamEntry(hoptype, macroName, paramName) {
    const macroCommand = this.getToken(hoptype, macroName);
    const exampleText = this.getParamExampleValue(macroName, paramName);
    this.yamlDoc[macroName][`_${paramName}`] = {
      use: 'ToDo: Beschreibung der Parameterwirkung',
      code: `<% ${macroCommand} ${paramName}="..." %>`,
      example:
        `<% ${macroCommand} ${paramName}="${exampleText}" %>\n` + 'ToDo: Beschreibung des Beispiels'
    };
    console.log(`Param "${paramName}" in ${macroCommand} neu ergänzt.`);
  }

  checkYamlDocForUpdates() {
    for (let hoptype in this.macros) {
      if (hoptype !== 'File' && hoptype !== 'Global') continue; // FixMe: temporary!
      let hasChanged = false;
      this.yamlDoc = readHoptypeYamlDoc(hoptype);
      if (!this.yamlDoc) {
        this.createNewHoptypeDoc(hoptype);
        hasChanged = true;
      }
      for (let macroName in this.macros[hoptype]) {
        if (!this.yamlDoc.hasOwnProperty(macroName)) {
          this.createNewMacroEntry(hoptype, macroName);
          hasChanged = true;
        }
        for (let paramName of this.macros[hoptype][macroName]) {
          if (!this.yamlDoc[macroName].hasOwnProperty(`_${paramName}`)) {
            this.createNewParamEntry(hoptype, macroName, paramName);
            hasChanged = true;
          }
        }
      }
      if (hasChanged) writeHoptypeYamlDoc(hoptype, this.yamlDoc);
    }
  }

  useMustacheTemplate(templateName, view, filePath) {
    let tmpl = fs.readFileSync(path.resolve(this.docsPath, 'templates', `${templateName}.md`));
    let rendered = mustache.render(tmpl.toString(), view);
    fs.writeFileSync(path.resolve(this.docsPath, 'files', filePath), rendered);
  }

  generateMarkdownFiles() {
    // select and process only hoptypes that have macros
    const hoptypes = Object.keys(this.macros).filter(
      hoptype => Object.keys(this.macros[hoptype]).length
    );
    this.useMustacheTemplate('overview', { hoptypes }, 'index.md');

    hoptypes.forEach(hoptype => {
      const macros = Object.keys(this.macros[hoptype]).sort();
      this.useMustacheTemplate('hoptype', { hoptype, macros }, `${hoptype}.md`);
      const hoptypeFolder = path.resolve(this.docsPath, 'files', hoptype);
      if (!fs.existsSync(hoptypeFolder)) fs.mkdirSync(hoptypeFolder);

      macros.forEach(macro => {
        const params = this.macros[hoptype][macro];
        const paramStr = params.map(param => `${param}="?"`).join(' ');
        const deprecated = this.deprecatedMacros.includes(`${hoptype}.${macro}`);
        this.useMustacheTemplate(
          'macro',
          { hoptype, macro, params, paramStr, deprecated },
          `${hoptype}/${macro}.md`
        );
      });
    });
  }
}

if (!argv.extract && !argv.generate) {
  console.log('Desired action must be specified with --extract|-e  and/or  --generate|-g');
  process.exit(1);
}

const docMacros = new DocMacros();
if (argv.extract) docMacros.extractMacrosFromFiles().checkYamlDocForUpdates();
if (argv.generate) docMacros.generateMarkdownFiles();
