/**
 * Filter and parse the twoday.net DDL to JSON
 * ===========================================
 * 
 */
const fs = require('fs');
const path = require('path');
const { Parser } = require('sql-ddl-to-json-schema');

const parser = new Parser('mysql');

/**
 * Filters unwanted DDL statements and returns an essential data definition string
 * @param {string} fileName sql file with DDL statements
 * @returns {string} filtered (clean) DDL string
 */
const readAndFilterSQL = fileName => {

  const sql = fs.readFileSync(path.resolve('./src/data/input', fileName)).toString();
  const unwantedSQL = ['insert', 'select', 'update', 'delete'];

  return sql
    .split('\n')
    .filter(line => 
      line.trim().length > 0 && // skip empty lines
      line[0] !== '#' // skip comments
    )
    .join('\n')
    .split(';')
    .filter(cmd => !unwantedSQL.includes(cmd.trim().substr(0, 6).toLowerCase()))
    .join(';');

};

/**
 * Parses Twodays MySQL/MariaDB DDL statements and returns compact JSON format
 * @param {string} fileName sql file with DDL statements
 * @param {boolean} saveIntermediateFiles saves files for review/test
 * @returns {object} DDL statements in JSON format
 */
const parseDDL = (fileName, saveIntermediateFiles) => {

  // Exec filter and write result for test/review
  const filtered = readAndFilterSQL(fileName);
  if (saveIntermediateFiles) fs.writeFileSync('./src/data/input/filtered.sql', filtered);

  // Parse clean DDL string, convert to JSON and write for test/review
  parser.feed(filtered);
  const compactJsonDDL = parser.toCompactJson(parser.results);
  if (saveIntermediateFiles) fs.writeFileSync('./src/data/compact-ddl.json', JSON.stringify(compactJsonDDL, null, 2));

  return compactJsonDDL;
}

module.exports = { parseDDL };
