const argv = require('yargs').options({
  save: {
    alias: 's',
    default: false,
    description: 'Save intermediate output files to disk for review/test',
    type: 'boolean',
  },
  list: {
    alias: 'l',
    default: false,
    description: 'List issues/problems/inconsistencies',
    type: 'boolean',
  },
}).argv;

const chalk = require('chalk');
const fs = require('fs');
const { EOL } = require('os');
const { parseDDL } = require('./ddlParser');
const { parseProperties } = require('./propParser');
const path = require('path');

// use "npm run dev" to write intermediate files for review, otherwise "npm run build" for no intermediate outputs
const saveIntermediateFiles = argv.save;
if (saveIntermediateFiles)
  console.log(chalk.blue('Intermediate result files will be written for review/test...'));

// generated mermaid-js class diagram input definitions
let code = '';

/**
 * Adds code string with leading spaces depending on level
 * @param {String} cmd Single Code/Command used in a Mermaid class diagram 
 * @param {Number} level Level that determins the space inset (currently only 0|1)
 * @returns {void} [changes global variable "code"]
 */
const addCode = (cmd, level = 1) => {
  code += `${' '.repeat(level * 4)}${cmd}${EOL}`;
};

/**
 * Returns a formatted field length for selected datatypes (currently only int|varchar)
 * @param {object} column field column object in enrichedDDL
 * @returns {String} int{xx} | varchar{xx} | empty string
 */
const getLen = column => {
  switch (column.type.datatype) {
    case 'int':
      return `{${column.type.width}}`;
    case 'varchar':
      return `{${column.type.length}}`;
    default:
      return '';
  }
};

const compactDDL = parseDDL('combined.sql', saveIntermediateFiles);
const enrichedDDL = parseProperties(compactDDL, saveIntermediateFiles);

console.log(chalk.blue('\nNow generating mermaid class diagram code...\n'));

// maps entity name to physical table, e.g. Root => AV_ROOT
const nameMapper = enrichedDDL.reduce((all, table) => {
  if (table.entity) all[table.entity] = table.name;
  return all;
}, {});

// initialize Mermaid class diagram
addCode('classDiagram', 0);
let noPropNames = 0;

// inspect each table definition
enrichedDDL.forEach(table => {
  addCode(`class ${table.name}`);
  if (!table.name.startsWith('AV_')) addCode(`<<abstract>> ${table.name}`);

  table.columns
    // sort table columns: lowercase before uppercase
    .sort((a, b) => {
      let aName = a.propName || a.name;
      let bName = b.propName || b.name;
      let sortA = `${aName.charCodeAt(0) < 95 ? '~' : ' '}${aName}`;
      let sortB = `${bName.charCodeAt(0) < 95 ? '~' : ' '}${bName}`;
      if (sortA < sortB) return -1;
      else if (sortA > sortB) return +1;
      else return 0;
    })

    // process each table column
    .forEach(column => {
      let isForeignKey = column.name.includes('_F_');
      if (argv.list && !column.hasOwnProperty('propName') && !isForeignKey) {
        noPropNames++;
        if (noPropNames === 1) {
          console.log(`The following database fields were found to be not present in any type.properties file and hence are not referenced in any public twoday code whatsoever (no prop name):\n`);
        }
        console.log(`  ${table.name} -> ${column.name} not found in type.properties.`);
      }
      let fType = column.type.datatype[0].toUpperCase() + column.type.datatype.slice(1);
      addCode(
        `${table.name} : ${isForeignKey ? '#' : '+'}${fType}${getLen(column)} ${
          column.propName || column.name
        }`
      );
    });

  if (table.hasOwnProperty('objects')) {
    table.objects.forEach(obj => {
      addCode(`${table.name} : +Obj{${obj.ref}} ${obj.name}`);
    });
  }

  if (table.hasOwnProperty('collections')) {
    let collSet = new Set();
    table.collections.forEach(coll => {
      let ref = coll.ref in nameMapper ? nameMapper[coll.ref] : coll.ref;
      collSet.add(ref);
      addCode(`${table.name} : ~Coll(${ref}) ${coll.name}`);
    });
    for (let ref of collSet)
      addCode(`${table.name} <-- ${ref} : Collection ${ref}`);
  }

  if (table.hasOwnProperty('mountpoints')) {
    table.mountpoints.forEach(obj => {
      addCode(`${table.name} : #8593;Mount{${obj.ref}} ${obj.name}`);
    });
  }

});

fs.writeFileSync('./src/data/mermaid.txt', code);
console.log(chalk.blue('\nMermaid class diagram definitions written to data/mermaid.txt.'));
