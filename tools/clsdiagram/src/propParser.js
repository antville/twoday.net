/**
 * Filter and parse the type.properties, then add selected info to the DDL json
 * ============================================================================
 * 
 */
const chalk = require('chalk');
const fg = require('fast-glob');
const fs = require('fs');
const path = require('path');

const getTableIndex = (ddl, tableName) => {

  for (i = 0, l = ddl.length; i < l; i++) {
    if (ddl[i].name === tableName) break;
  }

  if (i < l) return i;
  else {
    ddl.push({
      name: tableName,
      entity: '',
      columns: []
    });
    console.log(chalk.grey(`Entity "${tableName}" did not exist in DDL (assumed virtual class).`));
    return ddl.length - 1;
  }

};

const getPropIndex = (table, propName) => {

  // console.log(`Searching prop ${propName} in table ${table.name}`);
  for (i = 0, l = table.columns.length; i < l; i++) {
    if (table.columns[i].name === propName) break;
  }

  // if found, then return the field index
  if (i < l) return i;
  // else add the non-existent field, log and return appropriate index
  else {
    table.columns.push({
      name: propName,
      type: { datatype: '?new?' }
    });
    console.log(chalk.yellow(`Prop "${propName}" did not exist in table ${table.name}!`));
    return table.columns.length - 1;
  }
};

/**
 * Reads, filters and formats JAVA properties file
 * @returns {Object[]} Array of objects { key, value }
 */
const readAndFilterProps = () => {

  const propFile = fs.readFileSync(path.resolve(process.cwd(), './src/data/input/type.properties')).toString();

  return propFile
    .split('\n')
    .reduce((all, line) => {

      let [key, value = ''] = line
        .split('=')
        .map(part => part.trim());

      if (key.length && key[0] !== '#' && key !== '_db' && !key.includes('.')) all.push({ key, value });

      return all;

    }, []);

};

/**
 * Adds additional information to an table
 * @param {object} table Table object to add the information to
 * @param {string} key Key of table object 
 * @param {string[] | object} value Array of strings or an object to be added under the key
 * @returns {void}
 */
const addToTable = (table, key, value) => {

  if (!table.hasOwnProperty(key)) table[key] = [];

  if (Array.isArray(value)) table[key].push(...value);
  else table[key].push(value);

}

/**
 * Uses compacted DDL statements and enriches data with info from properties files
 * @param {object} ddl compacted DDL JSON received from ddlParser 
 * @param {boolean} saveIntermediateFiles saves files for review/test
 * @returns {object} enriched DDL statements with integrated additional info from type.properties
 */
const parseProperties = (ddl, saveIntermediateFiles) => {

  let table = null, entity = '', m = null;

  let props = readAndFilterProps();
  if (saveIntermediateFiles) fs.writeFileSync('./src/data/input/keyvalue.properties', JSON.stringify(props, null, 2));

  props.forEach((item, index) => {

    if (item.key === '_entity') {
      entity = item.value;
      // if the following key is not a _table statement, then create a virtual class of table name
      if (props[index + 1].key !== '_table') table = ddl[getTableIndex(ddl, entity)];
      return;
    }

    if (item.key === '_table') {
      table = ddl[getTableIndex(ddl, item.value)];
      table.entity = entity;
      return;
    }

    if (item.key === '_parent' || item.key === '_extends') {
      addToTable(table, item.key.substr(1), item.value.split(',').map(value => value.trim()));
      return;
    }

    m = item.value.match(/object\s*\((\w+)\)/i);
    if (m) {
      addToTable(table, 'objects', { name: item.key, ref: m[1] });
      return;
    }

    m = item.value.match(/collection\s*\((\w+)\)/i);
    if (m) {
      addToTable(table, 'collections', { name: item.key, ref: m[1] });
      return;
    }

    m = item.value.match(/mountpoint\s*\((\w+)\)/i);
    if (m) {
      addToTable(table, 'mountpoints', { name: item.key, ref: m[1] });
      return;
    }

    let p = getPropIndex(table, item.value);
    table.columns[p].propName = item.key;

  });

  if (saveIntermediateFiles) fs.writeFileSync(path.resolve(process.cwd(), 'src/data', 'enriched-ddl.json'), JSON.stringify(ddl, null, 2));
  return ddl;

}

module.exports = { parseProperties };
