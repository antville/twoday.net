/**
 * Fetch SQL DDL data and all type.properties files
 * ================================================
 *
 */
const chalk = require('chalk');
const fg = require('fast-glob');
const fs = require('fs');
const path = require('path');

// twoday.net repo url on Gitlab
const repoGitlabAPI = 'https://gitlab.com/api/v4/projects/8966097/repository/';
// twoday.net repo branch
const repoBranch = 'dev';
// add to get a file content base64 encoded
const apiFileUrl = 'files/';

/**
 * Writes a given content to a file in the data/input directory
 * @param {string} fileName file name for this content
 * @param {string} content string to write
 * @returns {void}
 */
const writeContentToFile = (fileName, content) => {
  fs.writeFileSync(
    path.resolve(process.cwd(), 'src/data/input', fileName),
    content
  );
  console.log(`Data written to input/${fileName}.\n`);
};

/**
 * Reads one or more files from Gitlab and returns them as a combined string
 * @param {string[]} treePaths Array of treePath strings (path and filename, e.g. 'code/Root/type.properties')
 * @returns {Promise}
 */
const readGitlabFiles = async treePaths => {
  let content = [];
  for (treePath of treePaths) {
    try {
      const res = await fetch(
        `${repoGitlabAPI}${apiFileUrl}${encodeURIComponent(
          treePath
        )}?ref=${repoBranch}`
      );
      const json = await res.json();
      console.log(`Reading ${treePath} completed.`);
      const isSQL = treePath.substr(-3) === 'sql';
      const origin =
        `${isSQL ? '--' : '##'} --> ${treePath} <--${isSQL ? ';' : ''}\n` +
        (isSQL ? '' : `_entity = ${treePath.split('/').slice(-2, -1)}\n`);
      content.push(origin + Buffer.from(json.content, 'base64').toString());
    } catch (e) {
      console.error('readGitlabFiles: ' + e);
    }
  }
  return content.join('\n');
};

const getFilePaths = async filePattern => {
  try {
    const filePaths = await fg(`../../(build|code|libraries|modules)/**/${filePattern}`);
    if (filePaths.length) {
      return filePaths.map(path => path.substr(6)); // deletes prefix '../../' 
    } else throw new Error(`Error: No ${filePattern} files found!?`);
  } catch (e) {
    console.log(`Error in getFilePaths: ${e}`);
    return [];
  }
};

/**
 * Reads Twoday's DB SQL (DDL) from Gitlab and stores it as a file to the data/input directory
 * @returns {Promise}
 */
const readDbSQLFiles = () => {
  console.log(chalk.blue('Searching twoday.net project for SQL files...'));
  return getFilePaths('*.sql')
    .then(paths =>
      paths.filter(path => !/oracle|dbpatch|tdyUpgrade/.test(path))
    )
    .then(paths => {
      console.log(chalk.blue(`...found ${chalk.yellow(paths.length)} SQL files (while ignoring oracle/dbpatch/upgrade files).`));
      return readGitlabFiles(paths);
    })
    .then(content => writeContentToFile('combined.sql', content))
    .catch(e => console.error(e));
};

/**
 * Reads all type.properties files from Gitlab and stores them as a combined file to the data/input directory
 * @returns {Promise}
 */
const readTypePropertyFiles = () => {
  console.log(chalk.blue('Searching twoday.net project for type.properties...'));
  return getFilePaths('type.properties')
    .then(paths => {
      console.log(chalk.blue(`...found ${chalk.yellow(paths.length)} type.properties files.`));
      return readGitlabFiles(paths);
    })
    .then(content => writeContentToFile('type.properties', content))
    .catch(e => console.error(e));
};

readDbSQLFiles()
  .then(() => readTypePropertyFiles())
  .then(() => console.log(chalk.blue('Fetching input files completed.')));
