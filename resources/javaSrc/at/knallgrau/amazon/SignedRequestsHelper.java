package at.knallgrau.amazon;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Map;
import java.util.SortedMap;
import java.util.TimeZone;
import java.util.TreeMap;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.EncoderException;
import org.apache.commons.codec.binary.Base64;

/**
 * helper class for signing amazon aws request params
 * @author fpm
 *
 */
public class SignedRequestsHelper {
	
	/**
	 * Allways use UTF-8 as encoding
	 */
	private static final String UTF8_CHARSET = "UTF-8";
	
	/**
	 * use this to sign the request
	 */
	private static final String HMAC_SHA256_ALGORITHM = "HmacSHA256";
	
	/**
	 * the uri to request
	 */
	private static final String REQUEST_URI = "/onca/xml";
	
	/**
	 * method to use to get response from aws
	 */
	private static final String REQUEST_METHOD = "GET";

	/**
	 * hostname to make request to. can be overwritten in constuctor
	 */
	private String endpoint = "ecs.amazonaws.com"; // must be lowercase
	
	/**
	 * public key to aws
	 */
	private String awsAccessKeyId = null;
	
	/**
	 * storage for #SecretKeySpec
	 */
	private SecretKeySpec secretKeySpec = null;
	
	/**
	 * storage for rhe crytp mechanism
	 */	
	private Mac mac = null;

	/**
	 * Creates a new SignedRequestsHelper
	 * 
	 * @param awsAccessKeyId public aws key
	 * @param awsSecretKey secret aws key
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeyException
	 */
	public SignedRequestsHelper(String awsAccessKeyId, String awsSecretKey)
			throws UnsupportedEncodingException, NoSuchAlgorithmException,
			InvalidKeyException {
		this.awsAccessKeyId = awsAccessKeyId;
		initMac(awsSecretKey);
	}

	/**
	 * Creates a new SignedRequestsHelper
	 * 
	 * @param awsAccessKeyId public aws key
	 * @param awsSecretKey secret aws key
	 * @param endpoint the hostname of the aws service
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeyException
	 */
	public SignedRequestsHelper(String awsAccessKeyId, String awsSecretKey, String endpoint)
			throws UnsupportedEncodingException, NoSuchAlgorithmException,
			InvalidKeyException {
		this.awsAccessKeyId = awsAccessKeyId;
		this.endpoint = endpoint;
		initMac(awsSecretKey);
	}

	/**
	 * 
	 * Creates the Mac for the Signing javax.crypto.Mac
	 * 
	 * @param awsSecretKey secret aws key
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeyException
	 */
	private void initMac(String awsSecretKey)
			throws UnsupportedEncodingException, NoSuchAlgorithmException,
			InvalidKeyException {
		byte[] secretyKeyBytes = awsSecretKey.getBytes(UTF8_CHARSET);
		secretKeySpec = new SecretKeySpec(secretyKeyBytes,
				HMAC_SHA256_ALGORITHM);
		mac = Mac.getInstance(HMAC_SHA256_ALGORITHM);
		mac.init(secretKeySpec);
	}
	
	/**
	 * creates the url for the aws request. sorts Parameters, does the url encoding and generates
	 * the signature.
	 * 
	 * @param params Map of request parameters
	 * @return complete url with endpoint, parameters and signature
	 */
	public String generateUrl(Map<String, String> params) {
		params.put("AWSAccessKeyId", awsAccessKeyId);
		params.put("Timestamp", timestamp());

		SortedMap<String, String> sortedParamMap = new TreeMap<String, String>(
				params);
		String canonicalQS = canonicalize(sortedParamMap);
		String toSign = REQUEST_METHOD + "\n" + endpoint + "\n" + REQUEST_URI
				+ "\n" + canonicalQS;

		String hmac = hmac(toSign);
		String sig = percentEncodeRfc3986(hmac);
		String url = "http://" + endpoint + REQUEST_URI + "?" + canonicalQS
				+ "&Signature=" + sig;

		return url;
	}

	/**
	 * Signs the given String with {@link #mac} and encodes it Base64
	 * @param stringToSign some String to sign
	 * @return {@link #mac} and Base64 encoded String
	 */
	private String hmac(String stringToSign) {
		String signature = null;
		byte[] data;
		byte[] rawHmac;
		try {
			data = stringToSign.getBytes(UTF8_CHARSET);
			rawHmac = mac.doFinal(data);
			Base64 encoder = new Base64();
			signature = new String(encoder.encode(rawHmac));
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(UTF8_CHARSET + " is unsupported!", e);
		} catch (EncoderException e) {
			throw new RuntimeException("EncoderException!", e);
		}
		return signature;
	}

	/**
	 * creates a new Timestamp String current Date
	 * 
	 * @return String timestamp
	 */
	private String timestamp() {
		String timestamp = null;
		Calendar cal = Calendar.getInstance();
		DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		dfm.setTimeZone(TimeZone.getTimeZone("GMT"));
		timestamp = dfm.format(cal.getTime());
		return timestamp;
	}

	/**
	 * does the Uri encoding for all key value pairs
	 * 
	 * @param sortedParamMap Map of parameters, key is the name of the parameter
	 * @return cannoical Parameters
	 */
	private String canonicalize(SortedMap<String, String> sortedParamMap) {
		if (sortedParamMap.isEmpty()) {
			return "";
		}

		StringBuffer buffer = new StringBuffer();
		Iterator<Map.Entry<String, String>> iter = sortedParamMap.entrySet()
				.iterator();

		while (iter.hasNext()) {
			Map.Entry<String, String> kvpair = iter.next();
			buffer.append(percentEncodeRfc3986(kvpair.getKey()));
			buffer.append("=");
			buffer.append(percentEncodeRfc3986(kvpair.getValue()));
			if (iter.hasNext()) {
				buffer.append("&");
			}
		}
		String cannoical = buffer.toString();
		return cannoical;
	}

	/**
	 * encodes a string for use in urls. Does some extra replacement like
	 * <ul>
	 * <li>"+" => "%20"</li> 
	 * <li>"*" => "%2A"</li> 
	 * <li>"%7E" => "~"</li> 
	 * </ul>
	 * @param s some String to urlencode
	 * @return String urlencoded
	 */
	private String percentEncodeRfc3986(String s) {
		String out;
		try {
			out = URLEncoder.encode(s, UTF8_CHARSET).replace("+", "%20")
					.replace("*", "%2A").replace("%7E", "~");
		} catch (UnsupportedEncodingException e) {
			out = s;
		}
		return out;
	}
}
