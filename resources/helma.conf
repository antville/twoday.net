##############################################################
###
###   Helma object publisher config file
###   Author:  Hannes Wallnoefer, <hannes@helma.at>
###   Author:  Andreas Bolka
###
###   This file should be placed in /etc/helma.conf.
###   It is read by the Helma service control script, 
###   usually /etc/init.d/helma.
###
##############################################################

##############################################################
###   The name of this Helma server/service and the
###   pid file to be used
#############################################################
HELMA_SERVICE=helma
HELMA_PID=/var/run/helma.pid


##############################################################
###   Full path to Java executable
##############################################################
JAVA_HOME=/usr/lib/jvm/java-8-oracle
JAVA_BIN=java

##############################################################
###    Options passed to the Java runtime
##############################################################
# -XX:+HeapDumpOnOutOfMemoryError
# -verbose:gc
# -XX:+PrintGCDetails
# -XX:+PrintGCTimeStamps
# -Xloggc:/var/log/helma/gc.log
# -Djdbcdecorator.configuration=/usr/local/helma/jdbclogger.properties
# -Djdbcstatistics.configuration=/usr/local/helma/jdbcstatistics.properties
# -Dknallgrau.Amazon.secretKey=YlAH/9AJHCAK7TJltWgrcd+wqJZFvE8GwEoqUKdk 
# -XX:OnOutOfMemoryError=\"/usr/local/bin/onOutOfMemory.sh\" -XX:+CMSIncrementalMode -Djgroups.bind_addr=212.41.237.58
# -verbose:gc -XX:+PrintGCDetails -XX:+PrintGCTimeStamps -Xloggc:/var/log/helma/gc.log
JAVA_OPTS="-Djava.awt.headless=true -server -Xms2048m -Xmx4192m -XX:NewSize=768m -XX:MaxNewSize=768m -XX:+UseConcMarkSweepGC -Dfile.encoding=utf-8 -Dmail.mime.charset=utf-8 -Djavax.xml.parsers.DocumentBuilderFactory=org.apache.xerces.jaxp.DocumentBuilderFactoryImpl -Djavax.xml.parsers.SAXParserFactory=org.apache.xerces.jaxp.SAXParserFactoryImpl" 

##############################################################
###   Helma install directory. This is where we look for 
###   the Helma jar files (launcher.jar, lib/* and lib/ext/*)
##############################################################
HELMA_INSTALL=/usr/local/helma


##############################################################
###   Helma home directory, in case it is different from the
###   Helma install dir. This is where Helma will look for 
###   properties files and applications.
##############################################################
HELMA_HOME=$HELMA_INSTALL


##############################################################
###   The user Helma should be running as
##############################################################
HELMA_USER=helma


##############################################################
###   File to which standard and error output from Helma 
###   is redirected
##############################################################
HELMA_LOG=/var/log/helma-out.log


##############################################################
###   Helma options. Possible options are:
###
###    -f file    Specify server.properties file
###    -p port    Specify RMI port number
###    -w port    Specify port number for embedded Web server
###    -x port    Specify XML-RPC port number
###    -jk port   Specify AJP13 port number
###
##############################################################
HELMA_ARGS="-w 127.0.0.1:8080 -jk 127.0.0.1:8009 -x :8081"
