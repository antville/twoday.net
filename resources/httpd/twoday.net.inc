ServerName twoday.net
ServerAlias *.twoday.net *.dev.twoday.net
ServerAlias 95.216.39.202 95.216.39.219

DocumentRoot /var/www/twoday/static

<Directory /var/www/twoday/static>
  AllowOverride None
</Directory>

ErrorDocument 500 /static/apache/error500.html
ErrorDocument 503 /static/apache/error500.html
ErrorDocument 403 /static/apache/error500.html
ErrorDocument 404 /static/apache/error404.html

<Location />
  Require all granted
  #Require host 089144220149.atnat0029.highway.webapn.at
</Location>

## FIXME: causes mixed-content errors for non-HTTPS images et al.
#ProxyPass /helma http://127.0.0.1:8080/helma
#ProxyPassReverse /helma http://127.0.0.1:8080/helma
#ProxyPreserveHost On
#RequestHeader set X-Forwarded-Proto "https"

RewriteEngine On
#LogLevel rewrite:trace5

## Permanently redirect these sites
RewriteCond %{HTTP_HOST} ^ithuba.twoday.net$
RewriteRule .* http://www.ithuba.org [R=permanent]
RewriteCond %{HTTP_HOST} ^elooffice.twoday.net$
RewriteRule .* http://blog.elooffice.de [R=permanent]

## Block some clients
RewriteMap hosts-deny txt:/etc/apache2/hosts.deny
RewriteCond ${hosts-deny:%{REMOTE_HOST}|NOT-FOUND} !=NOT-FOUND [OR]
RewriteCond ${hosts-deny:%{REMOTE_ADDR}|NOT-FOUND} !=NOT-FOUND
RewriteRule ^/.* - [F]

## Redirect IP to domain name
RewriteCond %{HTTP_HOST} ^(\d+\.){3}\d+$ 
RewriteRule ^/(.*) https://twoday.net/$1 [R=permanent]

## Truncate leading www from domain
RewriteCond %{HTTP_HOST} ^www\.(.+\.twoday\.net)$
RewriteRule ^/(.*) https://%1/$1 [R=permanent,L]

## Serve HTTPS only
RewriteCond %{HTTPS} off
RewriteRule ^/(.*) https://%{SERVER_NAME}/$1 [R=permanent]

## Add “development” stamp to every HTML output
#<Location ~ "^/(?!static)">
#  AddOutputFilterByType SUBSTITUTE text/html
#  Substitute "s|(</head>)|<style>#dev-stamp { font-size: 15vw; font-family: monospace; position: fixed; top: 150px; left: -100px; right: 0; bottom: 0; pointer-events: none; transform: rotate(10deg); opacity: 0.05; color: red; text-transform: uppercase; }</style>$1|i"
#  Substitute "s|(</body>)|<div id='dev-stamp'>development</div>$1|i"
#</Location>

## Rewrite RFC-5785 directory
RewriteRule ^/(\.well-known/.*) /$1 [PT,L]

## Configure the static directory and some static files
RewriteRule ^/static/(.*) /$1 [L]
RewriteRule ^/(modToolbar\.css|modToolbar\.js|jsLib\.js) /$1 [L]

## Configure static.twoday.net
RewriteCond %{HTTP_HOST} ^static\.
RewriteRule ^/(.*) /$1 [L]

## Rewrite special pages for some sites
RewriteCond %{HTTP_HOST} ^(chorherr)\.
RewriteRule ^/buch.* /helma/twoday/%1/stories/38791993 [PT,L]

## Pass through request to Helma
RewriteCond %{HTTP_HOST} ^twoday.net$
RewriteRule ^/(.*) /helma/twoday/$1 [PT,L]
RewriteCond %{HTTP_HOST} ^([^.]+)\.
RewriteRule ^/(.*) /helma/twoday/%1/$1 [PT,L]

## FIXME: evaluate relevance
RewriteRule ^twoday\.net/robots.txt /robots.txt [L]
RewriteRule ^twoday\.net/favicon.ico /favicon.ico [L]
RewriteCond /var/www/twoday/protectedStatic/www/cache/index.rdf -f
RewriteRule ^twoday\.net/rss /var/www/twoday/protectedStatic/www/cache/index.rdf [L]
RewriteCond /var/www/twoday/protectedStatic/www/cache/index.rdf -f
RewriteRule ^twoday\.net/index.rdf /var/www/twoday/protectedStatic/www/cache/index.rdf [L]
RewriteRule ^(.*)\..+\/favicon.ico /$1/images/favicon.ico [L]
