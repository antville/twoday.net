/**
 * macro returns the id of a HopObject
 */
function id_macro(param) {
   res.write(this._id);
   return;
}


/**
 * macro returns the url for any hopobject
 */
function href_macro(param) {
   res.write(this.href(param.action ? param.action : ""));
   return;
}


/**
 * macro rendering a skin or displaying
 * its source (param.as == "source")
 */
function skin_macro(param) {
   if (!param.name)
      return;
   if (param.as == "source") {
      var str = app.skinfiles[this._prototype][param.name];
      if (str && param.unwrap == "true")
         str = str.unwrap();
   } else
      var str = this.renderSkinAsString(param.name, param);
   res.write(str);
   return;
}


/**
 * this macro renders a text depending on
 * the value of a given property
 */
function switch_macro(param) {
   if (!param.name)
      return;
   res.write(this[param.name] ? param.on : param.off);
   return;
}


/**
 * generic macro that loops over the childobjects
 * and renders a specified skin for each of them
 * @param Object providing the following properties:
 *        skin: the skin to render for each item (required)
 *        collection: the collection containing the items
 *        limit: max. number of items per page
 *              (req.data.page determines the page number)
 *        sortby: function to be used for sorting the item list
 */
function loop_macro(param) {
   if (!param.skin)
      return;
   var items = param.collection ? this[param.collection] : this;
   if (!items || !items.size)
      return;
   // set default values
   var min = 0;
   var max = items.size();
   var pagesize = max;
   if (param.limit) {
      var n = parseInt(param.limit, 10);
      if (!isNaN(n))
         pagesize = n;
      var pagenr = parseInt(req.data.page, 10);
      if (isNaN(pagenr))
         pagenr = 0;
      min = Math.min(max, pagenr * pagesize)
      max = Math.min(max, min + pagesize);
   }
   if (param.sortby) {
      var sortFunction = this["sortby" + param.sortby];
      if (sortFunction) {
         var allitems = items.list();
         allitems.sort(sortFunction);
         var itemlist = allitems.slice(min, max);
      }
   } else
      var itemlist = items.list(min, pagesize);
   for (var i in itemlist)
      itemlist[i].renderSkin(param.skin);
   return;
}
