/**
 * constructor for a color object
 * @param IntegerOrRgbObject the red fraction or an rgb() object
 * @param Integer the green fraction
 * @param Integer the blue fraction
 * @return the resulting color object
 */
Color = function(R, G, B) {
   var value = null, name, hex, rgb;

   /**
    * return the decimal value of a color object 
    * @return Integer the decimal value
    */
   this.valueOf = function() {
      if (arguments.length > 0) {
         if (!rgb) {
            var compose = function(n, bits) {
               var div = Math.pow(2, bits);
               remainder = n % div;      
               return Math.floor(n/div);
            }
            var remainder = value;
            rgb = {
               red: compose(remainder, 16),
               green: compose(remainder, 8),
               blue: compose(remainder, 0)
            }
         }
         return rgb[arguments[0]];
      }
      return value;
   };

   /**
    * return the hexidecimal value of a color object
    * @return String the hexidecimal value
    */
   this.toString = function() {
      if (!value)
         return null;
      if (!hex)
         hex = value.toString(16).pad("0", 6, String.LEFT);
      return hex;
   };

   // the main code
   if (arguments.length % 2 == 0)
      throw("Insufficient arguments for creating Color");
   if (arguments.length == 1) {
     if (typeof R == "number") {
         value = R;
      } else if (typeof R == "string") {
         R = R.toLowerCase();
         if (Color.nameToHex[R]) {
            this.name = R;
            R = Color.nameToHex[R];
         } else if (Color.hexToName[R]) {
            this.name = Color.hexToName[R];
         } else if (R.startsWith("#"))
            R = R.substring(1);
         value = parseInt(R, 16);
      }
   } else
      value = R * Math.pow(2, 16) + G * Math.pow(2, 8) + B;

   if (value == null || isNaN(value))
      throw("Cannot create Color from " + R);
   return this;
}


/**
 * transform a hsl representation of a color
 * to the equivalent decimal value
 * @param Integer the hue fraction
 * @param Integer the saturation fraction
 * @param Integer the lightness fraction
 * @return Object the resulting color object
 *
 * note: this function is adapted from the 
 * HSLtoRGB conversion method as described at
 * http://www1.tip.nl/~t876506/ColorDesign.html#hr
 * (thanks!)
 */
Color.fromHsl = function(H,S,L) {
   function H1(H,S,L) {
      var R = 1; var G = 6*H; var B = 0;
      G = G*S + 1 - S; B = B*S + 1 - S;
      R = R*L; G = G*L; B = B*L;
      return [R,G,B];
   }
   
   function H2(H,S,L) {
      var R = 1-6*(H - 1/6); var G = 1; var B = 0;
      R = R*S + 1 - S; B = B*S + 1 - S;
      R = R*L; G = G*L; B = B*L;
      return [R,G,B];
   }
   
   function H3(H,S,L) {
      var R = 0; var G = 1; var B = 6*(H - 1/3);
      R = R*S + 1 - S; B = B*S + 1 - S;
      R = R*L; G = G*L; B = B*L
      return [R,G,B];
   }
   
   function H4(H,S,L) {
      var R = 0; var G = 1-6*(H - 1/2); var B = 1;
      R = R*S + 1 - S; G = G*S + 1 - S;
      R = R*L; G = G*L; B = B*L;
      return [R,G,B];
   }
   
   function H5(H,S,L) {
      var R = 6*(H - 2/3); var G = 0; var B = 1;
      R = R*S + 1 - S; G = G*S + 1 - S;
      R = R*L; G = G*L; B = B*L;
      return [R,G,B];
   }
   
   function H6(H,S,L) {
      var R = 1; var G = 0; var B = 1-6*(H - 5/6);
      G = G*S + 1 - S; B = B*S + 1 - S;
      R = R*L; G = G*L; B = B*L;
      return [R,G,B];
   }
   
   // H  [0-1] is divided into 6 equal sectors.
   // From within each sector the proper conversion function is called.
   var rgb;
   if (H < 1/6)      rgb = H1(H,S,L);
   else if (H < 1/3) rgb = H2(H,S,L);
   else if (H < 1/2) rgb = H3(H,S,L);
   else if (H < 2/3) rgb = H4(H,S,L);
   else if (H < 5/6) rgb = H5(H,S,L);
   else              rgb = H6(H,S,L);

   return new Color(
      Math.round(rgb[0]*255),
      Math.round(rgb[1]*255),
      Math.round(rgb[2]*255)
   );
}

/**
 * this dumps out the values to be used
 * in antville's colorpicker
 * @param Integer the square size of the colorpicker
 */
Color.dumpColorPicker = function(size) {
   res.contentType = "text/plain";
   var brightness = new Array();
   if (size == null)
      size = 12;
   var step = 1/(size/3)/3;
   var i = 0;
   res.write("this.brightness = [");
   for (var L=.0; L<=1.0; L+=step) {
      brightness[i] = new Array();
      res.write("\n   [");
      for (var H=.0; H<=1.0; H+=step) {
         for (var S=.0; S<=1.0; S+=step) {
            var color = Color.hslToRgb(H,S,L);
            var hexColor = color.toString();
            brightness[i][brightness[i].length] = hexColor;
            res.write('"'+hexColor+'"');
            if (S<1.0)
               res.write(",");
         }
        if (H<1.0)
           res.write(",");
      }
      res.write("]");
      if (i<size)
         res.write(",");
      i++;
   }
   res.write("\n];");
   return;
}

/**
 * object containig the hex values of named colors
 */
Color.nameToHex = {
   black: "000000",
   maroon: "800000",
   green: "008000",
   olive: "808000",
   navy: "000080",
   purple: "800080",
   teal: "008080",
   silver: "c0c0c0",
   gray: "808080",
   red: "ff0000",
   lime: "00ff00",
   yellow: "ffff00",
   blue: "0000ff",
   fuchsia: "ff00ff",
   aqua: "00ffff",
   white: "ffffff",
   aliceblue: "f0f8ff",
   antiquewhite: "faebd7",
   aquamarine: "7fffd4",
   azure: "f0ffff",
   beige: "f5f5dc",
   blueviolet: "8a2be2",
   brown: "a52a2a",
   burlywood: "deb887",
   cadetblue: "5f9ea0",
   chartreuse: "7fff00",
   chocolate: "d2691e",
   coral: "ff7f50",
   cornflowerblue: "6495ed",
   cornsilk: "fff8dc",
   crimson: "dc143c",
   darkblue: "00008b",
   darkcyan: "008b8b",
   darkgoldenrod: "b8860b",
   darkgray: "a9a9a9",
   darkgreen: "006400",
   darkkhaki: "bdb76b",
   darkmagenta: "8b008b",
   darkolivegreen: "556b2f",
   darkorange: "ff8c00",
   darkorchid: "9932cc",
   darkred: "8b0000",
   darksalmon: "e9967a",
   darkseagreen: "8fbc8f",
   darkslateblue: "483d8b",
   darkslategray: "2f4f4f",
   darkturquoise: "00ced1",
   darkviolet: "9400d3",
   deeppink: "ff1493",
   deepskyblue: "00bfff",
   dimgray: "696969",
   dodgerblue: "1e90ff",
   firebrick: "b22222",
   floralwhite: "fffaf0",
   forestgreen: "228b22",
   gainsboro: "dcdcdc",
   ghostwhite: "f8f8ff",
   gold: "ffd700",
   goldenrod: "daa520",
   greenyellow: "adff2f",
   honeydew: "f0fff0",
   hotpink: "ff69b4",
   indianred: "cd5c5c",
   indigo: "4b0082",
   ivory: "fffff0",
   khaki: "f0e68c",
   lavender: "e6e6fa",
   lavenderblush: "fff0f5",
   lawngreen: "7cfc00",
   lemonchiffon: "fffacd",
   lightblue: "add8e6",
   lightcoral: "f08080",
   lightcyan: "e0ffff",
   lightgoldenrodyellow: "fafad2",
   lightgreen: "90ee90",
   lightgrey: "d3d3d3",
   lightpink: "ffb6c1",
   lightsalmon: "ffa07a",
   lightseagreen: "20b2aa",
   lightskyblue: "87cefa",
   lightslategray: "778899",
   lightsteelblue: "b0c4de",
   lightyellow: "ffffe0",
   limegreen: "32cd32",
   linen: "faf0e6",
   mediumaquamarine: "66cdaa",
   mediumblue: "0000cd",
   mediumorchid: "ba55d3",
   mediumpurple: "9370db",
   mediumseagreen: "3cb371",
   mediumslateblue: "7b68ee",
   mediumspringgreen: "00fa9a",
   mediumturquoise: "48d1cc",
   mediumvioletred: "c71585",
   midnightblue: "191970",
   mintcream: "f5fffa",
   mistyrose: "ffe4e1",
   moccasin: "ffe4b5",
   navajowhite: "ffdead",
   oldlace: "fdf5e6",
   olivedrab: "6b8e23",
   orange: "ffa500",
   orangered: "ff4500",
   orchid: "da70d6",
   palegoldenrod: "eee8aa",
   palegreen: "98fb98",
   paleturquoise: "afeeee",
   palevioletred: "db7093",
   papayawhip: "ffefd5",
   peachpuff: "ffdab9",
   peru: "cd853f",
   pink: "ffc0cb",
   plum: "dda0dd",
   powderblue: "b0e0e6",
   rosybrown: "bc8f8f",
   royalblue: "4169e1",
   saddlebrown: "8b4513",
   salmon: "fa8072",
   sandybrown: "f4a460",
   seagreen: "2e8b57",
   seashell: "fff5ee",
   sienna: "a0522d",
   skyblue: "87ceeb",
   slateblue: "6a5acd",
   slategray: "708090",
   snow: "fffafa",
   springgreen: "00ff7f",
   steelblue: "4682b4",
   tan: "d2b48c",
   thistle: "d8bfd8",
   tomato: "ff6347",
   turquoise: "40e0d0",
   violet: "ee82ee",
   wheat: "f5deb3",
   whitesmoke: "f5f5f5",
   yellowgreen: "9acd32"
}

/**
 * object containig the color names for specific hex values
 */
Color.hexToName = {
   "000000": "black",
   "800000": "maroon",
   "008000": "green",
   "808000": "olive",
   "000080": "navy",
   "800080": "purple",
   "008080": "teal",
   "c0c0c0": "silver",
   "808080": "gray",
   "ff0000": "red",
   "00ff00": "lime",
   "ffff00": "yellow",
   "0000ff": "blue",
   "ff00ff": "fuchsia",
   "00ffff": "aqua",
   "ffffff": "white",
   "f0f8ff": "aliceblue",
   "faebd7": "antiquewhite",
   "7fffd4": "aquamarine",
   "f0ffff": "azure",
   "f5f5dc": "beige",
   "8a2be2": "blueviolet",
   "a52a2a": "brown",
   "deb887": "burlywood",
   "5f9ea0": "cadetblue",
   "7fff00": "chartreuse",
   "d2691e": "chocolate",
   "ff7f50": "coral",
   "6495ed": "cornflowerblue",
   "fff8dc": "cornsilk",
   "dc143c": "crimson",
   "00008b": "darkblue",
   "008b8b": "darkcyan",
   "b8860b": "darkgoldenrod",
   "a9a9a9": "darkgray",
   "006400": "darkgreen",
   "bdb76b": "darkkhaki",
   "8b008b": "darkmagenta",
   "556b2f": "darkolivegreen",
   "ff8c00": "darkorange",
   "9932cc": "darkorchid",
   "8b0000": "darkred",
   "e9967a": "darksalmon",
   "8fbc8f": "darkseagreen",
   "483d8b": "darkslateblue",
   "2f4f4f": "darkslategray",
   "00ced1": "darkturquoise",
   "9400d3": "darkviolet",
   "ff1493": "deeppink",
   "00bfff": "deepskyblue",
   "696969": "dimgray",
   "1e90ff": "dodgerblue",
   "b22222": "firebrick",
   "fffaf0": "floralwhite",
   "228b22": "forestgreen",
   "dcdcdc": "gainsboro",
   "f8f8ff": "ghostwhite",
   "ffd700": "gold",
   "daa520": "goldenrod",
   "adff2f": "greenyellow",
   "f0fff0": "honeydew",
   "ff69b4": "hotpink",
   "cd5c5c": "indianred",
   "4b0082": "indigo",
   "fffff0": "ivory",
   "f0e68c": "khaki",
   "e6e6fa": "lavender",
   "fff0f5": "lavenderblush",
   "7cfc00": "lawngreen",
   "fffacd": "lemonchiffon",
   "add8e6": "lightblue",
   "f08080": "lightcoral",
   "e0ffff": "lightcyan",
   "fafad2": "lightgoldenrodyellow",
   "90ee90": "lightgreen",
   "d3d3d3": "lightgrey",
   "ffb6c1": "lightpink",
   "ffa07a": "lightsalmon",
   "20b2aa": "lightseagreen",
   "87cefa": "lightskyblue",
   "778899": "lightslategray",
   "b0c4de": "lightsteelblue",
   "ffffe0": "lightyellow",
   "32cd32": "limegreen",
   "faf0e6": "linen",
   "66cdaa": "mediumaquamarine",
   "0000cd": "mediumblue",
   "ba55d3": "mediumorchid",
   "9370db": "mediumpurple",
   "3cb371": "mediumseagreen",
   "7b68ee": "mediumslateblue",
   "00fa9a": "mediumspringgreen",
   "48d1cc": "mediumturquoise",
   "c71585": "mediumvioletred",
   "191970": "midnightblue",
   "f5fffa": "mintcream",
   "ffe4e1": "mistyrose",
   "ffe4b5": "moccasin",
   "ffdead": "navajowhite",
   "fdf5e6": "oldlace",
   "6b8e23": "olivedrab",
   "ffa500": "orange",
   "ff4500": "orangered",
   "da70d6": "orchid",
   "eee8aa": "palegoldenrod",
   "98fb98": "palegreen",
   "afeeee": "paleturquoise",
   "db7093": "palevioletred",
   "ffefd5": "papayawhip",
   "ffdab9": "peachpuff",
   "cd853f": "peru",
   "ffc0cb": "pink",
   "dda0dd": "plum",
   "b0e0e6": "powderblue",
   "bc8f8f": "rosybrown",
   "4169e1": "royalblue",
   "8b4513": "saddlebrown",
   "fa8072": "salmon",
   "f4a460": "sandybrown",
   "2e8b57": "seagreen",
   "fff5ee": "seashell",
   "a0522d": "sienna",
   "87ceeb": "skyblue",
   "6a5acd": "slateblue",
   "708090": "slategray",
   "fffafa": "snow",
   "00ff7f": "springgreen",
   "4682b4": "steelblue",
   "d2b48c": "tan",
   "d8bfd8": "thistle",
   "ff6347": "tomato",
   "40e0d0": "turquoise",
   "ee82ee": "violet",
   "f5deb3": "wheat",
   "f5f5f5": "whitesmoke",
   "9acd32": "yellowgreen"
}
