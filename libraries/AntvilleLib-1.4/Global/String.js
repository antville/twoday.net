String.ANUMPATTERN     = /[^a-zA-Z0-9]/;
String.APATTERN        = /[^a-zA-Z]/;
String.NUMPATTERN      = /[^0-9]/;
String.URLPATTERN      = /[^a-zA-Z0-9$-_\.\+\!\*\'()\,#%~\\ ]/;
String.FILEPATTERN     = /[^a-zA-Z0-9-_\. ]/;
String.HEXPATTERN      = /[^a-fA-F0-9]/;
String.LOCALPATTERN_UNDERSCORE =
   /(^[a-z]{2}$)|(^([a-z]{2})_([A-Z]{2})$)|(^([a-z]{2})_([A-Z]{2})_([a-zA-Z][a-zA-Z0-9_]*)$)/;
String.LOCALPATTERN_HYPHEN =
   /(^[a-z]{2}$)|(^([a-z]{2})-([A-Z]{2})$)|(^([a-z]{2})-([A-Z]{2})-([a-zA-Z][a-zA-Z0-9_]*)$)/;
String.LEFT            = -1
String.BALANCE         = 0
String.RIGHT           = 1
String.ISOFORMAT       = "yyyy-MM-dd'T'HH:mm:ssZ";


/**
 * create a string from a bunch of substrings
 * @param String one or more strings as arguments
 * @return String the resulting string
 */
String.compose = function() {
   res.push();
   for (var i=0; i<arguments.length; i++)
      res.write(arguments[i]);
   return res.pop();
}


/**
 * creates a random string (numbers and chars)
 * @param len length of key
 * @param mode determines which letters to use. null or 0 = all letters;
 *     1 = skip 0, 1, l and o which can easily be mixed with numbers;
 *     2 = use numbers only
 * @returns random string
 */
String.random = function(len, mode) {
   if (mode == 2) {
      var x = Math.random() * Math.pow(10,len);
      return Math.floor(x);
   }
   var keystr = "";
   for (var i=0; i<len; i++) {
      var x = Math.floor((Math.random() * 36));
      if (mode == 1) {
         // skip 0,1
         x = (x<2) ? x + 2 : x;
         // don't use the letters l (charCode 21+87) and o (24+87)
         x = (x==21) ? 22 : x;
         x = (x==24) ? 25 : x;
      }
      if (x<10) {
         keystr += String(x);
      }   else   {
         keystr += String.fromCharCode(x+87);
      }
   }
   return keystr;
}

/**
 * append one string onto another and add some "glue"
 * if none of the strings is empty or null.
 * @param String the first string
 * @param String the string to be appended onto the first one
 * @param String the "glue" to be inserted between both strings
 * @return String the resulting string
 */
String.join = function(str1, str2, glue) {
   if (glue == null)
      glue = "";
   if (str1 && str2)
      return str1 + glue + str2;
   else if (str2)
      return str2;
   return str1;
}

/**
 * function checks if the string passed contains any
 * non-alphanumeric characters
 * @return Boolean
 */
String.prototype.isClean = function() {
   return !String.ANUMPATTERN.test(this);
}

/**
 * function cleans a string by throwing away all
 * non-alphanumeric characters
 * @return cleaned string
 */
String.prototype.clean = function() {
   return this.replace(new RegExp(String.ANUMPATTERN.source, "g"), "");
}

/**
 * checks if a date format pattern is correct
 * @return Boolean true if the pattern is correct
 */
String.prototype.isDateFormat = function() {
   try {
      new java.text.SimpleDateFormat(this);
      return true;
   } catch (err) {
      return false;
   }
}

/**
 * parse a timestamp into a date object. This is used when users
 * want to set createtime explicitly when creating/editing stories.
 * @param String date format to be applied
 * @param Object Java TimeZone Object (optional)
 * @return Object contains the resulting date
 */
String.prototype.toDate = function(format, timezone) {
   var sdf = res.data._dateformat;
   if (!sdf) {
      sdf = new java.text.SimpleDateFormat(format);
      res.data._dateformat = sdf;
   } else if (format != sdf.toPattern())
      sdf.applyPattern(format);
   if (timezone && timezone != sdf.getTimeZone())
      sdf.setTimeZone(timezone);
   try {
      return new Date(sdf.parse(this).getTime());
   } catch (err) {
      return null;
   }
}

/**
 * function checks if the string passed contains any characters that
 * are forbidden in URLs and tries to create a java.net.URL from it
 * @return Boolean
 */
String.prototype.isURL = function() {
   if (String.URLPATTERN.test(this))
      return false;
   try {
      new java.net.URL(this);
   } catch (err) {
      return false;
   }
   return true;
}

/**
 * function cleans a string passed as argument from any
 * characters that are forbidden in URLs and removes any
 * trailing slash.
 * @return cleaned string
 */
String.prototype.toURL = function () {
   var url = this.replace(new RegExp(String.URLPATTERN.source, "g"), "");
   while (url.endsWith("/"))
      url = url.substring(0, url.length-1);
   return url;
}

/**
 * function checks if the string passed contains any characters
 * that are forbidden in image- or filenames
 * @return Boolean
 */
String.prototype.isFileName = function() {
   return !String.FILEPATTERN.test(this);
}

/**
 * function cleans the string passed as argument from any characters
 * that are forbidden or shouldn't be used in filenames
 * @return Boolean
 */
String.prototype.toFileName = function() {
   var str = this;
   str = str.replace(new RegExp("[" + String.fromCharCode(252) + "]", "g"), "ue");
   str = str.replace(new RegExp("[" + String.fromCharCode(228) + "]", "g"), "ae");
   str = str.replace(new RegExp("[" + String.fromCharCode(246) + "]", "g"), "oe");
   str = str.replace(new RegExp("[" + String.fromCharCode(220) + "]", "g"), "Ue");
   str = str.replace(new RegExp("[" + String.fromCharCode(196) + "]", "g"), "Ae");
   str = str.replace(new RegExp("[" + String.fromCharCode(214) + "]", "g"), "Oe");
   str = str.replace(new RegExp("[" + String.fromCharCode(223) + "]", "g"), "ss");
   str = str.replace(/[^a-zA-Z0-9_]/g, "-");
   str = str.replace(/-+/g, "-");
   return str;
}

/**
 * function checks a string for a valid color value in hexadecimal format.
 * it may also contain # as first character
 *  @returns Boolean false, if string length (without #) > 6 or < 6 or
 *           contains any character which is not a valid hex value
 */
String.prototype.isHexColor = function() {
   var str = this;
   if (this.indexOf("#") == 0)
      str = this.substring(1);
   if (str.length != 6)
      return false;
   return !String.HEXPATTERN.test(str);
}

/**
 * converts a string into a hexadecimal color
 * representation (e.g. "ffcc33"). also knows how to
 * convert a color string like "rgb (255, 204, 51)".
 * @return String the resulting hex color (w/o "#")
 */
String.prototype.toHexColor = function() {
   if (this.startsWith("rgb")) {
      res.push();
      var col = this.replace(/[^0-9,]/g,"");
      var parts = col.split(",");
      for (var i in parts) {
         var num = parseInt(parts[i], 10);
         var hex = num.toString(16);
         res.write(hex.pad("0", 2, String.LEFT));
      }
      return res.pop();
   }
   var col = this.replace(new RegExp(String.HEXPATTERN.source), "");
   return col.toLowerCase().pad("0", 6, String.LEFT);
}

/**
 * function returns true if the string contains
 * only a-z and 0-9 (case insensitive!)
 * @return Boolean true in case string is alpha, false otherwise
 */
String.prototype.isAlphanumeric = function() {
   if (!this.length)
      return false;
   return !String.ANUMPATTERN.test(this);
}

/**
 * function returns true if the string contains
 * only characters a-z
 * @return Boolean true in case string is alpha, false otherwise
 */
String.prototype.isAlpha = function() {
   if (!this.length)
      return false;
   return !String.APATTERN.test(this);
}

/**
 * function returns true if the string contains
 * only 0-9
 * @return Boolean true in case string is numeric, false otherwise
 */
String.prototype.isNumeric = function() {
   if (!this.length)
      return false;
   return !String.NUMPATTERN.test(this);
}

/**
 * function parses a string and converts any
 * found URL into a HTML link tag
 * @return String resulting string with activated URLs
 */
String.prototype.activateURLs = function() {
   var pre = '<a href="';
   var mid = '">';
   var post = '</a>';
   var re = /(^|\/>|\s+)([fhtpsr]+:\/\/[^\s]+?)([\.,;:\)\]\"]?)(?=[\s<]|$)/gim;
   return this.replace(re, "$1" + pre + "$2" + mid + "$2" + post + "$3");
}

/**
 * transforms the first n characters of a string to uppercase
 * @param Number amount of characters to transform
 * @return String the resulting string
 */
String.prototype.capitalize = function(limit) {
   if (limit == null)
      limit = 1;
   var head = this.substring(0, limit);
   var tail = this.substring(limit, this.length);
   return head.toUpperCase() + tail.toLowerCase();
}

/**
 * transforms the first n characters of each
 * word in a string to uppercase
 * @return String the resulting string
 */
String.prototype.titleize = function() {
   var parts = this.split(" ");
   res.push();
   for (var i in parts) {
      res.write(parts[i].capitalize());
      if (i < parts.length-1)
         res.write(" ");
   }
   return res.pop();
}

/**
 * translates all characters of a string into HTML entities
 * @return String translated result
 */
String.prototype.entitize = function() {
   res.push();
   for (var i=0; i<this.length; i++) {
      res.write("&#");
      res.write(this.charCodeAt(i).toString());
      res.write(";");
   }
   return res.pop();
}

/**
 * breaks up a string into two parts called
 * head and tail at the given position
 * @param Number desired length of string
 * @param String pre-/suffix to be pre-/appended to shortened string
 * @param Boolean definitely cut after limit (ignore whitespace)
 * @return Object containing head and tail properties
 */
String.prototype.embody = function(limit, clipping, ignoreWhiteSpace) {
   var str = this;
   var result = {head: str, tail: ""};
   if (!limit)
      return result;
   result.head = str = stripTags(str);
   if (str.length <= limit)
      return result;
   var cut = (ignoreWhiteSpace == true) ? limit : str.indexOf(" ", limit);
   if (cut < 0)
      return result;
   result.head = str.substring(0, cut).trim();
   if (result.head) {
      result.tail = str.substring(cut).trim();
      if (result.tail) {
         if (clipping == null)
            clipping = "...";
         result.head += clipping;
         result.tail = clipping + result.tail;
      }
   }
   return result;
}

/**
 * function retuns only a part of the string passed as argument
 * length of the string to show is defined by argument "limit"
 * clipping is always done at the first space <em>after</em> the
 * limit
 * @see String.prototype.embody()
 */
String.prototype.clip = function(limit, clipping, ignoreWhiteSpace) {
   return this.embody(limit, clipping, ignoreWhiteSpace).head;
}

/**
 * get the head of a string
 * @see String.prototype.embody()
 */
String.prototype.head = function(limit, clipping, ignoreWhiteSpace) {
   return this.embody(limit, clipping, ignoreWhiteSpace).head;
}

/**
 * get the tail of a string
 * @see String.prototype.embody()
 */
String.prototype.tail = function(limit, clipping, ignoreWhiteSpace) {
   return this.embody(limit, clipping, ignoreWhiteSpace).tail;
}

/**
 * function inserts a string every number of characters
 * @param Int number of characters after which insertion should take place
 * @param String string to be inserted
 * @param Boolean definitely insert at each interval position
 * @return String resulting string
 */
String.prototype.group = function(interval, str, ignoreWhiteSpace) {
   if (!str || this.length < interval)
      return this;
   res.push();
   for (var i=0; i<this.length; i=i+interval) {
      var strPart = this.substring(i, i+interval);
      res.write(strPart);
      if (ignoreWhiteSpace == true || (strPart.length == interval && strPart.indexOf(" ") < 0))
         res.write(str);
   }
   return res.pop();
}

/**
 * insert a <br /> every number of chars of a string
 * (ie. hardwrap the string at the appropriate position)
 * @param Number number of chars after wich wrapping should occur
 * @return String the wrapped string
 */
String.prototype.hardwrap = function(interval) {
   return this.group(interval, "<br />", true);
}

/**
 * Softwraps a string every number of characters.
 * (ie. inserts &lt;span class=\"width:0;font-size:1px;\"%gt; &lt;/span%gt;.
 *  at the appropriate position)
 * Note: the &lt;wbr%gt; tag is not standards complient, so we use this
 *       not so perfect, but working trick.
 * @param Int number of characters after which wrapping should occur
 * @param Boolean definitely wrap after each interval (ignore whitespace)
 * @return String resulting (wrapped) string
 */
String.prototype.softwrap = function(interval, ignoreWhiteSpace) {
   res.push();
   var cl=0;
   var wrapped = false;
   for (var i=0; i<this.length; i++) {
      if (cl > interval) {
         res.write("<span style=\"width:0;font-size:1px;\"> </span>");
         cl = 0;
         wrapped = true;
      }
      var c = this.substring(i, i+1);
      // check for &entities;
      if (c == "&") {
         var result = this.substring(i).match(/(^\&((#x?[0-9]+)|([a-z]+));)/);
         if (result) {
            i += result[0].length - 1;
            cl++;
            res.write(result[0]);
            continue;
         }
      }
      res.write(c);
      if (ignoreWhiteSpace == true || (c != " ")) {
         cl++;
      } else {
         cl = 0;
      }
   }
   var r = res.pop();
   return r;
}

/**
 * replace all linebreaks and optionally all w/br tags
 * @param Boolean flag indicating if html tags should be replaced
 * @param String replacement for the linebreaks / html tags
 * @return String the unwrapped string
 */
String.prototype.unwrap = function(removeTags, replacement) {
   if (replacement == null)
      replacement = "";
   var str = this.replace(/[\n|\r]/g, replacement);
   if (removeTags)
      str = str.replace(/<[w]?br *\/?>/g, replacement);
   return str;
}

/**
 * function calculates the md5 hash of a string
 * @return String md5 hash of the string
 */
String.prototype.md5 = function() {
   return Packages.helma.util.MD5Encoder.encode(this);
}

/**
 * function repeats a string passed as argument
 * @param Int amount of repetitions
 * @return String resulting string
 */
String.prototype.repeat = function(multiplier) {
   res.push();
   for (var i=0; i<multiplier; i++)
      res.write(this);
   return res.pop();
}

/**
 * function returns true if the string starts with
 * the string passed as argument
 * @param String string pattern to search for
 * @return Boolean true in case it matches the beginning
 *         of the string, false otherwise
 */
String.prototype.startsWith = function(str, offset) {
   var javaObj = new java.lang.String(this);
   if (offset != null)
      return javaObj.startsWith(str, offset);
   return javaObj.startsWith(str);
}

/**
 * function returns true if the string ends with
 * the string passed as argument
 * @param String string pattern to search for
 * @return Boolean true in case it matches the end of
 *         the string, false otherwise
 */
String.prototype.endsWith = function(str) {
   var javaObj = new java.lang.String(this);
   return javaObj.endsWith(str);
}

/**
 * fills a string with another string up to a desired length
 * @param String the filling string
 * @param Number the desired length of the resulting string
 * @param Number the direction which the string will be padded in:
 *               -1: left   0: both (balance)  1: right
 *               (you can use the constants String.LEFT,
 *                String.BALANCE and String.RIGHT here as well.)
 * @return String the resulting string
 */
String.prototype.pad = function(str, len, mode) {
   if (str  == null || len == null)
      return this;
   var diff = len - this.length;
   if (diff == 0)
      return this;
   var left, right = 0;
   if (mode == null || mode == String.RIGHT)
      right = diff;
   else if (mode == String.LEFT)
      left = diff;
   else if (mode == String.BALANCE) {
      right = Math.round(diff / 2);
      left = diff - right;
   }
   res.push();
   for (var i=0; i<left; i++)
      res.write(str);
   res.write(this);
   for (var i=0; i<right; i++)
      res.write(str);
   return res.pop();
}

/**
 * function returns true if a string contains the string
 * passed as argument
 * @param String string to search for
 * @param Int Position to start search
 * @param Boolean
 */
String.prototype.contains = function(str, fromIndex) {
   if (this.indexOf(str, fromIndex ? fromIndex : 0) > -1)
      return true;
   return false;
}

/**
 * function compares a string with the one passed as argument
 * using diff
 * @param String String to compare against String object value
 * @param String Optional regular expression string to use for
 *             splitting. If not defined, newlines will be used.
 * @return Object Array containing one JS object for each line
 *                with the following properties:
 *                .num Line number
 *                .value String line if unchanged
 *                .deleted Obj Array containing deleted lines
 *                .inserted Obj Array containing added lines
 */
String.prototype.diff = function(mod, separator) {
   // if no separator use line separator
   var regexp = (typeof(separator) == "undefined") ?
      new RegExp("\r\n|\r|\n") :
      new RegExp(separator);
   // split both strings into arrays
   var orig = this.split(regexp);
   var mod = mod.split(regexp);
   // create the Diff object
   var diff = new Packages.helma.util.Diff(orig, mod);
   // get the diff.
   var d = diff.diff();
   if (!d)
      return null;

   var max = Math.max(orig.length, mod.length);
   var result = new Array();
   for (var i=0;i<max;i++) {
      var line = result[i];
      if (!line) {
         line = new Object();
         line.num = (i+1);
         result[i] = line;
      }
      if (d && i == d.line1) {
         if (d.deleted) {
            var del = new Array();
            for (var j=d.line0; j<d.line0+d.deleted; j++)
               del[del.length] = orig[j];
            line.deleted = del;
         }
         if (d.inserted) {
            var ins = new Array();
            for (var j=d.line1; j<d.line1+d.inserted; j++)
               ins[ins.length] = mod[j];
            line.inserted = ins;
         }
         i = d.line1 + d.inserted -1;
         d = d.link;
      } else {
         line.value = mod[i];
      }
   }
   return result;
}

/**
 * remove leading and trailing whitespace
 */
String.prototype.trim = function () {
   var s = new java.lang.String(this);
   return String(s.trim());
}

/**
 * wrapper methods for string-related
 * global helma functions
 */
String.prototype.encode = function() {
   return encode(this);
}
String.prototype.encodeXml = function() {
   return encodeXml(this);
}
String.prototype.encodeForm = function() {
   return encodeForm(this);
}
String.prototype.format = function() {
   return format(this);
}
String.prototype.stripTags = function() {
   return stripTags(this);
}


/**
 * Returns true if the string is a valid locale string.
 * @param delimiter   accepts only "-"
 * @return Boolean
 */
String.prototype.isLocaleFormat = function(delimiter) {
   if (delimiter == "-") {
      REGEXP_LOCAL = String.LOCALPATTERN_HYPHEN;
   } else {
      REGEXP_LOCAL = String.LOCALPATTERN_UNDERSCORE;
   }
   var match = this.match(REGEXP_LOCAL);
   if (match == null) return false;
   var language = (match[6] || match[3] || match[1]);
   var country = (match[7] || match[4]);
   var variant = (match[8]);
   try {
      if (variant) {
         var locale = new java.util.Locale(language, country, variant);
      } else if (country) {
         var locale = new java.util.Locale(language, country);
      } else {
         var locale = new java.util.Locale(language);
      }
      if (locale) return true;
   }
   catch (err) {
      return false;
   }
}


/**
 * converts a valid locale string to a Locale.
 * this function may return null, if the string is not valid.
 * @param delimiter   accepts only "-"
 * @return java.util.Locale
 */
String.prototype.toLocale = function(delimiter) {
   if (delimiter == "-") {
      REGEXP_LOCAL = String.LOCALPATTERN_HYPHEN;
   } else {
      REGEXP_LOCAL = String.LOCALPATTERN_UNDERSCORE;
   }
   var match = this.match(REGEXP_LOCAL);
   if (match == null) return null;
   var language = (match[6] || match[3] || match[1]);
   var country = (match[7] || match[4]);
   var variant = (match[8]);
   try {
      if (variant) {
         var locale = new java.util.Locale(language, country, variant);
      } else if (country) {
         var locale = new java.util.Locale(language, country);
      } else {
         var locale = new java.util.Locale(language);
      }
      return locale;
   }
   catch (err) {
      return null;
   }
}
