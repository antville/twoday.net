Http = {
   toString: function() {
      return "[HttpLibrary Object]";
   },

   getVersion: function() {
      return this.version;
   },

   /**
    * set a proxy
    */
   setProxy: function(host, port) {
      var sys = java.lang.System.getProperties();
      if (host) {
         if (!port)
            port = "8080";
         else if (typeof port == "number")
            port = port.toString();
         app.log("Setting proxy " + host + ":" + port);
         sys.put("http.proxySet", "true");
         sys.put("http.proxyHost", host);
         sys.put("http.proxyPort", port);
      } else {
         sys.put("http.proxySet", "false");
         sys.put("http.proxyHost", "");
         sys.put("http.prodyPort", "");
      }
      return true;
   },

   /**
    * returns host and port if a proxy is set, false otherwise
    */
   getProxy: function() {
      var sys = java.lang.System.getProperties();
      if (sys.get("http.proxySet") == "true")
         return sys.get("http.proxyHost") + ":" + sys.get("http.proxyPort");
      return false;
   },

   /**
    * retrieves a url via http head request
    * @param String the url or uri
    * @return Object the result containing a proper url, 
    *                the https response code and message
    */
   ping: function(url) {
      if (typeof url == "string") {
         var url = Http.evalUrl(url);
         if (!url)
            return;
      }
      var conn = url.openConnection();
      conn.setRequestMethod("HEAD");
      var result = {
         url: conn.getURL(),
         code: conn.getResponseCode(),
         message: conn.getResponseMessage()
      };
      conn.disconnect();
      return result;
   },

   /**
    * removes trailing slash from and evaluates a url
    * @param String the url or uri string
    * @return Object the result with error and result properties
    */
   evalUrl: function(url) {
      while (url.endsWith("/"))
         url = url.substring(0, url.length - 1);
      try {
         return new java.net.URL(url);
      } catch (err) {
         return null;
      }
   }
}




