/**
 * write out a property contained in app.properties
 * @param Object containing the name of the property
 */
function property_macro(param) {
   if (app.properties[param.name])
      res.write(app.properties[param.name]);
   return;
}


/**
 * wrapper to output a string from within a skin
 * just to be able to use different encodings
 * @param Object containing the string as text property
 */
function write_macro(param) {
   if (!param.text)
      return;
   res.write(param.text);
   return;
}


/**
 * renders the current datetime
 * @param Object containing a formatting string as format property
 */
function now_macro(param) {
   var d = new Date();
   if (param.format)
      res.write(d.format(param.format));
   else
      res.write(d);
   return;
}


/**
 * renders the infamous invisible GIF
 * directly from within the library
 */
function spacer_macro(param) {
   if (param.width == null)
      param.width = "2";
   if (param.height == null)
      param.height = "2";
   if (param.border == null)
      param.border = "0";
   param.src = root.href("pixel.gif");
   param.alt = "";
   Html.tag("img", param);
   return;
}


/**
 * renders a global skin
 */
function skin_macro(param) {
   if (param.name)
      renderSkin(param.name);
   return;
}
