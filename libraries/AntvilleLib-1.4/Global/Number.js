/**
 * format a Number to a String
 * @param String Format pattern
 * @return String Number formatted to a String
 * FIXME: this might need some localisation
 */
Number.prototype.format = function(fmt) {
   var df = fmt ? new java.text.DecimalFormat(fmt)
            : new java.text.DecimalFormat("#,##0.00");
   return df.format(0+this);
}

/** 
 * return the percentage of a Number
 * according to a given total Number
 * @param Int Total
 * @param String Format Pattern
 * @return Int Percentage
 */
Number.prototype.toPercent = function(total, fmt) {
	var p = this / (total / 100);
   if (!fmt)
      return Math.round(p * 100) / 100;
   return p.format(fmt);
}