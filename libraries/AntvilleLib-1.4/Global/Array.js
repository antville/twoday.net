/**
 * return the first index position of a value 
 * contained in an array
 * @param Object Array to use for checking
 * @param String|Object the String or Object to check
 */
Array.indexOf = function(arr, val) {
   var i = -1;
   while (i++ < arr.length -1) {
      if (arr[i] == val)
         return i;
   }
   return -1;
}


/**
 * return the last index position of a value 
 * contained in an array
 * @param Object Array to use for checking
 * @param String|Object the String or Object to check
 */
Array.lastIndexOf = function(arr, val) {
   var i = 1;
   while (arr.length - i++ >= 0) {
      if (arr[i] == val)
         return i;
   }
   return -1;
}


/**
 * check if an array passed as argument contains
 * a specific value (start from end of array)
 * @param Object Array to use for checking
 * @param String|Object the String or Object to check
 */
Array.contains = function(arr, val) {
   if (Array.indexOf(arr, val) > -1)
      return true;
   return false;
}

