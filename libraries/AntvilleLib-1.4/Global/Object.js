/**
 * copy the properties of an object into
 * a new object
 * @param Object the source object
 * @param Object the (optional) target object
 * @return Object the resulting object
 */
Object.clone = function(obj, clone) {
   if (!clone)
      var clone = {};
   for (var p in obj)
      clone[p] = obj[p];
   return clone;
}


/**
 * reduce an extended object (ie. a HopObject)
 * to a generic javascript object
 * @param HopObject the HopObject to be reduced
 * @return Object the resulting generic object
 */
Object.reduce = function(hopObj, recursive) {
   var result = {};
   for (var i in hopObj) {
      if (!hopObj[i])
         continue;
      if (hopObj[i] instanceof HopObject == false)
         result[i] = hopObj[i];
      else if (recursive)
         result[i] = Object.reduce(hopObj[i]);
   }
   return result;
}


/**
 * print the contents of an object for debugging
 * @param Object the object to dump
 * @param Boolean recursive flag (if true, dump child objects, too)
 */
Object.dump = function(obj, recursive) {
   for (var p in obj) {
      Html.element("strong", p);
      Html.tag("br");
      Html.openTag("blockquote");
      if (recursive && typeof obj[p] == "object") {
         var recurse = true;
         var types = [Function, Date, String, Number];
         for (var i in types) {
            if (obj[p] instanceof types[i]) {
               recurse = false
               break;
            }
         }
         if (obj[p]) {
            if (recurse == true)
               Object.dump(obj[p], true);
            else
               Html.element("pre", encode(obj[p].toString()));
         }
      } else if (obj[p])
         Html.element("pre", encode(obj[p].toString()));
      Html.closeTag("blockquote");
   }
   return;
}

