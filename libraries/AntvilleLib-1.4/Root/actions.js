/**
 * helper action for binary output of
 * the infamous invisible pixel GIF
 * @see spacer_macro
 */
function pixel_gif_action() {
   res.contentType = "image/gif";
   res.writeBinary(Image.PIXELGIF);
   return;
}
