/**
 * @fileoverview Event Framework.
 */

if (!global.knallgrau) {
    global.knallgrau = {};
}


knallgrau.Event = {};

knallgrau.Event.observers = {};

/**
 * Observer
 * @class 
 * <h1>Description</h1>
 * 
 * <h1>Usage</h1>
 * 
 * <h2>Register a method for an Event.</h2>
 * <i>Story/functions.js</i>
 * <pre>
 *  function modFooDoSomething(data, usr) { ; }
 *  knallgrau.Event.registerObserver("Story", "update", "modFooDoSomething");
 * </pre>
 *
 * <h2>Trigger an Event, and calls all registered observers.</h2>
 * <i>Story/functions.js</i>
 * <pre> 
 *  function doUpdate(data, usr) {
 *    knallgrau.Event.notifyObservers(this, "beforeUpdate", arguments);
 *    knallgrau.Event.notifyObservers(this, "afterUpdate", arguments);
 *  }
 * </pre>
 * @constructor
 * 
 */

knallgrau.Event.Observer = function(prototype, name, method, order, methodName) {
   var that = this;
   this.prototype = prototype;
   this.name = name;
   this.method = method;
   this.methodName = (methodName || method.toString());
   this.order = order || 5;
   this.isSame = function(observer) {
      return (this.method == observer.method) || (this.methodName == observer.methodName);
   };
};


/**
 * Triggers an Event, and calls all registered observers.
 * It will return an array of return values from all executed EventListeners,
 * which may also be undefined or null.
 * The length of the array is the same as the number of invoked EventListeners.
 *
 * @param element object that triggers the event; may be null for global events;
 * @param name the name of the event (eg. update, create)
 * @param args optional parameters passed to method
 * @return Array of function results
 */
knallgrau.Event.notifyObservers = function(element, name, args) {
   var result = [];
   // app.log("### knallgrau.Event.notifyObservers called: element=" + element + " name=" + name);
   if (element && element.__node__.getState() === Packages.helma.objectmodel.INodeState.DELETED) {
      app.debug("knallgrau.Event.create is called on a deleted node: " + element + " name=" + name);
      return;
   }
   // determine prototype-chain
   var jsProto = getGlobalObject();
   var prototypeChain = [];
   if (element && element != jsProto) {
      var proto = app.__app__.getPrototypeByName(element._prototype);
      while (proto) {
         prototypeChain.push(proto.getName());
         proto = proto.getParentPrototype();
      }
      var pName = (element._prototype == "hopobject") ? "HopObject" : element._prototype; // HELMABUG: Helma returns 'hopobject' in lower case for removed transient nodes
      jsProto = jsProto[pName].prototype;
   } else {
	   prototypeChain = ["Global"];
   }
   // walk up the prototype chain, and check for registered methods for this event and call them
   for (var i in prototypeChain) {
      if (knallgrau.Event.observers[prototypeChain[i]] && knallgrau.Event.observers[prototypeChain[i]][name]) {
         var observers = knallgrau.Event.observers[prototypeChain[i]][name];
         if (!observers) continue;
         for (var j = 0; j < observers.length; j++) {
            var method = observers[j].method;
            // app.log("### EVENT triggered on " + prototypeChain[i] + " name=" + name + ", calling=" + prototypeChain[i] + "." + method );
            if (isFunction(method)) {
               result.push(method(element, (args || {})));
            } else {
               // handle case of removed method
               if (!jsProto[method])
                  continue;
               // app.debug("EVENT triggered on " + prototypeChain[i] + " name=" + name + ", calling=" + prototypeChain[i] + "." + method );
               // call registered method, with the event-element as its scope
               result.push(jsProto[method].apply(element, args));
            }
         }
      }
   }
   return result;
};


/**
 * Register a method for an Event.
 *
 * @param prototype Prototype of object to be observed (eg. Story, Comment); if null, then a global event is observed;
 * @param eventName name of the event that should be observed (eg. create, update)
 * @param method objectReference to the observer
 * @param order Integer, ranging from 1 to 9; defaults to 5; this allows one to influence the order/priority of the observers
 * @param methodName String, the name of the anonymous function (mandatory if method is a function)
 */
knallgrau.Event.registerObserver = function(prototype, eventName, method, order, methodName) {
   // these arguments must be set
   if (!prototype || !eventName || !method) {
      app.debug("Wrong knallgrau.Event.registerObserver call: " + " prototype=" + prototype + " eventName=" + eventName + ", method=" + method + ", order=" + order + ", methodName=" + methodName);
      // don't register faulty observers
      return;
   }
   if (isFunction(method) && !methodName) {
      app.log("Wrong knallgrau.Event.registerObserver call for anonymous function calls: " + " prototype=" + prototype + " eventName=" + eventName + ", method=" + method + ", order=" + order + ", methodName=" + methodName);
      // don't register faulty observers
      return;
   }
   if (prototype != null && !isString(prototype)) {
      prototype = (prototype._prototype == "hopobject") ? "HopObject" : prototype._prototype; // HELMABUG: Helma returns 'hopobject' in lower case for removed transient nodes
   }
   // as default use Global
   var prototype = (prototype || "Global");
   
   // init prototype Events
   if (!knallgrau.Event.observers[prototype]) {
      knallgrau.Event.observers[prototype] = {};
   }
   
   // init prototype event callbacks
   if (!knallgrau.Event.observers[prototype][eventName]) {
      knallgrau.Event.observers[prototype][eventName] = [];
   }
   
   // get observers for event
   var observers = knallgrau.Event.observers[prototype][eventName];
   
   // create new Observer
   var newObserver = new knallgrau.Event.Observer(prototype, eventName, method, order, methodName);
   
   // find old observer
   var idx = 0;
   for ( ; idx < observers.length; idx++) {
      if (observers[idx].isSame(newObserver)) break;
   }
   
   // register new observer or overwrite old one
   observers[idx] = newObserver;
   
   // sort by order
   observers.sort(function(a, b) { return a.order - b.order; });
   knallgrau.Event.observers[prototype][eventName] = observers;
};
