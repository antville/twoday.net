
/**
 * @fileoverview Helper functions from http://www.crockford.com/javascript/remedial.html
 */
 
/**
 * code via http://www.crockford.com/javascript/remedial.html
 * Douglas Crockford
 * www.crockford.com
 */

/**
 * Returns true for Arrays.
 *
 * @param a  Array
 * @return Boolean
 */
function isArray(a) {
   return isObject(a) && a.constructor == Array;
}


/**
 * Returns true for Arrays.
 *
 * @param a  Array
 * @return Boolean
 */
function isBoolean(a) {
   return typeof a == 'boolean';
}


/**
 * Returns true for functions.
 *
 * @param a  Function
 * @return Boolean
 */
function isFunction(a) {
   return typeof a == 'function';
}


/**
 * Returns true for Null.
 *
 * @param a  Null
 * @return Boolean
 */
function isNull(a) {
   return typeof a == 'object' && !a;
}


/**
 * Returns true for Number.
 *
 * @param a  Number
 * @return Boolean
 */
function isNumber(a) {
   return typeof a == 'number' && isFinite(a);
}


/**
 * Returns true for Objects.
 *
 * @param a  Object
 * @return Boolean
 */
function isObject(a) {
   return (a && typeof a == 'object') || isFunction(a);
}


/**
 * Returns true for Dates.
 *
 * @param a  Date
 * @return Boolean
 */
function isDate(a) {
   return (isObject(a) && a instanceof Date);
}


/**
 * Returns true for strings.
 *
 * @param a  String
 * @return Boolean
 */
function isString(a) {
   return typeof a == 'string';
}


/**
 * Returns true if value is undefined.
 *
 * @param a  Value
 * @return Boolean
 */
function isUndefined(a) {
   return typeof a == 'undefined';
}
