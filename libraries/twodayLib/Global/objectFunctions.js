

if (!global.knallgrau) {
    global.knallgrau = {};
}

knallgrau.isDevTestMode = function(param) {
   // if in development mode, do nothing
   if ("development".equals(app.properties.environment)) {
      app.log("[knallgrau.isDevTestMode] development mode detected.");
      return true;
   }
   if ("testing".equals(app.properties.environment)) {
      app.log("[knallgrau.isDevTestMode] testing mode detected.");
      return true;
   }
   if ("test-t1".equals(app.properties.environment)) {
      app.log("[knallgrau.isDevTestMode] testing mode detected.");
      return true;
   }
   return false;
};

/**
 * Check whether this Helma instance is the master in a cluster scenario.
 * @return Boolean
 */
knallgrau.isHelmaClusterMaster = function() {
   return (app.properties.cluster != "true" || Packages.helma.swarm.ChannelUtils.isMaster(app.__app__));
};


knallgrau.setClusterCookie = function() {
   // set Cluster Cookie if Clustering is enabled
   if (app.properties.cluster == "true" && !req.data["JSESSIONID"]) {
      res.servletResponse.addHeader('Set-Cookie', 'JSESSIONID=worker.' + app.properties.clusterWorkerID + '; SameSite=none; Secure; Partitioned');
   }
};

knallgrau.isRequestByRobot = function() {
   var robotPattern = /(google|slurp|bot|java|jeeves|blog|search)/gi;
   var userAgent = (req.servletRequest.getHeader("user-agent") || "");
   if (!userAgent || userAgent.match(robotPattern))
      return true;
   else
      return false;
};

knallgrau.isGetPostRequest = function() {
   return (req.method == "GET" || req.method == "POST");
}

knallgrau.addToResponseHead = function(headerMarkup) {
   if (!headerMarkup) return;
   if (!res.data.head) res.data.head = "";
   res.data.head += headerMarkup;
}

