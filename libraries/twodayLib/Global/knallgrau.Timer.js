
if (!global.knallgrau) {
    global.knallgrau = {};
}

/**
 * @fileoverview Timer
 */


/**
 * @class 
 * <h1>Description</h1>
 * Allows you to clock processing time of methods.
 * <h1>Usage</h1> 
 * @constructor
 * 
 */
knallgrau.Timer = {};


/**
 *
 */   
knallgrau.Timer.getCurrentTime = function() {
   return java.lang.System.nanoTime();
};


/**
 *
 */
knallgrau.Timer.clock = function(msg) {
   var now = this.getCurrentTime();
   if (!res.meta.KnallgrauTimerCounter) {
      res.meta.KnallgrauTimerCounter = 0;
      res.meta.KnallgrauTimerId = app.requestCount;
      res.meta.KnallgrauTimerStart = now;
   }
   var timeSinceLast = res.meta.KnallgrauTimerLast ? Math.round((now - res.meta.KnallgrauTimerLast)*10/1000000)/10 : req.getRuntime();
   app.log("TIMER " + res.meta.KnallgrauTimerId + " " + req.action + " " + res.meta.KnallgrauTimerCounter + ": " + timeSinceLast + "ms " + req.getRuntime() + "ms " + (msg || ""));
   res.meta.KnallgrauTimerCounter++;
   res.meta.KnallgrauTimerLast = this.getCurrentTime();
};

