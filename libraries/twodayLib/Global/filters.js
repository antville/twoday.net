/**
 * Transforms a string to lowercase.
 *
 * @see String.prototype.toLowerCase
 */
function className_filter(input) {
   if (String.prototype.toFileName) {
      return (input || "").toString().toFileName().toLowerCase();
   }
   var str = input.toString().toLowerCase();
   str = str.replace(new RegExp("[" + String.fromCharCode(252) + "]", "g"), "ue");
   str = str.replace(new RegExp("[" + String.fromCharCode(228) + "]", "g"), "ae");
   str = str.replace(new RegExp("[" + String.fromCharCode(246) + "]", "g"), "oe");
   str = str.replace(new RegExp("[" + String.fromCharCode(223) + "]", "g"), "ss");
   str = str.replace(/[^a-zA-Z0-9_]/g, "-");
   str = str.replace(/-+/g, "-");
   return str;
}


/**
* Escapes the Quotes and newlines so we produce correct JSON Strings
*/
function escapeForJSON_filter(input) {
   var replace = Packages.org.mortbay.util.StringUtil.replace;
   var str = (input || "").toString();
   return replace(replace(str, '"', '\\"'), "\n", "\\\n");
}
