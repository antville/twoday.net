try {
   Helma = Helma;
} catch (err) {
   Helma = {
      toString: function() {
         return "[Helma JavaScript Library]";
      }
   }
}

Helma.Image = function(arg) {
   // according to http://grazia.helma.org/pipermail/helma-dev/2004-June/001253.html
   var generator = Packages.helma.image.ImageGenerator.getInstance();
   return generator.createImage(arg);
}
