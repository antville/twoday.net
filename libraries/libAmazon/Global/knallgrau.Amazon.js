
/**
 * Wrapper lib for Amazon Api implementation
 */

/**
 * Global initialization
 */
if (!global.knallgrau) {
    global.knallgrau = {};
}


knallgrau.Amazon = function(param) {
   var config = {
      serviceUrl: getProperty("knallgrau.Amazon.serviceUrl"),
      host: getProperty("knallgrau.Amazon.host"),
      serviceName: getProperty("knallgrau.Amazon.serviceName"),
      serviceOperation: getProperty("knallgrau.Amazon.serviceOperation"),
      accessKey: getProperty("knallgrau.Amazon.accessKey"),
      responseGroup: getProperty("knallgrau.Amazon.responseGroup"),
      secretKey: java.lang.System.getProperty("knallgrau.Amazon.secretKey"),
      returnE4X: true
   };
   if (!config.secretKey) {
	   config.secretKey = getProperty("knallgrau.Amazon.secretKey")
   }
   // fill with defaults
   config = Object.clone((param || {}), config);
   
   // crockford like private/public functions
   
   function doSearch(what, keywords) {
/*      var url = config.serviceUrl
                + "?"
                + "Service=" + encodeURIComponent(config.serviceName)
                + "&AWSAccessKeyId=" + encodeURIComponent(config.accessKey)
                + "&Operation=" + encodeURIComponent(config.serviceOperation)
                + "&ResponseGroup=" + encodeURIComponent(config.responseGroup)
                + "&SearchIndex=" + encodeURIComponent(what)
                + "&Keywords=#searchKeyword#";
                */
      var requestParams = new java.util.HashMap();
      requestParams.put("Service", config.serviceName);
      requestParams.put("Operation", config.serviceOperation);
      requestParams.put("ResponseGroup", config.responseGroup);
      requestParams.put("SearchIndex", what);
      requestParams.put("Keywords", keywords);
      var signedRequestsHelper = new Packages.at.knallgrau.amazon.SignedRequestsHelper(config.accessKey, config.secretKey, config.host);
      var url = signedRequestsHelper.generateUrl(requestParams);
      // app.log("[knallgrau.Amazon.doSearch] url: " + url);
      var client = new helma.Http();
      var response = client.getUrl(url);
      // app.log("[knallgrau.Amazon.doSearch] response.content: " + response.content);
      var txt = response.content;
      if (txt) {
         txt = txt.replace(/<\?xml version="1.0"[^\?]*\?>/, ""); // bug 336551 in rhino !!!
      }
      return config.returnE4X ? new XML(txt) : response.content;
   };
   
   var that = this;

   this.searchBooks = function(keywords) {
      return doSearch("Books", keywords);
   };
   
   this.searchElectronics = function(keywords) {
      return doSearch("Electronics", keywords);
   };
   this.searchPopMusic = function(keywords) {
      return doSearch("Music", keywords);
   };
   this.searchClassicalMusic = function(keywords) {
      return doSearch("Classical", keywords);
   };
   this.searchMusic = function(keywords) {
      var popResult = doSearch("Music", keywords);
      var classicalResult = doSearch("Classical", keywords);
      // append classical to popResult
      default xml namespace = "http://webservices.amazon.com/AWSECommerceService/2005-10-05";
      var items = result..Item;
      for (var i = 0; i < items.length(); i++) {
         popResult.ItemSearchResponse.Items.appendChild(items[i]);
      }
      default xml namespace = "http://www.w3.org/1999/xhtml";
      return popResult;
   };
   
   this.searchVHS = function(keywords) {
      return doSearch("VHS", keywords);
   };

   this.searchDVD = function(keywords) {
      return doSearch("DVD", keywords);
   };
   return this;
};
