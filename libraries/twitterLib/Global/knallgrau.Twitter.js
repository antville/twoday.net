

if (!global.knallgrau) {
    global.knallgrau = {};
}

knallgrau.Twitter = function(param) {
   var internalReturnFormat = ".json";
   var version = "1.1";
   var clientName = "twoday.net Twittererer " + version;
   // clientName = "Philipps Twittererer " + version;
   // fill with defaults
   var config = {
            userId: "",
            userPassword: "",
            serviceProtocol: "https",
            serviceHost: "twitter.com",
            serviceUpdatePath: "/statuses/update",
            serviceFollowersUpdatesPath: "/statuses/friends_timeline",
            returnValueFormat: ".json",
            debug: false,
            searchUrl: "http://search.twitter.com/search",
            userDataUrl: "http://twitter.com/users/show/",
            socketTimeout: 2000,
            readTimeout: 2000
         };
   
   // overwrite defaults by param if given
   if (param) {
      for (var i in config) {
         if (param[i])
            config[i] = param[i];
      }
      delete(i);
   }

   // check if config.returnvalueFormat starts with .
   
   // crockford like private functions
   
   function _debug(msg) {
      if (config.debug)
         app.log("[knallgrau.Twitter] " + uneval(msg));
   };
   
   function _generateUrl(servicePath, format, auth) {
      var url = config.serviceProtocol + "://";
      if (auth)
         url += config.userId + ":" + config.userPassword + "@";
      url += config.serviceHost + servicePath + format;
      _debug(url);
      return url;
   };
   
   function _getHttpClient() {
      var client = new helma.Http();
      //client.setCredentials(config.userId, config.userPassword);
      // client.setMethod("GET");
      client.setUserAgent(clientName);
      client.setTimeout(config.socketTimeout);
      client.setReadTimeout(config.readTimeout);
      return client;
   };
   
   function _doUpdate(text) {
      var url = config.serviceUrl
      + config.serviceUpdatePath
      + internalReturnFormat;
      var httpClient = _getHttpClient();
      //?status="your message here"
      httpClient.setMethod("POST");
      httpClient.setContent({status: text});
      var url = _generateUrl(config.serviceUpdatePath, internalReturnFormat, true);
      var result = httpClient.getUrl(url);
      // check status
      _debug(result);
      return result;
   };
   
   function _doGetFollowerUpdates() {
      var httpClient = _getHttpClient();
      var result = httpClient.getUrl(_generateUrl(config.serviceFollowersUpdatesPath, config.returnValueFormat, true));
      _debug(result);
      return (result.code == 200 ? result.content : null);
   };
   
   function _doGetSearchResults(searchString, sinceId) {
        var httpClient = _getHttpClient();
        var url = config.searchUrl + config.returnValueFormat + "?q=" + encodeURIComponent(searchString) + "&rpp=100" + (sinceId ?  "&since_id=" + sinceId : "");
        var result = httpClient.getUrl(url);
        _debug(result);
        return (result.code == 200 ? result.content : null);
   };
   
   var that = this;
   
   // crockford like public functions
   this.updateStatus = function(text) {
      _doUpdate(text);
   };

   this.getFollowerUpdates = function() {
      return _doGetFollowerUpdates();
   };
   
   this.getSearchResults = function(searchString, sinceId) {
      return _doGetSearchResults(searchString, sinceId);
   }

   this.getUserData = function(userName) {
      var httpClient = _getHttpClient();
      var url = config.userDataUrl + userName + config.returnValueFormat;
      var result = httpClient.getUrl(url);
      _debug(result);
      return (result.code == 200 ? result.content : null);
   }
   return this;
};
