
/**
 * Wrapper lib for akismet java implementation
 */

/**
 * Global initialization
 */
if (!global.knallgrau) {
    global.knallgrau = {};
}


if (!knallgrau.Akismet) {
  knallgrau.Akismet = {};
}


/**
 * Wrapper for call. Always checks for Spam
 *
 * @see knallgrau.Akismet.call
 */
knallgrau.Akismet.isSpam = function(param) {
   param.method = "commentCheck";
   return knallgrau.Akismet.call(param);
}


/**
 * Sends http post request to central akismet spamserver.
 * Hint: setting creatorName="viagra-test-123" will always 
 * mark comment as spam by akismet and can be used for testing.
 * @see http://akismet-java.sourceforge.net/
 *
 * @param param Object containing all the needed props:
 *             apiKey
 *             blogUrl
 *             method String (optional) "commentCheck", "submitSpam", "submitHam"
 *             ipAddress String (optional)
 *             userAgent String (optional)
 *             httpReferer String (optional)
 *             permaLink
 *             contentType String (optional) "comment", "trackback", "pingback", or a made up value like "registration"
 *             authorName
 *             authorEmail
 *             authorURL
 *             content
 * @return String result from akismet request (true | false in case of method=commentCheck)
 *
 */
knallgrau.Akismet.call = function(param) {
   var modSpamAkismetPkg = Packages.net.sf.akismet;
   // set some default values
   param.apiKey = (param.apiKey || app.properties.akismetApiKey);
   if (!param.apiKey) {
      throw new Exception("libAkismet: No API Key found!");
   }
   if (!param.blogUrl) {
      throw new Exception("libAkismet: No blog Url found!");
   }
   param.ipAddress = (param.ipAddress || req.data.http_remotehost);
   param.userAgent = (param.userAgent || req.data.http_browser);
   param.httpReferer = (param.httpReferer || req.data.http_referer);
   param.method = (param.method || "commentCheck");
   param.contentType = (param.contentType || "");
   // var before = new Date();
   var akismet = new modSpamAkismetPkg.Akismet(param.apiKey, param.blogUrl);
   var result = app.invoke(akismet, param.method, [ 
            param.ipAddress,
            param.userAgent,
            param.httpReferer,
            param.permaLink,
            param.contentType,
            param.authorName, // author 
            param.authorEmail, // authorEmail
            param.authorURL, // authorURL
            param.content,
            null // other
        ], 5000);
   // var after = new Date();
   // app.log("akismet: " + (after - before));
   var statusCode = akismet.getHttpResult();
   if (statusCode != 200) {
      throw new Exception("libAkismet: Error calling Akismet Server (StatusCode=" + statusCode + ")");
   }
   return result;
}
