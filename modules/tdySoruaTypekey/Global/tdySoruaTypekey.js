
app.modules.tdySoruaTypekey = {
   title: "SORUA - Typekey",
   description: "",
   version: "",
   state: "",
   url: "http://www.sorua.net",
   author: "",
   authorEmail: "",
   authorUrl: "",
   authorOrganisation: "Knallgrau",
   license: "twoday",
   licenseUrl: "",
   licenseText: "",
   copyright: "",
   isSidebar: false,
   hasSiteSettings: false,
   skins: []
}


app.modules.tdySoruaTypekey.settings = {
   LOGIN_URL: "https://www.typekey.com/t/typekey/login",
   LOGOUT_URL: "https://www.typekey.com/t/typekey/logout",
   TYPEKEY_TOKEN: ""
}

app.modules.tdySoruaTypekey.renderSystemSetup = function() {
   root.manage.renderSkin("tdySoruaTypekeySetup");
};


app.modules.tdySoruaTypekey.evalSystemSetup = function(param) {
   root.preferences.setProperty("tdySoruaTypekeyEnabled", req.data.preferences_tdySoruaTypekeyEnabled ? true : false);
};
