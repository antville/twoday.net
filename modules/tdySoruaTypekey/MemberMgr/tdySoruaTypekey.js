/**
 * SORUA AuthURI
 */
function tdySoruaTypekey_action() {
   // check whether the Typekey is activated
   
   if (root.preferences.getProperty("tdySoruaTypekeyEnabled") != true) { 
      res.redirect(root.href("main"));
   }

   if (!app.data.tdySoruaTypekey) {
      app.data.tdySoruaTypekey = new Array();
   }
   var returnUrl  = req.data["sorua-return-url"].toURL();
   var failUrl    = req.data["sorua-fail-url"].toURL();
   var userID     = req.data["sorua-user"];
   var action     = req.data["sorua-action"];
  
  if (returnUrl && action) {
    session.data.tdySoruaTypekey = {"returnUrl": returnUrl, 
                            "failUrl": failUrl,
                            "userID": userID,
                            "action": action};
  }
  
  /*if we have a response from typekey, our real return url is in the session.*/
  if(action == "sorua-validate-typekeyResponse"){
     returnUrl = session.data.tdySoruaTypekey["returnUrl"];
  }
  
   // handle wrong call of AuthURI
  if (!action || !returnUrl) {
     res.redirect(root.href("main"));
  }
  
   // extract domain of UserServer from the returnUrl
   var userServer = returnUrl.substring(returnUrl.indexOf("/") + 2, 
                                        returnUrl.indexOf("/", returnUrl.indexOf("/") + 2));

   
                                        
   /* authenticate-action*/
   if (action == "authenticate") {
      this.authenticateWithTypekey(returnUrl);
   // verify-action
   } else if (action == "verify") {
      // first remove outdated entries
      var now = new Date();
      var arr = new Array();
      for (var i in app.data.tdySoruaTypekey) {
         if (!app.data.tdySoruaTypekey[i] || !app.data.tdySoruaTypekey[i].time) continue;
         if (now.valueOf() - app.data.tdySoruaTypekey[i].time.valueOf() < 1000 * 30)
            arr[i] = app.data.tdySoruaTypekey[i];
      }
      app.data.tdySoruaTypekey = arr;
      // now check whether returnUrl has been used recently
      var usr = (app.data.tdySoruaTypekey[returnUrl] && app.data.tdySoruaTypekey[returnUrl].userID) ?
                  app.data.tdySoruaTypekey[returnUrl].userID : null;
      if (usr) {
         // write OK response
         res.contentType = "text/plain";
         res.status = 200;
         res.write("user:" + usr + "\n");
         var email = app.data.tdySoruaTypekey[returnUrl]["email"];
         if (email && email.contains("@") == false) email = null;
         if (email) res.write("email:" + email + "\n");
         app.data.tdySoruaTypekey[returnUrl] = null;
         return;
      } else {
         res.status = 403;
         return;
      }

   // logout-action
   } else if (action == "logout") {
      
      //if return URL is from localhost, we cannot logout with typekey
      var domain = returnUrl.substring(0,16);
      if( (domain == "http://localhost") || (domain == "http://127.0.0.1") ) {
         res.redirect(returnUrl);
      }
      
      var retUrl = app.modules.tdySoruaTypekey.settings.LOGOUT_URL+"?_return=" + encodeURIComponent(returnUrl);
      res.redirect(retUrl);
      
   
   }else if (action == "sorua-validate-typekeyResponse") {
      if(this.isValidTypekeyResponse()){
        var token = ((new Date()).valueOf()).toString().md5();
        var returnUrl = returnUrl.replace(/SORUA-AUTH-TOKEN/g, token);
        app.data.tdySoruaTypekey[returnUrl] = {"time": new Date(), "userID": req.data["name"]};
         if (req.data["email"]) app.data.tdySoruaTypekey[returnUrl].email = req.data["email"];
        res.redirect(returnUrl);
      }else{
        this.authenticateWithTypekey(returnUrl);
      }
   } else { 
     res.redirect(root.href("tdySoruaTypekey"));
   }   
}
 
/*make sure that the response really came from the typekey system*/
function isValidTypekeyResponse(){
   var token = app.modules.tdySoruaTypekey.settings.TYPEKEY_TOKEN;
   var sig = req.servletRequest.getParameter("sig"); 
   sig = sig.replace(/ /g,"+");
   var typeKey4J = new Packages.net.sf.typekey4j.TypeKey4J();
   typeKey4J.retrieveTypekeyDSAKey();
   var verified = typeKey4J.verify("1.1",req.data.email,req.data.name,
                                req.data.nick,req.data.ts,sig,token);
   return verified;
}

/**
 * forward to typekey login page, where user can 
 * authenticate with the typekey service
 */
function authenticateWithTypekey(returnUrl) {
  var token = app.modules.tdySoruaTypekey.settings.TYPEKEY_TOKEN;
  var url = app.modules.tdySoruaTypekey.settings.LOGIN_URL;
  var tkReturnUrl = this.href("tdySoruaTypekey")
     + "%3Fsorua-action%3Dsorua-validate-typekeyResponse%26sorua-return-url%3D"
     + encodeURIComponent(returnUrl);
  var tkrequest = url + "?v=1.1&t=" + encodeURIComponent(token) + "&_return=" + tkReturnUrl;
  res.redirect(tkrequest);
}
