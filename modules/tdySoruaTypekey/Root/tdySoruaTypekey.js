
/**
 * renders a checkbox to set tdySoruaTypekeyEnabled-flag
 */
function tdySoruaTypekeyEnabled_macro(param) {
   if (param.as == "editor") {
      Html.checkBox(root.preferences.createCheckBoxParam("tdySoruaTypekeyEnabled", param));
   } else {
      res.write(root.preferences.getProperty("tdySoruaTypekeyEnabled") ? getMessage("generic.yes") : getMessage("generic.no"));
   }
   return;
}
