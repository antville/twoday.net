
/**
 * If the user clicks on the "search on amazon" button this function renders some hidden fields that are filled
 * with the results of the amazon booksearch. urlparameters are 'title' and of the book to search for.
 * The resulting skin is rendered to a hidden iframe in the skin "modGadgetsSetup". In the onLoad function of the iframe, the values are written
 * out as list of links (search result links).
 */
function modGadgetsSearch_action() {
   var amazon = new knallgrau.Amazon();
   
   // search results from german electronics search
   var result = amazon.searchElectronics((req.data.title || ""));
   default xml namespace = "http://webservices.amazon.com/AWSECommerceService/2005-10-05";
   var items = result..Item;
   var param = {
      title: "",
      image: "",
      url: "",
   };
   for (var i = 0; i < items.length(); i++) {
      if (i != 0) {
         param.url += ":#:";
         param.title += ":#:";
         param.image += ":#:";
      }
      param.url += items[i].DetailPageURL.text();
      param.title += items[i].ItemAttributes.Title.text();
      param.image += items[i].MediumImage.URL.text();
   }
   default xml namespace = "http://www.w3.org/1999/xhtml";
   this.renderSkin("modGadgetsSearchResult", param);
   return;
}