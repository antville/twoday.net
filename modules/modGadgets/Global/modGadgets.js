
app.modules.modGadgets = {
   title: "Gadgets",
   description: "Displays a list of the Gadgets you possess.",
   version: "",
   state: "beta",
   url: "",
   author: "Johannes Lerch",
   authorEmail: "johannes.lerch@knallgrau.at",
   authorUrl: "http://www.knallgrau.at",
   authorOrganisation: "knallgrau.at",
   copyright: "(c) 2005 knallgrau.at",
   license: "antville",
   licenseUrl: "http://www.antville.org",
   isSidebar: true,
   type: MODULE_TYPE_LIST,
   skins: ["Site.modGadgets", "Site.modGadgetsItem", "Site.modGadgetsDivider"]
};


/**
 * Renders module in the sidebar. The renderSidebarItem function of modGadgets at first reads exisiting
 * booklist entries from the site's preferences.
 * After Gadgets the items and splitting them up, for every item the "modGadgetsItem" skin is rendered and inbetween the skin "modGadgetsDivider".
 * Linked alltogether the resulting string is handed over to the skin "modGadgets" as body and rendered to the sidebar.
 */
app.modules.modGadgets.renderSidebarItem = function(param) {
   if (!param) param = {};
   var GadgetsTitles = app.modules.modGadgets.getGadgetsProperty(this, "titles");
   var GadgetsUrls = app.modules.modGadgets.getGadgetsProperty(this, "urls");
   var GadgetsImages = app.modules.modGadgets.getGadgetsProperty(this, "images");

   res.push();
   for (var i=0; i<GadgetsTitles.length; i++) {
      var p = {};
      p.GadgetsTitle = GadgetsTitles[i];
      p.GadgetsUrl = GadgetsUrls[i];
      var Img = this.images.get(GadgetsImages[i]);
      if (!Img)
         p.GadgetsImage = GadgetsImages[i];
      else
         p.GadgetsImage = Img.getUrl();
      if (i != 0) this.renderSkin("modGadgetsDivider");
      this.renderSkin("modGadgetsItem", p);
   }
   var p = {};
   p.Gadgets = res.pop();
   res.push();
   this.renderSkin("modGadgets", p);
   var p = {};
   p.body = res.pop();
   p.editLink = true;
   this.renderSidebarModuleItem("modGadgets", p);
}


/**
 * Generates the arranger-field of the module's preferences and hands it over to the "modGadgetsSetup" skin.
 */
app.modules.modGadgets.renderPreferences = function() {
   Module.prototype.renderSetupSpacerLine();
   var p = {};
   p.GadgetsArranger = app.modules.modGadgets.renderGadgetsArranger(this);
   Module.prototype.renderSkin("modGadgetsSetup", p);
};


/**
 * Is called when user clicks the "save" button in module preferences. The values in the item-arranger are read and split up in title, author, url, image.
 * In the arranger itself the items are saved as strings separated by the sequence ":#:". After pushing the values into an array, they are stored in the
 * site's preferences by calling toSource array function.
 */
app.modules.modGadgets.evalModulePreferences = function() {
   var GadgetsTitles = new Array();
   var GadgetsUrls = new Array();
   var GadgetsImages = new Array();
   

   try {
      var GadgetsArrangerValues = JSON.parse(req.data.GadgetsArrangerValues);
   } catch (err) {
      return;
   }
   if (!GadgetsArrangerValues)
      var GadgetsArrangerValues = new Array();

   for (var i=0; i<GadgetsArrangerValues.length; i++) {
      var temp = GadgetsArrangerValues[i].split(":#:");
      GadgetsTitles.push(temp[0]);
      GadgetsUrls.push(temp[1]);
      GadgetsImages.push(temp[2]);
   }

   this.preferences.setProperty("modGadgets_titles", GadgetsTitles.toSource());
   this.preferences.setProperty("modGadgets_urls", GadgetsUrls.toSource());
   this.preferences.setProperty("modGadgets_images", GadgetsImages.toSource());
};


/**
 * Gets an array containing the titles, urls, authors, images of all list items. if nothing is found, an empty array is returned.
 *
 * @return Array containing either titles, urls, authors or images
 */
app.modules.modGadgets.getGadgetsProperty = function(obj, name) {
   try {
      var arr = JSON.parse(obj.preferences.getProperty("modGadgets_" + name));
   } catch (err) {
      ;
   }
   if (!arr)
      var arr = new Array();

   return arr;
};


/**
 * Creates the item arranger field for module's preferences. First all existing items are read, then pushed into an array that is used to create
 * a value-filled dropDown-List. This List is handed over to the skin "modGadgetsArranger", which also consists of three buttons to remove and reposition
 * items in the list.
 */
app.modules.modGadgets.renderGadgetsArranger = function(obj) {
   var GadgetsTitles = app.modules.modGadgets.getGadgetsProperty(obj, "titles");
   var GadgetsUrls = app.modules.modGadgets.getGadgetsProperty(obj, "urls");
   var GadgetsImages = app.modules.modGadgets.getGadgetsProperty(obj, "images");

   var GadgetsValues = [];
   for (var i=0; i<GadgetsTitles.length; i++) {
      GadgetsValues.push({value: GadgetsTitles[i] + ":#:" + GadgetsUrls[i] + ":#:" + GadgetsImages[i], display: GadgetsTitles[i] });
   }

   var p = {};
   p.GadgetsArranger = Html.dropDownAsString({name: "GadgetsArranger", id: "GadgetsArranger", size: 12, multiple: "multiple", style: "width:440px;", onchange: "getItem(); return false;"}, GadgetsValues);

   return Module.prototype.renderSkinAsString("modGadgetsArranger", p);
};
