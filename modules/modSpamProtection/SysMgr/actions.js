
function modSpamProtection_action() {
   var lists = root.modSpamProtection.getAllLists();
   var type = req.data.type;
   if (type && Array.contains(lists, type)) {
      var f = new Helma.File(getProtectedStaticDir("modSpamProtection"), type + ".txt");
      if (!f.exists()) var f = new Helma.File(getProtectedStaticDir("modSpamProtection"), type + ".csv.txt");
      if (req.isPost()) {
         checkSecretKey();
         f.remove();
         f.open();
         f.write(req.data.list);
         f.close();
         app.data["modSpamProtection-" + type] = root.modSpamProtection.getList(type);
         res.redirect(this.href(req.action));
      }
      req.data.list = (f.exists()) ? f.readAll() : "";
      res.data.title = getMessage("SysMgr.setup.title", {serverTitle: root.getTitle()});
      res.data.action = this.href(req.action);
      res.data.body = this.renderSkinAsString("modSpamProtectionEditList");
      this.renderMgrPage();
   } else {
      var param = {lists: ""};
      for (var i in lists) {
         param.lists += "<li><a href=\"" + this.href("modSpamProtection") + "?type=" + lists[i] + "\">" + lists[i] + "</a></li>";
      }
      res.data.body = this.renderSkinAsString("modSpamProtection", param);
      this.renderMgrPage();
   }
}
