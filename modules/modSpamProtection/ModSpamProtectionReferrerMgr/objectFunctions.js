
/**
 * Return array of block- and white-lists.
 */
function getLists() {
   return ["referrer-domain-white", "referrer-domain-block", "referrer-ip-white", "referrer-ip-block", "referrer-word-block"];
}


/**
 * Calls this.track() and performs controller-handling if spam is detected
 */
function handleSpam() {
   var spamLevel = this.track();
/*   if (spamLevel == 100) {
      var currentUrl = path[path.length - 1].href(req.action);
      res.reset();
      res.write("Referrer Spam Detection: <a href=\"" + currentUrl + "\">click here</a>"); // IE will not display this message; just Firefox & Opera
      res.status = 403; // HTTP Status code 'forbidden'; we need to set HTTP status code, so that logfile-analyzer knows, that it should not count this request
      res.abort(); // FIXME: is there an alternative to res.abort, which doesnt close db connections?
   } */
   if (spamLevel >= 50) {
      req.data.ref = 0; // set flag that no referrer should be stored
   }
};

/**
 * Determines whether the current request is referrer spam, and returns a spam-level.
 * 
 * @return level 0=="no spam", 50=="probably spam", 100=="definitely spam"
 */
function track() {
   var referrerUrl = (req.data.http_referer || "").toLowerCase();
   if (!referrerUrl)
      return 0;

   var clientIp = req.data.http_remotehost;
   var referrerHost = this.extractHostFromUrl(referrerUrl);
   var requestUrl = path[path.length-1].href(req.action);
   var requestHost = this.extractHostFromUrl(requestUrl);
   var rootHost = this.extractHostFromUrl(root.href());

   // return, if referrer comes from current host
   if (referrerHost == requestHost || referrerHost == rootHost)
      return 0;

   this.setup();

   // check permanent modSpamProtection-referrer-(block/white)lists
   if (app.data["modSpamProtection-referrer-domain-white"].contains(referrerHost))
      return 0;
   if (app.data["modSpamProtection-referrer-ip-white"].contains(clientIp))
      return 0;
   if (app.data["modSpamProtection-referrer-domain-block"].contains(referrerHost))
      return 100;
   if (app.data["modSpamProtection-referrer-ip-block"].contains(clientIp))
      return 100;
   var stopWords = app.data["modSpamProtection-referrer-word-block"].toArray();
   for (var i in stopWords) {
      if (referrerUrl.indexOf(stopWords[i]) != -1) return 50;
   }

   var info = this.getInfoObject(referrerHost);

   // we need at least over x requests, so that we can tell for sure whether this is a spammer or not
   if (info.requests < 1000) {
      info.requests++;
      info.ips.add(req.data.http_remotehost);
   }

   var spamLevel = this.getSpamLevel(info);
   if (info.requests >= 1000) {
      var line = info.requests + "/" + info.ips.size() + ":" + info.ips.toArray().join(",") + ":" + info.users.toArray().join(",");
      if (spamLevel == 100)
         this.addToList("referrer-domain-block", referrerHost, line);
      else if (spamLevel == 0)
         this.addToList("referrer-domain-white", referrerHost, line);
   }
   app.log("REFERRERSPAM DEBUG " + referrerHost + " info: " + info.requests + "/" + info.ips.size() + " = " + (info.requests / info.ips.size()) + " -> spamLevel: " + spamLevel);
   return spamLevel;
};

/**
 * Calculates SpamLevel, ranging from 0 to 100.
 * @return Int
 */
function getSpamLevel(info) {
   if (info.requests >= 1000 && info.requests/info.ips.size() > 5)
      return 100; // definitely a spammer
   else if (info.requests > 2 && info.requests/info.ips.size() > 2)
      return 50;  // probably a spammer
   else
      return 0;
};

/**
 * Fetches Info-Object, which collects data for a certain referrerHost.
 */
function getInfoObject(referrerHost) {
   var info = app.data["modSpamProtection-referrer"].get(referrerHost);
   if (!info) {
      var info = {};
      info.requests = 0;
      info.ips = new java.util.HashSet();
      info.users = new java.util.HashSet();
      app.data["modSpamProtection-referrer"].put(referrerHost, info);
   }
   return info;
};

/**
 * Initialize variables, and load (Block/White)-lists.
 */
function setup() {
   var lists = this.getLists();
   for (var i in lists) {
      if (!app.data["modSpamProtection-" + lists[i]]) {
         app.data["modSpamProtection-" + lists[i]] = this.getList(lists[i]);
      }
   }
   if (!app.data["modSpamProtection-referrer"])
      app.data["modSpamProtection-referrer"] = new Packages.helma.util.CacheMap(128);
};

/**
 * Handle spam reported by users.
 * @param referrerUrl reported referrer
 * @param user Identification String for user, who reported spam. e.g. username or ip-address.
 */
function reportSpam(referrerUrl, user) {
   var referrerHost = this.extractHostFromUrl(referrerUrl);
   // if on blocklist, do nothing
   if (app.data["modSpamProtection-referrer-domain-block"].contains(referrerHost))
      return;
   // if on whitelist, then ALERT
   if (app.data["modSpamProtection-referrer-domain-white"].contains(referrerHost)) {
      // app.logger.warn();
      app.log("REFERRERSPAM WARN item on whitelist has been reported as spam: " + referrerUrl + " user: " + user);
   }
   var info = this.getInfoObject(referrerHost);
   info.users.add(user || (new Date()).md5());
   // if more than 2 users report a certain referrer as spam, than that host is put on the blocklist
   if (info.users.size() >= 2) {
      var line = info.requests + "/" + info.ips.size() + ":" + info.ips.toArray().join(",") + ":" + info.users.toArray().join(",");
      this.addToList("referrer-domain-block", referrerHost, line);
   }
};

/**
 * Renders the current content of the ReferrerSpam-Cache
 */
function debug_action() {
   if (req.data.host) {
      var info = app.data["modSpamProtection-referrer"].get(req.data.host);
      res.writeln("<h1>request history for host " + req.data.host + "</h1>");
      res.writeln("<h3>requests:</h3>" + info.requests);
      res.writeln("<h3>ips:</h3>" + info.ips.toArray().join(","));
      res.writeln("<h3>users:</h3>" + info.users.toArray().join(","));
   }
   var cache = app.data["modSpamProtection-referrer"].getCachedObjects();
   res.writeln("<h1>currently filtered referrer hosts</h1>");
   for (var i in cache) {
      var host = cache[i];
      var info = app.data["modSpamProtection-referrer"].get(host);
      if (!info) continue;
      res.write("<a href='?host=" + host + "'>" + host + "</a>: reqs:" + info.requests + " ips:" + info.ips.size() + " users:" + info.users.size() + " reqs/ips:" + (info.requests/info.ips.size()) + "<br>");
   }
}
