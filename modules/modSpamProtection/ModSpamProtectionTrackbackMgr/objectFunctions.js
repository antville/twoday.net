
/**
 * Return array of block- and white-lists.
 */
function getLists() {
   return ["trackback-domain-white", "trackback-domain-block", "trackback-ip-white", "trackback-ip-block", "trackback-word-block"];
}


/*
*) �berpr�fen ob Trackback-URL einen Link auf das Ziel enth�lt?
   -> k�nnte von spammern ebenfalls umgangen werden
*) Anzahl der verwendeten IP-Adressen pro URL bringt nichts
*) Trackback-Spammer weisen ganz unterschiedl Muster auf?
*) Trackback-Volumen bringt nur was bei aggressiven Spammern
   -> wichtig aber dass man diese fr�hzeitig erkennt und abf�ngt
   Kriterium: Anzahl an Requests pro Domain pro Zeiteinheit
   Kriterium: Anzahl an Requests pro IP pro Zeiteinheit
   z.b. mehr als 3 pro stunde, mehr als 20 pro Tag
   speichern dieser Information in app.data
   Falls Kriterium erreicht wird, dann mail an sysadmin, sowie Hinzuf�gen zur Blocklist
*/

function handleSpam() {
   var spamLevel = this.track();
app.log("TRACKBACKSPAM DEBUG " + (req.data.url) + " -> " + spamLevel);
   if (spamLevel == 100) {
      throw new Error("Trackback Spam Detection");
   }
};

function track() {
   var trackbackUrl = (req.data.url).toLowerCase();
   var trackbackText = (req.data.title + " " + req.data.excerpt).toLowerCase();

   var clientIp = req.data.http_remotehost;
   var trackbackHost = this.extractHostFromUrl(trackbackUrl);
   var requestUrl = path[path.length-1].href(req.action);
   var requestHost = this.extractHostFromUrl(requestUrl);
   var rootHost = this.extractHostFromUrl(root.href());

   // return, if trackback comes from current host
   if (trackbackHost == requestHost || trackbackHost == rootHost)
      return 0;

   this.setup();

   // check permanent modSpamProtection-trackback-(block/white)lists
   if (app.data["modSpamProtection-trackback-domain-white"].contains(trackbackHost))
      return 0;
   if (app.data["modSpamProtection-trackback-ip-white"].contains(clientIp))
      return 0;
   if (app.data["modSpamProtection-trackback-domain-block"].contains(trackbackHost))
      return 100;
   if (app.data["modSpamProtection-trackback-ip-block"].contains(clientIp))
      return 100;
   var stopWords = app.data["modSpamProtection-trackback-word-block"].toArray();
   for (var i in stopWords) {
      if (trackbackUrl.indexOf(stopWords[i]) != -1 || trackbackText.indexOf(stopWords[i]) != -1) return 100;
   }

   var hostBytes = java.net.InetAddress.getByName(trackbackUrl.substring(7, trackbackUrl.indexOf("/", 7))).getHostAddress().split(".");
   var clientBytes = java.net.InetAddress.getByName(clientIp).getHostAddress().split(".");
   if (clientBytes[0] != hostBytes[0] || clientBytes[1] != hostBytes[1] || clientBytes[2] != hostBytes[2]) {
      app.log("TRACKBACKSPAM DEBUG " + hostBytes + " != " + clientBytes + " " + trackbackHost + " identified as spam");
      return 100;
   }

   var info = this.getInfoObject(trackbackHost);

   // stop counting after x requests
   if (info.requests < 100) {
      info.requests++;
      info.ips.add(req.data.http_remotehost);
   }

   var spamLevel = this.getSpamLevel(info);
   if (info.requests >= 100) {
      var line = info.requests + "/" + info.ips.size() + ":" + info.ips.toArray().join(",") + ":" + info.users.toArray().join(",");
      if (spamLevel == 100)
         this.addToList("trackback-domain-block", trackbackHost, line);
      else if (spamLevel == 0)
         this.addToList("trackback-domain-white", trackbackHost, line);
   }
   app.log("TRACKBACKSPAM DEBUG " + trackbackHost + " info: " + info.requests + "/" + info.ips.size() + " = " + (info.requests / info.ips.size()) + " -> spamLevel: " + spamLevel);
   return spamLevel;
}


function getInfoObject(trackbackHost) {
   var info = app.data["modSpamProtection-trackback"].get(trackbackHost);
   if (!info) {
      var info = {};
      info.requests = 0;
      info.ips = new java.util.HashSet();
      info.users = new java.util.HashSet();
      app.data["modSpamProtection-trackback"].put(trackbackHost, info);
   }
   return info;
}


function getSpamLevel(info) {
   if (info.ips.size() > 3) {
      return 100; // definitely a spammer
   } else {
      return 0;
   }
}


function setup() {
   var lists = this.getLists();
   for (var i in lists) {
      if (!app.data["modSpamProtection-" + lists[i]]) {
         app.data["modSpamProtection-" + lists[i]] = this.getList(lists[i]);
      }
   }
   if (!app.data["modSpamProtection-trackback"])
      app.data["modSpamProtection-trackback"] = new Packages.helma.util.CacheMap(64);
}


/**
 * Handle spam reported by users.
 * @param trackbackUrl reported trackback
 * @param user Identification String for user, who reported spam. e.g. username or ip-address.
 */
function reportSpam(trackbackUrl, user) {
   var trackbackHost = this.extractHostFromUrl(trackbackUrl);
   // if on blocklist, do nothing
   if (app.data["modSpamProtection-trackback-domain-block"].contains(trackbackHost))
      return;
   // if on whitelist, then ALERT
   if (app.data["modSpamProtection-trackback-domain-white"].contains(trackbackHost)) {
      // app.logger.warn();
      app.log("TRACKBACKSPAM WARN item on whitelist has been reported as spam: " + trackbackUrl + " user: " + user);
   }
   var info = this.getInfoObject(trackbackHost);
   info.users.add(user || (new Date()).md5());
   // if more than 2 users report a certain trackback as spam, than that host is put on the blocklist
   if (info.users.size() >= 2) {
      var line = info.requests + "/" + info.ips.size() + ":" + info.ips.toArray().join(",") + ":" + info.users.toArray().join(",");
      this.addToList("trackback-domain-block", trackbackHost, line);
   }
};
