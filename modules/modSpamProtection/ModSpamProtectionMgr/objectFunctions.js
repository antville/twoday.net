
// the following methods need to be implemented by extending objects
function handleSpam() {};
function track() {};
function getSpamLevel() {};
function setup() {};
function reportSpam(item, user) {};
function getLists() {};

/**
 * Adds entry to (Block/White)-list, and logs this event.
 */
function addToList(type, entry, info) {
   app.data["modSpamProtection-" + type].add(entry);
   app.log("SPAMPROTECTION INFO added " + entry + " to the " + type + "list [" + info + "]");
   var f = new Helma.File(getProtectedStaticDir("modSpamProtection"), type + ".txt");
   var line = entry + "=" + info;
   var str = ((f.exists()) ? (f.readAll() + "\n") : "") + line;
   if (f.exists()) f.remove();
   f.open();
   f.write(str);
   f.close();
};

/**
 * Reads (Block/White)-List from filesystem, and returns HashSet containing entries
 */
function getList(type) {
   var f = new Helma.File(getProtectedStaticDir("modSpamProtection"), type + ".txt");
   if (!f.exists()) {
      var bf = new Helma.File(getProtectedStaticDir("modSpamProtection"), type + ".csv.txt");
      if (bf.exists()) bf.hardCopy(f.getAbsolutePath());
   }
   var hash = new java.util.HashSet();
   var lines = (f.exists()) ? f.readAll().split("\n") : [];
   for (var i in lines) {
      if (!lines[i] || lines[i].startsWith("#")) continue;
      hash.add(lines[i].split("=")[0]);
   }
   return hash;
};

/**
 * Adds new entries fetched from remote server(s) to local (Block/White)-lists.
 */
function snycLists() {
   // TODO: sync lists with remote servers
   // e.g. http://referrercop.org/blacklist.php
};

/**
 * Helper method to extract the domain of an url, with the subdomain being cut off.
 * Will return the first three ip-octects for numerical urls
 */
function extractHostFromUrl(url) {
   if (!url || !url.startsWith("http://")) return;
   url += "/";
   var host = url.substring(7, url.indexOf("/", 7));
   if (host.match(/^[0-9\.]+$/))
      return host = host.substring(0, host.lastIndexOf("."));
   var lengthOfTld =  (host.contains(".co.") || host.contains(".com.") || host.contains(".ac.") || host.contains(".ne.")) ? 7 : 5;
   return host.substring((host.substring(0, host.length - lengthOfTld)).lastIndexOf(".") + 1);
};


function getAllLists() {
   var lists = [];
   lists = lists.concat(this.referrers.getLists());
   lists = lists.concat(this.trackbacks.getLists());
   lists = lists.concat(this.comments.getLists());
   return lists;
}
