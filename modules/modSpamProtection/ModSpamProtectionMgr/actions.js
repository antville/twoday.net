
/**
 * Renders links to all lists.
 */
function main_action() {
   var lists = this.getAllLists();
   res.write("<ul>");
   for (var i in lists) {
      res.write("<li><a href=\"" + this.href("list") + "?type=" + lists[i] + "\">" + lists[i] + "</a></li>");
   }
   res.write("</ul>");
}

/**
 * Renders a specific (Block/White)-List
 */
function list_action() {
   res.contentType = "text/plain";
   var f = new Helma.File(getProtectedStaticDir("modSpamProtection"), req.data.type + ".txt");
   if (f.exists()) {
      res.forward("modSpamProtection" + Helma.File.separator + req.data.type + ".txt");   
   }
};


/**
 * Handles spam reports by users.
 */
function reportSpam_action() {
   if (req.isPost()) {
      checkSecretKey();
      var type = req.data.type;
      if (type == "referrer")
         this.referrers.reportSpam(req.data.object, session.user.name);
      else if (type == "comment")
         ;
      else if (type == "trackback")
         ;
   }
}
