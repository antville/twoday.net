
/**
 * Permission checks (called by hopobject.onRequest())
 *
 * @param action  String, name of action
 * @param usr     User, who wants to access an action
 * @param level   Int, Membership level
 * @return Object if permission was denied <br />
 *   .message  message, if something went wrong <br />
 *   .url      recommanded redirect URL
 * @doclevel public
 */
function checkAccess(action, usr, level) {
   if (usr && usr.sysadmin) return;
   try {
      switch (action) {
         case "reportSpam" :
            checkIfLoggedIn(this.href(action));
            break;
         case "debug" :
            root.manage.checkAccessSysMgr(action);
            break;
         case "main" :
         case "list" :
            break;
         default :
            this.applyAllModuleMethods("checkModSpamProtectionMgrAccess", [action, usr, level]);
            break;
      }
   } catch (deny) {
      return {message: deny.toString(), url: (url ? url : (site ? site.href() : root.href()))};
   }
}
