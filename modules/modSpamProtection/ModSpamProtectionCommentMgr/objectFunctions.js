
/**
 * Return array of block- and white-lists.
 */
function getLists() {
   return ["comment-domain-white", "comment-domain-block", "comment-ip-white", "comment-ip-block"];
}
