
app.modules.modSpamProtection = {
   title: "spam protection",
   description: "",
   version: "",
   state: "",
   url: "http://www.twoday.net",
   author: "",
   authorEmail: "",
   authorUrl: "",
   authorOrganisation: "Knallgrau",
   license: "twoday",
   licenseUrl: "",
   licenseText: "",
   copyright: "",
   isSidebar: false,
   hasSiteSettings: false,
   skins: []
}


/**
 *
 */
app.modules.modSpamProtection.setMgrMenu = function() {
   var existingMenuItem = ROOT_MGR_MENU.getChildElement("rootadmin.modSpamProtection");
   if (!existingMenuItem) {
      var level1 = ROOT_MGR_MENU.getChildElement("rootadmin");
      level1.add("modSpamProtection", "root.manage", "modSpamProtection");
   }
   MGR_MENU_PATH.SysMgr.modSpamProtection = "rootadmin.modSpamProtection";
   MGR_MENU_PATH.SysMgr.modSpamProtectionEditList = "rootadmin.modSpamProtection";
};
