
/**
 * renders the navigationpath in the toolbar on the very top of each page
 *
 * @see global.modToolbarMacro
 * @doclevel public
 */
function modToolbarNavigationpath_macro(param) {

   // render in site navigation
   var useDropDowns = true;
   if (getBooleanInParam(param.simple)) useDropDowns = false;

   // site link
   if (this.topics.size() > 0 && useDropDowns) {
      res.write('<a id="modToolbar-topic-menu" onmouseover="openMenu(\'topic-menu\')" href="' + this.href() + '">' + this.alias + '</a>');
   } else {
      res.write('<a href="' + this.href() + '">' + this.alias + '</a>');
   }

   // image topics
   if (path.topic) {
      if (path.imagemgr && !path.layoutimagemgr) {
         if (path.imagemgr.topics.size() > 0 && useDropDowns) {
            res.write(' &gt; <a id="modToolbar-imagetopic-menu" onmouseover="openMenu(\'imagetopic-menu\')" href="' + this.href() + 'images/topics">' + getMessage("admin.imageTopics") + '</a>');
         } else {
            res.write(' &gt; <a href="' + this.href() + 'images/topics">' + getMessage("admin.imageTopics") + '</a>');
         }
         res.write(' &gt; <a href="' + path.topic.href() + '">' + path.topic.groupname + '</a>');

      } else {
         res.write(' &gt; <a href="' + path.topic.href() + '">' + path.topic.groupname + '</a>');

      }
   } else if (path.imagemgr && !path.layoutimagemgr) {
      if (path.imagemgr.topics.size() > 0 && useDropDowns) {
         res.write(' &gt; <a id="modToolbar-imagetopic-menu" onmouseover="openMenu(\'imagetopic-menu\')" href="' + this.href() + 'images/topics">' + getMessage("admin.imageTopics") + '</a>');
      } else {
         res.write(' &gt; <a href="' + this.href() + 'images/topics">' + getMessage("admin.imageTopics") + '</a>');
      }

   }

   if (res.handlers.story) {
      var story = res.handlers.story;
      var topic = res.handlers.story.topic;
      if (topic) {
         res.write(' &gt; <a href="' + this.topics.href(topic) + '">' + topic + '</a>');
      }

      story.renderSkin("modToolbarItem");

      if (story.discussions && this.discussions) {
         if (!session.user) {
            res.write(' &gt; ');
            var par = new Object();
            par.to = "login";
            par.text = getMessage("admin.story.comment");
            this.members.link_macro(par);
         } else {
            res.write(' &gt; <a href="' + story.href("comment") + '">' + getMessage("admin.story.comment") + '</a>');
         }
      }

   }

   if (this.usercontrib || res.meta.memberlevel >= CONTRIBUTOR) {
      var activeMenuItem = MGR_MENU.getChildElement("contribute");
      if (useDropDowns) {
         res.write(' - <a href="' + activeMenuItem.href() + '" id="modToolbar-contributor-menu" onmouseover="openMenu(\'contributor-menu\')">' + getMessage("admin.contributorMenu") + '</a>');
      } else {
         res.write(' - <a href="#contributormenu">' + getMessage("admin.contributorMenu") + '</a>');
      }

   }
   if (res.meta.memberlevel >= ADMIN) {
      var activeMenuItem = MGR_MENU.getChildElement("admin");
      if (useDropDowns) {
         res.write(' | <a href="' + activeMenuItem.href() + '" id="modToolbar-admin-menu" onmouseover="openMenu(\'admin-menu\')">' + getMessage("admin.adminMenu") + '</a>');
      } else {
         res.write(' | <a href="#adminmenu">' + getMessage("admin.adminMenu") + '</a>');
      }
   }
}


/**
 * renders the abo links on the right side of the toolbar on the very top of each page
 *
 * @see global.modToolbarMacro
 * @doclevel public
 */
function modToolbarRight_macro(param) {
   var site = res.handlers.site;
   if (session.user) {
      // res.write(session.user.name + ": ");
      res.write('<a id="modToolbar-account-menu" onmouseover="openMenu(\'account-menu\')" href="' + site.members.href("account") + '">' + session.user.name + '</a>');
      res.write(' - <a id="modToolbar-abo-menu" onmouseover="openMenu(\'abo-menu\')" href="' + site.members.href("updated") + '">' + getMessage("admin.abos") + '</a>');

   } else {
      var par = new Object();
      par.to = "login";
      par.text = getMessage("admin.user.login");
      site.members.link_macro(par);

   }
   return;
}


function modToolbarRenderDropdownItems(items, param) {
   Mgr.prototype.renderListItems(items, param, "modToolbarDropdown");
}


/**
 * renders the dropdown menu for contributors
 *
 * @see global.modToolbarMacro
 * @doclevel public
 */
function modToolbarContributorMenu_macro(param) {
   if (this.usercontrib || res.meta.memberlevel >= CONTRIBUTOR) {
      this.modToolbarRenderDropdown("contribute", param)
   }
}


/**
 * renders the dropdown menu for admins
 *
 * @see global.modToolbarMacro
 * @doclevel public
 */
function modToolbarAdminMenu_macro(param) {
  var site = res.handlers.site;
  if (site) {
    if (res.meta.memberlevel >= ADMIN) {
      this.modToolbarRenderDropdown("admin", param);
    }
  }
}


/**
 * renders a dropdown menu starting at activeKey
 *
 * activeKey String, menu key admin|contribute|...
 * @doclevel public
 */
function modToolbarRenderDropdown(activeKey, param) {
   var activeMenuItem = MGR_MENU.getChildElement(activeKey);
   var level = 2;

   var items = [];
   containerParam = {};
   var menuItemsLevel = activeMenuItem.getParentElement(level - 1).list();
   for (var i = 0; i < menuItemsLevel.length; i++) {
      var item = {
         isActive: menuItemsLevel[i].isParentOf(activeMenuItem),
         href: menuItemsLevel[i].href(),
         text: menuItemsLevel[i].getTitle()
      }
      items.push(item);
   }
   this.modToolbarRenderDropdownItems(items, param);
}


/**
 * renders all topics as dropdown menu
 *
 * @see global.modToolbarMacro
 * @doclevel public
 */
function modToolbarTopicMenu_macro(param) {
  if (res.handlers.site && res.handlers.site.topics.size() > 0) {
    var topicMgr = res.handlers.site.topics;
    var max = Math.min(topicMgr.size(), 16);

    for (var i = 0; i < max; i++) {
      var topic = topicMgr.get(i);
      this.renderDropdownMenuItem(topic.href(), topic.groupname);
    }
    if (topicMgr.size() > max) {
      this.renderDropdownMenuItem(res.handlers.site.href("topics"), "... " + getMessage("generic.more"));
    }
  }
}


/**
 * renders all image topics as dropdown menu
 *
 * @see global.modToolbarMacro
 * @doclevel public
 */
function modToolbarImagetopicMenu_macro(param) {
  // image topics
  if (path.imagemgr) {
    // res.write(path.imagemgr.topics.size());

    var imageTopicMgr = path.imagemgr.topics;
    var max = Math.min(imageTopicMgr.size(), 16);

    for (var i = 0; i < max; i++) {
      var topic = imageTopicMgr.get(i);
      this.renderDropdownMenuItem(topic.href(), topic.groupname);
    }
    if (imageTopicMgr.size() > max) {
      this.renderDropdownMenuItem(res.handlers.site.href("topics"), "... " + getMessage("generic.more"));
    }

  }
}


/**
 * renders the dropdown menu for abos
 *
 * @see global.modToolbarMacro
 * @doclevel public
 */
function modToolbarAboMenu_macro(param) {
  var site = res.handlers.site;
  if (site) {
    if (!res.meta.memberlevel) {
      this.renderDropdownMenuItem(site.href("subscribe"), getMessage("admin.abos.add"));
    }
    if (site.modEnotify && res.meta.memberlevel && site.members.get(session.user._id.toString()) && site.members.get(session.user._id.toString()).modEnotify == null) {
      this.renderDropdownMenuItem(site.href("modEnotify"), getMessage("admin.abos.byEmail"));
    }
  }
}


/**
 * renders the dropdown menu for abos
 *
 * @see global.modToolbarMacro
 * @doclevel public
 */
function modToolbarAccountMenu_macro(param) {
   var site = res.handlers.site;
   if (session.user && session.user.url) {
      this.renderDropdownMenuItem(session.user.url, "Home", session.user.url);
   }
   this.renderDropdownMenuItem(site.members.href("edit"), getMessage("admin.user.edit"));
   this.renderDropdownMenuItem(site.members.href("logout"), getMessage("admin.user.logout"));
}


/**
 * renders the dropdown menu item
 *
 * @param.href
 * @param.text
 * @doclevel public
 */
function modToolbarDropdownMenuItem_macro(param) {
   Mgr.prototype.renderSkin("modToolbarDropdownItem", param);
}


function renderDropdownMenuItem(link, text, title) {
   Mgr.prototype.renderSkin("modToolbarDropdownItem", {href: link, text: text, title: title});
}
