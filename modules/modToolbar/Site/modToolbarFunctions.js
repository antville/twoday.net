
/**
 * Returns the next blog in the root.publicSites-collection.
 *
 * @return Site
 */
function modToolbarGetNextSite() {
   var idx = root.publicSites.contains(this);
   if (idx == -1 || idx == root.publicSites.count() - 1 || this == root.sys_frontSite) idx = -1;
   // TODO check for display of toolbar in next site?? could be too performance-intensive
   return root.publicSites.get(idx);
}
