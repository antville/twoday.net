
/**
 * Sets Cookie which stores User Preference
 */
function modToolbarSwitch_action() {
   if (req.data.settbisclosed) {
     res.servletResponse.addHeader('Set-Cookie', 'tbisclosed=' + (req.data.settbisclosed == "1" ? 1 : 0) + '; Max-Age=' + 365*24*60*60 + '; SameSite=none; Secure; Partitioned');
   }
   if (req.data.http_referer) {
     res.redirect(req.data.http_referer);
   }

   res.data.title = "twoday toolbar settings";
   renderSkin("closePopup", getRenderRedirectParam(req.data.http_referer));
}


/**
 * Zaps to the next Site in the collection
 */
function modToolbarZap_action() {
   var idx = root.publicSites.contains(this);
   if (idx == -1 || idx == root.publicSites.count() - 1 || this == root.sys_frontSite) idx = -1;
   for (var i=idx + 1; i<root.publicSites.count(); i++) {
      var prop = root.publicSites.get(i).preferences.getProperty("modToolbarShow");
      if (prop == "none" || prop == "contributor" || prop == "admin" || prop == "collapsed") continue;
      res.redirect(root.publicSites.get(i).href() + "?ref=0");
   }
   res.redirect(this.href());
}

function checkAccessModToolbarZap(usr) {
}
