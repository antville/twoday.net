
/**
 * constructor function for ModToolbarItem objects
 * used to build the constant menu
 */
function ModToolbarItem(name, href, p, isHook) {
   this.name = name;
   this.href = href;
   this.parent = p;
   this.isHook = isHook;
   this.children = [];
}

ModToolbarItem.prototype.add = function(name, href) {
   var obj = new ModToolbarItem(name, href, this, false);
   this.children.push(obj);
   return obj;
}


ModToolbarItem.prototype.addHook = function(name) {
   var obj = new ModToolbarItem(name, href, this, true);
   this.children.push(obj);
   return obj;
}

ModToolbarItem.prototype.getChildElement = function(name) {
   var names = name.split(".");
   var obj = this;
   for (var i=0; i<names.length; i++) {
      obj = Array.getElementByKey(obj.children, names[i], "name"); // children[names[i]];
      if (obj == null) throw new Error("ModToolbarItem not found:" + name);
   }
   return obj;
}


ModToolbarItem.prototype.getParentElement = function(level) {
   if (level != null) {
      var obj = this;
      if (level > obj.getLevel) throw Error("ModToolbarItem not found");
      while (obj.getLevel() != level) obj = obj.parent;
      return obj;
   } else {
      return this.parent;
   }
}


ModToolbarItem.prototype.isParentOf = function (el) {
   var obj = el;
   while (obj && obj != this) {
      obj = obj.parent;
   }
   return (obj == this) ? true : false;
}


ModToolbarItem.prototype.getPrototype = function() {
   return "ModToolbarItem";
}


ModToolbarItem.prototype.getTitle = function() {
   return getMessage("ModToolbar.menu." + this.getFullKey());
}


ModToolbarItem.prototype.getFullKey = function () {
   var names = [];
   var obj = this;
   while (obj && obj.parent) {
      names.unshift(obj.name);
      obj = obj.parent;
   }
   return names.join(".");
}


ModToolbarItem.prototype.getLevel = function () {
   var level = 0;
   var obj = this;
   while (obj && obj.parent) {
      level++;
      obj = obj.parent;
   }
   return level;
}


ModToolbarItem.prototype.list = function () {
   return this.children;
}


var root, level1, level2, level3;
root = new ModToolbarItem("root");
level1 = root.add("admin", function() { return path.site.href("manage") });
   level2 = level1.add("site", function() { return path.site.href("manage") });
      level3 = level2.add("edit", function() { return path.site.href("edit") });
      level3 = level2.add("spamfilter", function() { return path.site.href("spamfilter") });
   level2 = level1.add("layout", function() { return res.handlers.layout.href("main") });
      level3 = level2.add("edit", function() { return res.handlers.layout.href("edit") });
         level3.add("choose", function() { return res.handlers.layout.href("choose") });
      level3 = level2.add("preferences", function() { return res.handlers.layout.href("preferences") });
      level3 = level2.add("images", function() { return res.handlers.layout.images.href("") });
      level3 = level2.add("skins", function() { return res.handlers.layout.skins.href("main") });
         level3.add("default", function() { return res.handlers.layout.skins.href("all") });
         level3.add("modified", function() { return res.handlers.layout.skins.href("modified") });
         level3.add("custom", function() { return res.handlers.layout.skins.href("custom") });
      level3 = level2.add("layouts", function() { return path.site.layouts.href("") });
   level2 = level1.add("members", function() { return path.site.members.href("") });
      level3 = level2.add("all", function() { return path.site.members.href("main") });
      level3 = level2.add("admins", function() { return path.site.members.href("admins") });
      level3 = level2.add("managers", function() { return path.site.members.href("managers") });
      level3 = level2.add("contributors", function() { return path.site.members.href("contributors") });
      level3 = level2.add("subscribers", function() { return path.site.members.href("subscribers") });
      level3 = level2.add("create", function() { return path.site.members.href("create") });
   level2 = level1.add("modules", function() { return path.site.modules.href("") });
      level3 = level2.add("all", function() { return path.site.modules.href("main") });
      level3 = level2.add("sidebar", function() { return path.site.modules.href("sidebar") });
   level2 = level1.add("stats", function() { return path.site.href("referrers") });
level1 = root.add("contribute", function() { return path.site.href("contribute") });
   level2 = level2.add("create", function() { return path.site.stories.href("create") });
   level2 = level1.add("stories", function() { return path.site.stories.href("") });
   level2 = level1.add("images", function() { return path.site.images.href("") });
   level2 = level1.add("files", function() { return path.site.files.href("") });
//   level2 = level1.add("polls", function() { return path.site.polls.href("") });
   level2 = level1.add("help", function() { return path.site.href("help") });
level1 = root.add("account", function() { return path.site.href("manage") });
TOOLBARMenu = root;
