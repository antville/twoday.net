
app.modules.modToolbar = {
   // this properties are displayed in the module manager
   // they overide properties of the module.properties file
   title:       "twoday.net toolbar",
   description: "Am oberen Rand deines Weblogs wird eine Navigationsleiste angezeigt, die Zugriff auf die wichtigsten Funktionen bietet.",
   version:     "1.1",
   state:       "beta",
   url:         "http://www.twoday.org",
   author:      "Matthias Platzer",
   authorEmail: "matthias@knallgrau.at",
   authorURL:   "http://www.knallgrau.at",
   authorOrganisation:  "knallgrau.at",
   copyright:   "(c) 2004 knallgrau.at",
   license:     "twoday",
   hasSiteSettings: true,
   skins:       []
};
// main skins: "Site.modToolbarMain","Site.modToolbarDropDowns","Site.modToolbarClosed"

app.modules.modToolbar.renderPreferences = function() {

   var options = [
      {value: "all", display: getMessage("modToolbar.preferences.showToolbarTo.all")},
      {value: "contributor", display: getMessage("modToolbar.preferences.showToolbarTo.contributor")},
      {value: "admin", display: getMessage("modToolbar.preferences.showToolbarTo.admin")},
      {value: "none", display: getMessage("modToolbar.preferences.showToolbarTo.none")}
   ]
   var param = {
      label: getMessage("modToolbar.preferences.showToolbarTo"),
      field: Html.dropDownAsString({name: "modToolbarShow"}, options, this.preferences.getProperty("modToolbarShow") ? this.preferences.getProperty("modToolbarShow") : "all")
   };
   Module.prototype.renderSetupLine(param);

};

app.modules.modToolbar.addStyleTag = function() {
   // check if toolbar is enabled?
   knallgrau.addToResponseHead(ToolBar.prototype.renderSkinAsString("styleTag"));
};

// register style
knallgrau.Event.registerObserver("Site", "beforeRenderPage", app.modules.modToolbar.addStyleTag, null, "app.modules.modToolbar.addStyleTag");
knallgrau.Event.registerObserver("Root", "beforeRenderPage", app.modules.modToolbar.addStyleTag, null, "app.modules.modToolbar.addStyleTag");
knallgrau.Event.registerObserver("Mgr", "beforeRenderMgrPage", app.modules.modToolbar.addStyleTag, null, "app.modules.modToolbar.addStyleTag");

app.modules.modToolbar.evalModulePreferences = function() {
   this.preferences.setProperty("modToolbarShow", req.data.modToolbarShow);
};

app.modules.modToolbar.evalInitialize = function() {
   return;
};

app.modules.modToolbar.renderInitialize = function() {
   this.renderSetupSpacerLine();

   this.renderSetupHeader({header: getMessage("modToolbar.title")});
};
