
/**
 * renders the toolbar on the very top of each page
 *
 * @param excludeClosed
 * @param excludeDropdowns
 *
 * @skin Site.modToolbarMain
 * @skin Site.modToolbarClosed
 * @doclevel public
 */
function modToolbar_macro(param) {
   if (res.handlers.site) {
      var setting = res.handlers.site.preferences.getProperty("modToolbarShow");
      if (setting == "none") return;
      if (setting == "contributor" && !(res.handlers.site.usercontrib || res.meta.memberlevel >= CONTRIBUTOR)) return;
      if (setting == "admin" && !(res.meta.memberlevel >= ADMIN)) return;
      if (req.data.tbisclosed == "1" || setting == "collapsed") {
         if (!param.excludeClosed) {
            res.handlers.site.renderSkin("modToolbarClosed", param);
         }
      } else {
         res.handlers.site.renderSkin("modToolbarMain", param);
      }
   }
   if (!param.excludeDropdowns) {
      modToolbarDropdowns_macro(param);
   }
}


/**
 * renders the dropdowns for the toolbar
 * include this macro in your layout if you use the excludeDropdowns param
 * at the modToolbar_macro
 * This allows you to place the dropdown code to another position in your HTML
 * For example at the end, after the more important content
 *
 * @skin Site.modToolbarDropDowns
 * @doclevel public
 */
function modToolbarDropdowns_macro(param) {
   if (res.handlers.site) {
      var setting = res.handlers.site.preferences.getProperty("modToolbarShow");
      if (setting == "none") return;
      if (setting == "contributor" && !(res.handlers.site.usercontrib || res.meta.memberlevel >= CONTRIBUTOR)) return;
      if (setting == "admin" && !(res.meta.memberlevel >= ADMIN)) return;
      if (req.data.tbisclosed == "1" || setting == "collapsed") return;
      res.handlers.site.renderSkin("modToolbarDropDowns", param);
   }
}


/**
 * renders a link to twoday.net in the toolbar path
 *
 * @text   text to be displayed
 * @title  text to be used as title
 * @doclevel public
 */
function modToolbarRootLink_macro(param) {
   if (!param.to)
      param.to = root.href();
   if (!param.text)
      param.title = root.getTitle();
   Html.openTag("a", root.createLinkParam(param));
   res.write(param.text || param.title || param.to);
   Html.closeTag("a");
   return;
}


/**
 * renders the closed toolbar
 * include this macro in your layout if you use the excludeClose param
 * at the modToolbar_macro
 * This allows you to place the closed toolbar to another position in your
 * layout. For example in the sidebars
 *
 * @skin modToolbarClosed
 * @doclevel public
 */
function modToolbarClosed_macro(param) {
   if (res.handlers.site) {
      var setting = res.handlers.site.preferences.getProperty("modToolbarShow");
      if (setting == "none") return;
      if (setting == "contributor" && !(res.handlers.site.usercontrib || res.meta.memberlevel >= CONTRIBUTOR)) return;
      if (setting == "admin" && !(res.meta.memberlevel >= ADMIN)) return;
      if (req.data.tbisclosed == "1" || setting == "collapsed")
         res.handlers.site.renderSkin("modToolbarClosed", param);
   }
}
