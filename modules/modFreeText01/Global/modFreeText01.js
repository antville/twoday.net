
app.modules.modFreeText01 = {
   title: "Free Text (1)",
   description: "renders freely definable HTML Text",
   version: "1.0",
   state: "stable",
   url: "",
   author: "Wolfgang Kamir",
   authorEmail: "wolfgang@knallgrau.at",
   authorUrl: "http://www.knallgrau.at",
   authorOrganisation: "knallgrau.at",
   copyright: "(c) 2004 knallgrau.at",
   license: "twoday",
   licenseUrl: "http://www.twoday.org",
   isSidebar: true,
   hasSiteSettings: true
};


/**
 * Reads userdefined free HTML text from site's preferences and hands it over to "modFreeText01" skin as parameter.
 * renderSidebarModuleItem function displays the module in sidebar (+ header).
 *
 * @see Site.renderSidbarModuleItem
 */
app.modules.modFreeText01.renderSidebarItem = function(param) {
   var p = {};
   p.body = this.preferences.getProperty("modfreetext01text") ? this.preferences.getProperty("modfreetext01text") : "";
   p.editLink = true;
   this.renderSidebarModuleItem("modFreeText01", p);
   return;
}


/**
 * Renders the module's preferences.
 */
app.modules.modFreeText01.renderPreferences = function() {
   var param = {};
   param.label = getMessage("modFreeText01.wizard.admin.settingsText");
   param.value = this.preferences.getProperty("modfreetext01text") ?
                 this.preferences.getProperty("modfreetext01text") : "";
   Module.prototype.renderSkin("modFreeText01SetupLine", param);
};


/**
 * Stores the userdefined free HTML text in the site's prefernces.
 */
app.modules.modFreeText01.evalModulePreferences = function() {
   this.preferences.setProperty("modfreetext01text", req.data.modFreeText01Text);
};
