
app.modules.modWebCounter = {
   title: "Web Counter Module",
   description: "Integrate External Webcounter-Counter Tools (Blogcounter, Sitemeter, and Blogstats)",
   version: "1.0",
   state: "alpha",
   url: "",
   author: "Barbara Ondrisek",
   authorEmail: "barbara.ondrisek@knallgrau.at",
   authorUrl: "",
   authorOrganisation: "Knallgrau",
   license: "twoday",
   licenseUrl: "",
   licenseText: "",
   copyright: "",
   isSidebar: true,
   type: MODULE_TYPE_INFO,
   hasSiteSettings: false,
   skins: []
}


app.modules.modWebCounter.renderSidebarItem = function(param) {
   var p = {};
   p.editLink = true;
   res.push();
   if (this.preferences.getProperty("modWebCounterTypeBlogcounter") && this.preferences.getProperty("modWebCounterTypeBlogcounter_settings")) {
      Module.prototype.renderSkin("modWebCounterBlogcounter", {username: this.preferences.getProperty("modWebCounterTypeBlogcounter_settings")});
      Html.tag("br");
   }
   if (this.preferences.getProperty("modWebCounterTypeSitemeter") && this.preferences.getProperty("modWebCounterTypeSitemeter_settings")) {
      Module.prototype.renderSkin("modWebCounterSitemeter", {siteName: this.preferences.getProperty("modWebCounterTypeSitemeter_settings")});
      Html.tag("br");
   }
   if (this.preferences.getProperty("modWebCounterTypeBlogstats") && this.preferences.getProperty("modWebCounterTypeBlogstats_settings")) {
      Module.prototype.renderSkin("modWebCounterBlogstats", {id: this.preferences.getProperty("modWebCounterTypeBlogstats_settings"), url: this.url || this.href()});
      Html.tag("br");
   }
   if (this.preferences.getProperty("modWebCounterTypeOther") && this.preferences.getProperty("modWebCounterTypeOther_settings")) {
      Module.prototype.renderSkin("modWebCounterOther", {html: this.preferences.getProperty("modWebCounterTypeOther_settings")});
      Html.tag("br");
   }
   p.body = res.pop();
   this.renderSidebarModuleItem("modWebCounter", p);
}

app.modules.modWebCounter.renderPreferences = function() {
   Module.prototype.renderSkin("modWebCounterSetup");
}


app.modules.modWebCounter.evalModulePreferences = function() {
   this.preferences.setProperty("modWebCounterTypeBlogcounter", req.data.preferences_modWebCounterTypeBlogcounter);
   this.preferences.setProperty("modWebCounterTypeSitemeter", req.data.preferences_modWebCounterTypeSitemeter);
   this.preferences.setProperty("modWebCounterTypeBlogstats", req.data.preferences_modWebCounterTypeBlogstats);
   this.preferences.setProperty("modWebCounterTypeOther", req.data.preferences_modWebCounterTypeOther);

   this.preferences.setProperty("modWebCounterTypeBlogcounter_settings", req.data.preferences_modWebCounterTypeBlogcounter_settings);
   this.preferences.setProperty("modWebCounterTypeSitemeter_settings", req.data.preferences_modWebCounterTypeSitemeter_settings);
   this.preferences.setProperty("modWebCounterTypeBlogstats_settings", req.data.preferences_modWebCounterTypeBlogstats_settings);
   this.preferences.setProperty("modWebCounterTypeOther_settings", req.data.preferences_modWebCounterTypeOther_settings);
}


/**
 * hook into Site.mostread_action
 */
app.modules.modWebCounter.renderMostRead = function() {
   res.write(getMessage("modWebCounter.mostread.text", {linkTo: this.modules.href("preferences") + "?mod=modWebCounter"}));
   Html.tag("br");
   Html.tag("br");
}

/**
 * hook into Site.mostread_action
 */
app.modules.modWebCounter.renderMostReadSidebar = function() {
   res.write(getMessage("modWebCounter.mostread.text", {linkTo: this.modules.href("preferences") + "?mod=modWebCounter"}));
}
