
/**
 * Check if a user is allowed to access the modWatchingSearch-action of this Site.
 *
 * @param usr User
 * @throws DenyException
 */
function checkAccessModWatchingSearch(usr) {
   this.checkAccessManage(usr);
   return;
}
