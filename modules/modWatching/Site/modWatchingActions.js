
/**
 * If the user clicks on the "search on amazon" button this function renders some hidden fields that are filled
 * with the results of the amazon booksearch. urlparameters are 'title' and 'author' of the book to search for.
 * The resulting skin is rendered to a hidden iframe in the skin "modWatchingSetup". In the onLoad function of the iframe, the values are written
 * out as list of links (search result links).
 */
function modWatchingSearch_action() {
   var amazon = new knallgrau.Amazon();
   
   if (req.data.dvd == "true") {
      // search results from dvd
      var result = amazon.searchDVD((req.data.title || ""));
   } else {
      // search results from vhs
      var result = amazon.searchVHS((req.data.title || ""));
   }
   
   default xml namespace = "http://webservices.amazon.com/AWSECommerceService/2005-10-05";
   var items = result..Item;

   if (items.length() == 0) {
      res.write("<div class=\"message\">" + getMessage("modWatching.modWatchingSetup.NoResults") + "</div>");
      return;
   } else {
      res.write("<div class=\"message\">" + getMessage("modWatching.modWatchingSetup.result.intro") + "</div>");
   }

   res.write("<h3>" + getMessage("modWatching.modWatchingSetup.TopResultsGerman") + "</h3>");
   for (var i = 0; i < items.length(); i++) {
      var url = items[i].DetailPageURL.text();
      var title = items[i].ItemAttributes.Title.text();
      var image = items[i].MediumImage.URL.text();
      title = title.toString().replace("'", "&acute;");
      res.write("<ul class=\"searchResultList\">\n");
      res.write('<li><a href="' + url + '" target="_blank" onclick="addItem(\''+url+'\', \''+title+'\', \''+image+'\'); return false;">' + title + '</a></li>\n');
      res.write("</ul>\n");
   }
   res.write("<br />\n");
   default xml namespace = "http://www.w3.org/1999/xhtml";
   return;
}
