
app.modules.modWatching = {
   title: "Watching",
   description: "Displays a list of the movies you are currently watching.",
   version: "",
   state: "beta",
   url: "",
   author: "Wolfgang Kamir",
   authorEmail: "wolfgang@knallgrau.at",
   authorUrl: "http://www.knallgrau.at",
   authorOrganisation: "knallgrau.at",
   copyright: "(c) 2004 knallgrau.at",
   license: "antville",
   licenseUrl: "http://www.antville.org",
   isSidebar: true,
   type: MODULE_TYPE_LIST,
   skins: ["Site.modWatching", "Site.modWatchingItem", "Site.modWatchingDivider"]
};


/**
 * Renders module in the sidebar. The renderSidebarItem function of modWatching at first reads exisiting
 * booklist entries from the site's preferences.
 * After reading the items and splitting them up, for every item the "modWatchingItem" skin is rendered and inbetween the skin "modWatchingDivider".
 * Linked alltogether the resulting string is handed over to the skin "modWatching" as body and rendered to the sidebar.
 */
app.modules.modWatching.renderSidebarItem = function(param) {
   if (!param) param = {};
   var WatchingTitles = app.modules.modWatching.getWatchingProperty(this, "titles");
   var WatchingUrls = app.modules.modWatching.getWatchingProperty(this, "urls");
   var WatchingImages = app.modules.modWatching.getWatchingProperty(this, "images");

   res.push();
   for (var i=0; i<WatchingTitles.length; i++) {
      var p = {};
      p.WatchingTitle = (WatchingUrls[i].length > 12) ? '<a href="' + WatchingUrls[i] + '" target="_blank">' + WatchingTitles[i] + '</a>' : WatchingTitles[i];
      var Img = this.images.get(WatchingImages[i]);
      if (Img) {
         p.WatchingImage = (WatchingUrls[i].length > 12) ? '<a href="' + WatchingUrls[i] + '" target="_blank">' + Img.getUrl() + '</a>' : '<img src="' + Img.getUrl() + '" />';
      }
      else if (WatchingImages[i].length > 12) {
         p.WatchingImage = (WatchingUrls[i].length > 12) ? '<a href="' + WatchingUrls[i] + '" target="_blank"><img src="' + WatchingImages[i] + '" /></a>' : '<img src="' + WatchingImages[i] + '" />';
      }
      else {
         p.WatchingImage = "";
      }
      if (i != 0) this.renderSkin("modWatchingDivider");
      this.renderSkin("modWatchingItem", p);
   }
   var p = {};
   p.Watching = res.pop();
   res.push();
   this.renderSkin("modWatching", p);
   var p = {};
   p.body = res.pop();
   p.editLink = true;
   this.renderSidebarModuleItem("modWatching", p);
}


/**
 * Generates the arranger-field of the module's preferences and hands it over to the "modWatchingSetup" skin.
 */
app.modules.modWatching.renderPreferences = function() {
   Module.prototype.renderSetupSpacerLine();
   var p = {};
   p.WatchingArranger = app.modules.modWatching.renderWatchingArranger(this);
   Module.prototype.renderSkin("modWatchingSetup", p);
};


/**
 * Is called when user clicks the "save" button in module preferences. The values in the item-arranger are read and split up in title, author, url, image.
 * In the arranger itself the items are saved as strings separated by the sequence ":#:". After pushing the values into an array, they are stored in the
 * site's preferences by calling toSource array function.
 */
app.modules.modWatching.evalModulePreferences = function() {
   var WatchingTitles = new Array();
   var WatchingUrls = new Array();
   var WatchingImages = new Array();

   try {
      var WatchingArrangerValues = JSON.parse(req.data.WatchingArrangerValues);
   } catch (err) {
      return;
   }
   if (!WatchingArrangerValues)
      var WatchingArrangerValues = new Array();

   WatchingArrangerValues = WatchingArrangerValues.slice(0,20);

   for (var i=0; i<WatchingArrangerValues.length; i++) {
      var temp = WatchingArrangerValues[i].split(":#:");
      WatchingTitles.push(stripTags(temp[0]));
      WatchingUrls.push(stripTags(temp[1]));
      WatchingImages.push(stripTags(temp[2]));
   }

   this.preferences.setProperty("modwatching_titles", WatchingTitles.toSource());
   this.preferences.setProperty("modwatching_urls", WatchingUrls.toSource());
   this.preferences.setProperty("modwatching_images", WatchingImages.toSource());
};


/**
 * Gets an array containing the titles, urls, authors, images of all list items. if nothing is found, an empty array is returned.
 *
 * @return Array containing either titles, urls, authors or images
 */
app.modules.modWatching.getWatchingProperty = function(obj, name) {
   try {
      var arr = JSON.parse(obj.preferences.getProperty("modwatching_" + name));
   } catch (err) {
      ;
   }
   if (!arr)
      var arr = new Array();

   return arr;
};


/**
 * Creates the item arranger field for module's preferences. First all existing items are read, then pushed into an array that is used to create
 * a value-filled dropDown-List. This List is handed over to the skin "modWatchingArranger", which also consists of three buttons to remove and reposition
 * items in the list.
 */
app.modules.modWatching.renderWatchingArranger = function(obj) {
   var WatchingTitles = app.modules.modWatching.getWatchingProperty(obj, "titles");
   var WatchingUrls = app.modules.modWatching.getWatchingProperty(obj, "urls");
   var WatchingImages = app.modules.modWatching.getWatchingProperty(obj, "images");

   var WatchingValues = [];
   for (var i=0; i<WatchingTitles.length; i++) {
      WatchingValues.push({value: WatchingTitles[i] + ":#:" + WatchingUrls[i] + ":#:" + WatchingImages[i], display: WatchingTitles[i]});
   }

   var p = {};
   p.WatchingArranger = Html.dropDownAsString({name: "WatchingArranger", id: "WatchingArranger", size: 12, multiple: "multiple", style: "width:440px;", onchange: "getItem(); return false;"}, WatchingValues);

   return Module.prototype.renderSkinAsString("modWatchingArranger", p);
};