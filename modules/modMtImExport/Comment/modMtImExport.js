
/**
 * renders the complete exportstring of the current comment (with replies)
 * @param Date the date exported comments will be marked
 * @param Date export only stories after this date, to export all stories set this param to null
 * @return the exportString of the Story or "" if the story is not going to be exported
 */
function modMtImExportGetExportString() {
  // render replies
  res.push();
  for(var i = 0; i < this.size(); i++) {
    this.get(i).renderSkin("modMtImExportReply");
  }
  
  return this.renderSkinAsString("modMtImExportComment", { replies: res.pop() });
}
