
/**
 * check if a user is allowed to export a site
 * @param Obj Userobject
 * @param Int Permission-Level
 */
function checkModMtImExportExport(usr, level) {
   // if not logged in or not logged in with sufficient permissions
   if (!usr || (level < ADMIN && !usr.sysadmin))
      throw new DenyException("modMtImExport.siteExport");
   return;
}
