
/**
 * this action performs a MT compatible Content Export
 */
function modMtImExport_action() {
   res.handlers.mgr = this.stories;
   var skinFileName = "modMtImExport";
   var resAction = this.href(req.action);
   var resParams = {};

   // do the export
   if (req.data.submitExport) {
      try {
         res.message = this.modMtImExportEvalExport(req.data, session.user);
      } catch (e) {
         //res.debug("error:" + e.fileName + ": line: " + e.lineNumber);
         res.message = e.toString();
      }

   // do the import
   } else if (req.data.submitImport) {
      try {
         res.message = this.modMtImExportEvalImport(req.data, session.user);
      // on error display an intermediate page
      } catch (e) {
         session.data["modMtImExportData"] = {
            storyDatas: e.storyDatas,
            userImportMode: e.userImportMode
         };
         var errorListString = (e.errorList)
            ? "<ul><li>" + e.errorList.join("</li><li>") + "</li></ul>"
            : e.toString();
         res.data.title = 
            getMessage("modMtImExport.header", {siteTitle: this.title});
         res.data.action = this.href(req.action);
         res.data.body = this.renderSkinAsString(
            "modMtImExportImportWithErrors", { errorList: errorListString });
         this.stories.renderMgrPage();
         //res.debug("error:" + e.fileName + ": line: " + e.lineNumber);
         return;
      }   
      
   // do the import (and ignore all errors)
   } else if (req.data.submitImportWithErrors) {
      var cachedObj = session.data["modMtImExportData"];
      if (!cachedObj) {
         res.message = getMessage("modMtImExport.import.noDataInCache");
      } else {
         try {
            res.message = this.modMtImExportSave(
               cachedObj.storyDatas,
               cachedObj.userImportMode,
               session.user
            );
         } catch (e) {
            res.message = e.toString();
            //res.debug("error:" + e.fileName + ": line: " + e.lineNumber);
         }
         // garbage collect
         session.data["modMtImExportData"] = null;
      }
   }

   // process response
   res.data.title = getMessage("modMtImExport.header", {siteTitle: this.title});
   res.data.action = resAction;
   // sidebar hack ?
   res.data.sidebar01 = this.renderSkinAsString("modMtImExportSidebar", {
      changeSkinsBody: getMessage("modMtImExport.sidebar.changeSkinsBody", {
         changeSkinHref: this.modules.href("skins") + "?mod=modMtImExport" })
   });
   var s = new Story();
   s.discussions = 1;
   res.handlers.newemptystory = s;
   res.data.body = this.renderSkinAsString(skinFileName, resParams);
   this.stories.renderMgrPage();
}
