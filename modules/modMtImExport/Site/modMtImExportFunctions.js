
/**
 * Performs the export
 *
 * @param param      Object data, provided by the export form (param)
 * @param creator   User    who performs the export, e.g. session.user
 * @throws Exception
 * @return Message
 */
function modMtImExportEvalExport(param, creator) {
   var lastExportTime = this.modMtImExportGetLastExportTime();
   var now = new Date();
   var content = ""; 
   var exportMethod = 
      (param.exportfrom == "lastexport" && lastExportTime != null)
         ? "update"
         : "full";

   var fileParams = {
      contenttype: "text/plain",
      alias: now.format("yyyy-MM-dd-", this.getLocale(), this.getTimeZone()) + "export-" + exportMethod + ".txt",
      description: getMessage("modMtImExport.export."+exportMethod+".fileDescription")   
   };

   // create file content
   res.push();
   for (var i = 0; i < this.allstories.size(); i++) {
      res.write(
         this.allstories.get(i).modMtImExportGetExportString(
            (param.exportrange == "all"),
            (exportMethod == "update") ? lastExportTime : null
         )
      );
   }
   // check for errors
   if(!(content = res.pop())) {
      throw new Exception("modMtImExport.nothingToExport");
   }
   
   // write the file to disk
   fileParams.rawfile = new Packages.helma.util.MimePart(
      fileParams.alias + ".txt",
      new java.lang.String(content).getBytes("UTF-8"),
      fileParams.contenttype
   );
   var newFileMsg = path.site.files.evalFile(fileParams, creator);
   
   // update module Properties
   this.modMtImExportSetLastExportTime(now);
   this.modMtImExportSetLastExporter(creator);
   res.push();
   Html.link({href: newFileMsg.obj.getUrl()}, newFileMsg.obj.alias)
   var exportedFileLink = res.pop();

   return getMessage("confirm.modMtImExport.export", {link: exportedFileLink});
}


/**
 * Performs the import
 *
 * @param param      Object data, provided by the import form (param)
 * @param creator   User    who performs the export, e.g. session.user
 * @throws Exception
 * @return Message
 */
function modMtImExportEvalImport(param, creator) {
   if (param.filetoimport == null || param.filetoimport.contentLength == 0) {
      throw new Exception("modMtImExport.emptyFile");
   }
   
   // define some variables
   var typeRegExp = /^([A-Z]+):\ *\n/; // pattern for storyPart type
   var emptyContentRegExp = /^[\t\n ]*$/; // pattern for empty content
   var importedText = param.filetoimport.getText();
   var errorList =[];
   
   // create string from raw-content if contentType is unsupported
   // (i.e. the file has no extension)
   if (importedText == null) {
      try {
         // we guess the user import a file exported in twoday and this is UTF-8
         importedText = new java.lang.String(
            param.filetoimport.getContent(), "UTF-8");
         importedText += ""; // convert to JS String obj
      } catch (e) {
         throw new Exception("modMtImExport.invalidContentType");
      }
   }

   // substitute different newline formats to \n
   var removeNL1RegExp = new RegExp(["\r\n"], ["g"]);
   var removeNL2RegExp = new RegExp(["\r"], ["g"]);
   importedText = importedText.replace(removeNL1RegExp, "\n");
   importedText = importedText.replace(removeNL2RegExp, "\n"); 

   var lines = importedText.match(/\n/g);
   if (lines.length > 10500) {
      throw new Exception("modMtImExport.fileTooBig");
   }

   // fetch parameters
   var importDiscussions = (param.importrange == "all");
   var userImportMode = 
      (param.userImportMode == "asCreator" && creator.trusted)
         ? "asCreator"
         : "asAuthor";

   // check story-overriding parameters
   var topicName = null;
   var addToFront = null;
   var editableby = null;
   var allowDiscussions = null;
   if (param.overrideimportoptions != null) {
      // check name of topic (if specified)
      if (param.topic) {
         topicName = param.topic;
      } else if(param.addToTopic) {
         topicName = param.addToTopic;
      }
      // check add stories to Frontpage
      addToFront = (param.addToFront != null);
      // check editableby priviliges
      editableby = param.editableby;
      // check allow discussions flag
      allowDiscussions = (param.discussions != null);
   }
   
   // create a default story data object
   var defaultStoryData = {
      publish: true,
      addToFront: addToFront,
      editableby: editableby,
      topic: topicName,
      discussions: allowDiscussions,
      http_remotehost: param.http_remotehost,
      createtime: null,
      content_title: null,
      content_text: null,
      author: null,
      email: null,
   }
   
   // parse file
   
   // get all stories
   var storyDatas = [];
   var stories = importedText.split(/\n--------(\n|\z)/);
      
   // process each story
   for (var i=0; i<stories.length; i++) {
      // check if the there is some content
      if (stories[i].length == 0) continue;
   
      // define some things
      var storyParts = stories[i].split(/\n-----(\n|\z)/);
      var storyData = null;
      var commentDatas = [];
      
      for(var j=0; j<storyParts.length; j++) {
         var storyPart = storyParts[j];
         // check if there is some content
         if (storyPart.match(emptyContentRegExp)) continue;
         
         // define some things
         var storyPartAlreadyParsed = false;
         
         // check if storyPart is a body, comment or reply
         var TypeOfStoryPart = null;
         if (TypeOfStoryPart = storyPart.match(typeRegExp)) {
            switch (TypeOfStoryPart[1]) {
               case "BODY":
                  // check for error
                  if (storyData == null) {
                     errorList.push(
                        getMessage("modMtImExport.import.foundBodyWithoutStory"));
                     break;
                  }
                  storyData.content_text = 
                     storyPart.substring(storyPart.indexOf("\n") + 1);
                  storyPartAlreadyParsed = true;
                  break;
               case "REPLY": // to be compatible with the old export-format
               case "COMMENT":
                  // check for error
                  if (storyData == null) {
                     errorList.push(
                        getMessage("modMtImExport.import.foundCommentWithoutStory"));
                     break;
                  }
                  if (importDiscussions) {
                  
                     // check if the storyPart is a reply
                     replyStart = storyPart.indexOf("REPLY:");
                     if (replyStart == (storyPart.indexOf("\n") + 1)) {
                        var tmpObj = this.modMtImExportParseComment(
                           storyPart.substring(storyPart.indexOf("\n", replyStart)+1));
                        // add reply in comment
                        commentDatas[commentDatas.length - 1].replyDatas.push(
                           tmpObj.commentData);
                        // update errorList
                        errorList = errorList.concat(tmpObj.errorList);
                        
                     // check if the storyPart is an 'old' reply
                     } else if (TypeOfStoryPart[1] == "REPLY") {
                        var tmpObj = this.modMtImExportParseComment(
                           storyPart.substring(storyPart.indexOf("\n") + 1));
                        // add reply in comment
                        commentDatas[commentDatas.length - 1].replyDatas.push(
                           tmpObj.commentData);
                        // update errorList
                        errorList = errorList.concat(tmpObj.errorList);
                        
                     // storyPart is a comment
                     } else {
                        var tmpObj = this.modMtImExportParseComment(
                           storyPart.substring(storyPart.indexOf("\n") + 1));
                        tmpObj.commentData.replyDatas = [];
                        // add comment
                        commentDatas.push(tmpObj.commentData)
                        // update errorstring
                        errorList = errorList.concat(tmpObj.errorList);
                     }
                     
                  }
                  storyPartAlreadyParsed = true;
                  break;
            }
         }

         // otherwise the storyPart must contain the story params
         if (!storyPartAlreadyParsed && storyData == null) {
            storyPartAlreadyParsed = true;
            var tmpObj = this.modMtImExportParseStory(
               storyPart, Object.clone(defaultStoryData));
            storyData = tmpObj.storyData;
            errorList = errorList.concat(tmpObj.errorList);
         }
         
         // check if the story was sucessfully parsed
         if (!storyPartAlreadyParsed) {
            errorList.push(getMessage(
               "modMtImExport.import.unknownPart",
               { details: storyPart.substring(0, storyPart.indexOf("\n") - 1) }
            ));
         }
      }
      
      // store comments in story, and add the story to the storyArray
      if (storyData) {
         storyData.commentDatas = commentDatas;
         storyDatas.push(storyData);
      }
   }

   // return on error
   if (errorList.length) {
      throw {
         errorList: errorList,
         storyDatas: storyDatas,
         userImportMode: userImportMode
      };
   }
   
   // save in db and return   
   return this.modMtImExportSave(storyDatas, userImportMode, creator);
}


/**
 * Parse a Story
 *
 * @param String      contains a string with the story-params
 * @param Obj   contains the default storyData obj (see in code)
 * @return Obj   contains two params: storyData and errorList
 */
function modMtImExportParseStory(storyString, storyData) {
   // define some variables
   var paramRegExp = /^([A-Z]+): *(.*) */; // pattern for parameter:value
   var emptyContentRegExp = /^[\t\n ]*$/; // pattern for empty content
   var errorList = [];
   var storyLines = storyString.split(/\n/);
   for (var i=0; i<storyLines.length; i++) {
      // check for empty content
      if (storyLines[i].match(emptyContentRegExp)) continue;
      // parse storyLine
      var paramOfStoryLine = null;
      if (paramOfStoryLine = storyLines[i].match(paramRegExp)) {
         switch (paramOfStoryLine[1]) {
            case "TITLE":
               if (storyData.content_title == null)
                  storyData.content_title = paramOfStoryLine[2];
               break;
            case "CATEGORY":
               if (storyData.topic == null)
                  storyData.topic = paramOfStoryLine[2];
               break;
            case "SHOWONFRONT":
               if (storyData.addToFront == null)
                  storyData.addToFront = (paramOfStoryLine[2] == "yes");
               break;
            case "EDITABLEBY":
               if (storyData.editableby == null)
                  storyData.editableby = paramOfStoryLine[2];
               break;
            case "DISCUSSIONS":
               if (storyData.discussions == null)
                  storyData.discussions = (paramOfStoryLine[2] == "yes");
               break;
            case "DATE":
               if (storyData.createtime == null) {
                  var createDate = new Date;
                  createDate.setTime(Date.parse(paramOfStoryLine[2]));
                  storyData.createtime = createDate.format("yyyy-MM-dd HH:mm:ss");
               }
               break;
            case "AUTHOR":
               if (storyData.author == null)
                  storyData.author = paramOfStoryLine[2];
               break;
            case "EMAIL":
               if (storyData.email == null)
                  storyData.email = paramOfStoryLine[2];
               break;
            case "STATUS":
               if (paramOfStoryLine[2] == "Draft")
                  storyData.publish = 0;
               break;
            case "ALLOW COMMENTS":
               break;
            default:
               errorList.push(
                  getMessage("modMtImExport.import.unknownParam",
                     { paramName: paramOfStoryLine[1] })
               );
               break;
         }
      } else {
         errorList.push(
            getMessage("modMtImExport.import.unknownParam",
            { paramName: storyLines[i] })
         );
      }
   }
   
   return {
      storyData: storyData,
      errorList: errorList
   };
}


/**
 * Parse a Comment
 *
 * @param String      contains a string with the comment-params
 * @return Obj   contains two params: commentData and errorList
 */      
function modMtImExportParseComment(commentString) {
   // define some vars
   var paramRegExp = /^([A-Z]+): *(.*) *$/; // pattern for parameter:value
   var errorList = [];
   var commentData = {
      content_title: null,
      content_text: "",
      createtime: null,
      author: null,
      email: null
   };   
   var commentLines = commentString.split(/\n/);
   for (var i=0; i<commentLines.length; i++) {
      var commentLine = commentLines[i];
      var paramOfCommentLine = null;
      if (paramOfCommentLine = commentLine.match(paramRegExp)) {
         switch (paramOfCommentLine[1]) {
            case "TITLE":
               if (commentData.content_title == null)
                  commentData.content_title = paramOfCommentLine[2];
               break;
            case "DATE":
               if (commentData.createtime == null) {
                  var createDate = new Date;
                  createDate.setTime(Date.parse(paramOfCommentLine[2]));
                  commentData.createtime = createDate.format("yyyy-MM-dd HH:mm:ss"); 
               }
               break;
            case "AUTHOR":
               if (commentData.author == null)
                  commentData.author = paramOfCommentLine[2];
               break;
            case "EMAIL":
               if (commentData.email == null)
                  commentData.email = paramOfCommentLine[2];
               break;
            case "URL":
               if (commentData.url == null)
                  commentData.url = paramOfCommentLine[2];
               break;
            case "IP":
               if (commentData.http_remotehost == null)
                  commentData.http_remotehost = paramOfCommentLine[2];
               break;
            default:
               errorList.push(getMessage(
                  "modMtImExport.import.unknownCommentParam", {
                     paramName: paramOfCommentLine[1],
                     content_title: commentData.content_title
                  }
               ));
               commentData.content_text += commentLine + "\n";
               break;
         }
      } else {
         commentData.content_text += commentLine + "\n";
      }
   }
   
   return {
      commentData: commentData,
      errorList: errorList
   };
}


/**
 * find the user who ownes the story/comment/reply
 *
 * @param String with the user-import-mode ("asAuthor", "asCreator")
 * @param Obj   a story or comment data obj (see in code)
 * @return User
 */
function modMtImExportFindCreator(userImportMode, dataObj, creator) {
   if (userImportMode == "asAuthor") return creator;
   var origCreator = root.users.get(dataObj.author);
   if (origCreator) return origCreator;
   return creator;
}


/**
 * store story/comment/reply dataObjs in DB
 *
 * @param Array of all storyDatas
 * @param String with the user-import-mode ("asAuthor", "asCreator")
 * @return String with import-details (and eventual errors)
 */
function modMtImExportSave(storyDatas, userImportMode, creator) {
   var errorList = [];
   var importedStories = 0;
   var importedComments = 0;
   
   for(var iStory = 0; iStory < storyDatas.length; iStory++) {
      var storyData = storyDatas[iStory];
      // try to store story, on success continue with the comments
      var storyObj = null;
      try {
         storyObj = this.stories.evalNewStory(
            storyData,
            this.modMtImExportFindCreator(userImportMode, storyData, creator)
         ).obj;
         // set original creator
         if (storyObj.creator.name != storyData.author) {
            storyObj.creatorName = storyData.author;
            storyObj.creatorEmail = storyData.email;
            storyObj.creatorUrl = storyData.url;
         }
      } catch (e) {
         errorList.push(getMessage(
            "modMtImExport.import.failedCreateStory",
            { details: e.toString() }
         ));
         continue;
      }
      
      // increment imported stories count to display it later to the user
      importedStories++;
      
      // are there comments?
      if (storyData.commentDatas.length == 0) continue;      
      // if yes, then store them
      for (var iComment=0; iComment<storyData.commentDatas.length; iComment++) {
         var commentData = storyData.commentDatas[iComment];
         var commentResult = storyObj.evalComment(
            commentData,
            this.modMtImExportFindCreator(userImportMode, commentData, creator)
         );
         if (commentResult.error) {
            errorList.push(getMessage(
               "modMtImExport.import.failedCreateComment",
               { title: commentData.content_title }
            ));
            continue;
         }
         var commentObj = storyObj.get(commentResult.id);
         // set original creator
         if (commentObj.creator.name != commentData.author) {
            commentObj.creatorName = commentData.author;
            commentObj.creatorEmail = commentData.email;
         }
         // increment imported comments count to display it later to the user
         importedComments++;
         // if no error, are there replies?
         if (commentData.replyDatas.length == 0) continue;
         // if yes, then store them
         for (var iReply = 0; iReply < commentData.replyDatas.length; iReply++) {
            var replyData = commentData.replyDatas[iReply];
            var replyResult = commentObj.evalComment(
               replyData, 
               this.modMtImExportFindCreator(userImportMode, replyData, creator)
            );
            if (replyResult.error) {
               errorList.push(getMessage(
                  "modMtImExport.import.failedCreateReply",
                  { title: replyData.content_title }
               ));
               continue;
            }
            var replyObj = commentObj.get(replyResult.id);
            // set original creator
            if (replyObj.creator.name != replyData.author) {
               replyObj.creatorName = replyData.author;
               replyObj.creatorEmail = replyData.email;
            }
            // increment imported comments count to display it later to the user
            importedComments++;
         }
      }
   }

   // create return message
   var message = getMessage("modMtImExport.import.importDone", {
      storyCount: importedStories + "", commentCount: importedComments + ""});
   if (errorList.length) {
      message = 
         getMessage("modMtImExport.import.errorsOccured") + "<br />" +
         errorList.join("<br />") + "<br />" +
         message;
   }
   return message;
}


/**
 * Returns the last exporter
 * @return User Object
 */
function modMtImExportGetLastExporter() {
   return root.users.get(
      this.preferences.getProperty("modMtImExportLastExporter"));
}


/**
 * Sets the last exporter
 * @param   user   User Object
 */
function modMtImExportSetLastExporter(user) {
   this.preferences.setProperty(
      "modMtImExportLastExporter",       
      user ? user.name : null
   );
}


/**
 * Returns the last export time
 * @return Date
 */
function modMtImExportGetLastExportTime() {
   return this.preferences.getProperty("modMtImExportLastExportTime");
}


/**
 * Sets the last export time
 * @param   exportTime   Date Object
 */
function modMtImExportSetLastExportTime(exportTime) {
   this.preferences.setProperty("modMtImExportLastExportTime", exportTime);
}
