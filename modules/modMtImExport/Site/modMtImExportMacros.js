
/**
 * this macro renders the additional import-options for trusted user, if the user logged in is a trusted one
 */
function modMtImExportTrustedUserImportOptions_macro(param) {
   if (session.user.trusted) this.renderSkin("modMtImExportTrustedUserImportOptions");
}


/**
 * renders the last ExportTime & User as a combined, formated string
 * @example
 *   2004-12-12 14:56 by Matix
 */
function modMtImExportLastExport_macro(param) {
   var exportTime = this.modMtImExportGetLastExportTime();
   if (exportTime) {
      res.write(exportTime.format("yyyy-MM-dd HH:mm", this.getLocale(), this.getTimeZone()));
      res.write(" by ");
      var lastExporter = this.modMtImExportGetLastExporter();
      if (lastExporter) {
         res.write(lastExporter.name);
      }
   } else {
      res.write("-");
   }
}


/**
 * renders the last Exporter, if available
 * @example
 *   Matix
 */
function modMtImExportLastExporter_macro() {
   var lastExporter = this.modMtImExportGetLastExporter();
   if (lastExporter) {
      res.write(lastExporter.name);
   }
}
