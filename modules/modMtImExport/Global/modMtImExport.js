
app.modules.modMtImExport = {
   title: "Im- Export",
   description: "Enables the user to export and import stories & comments. Uses the format of Movable Type.",
   version: "1.1",
   state: "beta",
   url: "",
   author: "Paul Alexandrow, Max Demmelbauer",
   authorEmail: "max.demmelbauer@knallgrau.at",
   authorUrl: "",
   authorOrganisation: "Knallgrau",
   license: "twoday.org",
   licenseUrl: "",
   licenseText: "",
   copyright: "",
   isSidebar: false,
   hasSiteSettings: false,
   skins: ["Story.modMtImExportStory", 
           "Story.modMtImExportStoryDelimiter",
           "Comment.modMtImExportComment", 
           "Comment.modMtImExportReply"]
};


app.modules.modMtImExport.setMgrMenu = function() {
  var existingMenuItem = MGR_MENU.getChildElement("admin.site.modMtImExport");
  if (existingMenuItem) return;
  var level2 = MGR_MENU.getChildElement("admin.site");
  level2.add("modMtImExport","path.site", "modMtImExport");
  MGR_MENU_PATH.Site.modMtImExport = "admin.site.modMtImExport";
};


app.modules.modMtImExport.checkSiteAccess = function(params) {
   var action = params[0];
   var usr    = params[1];
   var level  = params[2];
   var url    = params[3];
   if (action == "modMtImExport") {
      checkIfLoggedIn();
      this.checkModMtImExportExport(usr, level);
   }
   return params;
};
