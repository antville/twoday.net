
/**
 * renders the complete exportstring of the current story
 * @param bool set to true if the story's comments shall be exported as well
 * @param Date export only stories after this date, to export all stories set this param to null
 * @return the exportString of the Story or "" if the story is not going to be exported
 */
function modMtImExportGetExportString(exportComments, lastExportTime) {           
  // check if there is someting to do
  if (
    lastExportTime &&
    lastExportTime > this.modifytime
  ) return "";

  // render comments if necassarry (exportComments is true)
  res.push();
  if (exportComments) {
    this.comments.prefetchChildren();
    for (var i = 0; i < this.size(); i++) {
      res.write(this.get(i).modMtImExportGetExportString());
    }
  }
    
  return this.renderSkinAsString("modMtImExportStory", { comments: res.pop(), creatorName: this.creator.name, creatorEmail: this.creator.url ? this.creator.url : ""});
}
