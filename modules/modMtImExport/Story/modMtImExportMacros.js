
/**
 * Overwrites existing Story.creator_macro in order to display the Original Creator
 */
function creator_macro(param) {
   if (this.creatorName) {
      res.push();
      HopObject.prototype.creator_macro.apply(this, [param]);
      var importUser = res.pop();
      this.renderSkin("modMtImExportCreator", {creator: importUser});
   } else {
      HopObject.prototype.creator_macro.apply(this, [param]);
   }
}


/**
 * Renders the Original Creator of an imported Story/Comment
 */
function modMtImExportCreator_macro(param) {
   if (!this.creatorName)
      return;
   if (param.as == "link" && this.creatorEmail)
      Html.link({href: this.creatorEmail}, this.creatorName);
   else if (param.as == "url")
      res.write(this.creatorEmail);
   else
      res.write(this.creatorName);
   return;
}


/**
 * macro renders who may edit this story
 * subscribers|contributors|author
 */
function modMtImExportEditableBy_macro() {
   if (this.editableby == EDITABLEBY_ADMINS) {
      res.write("author");
   } else if (this.editableby == EDITABLEBY_CONTRIBUTORS) {
      res.write("contributors");
   } else {
      res.write("subscribers");
   }
}


/**
 * macro renders whether comments are allowed or not
 */
function modMtImExportDiscussions_macro() {
   return this.discussions ? "yes" : "no";
}


/**
 * renders the email of the creator if publishing is allowed
 */
function modMtImExportCreatorEmail_macro() {
   if (!this.creator)
      return;
   if (this.creator.url)
      res.write(this.creator.url);
   return;
}
