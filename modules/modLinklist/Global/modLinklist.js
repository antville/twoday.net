
app.modules.modLinklist = {
   title: "Linklist",
   description: "Displays a list with links to other webpages.",
   version: "",
   state: "beta",
   url: "",
   author: "Wolfgang Kamir",
   authorEmail: "wolfgang@knallgrau.at",
   authorUrl: "http://www.knallgrau.at",
   authorOrganisation: "knallgrau.at",
   copyright: "(c) 2004 knallgrau.at",
   license: "twoday",
   licenseUrl: "http://www.twoday.org",
   isSidebar: true,
   type: MODULE_TYPE_LIST,
   hasSiteSettings: true,
   skins: ["Site.modLinklist", "Site.modLinklistItem", "Site.modLinklistDivider"]
};


app.modules.modLinklist.renderSidebarItem = function(param) {
   if (!param) param = {};
   var LinkNames = app.modules.modLinklist.getLinklistNames(this);
   var LinkUrls = app.modules.modLinklist.getLinklistUrls(this);

   res.push();
   for (var i=0; i<LinkNames.length; i++) {
      if (LinkNames[i] == "divider") {
         this.renderSkin("modLinklistDivider");
      } else {
         var p = {};
         p.LinkName = LinkNames[i];
         p.LinkUrl = LinkUrls[i];
         this.renderSkin("modLinklistItem", p);
      }
   }
   var p = {};
   p.Linklist = res.pop();
   res.push();
   this.renderSkin("modLinklist", p);
   var p = {};
   p.body = res.pop();
   p.editLink = true;
   this.renderSidebarModuleItem("modLinklist", p);
   return;
}


app.modules.modLinklist.renderPreferences = function() {
   Module.prototype.renderSetupSpacerLine();

   var LinkNames = app.modules.modLinklist.getLinklistNames(this);
   var LinkUrls = app.modules.modLinklist.getLinklistUrls(this);
   var p = {};
   p.LinkArranger = app.modules.modLinklist.renderLinkArranger(this);
   req.data.modLinklistName = "";
   req.data.modLinklistUrl = "http://";
   Module.prototype.renderSkin("modLinklistSetup", p);
};

app.modules.modLinklist.evalModulePreferences = function() {
   var LinkNames = new Array();
   var LinkUrls = new Array();

   try {
      var LinkArrangerValues = JSON.parse(req.data.LinkArrangerValues);
   } catch (err) {
      ;
   }
   if (!LinkArrangerValues)
      var LinkArrangerValues = new Array();

   for (var i=0; i<LinkArrangerValues.length; i++) {
      var temp = LinkArrangerValues[i].split(":#:");
      LinkNames.push(temp[0]);
      LinkUrls.push(temp[1]);
   }

   this.preferences.setProperty("modlinklist_names", LinkNames.toSource());
   this.preferences.setProperty("modlinklist_urls", LinkUrls.toSource());
};

app.modules.modLinklist.getLinklistUrls = function(obj) {
   try {
      var LinkUrls = JSON.parse(obj.preferences.getProperty("modlinklist_urls"));
   } catch (err) {
      ;
   }
   if (!LinkUrls)
      var LinkUrls = new Array();

   return LinkUrls;
}

app.modules.modLinklist.getLinklistNames = function(obj) {
   try {
      var LinkNames = JSON.parse(obj.preferences.getProperty("modlinklist_names"));
   } catch (err) {
      ;
   }
   if (!LinkNames)
      var LinkNames = new Array();

   return LinkNames;
}

app.modules.modLinklist.renderLinkArranger = function(obj) {
   var LinkNames = app.modules.modLinklist.getLinklistNames(obj);
   var LinkUrls = app.modules.modLinklist.getLinklistUrls(obj);

   var LinkValues = [];
   for (var i=0; i<LinkNames.length; i++) {
      if (LinkNames[i] != "divider")
         LinkValues.push({value: LinkNames[i] + ":#:" + LinkUrls[i], display: LinkNames[i] + " (" + LinkUrls[i] + ")"});
      else
         LinkValues.push({value: "divider:#:divider", display: "----------"});
   }

   var p = {};
   p.LinkArranger = Html.dropDownAsString({name: "LinkArranger", id: "LinkArranger", size: 12, multiple: "multiple", style: "width:440px;", onchange: "getItem(); return false;"}, LinkValues);

   return Module.prototype.renderSkinAsString("modLinkArranger", p);
}