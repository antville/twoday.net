HTTPAuth = {
   toString: function() {
      return "[HTTPAuth Object]";
   },

   authenticate: function(realm) {
      var uname = req.username;
      var pwd = req.password;

      if (uname == null || uname == "" || pwd == null || pwd == "")
          return this.forceAuth(realm);

      // try to Auth agains twoday
      var theUser = root.users.get(uname);
      if (!theUser || theUser.password != pwd)
         return this.forceAuth(realm);
      return theUser;
   },
   
   forceAuth: function(realm) {
      res.reset();
      res.status = 401;
      res.realm = (realm != null) ? realm : "helma";
      // FIXME i18n ?
      renderSkin("modHTTPAuth401Response");
      return false;     
   }
}