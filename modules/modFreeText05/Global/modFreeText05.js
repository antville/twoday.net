
app.modules.modFreeText05 = {
   title: "Free Text (5)",
   description: "renders freely definable HTML Text",
   version: "1.0",
   state: "stable",
   url: "",
   author: "Wolfgang Kamir",
   authorEmail: "wolfgang@knallgrau.at",
   authorUrl: "http://www.knallgrau.at",
   authorOrganisation: "knallgrau.at",
   copyright: "(c) 2004 knallgrau.at",
   license: "twoday",
   licenseUrl: "http://www.twoday.org",
   isSidebar: true,
   hasSiteSettings: true
};


/**
 * Reads userdefined free HTML text from site's preferences and hands it over to "modFreeText05" skin as parameter.
 * renderSidebarModuleItem function displays the module in sidebar (+ header).
 *
 * @see Site.renderSidbarModuleItem
 */
app.modules.modFreeText05.renderSidebarItem = function(param) {
   var p = {};
   p.body = this.preferences.getProperty("modfreetext05text") ? this.preferences.getProperty("modfreetext05text") : "";
   p.editLink = true;
   this.renderSidebarModuleItem("modFreeText05", p);
   return;
}


/**
 * Renders the module's preferences.
 */
app.modules.modFreeText05.renderPreferences = function() {
   var param = {};
   param.label = getMessage("modFreeText05.wizard.admin.settingsText");
   param.value = this.preferences.getProperty("modfreetext05text") ?
                 this.preferences.getProperty("modfreetext05text") : "";
   Module.prototype.renderSkin("modFreeText05SetupLine", param);
};


/**
 * Stores the userdefined free HTML text in the site's prefernces.
 */
app.modules.modFreeText05.evalModulePreferences = function() {
   this.preferences.setProperty("modfreetext05text", req.data.modFreeText05Text);
};
