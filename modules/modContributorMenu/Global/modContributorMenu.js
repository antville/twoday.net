
app.modules.modContributorMenu = {
   version: "",
   state: "beta",
   url: "",
   author: "Wolfgang Kamir",
   authorEmail: "wolfgang@knallgrau.at",
   authorUrl: "http://www.knallgrau.at",
   authorOrganisation: "knallgrau.at",
   copyright: "(c) 2004 knallgrau.at",
   license: "twoday",
   licenseUrl: "http://www.twoday.org",
   isSidebar: true,
   type: MODULE_TYPE_NAVIGATION,
   hasSiteSettings: true,
   skins: ["Site.contribnavigation"]
};


app.modules.modContributorMenu.renderSidebarItem = function() {
   if (this.preferences.getProperty("usercontrib") || res.meta.memberlevel >= CONTRIBUTOR) {
      var p = {};
      p.body = this.renderSkinAsString("contribnavigation");
      this.renderSidebarModuleItem("modContributorMenu", p);
   }
   return;
}
