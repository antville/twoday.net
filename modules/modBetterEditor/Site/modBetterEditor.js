
/**
 * wrapper to make site/modBetterEditorJS.skin public
 */
function modBetterEditor_js_action() {
   try {
      this.checkView(session.user, res.meta.memberlevel);
   }
   catch (e) {
      res.writeln(getMessage("deny.access"));
      return;
   }
   res.setLastModified(res.handlers.layout.skins.getSkinLastModified("site", "modBetterEditorJS"));
   res.contentType = "text/javascript";
   this.renderSkin("modBetterEditorJS");
}


/**
 * wrapper to make site/modBetterEditorCSS.skin public
 */
function modBetterEditor_css_action() {
   try {
      this.checkView(session.user, res.meta.memberlevel);
   }
   catch (e) {
      res.writeln(getMessage("deny.access"));
      return;
   }
   res.setLastModified(res.handlers.layout.skins.getSkinLastModified("site", "modBetterEditorCSS"));
   res.contentType = "text/css";
   this.renderSkin("modBetterEditorCSS");
}
