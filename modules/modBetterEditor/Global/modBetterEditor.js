
app.modules.modBetterEditor = {
   title: "Better Editor",
   description: "Advanced HTML Editor for textfields",
   version: "1.1",
   state: "beta",
   url: "http://www.twoday.org",
   author: "Matthias Platzer",
   authorEmail: "matthias@knallgrau.at",
   authorURL: "http://www.knallgrau.at",
   authorOrganisation: "knallgrau.at",
   copyright: "(c) 2004 knallgrau.at",
   license: "twoday",
   isSidebar: false,
   hasSiteSettings: true,
   skins: ["StoryMgr.modBetterEditorHead", "Site.modBetterEditorCSS"]
};


app.modules.modBetterEditor.evalNewSite = function(args) {
   var creator = args[0];
   var newSite = args[1];
   newSite.preferences.setProperty("modBetterEditorEditor", "bettereditor");
   return;
}


app.modules.modBetterEditor.addEditorHead = function () {
   var mode = res.handlers.site.preferences.getProperty("modBetterEditorEditor");
   if (mode == "bettereditor") {
      knallgrau.addToResponseHead(res.handlers.site.stories.renderSkinAsString("modBetterEditorHead"));
   }
};

knallgrau.Event.registerObserver("Story", "beforeEditAction", app.modules.modBetterEditor.addEditorHead, null, "app.modules.modBetterEditor.addEditorHead");
knallgrau.Event.registerObserver("StoryMgr", "beforeCreateAction", app.modules.modBetterEditor.addEditorHead, null, "app.modules.modBetterEditor.addEditorHead");


app.modules.modBetterEditor.renderPreferences = function() {
   Module.prototype.renderSetupSpacerLine();
   Module.prototype.renderSetupHeader({header: getMessage("modBetterEditor.wizard.admin.settingsTitle")});
   var options = [
      {value: "plain", display: getMessage("modBetterEditor.wizard.admin.textArea")},
      {value: "bettereditor", display: getMessage("modBetterEditor.wizard.admin.betterEditor")},
   ];
   var param = {
      label: getMessage("modBetterEditor.wizard.admin.editor"),
      field: Html.dropDownAsString({name: "modBetterEditorEditor"}, options, this.preferences.getProperty("modBetterEditorEditor"))
   };
   Module.prototype.renderSetupLine(param);
   var options = [
      {value: "true", display: getMessage("generic.yes")},
      {value: "false", display: getMessage("generic.no")},
   ];
   var param = {
      label: getMessage("modBetterEditor.wizard.admin.enableAutoCompletionLabel"),
      field: Html.dropDownAsString({name: "modBetterEditorAutoCompletion"}, options, this.preferences.getProperty("modBetterEditorAutoCompletion")),
      hint: getMessage("modBetterEditor.wizard.admin.enableAutoCompletionHint")
   };
   Module.prototype.renderSetupLine(param);
};


app.modules.modBetterEditor.evalModulePreferences = function() {
   this.preferences.setProperty("modBetterEditorEditor", req.data.modBetterEditorEditor);
   this.preferences.setProperty("modBetterEditorAutoCompletion", req.data.modBetterEditorAutoCompletion);
};


app.modules.modBetterEditor.onGlobalCodeUpdate = function() {
   DefaultImages.button_iconview = {name: "modBetterEditor/button_iconview.gif", width: 24, height: 16, alt: "iconview"};
   DefaultImages.button_listview = {name: "modBetterEditor/button_listview.gif", width: 24, height: 16, alt: "listview"};
   DefaultImages.button_next = {name: "modBetterEditor/button_next.gif", width: 24, height: 16, alt: "next"};
   DefaultImages.button_prev = {name: "modBetterEditor/button_prev.gif", width: 24, height: 16, alt: "prev"};
   DefaultImages.editor_divider = {name: "modBetterEditor/editor_divider.gif", width: 8, height: 24, alt: ""};
   DefaultImages.editor_icon_bold = {name: "modBetterEditor/editor_icon_bold.gif", width: 24, height: 24, alt: "bold"};
   DefaultImages.editor_icon_cite = {name: "modBetterEditor/editor_icon_cite.gif", width: 24, height: 24, alt: "cite"};
   DefaultImages.editor_icon_file = {name: "modBetterEditor/editor_icon_file.gif", width: 24, height: 24, alt: "file"};
   DefaultImages.editor_icon_help = {name: "modBetterEditor/editor_icon_help.gif", width: 24, height: 24, alt: "help"};
   DefaultImages.editor_icon_image = {name: "modBetterEditor/editor_icon_image.gif", width: 24, height: 24, alt: "image"};
   DefaultImages.editor_icon_italic = {name: "modBetterEditor/editor_icon_italic.gif", width: 24, height: 24, alt: "italic"};
   DefaultImages.editor_icon_link = {name: "modBetterEditor/editor_icon_link.gif", width: 24, height: 24, alt: "link"};
   DefaultImages.editor_icon_macro = {name: "modBetterEditor/editor_icon_macro.gif", width: 24, height: 24, alt: "macro"};
   DefaultImages.editor_icon_ol = {name: "modBetterEditor/editor_icon_ol.gif", width: 24, height: 24, alt: "ol"};
   DefaultImages.editor_icon_strike = {name: "modBetterEditor/editor_icon_strike.gif", width: 24, height: 24, alt: "strike"};
   DefaultImages.editor_icon_ul = {name: "modBetterEditor/editor_icon_ul.gif", width: 24, height: 24, alt: "ul"};
};


app.modules.modBetterEditor.renderInitialize = function() {
   this.renderSetupSpacerLine();
   this.renderSetupHeader({header: getMessage("modBetterEditor.wizard.admin.settingsTitle")});
};
