
function insertAssetListAssets_macro(param) {
   var collection = (req.data.type == "image") ? this._parent.images : this._parent.files;
   if (req.data.topic && collection.topics.get(req.data.topic) != null) collection = collection.topics.get(req.data.topic);
   var start = (req.data.start) ? parseInt(req.data.start, 10) : 0;
   var show   = 20;
   var end    = Math.min(start + show - 1, collection.size() - 1);
   for (var i = start; i <= end; i++) {
      var obj = new Object();
      obj.count = i;
      collection.get(i).renderSkin("modBetterEditorWizardListItem", obj);
   }
   return;
}


function insertAssetNavigation_macro(param) {
   var collection = (req.data.type == "image") ? this._parent.images : this._parent.files;
   if (req.data.topic && collection.topics.get(req.data.topic) != null) collection = collection.topics.get(req.data.topic);

   var size   = collection.size();
   var start = (req.data.start) ? parseInt(req.data.start, 10) : 0;
   var show   = 20;
   var end    = Math.min(start + show - 1, size - 1);

   param.show = show;
   param.topic = req.data.topic;
   if (start > 0) param.prev_start = Math.max(start - show, 0);
   else param.prev_disabled = "disabled=\"disabled\"";
   if (end < size - 1) param.next_start = Math.min(start + show, size);
   else param.next_disabled = "disabled=\"disabled\"";
   var obj = new Object();
      obj.onChange = "chooseImageIndex(this)";
      obj.name = "imageIndexChooser";
      obj.style = "width: 80px; display: inline; margin-left: 10px; margin-right: 10px; text-align: center;";
   param.dropdown = Html.openTagAsString("select", obj);
   for (var i=0; i<(Math.floor(size/show) + 1); i++) {
      var obj = new Object();
      obj.value = i * show;
      if (start >= i*show && start < (i+1)*show) obj.selected = "selected";
      param.dropdown += Html.openTagAsString("option", obj);
      param.dropdown += ((i*show + 1) + " - " + Math.min((i+1)*show, size));
      param.dropdown += Html.closeTagAsString("option");
   }
   param.dropdown += Html.closeTagAsString("select");
   param.type = (req.data.type == "image") ? "image" : "file";
   this.renderSkin("insertAssetNavigation", param);
   return;
}


function imageUploadForm_macro(param) {
   this._parent.images.renderSkin("new", param);
}


function fileUploadForm_macro(param) {
   this._parent.files.renderSkin("new", param);
}
