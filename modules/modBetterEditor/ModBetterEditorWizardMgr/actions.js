
function insertAsset_action() {
   checkIfLoggedIn(this._parent.href("edit"));

   knallgrau.addToResponseHead(this.renderSkinAsString("insertAsset_js"));
   res.handlers.images = path.site.images;
   res.handlers.wizard = this;

   if (req.data.type == "image") {
      if (req.data.action == "choose") {
         res.data.title = getMessage("modBetterEditor.wizard.image.chooseTitle");
         res.data.header = res.data.title;
         res.data.subheader = getMessage("modBetterEditor.wizard.image.chooseSubHeader");
         res.data.body = this.renderSkinAsString("insertImageChooser");

      } else if (req.data.action == "upload") {
         if (req.data.save) {
            if ((!req.data.rawimage || req.data.rawimage.contentLength == 0) && req.data.url && req.data.url!="http://")
                req.data.rawimage = getURL(req.data.url);
            try {
               var result = this._parent.images.evalImg(req.data, session.user);
               res.message = result.toString();

               // BMWORACLE MOD - start
               if (this._parent.alias == "bmworacleracing") {
                  req.data.resizeto = "max";
                  req.data.width = "400";
                  req.data.height = "400";
                  req.data.alias = result.value + "_bmwPreview";
                  this._parent.images.evalImg(req.data, session.user);
               }
               // BMWORACLE MOD - end

               res.redirect(this.href("insertAsset") + "?action=insert&type=image&itemid=" + result.value);
            } catch (err) {
               res.message = err.toString();
            }
         }
         res.data.title = getMessage("modBetterEditor.wizard.image.uploadTitle");
         res.data.header = res.data.title;
         res.data.subheader = getMessage("modBetterEditor.wizard.image.uploadSubHeader");

         // BMWORACLE MOD - start
         if (this._parent.alias == "bmworacleracing")
            req.data.resizeto = "no";
         // BMWORACLE MOD - end

         res.data.body = this.renderSkinAsString("insertImageUpload");
      } else if (req.data.action == "insert") {
         res.data.title = getMessage("modBetterEditor.wizard.image.insertTitle");
         res.data.header = res.data.title;
         res.data.subheader = getMessage("modBetterEditor.wizard.image.insertSubHeader");
         req.data.insertas = "full";
         req.data.imageposition = "default";
         res.handlers.image = this._parent.images.get(req.data.itemid);

         // BMWORACLE MOD - start
         if (this._parent.alias == "bmworacleracing" && this._parent.images.get(req.data.itemid + "_bmwPreview") != null) {
            req.data.insertas = "bmworaclepreview";
            res.data.body = this.renderSkinAsString("insertImageInsert_bmwOracle");
         } else {
            res.data.body = this.renderSkinAsString("insertImageInsert");
         }
         // BMWORACLE MOD - end

      }
   } else if (req.data.type == "file") {
      if (req.data.action == "choose") {
         res.data.title = getMessage("modBetterEditor.wizard.file.chooseTitle");
         res.data.header = res.data.title;
         res.data.subheader = getMessage("modBetterEditor.wizard.file.chooseSubHeader");
         res.data.body = this.renderSkinAsString("insertFileChooser");
      } else if (req.data.action == "upload") {
         if (req.data.save) {
            try {
               var result = this._parent.files.evalFile(req.data, session.user);
               res.message = result.toString();
               res.redirect(this.href("insertAsset") + "?action=insert&type=file&itemid=" + result.value);
            } catch (err) {
               res.message = err.toString();
            }
         }
         res.data.title = getMessage("modBetterEditor.wizard.file.uploadTitle");
         res.data.header = res.data.title;
         res.data.subheader = getMessage("modBetterEditor.wizard.file.uploadSubHeader");
         res.data.body = this.renderSkinAsString("insertFileUpload");
      } else if (req.data.action == "insert") {
         res.data.title = getMessage("modBetterEditor.wizard.file.insertTitle");
         res.data.header = res.data.title;
         res.data.subheader = getMessage("modBetterEditor.wizard.file.insertSubHeader");
         res.handlers.file = this._parent.files.get(req.data.itemid);
         res.data.body = this.renderSkinAsString("insertFileInsert");
      }
   }
   this.renderSkin("main");
}
