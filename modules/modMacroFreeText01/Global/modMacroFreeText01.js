
app.modules.modMacroFreeText01 = {
   title: "Macro Free Text (1)",
   description: "renders freely definable Text allowing macros",
   version: "1.0",
   state: "stable",
   url: "",
   author: "Franz Philipp Moser",
   authorEmail: "philipp.moser@knallgrau.at",
   authorUrl: "http://www.knallgrau.at",
   authorOrganisation: "knallgrau.at",
   copyright: "(c) 2009 knallgrau.at",
   license: "twoday",
   licenseUrl: "http://www.twoday.org",
   isSidebar: true,
   hasSiteSettings: true,
   isRestricted: true
};


/**
 * Reads userdefined free HTML text from site's preferences and hands it over to "modFreeText01" skin as parameter.
 * renderSidebarModuleItem function displays the module in sidebar (+ header).
 *
 * @see Site.renderSidbarModuleItem
 */
app.modules.modMacroFreeText01.renderSidebarItem = function(param) {
   var p = {};
   p.body = this.preferences.getProperty("modmacrofreetext01text") ? this.preferences.getProperty("modmacrofreetext01text") : "";
   var bodySkin = createSkin(p.body);
   p.body = this.renderSkinAsString(bodySkin);
   p.editLink = true;
   this.renderSidebarModuleItem("modMacroFreeText01", p);
   return;
}


/**
 * Renders the module's preferences.
 */
app.modules.modMacroFreeText01.renderPreferences = function() {
   var param = {};
   param.label = getMessage("modMacroFreeText01.wizard.admin.settingsText");
   param.value = this.preferences.getProperty("modmacrofreetext01text") ?
                 this.preferences.getProperty("modmacrofreetext01text") : "";
   Module.prototype.renderSkin("modMacroFreeText01SetupLine", param);
};


/**
 * Stores the userdefined free HTML text in the site's prefernces.
 */
app.modules.modMacroFreeText01.evalModulePreferences = function() {
   this.preferences.setProperty("modmacrofreetext01text", req.data.modMacroFreeText01Text);
};
