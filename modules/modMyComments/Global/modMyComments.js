
app.modules.modMyComments = {
   version: "",
   state: "",
   url: "",
   author: "",
   authorEmail: "",
   authorUrl: "http://www.knallgrau.at",
   authorOrganisation: "knallgrau.at",
   copyright: "(c) 2006 knallgrau.at",
   license: "twoday",
   licenseUrl: "http://www.twoday.org",
   isSidebar: true,
   type: MODULE_TYPE_INFO,
   hasSiteSettings: true,
   skins: ["Story.modMyCommentsItem"]
};


app.modules.modMyComments.renderSidebarItem = function() {
   var p = {};
   var param = {};
   res.push();
   param.max = parseInt(this.preferences.getProperty("modmycomments_limit"), 10) || 5;
   param.skin = "modMyCommentsItem";
   this.creator.comments.list_macro(param, true);
   p.body = res.pop();
   this.renderSidebarModuleItem("modMyComments", p);
   return;
};


app.modules.modMyComments.renderPreferences = function() {
   Module.prototype.renderSetupSpacerLine();

   var limit = this.preferences.getProperty("modmycomments_limit");
   if (!limit) limit = 5;
   var options = [];
   for (var i=1; i<9; i++) options.push([i, i]);
   var param = {
      label: getMessage("modMyComments.preferences.limit"),
      field: Html.dropDownAsString({name: "modmycomments_limit"}, options, limit)
   };
   Module.prototype.renderSetupLine(param);
};


app.modules.modMyComments.evalModulePreferences = function() {
   this.preferences.setProperty("modmycomments_limit", req.data.modmycomments_limit);
};
