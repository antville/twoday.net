
app.modules.modUserStatus = {
   version: "",
   state: "beta",
   url: "",
   author: "Wolfgang Kamir",
   authorEmail: "wolfgang@knallgrau.at",
   authorUrl: "http://www.knallgrau.at",
   authorOrganisation: "knallgrau.at",
   copyright: "(c) 2004 knallgrau.at",
   license: "twoday",
   licenseUrl: "http://www.twoday.org",
   isSidebar: true,
   type: MODULE_TYPE_INFO,
   hasSiteSettings: true,
   skins: ["MemberMgr.statusloggedin", "MemberMgr.statusloggedout"]
};


app.modules.modUserStatus.renderSidebarItem = function(param) {
   var p = {};
   res.push();
   this.loginstatus_macro(param);
   p.body = res.pop();
   this.renderSidebarModuleItem("modUserStatus", p);
   return;
};
