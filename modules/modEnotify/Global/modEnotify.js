
app.modules.modEnotify = {
   title: "email notification",
   description: "",
   version: "",
   state: "",
   url: "",
   author: "",
   authorEmail: "",
   authorUrl: "",
   authorOrganisation: "Knallgrau",
   license: "twoday",
   licenseUrl: "",
   licenseText: "",
   copyright: "",
   isSidebar: false,
   hasSiteSettings: true,
   skins: []
}


app.modules.modEnotify.evalNewSite = function(args) {
   var creator = args[0];
   var newSite = args[1];
   newSite.modEnotify = 1;
   return;
}


app.modules.modEnotify.renderPreferences = function() {
   Module.prototype.renderSetupSpacerLine();
   Module.prototype.renderSetupHeader({header: getMessage("modEnotify.pref.title")});
   res.push();
   Html.checkBox({name: "modEnotify", value: "1", checked: this.modEnotify ? "checked" : null});
   var inputField = res.pop();
   var param = {
      label: getMessage("modEnotify.pref.checkbox"),
      field: inputField
   };
   Module.prototype.renderSetupLine(param);
   res.push();
   var txt = this.modEnotifyText;
   if (!txt) txt = getMessage("modEnotify.mail.body");
   Html.textArea({name: "modEnotifyText", value: txt, rows: 7, cols: 70});
   var inputField = res.pop();
   var param = {
      label: getMessage("modEnotify.pref.text"),
      field: inputField
   };
   Module.prototype.renderSetupLine(param);
};


app.modules.modEnotify.evalModulePreferences = function() {
   this.modEnotify = req.data.modEnotify ? 1 : 0;
   this.modEnotifyText = req.data.modEnotifyText;
};


app.modules.modEnotify.checkSiteAccess = function(params) {
   var action = params[0];
   var usr    = params[1];
   var level  = params[2];
   var url    = params[3];
   if (action == "modEnotify") {
      checkIfLoggedIn();
      this.modEnotifyCheck(usr, level);
   }
   return params;
}


app.modules.modEnotify.onAfterEvalComment = function (args) {
   var comment = args[0];
   if (comment) {
      if (comment.online) {
         comment.modEnotify();
      } else {
         // TODO inform users with lvl CONTENTMANAGER or higher
         comment.modEnotify(CONTENTMANAGER);
      }
   }
}

app.modules.modEnotify.onEvalStory = function (args) {
   var story = args[0];
   var content = args[1];
   if (content && content.isMajorUpdate && story.online == 2) story.modEnotify();
}


app.modules.modEnotify.onEvalNewStory = function (args) {
   var story = args[0];
   if (story && story.online)
      story.modEnotify();
}


app.modules.modEnotify.renderSubscriptionTasks = function (items) {
   var sub = this.site.members.get(session.user._id.toString());
   if (sub) items.push({href: this.site.href("modEnotify"), text: sub.modEnotify ? getMessage("modEnotify.membership.active") : getMessage("modEnotify.membership.inactive")});
   return items;
}
