
/**
 * function notifies members of this site, which wish to be notified
 * if you supply a membership level all Members with the level and above will be
 * enotified. default is SUBSCRIBER
 * 
 * @param membershipLevel (optional) on of ADMIN, CONTENTMANAGER, CONTRIBUTOR, SUBSCRIBER
 */
function modEnotify(membershipLevel) {
   if (!this.site.modEnotify) return;
   if (membershipLevel == null) {
      membershipLevel = SUBSCRIBER;
   }
   
   var members = this.site.members.list();
   for (var i = 0; i < members.length; i++) {
      if (this.modEnotifyShouldNotify(members[i]) == true) {
         if (members[i].level >= membershipLevel)
            this.modEnotifyCreateMail(members[i].user);
      }
   }
   return;
}


/**
 * function determines whether a certain user, should be notified
 * according to his notification settings
 *
 * @param Membership-Object
 * @return Boolean
 */
function modEnotifyShouldNotify (m) {
   if (m.modEnotify == null)
      return false;

   switch (m.modEnotify) {
      case "NEWTEXT" :
         if (m.user != this.creator)
            return true;
         else
            return false;
      case "NEWSTORY" :
         if (m.user != this.creator && this._prototype == "Story")
            return true;
         else
            return false;
      case "MYTEXT" :
         if (m.user == this.creator)
            return false;
         var obj = this;
         while (obj._prototype != "Story") {
            obj = obj.parent ? obj.parent : obj.story;
            if (obj.creator == m.user) return true;
         }
         return false;
      case "MYTHREAD" : // includes MYTEXT
         if (this.creator == m.user)
            return false;
         var obj = this._prototype == "Story" ? this : this.story;
         if (obj.creator == m.user)
            return true;
         for (var i=0; i<obj.comments.count(); i++) {
            if (obj.comments.get(i).creator == m.user) return true;
         }
         return false;
      case "MYTHREADNEWSTORY" : // includes NEWSTORY, MYTEXT
         if (this.creator == m.user)
            return false;
         if (this._prototype == "Story")
            return true;
         if (this.story.creator == m.user)
            return true;
         for (var i=0; i<this.story.comments.count(); i++) {
            if (this.story.comments.get(i).creator == m.user) return true;
         }
         return false;
   }
   return false;
}


/**
 * triggered by settings of e-mail notification
 * function creates Mail Object, which is stored to root.modEnotify.queue
 *
 * @param Obj User-Object, to whom the mail should be sent
 */
function modEnotifyCreateMail(usr) {
   var param = new Object();
   param.creator = this.creator ? this.creator.name : this.creatorName;
   param.type = (this._prototype == "Story") ? getMessage("Story") : getMessage("Comment");
   param.title = (this.title ? this.title : this.getRenderedContentPart("text")).stripTags();
   if (param.title) {
      // fix encoded characters
      var lookup = ["&euro;", "", "&#8218;", "&#402;", "&#8222;", "&#8230;", "&#8224;", "&#8225;", "&#710;", "&#8240;", "&#352;", "&#8249;", "&#338;", "", "&#381;", "", "", "&#8216;", "&#8217;", "&#8220;", "&#8221;", "&#8226;", "&#8211;", "&#8212;", "&#732;", "&#8482;", "&#353;", "&#8250;", "&#339;", "", "&#382;", "&#376;", "&nbsp;", "&iexcl;", "&cent;", "&pound;", "&curren;", "&yen;", "&brvbar;", "&sect;", "&uml;", "&copy;", "&ordf;", "&laquo;", "&not;", "&shy;", "&reg;", "&macr;", "&deg;", "&plusmn;", "&sup2;", "&sup3;", "&acute;", "&micro;", "&para;", "&middot;", "&cedil;", "&sup1;", "&ordm;", "&raquo;", "&frac14;", "&frac12;", "&frac34;", "&iquest;", "&Agrave;", "&Aacute;", "&Acirc;", "&Atilde;", "&Auml;", "&Aring;", "&AElig;", "&Ccedil;", "&Egrave;", "&Eacute;", "&Ecirc;", "&Euml;", "&Igrave;", "&Iacute;", "&Icirc;", "&Iuml;", "&ETH;", "&Ntilde;", "&Ograve;", "&Oacute;", "&Ocirc;", "&Otilde;", "&Ouml;", "&times;", "&Oslash;", "&Ugrave;", "&Uacute;", "&Ucirc;", "&Uuml;", "&Yacute;", "&THORN;", "&szlig;", "&agrave;", "&aacute;", "&acirc;", "&atilde;", "&auml;", "&aring;", "&aelig;", "&ccedil;", "&egrave;", "&eacute;", "&ecirc;", "&euml;", "&igrave;", "&iacute;", "&icirc;", "&iuml;", "&eth;", "&ntilde;", "&ograve;", "&oacute;", "&ocirc;", "&otilde;", "&ouml;", "&divide;", "&oslash;", "&ugrave;", "&uacute;", "&ucirc;", "&uuml;", "&yacute;", "&thorn;", "&yuml;"];
      for (var i=0; i<lookup.length; i++) {
         if (!lookup[i]) continue;
         param.title = param.title.replace(lookup[i], String.fromCharCode(i+128), "g");
      }
      if (param.title.length > 30) param.title = param.title.substring(0, 30) + "...";
   }
   param.site = this.site.getName();
   param.url = this.href();
   var txt = this.site.modEnotifyText;
   if (!txt) txt = getMessage("modEnotify.mail.body");
   var body = this.renderSkinAsString(createSkin(txt), param);
   body += "\n------------------------------\n";
   body += getMessage("modEnotify.mail.footer", {siteName: this.site.getName(), url: this.site.href("modEnotify")});
   sendMail({email: "info@twoday.net", name: "twoday.net"},
            {email: usr.email, name: usr.name},
             this.site.getName(),
             body);
   app.log("ENOTIFY INFO createMail " +
       "[site: " + this.site.alias + "] " +
       "[user: " + usr.name + "," + usr.email+"] " +
       "[href:" + this.href() + "]");
   return;
}

