
function modEnotifyOnAfterEvalNewStory(result, Args, oldFunc, obj) {
   var newStory = obj.get(result.id);
   if (newStory && newStory.online) newStory.modEnotify();
   return result;
}
