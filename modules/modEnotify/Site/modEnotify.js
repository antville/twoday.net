
/**
 * enotify action
 */
function modEnotify_action() {
   res.handlers.mgr = this.stories;
   if (req.data.submit == "cancel" || req.data.cancel)
      res.redirect(this.members.href("updated"));
   else if (req.data.submit == "save" || req.data.save) {
      try {
         this.modEnotifyEvalUpdate(req.data, session.user);
         res.message = getMessage("confirm.update");
         res.redirect(this.members.href("updated"));
      }
      catch (err) {
         res.message = err.toString();
      }
   }

   res.data.action = this.href(req.action);
   res.data.title = getMessage("modEnotify.header", {siteName: this.getName()});
   res.data.body = this.renderSkinAsString("modEnotify");
   this.stories.renderMgrPage("Site");
   return;
}


/**
 * function saves new settings for eMail notification
 * @param Obj Object containing the form values
 * @param Obj User-Object modifying this membership
 * @return Obj Object containing two properties:
 *             - error (boolean): true if error happened, false if everything went fine
 *             - message (String): containing a message to user
 */
function modEnotifyEvalUpdate(param, modifier) {
   if (session.user == null || this.members.get(session.user._id.toString()) == null)
      return;
   var m = this.members.get(session.user._id.toString());
   m.modEnotify = (param.modEnotify == "null") ? null : param.modEnotify;
   m.modifytime = new Date();
   m.modifier = modifier;
   return;
}


/**
 * macro renders radiobutton for setting the type of eMail notification for the current user and this site
 * value can be either  MYTEXT, MYTHREAD, NEWSTORY, NEWTEXT, MYTHREADNEWSTORY
 */
function modEnotify_macro (param) {
   if (session.user == null || this.members.get(session.user._id.toString()) == null)
      return;
   var val = this.members.get(session.user._id.toString()).modEnotify;
   if (param.as == "editor") {
      var obj = new Object();
      obj.name = "modEnotify";
      obj.value = param.value;
      obj.selectedValue = val ? val : "null";
      Html.radioButton(obj);
      return;
   } else {
      return val;
   }
}


/**
 * macro rendering enotify settings
 */
function modEnotifyAllow_macro(param) {
   param.modEnotifySkinEditUrl = res.handlers.layout.skins.href("edit") + "?key=site.modEnotifyMail";
   this.renderSkin("modEnotifyAllow", param);
   return;
}


/**
 * function checks if user is allowed to sign up for eMail notification
 * @param Obj Userobject
 * @param Int Permission-Level
 * @return String Reason for denial (or null if allowed)
 */
function modEnotifyCheck(usr, level) {
   if (!this.online && level == null)
      throw new DenyException("siteView");
   else if (!this.modEnotify)
      throw new Exception("modEnotify.enotifyDisabled");
   else if (level == null)
      throw new Exception("subscriptionNoExist");
   return;
}
