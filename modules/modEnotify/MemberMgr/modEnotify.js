
/**
 * macro renders a link to the enotify-action
 * but only if user is already a member of this site
 */
function modEnotifyLink_macro(param) {
   if (!path.site || res.meta.memberlevel == null || !path.site.modEnotify)
      return;
   Html.link({href: path.site.href("modEnotify")}, param.text ? param.text : getMessage("modEnotify.link.label"));
   return;
}
