
app.modules.modRssFetcher = {
   version: "",
   state: "",
   url: "",
   author: "Christopher Horn",
   authorEmail: "ch@wiesn.at",
   license: "twoday",
   licenseUrl: "http://www.twoday.org",
   isSidebar: true,
   type: MODULE_TYPE_EXTERNALCONTENTS,
   hasSiteSettings: true,
   skins: ["Story.modRssFetcherItem"]
};


app.modules.modRssFetcher.renderPreferences = function() {
   var param = {};
   if (!this.preferences.getProperty("modRssFetcherUrl")) req.data.preferences_modrss_url = "http://";
   param.modrssnritems = this.preferences.getProperty("modRssFetcherNritems") ? this.preferences.getProperty("modRssFetcherNritems") : "10";
   var values = this.preferences.getProperty("modRssFetcherUrl") ? this.preferences.getProperty("modRssFetcherUrl").split("|") : [];
   var options = [];
   for (var i in values) {
      options[i] = [values[i], values[i]];
   }
   param.dropdown = Html.dropDownAsString({name: "modRssFetcherUrl", id: "modRssFetcherUrl", size: 12, multiple: "multiple", style: "width:440px;", onchange: "getItem(); return false;"}, options);
   Module.prototype.renderSkin("modRssFetcherSetup", param);
};


app.modules.modRssFetcher.evalModulePreferences = function() {
   // url validation
   this.preferences.setProperty("modRssFetcherUrl", req.data.modrss_urllist.split("|").slice(0,20).join("|"));
   // TODO: auto-detection??

   var nritems = parseInt(req.data.preferences_modRssFetcherNritems, 10);
   if (!nritems) nritems = 4;
   nritems = Math.max(0, Math.min(nritems, 20));
   this.preferences.setProperty("modRssFetcherNritems", nritems);
};


app.modules.modRssFetcher.renderSidebarItem = function() {
   var robotPattern = /(google|slurp|bot|java)/gi;
   var userAgent = (req.servletRequest.getHeader("user-agent") || "");
//   if (userAgent.match(robotPattern)) return;

   var now = new Date();
   if (this.cache.modRssFetcherLastCheck == null || this.modifytime > this.cache.modRssFetcherLastCheck ||
         (now - this.cache.modRssFetcherLastCheck) > 1000 * 60 * 15) { // cache result for x minutes
      res.push();
      try {
         var items = new Array();
         var urls = this.preferences.getProperty("modRssFetcherUrl") ? this.preferences.getProperty("modRssFetcherUrl").split("|") : [];
         for (var i in urls) {
            if (!urls[i] || !urls[i].toLowerCase().startsWith("http://")) continue;
            var conn;
            var inputStream;
            try {
               // FIXME: this should be done by the new helmaLib helma.http library
               var url = new java.net.URL(urls[i]);
               var conn = url.openConnection();
               conn.setAllowUserInteraction(false);
               conn.setRequestMethod("GET");
               conn.setRequestProperty("User-Agent", root.href() + " modRssFetcher bot");
               conn.setConnectTimeout(1000);  // 1 sec time to connect
               conn.setReadTimeout(2000);     // 2 sec time to read
               var responseCode = conn.getResponseCode();
               var responseLength = conn.getContentLength();
               if (responseLength == 0 || responseCode != 200) {
                  conn.disconnect();
                  continue;
               }
               var inputStream = new java.io.BufferedInputStream(conn.getInputStream());
               var channel = Packages.de.nava.informa.parsers.FeedParser.parse(new Packages.de.nava.informa.impl.basic.ChannelBuilder(), inputStream);
               var itemsTmp = channel.getItems().toArray();
               for (var i in itemsTmp) {
                  items[items.length] = itemsTmp[i];
               }
               // items = items.concat(channel.getItems().toArray());
            } catch (err) {
               ;
            } finally {
               if (conn) conn.disconnect();
               if (inputStream) inputStream.close();
            }
         }
         items.sort(new Function("a", "b", "return b.getDate() - a.getDate()"));

         for (var i=0; i<Math.min(items.length, this.preferences.getProperty("modRssFetcherNritems")); i++) {
            var item = items[i];
            var text = stripTags(item.getDescription());
            var date = item.getDate();
            var title = stripTags(item.getTitle());
            var url = stripTags(item.getLink());
            var creator = stripTags(item.getCreator() ? item.getCreator() : "");

            var st = new Story();
            st.createtime = date;
            st.modifytime = date;
            st.creatorName = creator;
            st.setContent({title: title, text: stripTags(text)});

            var param = { date: date ? formatTimestamp(date, "short") : "", title: title, text: text, url: url};
            st.renderSkin("modRssFetcherItem", param);
         }
      } catch (err) {
         res.write(err);
      }
      this.cache.modRssFetcher = res.pop();
      this.cache.modRssFetcherLastCheck = new Date();
   }

   var skinParam = arguments[0] || {};
   skinParam.body = this.cache.modRssFetcher;
   skinParam.editLink = "true";
   this.renderSidebarModuleItem("modRssFetcher", skinParam);
}
