
app.modules.modCommentModeration = {
   title: "Comment Moderation",
   description: "Comment Moderation",
   version: "1.0",
   state: "beta",
   url: "",
   author: "Franz Philipp Moser was Barbara Ondrisek",
   authorEmail: "philipp.moser@knallgrau.at barbara.ondrisek@knallgrau.at",
   authorUrl: "http://www.knallgrau.at",
   authorOrganisation: "knallgrau.at",
   copyright: "(c) 2009 knallgrau.at",
   license: "twoday",
   licenseUrl: "",
   isSidebar: false,
   hasSiteSettings: true,
   skins: [],
   MODERATE_ANONYMOUS_COMMENTS: "1",
   MODERATE_ALL_COMMENTS: "2"
};

/**
 * on new sites disable comment moderation
 */
app.modules.modCommentModeration.evalNewSite = function(args) {
   var newSite = args[1];
   newSite.preferences.setProperty("modCommentModerationActive", null);
   return;
};

/**
 * Renders the module's preferences.
 */
app.modules.modCommentModeration.renderPreferences = function() {
   var active = this.preferences.getProperty("modCommentModerationActive");
   Module.prototype.renderSetupSpacerLine();
   var options = [
      {value: "0", display: getMessage("generic.no")},
      {value: app.modules.modCommentModeration.MODERATE_ANONYMOUS_COMMENTS, display: getMessage("modCommentModeration.moderateOnlyAnonymousCommets")},
      {value: app.modules.modCommentModeration.MODERATE_ALL_COMMENTS, display: getMessage("modCommentModeration.moderateAllCommets")}
   ];
   var param = {
      label: getMessage("modCommentModeration.Site.preferences"),
      field: Html.dropDownAsString({name: "modCommentModerationActive"}, options, active)
   };
   Module.prototype.renderSetupLine(param);
};

/**
 * Stores the module's settings in the site's preferences.
 */
app.modules.modCommentModeration.evalModulePreferences = function() {
   var moderationLevel = null;
   if (req.data.modCommentModerationActive == app.modules.modCommentModeration.MODERATE_ANONYMOUS_COMMENTS)
      moderationLevel = app.modules.modCommentModeration.MODERATE_ANONYMOUS_COMMENTS;
   if (req.data.modCommentModerationActive == app.modules.modCommentModeration.MODERATE_ALL_COMMENTS)
      moderationLevel = app.modules.modCommentModeration.MODERATE_ALL_COMMENTS;
   app.log("moderationLevel: " + moderationLevel);
   this.preferences.setProperty("modCommentModerationActive", moderationLevel);
};


/**
 * set comment online or offline
 */
app.modules.modCommentModeration.onEvalCommentModifyOnlineOffline = function(params) {
   var c = params[0];
   // all contentmanager and sysadmins post direct
   if (c.creator && (c.getSite().members.getMembershipLevel(c.creator) >= CONTENTMANAGER) || (session.user && session.user.sysadmin)) return;
   var moderationLevel = c.getSite().preferences.getProperty("modCommentModerationActive");
   if ((!c.creator && moderationLevel == app.modules.modCommentModeration.MODERATE_ANONYMOUS_COMMENTS)
         || moderationLevel == app.modules.modCommentModeration.MODERATE_ALL_COMMENTS)
      c.doSetOffline();
};


/**
 * show hint
 */
app.modules.modCommentModeration.renderCommentEdit = function(params) {
   if (path.Site) {
      var site = path.Site;
      var user = session.user;
      if (!site.preferences.getProperty("modCommentModerationActive")) return;
      if (session.user != this.creator) {
         res.write(getMessage("modCommentModeration.Comments.hint"));
         Html.tag("br");
      }
   }
};

/**
 * on delete comment
 */
app.modules.modCommentModeration.onAfterDeleteComment = function(params) {
   var c = params[0];
   // comment has been set online?
   if (c.online) return;

   this.offlineComments.removeChild(c);
   // check user status
   if (c.getSite().members.getMembershipLevel(session.user) >= CONTENTMANAGER || (session && session.user && session.user.sysadmin))
      res.redirect(this.site.href("modCommentModeration"));
};


/**
 * show text next to comment links in list
 */
app.modules.modCommentModeration.renderCommentLinks = function(params) {
   if (!this.online && this.ipaddress == req.data.http_remotehost) {
      Html.openTag("i");
      res.write(getMessage("modCommentModeration.Comment.renderCommentLinks.offline"));
      Html.closeTag("i");
      return;
   }
};


/**
 * add check access for action modCommentModeration
 */
app.modules.modCommentModeration.checkSiteAccess = function(params) {
   var action = params[0];
   var usr    = params[1];
   var level  = params[2];
   var url    = params[3];
   if (action == "modCommentModeration") {
      checkIfLoggedIn();
      if (!usr || (this.members.getMembershipLevel(usr) < CONTENTMANAGER && !usr.sysadmin))
         throw new DenyException("siteContribute");
   }
   return params;
};

app.modules.modCommentModeration.getTaskText = function() {
   var taskText = "";
   var site = res.handlers.site;
   if (site && site.commentsWaitingForModeration) {
      taskText += "(" + site.commentsWaitingForModeration.size() + ") ";
   }
   taskText += getMessage("Site.modCommentModeration.taskLabel");
   return taskText;
};

/**
 * render item in Site-manage
 */
app.modules.modCommentModeration.renderSiteManage = function(params) {
   if (this.preferences.getProperty("modCommentModerationActive")) {
      var param = {taskText: app.modules.modCommentModeration.getTaskText()};
      this.renderSkin("modCommentModerationSiteManage", param);
   }
};

/**
 * hook into contributor menu
 */
app.modules.modCommentModeration.getContributorTasks = function(params) {
   if (this.preferences.getProperty("modCommentModerationActive")) {
      var items = params[0];
      items[items.length] = {text: app.modules.modCommentModeration.getTaskText(), href: this.href("modCommentModeration"), action: "Site/modCommentModeration"};
   }
   return items;
};


/**
 * Hook into res.message

app.modules.modCommentModeration.addResponseMessageText = function(memberMgr, param) {
   var site = res.handlers.site;
   if (site && site.preferences.getProperty("modCommentModerationActive")) {
      // check user rights
      var u = root.getUserByKey(param.name, "local");
      app.log("## u: " + u);
      if (!u || (site.members.getMembershipLevel(u) < CONTENTMANAGER && !u.sysadmin))
         return;
      // ok everything all right append to res.message
      if (res) {
         res.message = res.message ? res.message + app.modules.modCommentModeration.getTaskText() : app.modules.modCommentModeration.getTaskText();
      }
   }
};

knallgrau.Event.registerObserver("MemberMgr", "afterSuccessfulLoginAction", app.modules.modCommentModeration.addResponseMessageText, null, "app.modules.modCommentModeration.addResponseMessageText");
 */
