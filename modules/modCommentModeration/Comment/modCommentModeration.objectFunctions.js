
/**
 * function sets the comment online
 */
function doSetOnline() {
   this.online = 1;
   this.waitingformoderation = 0;
   this.story.comments.add(this);
   this.story.offlineComments.removeChild(this);
   // call hook of comment onAfterEvalComment
   this.applyModuleMethods("onAfterEvalComment", [this, {}], false);
}

function doSetOffline() {
   this.online = 0;
   this.waitingformoderation = 1;
   this.story.comments.removeChild(this);
   this.story.offlineComments.add(this);
}

