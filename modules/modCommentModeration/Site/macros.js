
/**
 * renders navigation for listing stories
 */
function modContentModerationListNavigation_macro(param) {
   var items = this.modContentModerationGetListNavigationItems();
   res.push();
   this.stories.renderListNavigationItems(items, param);
   param.items = res.pop();
   this.stories.renderSkin("listNavigationContainer", param);
   return;
}
