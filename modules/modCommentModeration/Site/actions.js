
/**
 * manage comments
 */
function modCommentModeration_action() {

   if (req.data.publishContent || req.data.deleteContent) {
      var count = 0;
      for (var i in req.data) {
         if (i.indexOf("item_") == 0) {
            var id = i.substr("item_".length);
            var comment = Comment.getById(id);
            if (req.data.publishContent) 
               comment.doSetOnline();
            else if (req.data.deleteContent) {
               var story = comment.story;
               story.deleteComment(comment);
            }
            count++;
         }
      }
      if (count > 0) {
         if (req.data.publishContent) 
            res.message = new Message("modCommentModeration.setOnline");
         else if (req.data.deleteContent)
            res.message = new Message("modCommentModeration.delete");
         res.redirect(this.href("modCommentModeration"));
      } else {
         res.message = "Hmmm...";
      }
   }

   var items;
   res.data.commentlist = renderList(this.commentsWaitingForModeration, "modCommentModeration.listitem", 10, req.data.page);
   res.data.pagenavigation = renderPageNavigation(this.offlineComments, this.href("modCommentModeration"), 10, req.data.page);

   res.data.title = getMessage("Site.modCommentModeration.title");
   res.data.body = this.renderSkinAsString("modCommentModeration", {modCommentModerationCommentOnEntry: getMessage("modCommentModeration.onEntry")});
   this.renderPage();
}

