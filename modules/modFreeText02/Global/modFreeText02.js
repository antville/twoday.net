
app.modules.modFreeText02 = {
   title: "Free Text (2)",
   description: "renders freely definable HTML Text",
   version: "",
   state: "beta",
   url: "",
   author: "Wolfgang Kamir",
   authorEmail: "wolfgang@knallgrau.at",
   authorUrl: "http://www.knallgrau.at",
   authorOrganisation: "knallgrau.at",
   copyright: "(c) 2004 knallgrau.at",
   license: "twoday",
   licenseUrl: "http://www.twoday.org",
   isSidebar: true,
   hasSiteSettings: true
};


/**
 * Reads userdefined free HTML text from site's preferences and hands it over to "modFreeText02" skin as parameter.
 * renderSidebarModuleItem function displays the module in sidebar (+ header).
 *
 * @see Site.renderSidbarModuleItem
 */
app.modules.modFreeText02.renderSidebarItem = function() {
   var p = {};
   p.body = this.preferences.getProperty("modfreetext02text") ? this.preferences.getProperty("modfreetext02text") : "";
   p.editLink = true;
   this.renderSidebarModuleItem("modFreeText02", p);
   return;
}


/**
 * Renders the module's preferences.
 */
app.modules.modFreeText02.renderPreferences = function() {
   var param = {};
   param.label = getMessage("modFreeText02.wizard.admin.settingsText");
   param.value = this.preferences.getProperty("modfreetext02text") ?
                 this.preferences.getProperty("modfreetext02text") : "";
   Module.prototype.renderSkin("modFreeText02SetupLine", param);
};


/**
 * Stores the userdefined free HTML text in the site's prefernces.
 */
app.modules.modFreeText02.evalModulePreferences = function() {
   this.preferences.setProperty("modfreetext02text", req.data.modFreeText02Text);
};
