
/**
 * Check if a user is allowed to access the modListeningSearch-action of this Site.
 *
 * @param usr User
 * @throws DenyException
 */
function checkAccessModListeningSearch(usr) {
   this.checkAccessManage(usr);
   return;
}
