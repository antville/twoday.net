
/**
 * If the user clicks on the "search on amazon" button this function renders some hidden fields that are filled
 * with the results of the amazon booksearch. urlparameters are 'title' and 'interpreter' of the album to search for.
 * The resulting skin is rendered to a hidden iframe in the skin "modListeningSetup". In the onLoad function of the iframe, the values are written
 * out as list of links (search result links).
 */
function modListeningSearch_action() {
   var amazon = new knallgrau.Amazon();
   
   // search results from popmusic search
   var result = amazon.searchPopMusic((req.data.interpreter || "") + " " + (req.data.title || ""));
   default xml namespace = "http://webservices.amazon.com/AWSECommerceService/2005-10-05";

   var items = result..Item;
   var popLength = items.length();
   if (popLength != 0) {
      res.write("<div class=\"message\">" + getMessage("modListening.modListeningSetup.result.intro") + "</div>");

      res.write("<h3>" + getMessage("modListening.modListeningSetup.TopResultsPopMusic") + "</h3>");
      for (var i = 0; i < items.length(); i++) {
         var url = items[i].DetailPageURL.text();
         var title = items[i].ItemAttributes.Title.text();
         var image = items[i].MediumImage.URL.text();
         var interpreters = items[i]..Artist;
         var interpreter = "";
         for (var j = 0; j < interpreters.length(); j++) {
            if (j != 0) interpreter += ", ";
            interpreter += interpreters[j].text();
         }
         title = title.toString().replace("'", "&acute;");
         interpreter = interpreter.toString().replace("'", "&acute;");
         res.write("<ul class=\"searchResultList\">\n");
         res.write('<li><a href="' + url + '" target="_blank" onclick="addItem(\''+url+'\', \''+title+'\', \''+image+'\', \''+interpreter+'\'); return false;">' + title + '</a></li>\n');
         res.write("</ul>\n");
      }
   }
   
   result = amazon.searchClassicalMusic((req.data.interpreter || "") + " " + (req.data.title || ""));
   items = result..Item;
   if (popLength == 0 && items.length() == 0) {
      res.write("<div class=\"message\">" + getMessage("modListening.modListeningSetup.NoResults") + "</div>");
   } else {
      res.write("<br /><h3>" + getMessage("modListening.modListeningSetup.TopResultsClassical") + "</h3>");
      for (var i = 0; i < items.length(); i++) {
         var url = items[i].DetailPageURL.text();
         var title = items[i].ItemAttributes.Title.text();
         var image = items[i].MediumImage.URL.text();
         var interpreters = items[i]..Artist;
         var interpreter = "";
         for (var j = 0; j < interpreters.length(); j++) {
            if (j != 0) interpreter += ", ";
            interpreter += interpreters[j].text();
         }
         title = title.toString().replace("'", "&acute;");
         interpreter = interpreter.toString().replace("'", "&acute;");
         res.write("<ul class=\"searchResultList\">\n");
         res.write('<li><a href="' + url + '" target="_blank" onclick="addItem(\''+url+'\', \''+title+'\', \''+image+'\', \''+interpreter+'\'); return false;">' + title + '</a></li>\n');
         res.write("</ul>\n");
      }
      res.write("<br />\n");
   }
   default xml namespace = "http://www.w3.org/1999/xhtml";
   return;
}
