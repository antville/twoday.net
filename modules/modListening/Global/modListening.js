
app.modules.modListening = {
   title: "Listening",
   description: "Displays a list with music CDs you currently listen to.",
   version: "",
   state: "beta",
   url: "",
   author: "Wolfgang Kamir",
   authorEmail: "wolfgang@knallgrau.at",
   authorUrl: "http://www.knallgrau.at",
   authorOrganisation: "knallgrau.at",
   copyright: "(c) 2004 knallgrau.at",
   license: "antville",
   licenseUrl: "http://www.antville.org",
   isSidebar: true,
   type: MODULE_TYPE_LIST,
   skins: ["Site.modListening", "Site.modListeningItem", "Site.modListeningDivider"]
};


/**
 * Renders module in the sidebar. The renderSidebarItem function of modListening at first reads exisiting
 * musiclist entries from the site's preferences.
 * After reading the items and splitting them up, for every item the "modListening" skin is rendered and inbetween the skin "modListeningDivider".
 * Linked alltogether the resulting string is handed over to the skin "modListening" as body and rendered to the sidebar.
 */
app.modules.modListening.renderSidebarItem = function(param) {
   if (!param) param = {};
   var ListeningTitles = app.modules.modListening.getListeningProperty(this, "titles");
   var ListeningUrls = app.modules.modListening.getListeningProperty(this, "urls");
   var ListeningInterpreters = app.modules.modListening.getListeningProperty(this, "interpreters");
   var ListeningImages = app.modules.modListening.getListeningProperty(this, "images");

   res.push();
   for (var i=0; i<ListeningTitles.length; i++) {
      var p = {};
      p.ListeningTitle = (ListeningUrls[i].length > 12) ? '<a href="' + ListeningUrls[i] + '" target="_blank">' + ListeningTitles[i] + '</a>' : ListeningTitles[i];
      p.ListeningInterpreter = ListeningInterpreters[i];
      var Img = this.images.get(ListeningImages[i]);
      if (Img) {
         p.ListeningImage = (ListeningUrls[i].length > 12) ? '<a href="' + ListeningUrls[i] + '" target="_blank">' + Img.getUrl() + '</a>' : '<img src="' + Img.getUrl() + '" />';
      }
      else if (ListeningImages[i].length > 12) {
         p.ListeningImage = (ListeningUrls[i].length > 12) ? '<a href="' + ListeningUrls[i] + '" target="_blank"><img src="' + ListeningImages[i] + '" /></a>' : '<img src="' + ListeningImages[i] + '" />';
      }
      else {
         p.ListeningImage = "";
      }
      if (i != 0) this.renderSkin("modListeningDivider");
      this.renderSkin("modListeningItem", p);
   }
   var p = {};
   p.Listening = res.pop();
   res.push();
   this.renderSkin("modListening", p);
   var p = {};
   p.body = res.pop();
   p.editLink = true;
   this.renderSidebarModuleItem("modListening", p);
}


/**
 * Generates the arranger-field of the module's preferences and hands it over to the "modListeningSetup" skin.
 */
app.modules.modListening.renderPreferences = function() {

   var p = {};
   p.ListeningArranger = app.modules.modListening.renderListeningArranger(this);
   Module.prototype.renderSkin("modListeningSetup", p);
};


/**
 * Is called when user clicks the "save" button in module preferences. The values in the item-arranger are read and split up in title, interpreter, url, image.
 * In the arranger itself the items are saved as strings separated by the sequence ":#:". After pushing the values into an array, they are stored in the
 * site's preferences by calling toSource array function.
 */
app.modules.modListening.evalModulePreferences = function() {
   var ListeningTitles = new Array();
   var ListeningUrls = new Array();
   var ListeningInterpreters = new Array();
   var ListeningImages = new Array();

   try {
      var ListeningArrangerValues = JSON.parse(req.data.ListeningArrangerValues);
   } catch (err) {
      return;
   }
   if (!ListeningArrangerValues)
      var ListeningArrangerValues = new Array();

   ListeningArrangerValues = ListeningArrangerValues.slice(0,20);
   
   for (var i=0; i<ListeningArrangerValues.length; i++) {
      var temp = ListeningArrangerValues[i].split(":#:");
      ListeningTitles.push(stripTags(temp[0]));
      ListeningUrls.push(stripTags(temp[1]));
      ListeningInterpreters.push(stripTags(temp[2]));
      ListeningImages.push(stripTags(temp[3]));
   }

   this.preferences.setProperty("modlistening_titles", ListeningTitles.toSource());
   this.preferences.setProperty("modlistening_urls", ListeningUrls.toSource());
   this.preferences.setProperty("modlistening_interpreters", ListeningInterpreters.toSource());
   this.preferences.setProperty("modlistening_images", ListeningImages.toSource());
};


/**
 * Gets an array containing the titles, urls, interpreters, images of all list items. if nothing is found, an empty array is returned.
 *
 * @return Array containing either titles, urls, interpreters or images
 */
app.modules.modListening.getListeningProperty = function(obj, name) {
   try {
      var arr = JSON.parse(obj.preferences.getProperty("modlistening_" + name));
   } catch (err) {
      ;
   }
   if (!arr)
      var arr = new Array();

   return arr;
}


/**
 * Creates the item arranger field for module's preferences. First all existing items are read, then pushed into an array that is used to create
 * a value-filled dropDown-List. This List is handed over to the skin "modListening", which also consists of three buttons to remove and reposition
 * items in the list.
 */
app.modules.modListening.renderListeningArranger = function(obj) {
   var ListeningTitles = app.modules.modListening.getListeningProperty(obj, "titles");
   var ListeningUrls = app.modules.modListening.getListeningProperty(obj, "urls");
   var ListeningInterpreters = app.modules.modListening.getListeningProperty(obj, "interpreters");
   var ListeningImages = app.modules.modListening.getListeningProperty(obj, "images");

   var ListeningValues = [];
   for (var i=0; i<ListeningTitles.length; i++) {
      ListeningValues.push({value: ListeningTitles[i] + ":#:" + ListeningUrls[i] + ":#:" + ListeningInterpreters[i] + ":#:" + ListeningImages[i], display: ListeningTitles[i] + " (" + ListeningInterpreters[i] + ")"});
   }

   var p = {};
   p.ListeningArranger = Html.dropDownAsString({name: "ListeningArranger", id: "ListeningArranger", size: 12, multiple: "multiple", style: "width:440px;", onchange: "getItem(); return false;"}, ListeningValues);

   return Module.prototype.renderSkinAsString("modListeningArranger", p);
}