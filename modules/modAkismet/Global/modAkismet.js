
app.modules.modAkismet = {
   title: "Akismet Anti Spam",
   description: "Checks comments, trackbacks for Spam",
   version: "1.0",
   state: "stable",
   url: "",
   author: "Franz Philipp Moser",
   authorEmail: "philipp.moser@knallgrau.at",
   authorUrl: "http://www.knallgrau.at",
   authorOrganisation: "knallgrau.at",
   copyright: "(c) 2008 knallgrau.at",
   license: "twoday",
   licenseUrl: "",
   hasSiteSettings: true,
   skins: []
};


/**
 * Renders the module's preferences.
 */
app.modules.modAkismet.renderPreferences = function() {
   Module.prototype.renderSetupSpacerLine();
   var param = {
      label: getMessage("modAkismet.preferences.modAkismetKey.header"),
      field: Html.input({name: "modAkismetApiKey", id: "modAkismetApiKey", value: this.preferences.getProperty("modAkismetApiKey") ? this.preferences.getProperty("modAkismetApiKey") : ""})
   };
   Module.prototype.renderSetupLine(param);
};


/**
 * Stores the module's settings in the site's preferences.
 */
app.modules.modAkismet.evalModulePreferences = function() {
   this.preferences.setProperty("modAkismetApiKey", stripTags(req.data.modAkismetApiKey));
};


/**
 * Triggered before a comment is evaled
 * @param   site   HopObject Site, optional
 */
app.modules.modAkismet.onAfterModJCaptchaEvalComment = function(param) {
   var reqData = param[0];
   var creator = param[1];
   var site = res.handlers.site;
   // check if SPAM (anonymous only?)
   if (!creator) {
   // dont check if comment moderation is active because comments will always be offline
      if (site && site.preferences.getProperty("modCommentModerationActive")) {
         return;
      }
      app.log("[modAkismet] Akismet Call");
      var param = new Object();
      param.apiKey = site.preferences.getProperty("modAkismetApiKey") ? site.preferences.getProperty("modAkismetApiKey") : app.properties.akismetApiKey;
      param.blogUrl = site.href();
      param.permaLink = "";
      param.contentType = "comment";
      param.authorName = reqData.creatorName;
      param.authorEmail = reqData.creatorEmail;
      param.authorURL = reqData.creatorUrl;
      param.content = (reqData.content_title || "") + " " + (reqData.content_text || "");
      app.invokeAsync(app.modules.modAkismet, "countCalls", [{site: res.handlers.site}], (5).seconds());
      try {
         var result = knallgrau.Akismet.isSpam(param);
      } catch (e) {
         app.log("[modAkismet] Akismet Call failed: " + e);
         result = false;
      }
      if (result) {
         // app.invokeAsync(app.modules.modAkismet, "countSpam", [{site: res.handlers.site}], (5).seconds());
   	 // app.log("[modAkismet] Is SPAM param: " + uneval(param));
         // app.log("[modAkismet] result: " + uneval(result));
   	 // check if contentModeration is enabled
   	 
         // throw new Error(getMessage("modAkismet.Comment.isSpam"));
      }
   }
   return;
};


/**
 * Triggered when Trackback is evaled
 * @param   req.data   HopObject Site, optional
 */
app.modules.modAkismet.onAfterSpamProtectionEvalModTrackback = function(param) {
   var reqData = param[0];
   var param = new Object();
   var site = res.handlers.site;
   param.apiKey = site.preferences.getProperty("modAkismetApiKey") ? site.preferences.getProperty("modAkismetApiKey") : app.properties.akismetApiKey;
   param.blogUrl = site.href();
   param.permaLink = res.handlers.story ? res.handlers.story.href() : null;
   param.contentType = "trackback";
   //param.authorName = req.data.creatorName;
   //param.authorEmail = req.data.creatorEmail;
   param.authorURL = reqData.url;
   param.content = (reqData.title || "") + " " + (reqData.excerpt || "");
   app.invokeAsync(app.modules.modAkismet, "countCalls", [{trackback: true, site: res.handlers.site}], (5).seconds());
   if (knallgrau.Akismet.isSpam(param)) {
      app.invokeAsync(app.modules.modAkismet, "countSpam", [{trackback: true, site: res.handlers.site}], (5).seconds());
      app.log("[modAkismet] Trackback is Spam " + uneval(req.data));      
      throw new Error(getMessage("modAkismet.Comment.isSpam"));
   }
};


/**
 * increases counters
 *
 * @param param Object
 *              site String (optional) a Site Object
 */
app.modules.modAkismet.countCalls = function(param) {
   var site = param ? param.site : null;
   // set Root Count
   if (param && param.trackback) {
      var modAkismetSiteTrackbackCallCount = root.preferences.getProperty("modAkismetTrackbackCallCount") && isNumber(parseInt(root.preferences.getProperty("modAkismetTrackbackCallCount"), 10)) ? parseInt(root.preferences.getProperty("modAkismetTrackbackCallCount"), 10) : 0;
      root.preferences.setProperty("modAkismetTrackbackCallCount", ++modAkismetSiteTrackbackCallCount);
   } else {
      var modAkismetRootCallCount = root.preferences.getProperty("modAkismetCallCount") && isNumber(parseInt(root.preferences.getProperty("modAkismetCallCount"), 10)) ? parseInt(root.preferences.getProperty("modAkismetCallCount"), 10) : 0;
      root.preferences.setProperty("modAkismetCallCount", ++modAkismetRootCallCount);
   }
   // set Site Count
   if (site) {
      if (param && param.trackback) {
         var modAkismetSiteTrackbackCallCount = site.preferences.getProperty("modAkismetTrackbackCallCount") && isNumber(parseInt(site.preferences.getProperty("modAkismetTrackbackCallCount"), 10)) ? parseInt(site.preferences.getProperty("modAkismetTrackbackCallCount"), 10) : 0;
         site.preferences.setProperty("modAkismetTrackbackCallCount", ++modAkismetSiteTrackbackCallCount);
      } else {
         var modAkismetSiteCallCount = site.preferences.getProperty("modAkismetCallCount") && isNumber(parseInt(site.preferences.getProperty("modAkismetCallCount"), 10)) ? parseInt(site.preferences.getProperty("modAkismetCallCount"), 10) : 0;
         site.preferences.setProperty("modAkismetCallCount", ++modAkismetSiteCallCount);
      }
   }
   // reset counters?
   // send mail?
   var mailBody = "";
   var now = new Date();
   var mailSubject = app.name + " Akismet Call Nofification (" + now + ")";
   var mailParam = new Object();
   if (modAkismetRootCallCount % app.properties.akismetRootNotificationCount == 0) {
      mailParam.count = modAkismetRootCallCount;
      mailParam.spam = root.preferences.getProperty("modAkismetSpamCount");
      mailBody += root.renderSkinAsString("modAkismetMailBody", mailParam);
   }
   if (site && modAkismetSiteCallCount % app.properties.akismetSiteNotificationCount == 0) {
      mailParam.count = modAkismetSiteCallCount;
      mailParam.spam = site.preferences.getProperty("modAkismetSpamCount");
      mailBody += site.renderSkinAsString("modAkismetMailBody", mailParam);
   }
   if (mailBody) {
      sendMail(root.preferences.getProperty("sys_email"), root.preferences.getProperty("sys_email"), mailSubject, mailBody);
   }
};


/**
 * increases spam counters
 *
 * @param param Object
 *              site Site (optional) the site that should be updated
 *              trackback Boolean (optional) if the counter for Trackbackspam should be increased
 */
app.modules.modAkismet.countSpam = function(param) {
   var site = param ? param.site : null;
   // set Root Count
   if (param && param.trackback) {
      var modAkismetRootTrackbackSpamCount = root.preferences.getProperty("modAkismetTrackbackSpamCount") && isNumber(parseInt(root.preferences.getProperty("modAkismetTrackbackSpamCount"), 10)) ? parseInt(root.preferences.getProperty("modAkismetTrackbackSpamCount"), 10) : 0;
      root.preferences.setProperty("modAkismetTrackbackSpamCount", ++modAkismetRootTrackbackSpamCount);
   } else {
      var modAkismetRootSpamCount = root.preferences.getProperty("modAkismetSpamCount") && isNumber(parseInt(root.preferences.getProperty("modAkismetSpamCount"), 10)) ? parseInt(root.preferences.getProperty("modAkismetSpamCount"), 10) : 0;
      root.preferences.setProperty("modAkismetSpamCount", ++modAkismetRootSpamCount);
   }
   // set Site Count
   if (site) {
      if (param && param.trackback) {
         var modAkismetSiteTrackbackSpamCount = site.preferences.getProperty("modAkismetTrackbackSpamCount") && isNumber(parseInt(site.preferences.getProperty("modAkismetTrackbackSpamCount"), 10)) ? parseInt(site.preferences.getProperty("modAkismetTrackbackSpamCount"), 10) : 0;
         site.preferences.setProperty("modAkismetTrackbackSpamCount", ++modAkismetSiteTrackbackSpamCount);
      } else {
         var modAkismetSiteSpamCount = site.preferences.getProperty("modAkismetSpamCount") && isNumber(parseInt(site.preferences.getProperty("modAkismetSpamCount"), 10)) ? parseInt(site.preferences.getProperty("modAkismetSpamCount"), 10) : 0;
         site.preferences.setProperty("modAkismetSpamCount", ++modAkismetSiteSpamCount);
      }
   }
};


//app.modules.modAkismet.checkSiteAccess = function(param) {
   //look for action!
//   res.handlers.site.checkEdit(param[0], param[1]);
//};
