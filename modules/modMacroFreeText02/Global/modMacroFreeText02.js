
app.modules.modMacroFreeText02 = {
   title: "Macro Free Text (2)",
   description: "renders freely definable Text allowing macros",
   version: "1.0",
   state: "stable",
   url: "",
   author: "Franz Philipp Moser",
   authorEmail: "philipp.moser@knallgrau.at",
   authorUrl: "http://www.knallgrau.at",
   authorOrganisation: "knallgrau.at",
   copyright: "(c) 2009 knallgrau.at",
   license: "twoday",
   licenseUrl: "http://www.twoday.org",
   isSidebar: true,
   hasSiteSettings: true,
   isRestricted: true
};


/**
 * Reads userdefined free HTML text from site's preferences and hands it over to "modFreeText01" skin as parameter.
 * renderSidebarModuleItem function displays the module in sidebar (+ header).
 *
 * @see Site.renderSidbarModuleItem
 */
app.modules.modMacroFreeText02.renderSidebarItem = function(param) {
   var p = {};
   p.body = this.preferences.getProperty("modmacrofreetext02text") ? this.preferences.getProperty("modmacrofreetext02text") : "";
   var bodySkin = createSkin(p.body);
   p.body = this.renderSkinAsString(bodySkin);
   p.editLink = true;
   this.renderSidebarModuleItem("modMacroFreeText02", p);
   return;
}


/**
 * Renders the module's preferences.
 */
app.modules.modMacroFreeText02.renderPreferences = function() {
   var param = {};
   param.label = getMessage("modMacroFreeText02.wizard.admin.settingsText");
   param.value = this.preferences.getProperty("modmacrofreetext02text") ?
                 this.preferences.getProperty("modmacrofreetext02text") : "";
   Module.prototype.renderSkin("modMacroFreeText02SetupLine", param);
};


/**
 * Stores the userdefined free HTML text in the site's prefernces.
 */
app.modules.modMacroFreeText02.evalModulePreferences = function() {
   this.preferences.setProperty("modmacrofreetext02text", req.data.modMacroFreeText02Text);
};
