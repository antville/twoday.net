
app.modules.modSocialBookmarks = {
   title: "Social Bookmarking Services Links",
   description: "Adds Links to typical social bookmarking services",
   version: "0.1",
   state: "stable",
   url: "",
   author: "Franz Philipp Moser",
   authorEmail: "philipp.moser@knallgrau.at",
   authorUrl: "http://www.knallgrau.at",
   authorOrganisation: "knallgrau.at",
   copyright: "(c) 2008 knallgrau.at",
   license: "twoday",
   licenseUrl: "",
   isRestricted: true
};
