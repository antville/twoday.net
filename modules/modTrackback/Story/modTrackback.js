
/**
 * function creates a new Trackback Entry and writes a xml-encoded response message to the requesting site.
 */
function modTrackback_action(param) {
   res.contentType = "text/xml";
   try {
      var result = this.modTrackbackEval(req.data);
      res.write("<?xml version=\"1.0\" encoding=\"" + app.properties.charset + "\"?><response><error>0</error></response>");
   } catch (err) {
      res.write("<?xml version=\"1.0\" encoding=\"" + app.properties.charset + "\"?><response><error>1</error><message>An error occured while evaluating your ping (" + encodeXml(uneval(err)) +")</message></response>");
   }
}


/**
 * function evaluates and creates a new Trackback Entry
 * @param Obj Data of the new Trackback Entry
 * @param.title
 * @param.excerpt
 * @param.blog_name
 * @param.url
 */
function modTrackbackEval(param) {
   var modTrackbackActive = this.site.preferences.getProperty("modTrackbackActive");
   if (!modTrackbackActive) {
      throw new Error("Trackback is disabled for this blog");
   }
   if (!param || !param.url || !param.excerpt || !param.title) {
      throw new Error("no url, excerpt or title");
   }
   if (!Http.evalUrl(param.url)) {
      throw new Error("problem with url:" + param.url);
   }

   // track and handle Trackback-Spam
   if (root.modSpamProtection && root.modSpamProtection.trackbacks) {
      root.modSpamProtection.trackbacks.handleSpam(param);
   }

   this.applyModuleMethods("onAfterSpamProtectionEvalModTrackback", [param]);
   var tb = new ModTrackback(this.site, null, param.http_remotehost);
   tb.title = param.title;
   tb.text = param.excerpt;

   this.modTrackbacks.add(tb);

   tb.content.setProperty("blog_name", param.blog_name);
   tb.content.setProperty("url", param.url);

   return;
}


/**
 * macro renders a textarea for entering Trackback URLs that should be pinged after a story was created.
 */
function modTrackbackPings_macro(param) {
   var modTrackbackActive = path.Site.preferences.getProperty("modTrackbackActive");
   if (!modTrackbackActive)
      return;

   if (param && param.as == "editor") {
      param.name = "modTrackbackPings";
      Html.textArea(param);
   } else {
      try {
         var oldPings = JSON.parse(this.content.getProperty("modtrackbacksentpings"));
      } catch(err) {
         ;
      }
      if (!oldPings) var oldPings = new Array();
      Html.hidden({name: "modTrackbackSentPings", value: oldPings.toSource()});
      for (var i=0; i<oldPings.length; i++) {
         if (!oldPings[i]) continue;
         var param2 = new Object();
         param2.ping = oldPings[i];
         this.renderSkin("modTrackbackSentPing", param2);
      }
   }
   return;
}


/**
 * macro renders all received Trackback Entries for the story
 * @param.as "link"
 */
function modTrackbackCounter_macro(param) {
   var modTrackbackActive = res.handlers.site.preferences.getProperty("modTrackbackActive");
   if (!modTrackbackActive) return;

   var trackbackCount = this.modTrackbacks.size();
   var linkParam = new Object();
   linkParam.href = this.href() + "#trackbacks";
   var linkflag = (param.as === "link" || !param.as && trackbackCount > 0);
   if (linkflag) Html.openTag("a", linkParam);

   if (trackbackCount === 0) res.write(param.no || getMessage("modTrackback.no"));
   else if (trackbackCount === 1) res.write(param.one || getMessage("modTrackback.one"));
   else res.write(trackbackCount + (param.more || " " + getMessage("modTrackback.more")));

   if (linkflag) Html.closeTag("a");
}


/**
 * macro renders the Trackback URL for the story
 */
function modTrackbackShowUrl_macro(param) {
   var modTrackbackActive = this.site.preferences.getProperty("modTrackbackActive");
   if (!modTrackbackActive)
      return;

   this.renderSkin("modTrackbackUrl");
   return;
}


/**
 * macro renders the Trackback URL for the story
 */
function modTrackbackUrl_macro(param) {
   var modTrackbackActive = this.site.preferences.getProperty("modTrackbackActive");
   if (!modTrackbackActive)
      return;

   res.write(this.href("modTrackback"));
   return;
}


/**
 * macro renders all received Trackback Entries for the story
 * @skin Story.modTrackbackEntries
 */
function modTrackbackEntries_macro(param) {
   var modTrackbackActive = this.site.preferences.getProperty("modTrackbackActive");
   if (!modTrackbackActive)
      return;

   res.push();
   for (var i=0; i<this.modTrackbacks.size(); i++) {
      var tb = this.modTrackbacks.get(i);
      tb.renderSkin("main");
   }
   var entries = res.pop();
   if (entries == "") return;
   this.renderSkin("modTrackbackEntries", {entries: entries});
   return;
}


/**
 * function deletes a trackback entry
 * @param Obj Trackback-Object that should be deleted
 * @return String Message indicating success/failure
 */
function modTrackbackDelete(trackbackObj) {
   this.modTrackbacks.removeChild(trackbackObj);
   trackbackObj.remove();
   return new Message("trackbackDelete");
}


function modTrackbackForm_macro(param) {
   var modTrackbackActive = path.Site.preferences.getProperty("modTrackbackActive");
   if (!modTrackbackActive)
      return;
   this.renderSkin("modTrackbackForm");
}
