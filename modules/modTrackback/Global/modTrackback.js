
app.modules.modTrackback = {
   title: "Trackbacks",
   description: "Enables the user to use Trackback",
   version: "",
   state: "beta",
   url: "",
   author: "Wolfgang Kamir",
   authorEmail: "wolfgang@knallgrau.at",
   authorUrl: "http://www.knallgrau.at",
   authorOrganisation: "knallgrau.at",
   copyright: "(c) 2004 knallgrau.at",
   license: "twoday",
   licenseUrl: "http://www.twoday.org",
   isSidebar: false,
   hasSiteSettings: true,
   skins: ["Story.modTrackbackUrl", "Story.modTrackbackEntries", "ModTrackback.main", "Story.modTrackbackForm", "Story.modTrackbackSentPing"]
};


app.modules.modTrackback.evalNewSite = function(args) {
   var creator = args[0];
   var newSite = args[1];
   newSite.preferences.setProperty("modTrackbackActive", null);
   return;
}

/**
* function pings all given Trackback URLs after a story was created successfully
* @element the caller of notifyObservers
* @args Arguments of evalNewStory or evalStory
* @oldFunc original evalNewStory
* @obj StoryMgr-Object (use this instead of "this")
*/
// result, Args, oldFunc, obj
app.modules.modTrackback.pingUrls = function(element, args) {
   var param = args.param;
   if (!param.modTrackbackPings) return;
   var story = args.story;
   try {
      var oldPings = JSON.parse(param.modTrackbackSentPings);
   } catch(err) {
      ;
   }
   if (!oldPings)
      var oldPings = new Array();

   var jlsTitle = new java.lang.String(stripTags(story.getRenderedContentPart("title")));
   var jlsText = new java.lang.String(stripTags(story.getRenderedContentPart("text")));
   var jlsBlog = new java.lang.String(story.site.getName());

   if (jlsText.length() > 255) {
      jlsText = new java.lang.String(jlsText.substring(0, 253)  + "...");
   }

   var newPings = param.modTrackbackPings.split("\r\n");
   for (var i = 0; i < newPings.length; i++) {
      if (!newPings[i]) continue;
      if (Array.contains(oldPings, newPings[i])) continue;
      try {
         var client = new Packages.org.apache.commons.httpclient.HttpClient();
         var method = new Packages.org.apache.commons.httpclient.methods.PostMethod(newPings[i]);
         method.addParameter("url", story.href());
         method.addParameter("blog_name", jlsBlog);
         method.addParameter("title", jlsTitle);
         method.addParameter("excerpt", jlsText);
         // send the POST request
         client.executeMethod(method);
         var response = method.getResponseBodyAsString();
         oldPings.push(newPings[i]);
      } catch (err) {
         app.log("TRACKBACK ERROR " + err);
         continue;
      } finally {
         if (method) {
            method.releaseConnection();
            method.recycle();      
         }
      }
   }
   story.content.setProperty("modtrackbacksentpings", oldPings.toSource().toString());
   return;
};
knallgrau.Event.registerObserver("StoryMgr", "afterEvalNewStory", app.modules.modTrackback.pingUrls, null, "app.modules.modTrackback.pingUrls");
knallgrau.Event.registerObserver("Story", "afterEvalStory", app.modules.modTrackback.pingUrls, null, "app.modules.modTrackback.pingUrls");


app.modules.modTrackback.deleteTrackbacks = function(element, args) {
   var story = args.story;
   if (element.modTrackbacks) {
      for (var i = story.modTrackbacks.size(); i > 0; i--)
         story.modTrackbacks.get(i-1).remove();
   }
   return;
}
knallgrau.Event.registerObserver("StoryMgr", "afterDeleteStory", app.modules.modTrackback.deleteTrackbacks, null, "app.modules.modTrackback.deleteTrackbacks");


app.modules.modTrackback.renderPreferences = function() {
   Module.prototype.renderSetupSpacerLine();
   Module.prototype.renderSetupHeader({header: getMessage("modTrackback.pref.title")});
   res.push();
   Html.checkBox({name: "modTrackbackActive", value: "1", checked: res.handlers.site.preferences.getProperty("modTrackbackActive") ? "checked" : null});
   var inputField = res.pop();
   var param = {
      label: getMessage("modTrackback.pref.checkbox"),
      field: inputField
   };
   Module.prototype.renderSetupLine(param);
};


app.modules.modTrackback.evalModulePreferences = function() {
   res.handlers.site.preferences.setProperty("modTrackbackActive", req.data.modTrackbackActive);
};
