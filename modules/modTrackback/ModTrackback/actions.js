
/**
 * delete action
 */
function delete_action() {
   if (req.data.cancel)
      res.redirect(this.story.href());
   else if (req.data.remove) {
      try {
         var url = this.story.href();
         res.message = this.story.modTrackbackDelete(this);
         res.redirect(url);
      } catch (err) {
         res.message = err.toString();
      }
   }

   res.data.action = this.href(req.action);
   res.data.title = this.site.title;
   var skinParam = {
      description: getMessage("modTrackback.deleteDescription"),
   };
   res.data.body = this.renderSkinAsString("delete", skinParam);
   this.site.renderPage();
   return;
}
