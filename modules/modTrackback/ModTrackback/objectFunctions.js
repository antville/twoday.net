
/**
 * constructor function for comment objects
 */
function constructor(site, creator, ipaddress) {
   this.site = site;
   this.online = 1;
   this.editableby = EDITABLEBY_ADMINS;
   this.ipaddress = ipaddress;
   this.creator = creator;
   this.createtime = this.modifytime = new Date();
}

