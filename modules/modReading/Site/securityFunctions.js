
/**
 * Check if a user is allowed to access the modReadingSearch-action of this Site.
 *
 * @param usr User
 * @throws DenyException
 */
function checkAccessModReadingSearch(usr) {
   this.checkAccessManage(usr);
   return;
}
