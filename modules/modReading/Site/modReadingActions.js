
/**
 * If the user clicks on the "search on amazon" button this function renders some hidden fields that are filled
 * with the results of the amazon booksearch. urlparameters are 'title' and 'author' of the book to search for.
 * The resulting skin is rendered to a hidden iframe in the skin "modReadingSetup". In the onLoad function of the iframe, the values are written
 * out as list of links (search result links).
 */
function modReadingSearch_action() {
   var amazon = new knallgrau.Amazon();
   
   var result = amazon.searchBooks((req.data.author || "") + " " + (req.data.title || ""));

   // search results from german booksearch
   default xml namespace = "http://webservices.amazon.com/AWSECommerceService/2005-10-05";
   var items = result..Item;
   if (items.length() == 0) {
      res.write("<div class=\"message\">" + getMessage("modReading.modReadingSetup.NoResults") + "</div>");
   } else {
      res.write("<div class=\"message\">" + getMessage("modReading.modReadingSetup.result.intro") + "</div>");
      res.write("<h3>" + getMessage("modReading.modReadingSetup.TopResultsGerman") + "</h3>");
      for (var i = 0; i < items.length(); i++) {     
         var url = items[i].DetailPageURL.text();
         var image = items[i].MediumImage.URL.text();
         var title = items[i].ItemAttributes.Title.text();
         var authors = items[i].ItemAttributes..Author;
         var author = "";
         for (var j = 0; j < authors.length(); j++) {
            if (j != 0) author += ", ";
            author += authors[j].text();
         }
         
         title = title.toString().replace("'", "&acute;");
         author = author.toString().replace("'", "&acute;");
         res.write('<ul class="searchResultList">');
         res.write('<li><a href="'
                    + url
                    + '" target="_blank" onclick="addItem(\''
                    + url
                    + "', '"
                    + title
                    + "', '"
                    + image
                    + "', '"
                    + author
                    + '\'); return false;">'
                    + title
                    + ' ('
                    + author
                    + ')</a></li>\n');
         res.write("</ul>\n");
      }
      res.write("<br />\n");
   }
   default xml namespace = "http://www.w3.org/1999/xhtml";
   return;
}
