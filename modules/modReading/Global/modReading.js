
app.modules.modReading = {
   title: "Reading",
   description: "Displays a list of the books you are currently reading.",
   version: "",
   state: "beta",
   url: "",
   author: "Wolfgang Kamir",
   authorEmail: "wolfgang@knallgrau.at",
   authorUrl: "http://www.knallgrau.at",
   authorOrganisation: "knallgrau.at",
   copyright: "(c) 2004 knallgrau.at",
   license: "antville",
   licenseUrl: "http://www.antville.org",
   isSidebar: true,
   type: MODULE_TYPE_LIST,
   skins: ["Site.modReading", "Site.modReadingItem", "Site.modReadingDivider"]
};


/**
 * Renders module in the sidebar. The renderSidebarItem function of modReading at first reads exisiting
 * booklist entries from the site's preferences.
 * After reading the items and splitting them up, for every item the "modReadingItem" skin is rendered and inbetween the skin "modReadingDivider".
 * Linked alltogether the resulting string is handed over to the skin "modReading" as body and rendered to the sidebar.
 */
app.modules.modReading.renderSidebarItem = function(param) {
   if (!param) param = {};
   var ReadingTitles = app.modules.modReading.getReadingProperty(this, "titles");
   var ReadingUrls = app.modules.modReading.getReadingProperty(this, "urls");
   var ReadingAuthors = app.modules.modReading.getReadingProperty(this, "authors");
   var ReadingImages = app.modules.modReading.getReadingProperty(this, "images");

   res.push();
   for (var i=0; i<ReadingTitles.length; i++) {
      var p = {};
      p.ReadingTitle = (ReadingUrls[i].length > 12) ? '<a href="' + ReadingUrls[i] + '" target="_blank">' + ReadingTitles[i] + '</a>' : ReadingTitles[i];
      p.ReadingAuthor = ReadingAuthors[i];
      var Img = this.images.get(ReadingImages[i]);
      if (Img) {
         p.ReadingImage = (ReadingUrls[i].length > 12) ? '<a href="' + ReadingUrls[i] + '" target="_blank">' + Img.getUrl() + '</a>' : '<img src="' + Img.getUrl() + '" />';
      }
      else if (ReadingImages[i].length > 12) {
         p.ReadingImage = (ReadingUrls[i].length > 12) ? '<a href="' + ReadingUrls[i] + '" target="_blank"><img src="' + ReadingImages[i] + '" /></a>' : '<img src="' + ReadingImages[i] + '" />';
      }
      else {
         p.ReadingImage = "";
      }
      if (i != 0) this.renderSkin("modReadingDivider");
      this.renderSkin("modReadingItem", p);
   }
   var p = {};
   p.Reading = res.pop();
   res.push();
   this.renderSkin("modReading", p);
   var p = {};
   p.body = res.pop();
   p.editLink = true;
   this.renderSidebarModuleItem("modReading", p);
}


/**
 * Generates the arranger-field of the module's preferences and hands it over to the "modReadingSetup" skin.
 */
app.modules.modReading.renderPreferences = function() {
   Module.prototype.renderSetupSpacerLine();
   var p = {};
   p.ReadingArranger = app.modules.modReading.renderReadingArranger(this);
   Module.prototype.renderSkin("modReadingSetup", p);
};


/**
 * Is called when user clicks the "save" button in module preferences. The values in the item-arranger are read and split up in title, author, url, image.
 * In the arranger itself the items are saved as strings separated by the sequence ":#:". After pushing the values into an array, they are stored in the
 * site's preferences by calling toSource array function.
 */
app.modules.modReading.evalModulePreferences = function() {
   var ReadingTitles = new Array();
   var ReadingUrls = new Array();
   var ReadingAuthors = new Array();
   var ReadingImages = new Array();

   try {
      var ReadingArrangerValues = JSON.parse(req.data.ReadingArrangerValues);
   } catch (err) {
      return;
   }
   if (!ReadingArrangerValues)
      var ReadingArrangerValues = new Array();
      
   ReadingArrangerValues = ReadingArrangerValues.slice(0,20);

   for (var i=0; i<ReadingArrangerValues.length; i++) {
      var temp = ReadingArrangerValues[i].split(":#:");
      ReadingTitles.push(stripTags(temp[0]));
      ReadingUrls.push(stripTags(temp[1]));
      ReadingAuthors.push(stripTags(temp[2]));
      ReadingImages.push(stripTags(temp[3]));
   }

   this.preferences.setProperty("modreading_titles", ReadingTitles.toSource());
   this.preferences.setProperty("modreading_urls", ReadingUrls.toSource());
   this.preferences.setProperty("modreading_authors", ReadingAuthors.toSource());
   this.preferences.setProperty("modreading_images", ReadingImages.toSource());
};


/**
 * Gets an array containing the titles, urls, authors, images of all list items. if nothing is found, an empty array is returned.
 *
 * @return Array containing either titles, urls, authors or images
 */
app.modules.modReading.getReadingProperty = function(obj, name) {
   try {
      var arr = JSON.parse(obj.preferences.getProperty("modreading_" + name));
   } catch (err) {
      ;
   }
   if (!arr)
      var arr = new Array();

   return arr;
};


/**
 * Creates the item arranger field for module's preferences. First all existing items are read, then pushed into an array that is used to create
 * a value-filled dropDown-List. This List is handed over to the skin "modReadingArranger", which also consists of three buttons to remove and reposition
 * items in the list.
 */
app.modules.modReading.renderReadingArranger = function(obj) {
   var ReadingTitles = app.modules.modReading.getReadingProperty(obj, "titles");
   var ReadingUrls = app.modules.modReading.getReadingProperty(obj, "urls");
   var ReadingAuthors = app.modules.modReading.getReadingProperty(obj, "authors");
   var ReadingImages = app.modules.modReading.getReadingProperty(obj, "images");

   var ReadingValues = [];
   for (var i=0; i<ReadingTitles.length; i++) {
      ReadingValues.push({value: ReadingTitles[i] + ":#:" + ReadingUrls[i] + ":#:" + ReadingAuthors[i] + ":#:" + ReadingImages[i], display: ReadingTitles[i] + " (" + ReadingAuthors[i] + ")"});
   }

   var p = {};
   p.ReadingArranger = Html.dropDownAsString({name: "ReadingArranger", id: "ReadingArranger", size: 12, multiple: "multiple", style: "width:440px;", onchange: "getItem(); return false;"}, ReadingValues);

   return Module.prototype.renderSkinAsString("modReadingArranger", p);
};