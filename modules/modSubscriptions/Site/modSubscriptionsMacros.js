
/**
 * show item in abo-list depending on setting "modSubscriptionsSortBy"
 */
function showSubscriptionItem_macro(param) {
   if (!res.handlers.site) return;
   var sortBy = res.handlers.site.preferences.getProperty("modSubscriptionsSortBy") || "title";
   if (sortBy == "title") {
      this.title_macro(param);
   } else if (sortBy == "alias") {
      this.alias_macro(param);
   } else if (sortBy == "creator.name") {
      this.creator_macro(param);
   }
}
