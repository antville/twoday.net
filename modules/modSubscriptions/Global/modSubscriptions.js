
app.modules.modSubscriptions = {
   version: "",
   state: "beta",
   url: "",
   author: "Wolfgang Kamir",
   authorEmail: "wolfgang@knallgrau.at",
   authorUrl: "http://www.knallgrau.at",
   authorOrganisation: "knallgrau.at",
   copyright: "(c) 2004 knallgrau.at",
   license: "twoday",
   licenseUrl: "http://www.twoday.org",
   isSidebar: true,
   type: MODULE_TYPE_LIST,
   skins: ["Site.modSubscriptionsList", "Site.modSubscriptionsListItem"]
};


app.modules.modSubscriptions.renderSidebarItem = function(param) {
   var max = this.preferences.getProperty("modsubscriptionsmax");
   if (max && max == 0) return;
   var exclude = this.preferences.getProperty("modsubscriptionsexclude") ? this.preferences.getProperty("modsubscriptionsexclude").split(",") : [];
   var displaytype = this.preferences.getProperty("modsubscriptionsdisplaytype") ? this.preferences.getProperty("modsubscriptionsdisplaytype") : "subscriptions";

   if (this.creator == null) {
      app.log("[modSubscriptions.renderSidebarItem] site.creator is null: " + this.alias);
      return;
   }
   var memberships = this.creator.subscriptions.list();

   if (displaytype == "all") {
      for (var i=0; i<this.creator.memberships.size(); i++) {
         if (this.creator.memberships.get(i).level >= CONTRIBUTOR && Array.indexOf(memberships, this.creator.memberships.get(i)) == -1) {
            memberships[memberships.length] = this.creator.memberships[i];
         }
      }
   }
   var p02 = {subscriptionList : ""};

   var sortBy = this.preferences.getProperty("modSubscriptionsSortBy") || "title";
   var sortOrder = this.preferences.getProperty("modSubscriptionsSortOrder") || "<";
   var sortCondition = "return (a != null && b != null && b.site != null && a.site != null) ? (b.site." + sortBy + " " + sortOrder + " a.site." + sortBy + ") : null";
   memberships.sort(new Function("a", "b", sortCondition));

   for (var i in memberships) {
      var site = memberships[i].site;
      if (!site) continue;
      if (max && i == max) break;
      if (Array.indexOf(exclude, site.alias) != -1) continue;
      if (!site.online) continue;
      var p03 = {};
      res.push();
      site.image_macro({name: "favicon", fallback: "/favicon", alt: "o", border: "0"});
      p03.favicon = res.pop();
      p03.sortBy = this.preferences.getProperty("modSubscriptionsSortBy") || "title";
      p02.subscriptionList += site.renderSkinAsString("modSubscriptionsListItem", p03);
   }

   var p01 = {};
   p01.body = this.renderSkinAsString("modSubscriptionsList", p02);
   p01.editLink = true;
   this.renderSidebarModuleItem("modSubscriptions", p01);
   return;
}

app.modules.modSubscriptions.renderPreferences = function() {
   var param = {
      label: getMessage("modSubscriptions.wizard.admin.maximum"),
      field: Html.inputAsString({name: "modSubscriptionsMax", value: this.preferences.getProperty("modsubscriptionsmax") ? this.preferences.getProperty("modsubscriptionsmax") : "", size: "3"})
   };
   Module.prototype.renderSetupLine(param);
   Module.prototype.renderSetupSpacerLine();

   var param = {
      label: getMessage("modSubscriptions.wizard.admin.exclude"),
      field: Html.inputAsString({name: "modSubscriptionsExclude", value: this.preferences.getProperty("modsubscriptionsexclude") ? this.preferences.getProperty("modsubscriptionsexclude") : "", size: "40"})
   };
   Module.prototype.renderSetupLine(param);
   Module.prototype.renderSetupSpacerLine();

   var options = [
      {value: "all", display: getMessage("modSubscriptions.wizard.admin.all")},
      {value: "subscriptions", display: getMessage("modSubscriptions.wizard.admin.subsribers")},
   ];
   var param = {
      label: getMessage("modSubscriptions.wizard.admin.displaytype"),
      field: Html.dropDownAsString({name: "modSubscriptionsdisplaytype"}, options, this.preferences.getProperty("modsubscriptionsdisplaytype"))
   };
   Module.prototype.renderSetupLine(param);

   // modSubscriptionsSortBy
   options = [
      {value: "title", display: getMessage("modSubscriptions.wizard.sort.title")},
      {value: "alias", display: getMessage("modSubscriptions.wizard.sort.alias")},
      {value: "creator.name", display: getMessage("modSubscriptions.wizard.sort.creator.name")}
   ];
   param = {
      label: getMessage("modSubscriptions.wizard.sort.header"),
      field: Html.dropDownAsString({name: "modSubscriptionsSortBy"}, options, this.preferences.getProperty("modSubscriptionsSortBy"))
   };
   Module.prototype.renderSetupLine(param);

   // modSubscriptionsSortOrder
   options = [
      {value: "<", display: getMessage("modSubscriptions.wizard.sort.order.asc")},
      {value: ">", display: getMessage("modSubscriptions.wizard.sort.order.desc")},
   ];
   param = {
      label: getMessage("modSubscriptions.wizard.sort.order.header"),
      field: Html.dropDownAsString({name: "modSubscriptionsSortOrder"}, options, this.preferences.getProperty("modSubscriptionsSortOrder"))
   };
   Module.prototype.renderSetupLine(param);
}

app.modules.modSubscriptions.evalModulePreferences = function() {
   this.preferences.setProperty("modsubscriptionsmax", req.data.modSubscriptionsMax);
   this.preferences.setProperty("modsubscriptionsexclude", req.data.modSubscriptionsExclude);
   this.preferences.setProperty("modsubscriptionsdisplaytype", req.data.modSubscriptionsdisplaytype);
   this.preferences.setProperty("modSubscriptionsSortBy", req.data.modSubscriptionsSortBy);
   this.preferences.setProperty("modSubscriptionsSortOrder", req.data.modSubscriptionsSortOrder);
}
