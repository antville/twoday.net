
app.modules.modWeblogStatus = {
   header: "Weblog Status",
   version: "",
   state: "beta",
   url: "",
   author: "Wolfgang Kamir",
   authoremail: "wolfgang@knallgrau.at",
   authorUrl: "http://www.knallgrau.at",
   authororganisation: "knallgrau.at",
   copyright: "(c) 2004 knallgrau.at",
   license: "twoday",
   licenseUrl: "http://www.twoday.org",
   isSidebar: true,
   type: MODULE_TYPE_INFO,
   hasSiteSettings: true,
   skins: ["Site.weblogStatus"]
};


app.modules.modWeblogStatus.renderSidebarItem = function() {
   var p = {};
   p.body = this.renderSkinAsString("weblogStatus");
   this.renderSidebarModuleItem("modWeblogStatus", p);
   return;
}
