
app.modules.modTopList = {
   version: "1.0",
   state: "",
   url: "",
   author: "Johannes Lerch",
   authorEmail: "johannes.lerch@knallgrau.at",
   authorUrl: "http://www.knallgrau.at",
   authorOrganisation: "knallgrau.at",
   copyright: "(c) 2007 knallgrau.at",
   license: "twoday",
   licenseUrl: "http://www.twoday.org",
   isSidebar: false,
   hasSiteSettings: false
};

