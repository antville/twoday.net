
/**
 * renders a list of top items (sites or stories)
 * @param name name of the list (sites, stories, views, comments)
 * @param skin name of the skin to render on the objects in the collection
 * @param limit
 */
function modTopListList_macro(param) {
   if ((res.handlers.site && !res.handlers.site.trusted) || !param.itemSkin) return;
   var storedResHandlersSite = res.handlers.site;
   var name = req.data.modTopListName || "storiesByRank";
   var toplist = modTopListGetList(name);

   var skinParam = {cacheTime: formatTimestamp(toplist.timestamp, "short"), heading: getMessage("modToplist.listheader." + (name))};
   res.push();
   var limit = Math.min(50, param.limit || 10);
   for (var i=0; i<Math.min(limit, toplist.ids.length); i++) {
      var obj = (name.indexOf("sites") == 0) ? Site.getById(toplist.ids[i]) : Story.getById(toplist.ids[i]);
      // check if site is public
      if (obj && obj.getSite() && obj.getSite().show) {
         res.handlers.site = obj.getSite();
         obj.renderSkin(param.itemSkin, {score: toplist.scores[i], position: (i+1)});
      }
   }
   skinParam.list = res.pop();
   res.handlers.site = storedResHandlersSite;
   if (param.wrapperSkin) {
      renderSkin(param.wrapperSkin, skinParam);
   } else {
      res.write(skinParam.list);
   }
}

/**
 * gets the objects for the toplist
 * @param name name of the list to get
 */
function modTopListGetList(name, site) {
   // cache week collections for 5 minutes, month collections for one hour
   var cacheName = (site ? site.alias : "") + "modTopList" + name;
   if (app.data[cacheName] && ((new Date() - app.data[cacheName].timestamp) < (name.contains("Month") ? 1000 * 60 * 60 : 1000 * 60 * 5))) {
      return app.data[cacheName];
   }

   var excludedSitesSql = "";
   var excludedSites = getProperty("modTopListExcludedSites");
   if (excludedSites != null && excludedSites.match(/[ \d,]+/)) {
      excludedSitesSql = " s.TEXT_F_SITE NOT IN(" + excludedSites + ") AND ";
   }
   var sql = "";
   var idsPerDay = 5000; // that is a guess, for how many text-objects are posted by day; if we filter by ID, the sql-statement is a lot faster
   //var rankSql = "count(distinct c.TEXT_F_USER_CREATOR) * 5 + count(distinct c.TEXT_ID) + (s.TEXT_READS / 4)";
   var rankSql = "count(distinct c.TEXT_F_USER_CREATOR) * 4 + count(distinct c.TEXT_CREATOR_NAME) * 3 + sqrt(s.TEXT_READS) * 6 + pow(log(10, length(s.TEXT_TEXT)), 2) * 3";
   switch (name) {
      // what we need: hot stories (storiesByRank), top sites of week (sitesBySumRank, but with slightly lower index),
      // top sites of month (sitesBySumRank, 150000 limit), top stories of week, top stories of month
      case "storiesByRank": sql = "select s.TEXT_ID as id, round((" + rankSql + ") / sqrt(sqrt((1 + TIMEDIFF(now(), s.TEXT_CREATETIME))))) as score from AV_SITE join AV_TEXT s on SITE_ID=s.TEXT_F_SITE left join AV_TEXT c on s.TEXT_ID = c.TEXT_F_TEXT_STORY where " + excludedSitesSql + " s.TEXT_PROTOTYPE='Story' and s.TEXT_ID > ((select max(TEXT_ID) from AV_TEXT) - " + (3 * idsPerDay) + ") and c.TEXT_ID > ((select max(TEXT_ID) from AV_TEXT) - " + (3 * idsPerDay) + ") group by s.TEXT_ID order by score desc limit  50";
         break;

      case "storiesByRankPerWeek": sql = "select s.TEXT_ID as id, round(" + rankSql + ") as score from AV_SITE join AV_TEXT s on SITE_ID=s.TEXT_F_SITE left join AV_TEXT c on s.TEXT_ID = c.TEXT_F_TEXT_STORY where " + excludedSitesSql + " s.TEXT_PROTOTYPE='Story' and s.TEXT_ID > ((select max(TEXT_ID) from AV_TEXT) - " + (7 * idsPerDay)+ ") and c.TEXT_ID > ((select max(TEXT_ID) from AV_TEXT) - " + (7 * idsPerDay)+ ") group by s.TEXT_ID order by score desc limit  50";
         break;
      case "storiesByRankPerMonth": sql = "select s.TEXT_ID as id, round(" + rankSql + ") as score from AV_SITE join AV_TEXT s on SITE_ID=s.TEXT_F_SITE left join AV_TEXT c on s.TEXT_ID = c.TEXT_F_TEXT_STORY where " + excludedSitesSql + " s.TEXT_PROTOTYPE='Story' and" + (site ? " s.TEXT_CREATETIME > DATE_SUB(NOW(), INTERVAL 30 DAY) and s.TEXT_F_SITE = " + site._id : " s.TEXT_ID > ((select max(TEXT_ID) from AV_TEXT) - " + (30 * idsPerDay)+ ") and c.TEXT_ID > ((select max(TEXT_ID) from AV_TEXT) - " + (30 * idsPerDay)+ ")") + " group by s.TEXT_ID order by score desc limit  50";
         break;
      case "sitesBySumRankPerWeek": sql = "select id, sum(rank)/(sum(1) + 2) as score from (select SITE_ID as id, (" + rankSql + ") as rank from AV_SITE join AV_TEXT s on SITE_ID=s.TEXT_F_SITE left join AV_TEXT c on s.TEXT_ID = c.TEXT_F_TEXT_STORY where " + excludedSitesSql + " s.TEXT_PROTOTYPE='Story' and s.TEXT_ID > ((select max(TEXT_ID) from AV_TEXT) - " + (7 * idsPerDay)+ ") and c.TEXT_ID > ((select max(TEXT_ID) from AV_TEXT) - " + (7 * idsPerDay)+ ") group by s.TEXT_ID) as tmp group by id order by score desc limit  50";
         break;
      case "sitesBySumRankPerMonth": sql = "select id, sum(rank)/(sum(1) + 4) as score from (select SITE_ID as id, (" + rankSql + ") as rank from AV_SITE join AV_TEXT s on SITE_ID=s.TEXT_F_SITE left join AV_TEXT c on s.TEXT_ID = c.TEXT_F_TEXT_STORY where " + excludedSitesSql + " s.TEXT_PROTOTYPE='Story' and s.TEXT_ID > ((select max(TEXT_ID) from AV_TEXT) - " + (30 * idsPerDay)+ ") and c.TEXT_ID > ((select max(TEXT_ID) from AV_TEXT) - " + (30 * idsPerDay)+ ") group by s.TEXT_ID) as tmp group by id order by score desc limit  50";
         break;
   }

   var dbc = getDBConnection("twoday");
   var rows = dbc.executeRetrieval(sql);
   var ids = new Array();
   var scores = new Array();
   while (rows && rows.next()) {
      ids.push(rows.getColumnItem("id"));
      scores.push(Math.round(rows.getColumnItem("score")));
   }
   if (rows) rows.release();

   var result = {
      ids: ids,
      scores: scores,
      timestamp: new Date()
   };
   
   //we now invalidate the rss feed of the site where this module is placed, which is in fact a bit dirt
   path.Site.updateStaticRSSFeed("index");
   app.data[cacheName] = result;
   return result;
}
