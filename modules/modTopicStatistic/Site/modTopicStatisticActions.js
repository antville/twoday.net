
/**
 * main action of this module
 */
function modTopicStatisticDraw_action() {
   var param = {modTopicStatistic: ""};
   if (!this.stories.size() || !this.preferences.getProperty("archive"))
      return;

   var arrColors = new Array();
   
   arrColors.push("5C0B0B");
   arrColors.push("9C1313");
   arrColors.push("D31A1A");
   arrColors.push("FB271F");
   arrColors.push("FFA91F");
   arrColors.push("FDEF20");
   arrColors.push("D8FF25");
   arrColors.push("A8F42A");
   arrColors.push("79E12F");
   arrColors.push("4ECD36");
   arrColors.push("29BE3F");
   arrColors.push("12B752");
   arrColors.push("13C378");
   arrColors.push("15D59F");
   arrColors.push("1AE5F8");
   arrColors.push("18AEE7");
   arrColors.push("147BCA");
   arrColors.push("1158B4");
   arrColors.push("0E37A0");
   arrColors.push("111791");
   arrColors.push("381192");
   arrColors.push("6C139E");
   arrColors.push("A715A0");
   arrColors.push("D019B1");
   arrColors.push("F01DC3");
   arrColors.push("D019B1");
   arrColors.push("A715A0");
   arrColors.push("8B1171");
   arrColors.push("580A49");
   
   var connex = getDBConnection("twoday");

   var query = "select min(a) from (select SUBSTRING(TEXT_DAY, 1, 6) , count(*) as a from AV_TEXT where TEXT_PROTOTYPE='Story' and TEXT_F_SITE = " + this._id + " group by SUBSTRING(TEXT_DAY, 1, 6)) as subQ";
   var result = connex.executeRetrieval(query);
   if (!result)
      return;
   result.next();
   var minTopicCount = result.getColumnItem("min(a)");

   var query = "select max(a) from (select SUBSTRING(TEXT_DAY, 1, 6) , count(*) as a from AV_TEXT where TEXT_PROTOTYPE='Story' and TEXT_F_SITE = " + this._id + " group by SUBSTRING(TEXT_DAY, 1, 6)) as subQ";
   var result = connex.executeRetrieval(query);
   if (!result)
      return;
   result.next();
   var maxTopicCount = result.getColumnItem("max(a)");

   var query = "select count(distinct TEXT_TOPIC) from AV_TEXT where TEXT_F_SITE = " + this._id;
   var result = connex.executeRetrieval(query);
   if (!result)
      return;
   result.next();
   var topicCount = result.getColumnItem("count(distinct TEXT_TOPIC)");

   var query = "select distinct TEXT_TOPIC from AV_TEXT where TEXT_F_SITE = " + this._id;
   var topics = connex.executeRetrieval(query);
   var legendParam = {LegendColumns: ""};
   var legendColumnParam = {LegendItems: ""};

   var i = 0;
   var columnItemCounter = 0;
   if (topics) {
      var arrTopicColors = new Array();
      var arrTopicHighlightColors = new Array();
      while(topics.next()) {
         arrTopicColors[topics.getColumnItem("TEXT_TOPIC")] = arrColors[i];
         arrTopicHighlightColors[topics.getColumnItem("TEXT_TOPIC")] = this.modTopicStatisticBuildHighlightColor(arrColors[i]);
         legendColumnParam.LegendItems += this.renderSkinAsString("modTopicStatisticLegendItem", {topic: (topics.getColumnItem("TEXT_TOPIC") ? topics.getColumnItem("TEXT_TOPIC") : getMessage("modTopicStatistic.modTopicStatisticDraw.noTopic")), topicCaption: (topics.getColumnItem("TEXT_TOPIC") ? ( topics.getColumnItem("TEXT_TOPIC").toString().length >= 10 ? topics.getColumnItem("TEXT_TOPIC").toString().substring(0,10) + "..." : topics.getColumnItem("TEXT_TOPIC")) : getMessage("modTopicStatistic.modTopicStatisticDraw.noTopic")), color: arrColors[i], highlightColor: arrTopicHighlightColors[topics.getColumnItem("TEXT_TOPIC")]});
         i = (i == arrColors.length-1 ? 0 : i + 1);
         columnItemCounter++;
         if (columnItemCounter > topicCount/3) {
            legendParam.LegendColumns += this.renderSkinAsString("modTopicStatisticLegendColumn", legendColumnParam);
            legendColumnParam.LegendItems = "";
            columnItemCounter = 0;
         }
      }
   }
   // legend Columns left
   if (columnItemCounter != 0) {
      legendParam.LegendColumns += this.renderSkinAsString("modTopicStatisticLegendColumn", legendColumnParam);
   }
   
   var outerQuery = "select SUBSTRING(TEXT_DAY, 1, 6) as d, TEXT_TOPIC, count(*) from AV_TEXT where TEXT_F_SITE = " + this._id + " and TEXT_PROTOTYPE='Story' group by TEXT_TOPIC, d order by d desc, TEXT_TOPIC";
   var rows = connex.executeRetrieval(outerQuery);

   if (rows) {
      while (rows.next()) {
         if (rows.getColumnItem("d") != lastDate) {
            var innerQuery = "select count(*) from AV_TEXT where SUBSTRING(TEXT_DAY, 1, 6) = '" + rows.getColumnItem("d") + "' and TEXT_PROTOTYPE='Story' and TEXT_F_SITE = " + this._id;
            var countRow = connex.executeRetrieval(innerQuery);
            countRow.next();
            if (lastDate) {
               var ts = lastDate.toString().toDate("yyyyMM", this.getTimeZone());
               p1.month = formatTimestamp(ts, "MMMM");
               p1.count = count;
               p1.height = p2.height;
               param.modTopicStatistic += this.renderSkinAsString("modTopicStatisticMonth", p1);
            }
            var count = countRow.getColumnItem("count(*)");
            var p1 = {bars: ""};
         }
         if ((!lastDate) || (lastDate && rows.getColumnItem("d").substring(0,4) != lastDate.substring(0,4))) {
            var query = "select count(*) from AV_TEXT where TEXT_F_SITE = " + this._id + " and TEXT_PROTOTYPE = 'Story' and substring(TEXT_DAY, 1, 4) = '" + rows.getColumnItem("d").substring(0,4) + "'";
            var result = connex.executeRetrieval(query);
            if (!result)
               return;
            result.next();
            var yearStoryCount = result.getColumnItem("count(*)");

            var query = "select TEXT_TOPIC, count(*) from AV_TEXT where TEXT_F_SITE = " + this._id + " and TEXT_PROTOTYPE = 'Story' and substring(TEXT_DAY, 1, 4) = '" + rows.getColumnItem("d").substring(0,4) + "' group by TEXT_TOPIC";
            var yearStatistic = connex.executeRetrieval(query);
            var yearStatisticParam = {YearBars: "", Year: rows.getColumnItem("d").substring(0,4), YearStoryCount: yearStoryCount};
            if (yearStatistic) {
               while(yearStatistic.next()) {
                  var n = parseInt(yearStatistic.getColumnItem("count(*)"), 10);
                  var p = (n / yearStoryCount * 100).toString().split(".")[0];
                  var pDisplayed = (n / yearStoryCount * 100).toString().substring(0,4);
                  var p3 = {
                     percentage: (p>1 ? p: 0),
                     color: arrTopicColors[yearStatistic.getColumnItem("TEXT_TOPIC")],
                     highlightColor: arrTopicHighlightColors[yearStatistic.getColumnItem("TEXT_TOPIC")],
                     infoPopupMessage: (yearStatistic.getColumnItem("TEXT_TOPIC") ? yearStatistic.getColumnItem("TEXT_TOPIC") : getMessage("modTopicStatistic.modTopicStatisticDraw.noTopic")) + ": " + n + " " + getMessage("generic.of") + " " + yearStoryCount + " " + getMessage("modTopicStatistic.modTopicStatisticDraw.StoriesOnThisTopic") + ", " + pDisplayed + "%",
                     topic: (yearStatistic.getColumnItem("TEXT_TOPIC") ? yearStatistic.getColumnItem("TEXT_TOPIC") : getMessage("modTopicStatistic.modTopicStatisticDraw.noTopic")),
                  }
                  yearStatisticParam.YearBars += this.renderSkinAsString("modTopicStatisticNewYearBar", p3);
               }
            }
            
            param.modTopicStatistic += this.renderSkinAsString("modTopicStatisticNewYear", yearStatisticParam);
         }
         
         var n = parseInt(rows.getColumnItem("count(*)"), 10);
         var p = (n / count * 100).toString().split(".")[0];
         var h = (count / yearStoryCount) * 240;
         if (h<15) h=15;
         
         //15 + ((count - minTopicCount) / (maxTopicCount) * 30);
         var p2 = { 
            percentage: p,
            color: arrTopicColors[rows.getColumnItem("TEXT_TOPIC")],
            highlightColor: arrTopicHighlightColors[rows.getColumnItem("TEXT_TOPIC")],
            infoPopupMessage: (rows.getColumnItem("TEXT_TOPIC") ? rows.getColumnItem("TEXT_TOPIC") : getMessage("modTopicStatistic.modTopicStatisticDraw.noTopic")) + ": " + n + " " + getMessage("generic.of") + " " + count + " " + getMessage("modTopicStatistic.modTopicStatisticDraw.StoriesOnThisTopic") + ", " + p + "%",
            topic: (rows.getColumnItem("TEXT_TOPIC") ? rows.getColumnItem("TEXT_TOPIC") : getMessage("modTopicStatistic.modTopicStatisticDraw.noTopic")),
            height: h.toString().split(".")[0]
         }
         p1.bars += this.renderSkinAsString("modTopicStatisticBar", p2);
         var lastDate = rows.getColumnItem("d");
      }
      var ts = lastDate.toString().toDate("yyyyMM", this.getTimeZone());
      p1.month = formatTimestamp(ts, "MMMM");
      p1.count = count;
      p1.height = p2.height;
      param.modTopicStatistic += this.renderSkinAsString("modTopicStatisticMonth", p1);
   }
   
   param.modTopicStatisticLegend = this.renderSkinAsString("modTopicStatisticLegend", legendParam);
   res.data.title = getMessage("modTopicStatistic.title");
   res.data.body = this.renderSkinAsString("modTopicStatistic", param);
   res.data.title = "modTopicStatistic";
   this.renderPage();

   return;
}


/**
 * helper function
 */
function modTopicStatisticBuildHighlightColor(color) {
   var red = color.substring(0,2);
   var green = color.substring(2,4);
   var blue = color.substring(4,6);

   red = java.lang.Integer.parseInt(red, 16);
   green = java.lang.Integer.parseInt(green, 16);
   blue = java.lang.Integer.parseInt(blue, 16);

   red = (red <= 200 ? red + 55 : 255);
   green = (green <= 200 ? green + 55 : 255);
   blue = (blue <= 200 ? blue + 55 : 255);

   return java.lang.Integer.toHexString(red) + java.lang.Integer.toHexString(green) + java.lang.Integer.toHexString(blue);
}
