
modTopicStatistic 
was written by wolfgang kamir @ knallgrau.at

It adds a topic statistic page to every weblog.
siteurl/modTopicStatisticDraw

The page shows a colored table graph, with a vertical time-scale and a hotizontal story-scale. The more stories you have in a month/topic, the bigger the square is drawn.

This module doesn't add a lot of functionality to a weblog, but it is fun to see what topics where important when in your weblog.

By default everybody who may see the site may also see this page.

ToDo's:
- add a module setting to hide this page from public
