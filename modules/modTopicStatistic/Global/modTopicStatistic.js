
app.modules.modTopicStatistic = {
   version: "1.0",
   state: "stable",
   url: "",
   author: "Wolfgang Kamir",
   authorEmail: "wolfgang@knallgrau.at",
   authorUrl: "http://www.knallgrau.at",
   authorOrganisation: "knallgrau.at",
   copyright: "(c) 2004 knallgrau.at",
   license: "twoday",
   licenseUrl: "http://www.twoday.org",
   isSidebar: false,
   hasSiteSettings: false
};


app.modules.modTopicStatistic.renderPreferences = function() {
};


app.modules.modTopicStatistic.evalModulePreferences = function() {
};


app.modules.modTopicStatistic.checkSiteAccess = function(args) {
   var action = args[0];
   var usr    = args[1];
   var level  = args[2];
   var url    = args[3];
   if (action == "modTopicStatisticDraw") {
      checkIfLoggedIn();
      this.checkModMtImExportExport(usr, level);
   }
   return args;
};
