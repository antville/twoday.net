app.modules.modNiceUrls = {
   title: "Nice Permaurls",
   version: "1.0",
   state: "stable",
   url: "http://www.twoday.org",
   author: "Franz Philipp Moser",
   authorEmail: "",
   authorUrl: "http://www.knallgrau.at",
   authorOrganisation: "knallgrau.at",
   copyright: "(c) knallgrau.at",
   license: "twoday",
   licenseUrl: "http://www.twoday.org",
   hasSiteSettings: true
};

app.modules.modNiceUrls.renderPreferences = function() {
   Module.prototype.renderSetupSpacerLine();
   Module.prototype.renderSetupHeader({header: getMessage("modNiceUrls.pref.title")});
   res.push();
   Html.checkBox({name: "modNiceUrlsActive", value: "1", checked: res.handlers.site.preferences.getProperty("modNiceUrlsActive") ? "checked" : null});
   var inputField = res.pop();
   var param = {
      label: getMessage("modNiceUrls.pref.checkbox"),
      field: inputField
   };
   Module.prototype.renderSetupLine(param);
};


app.modules.modNiceUrls.evalModulePreferences = function() {
   res.handlers.site.preferences.setProperty("modNiceUrlsActive", req.data.modNiceUrlsActive);
};


app.modules.modNiceUrls.evalNewSite = function(args) {
   var creator = args[0];
   var newSite = args[1];
   newSite.preferences.setProperty("modNiceUrlsActive", true);
   return;
};

app.modules.modNiceUrls.onBeforeStoryEditAction = function(story) {
   if (res.handlers.site && res.handlers.site.preferences.getProperty("modNiceUrlsActive")) {
      req.data.modNiceUrls_urlid = (req.data.save || req.data.publish) ? app.modules.modNiceUrls.generateUrlId(req.data.modNiceUrls_urlid) : story.urlId;
   }
};
knallgrau.Event.registerObserver("Story", "beforeEditAction", app.modules.modNiceUrls.onBeforeStoryEditAction, null, "app.modules.modNiceUrls.onBeforeStoryEditAction");


app.modules.modNiceUrls.onAfterContentMacroEditor =  function(args) {
   if (res.handlers.site && res.handlers.site.preferences.getProperty("modNiceUrlsActive")) {
      var obj = args[0];
      var params = args[1];
      if (params && obj._prototype == "Story" && params.name == "content_title")
         obj.renderSkin("modNiceUrlsPermaUrlInput");
   }
};

app.modules.modNiceUrls.onEvalStory = function(args) {
   if (res.handlers.site && res.handlers.site.preferences.getProperty("modNiceUrlsActive")) {
      app.modules.modNiceUrls.setUrlId(args[0], app.modules.modNiceUrls.generateUrlId(req.data.modNiceUrls_urlid));
   }
};

app.modules.modNiceUrls.onEvalNewStory = function(args) {
   if (res.handlers.site && res.handlers.site.preferences.getProperty("modNiceUrlsActive")) {
      app.modules.modNiceUrls.setUrlId(args[0], app.modules.modNiceUrls.generateUrlId(req.data.modNiceUrls_urlid));
   }
};

app.modules.modNiceUrls.generateUrlId = function(workingUrlId) {
   // create urlId
   if (workingUrlId) {
      workingUrlId = workingUrlId.toLowerCase();
      workingUrlId = workingUrlId.replace(new RegExp("[" + String.fromCharCode(252) + "]", "g"), "ue");
      workingUrlId = workingUrlId.replace(new RegExp("[" + String.fromCharCode(228) + "]", "g"), "ae");
      workingUrlId = workingUrlId.replace(new RegExp("[" + String.fromCharCode(246) + "]", "g"), "oe");
      workingUrlId = workingUrlId.replace(new RegExp("[" + String.fromCharCode(220) + "]", "g"), "Ue");
      workingUrlId = workingUrlId.replace(new RegExp("[" + String.fromCharCode(196) + "]", "g"), "Ae");
      workingUrlId = workingUrlId.replace(new RegExp("[" + String.fromCharCode(214) + "]", "g"), "Oe");
      workingUrlId = workingUrlId.replace(new RegExp("[" + String.fromCharCode(223) + "]", "g"), "ss");
      workingUrlId = workingUrlId.replace(/[^a-z0-9 -]/g, "");
      workingUrlId = workingUrlId.replace(/ /g, "-");
      workingUrlId = workingUrlId.replace(/-+/g, "-");
      // 70 Chars are allowed
      workingUrlId = workingUrlId.substring(0, 70)
      // remove - from start and end of String because not very nice
      workingUrlId = workingUrlId.replace(/^-/, "");
      workingUrlId = workingUrlId.replace(/-$/, "");
   }
   return (workingUrlId && workingUrlId != "-") ? workingUrlId : null;
};

app.modules.modNiceUrls.setUrlId = function(story, newUrlId) {
      if (!story) return;
      // The user wants to delete the URL-ID
      if (newUrlId) {
         if (!story._parent.storiesByUrlId.get(newUrlId))
            story.urlId = newUrlId;
      } else {
         story.urlId = null;
      }
};
