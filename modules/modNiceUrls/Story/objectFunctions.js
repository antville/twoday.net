/**
 * Just a Wrapper to generates nice urls
 */
function processHref(href) {
   return this.generateHrefUrl(href, (this.story || this));
}

/*
 * Replace stories/ID with stories/urlTitle
 */
function generateHrefUrl(href, obj) {
   var parentUrl = obj._parent.href();
   // just for now all urls are rewritten
   href = href.replace(/^\/stories\/[^\/]+/i, "");
   // new url
   var returnHref = parentUrl + (obj.urlId || obj._id) + href;
   return returnHref;
}

