function getChildElement(name) {
  if (this.storiesByUrlId.get(name)) {
    return this.storiesByUrlId.get(name);
  } else {
    return this.get(name);
  }
}
