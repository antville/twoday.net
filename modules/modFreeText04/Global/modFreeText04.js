
app.modules.modFreeText04 = {
   title: "Free Text (4)",
   description: "renders freely definable HTML Text",
   version: "1.0",
   state: "stable",
   url: "",
   author: "Wolfgang Kamir",
   authorEmail: "wolfgang@knallgrau.at",
   authorUrl: "http://www.knallgrau.at",
   authorOrganisation: "knallgrau.at",
   copyright: "(c) 2004 knallgrau.at",
   license: "twoday",
   licenseUrl: "http://www.twoday.org",
   isSidebar: true,
   hasSiteSettings: true
};


/**
 * Reads userdefined free HTML text from site's preferences and hands it over to "modFreeText04" skin as parameter.
 * renderSidebarModuleItem function displays the module in sidebar (+ header).
 *
 * @see Site.renderSidbarModuleItem
 */
app.modules.modFreeText04.renderSidebarItem = function(param) {
   var p = {};
   p.body = this.preferences.getProperty("modfreetext04text") ? this.preferences.getProperty("modfreetext04text") : "";
   p.editLink = true;
   this.renderSidebarModuleItem("modFreeText04", p);
   return;
}


/**
 * Renders the module's preferences.
 */
app.modules.modFreeText04.renderPreferences = function() {
   var param = {};
   param.label = getMessage("modFreeText04.wizard.admin.settingsText");
   param.value = this.preferences.getProperty("modfreetext04text") ?
                 this.preferences.getProperty("modfreetext04text") : "";
   Module.prototype.renderSkin("modFreeText04SetupLine", param);
};


/**
 * Stores the userdefined free HTML text in the site's prefernces.
 */
app.modules.modFreeText04.evalModulePreferences = function() {
   this.preferences.setProperty("modfreetext04text", req.data.modFreeText04Text);
};
