
app.modules.modMacroFreeText03 = {
   title: "Macro Free Text (3)",
   description: "renders freely definable Text allowing macros",
   version: "1.0",
   state: "stable",
   url: "",
   author: "Franz Philipp Moser",
   authorEmail: "philipp.moser@knallgrau.at",
   authorUrl: "http://www.knallgrau.at",
   authorOrganisation: "knallgrau.at",
   copyright: "(c) 2009 knallgrau.at",
   license: "twoday",
   licenseUrl: "http://www.twoday.org",
   isSidebar: true,
   hasSiteSettings: true,
   isRestricted: true
};


/**
 * Reads userdefined free HTML text from site's preferences and hands it over to "modFreeText01" skin as parameter.
 * renderSidebarModuleItem function displays the module in sidebar (+ header).
 *
 * @see Site.renderSidbarModuleItem
 */
app.modules.modMacroFreeText03.renderSidebarItem = function(param) {
   var p = {};
   p.body = this.preferences.getProperty("modmacrofreetext03text") ? this.preferences.getProperty("modmacrofreetext03text") : "";
   var bodySkin = createSkin(p.body);
   p.body = this.renderSkinAsString(bodySkin);
   p.editLink = true;
   this.renderSidebarModuleItem("modMacroFreeText03", p);
   return;
}


/**
 * Renders the module's preferences.
 */
app.modules.modMacroFreeText03.renderPreferences = function() {
   var param = {};
   param.label = getMessage("modMacroFreeText03.wizard.admin.settingsText");
   param.value = this.preferences.getProperty("modmacrofreetext03text") ?
                 this.preferences.getProperty("modmacrofreetext03text") : "";
   Module.prototype.renderSkin("modMacroFreeText03SetupLine", param);
};


/**
 * Stores the userdefined free HTML text in the site's prefernces.
 */
app.modules.modMacroFreeText03.evalModulePreferences = function() {
   this.preferences.setProperty("modmacrofreetext03text", req.data.modMacroFreeText03Text);
};
