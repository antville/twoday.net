
app.modules.modWeblogSearchbar = {
   version: "",
   state: "beta",
   url: "",
   author: "Wolfgang Kamir",
   authorEmail: "wolfgang@knallgrau.at",
   authorUrl: "http://www.knallgrau.at",
   authorOrganisation: "knallgrau.at",
   copyright: "(c) 2004 knallgrau.at",
   license: "twoday",
   licenseUrl: "http://www.twoday.org",
   isSidebar: true,
   type: MODULE_TYPE_NAVIGATION,
   hasSiteSettings: true,
   skins: ["Site.searchbox"]
};

app.modules.modWeblogSearchbar.renderSidebarItem = function() {
   var p = {};
   p.body = this.renderSkinAsString("searchbox");
   this.renderSidebarModuleItem("modWeblogSearchbar", p);
   return;
}
