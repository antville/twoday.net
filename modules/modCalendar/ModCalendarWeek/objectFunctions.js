
/**
 * Sets groupname to the formatted timestamp
 */
function constructor(site, date) {
   this.site = site;
   this.groupname = date.format("yyyyw");
   return;
}


/**
 * Returns the Perma-URL for this week
 * @param offset Integer, shifts date by x Weeks; special value: "now"
 */
function getUrl(offset) {
   if (offset == "now") {
      var cweek = (new Date()).format("yyyyw");
   } else if (offset) {
      var date = parseStringToDate(this.groupname, "yyyyw");
      var cweek = (new Date(date.setDate(date.getDate() + offset * 7))).format("yyyyw");
   } else {
      var cweek = this.groupname;
   }
   return this.site.calendar.href("weekview") + "?calweek=" + cweek;
}


/**
 * Returns the Date of the first Day of the Week
 */
function getFirstDayOfWeek() {
   var date = parseStringToDate(this.groupname, "yyyyw"); // sunday
   if (date.format("E") == "Sun") {
      date = new Date(date.setDate(date.getDate() + 1)); // monday
   }
   return date;
}
