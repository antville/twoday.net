
/**
 * Returns the Perma-URL for this week
 * @param offset Integer, shifts date by x Weeks; special value: "now"
 */
function url_macro(param) {
   res.write(this.getUrl(param.offset));
   return;
}


/**
 * Returns the date in a specified format
 * @param format Deafault "w"
 * @param offset Integer, shifts date by x Weeks; special value: "now"
 */
function date_macro(param) {
   if (param.offset == "now") {
      var date = new Date();
   } else {
      var date = parseStringToDate(this.groupname, "yyyyw");
      if (param.offset) date.setDate(date.getDate() + param.offset * 7);
   }
   return formatTimestamp(date, param.format ? param.format : "w");
}


/**
 * Loops through the Days of this Weeks and renders a certain Skin for them
 * @param skin
 */
function listDays_macro(param) {
   var skinName = param.skin ? param.skin : "weekView";
   var date = parseStringToDate(this.groupname, "yyyyw");
   var cdate = this.getFirstDayOfWeek();
   for (var i=0; i<7; i++) {
      var day = new ModCalendarDay(this.site, cdate);
      day.renderSkin(skinName);
      cdate.setTime(cdate.getTime() + Date.ONEDAY);
   }
   return;
}
