
/**
 * Permission checks (called by hopobject.onRequest())
 *
 * @param action  String, name of action
 * @param usr     User, who wants to access an action
 * @param level   Int, Membership level
 * @return Object if permission was denied <br />
 *   .message  message, if something went wrong <br />
 *   .url      recommanded redirect URL
 * @doclevel public
 */
function checkAccess(action, usr, level) {
   if (usr && usr.sysadmin) return;
   try {
      switch (action) {
         case "main" :
            if (!this.site.online)
               checkIfLoggedIn(this.href(action));
            url = root.href();
            this.site.calendar.checkView(usr, level);
            break;
         case "delete" :
         case "edit" :
            checkIfLoggedIn(this.href(req.action));
            this.site.calendar.checkAdd(usr, level);
      }
   } catch (deny) {
      return {message: deny.toString(), url: this._parent.href()};
   }
   return;
}
