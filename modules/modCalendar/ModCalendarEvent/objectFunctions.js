
/**
 * constructor function for ModCalendarEvent objects
 */
function constructor(creator, ipaddress) {
   this.reads = 0;
   this.ipaddress = ipaddress;
   this.creator = creator;
   this.editableby = EDITABLEBY_ADMINS;
   this.createtime = new Date();
   this.modifytime = new Date();

   var date = new Date();
   date.setHours(9, 0, 0);
   this.modCalendarStartTime = date;
   date.setHours(18, 0, 0);
   this.modCalendarEndTime = date;
   this.modCalendarUntil = this.modCalendarStartTime;

   this.modCalendarIsWholeDay = 0;
}


/**
 * Returns the Perma-URL for this Event
 *
 * @param offset    Integer, shifts event by x
 */
function getUrl(offset) {
   var event = this;
   if (offset) {
      var idx = this.site.calendar.events.contains(this);
      var event = this.site.calendar.events.get(idx - offset);
      if (!event) return;
   }
   return event.href();
}


/**
 * Edits an existing MasterEvent. All settings are propagated
 * to its children, if they have not been modified there.
 * Currently it is not possible to change the Recurrence-Settings,
 * after the MasterEvent has been created.
 *
 * @param param Object
 * @param modifier User
 */
function evalMasterEvent(param, modifier) {

   var props = ["title", 
               "text",
               "rawcontent",
               "content_xml",
               "editableby",
               "topic",
               "modCalendarIsWholeDay", 
               "modCalendarStartTime", 
               "modCalendarEndTime"];
   var s = new ModCalendarEvent();
   s.evalEvent(param, modifier);
   for (var p in props) {
      // just handle the properties, which have changed
      if (this[props[p]] != s[props[p]]) {
         for (var i=0; i<this.count(); i++) {
            var childEvent = this.get(i);
            // if that specific property of the childEvent has not changed yet, it will get the new value
            if (props[p] == "content_xml" && 
                  [childEvent.content.getAll()].toSource() == [this.content.getAll()].toSource()) {
               childEvent.content.setAll(s.content.getAll());
            } else if ((props[p] == "modCalendarStartTime" || props[p] == "modCalendarEndTime") && 
                (childEvent[props[p]].getHours() == this[props[p]].getHours() &&
                childEvent[props[p]].getMinutes() == this[props[p]].getMinutes())) {
                  var time = childEvent[props[p]];
                  time.setHours(s[props[p]].getHours());
                  time.setMinutes(s[props[p]].getMinutes());
                  childEvent[props[p]] = time;
            } else if (childEvent[props[p]] == this[props[p]]) {
               childEvent[props[p]] = s[props[p]];
               childEvent.cache["lastRendered_" + props[p]] = null;
            }

         }
         this[props[p]] = s[props[p]];
      }
   }

   var result = new Message("eventUpdate", null, this);
   return result;
}


/**
 * Edits an existing Event.
 *
 * @param param Object 
 *        .http_remotehost String IP-Address of UserClient
 *        .content_xyz String ContentParts
 *        .createtime String, optional, used for explicitely setting the CreateTime
 *        .editableby
 *        .discussions
 *        .topic
 *        .addToTopic
 *        .modCalendarIsRecurrence
 *        .modCalendarIsWholeDay
 *        .modCalendarStartTime
 *        .modCalendarStartTimeDropDown
 *        .modCalendarEndTimeDropDown
 *        .modCalendarUntil
 * @param modifier User
 * @return Object
 */
function evalEvent(param, modifier) {

   // Event settings
   try {
      if (param.modCalendarIsWholeDay) {
         this.modCalendarIsWholeDay = 1;
         this.modCalendarStartTime = parseStringToDate(param.modCalendarStartTime + " 00:00", "yyyy-MM-dd HH:mm");
         this.modCalendarEndTime = parseStringToDate(param.modCalendarStartTime + " 24:00", "yyyy-MM-dd HH:mm");
      } else {
         var startTime = parseStringToDate(param.modCalendarStartTime + " " + param.modCalendarStartTimeDropDown, "yyyy-MM-dd HH:mm");
         var endTime = parseStringToDate(param.modCalendarStartTime + " " + param.modCalendarEndTimeDropDown, "yyyy-MM-dd HH:mm");
         if (endTime < startTime) endTime = new Date(endTime.setDate(endTime.getDate() + 1));
         this.modCalendarIsWholeDay = 0;
         this.modCalendarStartTime = startTime;
         this.modCalendarEndTime = endTime;
      }
   } catch (err) {
      throw new Exception("timestampParse", {});
   }

   // Story settings
   // collect content
   var content = extractContent(param);
   // if all story parts are null, return with error-message
   if (!content.exists)
      throw new Exception("textMissing");
   this.checkContentSize(content.value);
   this.setContent(content.value);
   // check if the create date is set in the param object
   if (param.createtime) {
      try {
         var fmt = (param.createtime.indexOf(":") != param.createtime.lastIndexOf(":")) ? "yyyy-MM-dd HH:mm:ss" : "yyyy-MM-dd HH:mm";
         var ctime = param.createtime.toDate(fmt, this.site.getTimeZone());
      } catch (err) {
         throw new Exception("timestampParse", param.createtime);
      }
   }
   this.editableby = !isNaN(parseInt(param.editableby, 10)) ?
                  parseInt(param.editableby, 10) : EDITABLEBY_ADMINS;
   this.discussions = param.discussions ? 1 : 0;
   // create day of event
   this.day = this.modCalendarStartTime.format("yyyyMMdd");

   // check name of topic (if specified)
   if (param.topic) {
       // FIXME: this should be solved more elegantly
      if (param.topic.contains("\\") || param.topic.contains("/") || param.topic.contains("?") || param.topic.contains("+"))
         throw new Exception("topicNoSpecialChars");
      this.topic = param.topic;
   } else if (param.addToTopic) {
      this.topic = param.addToTopic;
   }
   // set event immediately online
   this.online = 1;

   var result = new Message("eventUpdate", null, this);
   return result;
}
