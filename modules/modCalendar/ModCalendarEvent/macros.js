
/**
 * Renders the Perma-URL for this event.
 * By specifying offset=1 a link to the next event is created.
 *
 * @param offset Integer, shifts event by x
 */
function url_macro(param) {
   res.write(this.getUrl(parseInt(param.offset, 10)));
   return;
}


/**
 * Renders the startDate for this Event
 *
 * @param format
 * @param as "editor"
 */
function modCalendarStartTime_macro(param) {
   var format = param.format ? param.format : "HH:mm";
   if (param.as && param.as == "dropdown" && this.modCalendarStartTime) {
      var options = [];
      for (var i=0; i<24; i++) {
         for (var j=0; j<4; j++) {
            var val = i.format("00") + ":" + (j*15).format("00");
            options.push([val, val]);
         }
      }
      var selected = formatTimestamp(this.modCalendarStartTime, "HH:mm");
      var param2 = { name: "modCalendarStartTimeDropDown",
                     id: "modCalendarStartTimeDropDown"}
      if (this.modCalendarIsWholeDay == 1)
         param2.disabled = 1;
      Html.dropDown(param2, options, selected);
   } else if (param.as && param.as == "editor" && this.modCalendarStartTime && (this._prototype == "modCalendarEvent" || this.size() == 0)) {
      Html.input({name: "modCalendarStartTime", id: "modCalendarStartTime",  value: this.modCalendarStartTime.format(format), size:15});
   } else if (param.as && param.as == "editor" && this.modCalendarStartTime) {
      res.write(this.modCalendarStartTime.format(format));
      Html.hidden({name: "modCalendarStartTime", value: this.modCalendarStartTime.format("yyyy-MM-dd")});
   } else if (this.modCalendarStartTime) {
      res.write(this.modCalendarStartTime ? formatTimestamp(this.modCalendarStartTime, format) : "");
   }
   return;
}


/**
 * Renders the endDate for this Event
 *
 * @param format
 * @param as "editor"
 */
function modCalendarEndTime_macro(param) {
   var format = param.format ? param.format : "HH:mm";
   if (param.as && param.as == "dropdown") {
      var options = [];
      for (var i=0; i<24; i++) {
         for (var j=0; j<4; j++) {
            var val = i.format("00") + ":" + (j*15).format("00");
            options.push([val, val]);
         }
      }
      var selected = this.modCalendarEndTime.format("HH:mm");
      var param2 = { name: "modCalendarEndTimeDropDown",
                     id: "modCalendarEndTimeDropDown"}
      if (this.modCalendarIsWholeDay == 1)
         param2.disabled = 1;
      Html.dropDown(param2, options, selected);
   } else if (param.as && param.as == "editor") {
      Html.input({name: "modCalendarEndTime", id: "modCalendarEndTime",  value: this.modCalendarEndTime.format("yyyy-MM-dd"), size:15});
   } else {
      res.write(this.modCalendarEndTime ? formatTimestamp(this.modCalendarEndTime, format) : "");
   }
   return;
}


/**
 * Renders the until-TimeStamp for this Master-Event
 *
 * @param format
 * @param as "editor"
 */
function modCalendarUntil_macro(param) {
   var format = param.format ? param.format : "HH:mm";
   var until = this.modCalendarUntil ? formatTimestamp(this.modCalendarUntil, format) : "";
   if (param.as && param.as == "editor") {
      Html.input({name: "modCalendarUntil", id: "modCalendarUntil",  value: until, size:15});
   } else {
      res.write(until);
   }
   return;
}


/**
 * Renders the time span of this event
 * @param format Format to be used to render the Time
 * @param isWholeDay String, to be rendered if event IsWholeDay
 * @param span String, to be rendered between Start and Endtime
 */
function modCalendarTimeSpan_macro(param) {
   if (this.modCalendarIsWholeDay == 1) {
      res.write(param.isWholeDay != null ? param.isWholeDay : getMessage("modCalendarEvent.isWholeDay"));
   } else {
      var fmt = param.format ? param.format : "HH:mm";
      var endtime = formatTimestamp(this.modCalendarEndTime, fmt);
      if (endtime == "00:00") endtime = "24:00"; // ???
      var span = param.span ? param.span : " - ";
      res.write(formatTimestamp(this.modCalendarStartTime, fmt) + span + endtime);
   }
   return;
}


/**
 * Renders the skin ModCalendarEvent.editEventRecurrenceOptions for the MasterEvent
 */
function modCalendarRecurrenceOptions_macro(param) {
   if (this._prototype == "ModCalendarMasterEvent" && this.size() == 0)
      this.renderSkin("editEventRecurrenceOptions");
   return;
}


/**
 * Renders the CheckBox for the IsRecurrence-Flag
 */
function modCalendarIsRecurrence_macro(param) {
   param.name = "modCalendarIsRecurrence";
   param.value = "1";
   if (this.size() > 0) {
      param.disabled = "disabled";
      param.checked = "checked";
   }
   Html.checkBox(param);
   return;
}


/**
 * Renders the CheckBox for a specific Recurrence WeekDay
 * @param day 0=sunday,..6=saturday
 */
function modCalendarRecurrenceDay_macro(param) {
   param = this.createCheckBoxParam("modCalendarRecurrenceDay" + param.day, param);
   Html.checkBox(param);
   return;
}


/**
 * This macro renders a list of existing topics as dropdown element.
 * FIXME
 * @doclevel admin
 */
function topicchooser_macro(param) {
   var c = getDBConnection("twoday");
   var dbError = c.getLastError();
   if (dbError)
      return getMessage("error.database", dbError);

   // FIXME
   var query = 'SELECT DISTINCT TEXT_TOPIC FROM AV_TEXT WHERE TEXT_F_SITE = ' + res.handlers.site._id + ' AND TEXT_PROTOTYPE = \'ModCalendarEvent\' AND TEXT_TOPIC != \'\'';

   var rows = c.executeRetrieval(query);
   var dbError = c.getLastError();
   if (dbError)
      return getMessage("error.database", dbError);

   if (!param.firstOption)
      param.firstOption = "--- " + getMessage("Story.topicChooser.firstOption") + " ---";

   var selected = param.firstOption;
   var options = new Array();
   while (rows.next()) {
      var t = rows.getColumnItem("TEXT_TOPIC");
      options[options.length] = {value: t, display: t};
      if (this.topic == t)
         selected = t;
   }
   rows.release();

   param.name = "addToTopic";
   Html.dropDown(param, options, selected, param.firstOption);
   return;
}


/**
 * Renders the DayView for this Event
 *
 * @param skin   default='dayView'
 * @skin modCalendarDay.dayView
 * @skin modCalendarEvent.dayView
 */
function modCalendarDayView_macro(param) {
   var skinName = param.skin ? param.skin : "dayView";
   var cdate = modCalendarFetchDisplayDate(param, this);
   var day = new ModCalendarDay(this.site, cdate);
   day.renderSkin(skinName);
   return;
}


/**
 * Renders the WeekView for this Event
 * 
 * @param skin   default='weekView'
 * @skin modCalendarWeek.weekView
 * @skin modCalendarDay.weekView
 * @skin modCalendarEvent.weekView
 */
function modCalendarWeekView_macro(param) {
   var skinName = param.skin ? param.skin : "weekView";
   var cdate = modCalendarFetchDisplayDate(param, this);
   var week = new ModCalendarWeek(this.site, cdate);
   week.renderSkin(skinName);
   return;
}


/**
 * Renders the MonthView for this Event
 * 
 * @param skin   default='monthView'
 * @skin modCalendarMonth.weekView
 * @skin modCalendarWeek.monthView
 * @skin modCalendarDay.monthView
 * @skin modCalendarEvent.monthView
 */
function modCalendarMonthView_macro(param) {
   var skinName = param.skin ? param.skin : "monthView";
   var cdate = modCalendarFetchDisplayDate(param, this);
   var month = new ModCalendarMonth(this.site, cdate);
   month.renderSkin(skinName);
   return;
}
