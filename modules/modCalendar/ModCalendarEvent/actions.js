
/**
 * main action
 */
function main_action() {
   res.data.title = this.site.title;
   var storytitle = this.getRenderedContentPart("title");
   if (storytitle)
      res.data.title += ": " + stripTags(storytitle);
   res.data.body = this.renderSkinAsString("eventView");
   this.site.renderPage();
   // increment read-counter
   this.incrementReadCounter();
   logAccess();
   return;
}


/**
 * Deletes this Event, resp. Master-Event. Asks for confirmation first.
 */
function delete_action() {

   if (req.data.cancel) {
      res.redirect(this.site.calendar.href() + "?calday=" + this.day);

   } else if (req.data.remove) {
      try {
         res.message = this.site.calendar.deleteEvent(this);
         res.redirect(this.site.calendar.href() + "?calday=" + this.day);
      } catch (err) {
         res.message = err.toString();
      }

   } else if (this.parent && !req.data.editChild && !req.data.editMaster) {
      res.data.title = getMessage("modCalendarEvent.delete.title");
      res.data.body = this.renderSkinAsString("askMasterOrChild");
      this.site.renderPage();
      return;

   } else if (this.parent && req.data.editMaster) {
      res.redirect(this.parent.href("delete"));

   }
   
   res.data.action = this.href(req.action);
   var skinParam = {
      description: getMessage("m" + this._prototype.substring(1) + ".delete.deleteDescription"),
      detail: this.title + " (" + formatTimestamp(this.modCalendarStartTime, "yyyy-MM-dd HH:mm") + ")"
   }
   res.data.title = getMessage("modCalendarEvent.delete.title");
   res.data.body = this.renderSkinAsString("delete", skinParam);
   res.data.title += ": " + encode(this.title);
   this.site.renderPage();
   return;
}


/**
 * edit action for this event
 */
function edit_action() {
   // restore any rescued text
   if (session.data.rescuedText)
      restoreRescuedText();

   if (req.data.cancel) {
      res.redirect(this.site.calendar.href() + "?calday=" + this.day);

   } else if (req.data.publish) {
      try {
         if (this instanceof ModCalendarMasterEvent)
            var result = this.evalMasterEvent(req.data, session.user);
         else
            var result = this.evalEvent(req.data, session.user);
         res.message = result.toString();
         res.redirect(this.site.calendar.href() + "?calday=" + this.day);
      } catch (err) {
         res.message = err.toString() + err.lineNumber + err.fileName;
      }

   } else if (this.parent && !req.data.editChild && !req.data.editMaster) {
      res.data.title = getMessage("modCalendarEvent.edit.title");
      res.data.body = this.renderSkinAsString("askMasterOrChild");
      this.site.renderPage();
      return;

   } else if (this.parent && req.data.editMaster) {
      res.redirect(this.parent.href("edit"));

   }

   if (this.modCalendarIsWholeDay) req.data.modCalendarIsWholeDay = "1";
   res.data.title =  getMessage("m" + this._prototype.substring(1) + ".edit.title");
   res.data.body = this.renderSkinAsString("edit");
   this.site.renderPage();

   return;
}
