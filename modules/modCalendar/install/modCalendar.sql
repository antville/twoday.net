
alter table AV_TEXT add column TEXT_MOD_CALENDAR_STARTTIME datetime;
alter table AV_TEXT add column TEXT_MOD_CALENDAR_ENDTIME datetime;
alter table AV_TEXT add column TEXT_MOD_CALENDAR_UNTIL datetime;
alter table AV_TEXT add column TEXT_MOD_CALENDAR_ISWHOLEDAY tinyint(1);
alter table AV_TEXT modify column TEXT_PROTOTYPE varchar(30);
