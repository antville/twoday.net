
/**
 * Sets groupname to the formatted timestamp
 */
function constructor(site, date) {
   this.site = site;
   this.groupname = date.format("yyyyMMdd");
   return;
}


/**
 * Returns the Perma-URL for this Day
 *
 * @param offset    Integer, shifts date by x Days; special value: "now"
 */
function getUrl(offset) {
   if (offset == "now") {
      var cday = (new Date()).format("yyyyMMdd");
   } else if (offset) {
      var date = parseStringToDate(this.groupname, "yyyyMMdd");
      var cday = (new Date(date.setDate(date.getDate() + parseInt(offset, 10)))).format("yyyyMMdd");
   } else {
      var cday = this.groupname;
   }
   return this.site.calendar.href("dayview") + "?calday=" + cday;
}
