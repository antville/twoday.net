
/**
 * Renders the Perma-URL for this day
 *
 * @param offset Integer, shifts date by x Weeks; special value: "now"
 */
function url_macro(param) {
   res.write(this.getUrl(param.offset));
   return;
}


/**
 * Renders the date in a specified format
 *
 * @expample
 *   <a href="<% this.date offset="+1" %>">Go to next day <% this.date format="dd.MM.yyy" offset="+1" %></a>
 * @param format Default: 'EEEE'
 * @param offset Integer, shifts date by x Days; special value: "now"
 */
function date_macro(param) {
   if (param.offset == "now") {
      var date = new Date();
   } else {
      var date = parseStringToDate(this.groupname, "yyyyMMdd");
      if (param.offset) new Date(date.setDate(date.getDate() + param.offset));
   }
   return formatTimestamp(date, param.format ? param.format : "EEEE");
}


/**
 * Loops through the Events of this Day and renders a certain Skin for them
 *
 * @param skin
 * @param excludeTopics comma-separated List of Topics to be excluded from rendering; by default no topics are excluded;
 * @param includeTopics comma-separated List of Topics to be included for rendering; by default all topics are rendered;
 * Note: The topic for Events without a topic is set to the emtpy string. So in order to just include all Events without a Topic 
 *  assigned, you can use includeTopics=","
 * 
 */
function listEvents_macro(param) {
   var skinName = param.skin ? param.skin : "dayView";
   var coll = this.site.calendar.day.get(this.groupname);
   var excludeTopics = param.excludeTopics ? param.excludeTopics.split(",") : [];
   var includeTopics = param.includeTopics ? param.includeTopics.split(",") : [];
   if (!coll) return;
   for (var i=0; i<coll.count(); i++) {
      var topic = coll.get(i).topic ? coll.get(i).topic : "";
      if (includeTopics.length > 0 && !Array.contains(includeTopics, topic))
         continue;
      if (excludeTopics.length > 0 && Array.contains(excludeTopics, topic))
         continue;
      coll.get(i).renderSkin(skinName);
   }
   return;
}


/**
 * Display a link to let the user add a new event for this day
 */
function addevent_macro (param) {
   if (session.user) {
      try {
         path.Site.calendar.checkAdd(session.user, res.meta.memberlevel);
      } catch (deny) {
         return;
      }
      param.linkto = "create";
      param.urlparam = "calday=" + this.groupname;
      Html.openTag("a", path.Site.calendar.createLinkParam(param));
      if (param.text)
         res.format(param.text);
      else
         res.write(getMessage("modCalendar.addEvent"));
      Html.closeTag("a");
   }
   return;
}
