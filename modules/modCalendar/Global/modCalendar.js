
app.modules.modCalendar = {
   title: "Calendar",
   description: "Enables the user to manage Events and display a calendar",
   version: "0.9",
   state: "beta",
   url: "",
   author: "Wolfgang Kamir, Michael Platzer",
   authorEmail: "wolfgang@knallgrau.at, michael.platzer@knallgrau.at",
   authorUrl: "http://www.knallgrau.at",
   authorOrganisation: "knallgrau.at",
   copyright: "(c) 2004 knallgrau.at",
   license: "twoday",
   licenseUrl: "http://www.twoday.org",
   isSidebar: false,
   hasSiteSettings: false,
   skins: ["ModCalendarWeek.weekView", "ModCalendarDay.weekView", "ModCalendarEvent.weekView", 
      "ModCalendarDay.dayView", "ModCalendarEvent.dayView", 
      "ModCalendarEvent.eventView"]
};
