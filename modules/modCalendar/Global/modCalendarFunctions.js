
/**
 * Fetches the current date to displayed
 *
 * Order of precedence:
 *  1. URL-Parameters
 *  2. Request-Path
 *  3. Macro-Parameters
 *  4. date of the optional event 
 *  5. now
 * Default return value is the current Date
 *
 * @return Date.Object
 */
function modCalendarFetchDisplayDate(param, event) {
   if (req.data.calmonth) 
      var date = parseStringToDate(req.data.calmonth, "yyyyMM");
   else if (req.data.calweek)
      var date = parseStringToDate(req.data.calweek, "yyyyw");
   else if (req.data.calday)
      var date = parseStringToDate(req.data.calday, "yyyyMMdd");
   else if (req.data.calevent && path.Site.calendar.events.get(req.data.calevent))
      var date = path.Site.calendar.events.get(req.data.calevent).modCalendarStartTime;
   else if (path.ModCalendarMonth)
      var date = parseStringToDate(path.ModCalendarMonth.groupname, "yyyyMM");
   else if (path.ModCalendarWeek)
      var date = parseStringToDate(path.ModCalendarWeek.groupname, "yyyyw");
   else if (path.ModCalendarDay)
      var date = parseStringToDate(path.ModCalendarDay.groupname, "yyyyMMdd");
   else if (path.ModCalendarEvent)
      var date = path.ModCalendarEvent.modCalendarStartTime;
   else if (param && param.calmonth)
      var date = parseStringToDate(param.calmonth, "yyyyMM");
   else if (param && param.calweek)
      var date = parseStringToDate(param.calweek, "yyyyw");
   else if (param && param.calday)
      var date = parseStringToDate(param.calday, "yyyyMMdd");
   else if (param && param.calevent && path.Site.calendar.events.get(param.calevent))
      var date = path.Site.calendar.events.get(param.calevent).modCalendarStartTime;
   else if (event)
      var date = event.modCalendarStartTime;
   if (date == null) date = new Date();
   return date;
}
