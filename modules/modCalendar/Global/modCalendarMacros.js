
/**
 * Renders the date fetched via modCalendarFetchDisplayDate()
 *
 * @see Global.modCalendarFetchDisplayDate
 * @param format format of returned Date
 */
function modCalendarNow_macro(param) {
   var event = path.modCalendarEvent;
   var date = modCalendarFetchDisplayDate(param, event);
   return formatTimestamp(date, param.format);
}


/**
 * Renders the WeekView
 */
function modCalendarWeekView_macro(param) {
   var cdate = modCalendarFetchDisplayDate();
   var week = new ModCalendarWeek(path.Site, cdate);
   week.renderSkin("weekView");
   return;
}
