
/**
 * constructor function for ModCalendarMasterEvent objects
 */
function constructor(creator, ipaddress) {
   this.reads = 0;
   this.ipaddress = ipaddress;
   this.creator = creator;
   this.editableby = EDITABLEBY_ADMINS;
   this.createtime = new Date();
   this.modifytime = new Date();

   var date = new Date();
   date.setHours(9, 0, 0);
   this.modCalendarStartTime = date;
   date.setHours(18, 0, 0);
   this.modCalendarEndTime = date;
   this.modCalendarUntil = this.modCalendarStartTime;

   this.modCalendarIsWholeDay = 0;
}
