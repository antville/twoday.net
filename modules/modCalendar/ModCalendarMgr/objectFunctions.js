
/**
 * Creates a new Event (resp. MasterEvent).
 *
 * @param param Object 
 *        .http_remotehost String IP-Address of UserClient
 *        .content_xyz String ContentParts
 *        .createtime String, optional, used for explicitely setting the CreateTime
 *        .editableby
 *        .discussions
 *        .topic
 *        .addToTopic
 *        .modCalendarIsRecurrence
 *        .modCalendarIsWholeDay
 *        .modCalendarStartTime
 *        .modCalendarStartTimeDropDown
 *        .modCalendarEndTimeDropDown
 *        .modCalendarUntil
 * @param creator User
 * @return Object
 */
function evalNewEvent(param, creator) {
   if (param.modCalendarIsRecurrence) {
      var s = new ModCalendarMasterEvent(creator, param.http_remotehost);
   } else {
      var s = new ModCalendarEvent(creator, param.http_remotehost);
   }

   s.site = this._parent;
   s.evalEvent(param, creator);

   if (param.modCalendarIsRecurrence) {
      try {
         s.modCalendarUntil = parseStringToDate(param.modCalendarUntil + " " + s.modCalendarStartTime.format("HH:mm"), "yyyy-MM-dd HH:mm");
      } catch (err) {
         throw new Exception("timestampParse", {});
      }   
   }

   // store the event
   if (param.modCalendarIsRecurrence) {
      this.masterEvents.add(s);
      // create all child-events
      var days = Math.round((s.modCalendarUntil.getTime() - s.modCalendarStartTime.getTime()) / Date.ONEDAY) + 1;
      for (var i=0; i<days; i++) {
         var childStartTime = new Date(s.modCalendarStartTime.getTime() + Date.ONEDAY * i);
         childStartTime = new Date(childStartTime.setHours(s.modCalendarStartTime.getHours()));
         childStartTime = new Date(childStartTime.setMinutes(s.modCalendarStartTime.getMinutes()));
         if (!param["modCalendarRecurrenceDay" + childStartTime.getDay()]) continue;
         var e = new ModCalendarEvent(creator, param.http_remotehost);
         var content = extractContent(param);
         e.setContent(content.value);
         e.online = s.online;
         e.createtime = s.createtime;
         e.editableby = s.editableby;
         e.discussions = s.discussions;
         e.topic = s.topic;
         e.modCalendarStartTime = childStartTime;
         var childEndTime = new Date(childStartTime.setHours(s.modCalendarEndTime.getHours()));
         childEndTime = new Date(childEndTime.setMinutes(s.modCalendarEndTime.getMinutes()));
         e.modCalendarEndTime = childEndTime;
         e.modCalendarIsWholeDay = s.modCalendarIsWholeDay;
         e.day = e.modCalendarStartTime.format("yyyyMMdd");
         e.parent = s;
         e.site = s.site;
         s.add(e);
      }
      // handle the case where no children events have been created
      if (s.count() == 0) {
         s.remove();
         throw new Exception("eventCreate");
      }
      var revent = s.get(0);
   } else {
      this.events.add(s);
      var revent = s;
   }
   
   // update affected static RSS Feeds NOTYETIMPLEMENTED

   var result = new Message("eventCreate", null, revent);
   result.id = revent._id;
   result.url = this.href();
   return result;
}


/**
 * Removes this Event (and also all Child-Events)
 * @param eventObj modCalendarEvent to be removed
 */
function deleteEvent(eventObj) {
   if (eventObj.parent && eventObj.parent.size() == 1) {
      this.deleteEvent(eventObj.parent);
   }
   for (var i=eventObj.size()-1; i>=0; i--) {
      eventObj.get(i).remove()
   }
   eventObj.remove();
   return new Message("eventDelete");
}
