
/**
 * wrapper action for week view
 */
function main_action() {
   this.weekview_action();
   return;
}


/**
 * week view action
 */
function weekview_action() {
   var cdate = modCalendarFetchDisplayDate();
   res.data.title = path.Site.title + " " + 
                    getMessage("modCalendarMgr.main.title") + 
                    ": " + formatTimestamp(cdate, "yyyy w");
   var week = new ModCalendarWeek(this._parent, cdate);
   res.data.body = week.renderSkinAsString("weekView");
   this._parent.renderPage();
   logAccess();
   return;
}


function monthview_action() {
   // CONTINUE
   return;
}


/**
 * Renders all events of a single day
 * 
 * @skin 
 *
 */
function dayview_action() {
   var cdate = modCalendarFetchDisplayDate();
   res.data.title = path.Site.title + " " + 
                    getMessage("modCalendarMgr.main.title") + 
                    ": " + formatTimestamp(cdate, "dd.MM.yyyy");
   var day = new ModCalendarDay(this._parent, cdate);
   res.data.body = day.renderSkinAsString("dayView");
   this._parent.renderPage();
   logAccess();
   return;
}


/**
 * action for creating new (Master-)Event
 */
function create_action() {
   // restore any rescued text
   if (session.data.rescuedText)
      restoreRescuedText();

   // storing referrer in session-cache in case user clicks cancel later
   if (!session.data.referrer && req.data.http_referer)
      session.data.referrer = req.data.http_referer;
   
   if (req.data.cancel) {
      var url = session.data.referrer ? session.data.referrer : this.href();
      session.data.referrer = null;
      res.redirect(url);
   } else if (req.data.save || req.data.publish) {
      try {
         var result = this.evalNewEvent(req.data, session.user);
         res.message = result.toString();
         session.data.referrer = null;
         res.redirect(this.href() + "?calevent=" + result.id);
      } catch (err) {
         res.message = err.toString() + err.lineNumber + err.fileName;
      }
   }

   var event = new ModCalendarMasterEvent(session.user, req.data.http_remotehost);
   if (req.data.calday) {
      var cdate = parseStringToDate(req.data.calday, "yyyyMMdd");
      event.modCalendarStartTime = new Date(event.modCalendarStartTime.setDate(cdate.getDate()));
      event.modCalendarEndTime = new Date(event.modCalendarEndTime.setDate(cdate.getDate()));
      event.modCalendarStartTime = new Date(event.modCalendarStartTime.setMonth(cdate.getMonth()));
      event.modCalendarEndTime = new Date(event.modCalendarEndTime.setMonth(cdate.getMonth()));
      event.modCalendarStartTime = new Date(event.modCalendarStartTime.setFullYear(cdate.getFullYear()));
      event.modCalendarEndTime = new Date(event.modCalendarEndTime.setFullYear(cdate.getFullYear()));
      event.modCalendarUntil = event.modCalendarStartTime;
   }
   req.data["modCalendarRecurrenceDay" + event.modCalendarStartTime.getDay()] = "1";
   event.discussions = this._parent.preferences.getProperty("discussions");
   
   res.data.title = getMessage("modCalendarMgr.create.title");
   res.data.action = this.href("create");
   res.data.recurrence = "0";
   res.data.body = event.renderSkinAsString("edit");
   this._parent.renderPage();
   return;
}
