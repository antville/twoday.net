
/**
 * Permission checks (called by hopobject.onRequest())
 *
 * @param action  String, name of action
 * @param usr     User, who wants to access an action
 * @param level   Int, Membership level
 * @return Object if permission was denied <br />
 *   .message  message, if something went wrong <br />
 *   .url      recommanded redirect URL
 * @doclevel public
 */
function checkAccess(action, usr, level) {
   try {
      // currently just allow the calendar for trusted Sites
      if (!this._parent.trusted) throw new DenyException("modCalendarView");
      if (usr && usr.sysadmin) return;
      switch (action) {
         case "main" :
         case "dayview" :
         case "weekview" :
         case "monthview" :
            if (!this._parent.online)
               checkIfLoggedIn(this.href(action));
            url = root.href();
            this.checkView(usr, level);
            break;
         case "create" :
            checkIfLoggedIn(this.href(req.action));
            this.checkAdd(usr, level);
      }
   } catch (deny) {
      return {message: deny.toString(), url: this._parent.href()};
   }
   return;
}


/**
 * Checks whether user is allowed to view the calendar
 *
 * @param Obj Userobject
 * @param Int Permission-Level
 * @return String Reason for denial (or null if allowed)
 */
function checkView(usr, level) {
   if (!this._parent.online && level == null)
      throw new DenyException("modCalendarView");
   return;
}


/**
 * Checks whether user is allowed to add Events
 *
 * @param Obj Userobject
 * @param Int Permission-Level
 * @return String Reason for denial (or null if allowed)
 */
function checkAdd(usr, level) {
   if (!this._parent.preferences.getProperty("usercontrib") && (level & MAY_ADD_STORY) == 0)
      throw new DenyException("storyAdd");
   return;
}
