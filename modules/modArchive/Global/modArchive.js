
app.modules.modArchive = {
   version: "",
   state: "beta",
   url: "",
   author: "Wolfgang Kamir",
   authorEmail: "wolfgang@knallgrau.at",
   authorUrl: "http://www.knallgrau.at",
   authorOrganisation: "knallgrau.at",
   copyright: "(c) 2004 knallgrau.at",
   license: "twoday",
   licenseUrl: "http://www.twoday.org",
   isSidebar: true,
   type: MODULE_TYPE_NAVIGATION,
   hasSiteSettings: true,
   skins: ["Site.calendar", "Site.calendarweek", "Site.calendarday", "Site.calendardayheader", "Site.calendarselday"]
};


app.modules.modArchive.renderSidebarItem = function() {
   var selected = this.preferences.getProperty("modarchivetype") ? this.preferences.getProperty("modarchivetype") : 0;
   var p = {};
   res.push();
   if (selected == 1)
      this.monthlist_macro({});
   else
      this.calendar_macro({});
   p.body = res.pop();
   this.renderSidebarModuleItem("modArchive", p);
   return;
}

app.modules.modArchive.renderPreferences = function() {
   var selected = this.preferences.getProperty("modarchivetype") ? this.preferences.getProperty("modarchivetype") : 0;
   var options = new Array();
   options.push(getMessage("modArchive.wizard.admin.calendar"));
   options.push(getMessage("modArchive.wizard.admin.monthlist"));
   res.push();
   Html.dropDown({name: "modArchiveType"}, options, selected, null);
   var param = {
      label: getMessage("modArchive.wizard.admin.type"),
      field: res.pop()
   };
   Module.prototype.renderSetupLine(param);
};


app.modules.modArchive.evalModulePreferences = function() {
   this.preferences.setProperty("modarchivetype", req.data.modArchiveType);
};
