
/**
 * Renders a random image to the response, and stores that challenge in the current session.
 *
 */
function modJCaptcha_action() {
   if (knallgrau.isRequestByRobot()) {
      return;
   }

   if (!app.data.modJCaptchaService) {
      var pkg = Packages.com.octo.captcha;
      var water = new Packages.com.jhlabs.image.WaterFilter();
      water.setAmplitude(3);
      water.setAntialias(true);
      water.setPhase(200);
      water.setWavelength(70);
      var filterArray = new java.lang.reflect.Array.newInstance(java.awt.image.ImageFilter, 0);
      var backDef = new pkg.component.image.deformation.ImageDeformationByFilters(filterArray);
      var textDef = new pkg.component.image.deformation.ImageDeformationByFilters(filterArray);
      var filt = new java.lang.reflect.Array.newInstance(java.awt.image.ImageFilter, 1);
      filt[0] = water;
      var postDef = new pkg.component.image.deformation.ImageDeformationByFilters(filt);
      var dictionnaryWords = new pkg.component.word.wordgenerator.ComposeDictionaryWordGenerator(new pkg.component.word.FileDictionary("toddlist"));
      var randomPaster = new pkg.component.image.textpaster.BaffleRandomTextPaster(4, 5, java.awt.Color.black, 0, java.awt.Color.white);
      var back = new pkg.component.image.backgroundgenerator.UniColorBackgroundGenerator(180, 75, java.awt.Color.white);
      var shearedFont = new pkg.component.image.fontgenerator.RandomFontGenerator(30, 40);
      var word2image = new pkg.component.image.wordtoimage.DeformedComposedWordToImage(shearedFont, back, randomPaster, backDef, textDef, postDef);
      app.data.modJCaptchaService = new pkg.image.gimpy.GimpyFactory(dictionnaryWords, word2image);
   }

   var captcha = app.data.modJCaptchaService.getImageCaptcha();
   session.data.modJCaptchaResponse = captcha.getResponse();
   // app.log("##" + session.data.modJCaptchaResponse);
   var challenge = captcha.getImageChallenge();

   var jpegOutputStream = new java.io.ByteArrayOutputStream();
   var jpegEncoder = Packages.com.sun.image.codec.jpeg.JPEGCodec.createJPEGEncoder(jpegOutputStream);
   jpegEncoder.encode(challenge);

   var captchaChallengeAsJpeg = jpegOutputStream.toByteArray();
   res.contentType = "image/jpeg";
   res.writeBinary(captchaChallengeAsJpeg);
}
