
app.modules.modJCaptcha = {
   title: "JCatpcha - keeps spam bots away",
   description: "Generates random images that only humans can read.",
   version: "",
   state: "",
   url: "",
   author: "",
   authorEmail: "",
   authorUrl: "",
   authorOrganisation: "Knallgrau",
   license: "twoday",
   licenseUrl: "",
   licenseText: "",
   copyright: "",
   isSidebar: false,
   hasSiteSettings: false,
   skins: []
}


/**
 * security check for modJCaptcha_action
 */
app.modules.modJCaptcha.checkMemberMgrAccess = function(params) {
   var action = params[0];
   var usr    = params[1];
   var level  = params[2];
   switch (action) {
      case "modJCaptcha" :
         break;
   }
};


/**
 * Renders question in registration.
 */
app.modules.modJCaptcha.renderMemberMgrRegisterStep01 = function(args) {
   if (app.properties.modJCaptchaOnRegistration != "true") return;
   var obj = path.Site || root;
   obj.members.renderSkin("modJCaptcha");
}


/**
 * Check for whether answer and challenge match on registration. If not, then throw an exception.
 */
app.modules.modJCaptcha.onBeforeEvalRegistration = function(args) {
   if (app.properties.modJCaptchaOnRegistration != "true") return;
   if (root.users.count() == 0) return;
   if (!session.data.modJCaptchaResponse) {
      throw new Exception("modJCaptcha.noChallenge");
   }
   if (session.data.modJCaptchaResponse != req.data.modJCaptchaAnswer) {
      throw new Exception("modJCaptcha.wrongAnswer");
   }
   session.data.modJCaptchaResponse = null;
   return;
}


/**
 * Renders question for anonymous users while posting a comment.
 */
app.modules.modJCaptcha.renderCommentEdit = function(args) {
   if (app.properties.modJCaptchaOnAnonymousComment != "true") return;
   if (session.user) return;
   var obj = path.Site || root;
   obj.members.renderSkin("modJCaptcha");
}


/**
 * Check for whether answer and challenge match on posting an anonymous comment. If not, then throw an exception.
 */
app.modules.modJCaptcha.onBeforeEvalComment = function(args) {
   if (app.properties.modJCaptchaOnAnonymousComment != "true") return;
   if (session.user) return;
   if (!session.data.modJCaptchaResponse) {
      throw new Exception("modJCaptcha.noChallenge");
   }
   if (session.data.modJCaptchaResponse != req.data.modJCaptchaAnswer) {
      throw new Exception("modJCaptcha.wrongAnswer");
   }
   session.data.modJCaptchaResponse = null;
   this.applyModuleMethods("onAfterModJCaptchaEvalComment", args);
   return;
}


/**
 * Renders question for users which create new site.
 */
app.modules.modJCaptcha.renderMemberMgrNewSite = function(args) {
   if (app.properties.modJCaptchaOnAnonymousComment != "true") return;
   var obj = path.Site || root;
   obj.members.renderSkin("modJCaptcha");
}


/**
 * Check for whether answer and challenge match on creating a new site. If not, then throw an exception.
 */
app.modules.modJCaptcha.onBeforeEvalNewSite = function(args) {
   if (req.data.http_referer.indexOf("sitesetupwizard") != -1) return;
   if (app.properties.modJCaptchaOnEvalNewSite != "true") return;
   if (!session.data.modJCaptchaResponse) {
      throw new Exception("modJCaptcha.noChallenge");
   }
   if (session.data.modJCaptchaResponse != args[0].modJCaptchaAnswer) {
      res.message = getMessage("error.modJCaptcha.wrongAnswer");
      res.redirect(path.membermgr.href("newsite") + "?step=choosename");
   }
   session.data.modJCaptchaResponse = null;
   return;
}


/**
 * Renders question for user when trying to login a bit to long to prevent brute-force attacks
 */
app.modules.modJCaptcha.renderMemberMgrLogin = function(args) {
   if (!req.data.name) return;
   var user = root.getUserByKey(req.data.name, "local");
   if (user && user.failedLoginCount > 2) {
      var obj = path.Site || root;
      obj.members.renderSkin("modJCaptcha");
   }
}


/**
 * Check for whether answer and challenge match on login
 */
app.modules.modJCaptcha.onBeforeEvalLogin = function(args) {
   var username = args[0].name;
   var user = root.getUserByKey(username, "local");
   if (app.properties.modJCaptchaOnEvalLogin != "true") return;
   if (user && user.failedLoginCount > 2 && !session.data.modJCaptchaResponse) {
      throw new Exception("modJCaptcha.loginFailed");
   }
   if (user && user.failedLoginCount > 2 && session.data.modJCaptchaResponse != args[0].modJCaptchaAnswer) {
      throw new Exception("modJCaptcha.wrongAnswer");
   }
   session.data.modJCaptchaResponse = null;
   return;
}
