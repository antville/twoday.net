
app.modules.modRecentUpdates = {
   version: "",
   state: "beta",
   url: "",
   author: "Wolfgang Kamir",
   authorEmail: "wolfgang@knallgrau.at",
   authorUrl: "http://www.knallgrau.at",
   authorOrganisation: "knallgrau.at",
   copyright: "(c) 2004 knallgrau.at",
   license: "twoday",
   licenseUrl: "http://www.twoday.org",
   isSidebar: true,
   type: MODULE_TYPE_NAVIGATION,
   hasSiteSettings: true,
   skins: ["Story.historyview", "Comment.historyview"]
};


app.modules.modRecentUpdates.renderSidebarItem = function() {
   var p = {};
   var param = {};
   res.push();
   param.limit = this.preferences.getProperty("modrecentupdates_limit");
   param.show = this.preferences.getProperty("modrecentupdates_show");
   this.history_macro(param);
   p.body = res.pop();
   this.renderSidebarModuleItem("modRecentUpdates", p);
   return;
};


app.modules.modRecentUpdates.renderPreferences = function() {
   Module.prototype.renderSetupSpacerLine();

   var limit = this.preferences.getProperty("modrecentupdates_limit");
   if (!limit) limit = 8;
   var options = [];
   for (var i=1; i<21; i++) options.push([i, i]);
   var param = {
      label: getMessage("modRecentUpdates.preferences.limit"),
      field: Html.dropDownAsString({name: "modrecentupdates_limit"}, options, limit)
   };
   Module.prototype.renderSetupLine(param);

   var options = [
      {value: "all", display: getMessage("modRecentUpdates.preferences.show.all")},
      {value: "stories", display: getMessage("modRecentUpdates.preferences.show.stories")},
      {value: "comments", display: getMessage("modRecentUpdates.preferences.show.comments")}
   ];
   var param = {
      label: getMessage("modRecentUpdates.preferences.show"),
      field: Html.dropDownAsString({name: "modrecentupdates_show"}, options, this.preferences.getProperty("modrecentupdates_show"))
   };
   Module.prototype.renderSetupLine(param);
};


app.modules.modRecentUpdates.evalModulePreferences = function() {
   this.preferences.setProperty("modrecentupdates_limit", req.data.modrecentupdates_limit);
   this.preferences.setProperty("modrecentupdates_show", req.data.modrecentupdates_show);
};
