
app.modules.modRandomImage = {
   title: "Random Image",
   description: "Displays a random image from one of your image galleries.",
   version: "",
   state: "beta",
   url: "",
   author: "Wolfgang Kamir",
   authorEmail: "wolfgang@knallgrau.at",
   authorUrl: "http://www.knallgrau.at",
   authorOrganisation: "knallgrau.at",
   copyright: "(c) 2004 knallgrau.at",
   license: "twoday",
   licenseUrl: "http://www.twoday.org",
   isSidebar: true
};


/**
 * Renders module in the sidebar. The renderSidebarItem function of modRandomImage calls the randomimage_macro with param.as = "thumbnail".
 * param.topic is stored as site preference. If user has no topic (image gallery) selected, all galleries will be used.
 *
 */
app.modules.modRandomImage.renderSidebarItem = function(param) {
   var modRandomImageTopic = this.preferences.getProperty("modramdomimagetopic") ? this.preferences.getProperty("modramdomimagetopic") : "";
   var p = {}
   res.push();
   randomimage_macro({as: "thumbnail", linkto: "main", topic: modRandomImageTopic});
   p.body = res.pop();
   this.renderSidebarModuleItem("modRandomImage", p);
   return;
};


app.modules.modRandomImage.renderPreferences = function() {
   res.push();
   req.data.addToTopic = this.preferences.getProperty("modramdomimagetopic") ? this.preferences.getProperty("modramdomimagetopic") : "";
   res.handlers.site.images.topicchooser_macro({name: "addToTopic", firstOption: getMessage("modRandomImage.wizard.admin.all")});

   var param = {
      label: getMessage("modRandomImage.wizard.admin.topic"),
      field: res.pop()
   };
   Module.prototype.renderSetupLine(param);
};


app.modules.modRandomImage.evalModulePreferences = function() {
   this.preferences.setProperty("modramdomimagetopic", req.data.addToTopic);
};
