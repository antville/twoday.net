/***************************************************************************
    author: Franz Philipp Moser (pm AT fpm-edv.at)
    description: renders the groupname
 ***************************************************************************/

function name_macro(param) {
   res.write(this.getNavigationName());
}


function numberOfContent_macro(param) {
   res.write(this.size());
}


function styleClass_macro(param) {
      var biggest = param.biggest || 8;
      var smallest = param.smallest || 1;
      var max = session.data.maxTagSize || 999999;
      var min = session.data.minTagSize || 1;
      if (max == min) 
         return smallest;

      var factor = Number(biggest) / max;

      var tagSize = Math.round(factor * this.size());
      return (tagSize > smallest) ? tagSize : smallest;
}
