/***************************************************************************
    author: Franz Philipp Moser (pm AT fpm-edv.at)
    description: actions for a tag
 ***************************************************************************/

function main_action() {
   this.renderView(this, res.handlers.site.title + ": " + this.getNavigationName(), true);
   return;
}
