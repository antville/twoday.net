/***************************************************************************
    author: Franz Philipp Moser (pm AT fpm-edv.at)
    description: render list function
 ***************************************************************************/

/**
 * render the whole page containing a list of content
 * @param Object collection to work on
 * @param String Title to use
 */
function renderView(collection, title, skipOffline) {
   if (skipOffline) {
      collection = collection.list();
      var newCollection = new Array();
      for (var i = 0; i < collection.length; i++) {
         if (collection[i] && collection[i].content && collection[i].content.online) {
            newCollection[newCollection.length] = collection[i];
         }
      }
      collection = newCollection;
   }
   res.data.title = title;
   res.data.tagcontent = renderList(collection, "tagpreview", 10, req.data.page);
   res.data.pagenavigation = renderPageNavigation(collection, this.href(req.action), 10, req.data.page);
   res.data.body = this.renderSkinAsString("main");
   res.handlers.context.renderPage();
   return;
}
