/*
 * Lists all tags of the current Content
 */
function tags_macro(param) {
   // check if allowed to show tags
   if (res.handlers.site && res.handlers.site.preferences.getProperty("modTagActive") == "false") {
      return;
   }
   // check how many tags are there
   // get tags of content
   var tagCollection = this.tags;
   // if no tags where found
   if (!tagCollection)
      return "This Object is not taggable!";
   tagCollection.prefetchChildren(0, tagCollection.size());
   var tagCollectionSize = tagCollection.size();
   
   var tagString = new java.lang.StringBuffer();
   // Loop over Tags
   var params = new Object();
   for (var i = 0; i < tagCollectionSize; i++) {
      var theTag = tagCollection.get(i);
      tagString.append(param.itemprefix || "");
      if (param.as && param.as == "link") {
         params.count = theTag.size();
         if (res.handlers.site && res.handlers.site.tags && res.handlers.site.tags.get(theTag.name)) {
            tagString.append(res.handlers.site.tags.get(theTag.name).renderSkinAsString("listitem", params));
         }
      } else {
         tagString.append(theTag.name);
      }
      if ((i+1) < tagCollectionSize) {
         tagString.append(param.itemsuffix || " ");
      }
   }
   return tagString;
}
