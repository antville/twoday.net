/***************************************************************************
    author: Franz Philipp Moser (pm AT fpm-edv.at)
    description: evaluates and deletes tags of an object
 ***************************************************************************/
 
/*
 * Evals the Tags entered
 */
function evalTags() {
   var tagString = req.data.content_tags;
   // remove Whitespaces around ,
   if (!tagString || tagString.length <= 0) {
      // FIXME hmm should be done better, for now XMLRPC requests are skipped
      if (knallgrau.isGetPostRequest())
         this.deleteAllTags();
      return;
   }
   tagString = tagString.replace(/\s*,\s*/g, ",");
   var tagArray = tagString.split(",");
   var content = this;
   // construct the prototype od the TagMembership
   var tagMembershipPrototype = this._prototype + "TagMembership";
   // get all tags of content
   var tagCollection = content.tags;
   var tagCollectionSize = tagCollection.size();
   for (var ii = tagCollectionSize; ii > 0; ii--) {
      var tagGroupname = tagCollection.get(ii - 1).name;
      if (Array.contains(tagArray, tagGroupname)) {
         // delete it from tags
         // must be a while because a tag can not be added more than once
         while (Array.indexOf(tagArray, tagGroupname) != -1) {
            delete(tagArray[Array.indexOf(tagArray, tagGroupname)]);
         }
         // set online offline status
         tagCollection.get(ii - 1).status = (content.online == 0) ? 0 : 1;
      } else {
         // delete tagMembership
         tagCollection.get(ii - 1).remove();
      }
   }
   // add new tags to content
   for (var tagName in tagArray) {
      // check if not empty!
      if (tagArray[tagName] && tagArray[tagName].length > 0) {
         // create TagMembership
         var globalObject = getGlobalObject();
         var tagObject = new globalObject[tagMembershipPrototype](tagArray[tagName], session.user);
         // set online offline status
         tagObject.status = (content.online == 0) ? 0 : 1;
         content.tags.add(tagObject);
      }
   }
//   content.tags.invalidate();
   // fetch the grouped collection again
   var site = content.getSite();
   if (site)
      site.tags.invalidate();
   return this;
}

/**
 * function deletes all childobjects of a story (recursive!)
 */
function deleteAll() {
   for (var i=this.comments.size();i>0;i--)
      this.comments.get(i-1).remove();
   this.deleteAllTags();
   return true;
}

/*
 * hook for removing tags
 */
function deleteAllTags() {
   if (!this.tags)
      return;
   var collectionSize = this.tags.size();
   for (var i = collectionSize; i > 0; i--) {
      // delete tagMemberships
      if (this.tags.get(i-1)) {
         this.tags.get(i-1).remove();
      }
   }
   // fetch the grouped collection again
   this.getSite().tags.invalidate();
   return this;
}
