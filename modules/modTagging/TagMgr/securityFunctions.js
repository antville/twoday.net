/**
 * Permission checks (called by hopobject.onRequest())
 *
 * @param action  String, name of action
 * @param usr     User, who wants to access an action
 * @param level   Int, Membership level
 * @return Object if permission was denied <br />
 *   .message  message, if something went wrong <br />
 *   .url      recommanded redirect URL
 * @doclevel public
 */
function checkAccess(action, usr, level) {
   this.checkView(usr, level);
   return;
}

/**
 * check if a user is allowed to view a site
 * @param Obj Userobject
 * @param Int Permission-Level
 */
function checkView(usr, level) {
   if (!res.handlers.site.online) {
      // if not logged in or not logged in with sufficient permissions
      if (!usr || (level == null && !usr.sysadmin))
         throw new DenyException("siteView");
   }
   // taggs may not be viewed!
   if (res.handlers.site.preferences.getProperty("modTagActive") == "false")
      res.redirect(res.handlers.site.href());
   return;
}

