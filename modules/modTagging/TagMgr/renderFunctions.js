/***************************************************************************
    author: Franz Philipp Moser (pm AT fpm-edv.at)
    description: render list of tags for site
 ***************************************************************************/

/**
 * render the whole page containing all tags
 * @param Object collection to work on
 * @param String Title to use
 */
function renderView(collection, title, prototype) {
   var numberOfTags = collection.size();
   var numberOfTaggedEntries = 0;
   
   var maxSize = 0;
   res.data.tagList = new java.lang.StringBuffer();
   // Look how much Tags
   if (numberOfTags > 25) {
      var tag = new Tag();
      // to much tags to fetch so make a seperate sql
      var dbcon = getDBConnection ("twoday");
      var maxItems = Math.min(50, numberOfTags);
      var tmpPercent = maxItems / 100;
      var sql = "SELECT TAGMEMBERSHIP_TAGNAME, count(*) as count FROM AV_TAGMEMBERSHIP WHERE TAGMEMBERSHIP_F_SITE=" + res.handlers.site._id;
      if (prototype) {
         sql += " AND TAGMEMBERSHIP_PROTOTYPE='" + prototype + "TagMembership'";
      }
      sql += " GROUP by TAGMEMBERSHIP_TAGNAME ORDER BY count desc LIMIT " + maxItems;
      var dbres = dbcon.executeRetrieval(sql);
      var tags = [];
      if (dbres) {
         while (dbres.next()) {
            var skinParams = new Object();
            skinParams.name = dbres.getColumnItem(1).toString();
            skinParams.url = this.href();
            if (prototype) {
               prototype = prototype == "Story" ? "Storie": prototype;
            }
            skinParams.url += prototype ? prototype.toLowerCase() + "s/" + skinParams.name: skinParams.name;
            skinParams.count = dbres.getColumnItem(2).toString();
            skinParams.fontSize = Math.round(skinParams.count / tmpPercent * 10);
            if (skinParams.fontSize < 20)
               skinParams.fontSize = 20;
            if (skinParams.fontSize > 100)
               skinParams.fontSize = 100;
            maxSize = Math.max(maxSize, skinParams.fontSize);
            tags[tags.length] = skinParams;
         }
         dbres.release();
         var sorter = this.getTagSorter();
         tags.sort(sorter);
         // tags should be cached!
         for (var i = 0; i < tags.length; i++) {
            res.data.tagList.append(tag.renderSkinAsString("parameterizedlistitem", tags[i]));
         }
         delete(tag);
      }
   } else {
      collection.prefetchChildren(0, numberOfTags);
      for (var ii = 0; ii < numberOfTags; ii++) {
         numberOfTaggedEntries += collection.get(ii).size();
      }
      // a little mathematic :)
      // numberOfTaggedEntries => 100%
      // calculate 1%
      var tmpPercent = numberOfTaggedEntries / 100;
      // collection.get(ii).size() => ?%
      for (var ii = 0; ii < numberOfTags; ii++) {
         var tag = collection.get(ii);
         var tagSize = tag.size();
         var skinParams = new Object();
         skinParams.fontSize = Math.round(tagSize / tmpPercent * 10);
         if (skinParams.fontSize < 20)
            skinParams.fontSize = 20;
         if (skinParams.fontSize > 100)
            skinParams.fontSize = 100;
         maxSize = Math.max(maxSize, skinParams.fontSize);
         skinParams.count = tagSize + " Eintr�ge";
         res.data.tagList.append(tag.renderSkinAsString("listitem", skinParams));
      }
   }
   res.data.tagList = res.data.tagList.toString();
   res.data.body = this.renderSkinAsString("main", {lineHeight: maxSize});
   // for twoday
   // res.handlers.context.renderPage();
   res.handlers.site.renderSkin("page");
   return;
}
