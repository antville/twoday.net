/***************************************************************************
    author: Franz Philipp Moser (pm AT fpm-edv.at)
    description: show link to mytags_action
 ***************************************************************************/

function mytagslink_macro(param) {
   if ((session.user && session.user.tags) || param.showallways) {
      this.renderSkin("mytagslink.skin");
   }
   return;
}

