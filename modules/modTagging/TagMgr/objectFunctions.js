/***************************************************************************
    author: Franz Philipp Moser (pm AT fpm-edv.at)
    description: renders the groupname
 ***************************************************************************/

function getTagSorter() {
   return function(a, b) {
      var str1 = a.getNavigationName();
      var str2 = b.getNavigationName();
      if (str1 > str2)
         return 1;
      else if (str1 < str2)
         return -1;
      return 0;
   }

}

/*
 * returns the maximum and the minimum number of content stored in a tag from the
 * given collection
 *
 * @param collection the collection with the tags
 */
function getMaxMinTagSizes(collection) {
   var min = 999999;
   var max = 1;
   for (var i in collection) {
      var item = collection[i];
      if (item) {
         if (min > item.size()) {
            min = item.size();
         }
         if (max < item.size()) {
            max = item.size();
         }
      }
   }
   return {max: max, min: min};
}
