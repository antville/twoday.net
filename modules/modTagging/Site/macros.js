/**
 * renders a list of tags
 */
function tags_macro(param) {
   if (res.handlers.site.preferences.getProperty("modTagActive") == "true") {
      var collection = this.tags;
      var maxItems = param.max && !isNaN(parseInt(param.max, 10)) ? parseInt(param.max, 10) : 50;
      var itemskin = param.itemskin ? param.itemskin : "contentEditLink";
      var numberOfTags = Math.min(collection.size(), maxItems);
      if (numberOfTags > 25 || param.max) {
         var tag = new Tag();
         // to much tags to fetch so make a seperate sql
         var dbcon = getDBConnection("twoday");
         var sql = "SELECT TAGMEMBERSHIP_TAGNAME, count(*) as count FROM AV_TAGMEMBERSHIP WHERE TAGMEMBERSHIP_F_SITE=" + this._id + " AND TAGMEMBERSHIP_STATUS > 0 GROUP by TAGMEMBERSHIP_TAGNAME ORDER BY count desc LIMIT " + numberOfTags + ";";
         var dbres = dbcon.executeRetrieval(sql);
         if (dbres) {
            var tags = [];
            while (dbres.next()) {
               var param = new Object();
               if (this.tags.get(dbres.getColumnItem(1).toString())) {
                  tags[tags.length] = this.tags.get(dbres.getColumnItem(1).toString());
               }
            }
            dbres.release();
            var sorter = this.tags.getTagSorter();
            tags.sort(sorter);
            // store max and min
            var tagSizes = this.tags.getMaxMinTagSizes(tags);
            session.data.maxTagSize = tagSizes.max;
            session.data.minTagSize = tagSizes.min;
            var param = new Object();
            for (var i = 0; i < tags.length; i++) {
               // get tag from tagMgr and render it
               param.name = tags[i].getNavigationName();
               tags[i].renderSkin(itemskin, param);
             }
          }
      } else {
         collection.prefetchChildren(0, collection.size());
         var param = new Object()
         var tagSizes = this.tags.getMaxMinTagSizes(collection);
         session.data.maxTagSize = tagSizes.max;
         session.data.minTagSize = tagSizes.min;
         for (var ii = 0; ii < numberOfTags; ii++) {
            var tag = collection.get(ii);
            param.name = tag.getNavigationName();
            tag.renderSkin(itemskin, param);
         }
      }
      session.data.maxTagSize = null;
      session.data.minTagSize = null;
   }
}

