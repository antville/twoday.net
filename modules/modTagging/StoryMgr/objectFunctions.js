
/*
 * Evals the Tags entered
 */
function evalTags(content) {
   // construct the prototype od the TagMembership
   var tagMembershipPrototype = content._prototype + "TagMembership";
   var tagString = req.data.content_tags;
   if (!tagString || tagString.length <= 0)
      return content;
   // remove Whitespaces around ,
   tagString = tagString.replace(/\s*,\s*/g, ",");
   var tagArray = tagString.split(",");
   for (var tagName in tagArray) {
      // check if not empty and if it not exists
      if (tagArray[tagName] && tagArray[tagName].length > 0) {
         // create TagMembership
         var globalObject = getGlobalObject();
         var tagObject = new globalObject[tagMembershipPrototype](tagArray[tagName], session.user);
         tagObject.status = (content.online == 0) ? 0 : 1;
         content.tags.add(tagObject);
      }
   }
   // fetch the grouped collection again
   res.handlers.site.tags.invalidate();
   return content;
}
