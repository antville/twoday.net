/***************************************************************************
    author: Franz Philipp Moser (pm AT fpm-edv.at)
    description: constructor
 ***************************************************************************/

function constructor(tagName, tagCreator) {
   this.name = tagName;
   this.creator = tagCreator;
   this.createtime = new Date();
   // default are tags offline
   this.status = 0;
   this.site = res.handlers.site;
   return this;
}
