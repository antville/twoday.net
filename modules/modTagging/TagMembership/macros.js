/***************************************************************************
    author: Franz Philipp Moser (pm AT fpm-edv.at)
    description: renders preview of the linked content
 ***************************************************************************/

function contentPreview_macro(param) {
   var currentObject = this.content;
   currentObject.renderSkin("tagpreview");
   return;
}
