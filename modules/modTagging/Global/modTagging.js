app.modules.modTagging = {
   title: "Tagging",
   description: "Manages and displays Tags for Stories",
   version: "1.0",
   state: "beta",
   url: "",
   author: "Franz Philipp Moser",
   authorEmail: "philipp.moser@chello.at",
   authorUrl: "http://www.fpmedv.at/home/",
   authorOrganisation: "fpmedv.at",
   copyright: "(c) 2005 fpmedv.at",
   license: "antville",
   licenseUrl: "",
   hasSiteSettings: true,
   isSidebar: true,
   skins: ["Site.modTagging"],
   isRestricted: true
};


app.modules.modTagging.onEvalNewStory = function(param) {
   var theSite = this.getSite();
   if (theSite && theSite.preferences.getProperty("modTagActive") == "true") {
      var story = param[0];
      this.evalTags(story);
   }
   return;
}


/**
 * Story: After Story is edited or deleted handle tags
 */
 
/**
 * set story online or offline
 */
app.modules.modTagging.onEvalStory = function(params) {
   var theSite = this.getSite();
   if (theSite && theSite.preferences.getProperty("modTagActive") == "true") {
      var story = params[0];
      story.evalTags();
   }
   return;
}


/**
 * Twoday Specific functions
 */

app.modules.modTagging.evalNewSite = function(args) {
   var creator = args[0];
   var newSite = args[1];
   newSite.preferences.setProperty("modTagActive", false);
   return;
}


app.modules.modTagging.renderPreferences = function() {
   if (session.user && session.user.sysadmin) {
      if (this.preferences.getProperty("modTagActive") == null) {
         this.preferences.setProperty("modTagActive", "false");
      }
      Module.prototype.renderSetupSpacerLine();
      Module.prototype.renderSetupHeader({header: getMessage("modTag.wizard.admin.settingsTitle")});
      var options = [
         {value: "false", display: getMessage("generic.no")},
         {value: "true", display: getMessage("generic.yes")},
      ];
      var param = {
         label: getMessage("modTag.wizard.admin.editor"),
         field: Html.dropDownAsString({name: "modTagActive"}, options, this.preferences.getProperty("modTagActive"))
      };
      Module.prototype.renderSetupLine(param);
   } else {
      res.write("Sorry the Tagging Module is just a placeholder for now.");
   }
   return;
}


app.modules.modTagging.evalModulePreferences = function() {
   // only sysadmins may edit options of tagging module
   if (session.user && session.user.sysadmin) {
      this.preferences.setProperty("modTagActive", req.data.modTagActive);
   }
   return;
}


app.modules.modTagging.renderStoryEdit = function(params) {
   if (res.handlers.site.preferences.getProperty("modTagActive") == "true") {
      // render the story tagging skin
      this.renderSkin("modTagEditOptions");
   }
}


/**
 * Renders the "tagging module" in the sidebar.
 * @skin sidebar
 */
app.modules.modTagging.renderSidebarItem = function(param) {
   if (this.preferences.getProperty("modTagActive") != "true")
      return;
   p = new Object();
   p.body = res.handlers.site.renderSkinAsString("modTagging");
   p.editLink = false;
   res.handlers.site.renderSidebarModuleItem("modTagging", p);
   return;
}


/**
 * Checks whether this module has SidebarItem for the current site
 * @param   site   HopObject Site, optional
 * @return  Boolean
 */
app.modules.modTagging.getIsSidebar = function(site) {
   if (site && site.preferences.getProperty("modTagActive") == "true")
      return true;
   else
      return false;
}

/**
 * sets the tags online
 * @return  null
 */
app.modules.modTagging.onStoryPublishToTime = function() {
   if (this.site.preferences.getProperty("modTagActive") != "true")
      return;
   var tagCollection = this.tags;
   var tagCollectionSize = tagCollection.size();
   tagCollection.prefetchChildren(0, tagCollectionSize);
   for (var i = 0; i < tagCollectionSize; i++) {
      tagCollection.get(i).status = 1;
   }
   this.site.tags.invalidate();
   return;
}

app.modules.modTagging.onStoryToggleOnline = function(param) {
   if (this.site.preferences.getProperty("modTagActive") != "true")
      return;
   var newStatus = (param.newStatus == "online") ? 1 : 0;
   var tagCollection = this.tags;
   var tagCollectionSize = tagCollection.size();
   tagCollection.prefetchChildren(0, tagCollectionSize);
   for (var i = 0; i < tagCollectionSize; i++) {
      tagCollection.get(i).status = newStatus;
   }
   this.site.tags.invalidate();
   return;
}
