
app.modules.modAdminMenu = {
   version: "1.0",
   state: "beta",
   url: "",
   author: "Wolfgang Kamir",
   authorEmail: "wolfgang@knallgrau.at",
   authorUrl: "http://www.knallgrau.at",
   authorOrganisation: "knallgrau.at",
   copyright: "(c) 2004 knallgrau.at",
   license: "twoday",
   licenseUrl: "http://www.twoday.org",
   isSidebar: true,
   type: MODULE_TYPE_NAVIGATION,
   hasSiteSettings: true,
   skins: ["Site.adminnavigation"]
};

app.modules.modAdminMenu.renderSidebarItem = function() {
   if (res.meta.memberlevel >= ADMIN) {
      var p = {};
      p.body = this.renderSkinAsString("adminnavigation");
      this.renderSidebarModuleItem("modAdminMenu", p);
   }
   return;
}
