
app.modules.modPopupLinks = {
   version: "1.0",
   state: "beta",
   url: "http://www.twoday.org",
   author: "Barbara Ondrisek",
   authorEmail: "",
   authorUrl: "http://www.knallgrau.at",
   authorOrganisation: "knallgrau.at",
   copyright: "(c) 2006 knallgrau.at",
   license: "twoday",
   licenseUrl: "http://www.twoday.org",
   isSidebar: true,
   type: MODULE_TYPE_NAVIGATION,
   hasSiteSettings: true,
   skins: ["Site.modPopup"]
};


app.modules.modPopupLinks.renderSidebarItem = function() {
   var p = {};
   p.editLink = true;
   p.body = this.renderSkinAsString("modPopupLinks");
   this.renderSidebarModuleItem("modPopupLinks", p);
};

app.modules.modPopupLinks.onAfterRenderSidebarItem = function() {
   this.renderSkin("modPopupLinksCall");
}


app.modules.modPopupLinks.renderPreferences = function() {
   Module.prototype.renderSetupSpacerLine();
   var param = {
      label: getMessage("modPopupLinks.preferences.default.title"),
      field: this.renderSkinAsString("modPopupLinksSettings")
   };
   Module.prototype.renderSetupLine(param);
};


app.modules.modPopupLinks.evalModulePreferences = function() {
   this.preferences.setProperty("modPopupLinksDefault", req.data.modPopupLinksDefault ? true : false);
};
