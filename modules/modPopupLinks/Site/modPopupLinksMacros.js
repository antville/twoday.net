
function modPopupLinksDefault_macro(param) {
   param.name = "modPopupLinksDefault";
   param.value = this.preferences.getProperty("modPopupLinksDefault");
   param.selectedValue = true;

   Html.checkBox(param);
}
