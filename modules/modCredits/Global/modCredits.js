
app.modules.modCredits = {
   version: "1.0",
   state: "beta",
   url: "",
   author: "Wolfgang Kamir",
   authorEmail: "wolfgang@knallgrau.at",
   authorUrl: "http://www.knallgrau.at",
   authorOrganisation: "knallgrau.at",
   copyright: "(c) 2004 knallgrau.at",
   license: "twoday",
   licenseUrl: "http://www.twoday.org/license",
   isSidebar: true,
   type: MODULE_TYPE_INFO,
   hasSiteSettings: true,
   skins: ["Site.credits"]
};


app.modules.modCredits.renderSidebarItem = function() {
   var p = {};
   p.body = this.renderSkinAsString("credits");
   this.renderSidebarModuleItem("modCredits", p);
   return;
}
