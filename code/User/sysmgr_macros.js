
/**
 * macro counts
 */
function sysmgr_count_macro(param) {
   if (!param || !param.what)
      return;
   // this macro is allowed just for sysadmins
   if (!session.user.sysadmin)
      return;
   switch (param.what) {
      case "stories" :
         return this.stories.size();
      case "comments" :
         return this.comments.size();
      case "images" :
         return this.images.size();
      case "files" :
         return this.files.size();
      case "memberships" :
         return this.memberships.size();
      case "subscriptions" :
         return this.subscriptions.size();
      case "sites" :
         return this.sites.size();
   }
   return;
}


/**
 * function renders the statusflags for this user
 */
function sysmgr_statusflags_macro(param) {
   // this macro is allowed just for sysadmins
   if (!session.user.sysadmin)
      return;
   if (!this.emailIsConfirmed)
      res.write("<span class=\"flagred\" nowrap>" + this.getActivationHash() + "</span>");
   if (this.trusted)
      res.write("<span class=\"flagDark\" style=\"background-color:#009900;\">TRUSTED</span>");
   if (this.sysadmin)
      res.write("<span class=\"flagDark\" style=\"background-color:#006600;\">SYSADMIN</span>");
   if (this.blocked)
      res.write("<span class=\"flagDark\" style=\"background-color:#000000;\">BLOCKED BY " + this.blockuser.name + "</span>");
   return;
}


/**
 * function renders an edit-link
 */
function sysmgr_editlink_macro(param) {
   // this macro is allowed just for sysadmins
   if (!session.user.sysadmin || req.data.item == this._id || session.user == this)
      return;
   param.linkto = "users";
   param.urlparam = "item=" + this._id + "&action=edit";
   if (req.data.page)
      param.urlparam += "&page=" + req.data.page;
   param.anchor = this._id;
   Html.openTag("a", root.manage.createLinkParam(param));
   res.write(param.text ? param.text : getMessage("generic.edit"));
   Html.closeTag("a");
   return;
}


/**
 * function renders an link to login as user ( for admins)
 */
function sysmgr_loginas_macro(param) {
   // this macro is allowed just for sysadmins
   if (!session.user.sysadmin || req.data.item == this._id || session.user == this || this.sysadmin)
      return;
   param.linkto = "loginAs";
   param.urlparam = "item=" + this._id;
   Html.openTag("a", root.manage.createLinkParam(param));
   res.write(param.text ? param.text : getMessage("admin.user.loginAs"));
   Html.closeTag("a");
   return;
}


/**
 * macro renders the username as plain text
 */
function sysmgr_username_macro(param) {
   res.write(this.name);
   return;
}


/**
 * macro renders the timestamp of registration
 */
function sysmgr_registered_macro(param) {
   // this macro is allowed just for sysadmins
   if (!session.user.sysadmin)
      return;
   fmt=param.format ? param.format : "dd.MM.yyyy HH:mm";
   res.write(this.registered.format(fmt));
   return;
}


/**
 * macro renders the timestamp of last visit
 */
function sysmgr_lastvisit_macro(param) {
   // this macro is allowed just for sysadmins
   if (!session.user.sysadmin)
      return;
   if (this.lastvisit) {
      fmt=param.format ? param.format : "dd.MM.yyyy HH:mm";
      res.write(this.lastvisit.format(fmt));
   }
   return;
}


/**
 * macro renders the trust-state of this user
 */
function sysmgr_trusted_macro(param) {
   // this macro is allowed just for sysadmins
   if (!session.user.sysadmin)
      return;
   if (param.as == "editor") {
      var options = [getMessage("generic.no"), getMessage("generic.yes")];
      Html.dropDown({name: "trusted"}, options, this.trusted);
   } else
      res.write(this.trusted ? getMessage("generic.yes") : getMessage("generic.no"));
   return;
}


/**
 * macro renders the block-state of this user
 */
function sysmgr_blocked_macro(param) {
   // this macro is allowed just for sysadmins
   if (!session.user.sysadmin)
      return;
   if (param.as == "editor") {
      var options = [getMessage("generic.no"), getMessage("generic.yes")];
      Html.dropDown({name: "blocked"}, options, this.blocked);
   } else
      res.write(this.blocked ? getMessage("generic.yes") : getMessage("generic.no"));
   return;
}


/**
 * macro renders the sysadmin-state of this user
 */
function sysmgr_sysadmin_macro(param) {
   // this macro is allowed just for sysadmins
   if (!session.user.sysadmin)
      return;
   if (param.as == "editor") {
      var options = [getMessage("generic.no"), getMessage("generic.yes")];
      Html.dropDown({name: "sysadmin"}, options, this.sysadmin);
   } else
      res.write(this.sysadmin ? getMessage("generic.yes") : getMessage("generic.no"));
   return;
}


/**
 * macro renders links to last items of this user
 */
function sysmgr_lastitems_macro(param) {
   var max = param.max ? parseInt(param.max, 10) : 12;
   var coll = this[param.what];
   if (param.showall)
      max = coll.size();
   var cnt = Math.min(coll.count(), max);
   for (var i=0; i<cnt; i++) {
      Html.link({href: coll.get(i).href()}, "#" + (coll.count()-i) + " ");
   }
   return;
}


/**
 * function renders a delete-link
 */
function sysmgr_deletelink_macro(param) {
   // this macro is allowed just for sysadmins
   if (!session.user.sysadmin || req.data.item == this._id || session.user == this)
      return;
   param.linkto = "users";
   param.urlparam = "item=" + this._id + "&action=remove";
   if (req.data.page)
      param.urlparam += "&page=" + req.data.page;
   param.anchor = this._id;
   Html.openTag("a", root.manage.createLinkParam(param));
   res.write(param.text ? param.text : "delete");
   Html.closeTag("a");
   return;
}
