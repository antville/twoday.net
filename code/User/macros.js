
/**
 * macro rendering username
 */
function name_macro(param) {
   if (this.url && param.as != "text")
      Html.link({href: this.url}, this.getDisplayName());
   else
      res.write(this.getDisplayName());
   return;
}


/**
 * macro rendering password
 */
function password_macro(param) {
   return "";
}


/**
 * macro rendering password-changing block (i.e. skin editPassword)
 */
function editPassword_macro(param) {
   if (session.user.authType != "local")
      return;
   this.renderSkin("editPassword");
   return;
}


/**
 * macro rendering URL
 */
function url_macro(param) {
   if (param.as == "editor")
      Html.input(this.createInputParam("url", param));
   else if (this.url)
      res.write(this.url);
   return;
}


/**
 * macro rendering email
 */
function email_macro(param) {
   if (session.user && (session.user == this || session.user.sysadmin)) { 
      if (param.as == "editor")
         Html.input(this.createInputParam("email", param));
      else
         res.write(this.email);
   }
   return;
}


/**
 * macro rendering checkbox for publishemail
 */
function publishemail_macro(param) {
   if (param.as == "editor") {
      var inputParam = this.createCheckBoxParam("publishemail", param);
      if (req.data.save && !req.data.publishemail)
         delete inputParam.checked;
      Html.checkBox(inputParam);
   } else
      res.write(this.publishemail ? getMessage("generic.yes") : getMessage("generic.no"));
   return;
}


/**
 * macro renders the sites the user is a member of or has subscribed to
 * in order of their last update-timestamp
 */
function sitelist_macro(param) {
   var memberships = session.user.list();
   memberships.sort(new Function("a", "b", "return b.site.lastupdate - a.site.lastupdate"));
   for (var i in memberships) {
      var site = memberships[i].site;
      if (!site)
         continue;
      site.renderSkin("preview");
   }
   return;
}


/**
 * macro renders the sites the user is a member of or has subscribed to
 * in order of their last update-timestamp
 * @param max Integer, maximum number of items to be rendered
 */
function simpleSubscriptionsList_macro(param) {
   var sitelist = this.list();
   // FIXME sorting Problem.site may be null
   sitelist.sort(new Function("a", "b", "if (a.site == null) return b; if (b.site == null) return a;return b.site.lastupdate - a.site.lastupdate;"));
   var list = [];
   var max = (param.max && parseInt(param.max, 10) < sitelist.length) ? parseInt(param.max, 10) : sitelist.length;
   for (var i=0; i < max; i++) list[i] = sitelist[i].site;
   res.write(renderList(list, "simplesubscriptionlistitem"));
}


/**
 * macro renders the sites the user is a member of or has subscribed to
 * in order of their last update-timestamp
 */
function mySites_macro(param) {
   if (this.sites.size() == 0) {
      // create yout own weblog
      var items = [
         {
            href: path.membermgr.href("newsite"),
            action: "MemberMgr/newsite",
            text: getMessage("Root.newSite.header")
         }
      ];
      Mgr.prototype.renderTasks(items, param);
   } else {
      var sitelist = this.sites.list();
      return renderList(sitelist, "mgrsimplelistitem");
   }
}


/**
 * macro renders a list of available locales as dropdown
 */
function localechooser_macro(param) {
   renderLocaleChooser(this.getLocale());
   return;
}


/**
 * macro rendering the locale
 */
function locale_macro(param) {
   return this.getLocale().getDisplayName();
}


/**
 * macro that generates a link for account activation
 */
function activationlink_macro(param) {
   if (!this.emailIsConfirmed)
      Html.link({href: this.getActivationLink()}, param.text ? param.text : this.getActivationHash());
   return;
}


/**
 * Renders age of user in days.
 *
 * @doclevel public
 */
function age_macro(param) {
   var age = Math.floor((new Date() - this.registered) / Date.ONEDAY);
   res.write(age);
   return;
}
