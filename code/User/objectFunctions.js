
/**
 * Constructor function for User objects.
 *
 * @doclevel public
 */
function constructor() {
   this.type = "SORUA_LOCAL";
   this.authType = "local";
   this.blocked = 0;
}


/**
 * Returns displayName for this user
 */
function getDisplayName() {
   return this.name;
}


/**
 * Perform login for the current session with this user
 */
function doLogin() {
   // check if system is currently closed for normal user type
   // see /environments/../app.properties: sysAdminsOnly = true|false
   var sysAdminsOnly = getProperty('sysAdminsOnly').toLowerCase() === 'true';
   if (sysAdminsOnly && !this.sysadmin) throw new Exception('sysAdminsOnly');

   if (this.blocked) throw new Exception('accountBlocked');

   // check if email is confirmed
   // check whether user is allowed to login
   if (this.type == "USER_LOCAL" && this.emailIsConfirmed != 1 &&
         (new Date()).valueOf() - (this.emailChanged || this.registered).valueOf() > Date.ONEHOUR) {
      // prevent auto login
      addHeaderCookie('avType', '');
      addHeaderCookie('avUsr', '');
      addHeaderCookie('avPw', '');
      //put username + passwd into the session
      session.data.activationUserID  = this._id;
      session.data.activationUserPassword = this.digest;
      if (req.action != "activateExpiredEmail") {
         res.redirect(root.members.href("activateExpiredEmail"));
      } else {
         throw new Exception("accountNotActivatedLoginFailed");
      }
   }
   session.login(this);
   session.data.locale = null;
   this.lastVisit = new Date();
   this.failedLoginCount = 0;
   addHeaderCookie('avLoggedIn', 1, 0);
   return;
}


/**
 * sets a permanent cookie used for auto login
 */
function setAutoLoginCookie() {
   // user allowed us to set permanent cookies for auto-login
   addHeaderCookie('avType', this.authType);
   addHeaderCookie('avUsr', this.name);
   var hash = (this._id + this.registered.valueOf() + req.data.http_remotehost).md5();
   if (req.data.avPw && !req.data.avPw.contains(hash)) {
      var oldHash = req.data.avPw.contains(";") ? req.data.avPw.split(";")[1] : req.data.avPw;
      hash = oldHash + ";" + hash;
      addHeaderCookie('avPw', hash);
   } else if (!req.data.avPw) {
      addHeaderCookie('avPw', hash);
   }
   return;
}


/**
 * function deletes all assets of a User (recursive!)
 */
function deleteAll() {
   for (var i=this.images.size(); i>0; i--) {
      var s = this.images.get(i-1).site;
      if (s && s.images) {
         s.images.deleteImage(this.images.get(i-1));
      }
   }
   for (var i=this.files.size(); i>0; i--) {
      if (!this.files.get(i-1).site) continue;
      this.files.get(i-1).site.files.deleteFile(this.files.get(i-1));
   }
   for (var i=this.stories.size(); i>0; i--) {
      if (!this.stories.get(i-1).site) continue;
      this.stories.get(i-1).site.stories.deleteStory(this.stories.get(i-1));
   }
   for (var i=this.comments.size(); i>0; i--) {
      if (!this.comments.get(i-1).story) continue;
      this.comments.get(i-1).story.deleteComment(this.comments.get(i-1));
   }
   for (var i=this.memberships.size(); i>0; i--) {
      var s = this.memberships.get(i-1).site;
      this.memberships.get(i-1).remove();
      if (s && s.members.admins.count() == 0) {
         root.blockSite(s);
      }
   }
   for (var i=this.subscriptions.size(); i>0; i--) {
      this.subscriptions.get(i-1).remove();
   }
   for (var i=this.sites.size(); i>0; i--) {
      var s = this.sites.get(i-1);
      if (s && s.members.admins.count() == 0) {
         root.blockSite(s);
      }
   }
   return true;
}


/**
 * function generates an activation code with 6 letters
 */
function getActivationHash() {
   return Packages.helma.util.MD5Encoder.encode(this._id + this.email + (this.registered.valueOf() + "").substring(0,8)).substring(0,6);
}


/**
 * function generates a link for account activation
 */
function getActivationLink() {
   return root.members.href("register") + "?act=t&uid=" + this._id + "&id=" + this.getActivationHash();
}


/**
 * function performs necessary actions, if a user email is changed
 * @param email String, new Email-address of the User
 */
function changeEmail(email) {
   if (this.email == email) return;
   evalEmail(email);
   if (root.usersByEmail.get(email)) {
      throw new Exception("emailExisting");
   }
   // log changing of email address
   try { var cusr = session.user; }
   catch (err) { var cusr = null; }
   root.manage.syslogs.add(new SysLog("user", this.name, "changed email " + this.email + " to " + email, cusr));
   // store semi-colon separated list of old emails if they have been confirmed
   if (this.emailIsConfirmed) {
      var str = this.emailHistory ? this.emailHistory + ";" : "";
      str += this.email;
      this.emailHistory = str;
   }
   for (var i=0; i<this.sites.count(); i++)
      this.sites.get(i).email = email;
   this.email = email.trim();
   this.emailIsConfirmed = 0;
   this.emailChanged = new Date();
   this.sendRegistrationInfo();
   return;
}


/**
 * Returns preferred locale for user
 */
function getLocale() {
   var locale = this.cache.locale;
   if (locale)
       return locale;
   if (this.preferences.getProperty("language")) {
      if (this.preferences.getProperty("country"))
         locale = new java.util.Locale(this.preferences.getProperty("language"),
                                       this.preferences.getProperty("country"));
      else
         locale = new java.util.Locale(this.preferences.getProperty("language"));
   } else
      locale = root.getLocale();
   this.cache.locale = locale;
   return locale;
}


/**
 * Sets preferred locale for user
 *
 * @param locale String, ISO-639 language-code with optional ISO-3166 country-code; e.g. de_AT
 */
function setLocale(locale) {
   var prefs = this.preferences.getAll();
   // store selected locale
   if (locale) {
      var loc = locale.split("_");
      prefs.language = loc[0];
      prefs.country = loc.length == 2 ? loc[1] : null;
      this.cache.locale = null;
   }
   this.preferences.setAll(prefs);
   return;
}


/**
 * function sends a mail with the registration infos to the user
 */
function sendRegistrationInfo() {
   if (!root.preferences.getProperty("sys_email")) return;
   var sp = {name: this.name,
             activationid: this.getActivationHash(),
             activationlink: this.getActivationLink()};
   sendMail(root.preferences.getProperty("sys_email"),
            this.email,
            getMessage("mail.registration", root.getTitle()),
            root.members.renderSkinAsString("mailregconfirm", sp)
           );
   return;
}


function getDigest(password) {
   // The date columns were created without microseconds
   // https://mariadb.com/kb/en/library/microseconds-in-mariadb/
   var salt = Math.floor(this.registered / 1000);
   var digest = new java.security.MessageDigest.getInstance('SHA-256');
   var hash = digest.digest(new java.lang.String(password + salt).getBytes('UTF-8'));
   var numeric = new java.math.BigInteger(1, hash);
   var hex = numeric.toString(16);

   // Left-pad with zeros
   while (hex.length < 64) hex = '0' + hex;

   return hex;
}


function getResetLink() {
   var Hex = Packages.org.apache.commons.codec.binary.Hex;
   var user = this;
   res.debug(user);

   var hash = user.getDigest(user.digest).substr(-8);
   var salt = String(user.registered - 0).substring(0, 8)
   var password = java.lang.String(user.digest).toCharArray();

   var expiry = new Date() - 0 + 60 * 60 * 1000; // 1 hour
   var payload = new java.lang.String([hash, expiry].join());

   var cipher = getCipher(password, salt, 'encrypt');
   var cleartext = payload.getBytes();
   var ciphertext = cipher.doFinal(cleartext);

   var bytes = Hex.encodeHex(ciphertext);
   var key = new java.lang.String(bytes);

   return root.members.href('resetpwd') +
          '?id=' + user._id +
          '&key=' + encodeURIComponent(key);
}


/**
 * Just to avoid the INFO Messages
 */
function onLogout() {
}
