
/**
 * Performs a global right check using the definitions GLOBAL_MAY_XYZ and USER_ABC
 */
function may(right) {
   if (this.sysadmin) return true;
   return eval("(" + this.type + " & GLOBAL_MAY_" + right + ") == GLOBAL_MAY_" + right);
}
