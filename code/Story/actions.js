
/**
 * main action
 */
function main_action() {
   res.data.title = this.site.title;
   var storytitle = this.getRenderedContentPart("title");
   if (storytitle)
      res.data.title += ": " + stripTags(storytitle);
   res.data.body = this.renderSkinAsString("main");
   this.site.renderPage();
   // increment read-counter
   this.incrementReadCounter();
   logAccess();
   return;
}


/**
 * edit action
 */
function edit_action() {
   knallgrau.Event.notifyObservers(this, "beforeEditAction", req.data);
   // restore any rescued text
   if (session.data.rescuedText)
      restoreRescuedText();

   if (req.data.set) {
      this.toggleOnline(req.data.set);
      if (req.data.http_referer)
         res.redirect(req.data.http_referer);
      res.redirect(this.site.stories.href());
   } else if (req.data.cancel) {
      res.redirect(this.online ? this.href() : this.site.stories.href());
   } else if (req.data.save || req.data.publish) {
      try {
         checkSecretKey();
         var result = this.evalStory(req.data, session.user);
         res.message = result.toString();
         res.redirect(result.url);
      } catch (err) {
         res.message = err.toString();
      }
   }

   res.data.action = this.href(req.action);
   res.data.title = getMessage("Story.edit.title");
   if (this.title)
      res.data.title += ": " + encode(this.title);
   res.data.body = this.renderSkinAsString("edit");
   this.site.renderPage();
   return;
}


/**
 * delete action
 */
function delete_action() {
   if (req.data.cancel)
      res.redirect(this.site.stories.href());
   else if (req.data.remove) {
      try {
         checkSecretKey();
         res.message = this.site.stories.deleteStory(this);
         res.redirect(this.site.stories.href());
      } catch (err) {
         res.message = err.toString();
      }
   }

   res.data.action = this.href(req.action);
   res.data.title = getMessage("Story.delete.title");
   if (this.title)
      res.data.title += ": " + encode(this.title);

   if (this.title)
      var skinParam = {
         description: getMessage("Story.deleteDescription"),
         detail: this.title
      };
   else
      var skinParam = {description: getMessage("Story.deleteDescriptionNoTitle")};
   res.data.body = this.renderSkinAsString("delete", skinParam);
   this.site.renderPage();
   return;
}


/**
 * comment action
 */
function comment_action() {
   // restore any rescued text
   if (session.data.rescuedText)
      restoreRescuedText();

   if (req.data.cancel)
      res.redirect(this.href());
   else if (req.data.save) {
      try {
         checkSecretKey();
         var result = this.evalComment(req.data, session.user);
         res.message = result.toString();
         res.redirect(this.href() + "#" + result.id);
      } catch (err) {
         res.message = err.toString();
         app.log("COMMENT BUG? " + uneval(err));
      }
   }

   res.data.action = this.href(req.action);
   res.data.title = this.site.title;
   if (this.title)
      res.data.title += " - " + encode(this.title);
   res.data.body = this.renderSkinAsString("comment");
   this.site.renderPage();
   // increment read-counter
   this.incrementReadCounter();
   return;
}


/**
 * flag action (GET)
 * Renders the Story.flag skin to initiate a flag or unflag process for a story
 */
function flag_action_get() {
   // restrict access to sysadmins only
   if (!session.user || !session.user.sysadmin) {
      res.message = getMessage('deny.general');
      res.redirect(this.site ? this.site.href() : root.href());
   }

   // read preferences_xml of story
   var prefs = this.preferences.getAll();
   if (!prefs.hasOwnProperty('isflagged')) prefs.isflagged = false;

   // textual description of current flagging status
   var flagstate = getMessage('Story.flag.status', {
      title: this.title || 'noTitle',
      status: prefs.isflagged 
         ? getMessage('Story.flag.isMarked', { reason: prefs.flagtype }) 
         : getMessage('Story.flag.notMarked')
   });

   // build reasons object
   var reasons = { Generic: getMessage('warn.flag.Generic') };
   for (var i=1; i<FLAGTYPEREASONS.length; i++)
      reasons[FLAGTYPEREASONS[i]] = getMessage('warn.flag.' + FLAGTYPEREASONS[i], { generic: reasons.Generic });

   /**
    * reasons and prefs are param-passed to renderSkinAsString of the flag.skin to serve as pre-
    * defined object definitions as part of an Alpine script. To avoid syntax errors within the
    * Alpine x-data clause, the resulting stringify strings are here converted from double quote
    * strings to single quote.
    * The param object is stored in res.data to allow making other embedded macros (e.g. site.alpine)
    * use of the same param values.
   */
   res.data.action = this.href(req.action);
   res.data.param = {
      currentUser: session.user.name,
      currentReason: prefs.flagtype || '',
      currentAddition: prefs.flagaddition || '',
      reasons: JSON.stringify(reasons).replace(/"/g, "'"),
      prefs: JSON.stringify(prefs).replace(/"/g, "'"),
      state: flagstate
   };
   res.data.body = this.renderSkinAsString('flag', res.data.param);
   this.site.renderPage();
   return;
}


/**
 * flag action (POST)
 * processes Story.flag form submit events: flag/unflag/cancel
 */
function flag_action_post() {
   // quit on cancel
   if (req.data.cancel) res.redirect(this.href());

   // now must be req.data.flag or req.data.unflag
   try {
      // restrict access to sysadmins only
      if (!session.user) throw new DenyException('requireLogin');
      if (!session.user.sysadmin) throw new DenyException('general');
      checkSecretKey();
      // load story preferences_xml
      var prefs = this.preferences.getAll();
      if (req.data.flag) {
         // process flagging event
         prefs.isflagged = true;
         prefs.flagdate = formatTimestamp(new Date());
         prefs.flaguser = session.user.name;
         prefs.flagtype = req.data.flagtype;
         prefs.flagaddition = req.data.flagaddition.replace(/\'/g, '&#39;');
         delete(prefs.unflagdate);
         delete(prefs.unflaguser);
      } else if (req.data.unflag) {
         // process unflagging event
         prefs.isflagged = false;
         prefs.unflaggedtype = prefs.flagtype;
         delete(prefs.flagtype);
         delete(prefs.flagaddition);
         prefs.unflagdate = formatTimestamp(new Date());
         prefs.unflaguser = session.user.name;
      }
      // write updated preferences data
      this.preferences.setAll(prefs);
      // renew lastupdate property to trigger blogroll
      this.site.lastupdate = new Date();
      // now issue related mail to the post creator if so desired (flagusermail=true)
      var mailMessage = '';
      if (req.data.flagusermail) {
         var qualifier = prefs.isFlagged ? 'flag' : 'unflag';
         var mailTitle = 'mail.' + qualifier + 'Event';
         var mailBodyskin = 'mail' + qualifier + 'story';
         mailMessage = sendMail(
            root.preferences.getProperty('sys_email'), // from
            this.creator.email, // to
            getMessage(mailTitle, { siteName: this.site.getName() }),
            root.members.renderSkinAsString(mailBodyskin, {
               creatorname: this.creator.name,
               title: this.title,
               alias: this.site.alias + ' - ' + this.site.title,
               url: this.href()
            }));
      }
      // res.message = 'prefs: ' + JSON.stringify(prefs); // for debugging purposes
      res.message = getMessage('confirm.update') + ' ' + mailMessage;
   } catch (err) {
      res.message = err.toString();
   } finally {
      res.redirect(this.href());
   }
}
