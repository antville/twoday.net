
/**
 * Permission checks (called by hopobject.onRequest())
 *
 * @param action  String, name of action
 * @param usr     User, who wants to access an action
 * @param level   Int, Membership level
 * @return Object if permission was denied <br />
 *   .message  message, if something went wrong <br />
 *   .url      recommanded redirect URL
 * @doclevel public
 */
function checkAccess(action, usr, level) {
   if (usr && usr.sysadmin) return;
   var url = this.site.href();
   var site = this.site;
   try {
      if (site && !site.online) {
         checkIfLoggedIn(this.href(action));
         site.checkView(usr, level);
      }
      switch (action) {
         case "main" :
            this.checkView(usr, level);
            break;
         case "edit" :
            if (!usr && req.data.save)
               rescueText(req.data);
            checkIfLoggedIn(this.href(req.action));
            this.checkEdit(usr, level);
            break;
         case "delete" :
            checkIfLoggedIn(this.href(action));
            this.checkDelete(usr, level);
            break;
         case "comment" :
            if (!usr && req.data.save)
               rescueText(req.data);
            /**if (this.discussions && this.site.preferences.getProperty("discussionsby") != "anonymous")*/
            if (this.discussions)
               checkIfLoggedIn(this.href(req.action));
            url = this.href();
            this.checkComment(usr, level);
            break;
         default :
            url = this.applyAllModuleMethods("checkStoryAccess", [action, usr, level, url]);
            break;
      }
   } catch (deny) {
      return {message: deny.toString(), url: (url ? url : (site ? site.href() : root.href()))};
   }
   return;
}


/**
 * check if user is allowed to post a comment to this story
 * @param Obj Userobject
 * @param Int Permission-Level
 * @return String Reason for denial (or null if allowed)
 */
function checkComment(usr, level) {
   if (!this.site.online && level == null)
      throw new DenyException("siteView");
   var discussionsby = this.site.preferences.getProperty("discussionsby");
   if (discussionsby == "nobody")
      throw new DenyException("siteNoDiscussion");
   if (usr && usr.may("COMMENT") == false)
      throw new DenyException("globalMayComment");
   if (root.isIpAddressBlocked(req.data.http_remotehost)) {
      throw new DenyException("globalMayComment");
   }
   if (!this.discussions) {
      throw new DenyException("storyNoDiscussion");
   }
   if (this.site.isUserBlocked(usr, "discussions") ||
       (discussionsby != "anonymous" && !usr) ||
       (discussionsby == "local" && usr.authType != "local") ||
       (discussionsby == "sorua" && usr.authType != "sorua" && usr.authType != "local") ||
       (discussionsby == "contributors" && level < CONTRIBUTOR)) {
      throw new DenyException("globalMayComment");
   }
   return;
}


/**
 * check if user is allowed to delete a story
 * @param Obj Userobject
 * @param Int Permission-Level
 * @return String Reason for denial (or null if allowed)
 */
function checkDelete(usr, level) {
   if (this.creator != usr && (level & MEMBER_MAY_DELETE_ANYSTORY) == 0)
      throw new DenyException("storyDelete");
   return;
}


/**
 * check if user is allowed to edit a story
 * @param Obj Userobject
 * @param Int Permission-Level
 * @return String Reason for denial (or null if allowed)
 */
function checkEdit(usr, level) {
   if (this.creator != usr) {
      if (level == null)
         throw new DenyException("storyEdit");
      else if (this.editableby == EDITABLEBY_ADMINS && (level & MEMBER_MAY_EDIT_ANYSTORY) == 0)
         throw new DenyException("storyEdit");
      else if (this.editableby == EDITABLEBY_CONTRIBUTORS && (level & MEMBER_MAY_ADD_STORY) == 0)
         throw new DenyException("storyEdit");
   }
   return;
}


/**
 * check if user is allowed to view story
 * @param Obj Userobject
 * @param Int Permission-Level
 * @return String Reason for denial (or null if allowed)
 */
function checkView(usr, level) {
   this.site.checkView(usr, level);
   if (!this.online && this.creator != usr) {
      if (this.editableby == EDITABLEBY_ADMINS && (level & MEMBER_MAY_EDIT_ANYSTORY) == 0)
         throw new DenyException("storyView");
      else if (this.editableby == EDITABLEBY_CONTRIBUTORS && (level & MEMBER_MAY_ADD_STORY) == 0)
         throw new DenyException("storyView");
   }
   return;
}


/**
 * function explicitly allows some macros for use in the text of a story
 * @param Obj Skin-object to allow macros for
 */
function allowTextMacros(s) {
   s.allowMacro("image");
   s.allowMacro("this.image");
   s.allowMacro("layout.image");
   s.allowMacro("site.image");
   s.allowMacro("story.image");
   s.allowMacro("thumbnail");
   s.allowMacro("this.thumbnail");
   s.allowMacro("site.thumbnail");
   s.allowMacro("story.thumbnail");
   s.allowMacro("link");
   s.allowMacro("this.link");
   s.allowMacro("site.link");
   s.allowMacro("story.link");
   s.allowMacro("file");
   s.allowMacro("poll");
   s.allowMacro("logo");
   s.allowMacro("storylist");
   s.allowMacro("fakemail");
   s.allowMacro("this.topic");
   s.allowMacro("story.topic");
   s.allowMacro("imageoftheday");
   s.allowMacro("gallery");
   s.allowMacro("spacer");
   s.allowMacro("this.url");
   s.allowMacro("this.href");
   s.allowMacro("story.href");
   s.allowMacro("story.url");
   s.allowMacro("site.href");
   s.allowMacro("site.url");
   s.allowMacro("svgIcon");

   if (this.site && this.site.trusted) {
      s.allowMacro("this.list");
      s.allowMacro("site.list");
      s.allowMacro("topic.list");
      s.allowMacro("sitelist");
      s.allowMacro("constant");
      s.allowMacro("site.tdySigMonthlistWithStories");
   }

   // allow module text macros
   for (var i in app.modules) {
      if (app.modules[i].allowTextMacros)
         app.modules[i].allowTextMacros(s);
   }
   return;
}
