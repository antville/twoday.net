
/**
 * Macro for rendering a part of the story content.
 * FIXME: further description
 *
 * @param part       Identifier of the content part to be rendered. Most frequently this is "title" or "text", could also be any other value.
 * @param as         Defines what is rendered. Values are either "editor", "image", "link", or "plaintext". use "plaintext" to strip tags
 * @param cols       Defines the inputelement to be rendered
 * @param rows       Defines the inputelement to be rendered
 * @param clipping   Chars to be used for clipping if null "..."
 * @param limit      Defines how much content is rendered, if null the whole content is displayed
 * @skin Comment.main, Comment.listme FIXME
 * @doclevel public
 */
 function content_macro(param) {
   switch (param.as) {
      case "editor" :
         var part = param.part;
         var inputParam = this.content.createInputParam(param.part, param);
         delete inputParam.part;
         if ((part == "title" || part == "text") && inputParam.value == null)
            inputParam.value = this[part];
         if (param.cols || param.rows)
            Html.textArea(inputParam);
         else
            Html.input(inputParam);
         this.applyModuleMethods("onAfterContentMacroEditor", [this, param], false);
         break;

      case "image" :
         if (param.part == "title" || param.part == "text")
            var part = this[param.part];
         else
            var part = this.content.getProperty(param.part);
         if (part && this.site.images.get(part)) {
            delete param.part;
            renderImage(this.site.images.get(part), param);
         }
         break;

      default :
         if (param.clipping == null)
            param.clipping = "...";
         var part = this.getRenderedContentPart(param.part, param.as);
         if (part && param.as == "plaintext") part = part.stripTags();
         if (!part && param.fallback)
            part = this.getRenderedContentPart(param.fallback, param.as);
         if (param.as == "link") {
            if (this._prototype != "Comment") {
               try {
                  Html.openLink({href: this.href()});
               } catch(e) {
                  app.log("[content_macro] Comment href() Bug: " + res.handlers.site.alias);
                  // app.log(uneval(e));
               }

            } else {
               // FIXME: quickfix for comments without stories
               var story = this.story || path.Story;
               if (!story) {
                  app.log("[Story.content_macro] coudn't find story to comment: " + this._id);
                  return;
               }
               try {
                  Html.openLink({href: story.href() + "#" + this._id});
               } catch(e) {
                  app.log("[content_macro] Story href() or this._id Bug: " + res.handlers.site.alias);
                  // app.log(uneval(e));
               }
            }
            part = part ? part.stripTags() : param.clipping;
         }
         if (param.encode == "xml") {
            // Any ampersand from the format-call in getRenderedContentPart() will get encoded again by encodeXml.
            // By replacing decoding these ampersands, we prevent any double encoding
            part = encodeXml(part);
            part = part.replace("&amp;", "&", "g");
            var lookup = ["&euro;", "", "&#8218;", "&#402;", "&#8222;", "&#8230;", "&#8224;", "&#8225;", "&#710;", "&#8240;", "&#352;", "&#8249;", "&#338;", "", "&#381;", "", "", "&#8216;", "&#8217;", "&#8220;", "&#8221;", "&#8226;", "&#8211;", "&#8212;", "&#732;", "&#8482;", "&#353;", "&#8250;", "&#339;", "", "&#382;", "&#376;", "&nbsp;", "&iexcl;", "&cent;", "&pound;", "&curren;", "&yen;", "&brvbar;", "&sect;", "&uml;", "&copy;", "&ordf;", "&laquo;", "&not;", "&shy;", "&reg;", "&macr;", "&deg;", "&plusmn;", "&sup2;", "&sup3;", "&acute;", "&micro;", "&para;", "&middot;", "&cedil;", "&sup1;", "&ordm;", "&raquo;", "&frac14;", "&frac12;", "&frac34;", "&iquest;", "&Agrave;", "&Aacute;", "&Acirc;", "&Atilde;", "&Auml;", "&Aring;", "&AElig;", "&Ccedil;", "&Egrave;", "&Eacute;", "&Ecirc;", "&Euml;", "&Igrave;", "&Iacute;", "&Icirc;", "&Iuml;", "&ETH;", "&Ntilde;", "&Ograve;", "&Oacute;", "&Ocirc;", "&Otilde;", "&Ouml;", "&times;", "&Oslash;", "&Ugrave;", "&Uacute;", "&Ucirc;", "&Uuml;", "&Yacute;", "&THORN;", "&szlig;", "&agrave;", "&aacute;", "&acirc;", "&atilde;", "&auml;", "&aring;", "&aelig;", "&ccedil;", "&egrave;", "&eacute;", "&ecirc;", "&euml;", "&igrave;", "&iacute;", "&icirc;", "&iuml;", "&eth;", "&ntilde;", "&ograve;", "&oacute;", "&ocirc;", "&otilde;", "&ouml;", "&divide;", "&oslash;", "&ugrave;", "&uacute;", "&ucirc;", "&uuml;", "&yacute;", "&thorn;", "&yuml;"];
            for (var i=0; i<lookup.length; i++) {
               if (!lookup[i]) continue;
               part = part.replace(lookup[i], String.fromCharCode(i+128), "g");
            }
         }
         if (!param.limit) {
            var epilogue = param.part === 'text' ? this.renderWarningFlag() : '';
            res.write(part + epilogue);
         } else if (param.softwrap == "false")
            res.write(part.clip(param.limit, param.clipping));
           else
            res.write(part.clip(param.limit, param.clipping).softwrap(param.softwrap || "25"));
         if (param.as == "link")
            Html.closeLink();
   }
   return;
}


/**
 * Renders the onlinestatus.
 *
 * @param yes  Text to be rendered if the story is online. default = "online"
 * @param no   Text to be rendered if the story is offline. default = "offline"
 * @doclevel admin
 */
function online_macro(param) {
   if (this.publishToTimeState > 0) {
      res.write(getMessage("Story.notPublished") + " - ");
      if (this.publishToTimeState == 1)
         res.write(param.no ? param.no : getMessage("Story.offline"));
      else
         res.write(param.yes ? param.yes : getMessage("Story.online"));
      return;
   }

   if (!this.online)
      res.write(param.no ? param.no : getMessage("Story.offline"));
   else
      res.write(param.yes ? param.yes : getMessage("Story.online"));
   return;
}


/**
 * Renders the location of the story
 * FIXME: further description
 *
 * @doclevel FIXME
 */
function location_macro(param) {
   switch (this.online) {
      case 1:
         Html.link({href: this.site.topics.get(this.topic).href()}, "topic");
         break;
      case 2:
         res.write("site");
         break;
   }
   return;
}


/**
 * Renders the createtime of story, either as editor,
 * plain text or as link to the frontpage of the day
 *
 * @param as Defines the role as wich the time is rendered, valid values are "editor" and "link".
 * @param value The createTime of the Story.
 * @doclevel public
 */
function createtime_macro(param) {
   if (param.as == "editor") {
      if (this.createtime)
         param.value = formatTimestamp(this.createtime, "yyyy-MM-dd HH:mm");
      else
         param.value = formatTimestamp(new Date(), "yyyy-MM-dd HH:mm");
      param.name = "createtime";
      Html.input(param);
   } else if (this.createtime) {
      var text = formatTimestamp(this.createtime, param.format);
      if (param.as == "link" && this.online == 2)
         Html.link({href: this.site.get(String(this.day)).href()}, text);
      else
         res.write(text);
   }
   return;
}

 /**
  * Renders the modifytime of story, either as editor or
  * plain text
  *
  * @param as Defines the role as wich the time is rendered, valid values are "editor" or empty.
  * @param value The modifytime of the Story.
  * @doclevel public
  */
function modifytime_macro(param) {
   if (param.as == "editor") {
      if (this.modifytime)
         param.value = formatTimestamp(this.modifytime, "yyyy-MM-dd HH:mm");
      else
         param.value = formatTimestamp(new Date(), "yyyy-MM-dd HH:mm");
      param.name = "modifytime";
      Html.input(param);
   } else if (this.modifytime) {
      var text = formatTimestamp(this.modifytime, param.format);
      res.write(text);
   }
   return;
}


/**
 * Macro for rendering the link to edit the story if the user is allowed it to be edited.
 *
 * @param image  DEPRECATED; Renders an site.image, if present, instead of the text
 * @param text   optional link text, if you don't want to use the default ("locale.generic.edit")
 * @doclevel public
 */
function editlink_macro(param) {
   if (session.user) {
      try {
         this.checkEdit(session.user, res.meta.memberlevel);
      } catch (deny) {
         return;
      }
      Html.openLink({href: this.href("edit")});
      if (param.image && this.site.images.get(param.image))
         renderImage(this.site.images.get(param.image), param);
      else
         res.write(param.text ? param.text : getMessage("generic.edit"));
      Html.closeLink();
   }
   return;
}

/**
 * Macro for rendering the link to flag the story, but only if the user is a sysadmin
 *
 * @param text optional link text, if you don't want to use the default ("generic.flag") text
 * @doclevel public
 */
 function flaglink_macro(param) {
   if (session.user && session.user.sysadmin) {
      Html.openLink({href: this.href('flag')});
      res.write(param.text ? param.text : getMessage('generic.flag'));
      Html.closeLink();
   }
   return;
}

/**
 * Macro for rendering the state of preferences_xml's property "isflagged" (true|false)
 * which reflects if a story is currently flagged as disputable;
 * currently being used in Story.mgrlistitem skin (stories/main overview screen)
 *
 * @param on optional string for state isflagged=true; writes "generic.yes" if missing
 * @param off optional string for state isflagged=false; writes "generic.no" if missing
 * @param omitwhen optional string; values "true" or "false" 
 *  - "true":  omits the entire output if isflagged equals true
 *  - "false": omits the entire output if isflagged equals false
 * @doclevel public
 */
 function isflagged_macro(param) {
   // no param, no work
   if (!param) return;
   // extract the flag from the preferences_xml blob
   var isflagged = this.preferences.getProperty('isflagged');
   // fancy XOR-ing isflagged with omitwhen param to suppress output if so desired
   if (param.omitwhen && (isflagged ^ (param.omitwhen.toLowerCase() !== 'true'))) return;
   // otherwise move on with the output
   res.write(isflagged 
      ? (param.on || getMessage('generic.yes'))
      : (param.off || getMessage('generic.no'))
   );
   return;
}

/**
 * Macro for rendering a link to delete the story if the user is creator of this story.
 *
 * @param image  DEPRECATED; Renders an site.image, if present, instead of the text
 * @param text   optional link text, if you don't want to use the default ("locale.generic.delete")
 * @doclevel public
 */
function deletelink_macro(param) {
   if (session.user) {
      try {
         this.checkDelete(session.user, res.meta.memberlevel);
      } catch (deny) {
         return;
      }
      Html.openLink({href: this.href("delete")});
      if (param.image && this.site.images.get(param.image))
         renderImage(this.site.images.get(param.image), param);
      else
         res.write(param.text ? param.text : getMessage("generic.delete"));
      Html.closeLink();
   }
   return;
}


function bookmark_macro(param) {
   this.renderSkin("bookmark");
}


/**
 * This macro renders a link to
 * toggle the online-status of this story
 *
 * @param mode The mode for the online status
 * @param image  DEPRECATED; Renders an site.image, if present, instead of the text
 * @param text   optional link text, if you don't want to use the default ("locale.Story.setOnline"|"locale.Story.setOffline")
 * @doclevel public
 */
function onlinelink_macro(param) {
   if (session.user) {
      try {
         this.checkEdit(session.user, res.meta.memberlevel);
      } catch (deny) {
         return;
      }
      if (this.online && param.mode != "toggle")
         return;
      delete param.mode;
      param.linkto = "edit";
      param.urlparam = "set=" + (this.online ? "offline" : "online");
      Html.openTag("a", this.createLinkParam(param));
      if (param.image && this.site.images.get(param.image))
         renderImage(this.site.images.get(param.image), param);
      else {
         // currently, only the "set online" text is customizable, since this macro
         // is by default only used in that context outside the story manager.
         if (this.online)
            res.write(getMessage("Story.setOffline"));
         else
            res.write(param.text ? param.text : getMessage("Story.setOnline"));
      }
      Html.closeTag("a");
   }
   return;
}


/**
 * Macro for rendering a link to this story.
 *
 * @param image  DEPRECATED; Renders an site.image, if present, instead of the text
 * @param text   optional link text, if you don't want to use the default ("view")
 * @doclevel public
 */
function viewlink_macro(param) {
   try {
      this.checkView(session.user, res.meta.memberlevel);
   } catch (deny) {
      return;
   }
   Html.openLink({href: this.href()});
   if (param.image && this.site.images.get(param.image))
      renderImage(this.site.images.get(param.image), param);
   else
      res.write(param.text ? param.text : "view");
   Html.closeLink();
   return;
}


/**
 * Renders a link to create a new comment.
 *
 * @param to Where to link to. If not set value is "comment"
 * @param text Text to be rendered if present
 * @doclevel public
 */
function commentlink_macro(param) {
   try {
      this.checkComment(session.user, res.meta.memberlevel);
   } catch (err) {
      if (!this.discussions || session.user || this.site.preferences.getProperty("discussionsby") == "nobody") return;
   }
   Html.link({href: this.href(param.to ? param.to : "comment")},
             param.text ? param.text : getMessage("Story.commentlink"));
   return;
}


/**
 * Macro used to render the number of comments of a story.
 *
 * @param linkto action to link to, if not present default is "main"
 * @param as
 * @param one text to use when one comment is found
 * @param more text to use when more than one comment was found
 * @param no text to use when no comment is found
 * @doclevel public
 */
function commentcounter_macro(param) {
   if (this.site.preferences.getProperty("discussionsby") == "nobody" ||
       !this.discussions ) {
      return;
   }
   var commentCnt = this.comments.count();
   if (!param.linkto)
      param.linkto = "main";
   param.anchor = "comments";
   var linkParam = this.createLinkParam(param);
   // delete the macro-specific attributes for valid markup output
   delete linkParam.as;
   delete linkParam.one;
   delete linkParam.more;
   delete linkParam.no;
   var linkflag = (param.as == "link" && param.as != "text" ||
                   !param.as && commentCnt > 0);
   if (linkflag)
      Html.openTag("a", linkParam);
   if (commentCnt == 0) {
      res.write(param.no || param.no == "" ?
                param.no : getMessage("Comment.no"));
   } else if (commentCnt == 1) {
      res.write(param.one ? param.one : getMessage("Comment.one"));
   } else {
      res.write(commentCnt + ((param.more || param.more == "") ?
                param.more : " " + getMessage("Comment.more")));
   }
   if (linkflag)
      Html.closeTag("a");
   return;
}


/**
 * Macro for rendering all comments of a story.
 *
 * @skin Story.reply, Story.toplevel
 * @doclevel public
 */
function comments_macro(param) {
   var s = this.story ? this.story : this;
   if (s.site.preferences.getProperty("discussionsby") == "nobody" ||
       !s.discussions ) {
      return;
   }
   this.comments.prefetchChildren();
   for (var i=0;i<this.size();i++) {
      var c = this.get(i);
      if (!c.online) continue;

      var linkParam = new Object();
      linkParam.name = c._id;
      Html.openTag("a", linkParam);
      Html.closeTag("a");
      if (c.parent)
         c.renderSkin("reply", {nr: i+1});
      else
         c.renderSkin("toplevel", {nr: i+1});
   }
   return;
}


/**
 * This macro calls the checkComment security function and
 * if no error is thrown then render a form to add a comment.
 *
 * @param text Text to be rendered
 * @param skin            default = edit
 * @param skinLoggedOut   Comment-Skin to be used, if user is not logged in
 * @doclevel public
 */
function commentform_macro(param) {
   if (!session.user && this.site.preferences.getProperty("discussionsby") != "anonymous") {
      if (param.skinLoggedOut) {
         res.handlers.comment = (new Comment());
         this.renderSkin(param.skinLoggedOut);
      } else {
         Html.link({href: this.site.members.href("login")},
                   param.text ? param.text : getMessage("Comment.loginToAdd"));
      }
   }
   try {
      this.checkComment(session.user, res.meta.memberlevel);
      var skin = param.skin ? param.skin : "edit";
      res.data.action = this.href("comment");
      (new Comment()).renderSkin(skin);
   } catch (err) {
      ;
   }
   return;
}


/**
 * This macro renders the property of story that defines if
 * other users may edit this story
 *
 * @param as Role after wich the macro renders the content
 * @doclevel admin
 */
function editableby_macro(param) {
   if (param.as == "editor" && (session.user == this.creator || !this.creator)) {
      var options = [EDITABLEBY_ADMINS,
                     EDITABLEBY_CONTRIBUTORS,
                     EDITABLEBY_SUBSCRIBERS];
      var labels = [getMessage("Story.editableBy.admins"),
                    getMessage("Story.editableBy.contributors"),
                    getMessage("Story.editableBy.subscribers")];
      delete param.as;
      if (req.data.publish || req.data.save)
         var selValue = !isNaN(req.data.editableby) ? req.data.editableby : null;
      else
         var selValue = this.editableby;
      for (var i=0;i<options.length;i++) {
         Html.radioButton({name: "editableby", id: "editableby_" + options[i], value: options[i], selectedValue: selValue});
         res.write("&nbsp;");
         Html.openTag("label", {"for": "editableby_" + options[i]});
         res.write(labels[i]);
         Html.closeTag("label");
         res.write("&nbsp;");
      }
   } else {
      switch (this.editableby) {
         case 0 :
            res.write(getMessage("Story.editableBy.adminsLong", {siteTitle: path.Site.title}));
            return;
         case 1 :
            res.write(getMessage("Story.editableBy.contributorsLong", {siteTitle: path.Site.title}));
            break;
         case 2 :
            res.write(getMessage("Story.editableBy.subscribersLong", {siteTitle: path.Site.title}));
            break;
      }
   }
   return;
}


/**
 * This macro renders a checkbox for enabling/disabling discussions
 *
 * @param as Role after wich the macro renders the content
 * @doclevel admin
 */
function discussions_macro(param) {
   Html.openTag("p");
   if (param.as == "editor") {
      var inputParam = this.createCheckBoxParam("discussions", param);
      if ((req.data.publish || req.data.save) && !req.data.discussions) {
         delete inputParam.checked;
      }
      if (path.Site.preferences.getProperty("discussionsby") == "nobody") {
         delete inputParam.checked;
         inputParam.disabled = "disabled";
      }
      Html.checkBox(inputParam);
      Html.hidden({"name": "checkbox_discussions", "value": "discussions"});
   } else {
      if (path.Site.preferences.getProperty("discussionsby") == "nobody") {
         return;
      }
      res.write(this.discussions ? getMessage("generic.yes") : getMessage("generic.no"));
   }
   res.write("&nbsp;" + getMessage("Story.edit.allowComments"));
   Html.closeTag("p");
   return;
}


/**
 * This macro renders a list of existing topics as dropdown element.
 *
 * @doclevel admin
 */
function topicchooser_macro(param) {
   var size = path.Site.topics.size();
   var options = new Array();
   for (var i=0;i<size;i++) {
      var topic = path.Site.topics.get(i);
      if (topic.size()) {
         options[i] = {value: topic.groupname, display: topic.groupname};
         if (req.data.addToTopic)
            var selected = req.data.addToTopic;
         else if (this.topic == topic.groupname)
            var selected = topic.groupname;
      }
   }
   param.name = "addToTopic";
   if (!param.firstOption)
      param.firstOption = "--- " + getMessage("Story.topicChooser.firstOption") + " ---";
   Html.dropDown(param, options, selected, param.firstOption);
   return;
}


/**
 * This macro renders the name of the topic this story belongs to
 * either as link, image (if an image entiteld by the
 * topic name is available) or plain text
 *
 * @param as Defines what is rendered. Values are "text", "image"
 * @param imgprefix Defines the prefix for an image
 * @param text Text to be rendered
 * @doclevel public
 */
function topic_macro(param) {
   if (!this.topic || !this.online)
      return;
   if (param.as == "text")
      res.write(this.topic);
   else if (!param.as || param.as == "link") {
      Html.link({href: this.site.topics.href(this.topic)},
                param.text ? param.text : this.topic);
   } else if (param.as == "image") {
      if (!param.imgprefix)
         param.imgprefix = "topic_";
      var img = getPoolObj(param.imgprefix + this.topic, "images");
      if (!img)
         return;
      Html.openLink({href: this.site.topics.href(this.topic)});
      renderImage(img.obj, param)
      Html.closeLink();
   }
   return;
}


/**
 * This macro returns a list of references linking to a story
 * since referrers are asynchronously written to database by scheduler
 * it makes sense to cache them in story.cache.rBacklinks because they
 * won't change until the next referrer-update was done
 *
 * @return String rendered backlinks
 * @param limit Maximum of chars displayed
 * @skin Story.backlinkItem, Story.backlinks
 * @doclevel public
 */
function backlinks_macro(param) {
   // check if scheduler has done a new update of accesslog
   // if not and we have cached backlinks simply return them
   if (this.cache.lrBacklinks >= app.data.lastAccessLogUpdate)
      return this.cache.rBacklinks;

   var c = getDBConnection("twoday");
   var dbError = c.getLastError();
   if (dbError)
      return getMessage("error.database", dbError);

   // we're doing this with direct db access here
   // (there's no need to do it with prototypes):
   var query = "select ACCESSLOG_REFERRER, count(*) as \"COUNT\" from AV_ACCESSLOG where ACCESSLOG_F_TEXT = " + this._id + " group by ACCESSLOG_REFERRER order by \"COUNT\" desc, ACCESSLOG_REFERRER asc;";
   var rows = c.executeRetrieval(query);
   var dbError = c.getLastError();
   if (dbError)
      return getMessage("error.database", dbError);

   // we show a maximum of 100 backlinks
   var limit = Math.min((param.limit ? parseInt(param.limit, 10) : 100), 100);
   res.push();

   var skinParam = new Object();
   var cnt = 0;
   while (rows.next() && cnt <= limit) {
      skinParam.count = rows.getColumnItem("COUNT");
      skinParam.referrer = rows.getColumnItem("ACCESSLOG_REFERRER");
      skinParam.text = skinParam.referrer.clip(50, "...", true);
      this.renderSkin("backlinkItem", skinParam);
      cnt++;
   }
   rows.release();
   // cache rendered backlinks and set timestamp for
   // checking if backlinks should be rendered again
   skinParam = {referrers: res.pop()};
   if (skinParam.referrers.length > 0)
      this.cache.rBacklinks = this.renderSkinAsString("backlinks", skinParam);
   else
      this.cache.rBacklinks = "";
   this.cache.lrBacklinks = new Date();
   res.write(this.cache.rBacklinks);
   return;
}


/**
 * Macro used to render a checkbox defining whether the story is
 * published on the site's front page
 *
 * @param as Defines what is rendered. Values are "text", "image"
 * @doclevel admin
 */
function addtofront_macro(param) {
   if (param.as == "editor") {
      // if we're in a submit, use the submitted form value.
      // otherwise, render the object's value.
      if (req.data.publish || req.data.save) {
         if (!req.data.addToFront)
            delete param.checked;
      } else if (this.online == 1 || this.publishToTimeState == 2) {
         delete param.checked;
      }
      param.name = param.id = "addToFront";
      param.value = 1;
      delete param.as;
      Html.checkBox(param);
      Html.hidden({"name": "checkbox_addToFront", "value": "addToFront"});
   } else {
      res.write((this.online == 2) ? "yes" : "no");
   }
   return;
}


/**
 * This macro renders admin Task Items
 *
 * @skin Mgr.taskContainer
 * @doclevel admin
 */
function tasks_macro(param) {
   var items = [];

   // check rights
   var mayView = true;
   var mayEdit = true;
   var mayDelete = true;
   try { this.checkView(session.user, res.meta.memberlevel); } catch (deny) { mayView = false; }
   try { this.checkEdit(session.user, res.meta.memberlevel); } catch (deny) { mayEdit = false; }
   try { this.checkDelete(session.user, res.meta.memberlevel); } catch (deny) { mayDelete = false; }

   // build task List
   if (mayView) items.push({href: this.href("main"), text: getMessage("generic.view")});
   if (mayEdit) items.push({href: this.href("edit"), text: getMessage("generic.edit")});
   if (mayEdit) {
      items.push({
         href: this.href("edit") + "?set=" + (this.online ? getMessage("Story.offline") : getMessage("Story.online")),
         text: this.online ? getMessage("Story.setOffline") : getMessage("Story.setOnline")
      });
   }
   if (mayDelete) items.push({href: this.href("delete"), text: getMessage("generic.delete")});
   // add flagging link to task container
   if (session.user && session.user.sysadmin) 
      items.push({href: this.href('flag'), text: getMessage('generic.flag')});
   var modItems = this.applyModuleMethods("renderImageTasks", [], false, false);
   if (modItems) items = items.concat(modItems);

   // render task list
   res.push();
   this._parent.renderTaskItems(items, param);
   param.items = res.pop();

   this._parent.renderSkin("taskContainer", param);
   return;
}


/**
 * Macro Hook in Story/edit.skin
 *
 * @hook renderStoryEdit
 */
function hookStoryEdit_macro(param) {
   this.applyModuleMethods("renderStoryEdit", param);
   return;
}
