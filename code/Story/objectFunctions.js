
/**
 * constructor function for story objects
 */
 function constructor(creator) {
   this.reads = 0;
   this.creator = creator;
   this.editableby = EDITABLEBY_ADMINS;
   this.createtime = new Date();
   // create day of story with respect to site-timezone
   this.day = this.createtime.format("yyyyMMdd");
   this.modifytime = new Date();
}


/**
 * Returns the (clean) title of that story, or uses the first 20 characters of a story as a fallback.
 *
 */
function getTitle() {
   var txt;
   if (this.title) {
      txt = this.getRenderedContentPart("title", "plaintext");
   } else if (this.text) {
      txt = this.getRenderedContentPart("text", "plaintext");
      txt = txt.clip(20, "..");
   } else {
      txt = this.href();
   }
   return txt;
}


/**
 * check if story is ok; if true, save changed story
 * @param Obj Object containing the properties needed for creating a new Story
 * @param Obj User-Object modifying this story
 * @return Obj Object containing two properties:
 *             - error (boolean): true if error happened, false if everything went fine
 *             - message (String): containing a message to user
 */
function evalStory(param, modifier) {
   // collect content
   var origContent = this.content.getAll();
   origContent.title = this.title;
   origContent.text  = this.text;
   var content = extractContent(param, origContent);
   // if all story parts are null, return with error-message
   if (!content.exists)
      throw new Exception("textMissing");
   this.checkContentSize(content.value);
   // sanitize user input only if site is untrusted or allows stories to be written by everyone
   var skipSanitize = this.site.trusted || this.site.preferences.getProperty('usercontrib') === 0;
   if (!skipSanitize) {
      for (var i in content.value) {
         content.value[i] = sanitize(content.value[i], app.properties.allowedTagsForStory.split(","));
      }
   }
   // check if the createtime is set in param
   if (param.createtime) {
      try {
         var fmt = (param.createtime.indexOf(":") != param.createtime.lastIndexOf(":")) ? "yyyy-MM-dd HH:mm:ss" : "yyyy-MM-dd HH:mm";
         var ctime = param.createtime.toDate(fmt, this.site.getTimeZone());
         // re-create day of story with respect to site-timezone
         if (ctime) {
            this.createtime = ctime;
            var newDay = ctime.format("yyyyMMdd", this.site.getLocale(), this.site.getTimeZone());
            if (this.day != newDay) {
               this.day = newDay;
            }
         }
      } catch (err) {
         throw new Exception("timestampParse", param.createtime);
      }
   }
   // check name of topic (if specified)
   var topicName = null;
   if (param.topic) {
      param.topic = stripTags(param.topic);
       // FIXME: this should be solved more elegantly
      if (param.topic.contains("\\") || param.topic.contains("/") || param.topic.contains("?") || param.topic.contains("+"))
         throw new Exception("topicNoSpecialChars");
      if (this.site.topics[param.topic] || this.site.topics[param.topic + "_action"])
         throw new Exception("topicReservedWord");
      topicName = param.topic;
   } else if (param.addToTopic)
      topicName = stripTags(param.addToTopic);

   // always add a story to the front page, if that input-field is not being submitted
   if (param.checkbox_addToFront == null && param.addToFront == null) param.addToFront = 1;
   // store the new values of the story
   if (param.publish) {
      var newStatus = param.addToFront ? 2 : 1;
      if (!this.online || content.isMajorUpdate) {
         this.site.lastupdate = new Date();
      }
      if (!this.online) {
         this.site.lastposting = new Date();
      }
      this.toggleOnline("online");
      this.online = newStatus;
   } else {
      this.toggleOnline("offline");
   }

   // publishToTime?
   this.publishToTimeState = 0;
   if (this.createtime > new Date()) {
      this.publishToTimeState = this.online + 1;
      this.online = 0;
   }

   if (content.isMajorUpdate) {
      this.modifytime = new Date();
   }
   // check if the modifytime is set in param
   if (this.site.trusted && param.modifytime) {
      try {
         var fmt = (param.modifytime.indexOf(":") != param.modifytime.lastIndexOf(":")) ? "yyyy-MM-dd HH:mm:ss" : "yyyy-MM-dd HH:mm";
         var mtime = param.modifytime.toDate(fmt, this.site.getTimeZone());
         if (mtime) {
            this.modifytime = mtime;
         }
      } catch (err) {
         throw new Exception("timestampParse", param.modifytime);
      }
   }
   this.setContent(content.value);
   var oldTopic = this.topic;
   this.topic = topicName;

   // set whether story is also editable by others
   if (modifier == this.creator && (param.checkbox_editableby != null || param.editableby != null)) {
      this.editableby = !isNaN(param.editableby) ?
                        parseInt(param.editableby, 10) : EDITABLEBY_ADMINS;
   }
   // set whether comments are allowed or not
   if (param.checkbox_discussions != null || param.discussions != null) {
      this.discussions = param.discussions ? 1 : 0;
   }
   this.modifier = modifier;

   // update affected static RSS Feeds
   if (oldTopic) {
      var rssFeed = this.site.getStaticRSSFeed("topic." + oldTopic);
      if (rssFeed.exists()) rssFeed.remove();
   }
   this.site.updateAffectedStaticRSSFeeds(this.topic);

   // hook for modules
   this.applyModuleMethods("onEvalStory", [this, content], false);

   var result = new Message("storyUpdate");
   result.url = this.online > 0 ? this.href() : this.site.stories.href();
   result.id = this._id;
   knallgrau.Event.notifyObservers(this, "afterEvalStory", {"story": this, "param": param, "modifier": modifier, "result": result});
   return result;
}


/**
 * function sets story either online or offline
 */
function toggleOnline(newStatus) {
   this.publishToTimeState = 0;
   if (newStatus == "online") {
      this.online = 2;
      this.site.lastupdate = new Date();
      this.site.lastposting = new Date();
      var cmts = this.offlineComments.list();
      for (var i in cmts) {
         cmts[i].online = 1;
         this.comments.add(cmts[i]);
         this.offlineComments.removeChild(cmts[i])
      }
      // Manually add this item to the following collections, since
      // otherwise they would not be present in Site.writeRSSFeedToFile
      // during the processing of this request.
      this.site.add(this);
      this.site.lastmod.add(this);
      this.site.allstories.add(this);
      if (this.topic && this.site.topics.get(this.topic)) this.site.topics.get(this.topic).add(this);
      
   } else if (newStatus == "offline") {
      this.online = 0;
      // set all comments OFFLINE
      var cmts = this.comments.list();
      for (var i in cmts) {
         cmts[i].online = 0;
         this.offlineComments.add(cmts[i]);
         this.comments.removeChild(cmts[i]);
      }
      // manually remove this item from the following collections. For the
      // same reason as stated above.
      this.site.removeChild(this);
      this.site.lastmod.removeChild(this);
      this.site.allstories.removeChild(this);
      if (this.topic && this.site.topics.get(this.topic)) this.site.topics.get(this.topic).removeChild(this);
   }
   // update affected static RSS Feeds
   this.site.updateAffectedStaticRSSFeeds(this.topic);
   // hook for modules
   this.applyModuleMethods("onStoryToggleOnline", {"newStatus": newStatus});
   return true;
}


/**
 * Sets this text's content to a new content object.
 *
 * @param Obj an object containing the new content parts as properties.
 */
function setContent(newContent) {
   this.title = newContent.title;
   delete(newContent.title);
   this.text  = newContent.text;
   delete(newContent.text);
   this.content.setAll(newContent);
   return;
}


/**
 * Evaluates comment and stores it if ok
 * @param Obj Object containing properties needed for creation of comment
 * @param Obj User-Object which is the creator of this comment (optional)
 * @return Obj Object containing two properties:
 *             - error (boolean): true if error happened, false if everything went fine
 *             - message (String): containing a message to user
 */
function evalComment(param, creator) {

   this.applyModuleMethods("onBeforeEvalComment", [param, creator]);

   // collect content
   var content = extractContent(param);
   if (!content.exists)
      throw new Exception("textMissing");
   this.checkContentSize(content.value);
   // sanitize user input
   for (var i in content.value) {
      content.value[i] = sanitize(content.value[i], app.properties.allowedTagsForComment.split(","));
   }
   var c = new Comment(this.site, creator, param.http_remotehost);
   c.setContent(content.value);
   // check if the createtime is set in param; format is either yyyy-MM-dd HH:mm or yyyy-MM-dd HH:mm:ss
   if (param.createtime) {
      try {
         var fmt = (param.createtime.indexOf(":") != param.createtime.lastIndexOf(":")) ? "yyyy-MM-dd HH:mm:ss" : "yyyy-MM-dd HH:mm";
         c.createtime = param.createtime.toDate(fmt, this.site.getTimeZone());
      } catch (err) {
         throw new Exception("timestampParse", param.createtime);
      }
   }
   // check and set anonymous creator if necessary
   if (!creator) {
      if (!param.creatorName)
         throw new Exception("nameMissing");
      if (param.creatorEmail && !isEmail(param.creatorEmail))
         throw new Exception("emailInvalid");
      if (param.creatorUrl) {
         if (!param.creatorUrl.startsWith("http")) param.creatorUrl = "http://" + param.creatorUrl;
         if (!Http.evalUrl(param.creatorUrl))
            throw new Exception("urlInvalid");
      }
      c.creatorName = encode(stripTags(param.creatorName));
      c.creatorEmail = param.creatorEmail;
      c.creatorUrl = stripTags(param.creatorUrl);
   }
   this.add(c);
   
   this.addToCommentCollection(c);
   
   this.site.lastupdate = new Date();
   // update affected static RSS Feed
   this.site.updateStaticRSSFeed("comments");

   // Need this hook to modify online offline
   this.applyModuleMethods("onEvalCommentModifyOnlineOffline", [c, param], false);

   // TODO This hook should be called onAfterEvalComment
   this.applyModuleMethods("onAfterEvalComment", [c, param], false);

   var result = new Message("commentCreate");
   result.id = c._id;
   return result;
}


/**
 * delete all comments of this story
 */
function deleteComments() {
   for (var i=0; i<this.count(); i++) {
      var comment = this.get(i);
      comment.story.deleteComment(comment);
   }
}


/**
 * function deletes a whole thread
 * @param Obj Comment-Object that should be deleted
 * @return String Message indicating success/failure
 */
function deleteComment(commentObj) {
   if (this.site) this.site.lastupdate = new Date();
   // update affected static RSS Feed
   if (this.site) this.site.updateStaticRSSFeed("comments");
   for (var i=commentObj.size();i>0;i--)
      this.deleteComment(commentObj.get(i-1));
   // also remove from comment's parent since it has
   // cachemode set to aggressive and wouldn't refetch
   // its child collection index otherwise
   (commentObj.parent ? commentObj.parent : this).removeChild(commentObj);
   this.comments.removeChild(commentObj);
   commentObj.remove();

   this.applyModuleMethods("onAfterDeleteComment", [commentObj]);

   return new Message("commentDelete");
}


/**
 * function checks if the text of the story was already cached
 * and if it's still valid
 * if false, it caches it again
 * @return String cached text of story
 */
function getRenderedContentPart(name, fmt) {
   if (name == "title")
      var part = this.title;
   else if (name == "text")
      var part = this.text;
   else
      var part = this.content.getProperty(name);
   if (!part)
      return "";
   var key = fmt ? name + ":" + fmt : name;
   var lastRendered = this.cache["lastRendered_" + key];
   if (!lastRendered ||
       lastRendered.getTime() < this.content.getLastModified().getTime()) {
      switch (fmt) {
         case "plaintext":
            part = stripTags(part);
            break;
         case "alttext":
            part = stripTags(part);
            part = part.replace(/\"/g, "&quot;");
            part = part.replace(/\'/g, "&#39;");
            break;
         default:
            var s = createSkin(part);
            this.allowTextMacros(s);
            // enable caching; some macros (eg. poll, storylist)
            // will set this to false to prevent caching of a contentpart
            // containing them
            res.meta.cachePart = true;
            // The following is necessary so that global macros know where they belong to.
            // Even if they are embedded at some other site.
            var tmpSite;
            if (this.site != res.handlers.site) {
               tmpSite = res.handlers.site;
               res.handlers.site = this.site;
            }
            part = format(this.renderSkinAsString(s).activateURLs());
            if (tmpSite)
               res.handlers.site = tmpSite;
      }
      this.cache[key] = part;
      if (res.meta.cachePart)
         this.cache["lastRendered_" + key] = new Date();
   }
   return this.cache[key];
}


/**
 * function deletes all childobjects of a story (recursive!)
 */
function deleteAll() {
   for (var i = this.comments.size(); i > 0; i--)
      this.comments.get(i-1).remove();
   return true;
}


/**
 * function records the access to a story-object
 * by incrementing the counter of the Object representing
 * this story in app.data.readLog which will be stored
 * in database by scheduler
 */
function incrementReadCounter() {
   if (session.user && session.user == this.creator) return;
   var type = (path.Site) ? path.Site.type : null;
   // if (type == "free") return;
   // check if app.data.readLog already contains
   // an Object representing this story
   if (!app.data.readLog.containsKey(String(this._id))) {
      var logObj = new Object();
      logObj.site = this.site.alias;
      logObj.story = this._id;
      logObj.newReads = 1;
      app.data.readLog.put(String(this._id), logObj);
   } else {
      app.data.readLog.get(String(this._id)).newReads++;
   }
   return;
}


/**
 * Return either the title of the story or
 * the id prefixed with standard display name
 * to be used in the global linkedpath macro
 * @see hopobject.getNavigationName()
 */
function getNavigationName () {
   if (this.title)
      return this.title;
   else
      return getMessage("Story") + " " + this._id;
}


/**
 * checks if the size of any part of the
 * content exceeds it's limit
 * @return true if the content is valid (otherwise an Exception is thrown)
 */
function checkContentSize(newContent) {
   for (var i in newContent) {
      if (newContent[i].length > 64000)
         throw new Exception("contentPartTooLong", "64.000");
   }
   return true;
}

/**
 * additional functionality that is prototype specific
 */
function addToCommentCollection(c) {
   // also add to story.comments since it has
   // cachemode set to aggressive and wouldn't refetch
   // its child collection index otherwise
   this.comments.add(c);
}

/**
 * If a story/comment is flagged, the function returns the rendered warning box including
 * the related flag text assigned by the sysadmin (stored in the item's preferences_xml)
 * 
 * The scope is used as a random CSS scope qualifier, which will change with every call.
 * That way, warning boxes can't be easily suppressed or set to display:none. In addition
 * the CSS is scoped so it does not interfere with the blog's custom CSS.
 * 
 * @returns string rendered flag warning box or empty string
 */
function renderWarningFlag() {
   var prefs = this.preferences.getAll();

   if (prefs.isflagged) {
      // provide a random qualifier for a scoped CSS
      var scope = Packages.helma.util.MD5Encoder.encode(Math.random()).slice(-8);

      // provide direct click link to flag screen if current user is a sysadmin
      var onclick = session.user && session.user.sysadmin
         ? ' onclick=\'window.open("' + this.href('flag') + '", "_blank")\' onmouseover="this.style.cursor=\'pointer\'"'
         : '';

      // assemble the warning flag text
      var genericText = getMessage('warn.flag.Generic');
      var flagtext = prefs.flagtype === 'Generic'
         ? genericText
         : getMessage('warn.flag.' + prefs.flagtype, { generic: genericText });

      return this.renderSkinAsString('flagWarning', {
         scope: scope,
         onclick: onclick,
         flagtext: flagtext,
         flagaddition: prefs.flagaddition || ''
      });
   } else return '';
}
