
/**
 * renders an admin menu for site manager
 * @param level   comma separated list of numerical admin levels,
 *                starting at 1
 * @see Mgr.renderMenu
 */
function adminMenu_macro(param) {
   if (!path.site) return;
   if (res.meta.adminMenuPath == "public") return;
   this.renderMenu(MGR_MENU, param.level, param);
}


/**
 * renders an admin menu for root manager
 * @param level   comma separated list of numerical admin levels,
 *                starting at 1
 * @see Mgr.renderMenu
 */
function rootAdminMenu_macro(param) {
   if (res.meta.adminMenuPath == "public") return;
   this.renderMenu(ROOT_MGR_MENU, param.level, param);
}

function adminMenuGetKey() {
   // var obj = path[path.length - 1];
   // var action = req.action;
   // return res.meta.adminMenuKey
   return "admin.layout.images";
}


/**
 * Macro renders a separator for Mgr screens
 * @see Mgr.separator.skin
 */
function separator_macro(param) {
   this.renderSkin("separator", param);
}


/**
 * renders navigation for listing stories
 */
function listNavigation_macro(param) {

   var items = this.getListNavigationItems();
   res.push();
   this.renderListNavigationItems(items, param);
   param.items = res.pop();

   this.renderSkin("listNavigationContainer", param);
   return;
}


/**
 * returns an Array of User tasks
 * @param param Object   passed by Macro
 * @return Array
 */
function getUserModuleTasks(param) {
   return this.applyModuleMethods("renderUserTasks", [], false, false);
}


/**
 * render list of member tasks
 */
function userModuleTasks_macro(param) {
   var items = this.getUserModuleTasks(param);
   this.renderTaskItems(items, param);
   return;
}
