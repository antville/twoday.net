
function renderMgrPage(proto) {
   knallgrau.Event.notifyObservers(this, "beforeRenderMgrPage", proto);
   knallgrau.addToResponseHead(root.renderSkinAsString("statsCounter"));
   if (!proto) proto = this._prototype;
   if (path.site) {
      res.handlers.user = session.user;
      this.renderSkin("page");
   } else {
      this.renderSkin("rootmgr");
   }
   renderAlpineScript();
}

function renderMenuListItem(text, href, icon) {
   this.renderSkin("menuItem", {
      text: text,
      href: href,
      icon: icon
   });
}

function renderListItem(param, skin) {
   this.renderSkin(skin, {
      text: text,
      href: href,
      icon: icon
   });
}

function renderListNavigationItems(items, param) {
   this.renderListItems(items, param, "listNavigation", {hasDivider: true});
}

function renderMenuItems(items, param) {
   this.renderListItems(items, param, "menu", {hasDivider: true});
}

function renderTaskItems(items, param) {
   this.renderListItems(items, param, "task", {hasDivider: true});
}

function renderListItems(items, param, type, options) {
   if (items == null || !items instanceof Array) return;
   if (!options) options = {};
   var hasDivider = (options.hasDivider == true);
   var isActive;
   for (var i = 0; i < items.length; i++) {
      if (items[i].action && typeof items[i].action == "string") {
         var parts = items[i].action.split("/");
         if (items[i].isActive == undefined) {
            items[i].isActive = (path[path.length - 1]._prototype == parts[0] && (req.action == parts[1]));
         }
         if (items[i].href == undefined) {
            items[i].href = (res.handlers[parts[0].toLowerCase()] ? res.handlers[parts[0].toLowerCase()] : root).href(parts[1]);
         }
         if (items[i].text == undefined) {
            items[i].text = getMessage(parts[0] + "." + parts[1] + "." + type + "Label");
         }
      }
      // isActive ?
      if (typeof items[i].isActive == "boolean") {
         isActive = items[i].isActive;
      } else if (typeof items[i].isActive == "function") {
         isActive = items[i].isActive()
      } else if (typeof items[i].isActive == "string") {
         var isActiveParts = items[i].isActive.split("/");
         isActive = (path[path.length - 1]._prototype == isActiveParts[0] && (req.action == isActiveParts[0]));
      } else {
         isActive = false;
      }
      var className =
         isActive ?
         (
            param.itemActiveClass ?
            param.itemActiveClass :
            (
               param.itemClass ?
               param.itemClass + " active" :
               "active"
            )
         ) :
         param.itemClass;
      var href =
         (typeof items[i].href == "function") ?
         items[i].href() :
         items[i].href;
      // append action specific classname for all except twoday
      if (app.name != "twoday" && res.handlers.site) {
         var siteHref = res.handlers.site.href();
         if (href && href.indexOf(siteHref) != -1) {
            var additionalClassName = href.substring(siteHref.length, href.length);
            additionalClassName = additionalClassName.replace(/[^a-zA-Z0-9]/g, "").substring(0, 15);
            className = className ? className + " twoday-" + additionalClassName : "twoday-" + additionalClassName;
         }
      }
      if (param.itemSkin) {
         this.renderSkin(param.itemSkin, {text: items[i].text, href: href, itemClass: className});
      } else {
         this.renderSkin(type + "Item", {text: items[i].text, href: href, itemClass: className});
      }
      if (i < items.length - 1) {
         if (param.divider) {
            res.write(param.divider);
         } else if (param.dividerSkin) {
            this.renderSkin(param.dividerSkin);
         } else if (hasDivider) {
            this.renderSkin(type + "Divider");
         }
      }
   }
}


/**
 * renders an Array of task items passed by a macro
 * @param items Array   of task items
 * @param param Object  passed by macro
 */
function renderTasks(items, param) {
   res.push();
   this.renderTaskItems(items, param);
   param.items = res.pop();

   var containerSkin = param.containerSkin ? param.containerSkin : "taskContainer";
   this.renderSkin(containerSkin, param);
   return;
}

/**
 * renders an admin menu from an MENU-Object
 * used by Mgr.adminMenu_macro & co.
 * @param adminMenu    MENU-CONSTANT
 * @param adminLevel   comma separated list of numerical admin levels,
 *                     starting at 1
 * @param param        Object, passed by macro
 * @see Mgr.adminMenu_macro
 */
function renderMenu(adminMenu, adminLevel, param) {
   if (!adminLevel) adminLevel = "all";
   if (!adminMenu) adminMenu = MGR_MENU;
   if (!param) param = {};
   var navParam;
   var activeKey = res.meta.adminMenuPath;
   var activeMenuItem = adminMenu.getChildElement(activeKey);

   for (var level = 1; level <= 5; level++) {
      if (adminLevel.indexOf(level + "") != "-1" || adminLevel == "all") {
         var items = [];
         containerParam = {};
         var parentElement = activeMenuItem ? activeMenuItem.getParentElement(level - 1) : null;
         if (!parentElement) continue;
         var menuItemsLevel = parentElement.list();
         for (var i = 0; i < menuItemsLevel.length; i++) {
            if (isFunction(menuItemsLevel[i].object)) {
               var obj = menuItemsLevel[i].object();
            } else {
               try {
                  var obj = eval(menuItemsLevel[i].object);
               } catch (err) {}
            }
            var action = menuItemsLevel[i].action;
            var urlparam = "";
            if (action.indexOf("?") != -1) {
              urlparam = action.substring(action.indexOf("?"));
              action = action.substring(0, action.indexOf("?"));
            }
            var site = obj.getSite();
            var membershipLevel = site ? site.members.getMembershipLevel(session.user) : null;
            var check = obj.checkAccess(action, session.user, membershipLevel);
            if (check != null) continue;
            var item = {
               isActive: menuItemsLevel[i].isParentOf(activeMenuItem),
               href: obj.href(action) + urlparam,
               text: menuItemsLevel[i].getTitle()
            }
            items.push(item);
         }
         res.push();
         this.renderMenuItems(items, param);

         containerParam.items = res.pop();
         containerParam.className = "menu level" + level;
         this.renderSkin("menuContainer", containerParam);
      }
   }
}
