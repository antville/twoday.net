
/**
 * Returns a param-object for usage with closePopUp.skin
 * @example
 *   renderSkin("closePopUp", getRenderRedirectParam(someURL))
 * @param to   String, URL
 * @return Object
 */
function getRenderRedirectParam(to) {
  var param = new Object();
  param.js = "window.opener.location.replace('"+to+"')";
  param.url = to;
  return param;
}


/**
 * Returns a param-object for usage with closePopUp.skin
 * @example
 *   renderSkin("closePopUp", getRenderRedirectParam(someURL))
 * @return Object
 */
function getRenderReloadParam() {
  var param = new Object();
  param.js = "window.opener.location.reload()";
  param.url = "";
  return param;
}
