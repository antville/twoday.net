
/**
 * constructor function for MenuItem objects
 * used to build the constant menu
 */
function MenuItem(name, obj, action, p, isHook) {
   this.name = name;
   this.object = obj;
   this.action = action;
   this.parent = p;
   this.isHook = isHook;
   this.children = [];
}


MenuItem.prototype.add = function(name, obj, action) {
   var obj = new Mgr.prototype.MenuItem(name, obj, action, this, false);
   this.children.push(obj);
   return obj;
}


MenuItem.prototype.addHook = function(name) {
   var obj = new Mgr.prototype.MenuItem(name, null, null, this, true);
   this.children.push(obj);
   return obj;
}

MenuItem.prototype.getChildElement = function(name) {
   if (name == null) return;
   if (typeof name == "function") name = name();
   var names = name.split(".");
   var obj = this;
   for (var i=0; i<names.length; i++) {
      obj = Array.getElementByKey(obj.children, names[i], "name"); // children[names[i]];
      if (obj == null) {
         return null;
      }
   }
   return obj;
}


MenuItem.prototype.getParentElement = function(level) {
   if (level != null) {
      var obj = this;
      if (level > obj.getLevel()) return null;
      while (obj.getLevel() != level) obj = obj.parent;
      return obj;
   } else {
      return this.parent;
   }
}


MenuItem.prototype.isParentOf = function (el) {
   var obj = el;
   while (obj && obj != this) {
      obj = obj.parent;
   }
   return (obj == this) ? true : false;
}


MenuItem.prototype.getPrototype = function() {
   return "MenuItem";
}


MenuItem.prototype.getTitle = function() {
   var msg = getMessage("admin.menu." + this.getFullKey());
   return msg ? msg : "admin.menu." + this.getFullKey();
}


MenuItem.prototype.getFullKey = function () {
   var names = [];
   var obj = this;
   while (obj && obj.parent) {
      names.unshift(obj.name);
      obj = obj.parent;
   }
   return names.join(".");
}


MenuItem.prototype.getLevel = function () {
   var level = 0;
   var obj = this;
   while (obj && obj.parent) {
      level++;
      obj = obj.parent;
   }
   return level;
}


MenuItem.prototype.href = function () {
   var obj = (isFunction(this.object)) ?
             this.object() : eval(this.object);
   var action = this.action;
   var urlparam = "";
   if (action.indexOf("?") != -1) {
     urlparam = action.substring(action.indexOf("?"));
     action = action.substring(0, action.indexOf("?"));
   }
   var denyAccess = obj.checkAccess(action, session.user, res.meta.memberlevel);
   if (denyAccess) return null;
   else return obj.href(action) + urlparam;
}


MenuItem.prototype.checkAccess = function () {
   var obj = (isFunction(this.object)) ?
             this.object() : eval(this.object);
   var action = this.action;
   if (action.indexOf("?") != -1) {
     action = action.substring(0, action.indexOf("?"));
   }
   return obj.checkAccess(action, session.user, res.meta.memberlevel);
}


MenuItem.prototype.list = function () {
   return this.children;
}


var rootItem, level1, level2, level3;
rootItem = new MenuItem("root");
// level1 = rootItem.add("public", "res.handlers.site", "main");
level1 = rootItem.add("admin", "res.handlers.site", "manage");
   level2 = level1.add("site", "res.handlers.site", "manage");
      level3 = level2.add("view", "res.handlers.site", "main");
      level3 = level2.add("edit", "res.handlers.site", "edit");
      level3 = level2.add("spamfilter", "res.handlers.site", "spamfilter");
      level3 = level2.add("referrers", "res.handlers.site", "referrers");
      level3 = level2.add("mostread", "res.handlers.site", "mostread");
   level2 = level1.add("layout", function() { return (res.handlers.layout.site == res.handlers.site) ? res.handlers.layout : res.handlers.site.layout }, "main");
      level3 = level2.add("edit", "res.handlers.layout", "edit");
         level3.add("choose", "res.handlers.layout", "choose");
      level3 = level2.add("preferences", "res.handlers.layout", "preferences");
      level3 = level2.add("sidebars", "res.handlers.site.modules", "order");
      level3 = level2.add("images", "res.handlers.layout.images", "main");
      level3 = level2.add("skins", "res.handlers.layout.skins", "main");
         level3.add("default", "res.handlers.layout.skins", "all");
         level3.add("modified", "res.handlers.layout.skins", "modified");
         level3.add("custom", "res.handlers.layout.skins", "custom");
      level3 = level2.add("layouts", "res.handlers.site.layouts", "main");
      level3 = level2.add("switch", "res.handlers.site.layouts", "switch");
   level2 = level1.add("members", "res.handlers.site.members", "main");
      level3 = level2.add("all", "res.handlers.site.members", "main");
      level3 = level2.add("admins", "res.handlers.site.members", "admins");
      level3 = level2.add("managers", "res.handlers.site.members", "managers");
      level3 = level2.add("contributors", "res.handlers.site.members", "contributors");
      level3 = level2.add("subscribers", "res.handlers.site.members", "subscribers");
      level3 = level2.add("create", "res.handlers.site.members", "create");
   level2 = level1.add("modules", "res.handlers.site.modules", "sidebar");
      level3 = level2.add("sidebar", "res.handlers.site.modules", "sidebar");
      level3 = level2.add("order", "res.handlers.site.modules", "order");
      level3 = level2.add("extension", "res.handlers.site.modules", "main");
level1 = rootItem.add("contribute", "res.handlers.site", "contribute");
   level2 = level1.add("create", "res.handlers.site.stories", "create");
   level2 = level1.add("stories", "res.handlers.site.stories", "main");
   level2 = level1.add("images", "res.handlers.site.images", "main");
   level2 = level1.add("files", "res.handlers.site.files", "main");
   level2 = level1.add("polls", "res.handlers.site.polls", "main");
level1 = rootItem.add("account", "res.handlers.site.members", "account");
   level2 = level1.add("main", "res.handlers.site.members", "account");
   level2 = level1.add("settings", "res.handlers.site.members", "edit");
      level3 = level2.add("edit", "res.handlers.site.members", "edit");
   level2 = level1.add("memberships", "res.handlers.site.members", "memberships");
   level2 = level1.add("subscriptions", "res.handlers.site.members", "updated");
MGR_MENU = rootItem;

var rootItem, level1, level2;
rootItem = new MenuItem("root");
level1 = rootItem.add("rootadmin", "root", "main");
   level2 = level1.add("sites", "root.manage", "sites");
   level2 = level1.add("users", "root.manage", "users");
   level2 = level1.add("layouts", "root.layouts", "main");
   level2 = level1.add("logs", "root.manage", "logs");
   level2 = level1.add("setup", "root.manage", "setup");
   level2 = level1.add("modules", "root.modules", "main");
      level2.add("reload", "root.modules", "main?reload=1");
   level2 = level1.add("status", "root.manage", "status");
level1 = rootItem.add("account", "root.members", "edit");
   level2 = level1.add("main", "root.members", "account");
   level2 = level1.add("settings", "root.members", "edit");
      level3 = level2.add("edit", "root.members", "edit");
   level2 = level1.add("memberships", "root.members", "memberships");
   level2 = level1.add("subscriptions", "root.members", "updated");
ROOT_MGR_MENU = rootItem;

MGR_MENU_PATH = {
   "SysMgr": {
      "sites":          "rootadmin.sites",
      "users":          "rootadmin.users",
      "logs":           "rootadmin.logs",
      "setup":          "rootadmin.setup",
      "status":         "rootadmin.status"
   },
   "RootLayoutMgr": {
      "main":           "rootadmin.layouts"
   },
   "RootModuleMgr": {
      "main":           "rootadmin.modules",
      "import":         "rootadmin.modules.import"
   },
   "Site": {
      "manage":         "admin.site",
      "contribute":     "contribute",
      "edit":           "admin.site.edit",
      "block":          "admin.site.edit",
      "spamfilter":     "admin.site.spamfilter",
      "mostread":       "admin.site.mostread",
      "referrers":      "admin.site.referrers",
      "unsubscribe":    "account.subscriptions"
   },
   "ImageMgr": {
      "changeicon":     "admin.site"
   },
   "ModuleMgr": {
      "main":           "admin.modules.extension",
      "sidebar":        "admin.modules.sidebar",
      "order":          "admin.modules.order",
      "preferences":    "admin.modules",
      "skins":          "admin.modules"
   },
   "Module": {
      "initialize":     "rootadmin.modules",
      "uninstall":      "rootadmin.modules",
      "setup":          "rootadmin.modules"
   },
   "Membership": {
      "edit":           "admin.members",
      "delete":         "admin.members"
   },
   "MemberMgr": {
      "main":           "admin.members.all",
      "admins":         "admin.members.admins",
      "managers":       "admin.members.managers",
      "contributors":   "admin.members.contributors",
      "subscribers":    "admin.members.subscribers",
      "updated":        "account.subscriptions",
      "subscriptions":  "account.subscriptions",
      "memberships":    "account.memberships",
      "create":         "admin.members",
      "account":        "account.main",
      "edit":           "account.settings.edit",
      "logout":         "public",
      "sendpwd":        "public",
      "newsite":        "admin.members"
   },
   "SkinMgr": {
      "main":      function() { return (path.layout.site) ? "admin.layout.skins.custom" : "rootadmin.layouts" },
      "all":       function() { return (path.layout.site) ? "admin.layout.skins.default" : "rootadmin.layouts" },
      "modified":  function() { return (path.layout.site) ? "admin.layout.skins.modified" : "rootadmin.layouts" },
      "custom":    function() { return (path.layout.site) ? "admin.layout.skins.custom" : "rootadmin.layouts" },
      "safe":      function() { return (path.layout.site) ? "admin.layout.skins" : "rootadmin.layouts" },
      "edit":      function() { return (path.layout.site) ? "admin.layout.skins" : "rootadmin.layouts" },
      "create":    function() { return (path.layout.site) ? "admin.layout.skins" : "rootadmin.layouts" }
   },
   "LayoutMgr": {
      "main":      function() { return (res.handlers.site) ? "admin.layout.layouts" : "rootadmin.layouts" },
      "create":    function() { return (res.handlers.site) ? "admin.layout.layouts" : "rootadmin.layouts" },
      "import":    function() { return (res.handlers.site) ? "admin.layout.layouts" : "rootadmin.layouts" },
      "switch":    function() { return (res.handlers.site) ? "admin.layout.switch" : "rootadmin.layouts" },
   },
   "Layout": {
      "main":           function() { return (path.layout.site) ? "admin.layout" : "rootadmin.layouts" },
      "edit":           function() { return (path.layout.site) ? "admin.layout.edit" : "rootadmin.layouts" },
      "preferences":    function() { return (path.layout.site) ? "admin.layout.preferences" : "rootadmin.layouts" },
      "choose":         function() { return (path.layout.site) ? "admin.layout.choose" : "rootadmin.layouts" },
      "startTestdrive": function() { return (path.layout.site) ? "admin.layout" : "rootadmin.layouts" },
      "stopTestdrive":  function() { return (path.layout.site) ? "admin.layout" : "rootadmin.layouts" },
      "delete":         function() { return (path.layout.site) ? "admin.layout" : "rootadmin.layouts" },
      "download":       function() { return (path.layout.site) ? "admin.layout" : "rootadmin.layouts" },
/*  TODO translated Layouts currently disabled
      "translate":      function() { return (path.layout.site) ? "admin.layout" : "rootadmin.layouts" },
      "localizations":  function() { return (path.layout.site) ? "admin.layout" : "rootadmin.layouts" },
      "importlocale":   function() { return (path.layout.site) ? "admin.layout" : "rootadmin.layouts" }*/
   },
   "LayoutImageMgr": {
      "main":           function() { return (path.layout.site) ? "admin.layout.images" : "rootadmin.layouts" },
      "delete":         function() { return (path.layout.site) ? "admin.layout.images" : "rootadmin.layouts" },
      "all":            function() { return (path.layout.site) ? "admin.layout.images" : "rootadmin.layouts" },
      "default":        function() { return (path.layout.site) ? "admin.layout.images" : "rootadmin.layouts" },
      "additional":     function() { return (path.layout.site) ? "admin.layout.images" : "rootadmin.layouts" },
      "create":         function() { return (path.layout.site) ? "admin.layout.images" : "rootadmin.layouts" }
   },
   "LayoutImage": {
      "delete":         function() { return (path.layout.site) ? "admin.layout.images" : "rootadmin.layouts" },
      "main":           function() { return (path.layout.site) ? "admin.layout.images" : "rootadmin.layouts" },
      "edit":           function() { return (path.layout.site) ? "admin.layout.images" : "rootadmin.layouts" },
      "replace":        function() { return (path.layout.site) ? "admin.layout.images" : "rootadmin.layouts" }
   },
   "Skin": {
      "delete": "admin.layout.skins"
   }
}
