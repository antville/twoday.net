
/**
 * wrapper to make manageSkin.skin public as Mgr/manage.css
 */
function manage_css_action() {
   if (!res.handlers.layout) res.handlers.layout = new Layout();
   res.contentType = "text/css";
   this.renderSkin("style");
}


/**
 * wrapper to make wizardSimpleStyle.skin public
 */
function wizardsimple_css_action() {
   res.contentType = "text/css";
   this.renderSkin("wizardSimpleStyle");
}
