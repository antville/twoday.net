
/**
 * drop the "global" prototype to
 * display correct macro syntax
 */
function proto_macro() {
   if (this.proto.toLowerCase() != "global")
      res.write(this.proto);
   return;
}


/**
 * link to delete action
 */
function deletelink_macro(param) {
   Html.link({href: this.href("delete")}, param.text ? param.text : "delete");
   return;
}


/**
 * link to diff action
 * @param as     if set to popup javascript for popup will be added
 * @param popupMenubar     no default
 * @param popupToolbar     no default
 * @param popupStatus      no default
 * @param popupScrollbars  default=yes
 * @param popupResizable   default=yes
 * @param popupWidth       default=570
 * @param popupHeight      default=570
 * @param target           will be set to "_blank" if as="popup"
 */
function difflink_macro(param) {
   if (this.custom == 1 && !this.layout.skins.getOriginalSkin(this.proto, this.name)) return;

   param.href = this.href("diff");
   var text = param.text || "diff";
   delete param.text;
   if (!param.onclick && param.as == "popup") {
      param.onclick =
         "var popupWindow = window.open('" + param.href + "', 'popup', '" +
         (param.popupMenubar ? ", menubar=" + param.popupMenubar : "") +
         (param.popupToolbar ? ", toolbar=" + param.popupToolbar : "") +
         (param.popupStatus ? ", status=" + param.popupStatus : "") +
         ", scrollbars=" + (param.popupScrollbars ? param.popupScrollbars : "yes") +
         ", resizable=" + (param.popupResizable ? param.popupResizable : "yes") +
         ", width=" + (param.popupWidth ? param.popupWidth : 570) +
         ", height=" + (param.popupHeight ? param.popupHeight : 570) +
         "'); popupWindow.focus(); return false;";
      param.target = param.target || "_blank";
      delete param.popupMenubar;
      delete param.popupToolbar;
      delete param.popupStatus;
      delete param.popupScrollbars;
      delete param.popupResizable;
      delete param.popupWidth;
      delete param.popupHeight;
      delete param.as;
   }

   Html.link(param, text);
   return;
}
