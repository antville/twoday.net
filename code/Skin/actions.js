
/**
 * action rendering the differences between the original skin
 * and the modified one
 */
function diff_action() {
   // get the modified and original skins
   var originalSkin = this.layout.skins.getOriginalSkinSource(this.proto, this.name);

   if (originalSkin == null) {
      res.data.status = getMessage("Skin.diff.noDiffAvailable");
   } else {
      var diff = originalSkin.diff(this.skin ? this.skin : "");
      if (!diff) {
         res.data.status = getMessage("Skin.diff.noDiffFound");
      } else {
         res.push();
         var sp = new Object();
         for (var i in diff) {
            var line = diff[i];
            sp.num = line.num;
            if (line.deleted) {
               sp.status = "&minus;";
               sp["class"] = "removed";
               for (var j=0;j<line.deleted.length;j++) {
                  sp.num = line.num + j;
                  sp.line = encode(line.deleted[j]);
                  this.renderSkin("diffline", sp);
               }
            }
            if (line.inserted) {
               sp.status = "&plus;";
               sp["class"] = "added";
               for (var j=0;j<line.inserted.length;j++) {
                  sp.num = line.num + j;
                  sp.line = encode(line.inserted[j]);
                  this.renderSkin("diffline", sp);
               }
            }
            if (line.value != null) {
               sp.status = "&nbsp;";
               sp["class"] = "line";
               sp.line = encode(line.value);
               this.renderSkin("diffline", sp);
            }
         }
         res.data.diff = res.pop();
      }
   }
   res.data.body = this.renderSkinAsString("diff");
   res.data.title = getMessage("Skin.diff.display.title", {skinProto: this.proto, skinName: this.name, layoutTitle: this.layout.title});

   // rende wizard page
   this.layout.skins.renderSkin("wizardSimplePage", {});
   return;
}


/**
 * delete action
 */
function delete_action() {
   if (req.data.cancel) {
      if (this.module)
         res.redirect(res.handlers.site.modules.href("skins") + "?mod=" + this.module);
      else
         res.redirect(this.layout.skins.href());
   } else if (req.data.remove) {
      try {
         checkSecretKey();
         var mod = this.module;
         res.message = this.layout.skins.deleteSkin(this);
         if (mod)
            res.redirect(res.handlers.site.modules.href("skins") + "?mod=" + this.module);
         else
            res.redirect(this.layout.skins.href());
      } catch (err) {
         res.message = err.toString();
      }
   }

   res.data.action = this.href(req.action);
   res.data.title = res.handlers.context.getTitle();
   var skinParam = {
      description: getMessage("Skin.deleteDescription"),
      detail: this.name
   };
   res.data.body = this.renderSkinAsString("delete", skinParam);
   res.data.sidebar01 = this._parent._parent.renderSkinAsString("mainSidebar");
   res.handlers.context.layouts.renderMgrPage();
   return;
}
