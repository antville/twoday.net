
/**
 * evaluates the preferences for a module in a site
 */
function evalPreferences(param, user, moduleName) {
   // store the header for sidebar modules
   var headerFieldName = moduleName.toLowerCase() + "header";
   var mod = root.modules.get(moduleName);
   if (req.data[headerFieldName] && req.data[headerFieldName] != mod.getHeader()) {
      this._parent.preferences.setProperty(headerFieldName, stripTags(req.data[headerFieldName]));
   }

   // call module function
   if (app.modules[moduleName] && app.modules[moduleName].evalModulePreferences) {
      app.modules[moduleName].evalModulePreferences.apply(this._parent, []);
      this._parent.modifytime = new Date();
   }

   return new Message("update");
}


/**
 * evaluates the preferences for a module in a site
 */
function evalOrder(param, user) {
   this._parent.preferences.setProperty("sidebar01", param.sidebar01);
   this._parent.preferences.setProperty("sidebar02", param.sidebar02);
   return new Message("update");
}



/**
 * returns an Array of list navigation items
 * @param param Object   passed by Macro
 * @return Array
 */
function getListNavigationItems() {
   var items = [
      {
         text: getMessage("admin.menu.admin.modules.all"),
         action: "ModuleMgr/sidebar"
      },
      {
         text: getMessage("admin.menu.admin.modules.navigation"),
         action: "ModuleMgr/navigation"
      },
      {
         text: getMessage("admin.menu.admin.modules.info"),
         action: "ModuleMgr/info"
      },
      {
         text: getMessage("admin.menu.admin.modules.externalContents"),
         action: "ModuleMgr/externalContents"
      },
      {
         text: getMessage("admin.menu.admin.modules.list"),
         action: "ModuleMgr/list"
      },
      {
         text: getMessage("admin.menu.admin.modules.others"),
         action: "ModuleMgr/others"
      }
   ];
   return items;
}
