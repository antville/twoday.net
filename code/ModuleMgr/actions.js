
/**
 * main action
 */
function main_action() {
   if (!res.handlers.site) res.redirect(this.href("main"));

   res.data.title = getMessage("ModuleMgr.main.title");
   res.data.action = this.href();
   var modules = res.handlers.site.getModules();

   res.data.list = renderList(modules, "sitemgrlistitem", 20, req.data.page);
   res.data.pagenavigation = renderPageNavigation(modules, this.href("main"), 20, req.data.page);
   res.data.body = this.renderSkinAsString("main");
   res.data.sidebar02 = this.renderSkinAsString("mainSidebar");
   this.renderMgrPage();
}


/**
 * main action
 */
function sidebar_action() {
   if (!res.handlers.site) res.redirect(this.href("main"));

   res.data.title = getMessage("ModuleMgr.sidebar.title");
   res.data.action = this.href();

   var modules = res.handlers.site.getSidebarModules();
   res.data.list = renderList(modules, "sitemgrlistitem", 40, req.data.page);
   res.data.pagenavigation = renderPageNavigation(modules, this.href("sidebar"), 40, req.data.page);
   res.data.body = this.renderSkinAsString("main");
   res.data.sidebar02 = this.renderSkinAsString("mainSidebar");
   this.renderMgrPage();
}


/**
 * position modules in sidebars action
 * @skin ModuleMgr.order.skin
 * @skin ModuleMgr.order.skinSidebar
 */
function order_action() {
   if (!res.handlers.site) res.redirect(this.href("main"));

   res.data.title = getMessage("ModuleMgr.order.title");
   res.data.action = this.href(req.action);

   if (req.data.cancel) {
      res.redirect(this.href("sidebar"));
   } else if (req.data.save) {
      try {
         checkSecretKey();
         var result = this.evalOrder(req.data, session.user);
         res.message = result.toString();
      } catch (err) {
         res.message = err.toString();
      }
      res.redirect(this.href(req.action));
   }

   // depending on the orientation of the layout we have to draw the order dialog
   var param = {};
   var orientation = this._parent.layout.getOrientationValues();

   if (orientation.columns == "2c") {
      switch (orientation.side) {
      case "left":
         param.gridStyleCol_1 = "width: 190px;"
         param.gridStyleCol_2 = "width: 260px;"
         param.gridStyleCol_3 = "display: none;"
         param.sidebarLeft = this.getSidebarModulesChooser("sidebar01");
         param.sidebarLeft += this.getSidebarModulesChooser("sidebar02");
         break;
      case "right":
         param.gridStyleCol_1 = "display: none;"
         param.gridStyleCol_2 = "width: 260px;"
         param.gridStyleCol_3 = "width: 190px;"
         param.sidebarRight = this.getSidebarModulesChooser("sidebar01");
         param.sidebarRight += this.getSidebarModulesChooser("sidebar02");
         break;
      }
   }
   if (orientation.columns == "3c") {
      param.gridStyleCol_1 = "width: 130px;"
      param.gridStyleCol_2 = "width: 210px;"
      param.gridStyleCol_3 = "width: 130px;"
      param.sidebarLeft = this.getSidebarModulesChooser("sidebar01");
      param.sidebarRight = this.getSidebarModulesChooser("sidebar02");
   }
   param.availeableSidebarModulesChooser = this.getAvailableSidebarModulesChooser();

   res.data.body = this.renderSkinAsString("order", param);
   res.data.sidebar02 = this.renderSkinAsString("orderSidebar");
   this.renderMgrPage();
}


function getSidebarModulesChooser(id) {
   var param = {};
   param.label = id.capitalize(1).replace("0", " ");
   param.name = param.id = id + "Chooser";

   var modules = res.handlers.site.getModulesInSidebar(id);
   var chooserValues = [];
   for (var i in modules) {
      chooserValues.push({value: modules[i].name, display: (modules[i].getHeader().length > 15 ? modules[i].getHeader().substring(0, 13) + ".." : modules[i].getHeader())});
   }

   param.chooser = Html.dropDownAsString({name: id + "Chooser", id: id + "Chooser", size: 8, multiple: "multiple"}, chooserValues);

   return this.renderSkinAsString("orderChooser", param);
}


function getAvailableSidebarModulesChooser() {
   var id = "notinuse";
   var modules = res.handlers.site.getAvailableSidebarModules();
   var chooserValues = [];
   for (var i in modules) {
      chooserValues.push({value: modules[i].name, display: (modules[i].getHeader().length > 15 ? modules[i].getHeader().substring(0, 13) + ".." : modules[i].getHeader())});
   }

   return Html.dropDownAsString({name: id, id: id, size: 8, multiple: "multiple"}, chooserValues);
}


/**
 * preferences action for site
 */
function preferences_action() {
   if (res.handlers.site == null) res.redirect(this.href("main"));
   var mod = res.handlers.module = root.modules.get(req.data.mod);
   if (mod == null) res.redirect(this.href("main"));

   if (req.data.cancel) {
      res.redirect(path.site.modules.href("main"));
   } else if (req.data.preferencesform) {
      try {
         checkSecretKey();
         var result = this.evalPreferences(req.data, session.user, req.data.mod);
         res.message = result.toString();
         res.redirect(this.href("preferences") + "?mod=" + encodeURIComponent(req.data.mod));
      }
      catch (err) {
         res.message = err.toString();
      }
   }

   res.data.title = getMessage("Module.preferences.title", mod);
   res.data.action = this.href("preferences");
   res.push();
      mod.renderSetupSpacerLine();
      res.handlers.site.applyModuleMethod(res.handlers.module.name, "renderPreferences", {});
   res.data.modulePreferences = res.pop();
   if (!res.data.sidebar01) {
      var param = {
         header: mod.getTitle(),
         text:   mod.getDescription()
      };
      res.data.sidebar01 = res.handlers.module.renderSkinAsString(res.handlers.module.name + "PreferencesSidebar", param);
      // if no module preferences-sidebar-skin is defined, render standard-skin
      if (!res.data.sidebar01) {
         res.data.sidebar01 = res.handlers.module.renderSkinAsString("preferencesSidebar", param);
      }
   }

   res.data.body = res.handlers.module.renderSkinAsString("preferences");
   this.renderMgrPage();
}


/**
 * skins action for site
 */
function skins_action() {
   if (res.handlers.site == null) res.redirect(this.href("main"));
   var mod = res.handlers.module = root.modules.get(req.data.mod);
   if (mod == null) res.redirect(this.href("main"));
   res.data.title = getMessage("Module.skins.title", mod);
   res.data.body = res.handlers.module.renderSkinAsString("skins");
   res.data.sidebar02 = res.handlers.module.renderSkinAsString("skinsSidebar");
   this.renderMgrPage();
}


/**
 * render list of navigation modules
 */
function navigation_action() {
   if (!res.handlers.site) res.redirect(this.href("main"));

   res.data.title = getMessage("admin.menu.admin.modules.navigation");
   res.data.action = this.href();

   var modules = res.handlers.site.getNavigationModules();
   res.data.list = renderList(modules, "sitemgrlistitem", 40, req.data.page);
   res.data.pagenavigation = renderPageNavigation(modules, this.href("sidebar"), 40, req.data.page);
   res.data.body = this.renderSkinAsString("main");
   res.data.sidebar02 = this.renderSkinAsString("mainSidebar");
   this.renderMgrPage();
}


/**
 * render list of info modules
 */
function info_action() {
   if (!res.handlers.site) res.redirect(this.href("main"));

   res.data.title = getMessage("admin.menu.admin.modules.info");
   res.data.action = this.href();

   var modules = res.handlers.site.getInfoModules();
   res.data.list = renderList(modules, "sitemgrlistitem", 40, req.data.page);
   res.data.pagenavigation = renderPageNavigation(modules, this.href("sidebar"), 40, req.data.page);
   res.data.body = this.renderSkinAsString("main");
   res.data.sidebar02 = this.renderSkinAsString("mainSidebar");
   this.renderMgrPage();
}


/**
 * render list of externalContents modules
 */
function externalContents_action() {
   if (!res.handlers.site) res.redirect(this.href("main"));

   res.data.title = getMessage("admin.menu.admin.modules.externalContents");
   res.data.action = this.href();

   var modules = res.handlers.site.getExternalContentsModules();
   res.data.list = renderList(modules, "sitemgrlistitem", 40, req.data.page);
   res.data.pagenavigation = renderPageNavigation(modules, this.href("sidebar"), 40, req.data.page);
   res.data.body = this.renderSkinAsString("main");
   res.data.sidebar02 = this.renderSkinAsString("mainSidebar");
   this.renderMgrPage();
}


/**
 * render list of list modules
 */
function list_action() {
   if (!res.handlers.site) res.redirect(this.href("main"));

   res.data.title = getMessage("admin.menu.admin.modules.list");
   res.data.action = this.href();

   var modules = res.handlers.site.getListModules();
   res.data.list = renderList(modules, "sitemgrlistitem", 40, req.data.page);
   res.data.pagenavigation = renderPageNavigation(modules, this.href("sidebar"), 40, req.data.page);
   res.data.body = this.renderSkinAsString("main");
   res.data.sidebar02 = this.renderSkinAsString("mainSidebar");
   this.renderMgrPage();
}


/**
 * render list of other sidebar modules
 */
function others_action() {
   if (!res.handlers.site) res.redirect(this.href("main"));

   res.data.title = getMessage("admin.menu.admin.modules.others");
   res.data.action = this.href();

   var modules = res.handlers.site.getOtherModules();
   res.data.list = renderList(modules, "sitemgrlistitem", 40, req.data.page);
   res.data.pagenavigation = renderPageNavigation(modules, this.href("sidebar"), 40, req.data.page);
   res.data.body = this.renderSkinAsString("main");
   res.data.sidebar02 = this.renderSkinAsString("mainSidebar");
   this.renderMgrPage();
}
