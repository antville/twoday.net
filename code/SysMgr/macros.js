
/**
 * macro renders a select dropdown-box in 'search syslogs'
 */
function syslogShowDropdown_macro(param) {
   var options = [
      {value: "allLogs", display: "all Log-Entries"},
      {value: "siteLogs", display: "site Log-Entries"},
      {value: "userLogs", display: "user Log-Entries"},
      {value: "sysLogs", display: "system Log-Entries"}
   ];
   Html.dropDown({name: "show"}, options, req.data["show"]);
}


/**
 * macro renders a oder by dropdown-box for all search forms
 */
function orderDropdown_macro(param) {
   var options = [
      {value: "asc", display: "ascending"},
      {value: "desc", display: "descending"}
   ];
   Html.dropDown({name: "order"}, options, req.data["order"]);
}


/**
 * macro renders a keyword search-type dropdown-box for all search forms
 */
function keywordIsADropdown_macro(param) {
   var options = [
      {value: "substring", display: "substring"},
      {value: "exact", display: "exact"}/*,
      {value: "regexp", display: "regexp"}*/
   ];
   Html.dropDown({name: "keywordIsA"}, options, req.data["keywordIsA"]);
}


/**
 * macro renders a sorted by dropdown-box in 'search user'
 */
function userSortedByDropdown_macro(param) {
   var options = [
      {value: "lastOnline", display: "lastOnline"},
      {value: "registeredAt", display: "registeredAt"},
      {value: "username", display: "username"}
   ];
   Html.dropDown({name: "sort"}, options, req.data["sort"]);
}


/**
 * macro renders a select dropdown-box in 'search users'
 */
function userShowDropdown_macro(param) {
   var options = [
      {value: "allUsers", display: "all Users"},
      {value: "localUsers", display: "all local Users"},
      {value: "soruaUsers", display: "all sorua Users"},
      {value: "blockedUsers", display: "blocked Users"},
      {value: "trustedUsers", display: "trusted Users"},
      {value: "sysAdmins", display: "sysAdmins"},
      {value: "notConfirmed", display: "Users without confirmed Emails"}
   ];
   Html.dropDown({name: "show"}, options, req.data["show"]);
}


/**
 * macro renders a column dropdown-box in 'search users'
 */
function userColumnDropdown_macro(param) {
   var options = [
      {value: "username", display: "username"},
      {value: "email", display: "email"}
   ];
   Html.dropDown({name: "column"}, options, req.data["column"]);
}


/**
 * macro renders a select dropdown-box in 'search sites'
 */
function siteShowDropdown_macro(param) {
   var options = [
      {value: "allSites", display: "all Sites"},
      {value: "publicSites", display: "public Sites"},
      {value: "privateSites", display: "private Sites"},
      {value: "blockedSites", display: "blocked Sites"},
      {value: "trustedSites", display: "trusted Sites"},
      {value: "notpaidSites", display: "notpaid Sites"},
      {value: "notpaidSitesNext2Days", display: "notpaid within next 2 days"},
      {value: "notpaidSitesNext10Days", display: "notpaid within next 10 days"}
   ];
   Html.dropDown({name: "show"}, options, req.data["show"]);
}


/**
 * macro renders a sorted by dropdown-box in 'search user'
 */
function siteSortedByDropdown_macro(param) {
   var options = [
      {value: "lastUpdate", display: "last Update"},
      {value: "createTime", display: "createTime"},
      {value: "siteAlias", display: "siteAlias"},
      {value: "siteTitle", display: "siteTitle"},
      {value: "paiduntil", display: "paiduntil"},
      {value: "namelength", display: "name length"}
   ];
   Html.dropDown({name: "sort"}, options, req.data["sort"]);
}


/**
 * macro checks if there are any modules present
 * and if they need to be included in the system setup page
 */
function moduleSetup_macro(param) {
   this.applyAdminModuleMethods("renderSystemSetup", param);
   return;
}


/**
 * lists all available modules
 */
function moduleList_macro(param) {
   this.applyAdminModuleMethods("renderSetup", param);
   return;
}
