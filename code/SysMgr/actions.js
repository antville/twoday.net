
/**
 * main action
 */
function main_action() {
   res.redirect(this.href("status"));
}


/**
 * setup action
 */
function setup_action() {
   if (req.data.cancel)
      res.redirect(this.href("status"));
   else if (req.data.save) {
      try {
         checkSecretKey();
         res.message = this.evalSystemSetup(req.data, session.user);
         res.redirect(root.size() ? this.href("setup") : root.members.href("newsite"));
      } catch (err) {
         res.message = err.toString();
      }
   }

   res.data.title = getMessage("SysMgr.setup.title", {serverTitle: root.getTitle()});
   res.data.action = this.href(req.action);
   res.data.body = this.renderSkinAsString("setup");
   this.renderMgrPage();
   return;
}


/**
 * site maintenance action
 */
function sites_action() {
   res.data.title = getMessage("SysMgr.sites.title", {serverTitle: root.getTitle()});
   res.data.action = this.href(req.action);

   if (req.data.search || req.data.keywords) {
      checkSecretKey();
      root.manage.searchSites(req.data.show, req.data.sort, req.data.order, req.data.keywordIsA, req.data.keyword);
   } else if (req.data.cancel) {
      res.redirect(res.data.action + "?page=" + encodeURIComponent(req.data.page) + "#" + encodeURIComponent(req.data.item));
   } else if (req.data.remove && req.data.item) {
      checkSecretKey();
      var site = root.get(req.data.item);
      try {
         res.message = root.deleteSite(site);
         res.redirect(res.data.action + "?page=" + encodeURIComponent(req.data.page));
      } catch (err) {
         res.message = uneval(err);
      }
   } else if (req.data.save) {
      checkSecretKey();
      var result = this.updateSite(req.data, session.user);
      res.message = result;
      if (!result.error)
         res.redirect(res.data.action + "?page=" + encodeURIComponent(req.data.page) + "#" + encodeURIComponent(req.data.item));
   } else if (req.data.item) {
      req.data.selectedItem = root.get(req.data.item);
   }

   res.data.list = renderList(root.manage.sites, this.renderManagerView, 20, req.data.page);
   res.data.pagenavigation = renderPageNavigation(root.manage.sites, this.href(req.action), 20, req.data.page);
   res.data.body = this.renderSkinAsString("sitesearchform");
   res.data.body += this.renderSkinAsString("list");
   this.renderMgrPage();
   return;
}


/**
 * user maintenance action
 */
function users_action() {
   res.data.title = getMessage("SysMgr.users.title", {serverTitle: root.getTitle()});
   res.data.action = this.href(req.action);

   if (req.data.search || req.data.keyword) {
      checkSecretKey();
      root.manage.searchUsers(req.data.show, req.data.sort, req.data.order, req.data.column, req.data.keywordIsA, req.data.keyword);
   } else if (req.data.cancel) {
      res.redirect(res.data.action + "?page=" + encodeURIComponent(req.data.page) + "#" + encodeURIComponent(req.data.item));
   } else if (req.data.remove && req.data.item) {
      checkSecretKey();
      var user = User.getById(req.data.item);
      try {
         res.message = root.deleteUser(user);
         res.redirect(res.data.action + "?page=" + encodeURIComponent(req.data.page));
      } catch (err) {
         res.message = uneval(err);
      }
   } else if (req.data.save) {
      checkSecretKey();
      var result = this.updateUser(req.data, session.user);
      res.message = result;
      if (!result.error)
         res.redirect(res.data.action + "?page=" + encodeURIComponent(req.data.page) + "#" + encodeURIComponent(req.data.item));
   } else if (req.data.blockspamuser) {
      req.data.blocked = 1;
      checkSecretKey();
      var result = this.updateUser(req.data, session.user);
      this.blockSpamUser(req.data);
      res.message = result;
      if (!result.error)
         res.redirect(res.data.action + "?page=" + encodeURIComponent(req.data.page) + "#" + encodeURIComponent(req.data.item));
   } else if (req.data.item) {
      req.data.selectedItem = User.getById(req.data.item);
   }

   res.data.list = renderList(root.manage.users, this.renderManagerView, 20, req.data.page);
   res.data.pagenavigation = renderPageNavigation(root.manage.users, this.href(req.action), 20, req.data.page);
   res.data.body = this.renderSkinAsString("usersearchform");
   res.data.body += this.renderSkinAsString("list");
   this.renderMgrPage();
   return;
}


/**
 * log in as user from admin interface
 * 
 */
function loginAs_action() {
   if (!req.data.item || !User.getById(req.data.item)) {
      res.redirect(this.href("users"));
   }

   var user = User.getById(req.data.item);
   if (user.sysadmin) {
      res.redirect(this.href("users"));
   }
   try {
      user.doLogin();
      var redirectTarget = user.sites.count() > 0 ? user.sites.get(0) : root;
      res.redirect(redirectTarget.href());
   } catch (err) {
      res.message = err;
      res.redirect(this.href("users"));
   }
   return;
}


/**
 * action for displaying system logs
 */
function logs_action() {
   res.data.title = getMessage("SysMgr.logs.title", {serverTitle: root.getTitle()});
   res.data.action = this.href(req.action);

   if (req.data.search || req.data.keywords) {
      checkSecretKey();
      root.manage.searchSyslog(req.data.show, req.data.order, req.data.keyword);
   }

   res.data.list = renderList(root.manage.syslogs, this.renderManagerView, 20, req.data.page);
   res.data.pagenavigation = renderPageNavigation(root.manage.syslogs, this.href(req.action), 20, req.data.page);
   res.data.body = this.renderSkinAsString("syslogsearchform");
   res.data.body += this.renderSkinAsString("list");
   this.renderMgrPage();
   return;
}


/**
 * system status
 */
function status_action() {
   res.data.title = getMessage("SysMgr.status.title", {serverTitle: root.getTitle()});
   var status = new Object();
   status.upSince = formatTimestamp(new Date(app.upSince.getTime()), "long");
   status.activeThreads = app.activeThreads;
   status.maxThreads = app.maxThreads;
   status.freeThreads = app.freeThreads;
   status.requests = app.requestCount;
   status.errors = app.errorCount;
   status.xmlrpc = app.xmlrpcCount;
   status.cacheUsage = app.cacheusage;
   status.sessions = app.countSessions();
   status.totalMemory = Math.round(java.lang.Runtime.getRuntime().totalMemory() / 1024);
   status.freeMemory = Math.round(java.lang.Runtime.getRuntime().freeMemory() / 1024);
   status.usedMemory = status.totalMemory - status.freeMemory;
   res.data.body = this.renderSkinAsString("status", status);
   // render properties
   // statusSettings
   res.push();
   this.renderSkin("appPropertiesHead");
   for (var i in app.properties) {
      var param = {};
      param.key = i;
      param.value = app.properties[i].softwrap(20);
      // hide confidential data
      if (i.toLowerCase().match("passw") || i.toLowerCase().match("tdybilling") || i.toLowerCase().match("secret")){
         param.value = "***";
      }
      this.renderSkin("appPropertiesItem", param)
   }
   this.renderSkin("appPropertiesBottom");
   res.data.body += res.pop();
   this.renderMgrPage();
   return;
}


/**
 * Writes the stats.txt File to the Response
 */
function stats_txt_action() {
   res.contentType = "text/plain";
   res.forward("stats.txt");
   return;
}


/**
 * Ping action. Checks whether the server is up and running properly.
 * Returns Status Code 503 if an error occurs.
 */
function ping_action() {
   var dbc = getDBConnection("twoday");
   var rows = dbc.executeRetrieval("select count(*) from AV_SITE");
   if (!rows || !rows.next() || rows.getColumnItem("1") != root.count()) {
      res.servletResponse.sendError(503);
   } else {
      res.contentType = "text/plain";
      res.write("OK");
   }
   rows.release();
   res.push();
   if (root.count() > 0) {
      var obj = root.get(root.count() - 1);
   } else {
      var obj = root;
   }
   res.data.body = obj.renderSkinAsString("main");
   obj.main_action();
   var str = res.pop();
}


/**
 * Ping action, that indicates whether this Helma instance is
 * a master (or a slave) in a cluster-setup.
 */
function pingIsClusterMaster_action() {
   res.contentType = "text/plain";
   res.write(knallgrau.isHelmaClusterMaster());
}


/**
 * Clears the Object-Cache of the application
 */
function clearCache_action() {
   loadLocalization();
   app.clearCache();
   res.debug("DONE");
}


/**
 * Calls all merge* functions which are otherwise just called on startup.
 */
function mergeStatic_action() {
   mergeStaticDirs();
   res.debug("DONE");
}


/**
 * Special Function to fix a fillbuf-error
 * @see http://grazia.helma.org/pipermail/helma-dev/2005-March/001785.html
 */
function fixFillbuf_action() {
   if (req.data.site) {
      var dbc = getDBConnection("twoday");
      var sql = "SELECT SITE_ID FROM AV_SITE WHERE SITE_ALIAS='" + req.data.site + "'";
      var rows = dbc.executeRetrieval(sql);
      if (rows.next()) {
         var siteId = rows.getColumnItem("SITE_ID");
         var sql = "UPDATE AV_SITE SET SITE_PREFERENCES=REPLACE(SITE_PREFERENCES, \"</tagline>\", \" </tagline>\") where SITE_ID=" + siteId;
         dbc.executeCommand(sql);
         app.clearCache();
         res.debug("DONE");
      }
   }
   res.write("<form>Site Alias: <input name=site><input type=submit></form>");
}
