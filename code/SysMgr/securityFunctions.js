
/**
 * function is called at each request
 * and checks if user is logged in and is sysadmin
 */
function onRequest() {
   HopObject.prototype.onRequest.apply(this);
   if (!session.user && root.users.size() == 0)
      res.redirect(this.href("setup"));

   this.checkAccessSysMgr(req.action, session.user, null);

   // initialize sysmgr-object in session
   if (!session.data.mgr)
      session.data.mgr = new SysMgr();
}

/**
 * Check access to the SysMgr.
 */
function checkAccessSysMgr(action, usr) {
   if (action == "manage.css" || action == "ping" || action == "pingIsClusterMaster") return;
   checkIfLoggedIn(this.href(action));
   if (!usr.sysadmin) {
      res.message = new Exception("userNoSysAdmin");
      res.redirect(root.href());
   }
   var remoteHost = req.getHeader("X-Forwarded-For");
   if (!this.checkAddress(remoteHost)) {
      res.message = new Exception("userWrongIP", {ip: remoteHost});
      res.redirect(root.href());
   }
}


/**
 * check access to the manage-app by ip-addresses
 */
function checkAddress(remoteHost) {
   if (!remoteHost) {
      remoteHost = req.data.http_remotehost;
   }
   // if allowSysMgr value in server.properties has changed,
   // re-construct the addressFilter
   if (app.data.manageAddressString != getProperty("allowSysMgr")) {
      app.data.manageAddressFilter = this.createAddressFilter();
      app.data.manageAddressString = getProperty("allowSysMgr");
   }
   if (app.data.manageAddressFilter && !app.data.manageAddressFilter.matches(java.net.InetAddress.getByName(remoteHost))) {
      this.syslogs.add(new SysLog("system", null, "denied Request for System Manager for " + remoteHost, null));
      // forceStealth seems a bit like overkill here.
      // display a message that the ip address must be added to server.properties
      res.write("Access from address " + remoteHost + " denied.");
      return false;
   } else {
      return true;
   }
}
