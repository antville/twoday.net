
/**
 * Upgrade Patch for twoday_dbpatch-20050330-persistenRoot.sql
 *
 */
function patch20050330() {
   var dbDir = new Helma.File(app.__app__.appDir.toString(), "../../../db/" + app.name);
   if (!dbDir.exists()) {
      throw new Exception("no database directory found: " + dbDir.getAbsolutePath());
   }
   var rootFile = new Helma.File(dbDir, "0.xml");
   if (!rootFile.exists()) {
      throw new Exception("no 0.xml found");
   }
   app.log(" ********************** ");
   app.log(" ***   ROOT         *** ")
   app.log(" ********************** ");
   var str = rootFile.readAll();
   str = str.replace("Root", "HopObject");
   var rootXml = Xml.readFromString(str);
   for (var i in rootXml) {
      if (i == "modules") continue;
      if (i.toLowerCase() == "sys_layout")  {
         root.sys_layout = rootXml[i];
      } else if (i.toLowerCase() == "sys_frontsite") {
         root.sys_frontSite = rootXml[i];
      } else {
         app.log("set " + i + " to " + rootXml[i] + " for root");
         root.preferences.setProperty(i, rootXml[i]);
      }
   }
   rootFile.remove();
   app.log(" ********************** ");
   app.log(" ***   MODULES      *** ")
   app.log(" ********************** ");
   var files = dbDir.list();
   for (var f in files) {
      var name = files[f];
      var moduleFile = new Helma.File(dbDir, files[f]);
      if (name == "sessions" || name == "helma.xsl" || name == "idgen.xml" || name == "0.xml" || name == "1.xml" || name == "2.xml") {
         if (name == "1.xml" || name == "2.xml") moduleFile.remove();
         continue;
      }
      app.log("parsing " + name);
      try {
         var moduleXml = Xml.readFromString(moduleFile.readAll());
      } catch (err) {
         app.log(" ********************** ");
         app.log(" ***   ERROR " + name + "; processing continues");
         app.log(" ********************** ");
      }
      var moduleName = moduleXml["name"];
      if (root.modules.get("moduleName")) {
         throw new Exception("module " + moduleName + " is already there !?");
      }
      app.log("creating module " + moduleName);
      var m = new Module();
      for (var i in moduleXml) {
         m[i] = moduleXml[i];
         app.log("set " + i + " to " + moduleXml[i]);
      }
      app.log("persisting module " + moduleName);
      root.modules.add(m);
      moduleFile.remove();
   }
   return;
}
