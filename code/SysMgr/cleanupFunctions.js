
/**
 * function determines if it's time to start
 * automatic cleanup
 */
function autoCleanUp() {
   if (root.preferences.getProperty("sys_enableAutoCleanup")) {
      var startAtHour = root.preferences.getProperty("sys_startAtHour");
      var nextCleanup = new Date();
      nextCleanup.setDate(nextCleanup.getDate() + 1);
      nextCleanup.setHours((!isNaN(startAtHour) ? startAtHour : 0), 0, 0, 0);
      // check if it's time to run autocleanup
      if (!app.data.nextCleanup) {
         app.data.nextCleanup = nextCleanup;
         this.add(new SysLog("system", null, "autoCleanUp INFO: next cleanup scheduled for " + app.data.nextCleanup.format("EEEE, dd.MM.yyyy HH:mm"), null));
      } else if (new Date() >= app.data.nextCleanup) {
         this.syslogs.add (new SysLog("system", null, "autoCleanUp START", null));
         app.data.nextCleanup = nextCleanup;
         // now start the auto-cleanup-functions
         this.recordStats();
         this.cleanupAccesslog();
         this.blockInactiveSites();
         this.removeBlockedSites();
         this.removeUnconfirmedUsers();
         this.removeBlockedUsers();
         this.add(new SysLog("system", null, "autoCleanUp END: next cleanup scheduled for " + app.data.nextCleanup.format("EEEE, dd.MM.yyyy HH:mm"), null));
      }
   }
   return;
}


/**
 * Blocks sites that are inactive for too long (with the exception of trusted Sites).
 */
function blockInactiveSites() {
   var enable = root.preferences.getProperty("sys_blockInactiveSites");
   var afterDays = root.preferences.getProperty("sys_blockInactiveSitesAfterDays");
   if (!enable || !afterDays) return;
   var dbcon = getDBConnection("twoday");
   var sql = "SELECT SITE_ID FROM AV_SITE WHERE (SITE_ISTRUSTED IS NULL OR SITE_ISTRUSTED=0) AND " +
             " (SITE_TYPE='free' OR SITE_TDY_BILLING_PAIDUNTIL < DATE_SUB(NOW(), INTERVAL 30 DAY)) AND " +
             " (SITE_ISBLOCKED IS NULL OR SITE_ISBLOCKED=0) AND " +
             "( (SITE_LASTUPDATE IS NULL AND SITE_CREATETIME < DATE_SUB(NOW(), INTERVAL " + afterDays + " DAY)) OR " +
             "(SITE_LASTUPDATE IS NOT NULL AND SITE_LASTUPDATE < DATE_SUB(NOW(), INTERVAL " + afterDays + " DAY)) ) limit 50";
   var dbError = dbcon.getLastError();
   if (dbError) {
      app.log("Error establishing DB connection: " + dbError);
      return;
   }
   this.syslogs.add(new SysLog("system", null, "blockInactiveSites START", null));
   var dbres = dbcon.executeRetrieval(sql);
   if (!dbres) return;
   while (dbres.next()) {
      var s = root.getById(dbres.getColumnItem(1));
      if (!s) continue;
      root.blockSite(s);
      res.commit();
      sendMail(root.preferences.getProperty("sys_email"),
               s.email,
               getMessage("mail.blockInactiveSite", {siteName: s.getName()}),
               root.manage.renderSkinAsString("mailBlockInactiveSite", {siteUrl: s.href()})
              );
   }
   dbres.release();
   this.syslogs.add(new SysLog("system", null, "blockInactiveSites END", null));
   return true;
}


/**
 * Remove blocked Sites after a certain amount of time
 */
function removeBlockedSites() {
   var enable = root.preferences.getProperty("sys_removeBlockedSites");
   if (!enable) return;
   var dbcon = getDBConnection("twoday");
   // trigger final blog deletion when current date reaches site.lastdelwarn date
   var sql = "SELECT SITE_ID FROM AV_SITE WHERE SITE_ISBLOCKED=1 AND SITE_LASTDELWARN IS NOT NULL AND NOW() >= SITE_LASTDELWARN limit 50";
   var dbError = dbcon.getLastError();
   if (dbError) {
      app.log("Error establishing DB connection: " + dbError);
      return;
   }
   this.syslogs.add(new SysLog("system", null, "removeBlockedSites START", null));
   var dbres = dbcon.executeRetrieval(sql);
   if (!dbres) return;
   while (dbres.next()) {
      var u = root.getById(dbres.getColumnItem(1));
      if (!u) continue;
      var siteAlias = u.alias;
      root.deleteSite(u);
      this.syslogs.add(new SysLog("system", null, "removeBlockedSites Site " + siteAlias, null));
      res.commit();
   }
   dbres.release();
   this.syslogs.add(new SysLog("system", null, "removeBlockedSites END", null));
   return true;
}


/**
 * Remove Users who did not confirm their email address within a certain amount of time
 */
function removeUnconfirmedUsers() {
   var enable = root.preferences.getProperty("sys_removeUnconfirmedUsers");
   var afterDays = root.preferences.getProperty("sys_removeUnconfirmedUsersAfterDays");
   if (!enable || !afterDays) return;
   var dbcon = getDBConnection("twoday");
   var sql = "SELECT USER_ID FROM AV_USER WHERE USER_TYPE = 'USER_LOCAL' AND (USER_ISTRUSTED IS NULL OR USER_ISTRUSTED=0) AND (USER_EMAIL_ISCONFIRMED IS NULL OR USER_EMAIL_ISCONFIRMED=0) AND USER_EMAIL_LASTCONFIRMED IS NULL AND USER_REGISTERED < DATE_SUB(NOW(), INTERVAL " + afterDays + " DAY) limit 50";
   var dbError = dbcon.getLastError();
   if (dbError) {
      app.log("Error establishing DB connection: " + dbError);
      return;
   }
   this.syslogs.add(new SysLog("system", null, "removeUnconfirmedUsers START", null));
   var dbres = dbcon.executeRetrieval(sql);
   if (!dbres) return;
   while (dbres.next()) {
      var u = root.users.getById(dbres.getColumnItem(1));
      if (!u) continue;
      var userName = u.name;
      root.deleteUser(u);
      this.syslogs.add(new SysLog("system", null, "removeUnconfirmedUsers User " + userName, null));
      res.commit();
   }
   dbres.release();
   this.syslogs.add(new SysLog("system", null, "removeUnconfirmedUsers END", null));
   return true;
}


/**
 * Remove blocked Users after a certain amount of time
 */
function removeBlockedUsers() {
   var enable = root.preferences.getProperty("sys_removeBlockedUsers");
   var afterDays = root.preferences.getProperty("sys_removeBlockedUsersAfterDays");
   if (!enable || !afterDays) return;
   var dbcon = getDBConnection("twoday");
   var sql = "SELECT USER_ID FROM AV_USER WHERE (USER_ISTRUSTED IS NULL OR USER_ISTRUSTED=0) AND USER_ISBLOCKED=1 AND USER_BLOCKTIME IS NOT NULL AND USER_BLOCKTIME < DATE_SUB( NOW(), INTERVAL " + afterDays + " DAY) limit 50";
   var dbError = dbcon.getLastError();
   if (dbError) {
      app.log("Error establishing DB connection: " + dbError);
      return;
   }
   this.syslogs.add(new SysLog("system", null, "removeBlockedUsers START", null));
   var dbres = dbcon.executeRetrieval(sql);
   if (!dbres) return;
   while (dbres.next()) {
      var u = root.users.getById(dbres.getColumnItem(1));
      if (!u) continue;
      var userName = u.name;
      root.deleteUser(u);
      this.syslogs.add(new SysLog("system", null, "removeBlockedUsers User " + userName, null));
      res.commit();
   }
   dbres.release();
   this.syslogs.add(new SysLog("system", null, "removeBlockedUsers END", null));
   return true;
}


/**
 * function deletes all accesslog-records older than 48 hours
 * and with story-id = null
 */
function cleanupAccesslog() {
   var dbConn = getDBConnection("twoday");
   var dbError = dbConn.getLastError();
   if (dbError) {
      this.syslogs.add (new SysLog("system", null, "failed to clean up accesslog-table!", null));
      return;
   }
   // remove all site-referrers without any specific story associated after 2 days
   var threshold = new Date();
   threshold.setDate(threshold.getDate() -2);
   var query = "delete from AV_ACCESSLOG where ACCESSLOG_F_TEXT is null and ACCESSLOG_DATE < '" + threshold.format("yyyy-MM-dd HH:mm:ss") + "'";
   var delRows = dbConn.executeCommand(query);
   if (delRows)
      this.syslogs.add (new SysLog("system", null, "removed " + delRows + " records from accesslog-table", null));
   // remove all story-referrers after 30 days
   var threshold = new Date();
   threshold.setDate(threshold.getDate() -30);
   var query = "delete from AV_ACCESSLOG where ACCESSLOG_DATE < '" + threshold.format("yyyy-MM-dd HH:mm:ss") + "'";
   var delRows = dbConn.executeCommand(query);
   if (delRows)
      this.syslogs.add (new SysLog("system", null, "removed " + delRows + " story accesses from accesslog-table", null));
   return;
}
