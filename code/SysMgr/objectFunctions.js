
/**
 * Performs necessary tasks when installing the application.
 */
function onInstallation() {
   // initialize and activate all modules in INITMODULESONSETUP (standard-modules for sidebar, etc.)
   for (i in INITMODULESONSETUP) {
      root.modules.loadModule(INITMODULESONSETUP[i]);
      var mod = root.modules.get(INITMODULESONSETUP[i]);
      if (mod) {
         mod.evalInitialize(true);
         mod.isActivated = true;
      }
   }
   root.preferences.setProperty("modSoruaUserServerEnabled", true);
   root.preferences.setProperty("modSoruaUserServerAcceptUnknown", true);
   root.preferences.setProperty("modSoruaAuthServerEnabled", true);
   root.preferences.setProperty("modSoruaUserServerKnownAuthServers", "http://www.twoday.net/members/modSoruaAuthServer;twoday.net");
   root.preferences.setProperty("sys_limitNewSites", "1");
   return;
}


/**
 * constructor-function
 */
function constructor() {
   this.searchSites();
   this.searchUsers();
   this.searchSyslog();
}


/**
 * function manipulates the sites-collection
 */
function searchSites(show, sort, order, keywordIsA, keyword) {
   // construct the sql-clause for manual subnodeRelation
   var sql = "";
   if (show == "publicSites")
      sql += "WHERE SITE_ISONLINE=1 AND SITE_ISBLOCKED=0 ";
   else if (show == "privateSites")
      sql += "WHERE SITE_ISONLINE=0 AND SITE_ISBLOCKED=0 ";
   else if (show == "blockedSites")
      sql += "WHERE SITE_ISBLOCKED=1 ";
   else if (show == "trustedSites")
      sql += "WHERE SITE_ISTRUSTED=1 ";
   else if (show == "notpaidSites")
      sql += "WHERE SITE_TDY_BILLING_PAIDUNTIL < NOW() AND SITE_TYPE<>'beta' AND SITE_TYPE<>'free'";
   else if (show == "notpaidSitesNext2Days")
      sql += "WHERE ( SITE_TDY_BILLING_PAIDUNTIL < DATE_ADD( NOW(), INTERVAL 2 DAY) AND SITE_TDY_BILLING_PAIDUNTIL > NOW() ) AND SITE_TYPE<>'beta' AND SITE_TYPE<>'free'";
   else if (show == "notpaidSitesNext10Days")
      sql += "WHERE ( SITE_TDY_BILLING_PAIDUNTIL < DATE_ADD( NOW(), INTERVAL 10 DAY) AND SITE_TDY_BILLING_PAIDUNTIL > NOW() ) AND SITE_TYPE<>'beta' AND SITE_TYPE<>'free'";

   if (keyword) {
      // additional keywords are given, so we're using them
      if (keywordIsA == "substring") { // doing normal keyword-search
         var kArray = stripTags(keyword).split(" ");
         for (var i in kArray) {
            var k = kArray[i];
            sql += sql.length > 0 ? "AND " : "WHERE ";
            sql += "SITE_ALIAS REGEXP '" + keyword + "' OR SITE_TITLE REGEXP '" + keyword + "' ";
         }
      } else if (keywordIsA == "exact") { // searching for email-addresses
         sql += sql.length > 0 ? "AND " : "WHERE ";
         sql += "SITE_ALIAS = '" + keyword + "' OR SITE_TITLE = '" + keyword + "' ";
      } else if (keywordIsA= "regexp") { // regular expression
       sql += sql.length > 0 ? "AND " : "WHERE ";
         sql += "(SITE_ALIAS LIKE '%" + k + "%' OR SITE_TITLE LIKE '%" + k + "%') ";
      }
   }
   if (!sort || sort == "lastUpdate")
      sql += "ORDER BY SITE_LASTUPDATE ";
   else if (sort == "createTime")
      sql += "ORDER BY SITE_CREATETIME ";
   else if (sort == "siteAlias")
      sql += "ORDER BY SITE_ALIAS ";
   else if (sort == "siteTitle")
      sql += "ORDER BY SITE_TITLE ";
   else if (sort == "paiduntil")
      sql += "ORDER BY SITE_TDY_BILLING_PAIDUNTIL ";
   else if (sort == "namelength")
      sql += "ORDER BY LENGTH(SITE_ALIAS) ";
   if (!order || order == "desc")
      sql += "desc ";
   else if (order == "asc")
      sql += "asc ";

   // now do the actual search with a manual subnodeRelation
   this.sites.subnodeRelation = sql;
   return;
}


/**
 * function manipulates the users-collection
 */
function searchUsers(show, sort, order, column, keywordIsA, keyword) {
   // construct the sql-clause for manual subnodeRelation
   var sql = "";
   var column = (column == "email") ? "USER_EMAIL" : "USER_NAME";
   if (show == "blockedUsers")
      sql += "WHERE USER_ISBLOCKED=1 ";
   else if (show == "localUsers")
      sql += "WHERE USER_TYPE='USER_LOCAL' ";
   else if (show == "soruaUsers")
      sql += "WHERE USER_TYPE='USER_SORUA' ";
   else if (show == "trustedUsers")
      sql += "WHERE USER_ISTRUSTED=1 ";
   else if (show == "sysAdmins")
      sql += "WHERE USER_ISSYSADMIN=1 ";
   else if (show == "notConfirmed")
      sql += "WHERE USER_EMAIL_ISCONFIRMED IS NULL ";
   if (keyword) {
      // additional keywords are given, so we're using them
      if (keywordIsA == "substring") { // doing normal keyword-search
         var kArray = stripTags(keyword).split(" ");
         for (var i in kArray) {
            var k = kArray[i];
            sql += sql.length > 0 ? "AND " : "WHERE ";
            sql += column + " LIKE '%" + k + "%' ";
         }
      } else if (keywordIsA == "exact") { // searching for email-addresses
         sql += sql.length > 0 ? "AND " : "WHERE ";
         sql += column + " = '" + keyword + "' ";
      } else if (keywordIsA= "regexp") { // regular expression
         sql += sql.length > 0 ? "AND " : "WHERE ";
         sql += column + " REGEXP '" + keyword + "' ";
      }
   }
   if (!sort || sort == "lastOnline")
      sql += "ORDER BY USER_LASTVISIT ";
   else if (sort == "registeredAt")
      sql += "ORDER BY USER_REGISTERED ";
   else if (sort == "username")
      sql += "ORDER BY USER_NAME ";
   if (!order || order == "desc")
      sql += "desc ";
   else if (order == "asc")
      sql += "asc ";

   // now do the actual search with a manual subnodeRelation
   this.users.subnodeRelation = sql;
   return;
}


/**
 * function manipulates the syslogs-collection
 */
function searchSyslog(show, order, keyword) {
   // construct the sql-clause for manual subnodeRelation
   var sql = "";
   if (show == "siteLogs")
      sql += "WHERE SYSLOG_TYPE = 'site' ";
   else if (show == "userLogs")
      sql += "WHERE SYSLOG_TYPE = 'user' ";
   else if (show == "sysLogs")
      sql += "WHERE SYSLOG_TYPE = 'system' ";
   if (keyword) {
      // additional keywords are given, so we're using them
      var kArray = stripTags(keyword).split(" ");
      for (var i in kArray) {
         var k = kArray[i];
         sql += sql.length > 0 ? "AND " : "WHERE ";
         sql += "(SYSLOG_OBJECT LIKE '%" + k + "%' OR SYSLOG_ENTRY LIKE '%" + k + "%') ";
      }
   }
   if (!order || order == "desc")
      sql += "ORDER BY SYSLOG_CREATETIME desc, SYSLOG_ID desc ";
   else if (order == "asc")
      sql += "ORDER BY SYSLOG_CREATETIME asc, SYSLOG_ID asc ";

   // now do the actual search with a manual subnodeRelation
   this.syslogs.subnodeRelation = sql;
   return;
}


/**
 * function stores updated site-preferences
 */
function updateSite(param, admin) {
   var site = root.get(param.item);
   if (!site)
      throw new Exception("siteEditMissing");
   var trust = parseInt(param.trusted, 10);
   var block = parseInt(param.blocked, 10);
   var online = parseInt(param.online, 10);
   var url = param.url;
   if (trust > site.trusted) {
      this.syslogs.add(new SysLog("site", site.alias, "granted trust", admin));
   } else if (trust < site.trusted) {
      this.syslogs.add(new SysLog("site", site.alias, "revoked trust", admin));
   }
   if (block > site.blocked) {
      root.blockSite(site);
   } else if (block < site.blocked) {
      root.unblockSite(site);
   }

   if (url != site.url)
      this.syslogs.add(new SysLog("site", site.alias, "changing url from " + site.url + " to " + url, admin));
   site.trusted = trust;
   if (site.online != online) site.toggleOnline(online ? "online" : "offline");
   site.url = url;
   return new Message("update");
}


/**
 * function stores updated user
 */
function updateUser(param, admin) {
   var u = User.getById(param.item);
   if (!u)
      throw new Exception("userEditMissing");
   if (u == admin)
      throw new Exception("accountModifyOwn");
   u.changeEmail(param.email);
   // check if this is an attempt to remove the last sysadmin
   var sysadmin = parseInt(param.sysadmin, 10);
   var trust = parseInt(param.trusted, 10);
   var block = parseInt(param.blocked, 10);
   if (u.sysadmin && this.sysadmins.size() == 1)
      throw new Exception("adminDeleteLast");
   else {
      //logging
      if (sysadmin > u.sysadmin)
         this.syslogs.add(new SysLog("user", u.name, "granted sysadmin-rights", admin));
      else if (sysadmin < u.sysadmin)
         this.syslogs.add(new SysLog("user", u.name, "revoked sysadmin-rights", admin));
      u.sysadmin = sysadmin;
   }
   if (trust > u.trusted)
      this.syslogs.add(new SysLog("user", u.name, "granted trust", admin));
   else if (trust < u.trusted)
      this.syslogs.add(new SysLog("user", u.name, "revoked trust", admin));
   if (block > u.blocked) {
      root.blockUser(u);
   } else if (block < u.blocked) {
      root.unblockUser(u);
   }
   u.trusted = trust;
   return new Message("update");
}


/**
 * function checks if the system parameters are correct
 */
function evalSystemSetup(param, admin) {
   root.preferences.setProperty("sys_title", param.preferences_sys_title);
   // global ip filter
   root.preferences.setProperty("sys_ipfilter", param.preferences_sys_ipfilter);
   root.preferences.setProperty("sys_tagline", param.preferences_sys_tagline);
   if (param.sys_frontSite) {
      var s = root.get(param.sys_frontSite);
      if (!s)
         throw new Exception("systemFrontsiteMissing");
      root.sys_frontSite = s;
   } else {
      root.sys_frontSite = null;
   }
   // check system email
   if (!param.preferences_sys_email)
      throw new Exception("systemEmailMissing");
   evalEmail(param.preferences_sys_email);
   root.preferences.setProperty("sys_email", param.preferences_sys_email);
   // store selected locale in this.language and this.country
   if (param.locale) root.setLocale(param.locale);
   root.preferences.setProperty("sys_timezone", param.timezone);
   root.cache.timezone = null;
   // long dateformat
   root.preferences.setProperty("longdateformat", param.longdateformat ? param.longdateformat : null);
   // short dateformat
   root.preferences.setProperty("shortdateformat", param.shortdateformat ? param.shortdateformat : null);
   // disk quota
   root.preferences.setProperty("sys_diskQuota", (param.preferences_sys_diskQuota != null && param.preferences_sys_diskQuota != "") ? parseInt(param.preferences_sys_diskQuota, 10) : null);

   // limiting site-creation
   root.preferences.setProperty("sys_limitNewSites", param.preferences_sys_limitNewSites ? parseInt(param.preferences_sys_limitNewSites, 10) : null);
   root.preferences.setProperty("sys_minMemberAge", param.preferences_sys_minMemberAge ? parseInt(param.preferences_sys_minMemberAge, 10) : null);
   root.preferences.setProperty("sys_minMemberSince", param.preferences_sys_minMemberSince ? param.preferences_sys_minMemberSince.toDate("yyyy-MM-dd HH:mm", root.getTimeZone()) : null);
   root.preferences.setProperty("sys_waitAfterNewSite", param.preferences_sys_waitAfterNewSite ? parseInt(param.preferences_sys_waitAfterNewSite, 10) : null);
   root.preferences.setProperty("sys_limitSitesPerUser", param.preferences_sys_limitSitesPerUser ? parseInt(param.preferences_sys_limitSitesPerUser, 10) : null);

   // auto-cleanup
   root.preferences.setProperty("sys_enableAutoCleanup", param.preferences_sys_enableAutoCleanup ? true : false);
   root.preferences.setProperty("sys_startAtHour", parseInt(param.preferences_sys_startAtHour, 10));

   // auto-block
   root.preferences.setProperty("sys_blockInactiveSites", param.preferences_sys_blockInactiveSites ? true : false);
   root.preferences.setProperty("sys_blockInactiveSitesAfterDays", param.preferences_sys_blockInactiveSitesAfterDays ? parseInt(param.preferences_sys_blockInactiveSitesAfterDays, 10) : null);

   // auto-removal
   root.preferences.setProperty("sys_removeBlockedSites", param.preferences_sys_removeBlockedSites ? true : false);
   root.preferences.setProperty("sys_removeBlockedSitesAfterDays", param.preferences_sys_removeBlockedSitesAfterDays ? parseInt(param.preferences_sys_removeBlockedSitesAfterDays, 10) : null);
   root.preferences.setProperty("sys_removeBlockedUsers", param.preferences_sys_removeBlockedUsers ? true : false);
   root.preferences.setProperty("sys_removeBlockedUsersAfterDays", param.preferences_sys_removeBlockedUsersAfterDays ? parseInt(param.preferences_sys_removeBlockedUsersAfterDays, 10) : null);
   root.preferences.setProperty("sys_removeUnconfirmedUsers", param.preferences_sys_removeUnconfirmedUsers ? true : false);
   root.preferences.setProperty("sys_removeUnconfirmedUsersAfterDays", param.preferences_sys_removeUnconfirmedUsersAfterDays ? parseInt(param.preferences_sys_removeUnconfirmedUsersAfterDays, 10) : null);

   // set the default layout
   if (param.layout)
      root.layouts.setDefaultLayout(param.layout);
   // call the evalSystemSetup method of every module
   this.applyAdminModuleMethods("evalSystemSetup", param);

   // add a new entry in system-log
   this.syslogs.add(new SysLog("system", null, "changed system setup", session.user));
   return new Message("systemUpdate");
}


/**
 * Adds a new row filled with statistics into a tab separated file named protectedStaticPath/stats.txt
 * Can/should be called every day
 */
function recordStats() {
   var siteCountSQL    = "select count(*) from AV_SITE";
   var storyCountSQL   = "select count(*) from AV_TEXT where TEXT_PROTOTYPE='story'";
   var commentCountSQL = "select count(*) from AV_TEXT where TEXT_PROTOTYPE='comment'";
   var imageCountSQL   = "select count(*) from AV_IMAGE where IMAGE_F_IMAGE_PARENT IS NULL";
   var fileCountSQL    = "select count(*) from AV_FILE";
   var diskUsageSQL    = "select sum(SITE_DISKUSAGE) from AV_SITE";
   var userCountSQL    = "select count(*) from AV_USER";
   var skinCountSQL    = "select count(*) from AV_SKIN";
   var accessCountSQL  = "select count(*) from AV_ACCESSLOG";
   var activeSitesSQL  = "select count(*) from AV_SITE where (TO_DAYS(NOW()) - TO_DAYS(SITE_LASTUPDATE) <= 28)";
   var activeUsersSQL  = "select count(*) from AV_USER where (TO_DAYS(NOW()) - TO_DAYS(USER_LASTVISIT) <= 28)";

   var date = (new Date()).format("yyyy-MM-dd");
   var fields = ["userCount", "siteCount", "storyCount", "commentCount",
                 "imageCount", "fileCount", "diskUsage", "skinCount",
                 "accessCount", "activeSites", "activeUsers"];
   var vals = new Array();

   var dbcon = getDBConnection ("twoday");
   for (var i=0; i<fields.length; i++) {
      var dbres = dbcon.executeRetrieval(eval(fields[i] + "SQL"));
      vals[i] = (dbres && dbres.next()) ? dbres.getColumnItem(1).toString() : "";
      dbres.release();
   }

   var line = date + "\t";
   for (var v in vals) line += vals[v] + "\t";
   var f = new Helma.File(getProperty("protectedStaticPath"), "stats.txt");
   if (f.exists()) {
      var str = f.readAll() + "\n" + line;
      f.remove();
   } else
      var str = line;
   f.open();
   f.write(str);
   f.close();
   return;
}


/**
  * initializes addressFilter from app.properties,
  * hostnames are converted, wildcards are only allowed in ip-addresses
  * (so, no network-names, sorry)
  */
function createAddressFilter() {
   var filter = new Packages.helma.util.InetAddressFilter();
   var str = getProperty("allowSysMgr");
   if ( str!=null && str!="" ) {
      var arr = str.split(",");
      for ( var i in arr ) {
         var str = new java.lang.String(arr[i]);
         try {
             filter.addAddress(str.trim());
         } catch (a) {
             try {
                 var str = java.net.InetAddress.getByName(str.trim()).getHostAddress();
                 filter.addAddress(str);
             } catch (b) {
                 app.log("error using address " + arr[i] + ": " + b);
             }
         }
      }
      this.syslogs.add(new SysLog("system", null, "allowed IP addresses for System Manager: " + getProperty("allowSysMgr"), null));
   } else {
      filter = null;
      this.syslogs.add(new SysLog("system", null, "all IP addresses will be allowed for System Manager", null));
   }
   return filter;
}

/**
 * function blocks all Sites and Comments of an User
 */
function blockSpamUser(param) {
   var u = User.getById(param.item);
   if (!u)
      throw new Exception("userEditMissing");

   // loop through sites
   var siteCount = u.sites.count();
   for (var i = 0; i < siteCount; i++) {
      var site = u.sites.get(i);
      root.blockSite(site);
   }
   
   // loop over comments
   var commentCount = u.comments.count();
   for (var i = 0; i < commentCount; i++) {
      var comment = u.comments.get(i);
      comment.doSetOffline();
   }
}

