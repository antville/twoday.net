
/**
 * display all images of a site or layout
 */
function main_action() {
   res.data.imagelist = renderList(this.getImportantImages(), "mgrlistitem", 20, req.data.page);
   res.data.pagenavigation = renderPageNavigation(this.getImportantImages(), this.href(), 20, req.data.page);
   res.data.title = getMessage("LayoutImageMgr.main.title", {layoutTitle: this._parent.title});
   res.data.body = this.renderSkinAsString("main");
   res.data.sidebar01 = this.renderSkinAsString("mainSidebar");
   this.renderMgrPage();
   return;
}


/**
 * display all images of a site or layout
 */
function all_action() {
   res.data.imagelist = renderList(this.mergeImages(), "mgrlistitem", 20, req.data.page);
   res.data.pagenavigation = renderPageNavigation(this.mergeImages(), this.href("all"), 20, req.data.page);
   res.data.title = getMessage("LayoutImageMgr.main.title", {layoutTitle: this._parent.title});
   res.data.body = this.renderSkinAsString("main");
   res.data.sidebar01 = this.renderSkinAsString("mainSidebar");
   this.renderMgrPage();
   return;
}


/**
 * display the images of the parent layout
 */
function default_action() {
   if (!this._parent.parent) {
      res.message = new Exception("layoutNoParent");
      res.redirect(this.href());
   }
   res.data.imagelist = renderList(this._parent.parent.images, "mgrlistitem", 20, req.data.page);
   res.data.pagenavigation = renderPageNavigation(this._parent.parent.images, this.href(req.action), 20, req.data.page);
   res.data.title = getMessage("LayoutImageMgr.default.title", {parentLayoutTitle: this._parent.parent.title});
   res.data.body = this.renderSkinAsString("main");
   res.data.sidebar01 = this.renderSkinAsString("mainSidebar");
   this.renderMgrPage();
   return;
}


/**
 * display the images of this layout
 */
function additional_action() {
   res.data.imagelist = renderList(this, "mgrlistitem", 20, req.data.page);
   res.data.pagenavigation = renderPageNavigation(this, this.href(req.action), 20, req.data.page);
   res.data.title = getMessage("LayoutImageMgr.additional.title", {layoutTitle: this._parent.title});
   res.data.body = this.renderSkinAsString("main");
   res.data.sidebar01 = this.renderSkinAsString("mainSidebar");
   this.renderMgrPage();
   return;
}


/**
 * action for creating new Image objects
 */
function create_action() {
   if (req.data.cancel) {
      res.redirect(this.href());
   } else if (req.data.save) {
      // check if a url has been passed instead of a file upload. hw
      if ((!req.data.rawimage || req.data.rawimage.contentLength == 0) && req.data.url)
          req.data.rawimage = getURL(req.data.url);
      try {
         checkSecretKey();
         var result = this.evalImg(req.data, session.user);
         res.message = result.toString();
         session.data.referrer = null;
         res.redirect(this.href("all"));
      } catch (err) {
         res.message = err.toString();
      }
   }

   if (!req.data["resizeto"]) req.data["resizeto"] = "no";

   res.data.action = this.href(req.action);
   res.data.title = getMessage("LayoutImageMgr.create.title", {parentTitle: this._parent.title});
   res.data.body = this.renderSkinAsString("new");
   res.data.sidebar01 = this.renderSkinAsString("newSidebar");
   this.renderMgrPage();
   return;
}
