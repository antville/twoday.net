
/**
 * Permission checks (called by hopobject.onRequest())
 *
 * @param action  String, name of action
 * @param usr     User, who wants to access an action
 * @param level   Int, Membership level
 * @return Object if permission was denied <br />
 *   .message  message, if something went wrong <br />
 *   .url      recommanded redirect URL
 * @doclevel public
 */
function checkAccess(action, usr, level) {
   if (usr && usr.sysadmin) return;
   var url;
   var site = this._parent._parent._parent && this._parent._parent._parent._prototype == "Site" ? this._parent._parent._parent : null;
   try {
      if (site && !site.online) {
         checkIfLoggedIn(this.href(action));
         site.checkView(usr, level);
      }
      switch (action) {
         case "main" :
         case "all" :
         case "default" :
         case "additional" :
         case "create" :
            this.checkAdd(usr, level);
            break;
         default :
            url = this.applyAllModuleMethods("checkLayoutImageMgrAccess", [action, usr, level, url]);
            break;
      }
   } catch (deny) {
      return {message: deny.toString(), url: (url ? url : (site ? site.href() : root.href()))};
   }
   return;
}


/**
 * check if user is allowed to add pictures
 * (overwrites ImageMgr.checkAdd())
 * @see ImageMgr/securityFunctions.js
 * @param Obj Userobject
 * @param Int Permission-Level
 * @return String Reason for denial (or null if allowed)
 */
function checkAdd(usr, level) {
   if (this._parent.site && !usr.sysadmin) {
      if ((level & MEMBER_MAY_EDIT_LAYOUTS) == 0)
         throw new DenyException("layoutEdit");
   } else if (!usr.sysadmin)
      throw new DenyException("imageAdd");
   return;
}
