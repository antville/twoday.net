

/**
 * returns an Array of list navigation items
 * @param param Object   passed by Macro
 * @return Array
 */
function getListNavigationItems() {
   var items = [
      {
         text: getMessage("admin.menu.admin.layout.images.important"),
         action: "LayoutImageMgr/main"
      },
      {
         text: getMessage("admin.menu.admin.layout.images.all"),
         action: "LayoutImageMgr/all"
      },
      {
         text: getMessage("admin.menu.admin.layout.images.additional"),
         action: "LayoutImageMgr/additional"
      }
   ];
   return items;
}


/**
 * render list of LayoutImage tasks
 */
function layoutImageTasks_macro(param) {
   var containerParam = Object.clone(param);
   var itemParam = Object.clone(param);
   var layoutImageMgr = this;

   var items = [
      {
         text: getMessage("LayoutImageMgr.create.taskLabel"),
         href: layoutImageMgr.href("create"),
         isActive: "LayoutImageMgr/create"
      }
   ];

   res.push();
   this.renderTaskItems(items, param);
   containerParam.items = res.pop();

   this.renderSkin("taskContainer", containerParam);
   return;
}
