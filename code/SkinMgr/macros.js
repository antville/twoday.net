/**
 * list the (most important) macros
 * available for a specific skin of a
 * prototype (except Global)
 * @see this.globalmacros_macro
 * FIXME: needs improvement
 */
function macros_macro(param) {
   this.renderMacroList(param);
   return;
}


/**
 * list macros available in a global
 * skin of a prototype
 * @see this.macros_macro
 * FIXME: needs improvement
 */
function globalmacros_macro(param) {
   param.proto = "Global";
   param.includeGlobal = true;
   this.renderMacroList(param);
   return;
}


/**
 * list skin-specific macros (param,
 * response etc.) of a skin
 */
function skinmacros_macro(param) {
   if (!req.data.key)
      return;
   var parts = req.data.key.split(".");
   if (!HELP.skins[parts[0]])
      return;
   if (!HELP.skins[parts[0]][parts[1]])
      return;
   var macros = HELP.skins[parts[0]][parts[1]];
   macros.sort();
   if (!param.itemprefix)
      param.itemprefix = "";
   if (!param.itemsuffix)
      param.itemsuffix = "<br />";
   for (var i in macros) {
      res.write(param.itemprefix);
      res.encode("<% ");
      res.write(macros[i]);
      res.encode(" %>");
      res.write(param.itemsuffix);
   }
   return;
}


/**
 * renders a dropdown containing available
 * prototypes
 */
function prototypechooser_macro(param) {
   var options = [];
   for (var i in app.skinfiles)
      options.push({value: i, display: i});
   options.sort(function(a, b) {return a.display.charCodeAt(0) - b.display.charCodeAt(0); });
   Html.dropDown({name: "prototype"}, options, null, param.firstOption);
   return;
}


/**
 *
 */
function languageRootSkin_macro(param) {
   return;
   if (param["for"] == "editor") {
      var key = req.data.key.split(".");
      var proto = key[0];
      var name = key[1];
      var layout = (req.data.preferedRootLocale) ? path.layout.getLocalization(req.data.preferedRootLocale, true) : path.layout.getLocaleParent();
      var skinSource = layout.skins.getSkinSource(proto, name);
      var skin = layout.skins.getSkinInPath(proto, name);

      res.write('Try to compare to ');
      path.layout.localizations_macro({as: "editor", onchange: "this.form.submit()"});
      res.write('<br />');

      res.write('Localization shown: ');
      var locale = (skin) ? skin.layout.locale : root.preferences.getProperty("sys_language") + (root.preferences.getProperty("sys_country") ? "_" + root.preferences.getProperty("sys_country") : "");
      res.write('<b>' + convertStringToLocale(skin.layout.locale).getDisplayName() + '</b>');
      if (req.data.preferedLocaleParent && locale != req.data.preferedLocaleParent) {
         res.write(', ' + convertStringToLocale(req.data.preferedLocaleParent).getDisplayName() + ' is not available.');
      }
      res.write('<br />');

      res.write('<textarea name="skin" cols="50" rows="20" wrap="virtual" disabled="disabled" class="formWide">')
      res.write(encodeForm(skinSource));
      res.write('</textarea>');
   }
   return;
}


/**
 * returns an Array of list navigation items
 * @param param Object   passed by Macro
 * @return Array
 */
function getListNavigationItems() {
   return [
      {
         text: getMessage("admin.menu.admin.layout.skins.main"),
         action: "SkinMgr/main"
      },
      {
         text: getMessage("admin.menu.admin.layout.skins.default"),
         action: "SkinMgr/all"
      },
      {
         text: getMessage("admin.menu.admin.layout.skins.modified"),
         action: "SkinMgr/modified"
      }
   ];
}


/**
 * returns an Array of SkinManager tasks
 * @param param Object   passed by Macro
 * @return Array
 */
function getSkinManagerTasks(param) {
   var items = [
      {
         text: getMessage("SkinMgr.createCustom.title", {layoutTitle: this._parent.title}),
         href: this.href("create"),
         action: "SkinMgr/create"
      }
   ];
   return items;
}


/**
 * render list of SkinManager tasks
 */
function skinManagerTasks_macro(param) {
   var items = this.getSkinManagerTasks(param);
   this.renderTasks(items, param);
}


/**
 * render a list item for a skin
 * @param action  where to return after editing
 * @param key     Prototype.skinName   key
 */
function skinListItem_macro(param) {
   res.push();
   var sp = param;
   param.action = param.action ? param.action : "main";
   param.skinset = param.skinset ? param.skinset : "main";
   param.module = param.module ? param.module : req.data.mod;
   var orientation = this._parent.getOrientationValues();
   if (param.key == "Site.page" && orientation.columns == "3c") {
      param.key = "Site.page_3c";
   }
   var splitKey = param.key.split(".");

   var s = this.getSkin(splitKey[0], splitKey[1]);
   if (s)
      sp.status = s.renderSkinAsString("status");
   var desc = this.getSkinDescription("skin", sp.key);
   sp.title = ((s && s.title) ? s.title : desc[0]);
   sp.text = ((s && s.description) ? s.description : desc[1]);
   this.renderSkin("treeleaf", sp);
   return res.pop();
}
