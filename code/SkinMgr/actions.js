
/**
 * all action
 * -> former antville main_action
 */
function all_action() {
   res.data.title = getMessage("SkinMgr.main.title", {layoutTitle: this._parent.title});
   res.data.list = this.renderTree(req.data);
   res.data.body = this.renderSkinAsString("main");
   res.data.sidebar01 = this.renderSkinAsString("mainSidebar");
   this.renderMgrPage();
   return;
}


/**
 * main action
 * uses skin start
 */
function main_action() {
   res.data.title = getMessage("SkinMgr.default.title", {layoutTitle: this._parent.title});
   res.data.body = this.renderSkinAsString("start");
   res.data.sidebar01 = this.renderSkinAsString("mainSidebar");
   this.renderMgrPage();
   return;
}


/**
 * list only modified skins
 */
function modified_action() {
   res.data.title = getMessage("SkinMgr.modified.title", {layoutTitle: this._parent.title});
   res.data.list = this.renderList(this.modified, req.action);
   res.data.body = this.renderSkinAsString("main");
   res.data.sidebar01 = this.renderSkinAsString("mainSidebar");
   this.renderMgrPage();
   return;
}


/**
 * list only custom skins
 */
function custom_action() {
   res.data.title = getMessage("SkinMgr.custom.title", {layoutTitle: this._parent.title});
   res.data.list = this.renderList(this.getCustomSkins(), req.action);
   res.data.body = this.renderSkinAsString("main");
   res.data.sidebar01 = this.renderSkinAsString("mainSidebar");
   this.renderMgrPage();
   return;
}


/**
 * action renders the skinmgr menu in a safe (eg. unscrewable) way using
 * the page skin of skinmgr instead of the one of the site
 * so if something goes wrong this action should at least
 * give users the possibility to undo their changes
 */
function safe_action() {
   res.data.title = this._parent.title;
   res.data.list = this.renderList(this.modified);
   res.data.body = this.renderSkinAsString("main");
   this.renderMgrPage();
   return;
}


/**
 * edit action
 */
function edit_action() {
   if (req.data.cancel) {
      if (req.data.module)
         res.redirect(res.handlers.site.modules.href("skins") + "?mod=" + encodeURIComponent(req.data.module) + "#" + encodeURIComponent(req.data.key));
      else
         res.redirect(this.href(req.data.action) + "?skinset=" + encodeURIComponent(req.data.skinset) + "#" + encodeURIComponent(req.data.key));
   } else if (req.data.save || req.data.close || req.data.reload) {
      try {
         checkSecretKey();
         res.message = this.saveSkin(req.data, session.user);
         if (req.data.reload && session.user.sysadmin) {
            importFlagReasonsFromTOMLSkin();
         }
         if (req.data.close || req.data.reload) {
            if (req.data.module)
               res.redirect(res.handlers.site.modules.href("skins") + "?mod=" + encodeURIComponent(req.data.module) +"#" + encodeURIComponent(req.data.key));
            else
               res.redirect(this.href(req.data.action.clean()) + "?skinset=" + encodeURIComponent(req.data.skinset) + "#" + encodeURIComponent(req.data.key));
         }
         res.redirect(this.href(req.action) + "?key=" + encodeURIComponent(req.data.key) + "&skinset=" + encodeURIComponent(req.data.skinset) + "&action=" + encodeURIComponent(req.data.action));
      } catch (err) {
         res.message = err.toString();
      }
   }

   if (!req.data.key)
      res.redirect(this.href());
   var sp = new Object();
   var splitKey = req.data.key.split(".");
   var s = this.getSkin(splitKey[0], splitKey[1]);
   var desc = this.getSkinDescription("skin", req.data.key);
   sp.title = ((s && s.title) ? s.title : desc[0]);
   sp.description = sp.text = ((s && s.description) ? s.description : desc[1] ? desc[1] : "");
   sp.skin = this.getSkinSource(splitKey[0], splitKey[1]);
   sp.action = req.data.action;

   // if sysAdmin-User requests to edit the Site.flagReasons skin, then provide TOML validation capability
   // as well as a special button to initiate a live reload of the newly parsed flagReasons
   if (session.user.sysadmin && splitKey[1] === 'flagReasons') {
      sp.loadTOML = '<script class="toml" src="' + getStaticUrl() + 'sys.files/js/toml-browser.js" defer></script>';
      sp.reloadFlagReasonsBtn = '<input type="submit" name="reload" value="Live Reload flagReasons" />';
   } else {
      sp.loadTOML = '';
      sp.reloadFlagReasonsBtn = '';
   }

   res.data.action = this.href(req.action);
   res.data.title = getMessage("SkinMgr.edit.title", {skinProto: splitKey[0], skinName: splitKey[1], layoutTitle: this._parent.title});
   res.data.body = this.renderSkinAsString("edit", sp);
   this.renderMgrPage();
   return;
}


/**
 * action for creating a custom skin
 */
function create_action() {
   if (req.data.cancel)
      res.redirect(this.href());
   else if (req.data.save) {
      try {
         checkSecretKey();
         var result = this.evalCustomSkin(req.data, session.user);
         res.message = result.toString();
         if (!result.error)
            res.redirect(this.href("edit") + "?key=" + encodeURIComponent(req.data.prototype) + "." + encodeURIComponent(req.data.name));
      } catch (err) {
         res.message = err.toString();
      }
   }

   res.data.action = this.href(req.action);
   res.data.title = getMessage("SkinMgr.createCustom.title", {layoutTitle: this._parent.title});
   res.data.body = this.renderSkinAsString("new");
   this.renderMgrPage();
   return;
}
