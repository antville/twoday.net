
/**
 * Permission checks (called by hopobject.onRequest())
 *
 * @param action  String, name of action
 * @param usr     User, who wants to access an action
 * @param level   Int, Membership level
 * @return Object if permission was denied <br />
 *   .message  message, if something went wrong <br />
 *   .url      recommanded redirect URL
 * @doclevel public
 */
function checkAccess(action, usr, level) {
   if (usr && usr.sysadmin) return;
   checkIfLoggedIn(this.href(req.action));
   if (!res.handlers.site && !usr.sysadmin) {
      var msg = new Exception("userNoSysAdmin");
      return {message: msg, url: root.href()};
   }
   return;
}
