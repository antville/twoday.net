
/**
 * main action
 */
function main_action() {
   res.data.title = getMessage("RootModuleMgr.main.title");
   res.data.action = this.href();
   res.data.list = "";
   for (var i = this.size() - 1; i >= 0; i--) {
      if (app.modules[this.get(i).name] == null) {
         this.get(i).remove();
      }
   }
   if (req.data.reload) {
      // app.modules.invalidate();
      app.clearCache();
   }
   for (var i in app.modules) {
      if (req.data.reload) {
         this.loadModule(i);
      } else if (this.get(i) == null) {
         var module = this.loadModule(i);
      }
   }
   if (req.data.reload) {
      res.redirect(this.href("main"));
   }

   res.data.list = renderList(this, "mgrlistitem", 20, req.data.page);
   res.data.pagenavigation = renderPageNavigation(this, this.href(), 20, req.data.page);
   res.data.body = this.renderSkinAsString("main");
   this.renderMgrPage();

   return;
}


/**
 * import action
 */
function import_action() {
   if (req.data.cancel) {
      res.redirect(this.href());
   } else if (req.data["import"]) {
      try {
         checkSecretKey();
         var result = this.evalImport(req.data, session.user);
         res.message = result.toString();
         app.modules;
         res.redirect(this.href("main") + "?reload=true");
      } catch (err) {
         res.message = err.toString();
      }
   }
   // render a list of root layouts that are shareable

   res.data.title = getMessage("RootModuleMgr.import.title", {siteTitle: res.handlers.context.getTitle()});
   res.data.action = this.href(req.action);
   res.data.body = this.renderSkinAsString("import");
   this.renderMgrPage();
   return;
}


/**
 * zip's all modules in the modules-dir and updates them in the app-dir
 */
function zipmodules_action() {
   var appDir = new Helma.File(app.dir);
   if (!appDir.exists()) throw new Exception("");

   var modulesDir = new Helma.File(appDir.getParent(), "modules");
   if (!modulesDir.exists()) res.redirect(this.href());

   var excludeFilter = function () {
      var name = this.getName();
      if (name == "Thumbs.db" || name == "CVS" || name == ".svn" || name.substring(0, 1) == ".") return false;
      if (name.substring(name.length - 3) == "zip") return false;
      return true;
   }
   var moduleFilter = function () {
      if (this.getName().substring(0, 3) == "mod") return true;
      return false;
   }
   var moduleDirectoryList = modulesDir.list(moduleFilter);
   for (var i in moduleDirectoryList) {
      var moduleDirectory = new Helma.File(modulesDir, moduleDirectoryList[i])
      app.log("start zipping " + moduleDirectory.getAbsolutePath());
      if (moduleDirectory.isDirectory()) {
         var z = new Zip();
         var deepList = moduleDirectory.listRecursive(excludeFilter);
         for (var j in deepList) {
            if (deepList[j] == moduleDirectory.getAbsolutePath()) continue;

            var moduleFile = new Helma.File(deepList[j]);
            var moduleFileName = moduleFile.getName();
            var relativePath = deepList[j].substring(modulesDir.getAbsolutePath().length + 1 + moduleDirectory.getName().length + 1);
            if (relativePath == "") continue;
            if (relativePath.indexOf(Helma.File.separator) != -1) {
               relativePath = relativePath.substring(0, relativePath.lastIndexOf(Helma.File.separator));
            } else {
               relativePath = "";
            }
            if (!moduleFile.isDirectory()) {
               z.add(moduleFile, relativePath);
               app.log("added " + moduleFile + " at " + relativePath);
            }
         }
         z.save(app.dir + "\\" + moduleDirectoryList[i] + ".zip");
         z.save(moduleDirectory.getParent().getAbsolutePath() + "\\zipArchives\\" + moduleDirectoryList[i] + ".zip");
         app.log("saved " + app.dir + "\\" + moduleDirectoryList[i] + ".zip");
      }
   }
   res.message = "Done!";
   res.redirect(this.href("main") + "?reload=1");
   return;
}
