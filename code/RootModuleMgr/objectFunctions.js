
/**
 * Creates a Module Object that will store informations about this module
 */
function loadModule(name) {
   if (!app.modules[name]) return;
   var mod = (this.get(name)) ? (this.get(name)) : new Module(name);
   var props = Module.standardProperties;
   for (var i=0; i<props.length; i++) mod[props[i]] = app.modules[name][props[i]];
   mod.isActivated = 1;
   mod.isInitialized = 1;
   this.add(mod);
   return;
}


/**
 * import a new Module that was uploaded as a zip file
 */
function evalImport(param, creator) {
   if (param.uploadError) {
      // looks like the file uploaded has exceeded uploadLimit ...
      throw new Exception("moduleImportTooBig");
   } else if (!param.zipfile || param.zipfile.contentLength == 0) {
      // looks like nothing was uploaded ...
      throw new Exception("moduleImportNoUpload");
   }
   var alias = buildAliasFromFile(param.zipfile, this);
   var fileName = alias +  ".zip";
   if (root.modules.get(alias) || app.modules[alias]) {
      throw new Exception("moduleExists", alias);
   }

   var dir = new Helma.File(app.dir).getAbsolutePath();
   var existingFile = new Helma.File(dir, fileName);
   if (existingFile.exists()) {
      throw new Exception("moduleFileExists", fileName);
   }
   try {
      var name = param.zipfile.writeToFile(dir, fileName);
      // app.clearCache();

      return new Message("moduleImport", fileName);
   } catch (err) {
      throw new Exception("moduleImport");
   }
   return;
}
