
/**
 * Checks whether a certain User already exists.
 * Intended to be called via Xml-Rpc.
 * Wrapper for Root.getUserByKey(userid)
 *
 * @see Root.getUserByKey
 *
 * @param userid email or username
 * @return Boolean
 */
function tdyXmlRpcCheckUserExists(userid) {
   this.tdyXmlRpcOnRequest();
   var usr = root.users.get(userid);
   if (!usr) {
      usr = root.usersByEmail.get(userid);
   }
   if (usr)
      return true;
   else
      return false;
}


/**
 * Checks User Credentials. XmlRpc-Wrapper for MemberMgr.evalLogin
 *
 * @see MemberMgr.evalLogin
 *
 * @param userid String, email or username
 * @param password
 * @return Struct containing email, url, publishemail, name
 */
function tdyXmlRpcCheckUserCredentials(userid, password) {
   this.tdyXmlRpcOnRequest();
   var usr = root.users.get(userid);
   if (!usr) {
      usr = root.usersByEmail.get(userid);
   }
   if (!usr || usr.password != password || usr.blocked) {
      throw new Exception("loginTypo");
   }
   var obj = {};
   var props = ["name", "email", "publishemail", "url", "emailIsConfirmed"];
   for (var i=0; i<props.length; i++) {
      if (usr[props[i]]) obj[props[i]] = usr[props[i]];
   }
   return obj;
}


/**
 * Registers a new User. XmlRpc-Wrapper for MemberMgr.evalRegistration
 *
 * @see MemberMgr.evalRegistration
 *
 * @param email
 * @param username
 * @param password
 * @return Boolean
 */
function tdyXmlRpcRegisterUser(email, username, password) {
   this.tdyXmlRpcOnRequest();
   try {
      var param = {terms: 1,
                   email: email,
                   password1: password,
                   password2: password,
                   name: username}
      var result = root.members.evalRegistration(param);
      var usr = result.obj;
      var sp = {
         name: username,
         password: password,
         activationid: usr.getActivationHash(),
         activationlink: usr.getActivationLink()
      };
      sendMail(root.preferences.getProperty("sys_email"),
               email,
               getMessage("mail.registration", root.getTitle()),
               root.members.renderSkinAsString("mailregconfirm", sp)
              );
      return true;
   } catch (err) {
      return false;
   }
}


/**
 * Sends Password-Reminder. XmlRpc-Wrapper for MemberMgr.sendPwd
 *
 * @see MemberMgr.sendPwd
 *
 * @param userid String, email or username
 * @return Boolean
 */
function tdyXmlRpcSendPasswordReminder(userid) {
   this.tdyXmlRpcOnRequest();
   try {
      root.members.sendPwd(userid);
      return true;
   } catch (err) {
      return false;
   }
}


/**
 * Checks whether a certain User has confirmed his email-address.
 * Intended to be called via Xml-Rpc.
 *
 * @param userid email or username
 * @return Boolean
 */
function tdyXmlRpcCheckEmailConfirmed(userid) {
   this.tdyXmlRpcOnRequest();
   var usr = root.users.get(userid);
   if (!usr) {
      usr = root.usersByEmail.get(userid);
   }
   if (usr && usr.emailIsConfirmed)
      return true;
   else
      return false;
}


/**
 * Security check for access. This should be called
 * at the beginning of all XmlRpc-callable functions
 * in this module.
 *
 * @throws DenyException
 */
function tdyXmlRpcCheckAccess() {
   // FIXME
   // check whether request comes from a certain host
   // throw new DenyException("access");
   return;
}


/**
 * onRequest for this module
 *
 */
function tdyXmlRpcOnRequest() {
   this.tdyXmlRpcCheckAccess();	
   var arr = [];
   var locale = getCurrentLocale();
   var fbacks = app.data.fallbackLocales[locale.toString()];
   for (var i=0; i<fbacks.length; i++) arr.push(getLocaleSkinDirPath(fbacks[i]));
   res.skinpath = arr;
   return;
}
