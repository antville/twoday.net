
/**
 * macro renders the username
 */
function username_macro(param) {
   var name = this.user.name;
   if (param.linkto && (param.linkto != "edit" || this.user != session.user))
      Html.link({href: this.href(param.linkto)}, name);
   else
      res.write(name);
   return;
}


/**
 * macro renders e-mail address
 */
function email_macro(param) {
   if (this.user.publishemail)
      return this.user.email;
   return "***";
}


/**
 * macro renders a member's url as text or link
 */
function url_macro(param) {
   if (!this.user.url)
      return;
   if (param.as == "link")
      Html.link({href: this.user.url}, this.user.url);
   else
      res.write(this.user.url);
   return;
}


/**
 * macro renders user-level
 */
function level_macro(param) {
   if (param.as == "editor") {
      var roles = [
         [SUBSCRIBER, getMessage("Membership.role.subscriber")],
         [CONTRIBUTOR, getMessage("Membership.role.contributor")],
         [CONTENTMANAGER, getMessage("Membership.role.contentManager")],
         [ADMIN, getMessage("Membership.role.admin")]
      ]
      Html.dropDown({name: "level"}, roles, this.level, param.firstOption);
   } else {
      if (this.site.creator == this.user)
         res.write(getMessage("Membership.role.owner"));
      else
         res.write(getRole(this.level));
   }
   return;
}


/**
 * macro renders the username
 */
function editlink_macro(param) {
   if (this.user != session.user)
      Html.link({href: this.href("edit")}, param.text ? param.text : getMessage("generic.edit"));
   return;
}


/**
 * macro renders a link for deleting a membership
 */
function deletelink_macro(param) {
   if (this.level != ADMIN)
      Html.link({href: this.href("delete")},
                param.text ? param.text : getMessage("generic.remove"));
   return;
}


/**
 * macro renders a link to unsubscribe-action
 */
function unsubscribelink_macro(param) {
   if (this.level == SUBSCRIBER)
      Html.link({href: this.site.href("unsubscribe")},
                param.text ? param.text : getMessage("Membership.unsubscribe"));
   return;
}


/**
 * renders Task Items (for the admin)
 */
function tasks_macro(param) {
   var items = [];

   // check rights
   var mayEdit = true;
   try { this._parent.checkEditMembers(session.user, res.meta.memberlevel); } catch (deny) { mayEdit = false; }

   // build task List
   if (mayEdit) {
      if (this.user != session.user && this.user != this.site.creator)
         items.push({href: this.href("edit"), text: getMessage("generic.edit")});

      if (this.user != session.user)
         items.push({href: this.href("mailto"), text: getMessage("Membership.mailto.taskLabel")});

      // enable membership removal if at least one admin role remains
      if (this.user != session.user && this._parent.admins.size() > 1)
         items.push({href: this.href("delete"), text: getMessage("generic.remove")});
   }
   var modItems = this.applyModuleMethods("renderMembershipTasks", [], false, false);
   if (modItems) items = items.concat(modItems);

   // render task list
   res.push();
   this._parent.renderTaskItems(items, param);
   param.items = res.pop();

   this._parent.renderSkin("taskContainer", param);
   return;
}


/**
 * renders Task Items (for the subscriber)
 */
function subscriberTasks_macro(param) {
   if (!this.site)
      return;

   var items = [];

   // check rights
   var mayEdit = true;
   try { this._parent.checkEditMembers(session.user, res.meta.memberlevel); } catch (deny) { mayEdit = false; }

   // build task List
   items.push({href: this.site.href(), text: getMessage("generic.view")});
   if (this.level == SUBSCRIBER) items.push({href: this.site.href("unsubscribe"), text: getMessage("Membership.unsubscribe")});
   var modItems = this.applyModuleMethods("renderSubscriptionTasks", [], false, false);
   if (modItems) items = items.concat(modItems);

   // render task list
   res.push();
   this._parent.renderTaskItems(items, param);
   param.items = res.pop();

   this._parent.renderSkin("taskContainer", param);
   return;
}

function renderSiteSkin_macro(param) {
	try {
		this.site.checkView(session.user, null);
		if (param.skin)
			this.site.renderSkin(param.skin);
		
	} catch (e) {}
}
