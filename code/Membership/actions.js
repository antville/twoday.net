
/**
 * edit action
 */
function edit_action() {
   if (req.data.cancel)
      res.redirect(this._parent.href());
   else if (req.data.save) {
      try {
         checkSecretKey();
         res.message = this.updateMembership(parseInt(req.data.level, 10), session.user);
         res.redirect(this._parent.href());
      } catch (err) {
         res.message = err.toString();
      }
   }

   res.data.action = this.href(req.action);
   res.data.title = getMessage("Membership.edit.title", {userName: this.user.name});
   res.data.body = this.renderSkinAsString("edit");
   res.data.sidebar02 = this.renderSkinAsString("editSidebar");
   this._parent.renderMgrPage("Membership");
   return;
}


/**
 * delete action
 */
function delete_action() {
   if (req.data.cancel)
      res.redirect(this._parent.href());
   else if (req.data.remove) {
      try {
         checkSecretKey();
         var url = this._parent.href();
         res.message = this._parent.deleteMembership(this);
         res.redirect(url);
      } catch (err) {
         res.message = err.toString();
      }
   }

   res.data.action = this.href(req.action);
   res.data.title = getMessage("Membership.delete.title", {userName: this.user.name});
   var skinParam = {
      description: getMessage("Membership.deleteDescription"),
      detail: this.user.name
   };
   res.data.body = this.renderSkinAsString("delete", skinParam);
   this._parent.renderMgrPage("Membership");
   return;
}


/**
 * send an e-mail to the user owning this membership
 */
function mailto_action() {
   if (req.data.cancel) {
      res.redirect(this._parent.href());
   } else if (req.data.send) {
      if (req.data.text) {
         try {
            checkSecretKey();
            var param = {text: req.data.text, sender: session.user.name};
            var mailbody = this.renderSkinAsString("mailmessage", param);
            res.message = sendMail(session.user.email,
                                   this.user.email,
                                   getMessage("mail.toUser", root.preferences.getProperty("sys_title")),
                                   mailbody);
            res.redirect(this._parent.href());
         } catch (err) {
            res.message = err.toString();
         }
      } else {
         res.message = new Exception("mailTextMissing");
      }
   }

   res.data.action = this.href(req.action);
   res.data.title = getMessage("Membership.mailto.title", {userName: this.user.name});
   res.data.body = this.renderSkinAsString("mailto");
   this._parent.renderMgrPage();
   return;
}
