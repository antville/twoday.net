/**
 * constructor function for membership objects
 */
function constructor(usr, level) {
   this.user = usr;
   this.level = level ? level : SUBSCRIBER;
   this.createtime = new Date();
}


/**
 * function updates a membership
 * @param level Int Integer representing role of user
 * @param modifier Obj User-object modifying this membership
 * @return Obj Object containing two properties:
 *             - error (boolean): true if error happened, false if everything went fine
 *             - message (String): containing a message to user
 */
function updateMembership(level, modifier) {
   if (isNaN(level))
      throw new Exception("memberNoRole");
   // return if nothing has changed
   if (level == this.level)
      return new Message("update");
   // editing the own membership is denied
   if (this.user == modifier && !modifier.sysadmin)
      throw new DenyException("memberEditSelf");
   // "downgrading" the owner is denied
   if (this.user == this.site.creator && level < this.level)
      throw new DenyException("memberEditOwner");
   if (this.user.may("BE" + getRole(level, true)) == false)
      throw new DenyException("globalMayBeMember2");
   if (!this.user.emailIsConfirmed)
      throw new Exception("accountNotActivatedAddMember");
   if (level != this.level) {
      this.level = level;
      this.modifier = modifier;
      this.modifytime = new Date();
      sendMail(root.preferences.getProperty("sys_email"),
               this.user.email,
               getMessage("mail.statusChange", this.site.title),
               this.renderSkinAsString("mailstatuschange")
              );
   }
   return new Message("update");
}
