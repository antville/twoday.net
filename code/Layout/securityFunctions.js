
/**
 * Permission checks (called by hopobject.onRequest())
 *
 * @param action  String, name of action
 * @param usr     User, who wants to access an action
 * @param level   Int, Membership level
 * @return Object if permission was denied <br />
 *   .message  message, if something went wrong <br />
 *   .url      recommanded redirect URL
 * @doclevel public
 */
function checkAccess(action, usr, level) {
   if (usr && usr.sysadmin) return;
   var url;
   var site = this._parent._parent._prototype == "Site" ? this._parent._parent : null;
   try {
      if (site && !site.online) {
         checkIfLoggedIn(this.href(action));
         site.checkView(usr, level);
      }
      switch (action) {
         case "main" :
         case "preferences" :
         case "edit" :
         case "choose" :
         case "startTestdrive" :
         case "stopTestdrive" :
         case "delete" :
         case "download" :
         case "download_full_zip" :
         case "download_zip" :
         case "translate" :
         case "localizations" :
         case "importlocale" :
            this.checkEdit(usr, level);
            break;
         default :
            url = this.applyAllModuleMethods("checkLayoutAccess", [action, usr, level, url]);
            break;
      }
   } catch (deny) {
      return {message: deny.toString(), url: (url ? url : (site ? site.href() : root.href()))};
   }
   return;
}


/**
 * check if user is allowed to edit this layout
 * @param Obj Userobject
 * @param Int Permission-Level
 * @return String Reason for denial (or null if allowed)
 */
function checkEdit(usr, level) {
   if (this.site) {
      if ((level & MEMBER_MAY_EDIT_LAYOUTS) == 0)
         throw new DenyException("layoutEdit");
   } else if (!usr.sysadmin) {
      throw new DenyException("layoutEdit");
   }
   return null;
}
