
/**
 * main action
 */
function main_action() {
   if (!path.Mgr) res.redirect(this.href("main"));
   res.data.title = getMessage("Layout.main.title", {layoutTitle: this.getNavigationName()});
   res.data.body = this.renderSkinAsString("main");
   res.data.sidebar01 = this.renderSkinAsString("mainSidebar");
   path.Mgr.renderMgrPage("Layout");
   return;
}


/**
 * edit action
 */
function preferences_action() {
   if (!path.Mgr) res.redirect(this.href("preferences"));
   if (req.data.cancel) {
      res.redirect(this.href());
   } else if (req.data.save) {
      req.data.title = this.title;
      req.data.description = this.description;
      req.data.shareable = this.shareable;
      try {
         checkSecretKey();
         res.message = this.evalLayout(req.data, session.user);
         res.redirect(this.href(req.action));
      } catch (err) {
         res.message = err.toString();
      }
   }

   res.data.action = this.href(req.action);
   req.data.preferences_orientation = this.preferences.getProperty("orientation");
   res.data.title = getMessage("Layout.preferences.title", {layoutTitle: this.getNavigationName()});
   res.data.body = this.renderSkinAsString("preferences");
   res.data.sidebar01 = this.renderSkinAsString("preferencesSidebar");
   path.mgr.renderMgrPage("Layout");
   return;
}


/**
 * edit action
 */
function edit_action() {
   if (req.data.cancel)
      res.redirect(this.href());
   else if (req.data.save) {
      try {
         checkSecretKey();
         res.message = this.evalLayout(req.data, session.user);
         res.redirect(this.href("main"));
      } catch (err) {
         res.message = err.toString();
      }
   }

   res.data.action = this.href(req.action);
   res.data.title = getMessage("Layout.edit.title", {layoutTitle: this.getNavigationName()});
   res.data.body = this.renderSkinAsString("edit");
   res.data.sidebar01 = this.renderSkinAsString("editSidebar");
   path.mgr.renderMgrPage("Layout");
   return;
}


/**
 * choose a new basis layout
 */
function choose_action() {
   if (req.data.cancel)
      res.redirect(this.href());
   else if (req.data.create) {
      req.data.title = this.title;
      req.data.description = this.description;
      try {
         checkSecretKey();
         var parent = root.layouts.get(req.data.layout);
         res.message = this.setParentLayout(parent);
         res.redirect(this.href("edit"));
      } catch (err) {
         res.message = err.toString();
      }
   }

   // render a list of root layouts that are shareable
   var shareable = root.layouts.getShareable(session.user, res.meta.memberlevel);
   res.data.layoutlist = renderList(shareable, "chooserlistitem", 20, req.data.page);
   res.data.pagenavigation = renderPageNavigation(shareable, this.href(req.action), 20, req.data.page);

   res.data.title = getMessage("LayoutMgr.choose.title", {siteTitle: res.handlers.context.getTitle()});
   res.data.action = this.href(req.action);
   res.data.body = this.renderSkinAsString("choose");
   path.mgr.renderMgrPage("Layout");
   return;
}


/**
 * action to test-drive this layout in the current session.
 */
function startTestdrive_action() {
   if (!res.handlers.site || res.handlers.site.layout == this) {
      session.data.layout = null;
      res.redirect(req.data.http_referer);
      return;
   }
   session.data.layout = this;
   if (req.data.http_referer)
      res.redirect(req.data.http_referer);
   else
      res.redirect(res.handlers.context.href());
   return;
}


/**
 * stop a layout test and resume normal browsing.
 */
function stopTestdrive_action() {
   session.data.layout = null;
   res.message = new Message("layoutStopTestdrive");
   if (req.data.http_referer)
      res.redirect(req.data.http_referer);
   else
      res.redirect(res.handlers.context.href());
   return;
}


/**
 * action deletes this layout
 */
function delete_action() {
   if (this.isDefaultLayout() || this.sharedBy.size() > 0) {
      res.message = new DenyException("layoutDelete");
      res.redirect(this._parent.href());
   }
   if (req.data.cancel)
      res.redirect(this._parent.href());
   else if (req.data.remove) {
      var href = this._parent.href();
      var localeHref = this.getLocaleParent().href("localizations");
      var isTranslation = this.isTranslation();
      try {
         checkSecretKey();
         if (session.data.layout && this == session.data.layout) session.data.layout = null;
         res.message = this._parent.deleteLayout(this);
         if (isTranslation) res.redirect(localeHref);
         else res.redirect(href);
      } catch (err) {
         res.message = err.toString();
         if (isTranslation) res.redirect(localeHref);
         else res.redirect(href);
      }
   }

   res.data.action = this.href(req.action);
   res.data.title = res.handlers.context.getTitle();
   var skinParam = {
      description: "the layout",
      detail: this.title
   };
   res.data.body = this.renderSkinAsString("delete", skinParam);
   path.mgr.renderMgrPage("Layout");
   return;
}


/**
 * download action
 */
function download_action() {
   if (req.data.cancel)
      res.redirect(this._parent.href());
   else if (req.data.full)
      res.redirect(this.href("download_full.zip"));
   else if (req.data.changesonly)
      res.redirect(this.href("download.zip"));

   res.data.action = this.href(req.action);
   res.data.title = getMessage("Layout.download.title", {layoutTitle: this.getNavigationName()});
   res.data.body = this.renderSkinAsString("download", {text: getMessage("Layout.download.text", {layoutTitle: this.getNavigationName()})});
   res.data.sidebar02 = this.renderSkinAsString("downloadSidebar");
   path.mgr.renderMgrPage("Layout");
   return;
}


/**
 * create a Zip file containing the whole layout
 */
function download_full_zip_action() {
   try {
      var data = this.evalDownload(true);
      res.contentType = "application/zip";
      res.writeBinary(data);
   } catch (err) {
      // var error = new Exception("layoutDownload");
      // res.message = "some error";
      res.message = err;
      res.redirect(this.href());
   }
   return;
}


/**
 * create a .zip file containing layout changes only
 */
function download_zip_action() {
   try {
      var data = this.evalDownload(false);
      res.contentType = "application/zip";
      res.writeBinary(data);
   } catch (err) {
      res.message = new Exception("layoutDownload");
      res.redirect(this.href());
   }
   return;
}


/**
 * action translates this layout
 */
function translate_action() {
   // TODO translated Layouts currently disabled
   return;
   if (this.isTranslation() && this.parent.getLocale().variant) {
      res.message = new DenyException("layoutTranslate");
      res.redirect(this._parent.href());
   }
   if (req.data.cancel) {
      res.redirect(this._parent.href());
   } else if (req.data.translate) {
      var href = this._parent.href();
      try {
         checkSecretKey();
         var result = this.evalTranslate(req.data, session.user);
         res.message = result.toString();
         var layout = result.obj;
         if (layout.isTranslation()) {
            res.redirect(layout.getLocaleParent().href("localizations"));
         } else {
            res.redirect(href);
         }
      } catch (err) {
         res.message = err.toString();
      }
   }

   res.data.action = this.href(req.action);
   res.data.title = getMessage("Layout.translate.title", {layoutTitle: this.getNavigationName()});
   res.data.body = this.renderSkinAsString("translate");
   path.mgr.renderMgrPage("Layout");
   return;
}


/**
 * main action
 */
function localizations_action() {
   // TODO translated Layouts currently disabled
   return;
   res.data.title = getMessage("LayoutMgr.localizations.title", {layoutTitle: this.getNavigationName()});
   res.data.action = this.href();
   res.data.layoutlist = renderList(this.getLocaleParent().localizations, "localelistitem", 20, req.data.page);
   res.data.pagenavigation = renderPageNavigation(this.getLocaleParent().localizations, this.href("localizations"), 20, req.data.page);
   res.data.body = this.renderSkinAsString("localizations");
   path.mgr.renderMgrPage("Layout");
   return;
}


/**
 * import locale action
 * will just work for importing changes to a localeParent
 */
function importlocale_action() {
   // TODO translated Layouts currently disabled
   return;
   if (req.data.cancel)
      res.redirect(this.href());
   else if (req.data["import"]) {
      try {
         checkSecretKey();
         var result = this.evalImport(req.data, session.user);
         res.message = result.toString();
//         res.redirect(this.href());
      } catch (err) {
         res.message = err.toString();
      }
   }
   // render a list of root layouts that are shareable
   var shareable = root.layouts.getShareable(session.user, res.meta.memberlevel);
   res.data.layoutlist = renderList(shareable, "chooserlistitem", 5, req.data.page);
   res.data.pagenavigation = renderPageNavigation(shareable, this.href(req.action), 5, req.data.page);

   res.data.title = getMessage("LayoutMgr.importLocale.title", {layoutTitle: this.title});
   res.data.action = this.href(req.action);
   res.data.body = this.renderSkinAsString("importlocale");
   path.mgr.renderMgrPage("Layout");
   return;
}


/**
 * redirecting action
 */
function skins_action() {
   res.redirect(this.href("skins") + "/main");
}
