
/**
 * constructor function for layout objects
 */
function constructor(site, title, creator) {
   this.site = site;
   this.title = title;
   this.creator = creator;
   this.createtime = new Date();
   this.shareable = 0;
   var prefs = new HopObject();
   prefs.bgcolor = "ffffff";
   prefs.textfont = "Trebuchet MS, Verdana, Arial";
   prefs.textsize = "13px";
   prefs.textcolor = "000000";
   prefs.linecolor = "cccc66";
   prefs.blockcolor = "a8a86e";
   prefs.linkcolor = "666633";
   prefs.alinkcolor = "cccc66";
   prefs.vlinkcolor = "666633";
   prefs.titlefont = "Trebuchet MS, Verdana, Arial";
   prefs.titlesize = "16px";
   prefs.titlecolor = "666633";
   prefs.smallfont = "Trebuchet MS, Verdana, Arial";
   prefs.smallsize = "12px";
   prefs.smallcolor = "666666";
   this.preferences_xml = Xml.writeToString(prefs);
}


/**
 * evaluate submitted form values and update
 * the layout object
 */
function evalLayout(param, modifier) {
   if (!param.title || !param.title.trim()) {
      if (!this.parent || !this.parent.title)
         throw new Exception("layoutTitleMissing");
      else
         param.title = this.parent.title;
   }
   this.title = param.title;
   this.description = param.description;
   if (param.locale) this.locale = param.locale;

   // check if orientations are valid
   if (param.preferences_allowedOrientations) {
      var o = param.preferences_allowedOrientations.split(";");
      if (o.length == 0)
         throw new Exception("invalidLayoutOrientations");
      for (var i = 0; i < o.length; i++) {
         if (!Array.contains(LAYOUT_ORIENTATIONS, o[i]))
            throw new Exception("invalidLayoutOrientations");
      }
      if (this.preferences.getProperty("orientation") == null)
         this.preferences.setProperty("orientation", o[0]);
   }

   // get preferences from param object
   var prefs = this.preferences.getAll();
   for (var i in param) {
      if (i.startsWith("preferences_"))
         prefs[i.substring(12)] = param[i];
   }
   // store preferences
   this.preferences.setAll(prefs);
   // parent layout
   if (param.layout && param.layout != "") {
      this.parent = root.layouts.get(param.layout);
   }
   this.shareable = param.shareable;
   this.modifier = modifier;
   this.modifytime = new Date();
   return new Message("layoutUpdate", this.title);
}


/**
 * delete all skins and images belonging to this layout
 */
function deleteAll() {
   this.images.deleteAll();
   this.skins.deleteAll();
   return true;
}


/**
 * Return the name of this layout
 * to be used in the global linkedpath macro
 * @see hopobject.getNavigationName()
 */
function getNavigationName() {
   if (this.isTranslation()) {
      return this.title + "-" + this.getLocale().getDisplayName();
   } else {
      return this.title;
   }
}


/**
 * render the path to the static directory of this
 * layout object
 * @param String name of subdirectory (optional)
 */
function staticPath(subdir) {
   if (this.site)
      this.site.staticPath();
   else
      res.write(app.properties.staticPath);
   res.write("layouts/");
   res.write(this.alias);
   res.write("/");
   if (subdir)
      res.write(subdir);
   return;
}


/**
 * return the path to the static directory of this
 * layout object
 * @param String name of subdirectory (optional)
 * @return String path to static directory
 */
function getStaticPath(subdir) {
   res.push();
   this.staticPath(subdir);
   return res.pop();
}


/**
 * render the URL of the directory where images
 * of this layout are located
 */
function staticUrl() {
	var staticUrl = "";
   if (this.site)
      var staticUrl = this.site.getStaticUrl();
   else
      var staticUrl = app.properties.staticUrl;
	if(req && req.servletRequest && req.servletRequest.isSecure())
   	staticUrl = staticUrl.replace("http://", "https://");
   res.write(staticUrl)
   res.write("layouts/");
   res.write(this.alias);
   res.write("/");
   return;
}


/**
 * return the URL of the directory where images
 * of this layout are located
 * @return String URL of the image directory
 */
function getStaticUrl() {
   res.push();
   this.staticUrl();
   return res.pop();
}


/**
 * return the directory where images of this layout
 * are stored
 * @return Object File Object representing the image
 *                directory on disk
 */
function getStaticDir(subdir) {
   var f = new Helma.File(this.getStaticPath());
   f.mkdir();
   if (subdir) {
      f = new Helma.File(f, subdir);
      f.mkdir();
   }
   return f;
}


/**
 * Helper function: is this layout the default in the current context?
 */
function isDefaultLayout() {
   if (this.site && this.site.layout == this)
      return true;
   if (!this.site && root.sys_layout == this)
      return true;
   return false;
}


/**
 * Helper function: is this layout a translated layout
 */
function isTranslation() {
   return (this.localeParent != null);
}


/**
 * Helper function: is this layout a translated layout
 */
function hasTranslations() {
   return (this.localizations.size() > 0);
}


/**
 * make this layout object a child layout of the one
 * passed as argument and copy the layout-relevant
 * preferences
 * @param Object parent layout object
 */
function setParentLayout(parent) {
   this.parent = parent;
   // copy relevant preferences from parent
   var prefs = new HopObject();
   var parentPrefs = parent.preferences.getAll();
   prefs.bgcolor = parentPrefs.bgcolor;
   prefs.textfont = parentPrefs.textfont;
   prefs.textsize = parentPrefs.textsize;
   prefs.textcolor = parentPrefs.textcolor;
   prefs.linkcolor = parentPrefs.linkcolor;
   prefs.alinkcolor = parentPrefs.alinkcolor;
   prefs.vlinkcolor = parentPrefs.vlinkcolor;
   prefs.titlefont = parentPrefs.titlefont;
   prefs.titlesize = parentPrefs.titlesize;
   prefs.titlecolor = parentPrefs.titlecolor;
   prefs.smallfont = parentPrefs.smallfont;
   prefs.smallsize = parentPrefs.smallsize;
   prefs.smallcolor = parentPrefs.smallcolor;
   prefs.linecolor = parentPrefs.linecolor;
   prefs.blockcolor = parentPrefs.blockcolor;
   prefs.copyright = parentPrefs.copyright;
   prefs.email = parentPrefs.email;
   prefs.orientation = parentPrefs.orientation;
   prefs.allowedOrientations = parentPrefs.allowedOrientations;
   prefs.importantImages = parentPrefs.importantImages;
   this.preferences.setAll(prefs);
   return;
}


/**
 * dump a layout object by copying all necessary properties
 * to a transient HopObject and then return the Xml dump
 * of it (this way we avoid any clashes with usernames)
 * @param Object Zip object to dump layout to
 * @param Boolean true for full export, false for incremental
 * @return Boolean true
 */
function dumpToZip(z, fullExport, locale) {
   var dir = locale ? locale + "/" : "";
   var cl = new HopObject();
   cl.title = this.title;
   cl.alias = this.alias;
   cl.locale = this.getLocale().toString();
   cl.parent = this.parent ? this.parent.alias : null;
   cl.localeParent = this.localeParent ? this.localeParent.alias : null;
   cl.description = this.description;
   cl.preferences = this.preferences.getAll();
   cl.creator = this.creator ? this.creator.name : null;
   cl.createtime = this.creator ? this.createtime : null;
   cl.exporttime = new Date();
   cl.exporter = session.user.name;
   cl.fullExport = fullExport;
   cl.modifier = this.modifier ? this.modifier.name : null;
   cl.modifytime = this.modifytime;
   var buf = new java.lang.String(Xml.writeToString(cl)).getBytes();
   z.addData(buf, dir + "preferences.xml");
   return true;
}


/**
 * dump just the localization info to archiveinfo.xml
 * @param Object Zip object to dump layout to
 * @param Boolean true for full export, false for incremental
 * @return Boolean true
 */
function dumpLocalizationInfoToZip(z, fullExport) {
   var cl = new HopObject();
   cl.title = this.title;
   cl.alias = this.alias;
   cl.locale = this.getLocale().toString();
   cl.parent = this.parent ? this.parent.alias : null;
   cl.description = this.description;
   cl.creator = this.creator ? this.creator.name : null;
   cl.createtime = this.creator ? this.createtime : null;
   cl.exporttime = new Date();
   cl.exporter = session.user.name;
   cl.fullExport = fullExport;
   cl.modifier = this.modifier ? this.modifier.name : null;
   cl.modifytime = this.modifytime;
   var buf = new java.lang.String(Xml.writeToString(cl)).getBytes();
   z.addData(buf, "archiveinfo.xml");
   return true;
}


/**
 * create a .zip file containing the whole layout (including
 * skins, images and properties)
 * @param Boolean true for full export, false for incremental
 * @param Object Byte[] containing the binary data of the zip file
 */
function evalDownload(fullExport) {
   // create the zip file
   var z = new Zip();

   // if this is a localeParent a full export will include all localized
   // version as partly exports
   if (fullExport && false && !this.isTranslation()) {
      // add basic info about the localeParent
      this.dumpLocalizationInfoToZip(z, fullExport);
      // get all localizations includinge the localeParent(=this)
      // as the first element in the array
      var localizations = this.getAllLocalizations(true);

      // loop through all localization, but export just the
      // root localization as a full export
      for (var i = 0; i < localizations.length; i++) {
         var locale = localizations[i].getLocale().toString();

         // first, dump the layout and add it to the zip file
         localizations[i].dumpToZip(z, (i==0), locale);
         localizations[i].images.dumpToZip(z, (i==0), null, locale);
         localizations[i].skins.dumpToZip(z, (i==0), null, locale);
      }

   } else {
      // first, dump the layout and add it to the zip file
      this.dumpToZip(z, fullExport);
      // add the metadata of layout images
      // to the directory "imagedata" in the zip file
      var imgLog = this.images.dumpToZip(z, fullExport);
      // add skins to the zip archive
      var skinLog = this.skins.dumpToZip(z, fullExport);

   }
   z.close();
   return z.getData();
}


/**
 * retrieve an image from ImageMgr
 * this method walks up the hierarchy of layout objects
 * until it finds an image, otherwise returns null
 * @param String name of image to retrieve
 * @param String name of fallback image to retrieve (optional)
 * @return Object image object or null
 */
function getImage(name, fallback) {
   var handler = this;
   while (handler) {
      if (handler.images.get(name))
         return handler.images.get(name);
      if (handler.images.get(fallback))
         handler.images.get(fallback)
      handler = handler.parent;
   }
   return null;
}


/**
 * walk up the layout hierarchy and add all skinmgr
 * to an array
 * @return Object Array containing skinmgr objects
 */
function getSkinPath() {
   var sp = new Array();
   if (this.skins.count() > 0) sp.push(this.skins);
   var handler = this;
   while ((handler = handler.parent) != null) {
      if (handler.skins.count() > 0) sp.push(handler.skins);
   }
   return sp;
}


/**
 * walk up all parents and add them to a Hashtable
 * (the key is the layout._id, value is Boolean true
 * @return Object java.util.Hashtable
 */
function getParents() {
   var parents = new java.util.Hashtable();
   var handler = this;
   while ((handler = handler.parent) != null)
      parents.put(handler._id, true);
   return parents;
}


/**
 * creates a translated layout
 * called by Layout.translate_action
 * @param param Object  parameters passed to create the translated layout
 *    .locale      String example: en_US, de_AT_steirisch
 *    .variant     String optinal variant string
 * @param creator User  usualy this is the session.user
 */
function evalTranslate(param, creator) {
   var localeParent = this.getLocaleParent();
   var localeString = (param.locale ? param.locale : this.getLocale().toString()) + (param.variant ? "_" + param.variant : "");
   if (param.variant && convertStringToLocale(param.locale) && convertStringToLocale(param.locale).variant) {
      throw new Exception("layoutHasVariant", {locale: convertStringToLocale(param.locale).getDisplayName()});
   }

   var testVariant = new RegExp('[A-Za-z][A-Za-z0-9]*');
   if (param.variant && !param.variant.match(testVariant)) {
      throw new Exception("layoutInvalidVariant", {locale: localeString});
   }
   var locale = convertStringToLocale(localeString);
   if (locale == null) {
      throw new Exception("layoutLocaleNotAvailable", {locale: localeString});
   }
   var existingLocalizations = this.getAllLocalesAsStrings(true);
   if (Array.contains(existingLocalizations, localeString)) {
      throw new Exception("layoutLocaleExists", {locale: localeString});
   }

   var parent = this;
   // create layout for language if not existing yet
   if (locale.country) {
      var languageParent = localeParent.localizations.get(locale.language) || (localeParent.getLocale().toString() == locale.language ? localeParent : null);
      if (languageParent == null) {
         var result = localeParent.evalTranslate({locale: locale.language}, creator);
         languageParent = result.obj;
      }
      parent = languageParent;

      // create layout for language_COUNTRY if not existing yet
      if (locale.variant) {
         var countryParent = localeParent.localizations.get(param.locale);
         if (countryParent == null) {
            var result = languageParent.evalTranslate({locale: param.locale}, creator);
            countryParent = result.obj;
         }
         parent = countryParent;
      }
   }

   var newLayout = new Layout(null, (localeParent.title), creator);
   newLayout.setParentLayout(parent);
   newLayout.localeParent = localeParent;
   newLayout.title = localeParent.title;
   newLayout.locale = localeString;
   newLayout.alias = localeParent.alias + "_" + localeString;

   if (!localeParent._parent.add(newLayout)) {
      throw new Exception("layoutTranslate");
   }
   return new Message("layoutTranslate", (newLayout.title + " - " + newLayout.getLocale().getDisplayName()), newLayout);
}


/**
 * function returns this.locale as java.util.Locale
 * if not specified for this layout it will fallback
 * to parent &gt; site &gt; root
 * @return java.util.Locale
 */
function getLocale() {
   if (this.locale) {
      return convertStringToLocale(this.locale);
   } else if (this.parent) {
      return this.parent.getLocale();
   } else if (this.site) {
      return this.site.getLocale();
   } else if (app.properties.defaultLocale) {
      return convertStringToLocale(app.properties.defaultLocale);
   } else {
      return root.getLocale();
   }
}


/**
 * returns an array of localized layouts
 *
 * @param includeOriginal Boolean wether to include the original (untranslated layout)
 * @return Array[Layout]
 */
function getAllLocalizations(includeOriginal) {
   var result = this.getLocaleParent().localizations.list();
   if (includeOriginal) result.unshift(this.getLocaleParent());
   return result;
}


/**
 * returns an array of java.util.Locals of localized layouts
 *
 * @param includeOriginal Boolean wether to include the original (untranslated layout)
 * @see Layout.getAllLocalizations
 * @return Array[java.util.Local]
 */
function getAllLocales(includeOriginal) {
   var localizations = this.getAllLocalizations(includeOriginal);
   var result = [];
   for (var i = 0; i < localizations.length; i++) {
      result.push(localizations[i].getLocale());
   }
   return result;
}


/**
 * returns an array of locale strings of localized layouts
 *
 * @param includeOriginal Boolean wether to include the original (untranslated layout)
 * @see Layout.getAllLocalizations
 * @return Array[String]
 */
function getAllLocalesAsStrings(includeOriginal) {
   var localizations = this.getAllLocalizations(includeOriginal);
   var result = [];
   for (var i = 0; i < localizations.length; i++) {
      if (localizations[i].getLocale())
         result.push(localizations[i].getLocale().toString());
   }
   return result;
}

function getLocaleParent() {
   return this.localeParent ? this.localeParent : this;

}


/**
 * returns the next available localization
 * resolves de_AT_Steirisch > de_AT > de > (en)
 *
 * @param locale            String|Locale
 * @param includeLocaleParent Boolean        wether to return the localeParent as fallback
 * @return Layout
 */
function getLocalization(locale, includeLocaleParent) {
   var locale = (typeof locale == "string") ? convertStringToLocale(this.locale) : locale;
   var localeParent = this.getLocaleParent();
   var localization;

   if (localeParent.locale == locale.toString()) return localeParent;

   localization = localeParent.localizations.get(locale.toString());
   if (localization) return localization;

   if (locale.variant) {
      localization = localeParent.localizations.get(locale.language + "_" + locale.country);
      if (localization) return localization;
   }

   if (locale.country) {
      localization = localeParent.localizations.get(locale.language);
      if (localization) return localization;
   }

   return (includeRoot) ? localeParent : null;
}


/**
 * returns the message that will be displayed while testing a layout
 */
function getTestDriveMessage() {
   var param = {
      "message": (session.data.layout.site == res.handlers.site) ?
         getMessage("Layout.testDrive.testAndEditMessage", {layoutTitle: res.handlers.layout.title}) :
         getMessage("Layout.testDrive.testMessage", {layoutTitle: res.handlers.layout.title})
   }
   return this.renderSkinAsString("testdrive", param);
}


/**
 * function returns an object containing the orientation values
 * result.columns      ... 1c,2c,3c
 * result.orientation  ... center,full,left
 * result.side         ... left,right
 * @return Object
 */
function getOrientationValues(orientation) {
   var orientation = (orientation) ? orientation : this.preferences.getProperty("orientation");
   if (!orientation) orientation = "2c_center_left";
   var result = {};
   var values = orientation.split("_");
   result.columns = values[0];
   result.orientation = values[1];
   result.side = values[2];
   result.toString = function () {
      return orientation;
   }
   return result;
}


/**
 * check if user is allowed to use this layout as a parent layout
 * @param Obj Userobject
 * @param Int Permission-Level
 * @return Boolean
 */
function isShareable(usr, level) {
   switch (parseInt(this.shareable, 10)) {
      case 0 :
         return false;
         break;
      case 1 :
         return true;
         break;
      case 2 :
         return (usr != null && usr.trusted == 1);
         break;
      case 3 :
         return (session.user.sysadmin == 1);
         break;
   }
   return false;
}
