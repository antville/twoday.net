
/**
 * macro rendering bgcolor
 */
function bgcolor_macro(param) {
   if (param.as == "editor")
      Html.input(this.preferences.createInputParam("bgcolor", param));
   else if (param.as == "number")
      res.write(this.bgcolor);
   else
      renderColor(this.preferences.getProperty("bgcolor"));
   return;
}


/**
 * macro rendering textfont
 */
function textfont_macro(param) {
   if (param.as == "editor") {
      param.size = 40;
      Html.input(this.preferences.createInputParam("textfont", param));
   } else
      res.write(this.preferences.getProperty("textfont"));
   return;
}


/**
 * macro rendering textsize
 */
function textsize_macro(param) {
   if (param.as == "editor")
      Html.input(this.preferences.createInputParam("textsize", param));
   else
      res.write(this.preferences.getProperty("textsize"));
   return;
}


/**
 * macro rendering textcolor
 */
function textcolor_macro(param) {
   if (param.as == "editor")
      Html.input(this.preferences.createInputParam("textcolor", param));
   else if (param.as == "number")
      res.write(this.textcolor);
   else
      renderColor(this.preferences.getProperty("textcolor"));
   return;
}


/**
 * macro rendering linkcolor
 */
function linkcolor_macro(param) {
   if (param.as == "editor")
      Html.input(this.preferences.createInputParam("linkcolor", param));
   else if (param.as == "number")
      res.write(this.linkcolor);
   else
      renderColor(this.preferences.getProperty("linkcolor"));
   return;
}


/**
 * macro rendering alinkcolor
 */
function alinkcolor_macro(param) {
   if (param.as == "editor")
      Html.input(this.preferences.createInputParam("alinkcolor", param));
   else if (param.as == "number")
      res.write(this.alinkcolor);
   else
      renderColor(this.preferences.getProperty("alinkcolor"));
   return;
}


/**
 * macro rendering vlinkcolor
 */
function vlinkcolor_macro(param) {
   if (param.as == "editor")
      Html.input(this.preferences.createInputParam("vlinkcolor", param));
   else if (param.as == "number")
      res.write(this.vlinkcolor);
   else
      renderColor(this.preferences.getProperty("vlinkcolor"));
   return;
}


/**
 * macro rendering blockcolor
 */
function blockcolor_macro(param) {
   if (param.as == "editor")
      Html.input(this.preferences.createInputParam("blockcolor", param));
   else if (param.as == "number")
      res.write(this.blockcolor);
   else
      renderColor(this.preferences.getProperty("blockcolor"));
   return;
}


/**
 * macro rendering linecolor
 */
function linecolor_macro(param) {
   if (param.as == "editor")
      Html.input(this.preferences.createInputParam("linecolor", param));
   else if (param.as == "number")
      res.write(this.linecolor);
   else
      renderColor(this.preferences.getProperty("linecolor"));
   return;
}


/**
 * macro rendering titlefont
 */
function titlefont_macro(param) {
   if (param.as == "editor") {
      param.size = 40;
      Html.input(this.preferences.createInputParam("titlefont", param));
   } else
      res.write(this.preferences.getProperty("titlefont"));
   return;
}


/**
 * macro rendering titlesize
 */
function titlesize_macro(param) {
   if (param.as == "editor")
      Html.input(this.preferences.createInputParam("titlesize", param));
   else
      res.write(this.preferences.getProperty("titlesize"));
   return;
}


/**
 * macro rendering titlecolor
 */
function titlecolor_macro(param) {
   if (param.as == "editor")
      Html.input(this.preferences.createInputParam("titlecolor", param));
   else if (param.as == "number")
      res.write(this.titlecolor);
   else
      renderColor(this.preferences.getProperty("titlecolor"));
   return;
}


/**
 * macro rendering smallfont
 */
function smallfont_macro(param) {
   if (param.as == "editor") {
      param.size = 40;
      Html.input(this.preferences.createInputParam("smallfont", param));
   } else
      res.write(this.preferences.getProperty("smallfont"));
   return;
}


/**
 * macro rendering smallfont-size
 */
function smallsize_macro(param) {
   if (param.as == "editor")
      Html.input(this.preferences.createInputParam("smallsize", param));
   else
      res.write(this.preferences.getProperty("smallsize"));
   return;
}


/**
 * macro rendering smallfont-color
 */
function smallcolor_macro(param) {
   if (param.as == "editor")
      Html.input(this.preferences.createInputParam("smallcolor", param));
   else if (param.as == "number")
      res.write(this.smallcolor);
   else
      renderColor(this.preferences.getProperty("smallcolor"));
   return;
}


/**
 * renders the layout title as editor
 */
function title_macro(param) {
   if (param.as == "editor")
      Html.input(this.createInputParam("title", param));
   else {
      if (param.linkto) {
         Html.openLink({href: this.href(param.linkto == "main" ? "" : param.linkto)});
         res.write(this.title);
         Html.closeLink();
      } else
         res.write(this.title);
   }
   return;
}


/**
 * macro renders an image out of the layout imagepool
 * either as plain image, thumbnail, popup or url
 * param.name can contain a slash indicating that
 * the image belongs to a different site or to root
 */
function image_macro(param) {
   var img;
   if ((img = this.getImage(param.name, param.fallback)) == null) {
      if (param.as == "url") 
         DefaultImages.render("pixel", param);
      return;
   }
   // return different display according to param.as
   switch (param.as) {
      case "url" :
         return img.getUrl();
      case "thumbnail" :
         if (!param.linkto)
            param.linkto = img.getUrl();
         if (img.thumbnail)
            img = img.thumbnail;
         break;
      case "popup" :
         param.linkto = img.getUrl();
         param.onclick = img.getPopupUrl();
         if (img.thumbnail)
            img = img.thumbnail;
         break;
   }
   delete(param.name);
   delete(param.as);
   // render image tag
   if (param.linkto) {
      Html.openLink({href: param.linkto});
      delete(param.linkto);
      renderImage(img, param);
      Html.closeLink();
   } else
      renderImage(img, param);
   return;
}


/**
 * render a link to testdrive if the layout is *not*
 * the currently active layout
 */
function testdrivelink_macro(param) {
   if (res.handlers.layout == this)
      return;
   if (param.as == "url") {
      return this.href("startTestdrive");
   } else {
      Html.link({href: this.href("startTestdrive")},
                param.text ? param.text : getMessage("Layout.test"));
   }
   return;
}


/**
 * render a link for deleting the layout, but only if
 * layout is *not* the currently active layout
 */
function deletelink_macro(param) {
   if (this.isDefaultLayout() || this.sharedBy.size() > 0)
      return;
   Html.link({href: this.href("delete")},
             param.text ? param.text : getMessage("generic.delete"));
   return;
}


/**
 * render a link for activating the layout, but only if
 * layout is *not* the currently active layout
 */
function activatelink_macro(param) {
   if (this.isDefaultLayout() || (this.isTranslation() && this.getLocale().variant))
      return;
   Html.link({href: this._parent.href() + "?activate=" + this.alias},
             param.text ? param.text : getMessage("Layout.activate"));
   return;
}


/**
 * render a link for translating the layout, but only if
 * layout is *not* a translation
 */
function translatelink_macro(param) {
   // TODO translated Layouts currently disabled
   return;
   if (this.isTranslation() && this.getLocale().variant)
      return;
   Html.link({href: this.href("translate")},
             param.text ? param.text : getMessage("Layout.translate"));
   return;
}


/**
 * render a link for translating the layout, but only if
 * layout is *not* a translation
 */
function localizationslink_macro(param) {
   // TODO translated Layouts currently disabled
   return;
   Html.link({href: this.getLocaleParent().href("translate")},
             param.text ? param.text : getMessage("Layout.localizations"));
   return;
}


/**
 * render the description of a layout, either as editor
 * or as plain text
 */
function description_macro(param) {
   if (param.as == "editor")
      Html.textArea(this.createInputParam("description", param));
   else if (this.description) {
      if (param.limit)
         res.write(this.description.clip(param.limit));
      else
         res.write(this.description);
   }
   return;
}


/**
 * render the property "shareable" either as editor (checkbox)
 * or as plain text (editor-mode works only for root-layouts)
 */
function shareable_macro(param) {
   if (param.as == "editor" && !this.site) {
      var chooserParam = {};
      var options = [
         {value: SHAREABLE_FOR_NONE, display: getMessage("Layout.shareable.for.none")},
         {value: SHAREABLE_FOR_ALL, display: getMessage("Layout.shareable.for.all")},
         {value: SHAREABLE_FOR_TRUSTED, display: getMessage("Layout.shareable.for.trusted")},
         {value: SHAREABLE_FOR_ADMIN, display: getMessage("Layout.shareable.for.admin")}
      ];
      chooserParam.name = "shareable";
      Html.dropDown(chooserParam, options, (this.shareable != null ? this.shareable : SHAREABLE_FOR_NONE));
      return;
   } else {
      res.write(getMessage("Layout.shareable.for." + SHAREABLE_FOR_NAMES[this.shareable]));
   }
   return;
}


/**
 * render the title of the parent layout
 */
function parent_macro(param) {
   if (param.as == "editor") {
      this._parent.renderParentLayoutChooser(this, param.firstOption);
   } else if (this.parent)
      return this.parent.title;
   return;
}


/**
 * render the copyright information of this layout
 * either as editor or as plain text
 */
function copyright_macro(param) {
   if (param.as == "editor" && (session.user.trusted || !this.imported) && !this.languageparent)
      Html.input(this.preferences.createInputParam("copyright", param));
   else if (this.preferences.getProperty("copyright"))
      res.write(this.preferences.getProperty("copyright"));
   return;
}


/**
 * render the contact email address of this layout
 * either as editor or as plain text
 */
function email_macro(param) {
   if (param.as == "editor" && (session.user.trusted || !this.imported))
      Html.input(this.preferences.createInputParam("email", param));
   else if (this.preferences.getProperty("email"))
      res.write(this.preferences.getProperty("email"));
   return;
}


/**
 * render the allowed Orientations of this layout
 * either as editor or as plain text
 */
function allowedOrientations_macro(param) {
   if (param.as == "editor" /* && !this.imported*/)
      Html.textArea(this.preferences.createInputParam("allowedOrientations", param));
   else if (this.preferences.getProperty("allowedOrientations"))
      res.write(this.preferences.getProperty("allowedOrientations"));
   return;
}


/**
 * render important Images (their names) of this layout
 * either as editor or as plain text
 */
function importantImages_macro(param) {
   if (param.as == "editor" /* && !this.imported*/)
      Html.textArea(this.preferences.createInputParam("importantImages", param));
   else if (this.preferences.getProperty("importantImages"))
      res.write(this.preferences.getProperty("importantImages"));
   return;
}


/**
 * overwrite the switch macro in antvillelib
 * for certain properties (but pass others thru)
 */
function switch_macro(param) {
   if (param.name == "active") {
      var currLayout = res.handlers.context.getLayout();
      return currLayout == this ? param.on : param.off;
   }
   HopObject.prototype.apply(this, [param]);
   return;
}


/**
 * macro renders a list of available locales as dropdown
 */
function localizationchooser_macro(param) {
/*   if (this.isTranslation()) {
      if (this.getLocale().country) {
         param.name = "locale";
         param.value = this.getLocale();
         Html.hidden(param);
         res.write(this.getLocale().getDisplayName());
      } else {
         renderLocaleChooser(this.getLocale(), [], this.getLocale().toString(), param);
      }
   } else { */
      renderLocaleChooser(this.getLocale(), [], null, param);
/*   }      */
   return;
}

function localizations_macro(param) {
   var param = param ? param : {};
   var localizations = this.getAllLocalizations(true);
   if (param.as == "editor") {
      var options = [];
//      var localeParent = this.getLocaleParent();
//      options.push({value: localeParent.getLocale().toString(), display: localeParent.getLocale().getDisplayName()})
      for (var i = 0; i < localizations.length; i++) {
         options.push({value: localizations[i].getLocale().toString(), display: localizations[i].getLocale().getDisplayName()});
      }
      param.name = "preferedLocaleParent";
      Html.dropDown(param, options, req.data.preferedLocaleParent);
   } else {
      var max = param.max ? parseInt(param.max, 10) : 10;
      for (var i = 0; i < Math.min(max, localizations.length); i++) {
         res.write(param.itemprefix);
         Html.link({href: localizations[i].href()}, localizations[i].getLocale().getDisplayName());
         if (i < localizations.length - 1) res.write(param.divider);
         res.write(param.itemsuffix);
      }
      if (max < localizations.length) res.write("....");
   }
}


/**
 * macro renders a list of available locales as dropdown
 */
function locale_macro(param) {
   var locale = this.getLocale();
   if (param.as == "editor" && session.user.sysadmin) {
      renderLocaleChooser(this.getLocale());
   } else if (param.linkto) {
      Html.openLink({href: this.href(param.linkto == "main" ? "" : param.linkto)});
      res.write(locale.getDisplayName());
      Html.closeLink();
   } else {
      res.write(locale.getDisplayName());
   }
   return;
}


/**
 * render list of main Layoute tasks
 */
function layoutMainTasks_macro(param) {
   var containerParam = Object.clone(param);
   var itemParam = Object.clone(param);

   var items = [
      {
         href: this.href("edit"),
         action: "Layout/edit"
      },
      {
         href: this.href("preferences"),
         action: "Layout/preferences"
      },
      {
         href: this.images.href("main"),
         action: "LayoutImageMgr/main"
      },
      {
         href: this.skins.href("main"),
         action: "SkinMgr/main"
      },
      {
         href: this.href("download"),
         action: "Layout/download"
      }
   ];

   if (path.rootlayoutmgr) {
// TODO translated Layouts currently disabled
/*      items.push({
         href: this.href("translate"),
         action: "Layout/translate"
      });
      items.push({
         href: this.href("localizations"),
         action: "Layout/localizations"
      });*/
   }

   if (path.site && path.site.layout != res.handlers.layout) {
      items.push({
         href: this.href("stopTestDrive"),
         action: "Layout/stopTestDrive"
      });
   }

   res.push();
   this._parent.renderTaskItems(items, param);
   containerParam.items = res.pop();

   this._parent.renderSkin("taskContainer", containerParam);
   return;
}


/**
 * render list of main Layoute tasks
 */
function layoutEditTasks_macro(param) {
   var containerParam = Object.clone(param);
   var itemParam = Object.clone(param);

   var items = [];
   if (session.user.trusted)
   {
      items[items.length] =
      {
         href: this.images.href("create") + "?alias=preview200&width=200&height=150&resizeto=crop",
         text: getMessage("LayoutImage.create.preview200.taskLabel"),
         action: "LayoutImage/create"
      };
      items[items.length] =
      {
         href: this.images.href("create") + "?alias=preview_full",
         text: getMessage("LayoutImage.create.preview_full.taskLabel"),
         action: "LayoutImage/create"
      };
   }
   items[items.length] =
   {
      href: this.href("preferences"),
      action: "Layout/preferences"
   };

   if (path.site && path.site.layout != res.handlers.layout) {
      items.push({
         href: this.href("stopTestDrive"),
         action: "Layout/stopTestDrive"
      });
   }

   res.push();
   this._parent.renderTaskItems(items, param);
   containerParam.items = res.pop();

   this._parent.renderSkin("taskContainer", containerParam);
   return;
}


/**
 * renders a table containing radiobuttons plus preview-pics for all allowed orientations
 */
function orientationChooser_macro(param) {
   if (!param.cols) param.cols = 3;
   var p = new Object();
   p.name = (param.name) ? param.name : "preferences_orientation";

   var tabledata = new Array();
   var allowedOrientations = this.preferences.getProperty("allowedOrientations") ? this.preferences.getProperty("allowedOrientations").split(";") : [];
   if (allowedOrientations.length <= 1) return;

   var rowCounter = 0;
   for (var i = 0; i < allowedOrientations.length; i++) {
      if (i % param.cols == 0) {
         tabledata[rowCounter] = new Array();
         rowCounter++;
      }
      p.id = allowedOrientations[i];
      p.description = getMessage("Layout.orientations." + allowedOrientations[i]);
      p.checked = (this.preferences.getProperty("orientation") == allowedOrientations[i]) ? "checked" : "";
      tabledata[rowCounter-1][i % param.cols] = this.renderSkinAsString("orientationChooserItem", p);
   }

   var attr = new Object();
   param.cols = null;
   param.name = null;
   attr.table = param;

   Html.openTag("h3");
   Html.link({name:"colors"}, getMessage("Layout.preferences.layout.header"));
   Html.closeTag("h3");
   Html.table(null, tabledata, attr);
   this._parent.separator_macro({});
   return;
}


/**
 * renders a dropdown (+ javaScript) for presetting color fields
 */
function colorPresetChooser_macro(param) {
   options = new Array();
   options[0] = {"value": "currentColorSettings", "display": ((getMessage("Layout.colorPresets.current")) ? getMessage("Layout.colorPresets.current") : "Current Color Settings") };
   if (!param.id) param.id = "colorPresetChooser";
   var p = new Object();
   p.presetsObject = "COLOR_PRESETS = { \"currentColorSettings\": { \"bgcolor\": \"" + ((this.preferences.getProperty("bgcolor")) ? this.preferences.getProperty("bgcolor") : "") + "\", \"titlecolor\": \"" + ((this.preferences.getProperty("titlecolor")) ? this.preferences.getProperty("titlecolor") : "") + "\", \"textcolor\": \"" + ((this.preferences.getProperty("textcolor")) ? this.preferences.getProperty("textcolor") : "") + "\", \"smalltextcolor\": \"" + ((this.preferences.getProperty("smallcolor")) ? this.preferences.getProperty("smallcolor") : "") + "\", \"linecolor\": \"" + ((this.preferences.getProperty("linecolor")) ? this.preferences.getProperty("linecolor") : "") + "\", \"blockcolor\": \"" + ((this.preferences.getProperty("blockcolor")) ? this.preferences.getProperty("blockcolor") : "") + "\", \"linkcolor\": \"" + ((this.preferences.getProperty("linkcolor")) ? this.preferences.getProperty("linkcolor") : "") + "\", \"alinkcolor\": \"" + ((this.preferences.getProperty("alinkcolor")) ? this.preferences.getProperty("alinkcolor") : "") + "\", \"vlinkcolor\": \"" + ((this.preferences.getProperty("vlinkcolor")) ? this.preferences.getProperty("vlinkcolor") : "") + "\" }";
   for (var i = 0; i < COLOR_PRESETS.length; i++) {
      options[(i+1)] = {"value": COLOR_PRESETS[i].name, "display": (getMessage(("Layout.colorPresets." + COLOR_PRESETS[i].name + ".title"))) ? getMessage(("Layout.colorPresets." + COLOR_PRESETS[i].name + ".title")) : COLOR_PRESETS[i].title};
      p.presetsObject = p.presetsObject + ", \"" + COLOR_PRESETS[i].name + "\" : { \"bgcolor\" : \"" + COLOR_PRESETS[i].bgcolor + "\", \"titlecolor\" : \"" + COLOR_PRESETS[i].titlecolor + "\", \"textcolor\" : \"" + COLOR_PRESETS[i].textcolor + "\", \"smalltextcolor\" : \"" + COLOR_PRESETS[i].smalltextcolor + "\", \"linecolor\" : \"" + COLOR_PRESETS[i].linecolor + "\", \"blockcolor\" : \"" + COLOR_PRESETS[i].blockcolor + "\", \"linkcolor\" : \"" + COLOR_PRESETS[i].linkcolor + "\", \"vlinkcolor\" : \"" + COLOR_PRESETS[i].vlinkcolor + "\", \"alinkcolor\" : \"" + COLOR_PRESETS[i].alinkcolor + "\" }";
   }
   p.presetsObject = p.presetsObject + " };";
   p.dropDownId = param.id;
   this.renderSkin("colorPresetChooserScript", p);
   param.onChange = "javascript:selectColorPreset()";
   Html.dropDown(param, options, 0, null);
   return;
}


/**
 * renders a dropdown (+ javaScript) for presetting font fields
 */
function fontPresetChooser_macro(param) {
   options = new Array();
   options[0] = {"value": "currentFontSettings", "display": ((getMessage("Layout.fontPresets.current")) ? getMessage("Layout.fontPresets.current") : "Current Font Settings") };
   if (!param.id) param.id = "fontPresetChooser";
   var p = new Object();
   p.presetsObject = "FONT_PRESETS = { \"currentFontSettings\": { \"titlefont\": \"" + ((this.preferences.getProperty("titlefont")) ? this.preferences.getProperty("titlefont") : "") + "\", \"titlefontsize\": \"" + ((this.preferences.getProperty("titlesize")) ? this.preferences.getProperty("titlesize") : "") + "\", \"textfont\": \"" + ((this.preferences.getProperty("textfont")) ? this.preferences.getProperty("textfont") : "") + "\", \"textfontsize\": \"" + ((this.preferences.getProperty("textsize")) ? this.preferences.getProperty("textsize") : "") + "\", \"smalltextfont\": \"" + ((this.preferences.getProperty("smallfont")) ? this.preferences.getProperty("smallfont") : "") + "\", \"smalltextfontsize\": \"" + ((this.preferences.getProperty("smallsize")) ? this.preferences.getProperty("smallsize") : "") + "\" }";
   for (var i = 0; i < FONT_PRESETS.length; i++) {
      options[(i+1)] = {"value": FONT_PRESETS[i].name, "display": (getMessage(("Layout.fontPresets." + FONT_PRESETS[i].name + ".title"))) ? getMessage(("Layout.fontPresets." + FONT_PRESETS[i].name + ".title")) : FONT_PRESETS[i].title};
      p.presetsObject = p.presetsObject + ", \"" + FONT_PRESETS[i].name + "\" : { \"titlefont\" : \"" + FONT_PRESETS[i].titlefont + "\", \"titlefontsize\" : \"" + FONT_PRESETS[i].titlefontsize + "\", \"textfont\" : \"" + FONT_PRESETS[i].textfont + "\", \"textfontsize\" : \"" + FONT_PRESETS[i].textfontsize + "\", \"smalltextfont\" : \"" + FONT_PRESETS[i].smalltextfont + "\", \"smalltextfontsize\" : \"" + FONT_PRESETS[i].smalltextfontsize + "\" }";
   }
   p.presetsObject = p.presetsObject + " };";
   p.dropDownId = param.id;
   this.renderSkin("fontPresetChooserScript", p);
   param.onChange = "javascript:selectFontPreset()";
   Html.dropDown(param, options, 0, null);
   return;
}


/**
 * renders additional editing options only for trusted users
 */
function trustedUserEditOptions_macro(param) {
   if (session.user && session.user.trusted) this.renderSkin("trustedUserEditOptions");
   return;
}

function rootEditOptions_macro(param) {
   if (this._parent._prototype == "RootLayoutMgr") this.renderSkin("rootEditOptions");
   return;
}


/**
 * renders an explanation listing all allowed orientations for this skin
 */
function allowedOrientationsHelpText_macro(param) {
   var pv = "";
   for (var i = 0; i < LAYOUT_ORIENTATIONS.length; i++) {
      pv = pv + ((i > 0) ? ";<wbr />" : "") + LAYOUT_ORIENTATIONS[i];
   }
   res.write(getMessage("Layout.allowedOrientationsHelp", { possibleValues: pv }));
   return;
}


/**
 * Renders the parentLayoutInfo.skin if a layout parent is available
 */
function parentLayoutInfo_macro(param) {
   if (this.parent) {
      this.parent.renderSkin("parentLayoutInfo", param);
   }
}


/**
 * renders the correct stylesheets for style.css depending on the orientation of the layout
 *
 * @param orientation   optional orientation key.
 *                      the default value will be stored in layout.preferences.orientation
 *                      valid values: 1c_center
 *                                    1c_full
 *                                    1c_left
 *                                    2c_center_left
 *                                    2c_full_left
 *                                    2c_left_left
 *                                    2c_center_right
 *                                    2c_full_right
 *                                    2c_left_right
 *                                    3c_full
 * @skin style_orientation_2c_left
 * @skin style_orientation_2c_right
 * @skin style_orientation_center
 * @skin style_orientation_full
 * @skin style_orientation_left
 */
function orientation_css_macro(param) {
   var orientation = this.getOrientationValues(param.orientation);
   res.write("/* " + orientation.toString() + " */\n");
   res.handlers.site.renderSkin("style_orientation_" + orientation.toString());
}


/**
 * if a preview_full image is available for this layout, render a link to open a window displaying it.
 */
function fullpreviewlink_macro(param) {
   if (this.images.get("preview_full"))
      this.renderSkin("fullPreviewLink");
}


/**
 * renders Task Items (for Site Admin)
 */
function tasks_macro(param) {
   var items = [];

   // check rights

   // build task List
   if (res.handlers.layout != this)
      items.push({href: this.href("startTestdrive"), text: getMessage("Layout.test")});
   if (!this.isDefaultLayout() && (!this.isTranslation() || !this.getLocale().variant))
      items.push({href: this._parent.href() + "?activate=" + this.alias, text: getMessage("Layout.activate")});
   items.push({href: this.href("download"), text: getMessage("Layout.export")});
   if (!this.isDefaultLayout() && this.sharedBy.size() == 0)
      items.push({href: this.href("delete"), text: getMessage("generic.delete")});
   var modItems = this.applyModuleMethods("renderLayoutTasks", [], false, false);
   if (modItems) items = items.concat(modItems);

   // render task list
   res.push();
   this._parent.renderTaskItems(items, param);
   param.items = res.pop();

   this._parent.renderSkin("taskContainer", param);
   return;
}
