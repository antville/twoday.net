
/**
 * Returns File Representation of Zip-File
 */
function getZipFile() {
  return Helma.File(app.dir, this.name + ".zip");
}


/**
 * evaluates the initialize form and calls evalInitialize on the module on root level
 * a module must be initialized by the server admin before site owners may use it
 * this function calls app.modules[name].evalInitialize() for this module
 * a module developer may use the initialize function to create db_columns, copy file,
 * manipulate layouts, set default properties, etc...
 * if something goes wrong inside evalInitialize() you should throw an error, and set res.message
 *
 * @see Module.loadModule
 */
function evalInitialize() {
   if (app.modules[this.name] && app.modules[this.name].evalInitialize) {
      try {
         app.modules[this.name].evalInitialize.apply(this, []);
      } catch (e) {
         throw(e);
         return;
      }
      HopObject.prototype.applyModuleMethod(this.name, "setMgrMenu", []);
   }

   this.loadModule();

   this.isInitialized = true;
}


/**
 * loads the language property files and skin files
 *
 */
function loadModule() {
   // extract properties- and skin-Files
   var zipFile = this.getZipFile();
   var dir = new Helma.File(app.dir);
   dir = dir.getParent();
   var installedFiles = new Array();
   if (zipFile.exists()) {
      var zip = new Zip(zipFile);
      var fileList = zip.list();
      var REGEXP_SKINS_FILEPATH = /^([a-zA-Z][a-zA-Z0-9_\-.]*)\/([a-zA-Z][a-zA-Z0-9_\-.]*\.skin)$/;
      var REGEXP_LOCALE_SKINS_FILEPATH = /^locale\/(([a-z]{2})|([a-z]{2}_[A-Z]{2})|([a-z]{2}_[A-Z]{2}_[a-zA-Z][a-zA-Z0-9_]*))\/skins\/([a-zA-Z][a-zA-Z0-9_\-.]*)\/([a-zA-Z][a-zA-Z0-9_\-.]*\.skin)$/;
      var REGEXP_PROPERTIES_FILEPATH = /^locale\/(([a-z]{2})|([a-z]{2}_[A-Z]{2})|([a-z]{2}_[A-Z]{2}_[a-zA-Z][a-zA-Z0-9_]*))\/([a-zA-Z][a-zA-Z0-9_\-.]*\.properties)$/;
      var REGEXP_STATIC_FILEPATH = /^install\/static\/([a-zA-Z0-9_\-.]*)$/;

      for (var j in fileList.toc) {
         var filePath = fileList.toc[j].name;
         // extract license
         if (filePath == this.name + "License.txt") {
            var file = new Helma.File(app.dir, filePath);
            if (file.exists()) file.remove();
            zip.extract("code/" + filePath, dir);
            installedFiles.push(file.getAbsolutePath());
         }

         // read license from file
         var appDir = new Helma.File(app.dir);
         var licenseTxtFile =  new Helma.File(appDir.getAbsolutePath(), this.name + "License.txt");
         if (licenseTxtFile.exists()) {
            this.licenseText = licenseTxtFile.readAll();
         }

         // extract all skin-files (CURRENTLY NOT NECESSARY)
/*         var match = filePath.match(REGEXP_SKINS_FILEPATH);
         if (match) {
            var protoName = match[1];
            var fileName  = match[2];
            var dir01 = new Helma.File(dir, "code");
            var dir02 = new Helma.File(dir01, protoName);
            var file = new Helma.File(dir02, fileName);
            if (file.exists()) file.remove();
            var zFile = new java.util.zip.ZipFile(zip.file);
            var entry = zFile.getEntry(protoName + "/" + fileName);
            var buf = zip.extract(protoName + "/" + fileName).data;
            var dest = new java.io.File(file.getAbsolutePath());
            if (!dest.getParentFile().exists()) dest.getParentFile().mkdirs();
            try {
               var outStream = new java.io.BufferedOutputStream(new java.io.FileOutputStream(dest));
               outStream.write(buf, 0, entry.size);
            } finally {
               outStream.close();
            }
            installedFiles.push("code/" + protoName + "/" + fileName);
         }*/
         // extract all locale skin-files
         var match = filePath.match(REGEXP_LOCALE_SKINS_FILEPATH);
         var match = filePath.match(REGEXP_STATIC_FILEPATH);
         if (match && match[1] != "CVS" && match[1] != ".svn" && match[1] != "") {
            var fileName  = match[1];
            var file = new Helma.File(getStaticDir(), fileName);
            if (file.exists()) file.remove();
            var zFile = new java.util.zip.ZipFile(zipFile);
            var entry = zFile.getEntry(filePath);
            var buf = zip.extract(filePath).data;
            var dest = new java.io.File(file.getAbsolutePath());
            if (!dest.getParentFile().exists()) dest.getParentFile().mkdirs();
            try {
               var outStream = new java.io.BufferedOutputStream(new java.io.FileOutputStream(dest));
               outStream.write(buf, 0, entry.size);
            } finally {
               outStream.close();
            }
            installedFiles.push(file.getAbsolutePath());
         }
         var match = filePath.match(REGEXP_LOCALE_SKINS_FILEPATH);
         if (match) {
            var language  = match[1];
            var protoName = match[5];
            var fileName  = match[6];
            var localPath = getLocaleDir(language).getAbsolutePath();
            var file = new Helma.File(localPath, fileName);
            if (file.exists()) file.remove();
            zip.extract("locale/" + language + "/skins/" + protoName + "/" + fileName, dir);
            installedFiles.push(file.getAbsolutePath());
         }
         // extract all language properties-files
         var match = filePath.match(REGEXP_PROPERTIES_FILEPATH);
         if (match) {
            var language = match[1];
            var fileName = match[5];
            var localPath = getLocaleDir(language).getAbsolutePath();
            var file = new Helma.File(localPath, fileName);
            if (file.exists()) file.remove();
            zip.extract("locale/" + language + "/" + fileName, dir);
            installedFiles.push(file.getAbsolutePath());
         }
      }
   }
   this.installedFiles = installedFiles.join(",");
   loadLocalization();
}


/**
 * evaluates the uninstall form and calls evalUninstall on the module
 * this function calls app.modules[name].evalUninstall() for this module
 * a module developer may use the evalUninstall function to clean up stuff,
 * that was created on evalInitialize, or evalSetup
 *
 * if something goes wrong inside evalUninstall()
 * you should throw an ModuleException
 */
function evalUninstall() {
   if (this.isInitialized && app.modules[this.name] && app.modules[this.name].evalUninstall) {
      try {
         app.modules[this.name].evalUninstall.apply(this, []);
      } catch (e) {
         if (!res.message) {
            res.message = getMessage("Module.uninstall.failedMessage", {moduleTitle: this.title});
         }
         return;
      }
      this.isInitialized = false;
   }
   var zipFile = this.getZipFile();
   if (zipFile.exists()) zipFile.remove();
   // remove all installed files
   var dir = new Helma.File(app.dir);
   dir = dir.getParent();
   var files = this.installedFiles ? this.installedFiles.split(",") : [];
   for (var i=0; i<files.length; i++) {
      var f = new Helma.File(files[i]);
      if (f.exists()) f.remove();
   }
   var redirectTo = this._parent.href("main") + "?reload=true";
   app.modules[this.name] = null;
   this.remove();
   res.redirect(redirectTo);
}


/**
 * evaluates the setup form on save
 */
function evalSetup() {
   if (app.modules[this.name] && app.modules[this.name].evalSetup) {
      try {
         app.modules[this.name].evalSetup.apply(this, []);
      } catch (e) {
         res.message = getMessage("Module.setup.failedMessage", {moduleTitle: this.title});
         return;
      }
   }
   this.isActivated = (req.data.isActivated == "1");
   // this.isInitialized = true;
   res.redirect(this._parent.href("main"));
}


/**
 * returns the title of this module
 * the title may be internationalized
 */
function getTitle() {
   var localeTitle = getMessage(this.name + ".title");
   return (localeTitle) ? localeTitle : (this.title ? this.title : this.name);
}


/**
 * returns the description of this module
 * the description may be internationalized
 */
function getDescription() {
   var localeDescription = getMessage(this.name + ".description");
   return (localeDescription) ? localeDescription : this.description;
}


/**
 * returns the header of this module
 * the header may be internationalized
 */
function getHeader() {
   if (res.handlers.site && res.handlers.site.preferences.getProperty(this.name.toLowerCase() + "header")) {
      return res.handlers.site.preferences.getProperty(this.name.toLowerCase() + "header");
   }
   var localeHeader = getMessage(this.name + ".header");

   if (localeHeader) return localeHeader;
   if (this.header) return this.header;
   return this.getTitle();
}
