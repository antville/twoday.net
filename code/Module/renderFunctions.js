
function renderSetupSpacerLine(param) {
   this.renderSkin("setupspacerline", param);
}

function renderSetupHeader(param) {
   this.renderSkin("setupheader", param);
}

function renderSetupLine(param) {
   this.renderSkin("setupline", param);
}
