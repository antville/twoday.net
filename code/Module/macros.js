
/**
 * Renders title of this Module
 */
function title_macro(param) {
   return this.getTitle();
}


/**
 * Renders description of this Module
 */
function description_macro(param) {
   return this.getDescription();
}


/**
 * Renders the management links for Modules on the root level
 * @param itemprefix   will be rendered before the link
 * @param itemsuffix   will be rendered after the link
 */
function manageLinks_macro(param) {
   if (!this.isInitialized) {
      res.write(param.itemprefix);
      this.link_macro({to: this.href("initialize"), text: getMessage("Module.initialize")});
      res.write(param.itemsuffix);

   } else {
      res.write(param.itemprefix);
      this.link_macro({to: this.href("setup"), text: getMessage("Module.setup")});
      res.write(param.itemsuffix);
   }

   res.write(param.itemprefix);
   this.link_macro({to: this.href("uninstall"), text: getMessage("Module.uninstall")});
   res.write(param.itemsuffix);

}


/**
 * Renders the ACTIVE | INACTIVE labels for Modules on the site level
 */
function statusflag_macro(param) {
   if (this.isActivated) {
      res.write("<span class=\"flagDark\" style=\"background-color:#006600;\">" + getMessage("Module.ACTIVE") + "</span>");
   } else {
      res.write("<span class=\"flagDark\" style=\"background-color:#CC3333;\">" + getMessage("Module.INACTIVE") + "</span>");
   }
}


/**
 * Applies/Renders the app.module.renderSetup() for the Module.setup form
 */
function setup_macro(param) {
   this.applyAdminModuleMethod(this.name, "renderSetup", param);
}


/**
 * Applies/Renders the app.module.renderSetup() for the Module.setup form
 */
function licenseInfo_macro(param) {

   this.renderSetupHeader({header: getMessage("Module.propertyLabel.license")});

   var param = {
      label: getMessage("Module.propertyLabel.license") + ":",
      field: this["license"]
   }
   var licenseURL = this.getLicenseURL();
   if (licenseURL) param.field = '<a href="' + licenseURL + '">' + param.field + '</a>';

   this.renderSetupLine(param);

   if (this.licenseText) {
      var param = {
         label: "",
         field: '<div style="height: 150px; overflow: auto;">' + this.licenseText.format() + '</div>'
      }
      this.renderSetupLine(param);
      this.renderSetupSpacerLine();

      if (this.license && this.license != "antville") {
         var options = [
            {value: "1", display: getMessage("generic.yes")},
            {value: "0", display: getMessage("generic.no")}
         ];

         var param = {
            label: getMessage("Module.initialize.acceptLicense") + ":",
            field: Html.dropDownAsString({name: "acceptLicense"}, options, (this.isLicenseAccepted ? "1" : "0"))
         }
         this.renderSetupLine(param);
      }
   }

   this.renderSetupSpacerLine();
}


/**
 * Applies/Renders the app.module.renderSetup() for the Module.setup form
 */
function initialize_macro(param) {
   this.applyAdminModuleMethod(this.name, "renderInitialize", param);
}


/**
 * Renders basic infos about the Module
 */
function info_macro() {
   var displayProperties = Module.displayProperties;
   for (var i = 0; i < displayProperties.length; i++) {
      if (!this[displayProperties[i]]) continue;
      var label = getMessage("Module.propertyLabel." + displayProperties[i]);
      var param = {
         label: (label ? label : displayProperties[i]) + ":",
         field: this[displayProperties[i]],
      }
      if (displayProperties[i] == "author") {
         if (this["authorURL"]) param.field = '<a href="' + this["authorURL"] + '">' + param.field + '</a>';
         if (this["authorEmail"]) param.field = param.field + '<br />mailto: <a href="mailto:' + this["authorEmail"] + '">' + this["authorEmail"] + '</a>';
      } else if (displayProperties[i] == "version") {
         if (this["state"]) param.field = param.field + ' ' + this["state"];
      }

      this.renderSetupLine(param);
   }

   this.renderSetupSpacerLine();
}


/**
 * Renders the Module activation form element
 */
function activation_macro() {
   var param = {header: getMessage("Module.activation.title", null, this)};
   this.renderSetupHeader(param);

   var options = [
      {value: "1", display: getMessage("generic.yes")},
      {value: "0", display: getMessage("generic.no")}
   ];

   var param = {
      label: getMessage("Module.setup.activate"),
      field: Html.dropDownAsString({name: "isActivated"}, options, (this.isActivated ? "1" : "0"))
   }
   this.renderSetupLine(param);
}


/**
 * Applies/Renders the app.module.renderPreferences() for the ModuleMgr.preferences form
 */
function sidebarItemHeader_macro(param) {
   if (!this.isSidebar) return;
   if (param.as == "editor") {
      var param = {
         label: "<label>" + getMessage("Module.preferences.sidebarHeader") + "</label>",
         field: Html.inputAsString({name: this.name.toLowerCase() + "header", value: this.getHeader(), size: 40})
      };
      this.renderSetupLine(param);
   } else {
      return this.getHeader();
   }
}


/**
 *
 */
function skins_macro(param) {
   var skins = app.modules[this.name].skins ? app.modules[this.name].skins : [];
   for (var i=0; i<skins.length; i++) {
      var sp = param;
      sp.key = skins[i];
      sp.module = req.data.mod;
      var splitKey = skins[i].split(".");
      var s = res.handlers.site.layout.skins.getSkin(splitKey[0], splitKey[1]);
      if (s) sp.status = s.renderSkinAsString("status");
      var desc = res.handlers.site.layout.skins.getSkinDescription("skin", sp.key, this.name);
      sp.title = ((s && s.title) ? s.title : desc[0]);
      sp.text = ((s && s.description) ? s.description : desc[1]);
      res.handlers.site.layout.skins.renderSkin("treeleaf", sp);
   }
   return;
}


/**
 * renders Task Items (for Site Admin)
 */
function tasks_macro(param) {
   var items = [];

   // check rights

   // build task List
   items.push({href: path.Mgr.href("preferences") + "?mod=" + this.name, text: getMessage("Module.preferences")});
   if (app.modules[this.name].skins && app.modules[this.name].skins.length>0)
      items.push({href: path.Mgr.href("skins") + "?mod=" + this.name, text: getMessage("Module.skins")});
   var modItems = this.applyModuleMethods("renderModuleTasks", [], false, false);
   if (modItems) items = items.concat(modItems);

   // render task list
   res.push();
   this._parent.renderTaskItems(items, param);
   param.items = res.pop();

   this._parent.renderSkin("taskContainer", param);
   return;
}
