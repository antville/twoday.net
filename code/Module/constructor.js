
/**
 * constructor for Module
 */
function constructor(name) {
   this.name = name;
   this.createtime = new Date();
}


function getLicenseURL() {
   if (!this.license) return;
   if (this.licenseURL) return this.licenseURL;
   var knownLicense = this.license.match(Module.knownLicenses);
   if (this.license && knownLicense) {
      return Module.licenseURLs[knownLicense];
   }
   return null;
}
