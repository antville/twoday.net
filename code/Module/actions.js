
/**
 * initialize action
 * the server admin has to initialize a module before site owners may use it
 * @see Module.evalInitialize
 */
function initialize_action() {
   if (this.isInitialized) res.redirect(this.href("setup"));
   res.data.title = getMessage("Module.initialize.title", null, this);
   res.data.action = this.href("initialize");

   if (req.data.initialize) {
      var licenseIsKnown = Module.knownLicenses.test(this.license);
      this.acceptedLicense = true;
      try {
         checkSecretKey();
         this.evalInitialize();
         res.redirect(this.href("setup"));
      } catch (err) {
         res.message = err + " " + err.fileName + " " + err.lineNumber;
      }
   } else if (req.data.cancel) {
      res.redirect(this._parent.href("main"));
   }

   res.data.body = this.renderSkinAsString("initialize");
   this._parent.renderMgrPage();

   return;
}


/**
 * uninstall action
 * this will trigger the Module.evalUninstall() for the Module
 * and remove the .zip File from the app-dir
 * @see Module.evalUninstall
 */
function uninstall_action() {
   res.data.title = getMessage("Module.uninstall.title", null, this);
   res.data.action = this.href("uninstall");

   if (req.data.uninstall) {
      checkSecretKey();
      this.evalUninstall();
   } else if (req.data.cancel) {
      res.redirect(this._parent.href("main"));
   }

   res.data.body = this.renderSkinAsString("uninstall");
   this._parent.renderMgrPage();

   return;
}


/**
 * setup action
 * this shows basic infos about the module and an enable/disable dropdown
 * a module may add form elements by renderSetup()
 * @see Module.evalSetup
 */
function setup_action() {
   res.data.title = getMessage("Module.setup.title", null, this);
   res.data.action = this.href("setup");

   if (req.data.submit) {
      checkSecretKey();
      this.evalSetup();
   } else if (req.data.cancel) {
      res.redirect(this._parent.href("main"));
   }

   res.data.body = this.renderSkinAsString("setup");
   this._parent.renderMgrPage();

   return;
}
