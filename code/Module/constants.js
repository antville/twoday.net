
Module.standardProperties =
   ["title", "description", "version", "state", "url",
    "author", "authorEmail", "authorUrl", "authorOrganisation",
    "license", "licenseUrl", "licenseText", "copyright",
    "isSidebar", "type", "hasSiteSettings", "skins", "isRestricted"];

Module.displayProperties =
   ["version", "author", "authorOrganisation", "copyright"];

Module.licenseURLs = {
   helma:        "http://www.helma.org/license",
   antville:     "http://www.antville.org/license",
   eigentext:    "http://www.eigentext.org/license",
   "twoday.org": "http://www.twoday.org/license"
}

Module.knownLicenses = /(antville)|(helma)|(twoday)|(twoday\.org)|(eigentext)/gi
