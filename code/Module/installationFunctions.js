
/**
 * This file contains helper function for installing (initializing) and
 * uninstalling modules, which may be used by evalInitialize() and
 * evalUninstall()
 */

/**
 * executes modModName.sql file on db
 * returns a reult object, which contains all sql messages in an array
 * @return Object
 *   .error     Boolean        indicates if an error occured
 *   .messages  Array[String]  message log
 */
function applyDbPatch() {
   var result = {};
   result.messages = [];
   var zipFile = this.getZipFile();
   var contents = Zip.extractData(zipFile.toByteArray());
   var sqlFile = contents.files["install"]["sql"][this.name + ".sql"];
   if (!sqlFile.data) return;
   var sql = new java.lang.String(sqlFile.data, 0, sqlFile.data.length);
   var con = getDBConnection("twoday");
   var lines = sql.split("\n");
   for (var i = 0; i < lines.length; i++) {
      if (lines[i] == "" || lines[i].substring(0,1) == "#")
         continue;
      var exec = con.executeCommand(lines[i]);
      if (exec != false) {
         result.messages.push("SQL: <b>"+lines[i]+"</b> "+exec+" row(s) affected.");
      } else {
         result.error = true;
         result.messages.push("SQL: <b>"+lines[i]+"</b> Error occured: this database batch might already have been called before ("+con.getLastError()+")");
      }
   }
   return result;
}


/**
 * imports a layout located in the install directory
 *
 * @
 */
function importLayout(layoutName, dirName, merge) {
   var zipFile = this.getZipFile();
   var contents = Zip.extractData(zipFile.toByteArray());
   var layout = root.layouts.get(layoutName);
   var layoutObj = contents.files["install"][dirName];
   layout.images.evalImport(layoutObj["imagedata"], layoutObj["images"]);
   return;
}
