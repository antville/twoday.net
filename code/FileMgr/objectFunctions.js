
/**
 * Checks if file fits to the minimal needs.
 *
 * @param param  Object containing the properties needed for creating a new File
 *   .rawfile      ByteArray file data passed by a web form
 *   .alias        (optional) alias to be used. otherwise we will create the alias from the uploaded file name
 *   .alttext      an alt text for this file. this will be used as the title text in the a tag.
 *   .description  a description for this file
 * @param creator User creating this image
 * @return Obj Object containing two properties:
 *             - error (boolean): true if error happened, false if everything went fine
 *             - message (String): containing a message to user
 * @throws Exception
 * @doclevel public
 */
function evalFile(param, creator) {
   if (param.uploadError) {
      // looks like the file uploaded has exceeded uploadLimit ...
      throw new Exception("fileFileTooBig");
   }
   if (!param.rawfile || param.rawfile.contentLength == 0) {
      // looks like nothing was uploaded ...
      throw new Exception("fileNoUpload");
   }
   var filesize = Math.round(param.rawfile.contentLength / 1024);
   if (this._parent.getDiskUsage() > this._parent.getDiskQuota()) {
      // disk quota has already been exceeded
      throw new Exception("siteQuotaExceeded");
   }
   var newFile = new File(creator);
   // if no alias given try to determine it
   if (!param.alias) {
      newFile.alias = buildAliasFromFile(param.rawfile, this);
   } else {
      if (!param.alias.isFileName())
         throw new Exception("noSpecialChars");
      newFile.alias = buildAlias(param.alias, this);
   }
   // store properties necessary for file-creation
   var origname = param.rawfile.getName();
   var fileext = origname.contains(".") ? origname.substring(origname.lastIndexOf(".") + 1) : "";
   newFile.filename = newFile.alias + "." + fileext.toLowerCase();
   newFile.filesize = param.rawfile.contentLength;
   newFile.mimetype = param.rawfile.contentType;
   newFile.description = stripTags(param.description);
   var dir = this._parent.getStaticDir("files");
   param.rawfile.writeToFile(dir, newFile.filename);
   if (!newFile.filename)
      throw new Exception("fileSave");
   // the file is on disk, so we add the file-object
   if (!this.add(newFile))
      throw new Exception("fileCreate", newFile.alias);
   return new Message("fileCreate", newFile.alias, newFile);
}


/**
 * Delete a file from database and disc.
 *
 * @param fileObj File-object to delete
 * @return Message "fileDelete"
 * @doclevel public
 */
function deleteFile(fileObj) {
   // first remove the file from disk
   var f = new Helma.File(this._parent.getStaticDir("files"), fileObj.filename);
   if (f && f.exists()) {
      f.remove();
   }
   fileObj.remove();
   return new Message("fileDelete");
}


/**
 * Deletes all files. This functions calls deleteFile an all files of this collection (site).
 *
 * @devnote   Why do we return Boolean instead of Message?
 * @return Boolean
 * @doclevel public
 */
function deleteAll() {
   for (var i=this.size();i>0;i--)
      this.deleteFile(this.get(i-1));
   return true;
}
