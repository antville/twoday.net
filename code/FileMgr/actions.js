
/**
 * Render list of files for a site. (20 per page)
 *
 * @req page
 * @skin File.mgrlistitem
 * @skin FileMgr.main
 * @doclevel public
 */
function main_action() {
   res.data.filelist = renderList(this, "mgrlistitem", 20, req.data.page);
   res.data.pagenavigation = renderPageNavigation(this, this.href(), 20, req.data.page);
   res.data.title = getMessage("FileMgr.main.title", {siteTitle: this._parent.title});
   res.data.body = this.renderSkinAsString("main");
   this._parent.renderPage();
   return;
}


/**
 * Render list of files that belong to the session.user for a site. (20 per page)
 *
 * @req page
 * @skin File.mgrlistitem
 * @skin FileMgr.main
 * @doclevel public
 */
function myfiles_action() {
   var ms = this._parent.members.get(session.user._id.toString());
   res.data.filelist = renderList(ms ? ms.files : [], "mgrlistitem", 20, req.data.page);
   res.data.pagenavigation = renderPageNavigation(ms ? ms.files : [], this.href(), 20, req.data.page);
   res.data.title = getMessage("FileMgr.myfiles.title", {siteTitle: this._parent.title});
   res.data.body = this.renderSkinAsString("main");
   this._parent.renderPage();
   return;
}


/**
 * Renders a form to upload a new file and adds it to this collection (site)
 *
 * @req cancel  Any value will cancel this action and redirect to this comments story. In most cases the name of the cancel button is set to "cancel".
 * @req save    Any value will cause this action to save the form data, and save the new file. In most cases the name of the submit button is set to "save".
 * @skin FileMgr.new        Form for uploading a new file
 * @see FileMgr.evalFile    This will handle the form data, and save the file
 * @doclevel public
 */
function create_action() {
   if (req.data.cancel)
      res.redirect(this.href());
   else if (req.data.save) {
      try {
         checkSecretKey();
         res.message = this.evalFile(req.data, session.user);
         res.redirect(this.href());
      } catch (err) {
         res.message = err.toString();
      }
   }

   res.data.action = this.href(req.action);
   res.data.title = getMessage("FileMgr.create.title", {siteTitle: this._parent.title});
   res.data.body = this.renderSkinAsString("new");
   this._parent.renderPage();
   return;
}
