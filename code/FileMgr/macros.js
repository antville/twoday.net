
/**
 * Returns an Array of list navigation items.
 * These are display usualy on top of a list of files.
 *
 * @return Array
 * @doclevel admin
 */
function getListNavigationItems() {
   var items = [
      {
         text: getMessage("admin.menu.contribute.files.all"),
         action: "FileMgr/main"
      },
      {
         text: getMessage("admin.menu.contribute.files.myfiles"),
         action: "FileMgr/myfiles"
      }
   ];
   return items;
}


/**
 * Returns an Array of FileManager tasks.
 *
 * @return Array
 * @doclevel admin
 */
function getFileManagerTasks() {
   var items = [
      {
         action: "FileMgr/create"
      }
   ];
   return items;
}


/**
 * Render list of FileManager tasks
 *
 * @see FileMgr.getFileManagerTasks
 * @see Mgr.renderTasks
 * @doclevel admin
 */
function fileManagerTasks_macro(param) {
   var items = this.getFileManagerTasks();
   this.renderTasks(items, param);
}
