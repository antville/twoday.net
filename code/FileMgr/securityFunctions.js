
/**
 * Permission checks (called by hopobject.onRequest())
 *
 * @param action  String, name of action
 * @param usr     User, who wants to access an action
 * @param level   Int, Membership level
 * @return Object if permission was denied <br />
 *   .message  message, if something went wrong <br />
 *   .url      recommanded redirect URL
 * @doclevel public
 */
function checkAccess(action, usr, level) {
   if (usr && usr.sysadmin) return;
   var url;
   var site = this._parent;
   try {
      if (site && !site.online) {
         checkIfLoggedIn(this.href(action));
         site.checkView(usr, level);
      }
      switch (action) {
         case "main" :
         case "myfiles" :
         case "create" :
            checkIfLoggedIn(this.href(action));
            this.checkAdd(usr, level);
            break;
         default :
            url = this.applyAllModuleMethods("checkFileMgrAccess", [action, usr, level, url]);
            break;
      }
   } catch (deny) {
      return {message: deny.toString(), url: (url ? url : (site ? site.href() : root.href()))};
   }
   return;
}


/**
 * Checks if user is allowed to add files
 *
 * @param usr    User who wants to perform the task
 * @param level  Int Permission-Level
 * @throws DenyException
 * @doclevel public
 */
function checkAdd(usr, level) {
   if (!this._parent.preferences.getProperty("usercontrib") && (level & MEMBER_MAY_ADD_FILE) == 0)
      throw new DenyException("fileAdd");
   return;
}
