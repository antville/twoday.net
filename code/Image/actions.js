
/**
 * main action
 * directly writes Image to the Response
 *
 * @doclevel public
 */
function main_action() {
   var site = this.layout ? this.layout.site : this.site;
   if ((this.layout && this.layout.site && this.layout.site.online) ||
       (this.site && this.site.online)) {
      res.redirect(this.getUrl());
   } else if (this.layout && this.layout.site && !this.layout.site.online) {
      var loc = this.layout.site.alias + Helma.File.separator + "layouts" +
                Helma.File.separator + this.layout.alias +
                Helma.File.separator + this.filename;
   } else {
      var loc = site.alias + Helma.File.separator + "images" +
                Helma.File.separator + this.filename;
   }
   res.contentType = this.mimetype;
   res.forward(loc);
}


/**
 * thumbnail action
 * directly writes Thumbnail Image to the Response
 */
function thumbnail_action() {
   var img = this.thumbnail ? this.thumbnail : this;
   img.main_action();
   return;
}


/**
 * info action (former main action)
 */
function info_action() {
   res.data.body = this.renderSkinAsString("info");

   if (path.LayoutImage) {
      res.data.title = getMessage("LayoutImage.main.title", {imageTitle: this.filename});
      res.data.sidebar01 = this._parent.renderSkinAsString("mainSidebar");
      this._parent.renderMgrPage();
   } else {
      res.handlers.context.renderPage();
   }
   return;
}


/**
 * edit action
 */
function edit_action() {
   if (req.data.cancel)
      res.redirect(this.href());
   else if (req.data.save) {
      checkSecretKey();
      res.message = this.evalImg(req.data, session.user);
      res.redirect(this.href("info"));
   }

   res.data.action = this.href(req.action);
   res.data.body = this.renderSkinAsString("edit");

   if (path.LayoutImage) {
      res.data.sidebar01 = this._parent.renderSkinAsString("mainSidebar");
      this._parent.renderMgrPage();
   } else {
      res.handlers.context.renderPage();
   }
   return;
}


/**
 * delete action
 */
function delete_action() {
   if (req.data.cancel)
      res.redirect(path.ImageMgr.href());
   else if (req.data.remove) {
      try {
         checkSecretKey();
         var url = this._parent.href();
         res.message = this._parent.deleteImage(this);
         res.redirect(url);
      } catch (err) {
         res.message = err.toString();
      }
   }

   res.data.action = this.href(req.action);
   res.data.title = getMessage(this._prototype + ".delete.title", {imageAlias: this.alias});
   var skinParam = {
      description: getMessage("Image.deleteDescription"),
      detail: this.alias
   };
   res.data.body = this.renderSkinAsString("delete", skinParam);
   if (this._parent._prototype == "LayoutImageMgr") {
      this._parent.renderMgrPage("LayoutImage");
   } else {
      res.handlers.context.renderPage()
   }
   return;
}


/**
 * action for creating new Image objects
 */
function replace_action() {
   if (req.data.cancel) {
      res.redirect(this.href());
   } else if (req.data.save) {
      // check if a url has been passed instead of a file upload. hw
      if ((!req.data.rawimage || req.data.rawimage.contentLength == 0) && req.data.url)
          req.data.rawimage = getURL(req.data.url);
      try {
         checkSecretKey();
         // var result = this.evalImg(req.data, session.user);
         var result = this.evalReplace(req.data, session.user)

         res.message = result.toString();
         session.data.referrer = null;
         res.redirect(result.obj.href("info"));
      } catch (err) {
         res.message = err.toString();
      }
   } else {
      // first request: initialize
      req.data.width = this.width;
      req.data.height = this.height;
      req.data.resizeto = "crop";
   }

   res.data.action = this.href(req.action);
   res.data.title = getMessage("Image.replace.title", {imgTitle: this.alias});
   res.data.body = this.renderSkinAsString("replace");

   if (path.LayoutImage) {
      res.data.sidebar01 = this.renderSkinAsString("replaceSidebar");
      this._parent.renderMgrPage();
   } else {
      res.handlers.context.renderPage();
   }

   return;
}


/**
 * Custom action which renders the skin Image.custom. 
 * Has the same security check as Image.main_action.
 */
function custom_action() {
   var site = this.site;
   var sk = (site && site.layout.skins.get("Image")) ? site.layout.skins.get("Image").get("custom") : null;
   if (sk) {
      this.renderSkin("custom");
   } else {
      res.redirect(this.href());
   }
   return;
}
