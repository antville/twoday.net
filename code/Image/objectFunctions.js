
/**
 * Constructor function for image objects.
 * Will set creator and createtime.
 *
 * @param creator  User
 * @doclevel public
 */
function constructor(creator) {
   this.creator = creator;
   this.createtime = new Date();
}


/**
 * Save an image as file on local disk,
 * but before check if image should be resized.
 * invokes Image.manipulateImageSize to perform resizing
 *
 * @param rawimage   ByteArray uploaded image
 * @param dir        File Object representing the destination directory
 * @param param      Object with aditional options, will be passed to Image.manipulateImageSize
 *   .webpublishingwizard   data provieded by the windows webpublishing wizard has to be handled different
 * @see Image.manipulateImageSize
 * @param return Helma.File  as saved on disk
 * @doclevel private
 */
function save(rawimage, dir, param) {
   // determine filetype of image (one could do this also by checking the mimetype)
   if (param.webpublishingwizard) {
      this.mimetype = getMimeType(rawimage.getName().split(".").pop());
   } else {
      this.mimetype = rawimage.contentType;
   }

   var img = this.manipulateImageSize(rawimage, param);

   // finally we try  to save the resized image
   try {
      if (img) {
         img.saveAs(dir.getPath() + Helma.File.separator + this.filename);
         img.dispose();
      } else {
         rawimage.writeToFile(dir.getPath(), this.filename);
      }
   } catch (err) {
      app.log("Error in image.save(): can't save image to " + dir.getPath() + Helma.File.separator + this.filename + ": " + err + " " + err.lineNumber + " " + err.fileName);
      throw new Exception("imageSave");
   }
   var f = new Helma.File(dir.getPath(), this.filename);
   this.filesize = f.getLength();
   return f;
}


/**
 * Performs resize - crop of this image
 * (should be extended to rotate - resize - crop - filter)
 * and returns the result (but doesn't save it to disk).
 * If there was no resizing performed this function will return null.
 *
 * @param rawimage   ByteArray uploaded image
 * @param dir        File Object representing the destination directory
 * @param param      Object holds information on how to save / manipulate the binary image
 *   param.width       target width of the resized image
 *   param.height      target height of the resized image
 *   param.resizeto    max,crop,scale,exact,no
 *                       max ...   reduzes size to max w/h
 *                       crop ...  scales img to w/h, keeps ratio and crops the img
 *                       scale ... scales img to w/h, keeps ratio
 *                                 this is the same like max, but it will also upscale the img
 *                       exact ... scales img to exactly w/h, won't keep ratio
 *                       no ...    won't do anything
 *   para.maxheight    deprecated, maximum width to resize image to
 *   para.maxwidth     deprecated, maximum height to resize image to
 * @see Image.save
 * @return Helma.Image or NULL
 * @doclevel private
 */
function manipulateImageSize(rawimage, param) {
   if (!param) param = {resizeto: "no"};
   if (!param.resizeto) param.resizeto = "max";

   // check for animated GIFs, since these are currently not resized!
   var imageInfo = new Packages.helma.image.ImageInfo();
   imageInfo.setDetermineImageNumber(true);
   var inp = new java.io.ByteArrayInputStream(rawimage.getContent());
   imageInfo.setInput(inp);
   imageInfo.check();
   var mimetype = imageInfo.getMimeType();
   if (imageInfo.getNumberOfImages() > 1) param.resizeto = "no";

   var img = new Helma.Image(rawimage.getContent());
   var currentWidth = img.getWidth();
   var currentHeight = img.getHeight();
   this.width = currentWidth;
   this.height = currentHeight;

   // rewriting deprecated parameters
   // param.maxsize / param.maxwidth
   if (param.maxwidth || param.maxheight) {
      param.width = param.maxwidth;
      param.height = param.maxheight;
      param.resizeto = "max";
   }

   // RESIZE image
   // calculate targetWidth & targetHeight
   var targetWidth;
   var targetHeight;
   var keepRatio = true;
   // max,crop
   if (param.resizeto == "exact") {
      targetWidth = param.width ? parseInt(param.width, 10) : currentHeight;
      targetHeight = param.height ? parseInt(param.height, 10) : currentWidth;
      keepRatio = false;
   } else if (param.resizeto == "max") {
      targetWidth = (param.width && parseInt(param.width, 10) < currentWidth) ? parseInt(param.width, 10) : currentWidth;
      targetHeight = (param.height && parseInt(param.height, 10) < currentHeight) ? parseInt(param.height, 10) : currentHeight;
   } else if (param.resizeto == "scale" || param.resizeto == "crop") {
      targetWidth = (param.width) ? parseInt(param.width, 10) : currentWidth;
      targetHeight = (param.height) ? parseInt(param.height, 10) : currentHeight;
   } else {
      targetWidth = currentWidth;
      targetHeight = currentHeight;
   }

   if (keepRatio && (targetWidth / targetHeight != currentWidth / currentHeight)) {
      if ((targetWidth / targetHeight > currentWidth / currentHeight)) {
         if (param.resizeto == "crop") {
            targetHeight = Math.round(targetWidth * (currentHeight / currentWidth), 10);
         } else {
            targetWidth = Math.round(targetHeight * (currentWidth / currentHeight), 10);
         }
      } else {
         if (param.resizeto == "crop") {
            targetWidth = Math.round(targetHeight * (currentWidth / currentHeight), 10);
         } else {
            targetHeight = Math.round(targetWidth * (currentHeight / currentWidth), 10);
         }
      }
   }

   // perform resizing
   if (targetWidth != currentWidth || targetHeight != currentHeight) {
      try {
         img.resize(targetWidth, targetHeight);
         if (mimetype == 'image/gif') {
            img.reduceColors(256);
         }
      } catch (err) {
         throw new Exception("imageResize");
      }
      currentWidth = targetWidth;
      currentHeight = targetHeight;
   }

   // CROP image
   if (param.resizeto == "crop") {
      cropWidth = param.width ? parseInt(param.width, 10) : currentWidth;
      cropHeight = param.height ? parseInt(param.height, 10) : currentHeight;
      // perform cropping
      var wf = currentWidth / cropWidth;
      var hf = currentHeight / cropHeight;
      if (wf > hf) {
         var x = Math.round((currentWidth - cropWidth) / 2);
         var y = 0;
      } else {
         var x = 0;
         var y = Math.round((currentHeight - cropHeight) / 2);
      }
      if (param.cropx && (parseInt(param.cropx, 10) + cropWidth) < currentWidth) x = parseInt(param.cropx, 10);
      if (param.cropy && (parseInt(param.cropy, 10) + cropHeight) < currentHeight) y = parseInt(param.cropy, 10);
      if (cropWidth != currentWidth || cropHeight != currentHeight) {
         try {
            img.crop(x, y, cropWidth, cropHeight);
         } catch (err) {
            throw new Exception("imageCrop");
         }
         currentWidth = cropWidth;
         currentHeight = cropHeight;
      } else {
         // if there was no resizing performed we return null
         if (this.width == targetWidth && this.height == targetHeight) {
            return null;
         }
      }
   } else {
      // if there was no resizing performed we return null
      if (this.width == targetWidth && this.height == targetHeight) {
         return null;
      }
   }

   this.width = currentWidth;
   this.height = currentHeight;

   return img;
}


/**
 * Checks if new Image-parameters are correct ...
 *
 * @param param Obj Object containing the form values
 *
 * @param modifier User-Object modifying this image
 *
 * @return Obj Object containing two properties:
 *             - error (boolean): true if error happened, false if everything went fine
 *             - message (String): containing a message to user
 */
function evalImg(param, modifier) {
   this.alttext = stripTags(param.alttext);
   this.modifier = modifier;
   this.modifytime = new Date();
   if (this.thumbnail) {
      this.thumbnail.alttext = this.alttext;
      this.thumbnail.modifytime = this.modifytime;
      this.thumbnail.modifier = this.modifier;
   }
   // check name of topic (if specified)
   var topicName = null;
   if (param.topic) {
      param.topic = stripTags(param.topic);
      if (param.topic.contains("\\") || param.topic.contains("/") || param.topic.contains("?") || param.topic.contains("+"))
         throw new Exception("topicNoSpecialChars");
      topicName = param.topic;
   } else if (param.addToTopic)
      topicName = stripTags(param.addToTopic);
   this.topic = topicName;
   return new Message("update");
}


/**
 * function creates a thumbnail of this image
 * does nothing if the image uploaded is smaller than 100x100px
 * @return Boolean true in any case ...
 */
function createThumbnail() {
   if (this.layout) {
      var dir = this.layout.getStaticDir();
      var thumb = new LayoutImage(this.creator);
   } else {
      var dir = this.site.getStaticDir("images");
      var thumb = new Image(this.creator);
   }
   thumb.site = this.site;
   thumb.layout = this.layout;
   thumb.filename = this.filename.substring(0, this.filename.lastIndexOf(".")) + "_small" + this.filename.substring(this.filename.lastIndexOf("."));
   var rawimage = new Packages.helma.util.MimePart(thumb.filename, this.getFile().toByteArray(), this.mimetype);
   thumb.save(rawimage, dir, {width: THUMBNAILWIDTH, height: THUMBNAILHEIGHT, resizeto: "max"});
   thumb.alttext = this.alttext;
   thumb.alias = this.alias;
   thumb.parent = this;
   this.thumbnail = thumb;
   return;
}


/**
 * function creates a thumbnail of this image
 * does nothing if the image uploaded is smaller than 100x100px
 * @param uploaded image
 * @return Boolean true in any case ...
 */
function createPreview(rawimage, dir) {
   var thumb = (this.site ? new Image(this.creator) : new LayoutImage(this.creator));
   thumb.site = this.site;
   thumb.layout = this.layout;
   thumb.filename = this.filename.substring(0, this.filename.lastIndexOf(".")) + "_small" + this.filename.substring(this.filename.lastIndexOf("."));
   thumb.save(rawimage, dir, {width: THUMBNAILWIDTH, height: THUMBNAILHEIGHT, resizeto: "max"});
   thumb.alttext = this.alttext;
   thumb.alias = this.alias;
   thumb.parent = this;
   this.thumbnail = thumb;
   return;
}


/**
 * return the call to the client-side popup-script
 * for image-object
 * @return String call of popup-script
 */
function getPopupUrl() {
   res.push();
   res.write("javascript:openPopup('");
   res.write(this.getUrl());
   res.write("',");
   res.write(this.width);
   res.write(",");
   res.write(this.height);
   res.write(");return false;");
   return res.pop();
}


/**
 * return the url of the image
 */
function getUrl() {
   if (this.site.online) {
      res.push();
      this.site.staticUrl("images/");
      res.write(this.filename);
      return res.pop();
   } else {
      if (this.parent) {
         return this.parent.href("thumbnail");
      } else {
         return this.href();
      }
   }
}


/**
 * return the image file on disk
 * @return Object File object
 */
function getFile() {
   if (this.layout) {
      return new Helma.File(this.layout.getStaticDir(), this.filename);
   } else {
      return new Helma.File(this.site.getStaticPath("images"), this.filename);
   }
}


/**
 * dump an image to a zip file passed as argument
 * @return Object HopObject containing the metadata of the image(s)
 */
function dumpToZip(z) {
   var data = new HopObject();
   if (this.thumbnail)
      data.thumbnail = this.thumbnail.dumpToZip(z);
   data.alias = this.alias;
   data.filename = this.filename;
   data.mimetype = this.mimetype;
   data.filesize = this.filesize;
   data.width = this.width;
   data.height = this.height;
   data.alttext = this.alttext;
   data.createtime = this.createtime;
   data.modifytime = this.modifytime;
   data.exporttime = new Date();
   data.creator = this.creator ? this.creator.name : null;
   data.modifier = this.modifier ? this.modifier.name : null;
   // add the image file to the zip archive
   z.add(this.getFile(), "images");
   return data;
}


/**
 * function checks if image fits to the minimal needs
 * @param Obj Object containing the properties needed for creating a new Image
 * @param Obj User-Object creating this image
 * @return Obj Object containing two properties:
 *             - error (boolean): true if error happened, false if everything went fine
 *             - message (String): containing a message to user
 */
function evalReplace(param, creator) {
   if (param.uploadError) {
      // looks like the file uploaded has exceeded uploadLimit ...
      throw new Exception("imageFileTooBig");
   } else if (!param.rawimage || param.rawimage.contentLength == 0) {
      // looks like nothing was uploaded ...
      throw new Exception("imageNoUpload");
   } else if (param.rawimage && (!param.rawimage.contentType || !evalImgType(param.rawimage.contentType))) {
      // whatever the user has uploaded wasn't recognized as an image
      throw new Exception("imageNoImage");
   }
   var filesize = Math.round(param.rawimage.contentLength / 1024);
   var currentFileSize = this.filesize;
   if (this.site && this.site.getDiskUsage() + filesize - currentFileSize > this.site.getDiskQuota()) {
      // disk quota has already been exceeded
      throw new Exception("siteQuotaExceeded");
   }
   if (this.layout && this.layout.site && this.layout.site.getDiskUsage() + filesize - currentFileSize > this.layout.site.getDiskQuota()) {
      // disk quota has already been exceeded
      throw new Exception("siteQuotaExceeded");
   }

   if (this.alias == "icon") {
      param.resizeto = "crop";
      param.width = "48";
      param.height = "48";
   }
   if (this.alias == "favicon") {
      param.resizeto = "crop";
      param.width = "16";
      param.height = "16";
   }

   // save/resize the image
   var dir = (this.site) ? this.site.getStaticDir("images") : this._parent._parent.getStaticDir();
   this.save(param.rawimage, dir, param);

   // the fullsize-image is stored, so we check if a thumbnail should be created too
   if (this.width > THUMBNAILWIDTH || this.height > THUMBNAILHEIGHT) {
      this.createThumbnail();
   } else {
      if (this.thumbnail) {
         this.thumbnail._parent.deleteImage(this.thumbnail);
      }
   }
   
   var result = new Message("imageReplace", this.alias, this);
   result.url = this.href();
   return result;
}
