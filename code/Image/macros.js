
/**
 * macro rendering alias of image
 */
function alias_macro(param) {
   if (param.as == "editor")
      Html.input(this.createInputParam("alias", param));
   else
      res.write(this.alias);
   return;
}


/**
 * macro rendering alternate text of image
 */
function alttext_macro(param) {
   if (param.as == "editor")
      Html.textArea(this.createInputParam("alttext", param));
   else if (this.alttext)
      res.write(this.alttext);
   return;
}


/**
 * macro renders the width of the image
 */
function width_macro(param) {
   res.write(this.width);
   return;
}


/**
 * macro renders the height of the image
 */
function height_macro(param) {
   res.write(this.height);
   return;
}


/**
 * macro rendering filesize
 */
function filesize_macro(param) {
   res.write((this.filesize / 1024).format("###,###") + " KB");
   return;
}


/**
 * macro renders the url to this image
 */
function url_macro(param) {
   res.write(this.getUrl());
   return;
}


/**
 * Renders the name of the ImageTopic this Image belongs to.
 * Either as link, image (if an image entiteld by the
 * topic name is available) or plain text.
 *
 * @param as "link", "image", "text"
 * @param imgprefix If as=="image" then the Image imgprefix + topicName
 *                  is tried to be fetched. Default='imagetopic_'.
 * @doclevel public
 */
function topic_macro(param) {
   if (!this.topic) return;
   if (param.as == "link") {
      Html.openLink({href: this.site.images.topics.href(this.topic)});
      res.write(this.topic);
      Html.closeLink();
   } else if (param.as == "image") {
      if (!param.imgprefix) param.imgprefix = "imagetopic_";
      delete(param.imgprefix);
      var img = getPoolObj(param.imgprefix + this.topic, "images");
      if (!img) return;
      Html.openLink(this.site.images.topics.href(this.topic));
      renderImage(img.obj, param)
      Html.closeLink();
   } else {
      res.write(this.topic);
   }
   return;
}


/**
 * render a link to image-edit
 */
function editlink_macro(param) {
   if (session.user) {
      try {
         this.checkEdit(session.user, res.meta.memberlevel);
      } catch (deny) {
         return;
      }
      Html.openLink({href: this.href("edit")});
      if (param.image && this.site.images.get(param.image))
         renderImage(this.site.images.get(param.image), param);
      else
         res.write(param.text ? param.text : getMessage("generic.edit"));
      Html.closeLink();
   }
   return;
}


/**
 * render a link to delete action
 */
function deletelink_macro(param) {
   if (session.user) {
      try {
         this.checkDelete(session.user, res.meta.memberlevel);
      } catch (deny) {
         return;
      }
      Html.openLink({href: this.href("delete")});
      if (param.image && this.site.images.get(param.image))
         renderImage(this.site.images.get(param.image), param);
      else
         res.write(param.text ? param.text : getMessage("generic.delete"));
      Html.closeLink();
   }
   return;
}


/**
 * render the image-tag (link to main action if image
 * is a thumbnail)
 */
function show_macro(param) {
   var img = this;
   // if we have a thumbnail, display that
   if (param.as == "thumbnail" && this.thumbnail) {
      var url = this.href("info");
      img = this.thumbnail;
   } else {
      var url = img.getUrl();
   }
   delete param.what;
   delete param.as;
   param.src = img.getUrl();
   Html.openLink({href: url});
   renderImage(img, param);
   Html.closeLink();
   return;
}


/**
 * macro renders the name of the gallery this image belongs to
 */
function gallery_macro(param) {
   if (!this.topic)
      return;
   if (!param.as || param.as == "text")
      res.write(this.topic);
   else if (param.as == "link") {
      Html.link({href: path.Site.images.topics.href(this.topic)},
                param.text ? param.text : this.topic);
   } else if (param.as == "image") {
      if (!param.imgprefix)
         param.imgprefix = "topic_";
      var img = getPoolObj(param.imgprefix + this.topic, "images");
      if (!img)
         return;
      Html.openLink({href: path.Site.topics.href(this.topic)});
      renderImage(img.obj, param)
      Html.closeLink();
   }
   return;
}


/**
 * render the code for embedding this image
 */
function code_macro(param) {
   res.write("&lt;% ");
   res.write(this instanceof LayoutImage ? "layout.image" : "image");
   res.write(" name=\"" + this.alias + "\" %&gt;");
   return;
}


/**
 * render a link to delete action
 * calls image.deletelink_macro, but only
 * if the layout in path is the one this image
 * belongs to
 */
function replacelink_macro(param) {
   if (session.user) {
      try {
         this._parent.checkAdd(session.user, res.meta.memberlevel);
      } catch (deny) {
         return;
      }
      Html.openLink({href: this.href("replace")});
      if (param.image && this.site.images.get(param.image))
         renderImage(this.site.images.get(param.image), param);
      else
         res.write(param.text ? param.text : getMessage("generic.replace"));
      Html.closeLink();
   }
   return;
}


/**
 * render the image-tag (link to main action if image
 * is a thumbnail)
 */
function thumbnail_macro(param) {
   // if we have a thumbnail, display that
   var img = this.thumbnail ? this.thumbnail : this;
   param.src = img.getUrl();
   renderImage(img, param);
   return;
}


/**
 * renders Task Items
 */
function tasks_macro(param) {
   var items = [];

   // check rights
   var mayView = true;
   var mayEdit = true;
   var mayDelete = true;
   try { this.checkView(session.user, res.meta.memberlevel); } catch (deny) { mayView = false; }
   try { this.checkEdit(session.user, res.meta.memberlevel); } catch (deny) { mayEdit = false; }
   try { this.checkDelete(session.user, res.meta.memberlevel); } catch (deny) { mayDelete = false; }

   // build task List
   if (mayView) items.push({href: this.getUrl(), text: getMessage("generic.view")});
   if (mayEdit) items.push({href: this.href("edit"), text: getMessage("generic.edit")});
   if (mayDelete) items.push({href: this.href("delete"), text: getMessage("generic.delete")});
   if (mayEdit) items.push({href: this.href("replace"), text: getMessage("generic.replace")});
   var modItems = this.applyModuleMethods("renderImageTasks", [], false, false);
   if (modItems) items = items.concat(modItems);

   // render task list
   res.push();
   this._parent.renderTaskItems(items, param);
   param.items = res.pop();

   this._parent.renderSkin("taskContainer", param);
   return;
}
