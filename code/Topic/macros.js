
/**
 * macro rendering title of topic
 */
function title_macro(param) {
   if (param.as == "link") {
      Html.link({href: this.href()}, this.groupname);
   } else {
      res.write(this.groupname);
   }
}


/**
 * Display a link to let the user add a new story
 * to this topic.
 */
function addstory_macro (param) {
   if (session.user) {
      try {
         path.Site.stories.checkAdd(session.user, res.meta.memberlevel);
      } catch (deny) {
         return;
      }
      param.linkto = "create";
      param.urlparam = "topic=" + this.groupname;
      Html.openTag("a", path.Site.stories.createLinkParam(param));
      if (param.text)
         res.format(param.text);
      else
         res.write(getMessage("Topic.addStoryToTopic"));
      Html.closeTag("a");

      Html.tag("br");
      Html.link({href: this.href("edit")}, getMessage("Topic.editTopic"));
   }
   return;
}


/**
 * Display a link to let the user add a new Image to this topic
 */
function addimage_macro (param) {
   if (session.user) {
      try {
         path.Site.images.checkAdd(session.user, res.meta.memberlevel);
      } catch (deny) {
         return;
      }
      param.linkto = "create";
      param.urlparam = "topic=" + this.groupname;
      Html.openTag("a", path.Site.images.createLinkParam(param));
      if (param.text)
         res.format(param.text);
      else
         res.write(getMessage("ImageTopicMgr.addImageToGallery"));
      Html.closeTag("a");

      Html.tag("br");
      Html.link({href: this.href("edit")}, getMessage("ImageTopic.editTopic"));
   }
   return;
}


/**
 * Display link to edit topic
 */
function editlink_macro (param) {
   try {
      this.checkEdit(session.user, res.meta.memberlevel);
   } catch (deny) {
      return;
   }

   Html.link({href: this.href("edit")}, param.text || getMessage("generic.rename"));
}
