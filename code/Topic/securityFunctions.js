
/**
 * Permission checks (called by hopobject.onRequest())
 *
 * @param action  String, name of action
 * @param usr     User, object
 * @param level   Int, Membership level
 * @see Site.checkView
 * @return Obj
 *   .message  message, if something went wrong
 *   .url      recommanded redirect URL
 * @doclevel public
 */
function checkAccess(action, usr, level) {
   if (usr && usr.sysadmin) return;
   var url;
   var site = path.Site;
   try {
      if (site && !site.online) {
         checkIfLoggedIn(this.href(action));
         site.checkView(usr, level);
      }
      switch (action) {
         case "main" :
         case "index_rdf" :
         case "rss" :
            path.Site.checkView(usr, level);
            break;
         case "edit" :
            this.checkEdit(usr, level);
            break;
         default :
            url = this.applyAllModuleMethods("checkTopicAccess", [action, usr, level, url]);
            break;
      }
   } catch (deny) {
      return {message: deny.toString(), url: (url ? url : (site ? site.href() : root.href()))};
   }
   return;
}


/**
 * check if user is allowed to edit a topic
 * @param Obj Userobject
 * @param Int Permission-Level
 * @return String Reason for denial (or null if allowed)
 */
function checkEdit(usr, level) {
   if (usr == null ||  (level & MEMBER_MAY_EDIT_ANYSTORY) == 0) {
      throw new DenyException("general");
   }
}
