
/**
 * RSS 1.0 Topic Feed for specific topic
 */
function index_rdf_action() {
  if (this._parent._prototype == "ImageTopicMgr") return;
  this._parent._parent.writeRSSFeedToResponse("topic." + this.groupname);
  return;
}


/**
 * Redirect to correct location of RSS Feed
 */
function rss_action() {
   res.redirect(this.href("index.rdf"));
}

/**
 *  
 */
function edit_action() {
   if (req.data.cancel) {
      // redirect back to topic list 
      res.redirect(this._parent._parent.href("topics"));
   } else if (req.data.save) {      
      try {         
         checkSecretKey();
         var result = this.evalTopic(req.data, session.user);
         res.message = result.toString();
         res.redirect(result.url);
      } catch (err) {
         res.message = err.toString();
      }
   }

   res.data.action = this.href(req.action);

   if (path.imagemgr)
      res.data.body = this.renderSkinAsString("galleryEdit");
   else 
      res.data.body = this.renderSkinAsString("edit");
   path.site.renderPage();
}
