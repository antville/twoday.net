
/**
 * Return either the title of the story or
 * the id prefixed with standard display name
 * to be used in the global linkedpath macro
 * @see hopobject.getNavigationName()
 */
function getNavigationName () {
   return this.groupname;
}


/**
 * check if topicname is ok; update topics of all related (online) stories
 * @param Obj Object
     .topicname new topicname (user input)
 * @modifier Obj User-Object modifying this Topic
 * @return Obj Object containing two properties:
 *             - error (boolean): true if error happened, false if everything went fine
 *             - message (String): containing a message to user
 */
function evalTopic(param, modifier) {
   var newTopicName = param.topicname;
   var oldTopicName = this._id;
   var topicMgr = this._parent;

   newTopicName = stripTags(newTopicName);
   if (newTopicName.contains("\\") || newTopicName.contains("/") || newTopicName.contains("?") || newTopicName.contains("+"))
      throw new Exception("topicNoSpecialChars");
   if (topicMgr[newTopicName] ||
         topicMgr[newTopicName + "_action"]) {
      throw new Exception("topicReservedWord");
   }
   if (newTopicName == "") {
      newTopicName = null;
   }
   // go through story collection and update newTopicName
   if (oldTopicName != newTopicName) {
      var topicSize = this.size();
      for (var i = topicSize - 1; i >= 0; i--) {
         var story = this.get(i);
         story.topic = newTopicName;
         this.removeChild(story);
      }
      if (topicMgr._prototype == "TopicMgr") {
         topicMgr.getSite().updateStaticRSSFeed("topic." + oldTopicName);
         topicMgr.getSite().updateAffectedStaticRSSFeeds(newTopicName);
      }
   }
   var result = new Message("update");
   result.url = topicMgr.href();
   result.id = this._id;
   return result;
}
