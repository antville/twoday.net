
/**
 * main action
 */
function main_action() {
   res.data.title = getMessage("TopicMgr.main.title", {title: this._parent.title});   
   res.data.body = this.renderSkinAsString ("main");
   path.site.renderPage();
}
