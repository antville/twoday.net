
/**
 * Overwrite link macro to use groupname.
 * FIXME: (???) No fancy options.
 *
 * @overrides HopObject.link_macro
 */
function link_macro () {
   Html.link({href: this.href()}, this.groupname);
   return;
}


/**
 * Renders the groupname, which is the day's date formated as yyyyMMdd as formatted date
 *
 * @param format   Java SimpleDateFormat pattern, default='yyyy-MM-dd HH:mm'
 * @see http://java.sun.com/j2se/1.4.1/docs/api/java/text/SimpleDateFormat.html
 * @see Global.formatTimestamp
 * @doclevel public
 */
function date_macro(param) {
   var ts = this.groupname.toDate("yyyyMMdd", this._parent.getTimeZone());
   try {
      return formatTimestamp(ts, param.format);
   } catch (err) {
      return "[" + getMessage("error.invalidDatePattern") + "]";
   }
}
