
/**
 * Renders a single day in the archive. Also used by image topics to render an image gallery. (This is subject to change in the near future 27.09.2004)
 *
 * @skin Day.main
 * @skin ImageTopicMgr.imagetopics
 * @doclevel public
 */
function main_action() {
   if (this._prototype.toLowerCase() == "day" && !path.Site.preferences.getProperty("archive"))
      res.redirect(path.Site.href());

   res.data.title = path.Site.title + ": ";
   this.renderStorylist(parseInt(req.data.start, 10));
   if (this._prototype.toLowerCase() == "day") {
      var ts = this.groupname.toDate("yyyyMMdd", this._parent.getTimeZone());
      res.data.title += formatTimestamp(ts, "dd MMMM yyyy");
   } else
      res.data.title += this.groupname;
   if (this._parent._prototype == "ImageTopicMgr")
      res.data.body = this.renderSkinAsString("imagetopic");
   else
      res.data.body = this.renderSkinAsString("main");
   path.Site.renderPage();
   return;
}
