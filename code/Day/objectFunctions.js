
/**
 * Deletes all childobjects of a day (recursive!)
 *
 * @doclevel private
 */
function deleteAll() {
   for (var i=this.size();i>0;i--) {
      var story = this.get(i-1);
      story.deleteAll();
      story.remove();
   }
   return true;
}


/**
 * Return the groupname of a day-object formatted as
 * date-string to be used in the global linkedpath macro
 *
 * @overrides HopObject.getNavigationName
 * @doclevel private
 */
function getNavigationName () {
   var ts = this.groupname.toDate("yyyyMMdd", this._parent.getTimeZone());
   return formatTimestamp(ts, "EEEE, dd.MM.yyyy");
}
