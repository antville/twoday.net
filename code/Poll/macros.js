
/**
 * macro renders a poll's title
 * (either as text or editor)
 */
function title_macro(param) {
   if (param.as == "editor") {
      var param2 = this.createInputParam("title", param);
      param2.name = "polltitle";
      if (req.data.polltitle) param2.value = req.data.polltitle;
      Html.input(param2);
   } else {
      res.write(this.title);
   }
   return;
}


/**
 * macro renders a poll's question
 * (either as text or editor)
 */
function question_macro(param) {
   if (param.as == "editor")
      Html.textArea(this.createInputParam("question", param));
   else
      res.write(this.question);
   return;
}


/**
 * macro renders one choice of a poll
 * (either as text or as editor)
 */
function choices_macro(param) {
   var vote;
   if (session.user && this.votes.get(session.user._id.toString()))
      vote = this.votes.get(session.user._id.toString()).choice;
   for (var i=0; i<this.size(); i++) {
      var choice = this.get(i);
      param.name = "choice";
      param.title = renderSkinAsString(createSkin(choice.title));
      param.value = choice._id;
      param.checked = "";
      if (choice == vote)
         param.checked = " checked=\"checked\"";
      res.write(choice.renderSkinAsString("main", param));
   }
   return;
}


/**
 * macro renders results of a poll as bar chart
 */
function results_macro(param2) {
   for (var i=0; i<this.size(); i++) {
      var c = this.get(i);
      var param = new Object();
      param.title = c.title;
      param.count = c.size();
      param.percent = 0;
      if (param.count > 0) {
         param.percent = param.count.toPercent(this.votes.size());
         param.width = Math.round(param.percent * 2.5);
         param.graph = c.renderSkinAsString("graph", param);
         if (param.count == 1)
            param.text = " " + (param2.one ? param2.one : getMessage("Poll.votes.one"));
         else
            param.text = " " + (param2.more ? param2.more : getMessage("Poll.votes.more"));
      } else
         param.text = " " + (param2.no ? param2.no : getMessage("Poll.votes.no"));
      c.renderSkin("result", param);
   }
   return;
}


/**
 * macro renders totals of a poll
 */
function total_macro(param) {
   var n = this.votes.size();
   if (n == 0)
      n += " " + (param.no ? param.no : getMessage("Poll.votes.no"));
   else if (n == 1)
      n += " " + (param.one ? param.one : getMessage("Poll.votes.one"));
   else
      n += " " + (param.more ? param.more : getMessage("Poll.votes.more"));
   return n;
}


/**
 * macro renders a link to the poll editor
 */
function editlink_macro(param) {
   if (session.user) {
      try {
         this.checkEdit(session.user, res.meta.memberlevel);
      } catch (deny) {
         return;
      }
      Html.link({href: this.href("edit")},
                param.text ? param.text : getMessage("generic.edit"));
   }
   return;
}


/**
 * macro rendering a link to delete a poll
 * (only if the user also is the creator)
 */
function deletelink_macro(param) {
   if (session.user) {
      try {
         this.checkDelete(session.user, res.meta.memberlevel);
      } catch (deny) {
         return;
      }
      Html.link({href: this.href("delete")},
                param.text ? param.text : getMessage("generic.delete"));
   }
   return;
}


/**
 * macro renders a link to the poll
 */
function viewlink_macro(param) {
   try {
      if (!this.closed) {
         this.checkVote(session.user, res.meta.memberlevel);
         Html.link({href: this.href()},
                   param.text ? param.text : getMessage("Poll.vote"));
      }
   } catch (deny) {
      return;
   }
   return;
}


/**
 * macro renders a link as switch to close/re-open a poll
 */
function closelink_macro(param) {
   if (session.user) {
      try {
         this.checkDelete(session.user, res.meta.memberlevel);
      } catch (deny) {
         return;
      }
      var str = this.closed ? getMessage("Poll.reopen") : getMessage("Poll.close");
      Html.link({href: this.href("toggle")},
                param.text ? param.text : str);
   }
   return;
}


/**
 * macro renders the time a poll was closed
 */
function closetime_macro(param) {
   if (this.closed) {
      if (!param.format)
         param.format = "short";
      res.write(formatTimestamp(this.modifytime, param.format));
   }
   return;
}


/**
 * renders Task Items
 */
function tasks_macro(param) {
   var items = [];

   // check rights
   var mayView = true;
   var mayEdit = true;
   var mayDelete = true;
   try { this.site.checkView(session.user, res.meta.memberlevel); } catch (deny) { mayView = false; }
   try { this.checkEdit(session.user, res.meta.memberlevel); } catch (deny) { mayEdit = false; }
   try { this.checkDelete(session.user, res.meta.memberlevel); } catch (deny) { mayDelete = false; }

   // build task List
   if (mayView) {
      items.push({href: this.href("main"), text: getMessage("generic.view")});
      items.push({href: this.href("results"), text: getMessage("Poll.results")});
   }
   if (mayEdit) items.push({href: this.href("edit"), text: getMessage("generic.edit")});
   if (mayDelete) {
      items.push({href: this.href("toggle"), text: this.closed ? getMessage("Poll.reopen") : getMessage("Poll.close")});
      items.push({href: this.href("delete"), text: getMessage("generic.delete")});
   }
   var modItems = this.applyModuleMethods("renderPollTasks", [], false, false);
   if (modItems) items = items.concat(modItems);

   // render task list
   res.push();
   this._parent.renderTaskItems(items, param);
   param.items = res.pop();

   this._parent.renderSkin("taskContainer", param);
   return;
}
