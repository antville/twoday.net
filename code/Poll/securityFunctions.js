
/**
 * Permission checks (called by hopobject.onRequest())
 *
 * @param action  String, name of action
 * @param usr     User, who wants to access an action
 * @param level   Int, Membership level
 * @return Object if permission was denied <br />
 *   .message  message, if something went wrong <br />
 *   .url      recommanded redirect URL
 * @doclevel public
 */
function checkAccess(action, usr, level) {
   if (usr && usr.sysadmin) return;
   var url;
   var site = this._parent._prototype == "Site" ? this._parent : null;
   try {
      if (site && !site.online) {
         checkIfLoggedIn(this.href(action));
         site.checkView(usr, level);
      }
      switch (action) {
         case "edit" :
            checkIfLoggedIn(this.href(action));
            this.checkEdit(usr, level);
            break;
         case "delete" :
            checkIfLoggedIn(this.href(action));
            this.checkDelete(usr, level);
            break;
         case "results" :
            this.site.checkView(usr, level);
            break;
         case "toggle" :
            checkIfLoggedIn(this.href(action));
            this.checkDelete(usr, level);
            break;
         default :
            url = this.applyAllModuleMethods("checkPollAccess", [action, usr, level, url]);
            break;
      }
   } catch (deny) {
      return {message: deny.toString(), url: (url ? url : (site ? site.href() : root.href()))};
   }
   return;
}


/**
 * check if user is allowed to vote in a poll
 * @param Obj Userobject
 * @param Int Permission-Level
 * @return String Reason for denial (or null if allowed)
 */
function checkVote(usr, level) {
   this.site.checkView(usr, level);
   if (this.closed)
      throw new DenyException("pollClosed");
   return;
}


/**
 * check if user is allowed to edit a poll
 * @param Obj Userobject
 * @param Int Permission-Level
 * @return String Reason for denial (or null if allowed)
 */
function checkEdit(usr, level) {
   if (this.votes.size() > 0)
      throw new DenyException("pollEdit");
   if (this.creator != usr && (level & MEMBER_MAY_EDIT_ANYSTORY) == 0)
      throw new DenyException("pollEdit");
   return;
}


/**
 * check if user is allowed to delete a poll
 * @param Obj Userobject
 * @param Int Permission-Level
 * @return String Reason for denial (or null if allowed)
 */
function checkDelete(usr, level) {
   if (this.creator != usr && (level & MEMBER_MAY_DELETE_ANYSTORY) == 0)
      throw new DenyException("pollDelete");
   return;
}
