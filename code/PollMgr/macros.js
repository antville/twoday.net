
/**
 * returns an Array of list navigation items
 * @param param Object   passed by Macro
 * @return Array
 */
function getListNavigationItems() {
   var items = [
      {
         text: getMessage("admin.menu.contribute.polls.all"),
         action: "PollMgr/main"
      },
      {
         text: getMessage("admin.menu.contribute.polls.open"),
         action: "PollMgr/open"
      },
      {
         text: getMessage("admin.menu.contribute.polls.mypolls"),
         action: "PollMgr/mypolls"
      }
   ];
   return items;
}


/**
 * returns an Array of PollManager tasks
 * @param param Object   passed by Macro
 * @return Array
 */
function getPollManagerTasks(param) {
   var items = [
      {
         action: "PollMgr/create"
      }
   ];
   return items;
}


/**
 * render list of PollManager tasks
 */
function pollManagerTasks_macro(param) {
   var items = this.getPollManagerTasks(param);
   this.renderTasks(items, param);
}
