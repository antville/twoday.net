
/**
 * render main tasks for layout manager
 */
function layoutMgrMainTasks_macro(param) {

   var containerParam = Object.clone(param);
   var itemParam = Object.clone(param);

   var items = [
      {  action: "LayoutMgr/create"  },
      {  action: "LayoutMgr/import"  }
   ]
   res.push();
   this.renderTaskItems(items, itemParam);
   containerParam.items = res.pop();

   this.renderSkin("taskContainer", containerParam);
   return;
}
