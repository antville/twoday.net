
/**
 * Checks if user is logged in or not.
 * If false, it redirects to the login-page, but before it stores the url in session.data.referrer to jump back (if passed as argument)
 *
 * @param referrer String   URL to be stored in session.data.referrer, if user is not logged in and we redirect her to the login page
 */
function checkIfLoggedIn(referrer) {
   if (!session.user) {
      // user is not logged in
      if (referrer)
         session.data.referrer = referrer;
      res.redirect(res.handlers.site ? res.handlers.site.members.href("login")  : root.members.href("login"));
   }
   return;
}


/**
 * Checks if the request is actually a POST-request.
 * This should be included into each action, that
 * handles (critical) forms in order to prevent
 * CSRF-type attacks.
 *
 * @throw DenyException
 */
function checkPostMethod() {
   if (req.isPost() == false) {
      app.log("SECURITY: NO POST");
      throw new DenyException("noPost");
   }
   return;
}


/**
 * Checks if the request is coming from a certain action.
 * Browsers need to have Referrers enabled. This can be
 * used as a possible prevention against CSRF-type attacks.
 *
 * See http://codex.wordpress.org/Enable_Sending_Referrers if
 * troubles occur with browser that don't have referrers enabled.
 *
 * @param allowedReferrers String[] Array of allowed referrers, which should (additionally to the current action) be allowed.
 * @throw DenyException
 */
function checkReferrer(allowedReferrers) {
   var referrer = req.data.http_referer;
   if (referrer && referrer.indexOf("?") != -1) referrer = referrer.substring(0, referrer.indexOf("?"));
   allowedReferrers = allowedReferrers ? allowedReferrers : [];
   // always add the current request as allowed referrer
   allowedReferrers.push(path[path.length - 1].href(req.action));
   if (!referrer || !Array.contains(allowedReferrers, referrer)) {
      app.log("SECURITY: WRONG REFERRER " + referrer);
      throw new DenyException("wrongReferrer");
   }
   return;
}


/**
 * Checks if the request is coming for the correct host.
 * This can be used as a possible prevention against CSRF-type attacks,
 * which use XMLHttpRequest. Just is effective if each blog actually
 * runs under its own subdomain. The danger is that all domains are
 * reachable under a certain domain/ip-address, and that it is possible
 * to forge arbitrary http requests with XMLHttpRequest.
 *
 * Will perform a redirect to the correct host, if the host doesn't match.
 */
function checkHttpHost() {
   if (req.action == "ping")
      return;
   var host = req.servletRequest.getHeader("host") ? req.servletRequest.getHeader("host").toLowerCase() : "";
   var reqpath = path[path.length - 1].href(req.action);
   if (reqpath.indexOf("http://") != 0) {
      // we cant perform this type of check for relative base urls
      return;
   }
   var domain = reqpath.substring(7).toLowerCase();
   domain = domain.indexOf("/") != -1 ? domain.substring(0, domain.indexOf("/")) : domain;
   // strip any port numbers from the end
   domain = domain.replace(/\:\d+$/, "");
   host = host.replace(/\:\d+$/, "");
   if (domain != host) {
      // sites that get proxied through, will have a differing host. but they still need to match their own unique subdomain.
      if (path.Site && app.properties.subdomains == "true" && host == (path.Site.alias.toLowerCase() + "." + app.properties.defaulthost)) {
         return;
      }
      if (req.action != "notfound") {
         app.log("SECURITY: WRONG HOST " + domain + " != " + host + "->" + reqpath + "; " + req.data.http_referer);
      }
      res.redirect(reqpath);
   }
   return;
}


/**
 * Checks for a valid secretKey, that needs to be included into the
 * form. This is necessary to prevent (simple) CSRF-type attacks. This
 * function should be called in every action that processes a form.
 * Requires the secretKey-macro to be included into the form. As a
 * fallback checkPostMethod + checkReferrer will be used.
 *
 */
function checkSecretKey() {
   var secretKey = req.data.secretKey;

   // first remove outdated entries (i.e. older than 60min)
   var now = new Date();
   var ht = new java.util.Hashtable();
   if (!session.data.secretKeys) {
      session.data.secretKeys = new java.util.Hashtable();
   }
   var keys = session.data.secretKeys.keys();
   var cnt = 0;
   for (var keys = session.data.secretKeys.keys(); keys.hasMoreElements() ;) {
      var nextKey = keys.nextElement();
      var entry = session.data.secretKeys.get(nextKey);
      if (now.valueOf() - entry.timestamp < 1000 * 60 * 60) {
         ht.put(nextKey, entry);
         cnt++;
      }
      if (cnt > 1000) break;
   }
   session.data.secretKeys = ht;

   // check whether secretKey is valid
   if (secretKey == null || ht.get(secretKey) == null ||
      (ht.get(secretKey).validFor == "rest" && req.action != "rest") ||
      (ht.get(secretKey).validFor != "rest" && ht.get(secretKey).validFor != path[path.length-1].href(req.action))) {
      // checkSecretKey will fail, when a user used the back-button in the browser, or the browser/proxy cached the
      // form page (and its secret key). therefore we check for the referrer as a fallback. if the referrer is OK,
      // then we accept the request anyways.
      if (secretKey && ht.get(secretKey)) session.data.secretKeys.remove(secretKey);
      try {
         checkPostMethod();
         checkReferrer();
      } catch (err) {
         app.log("SECURITY: WRONG SECRET KEY " + secretKey + " " + path[path.length-1].href(req.action) + " " + session._id +
            " AND (WRONG REFERRER " + req.data.http_referer + " OR WRONG METHOD " + req.method + ")");
         throw new DenyException("wrongSecretKey");
      }

   } else if (ht.get(secretKey).validFor == "rest" && req.action == "rest") {
      // secretRestKeys may be used more than once until they time out
      ;

   } else {
      // each secretKey may just be used once
      session.data.secretKeys.remove(secretKey);
   }
   return;
}


/**
 * Creates a secret Key, stores it in the session and returns its value.
 *
 * @param validFor "rest" or some specific action
 */
function createSecretKey(validFor) {
   if (!session.data.secretKeys) {
      session.data.secretKeys = new java.util.Hashtable();
   }
   var secretKey = Packages.helma.util.MD5Encoder.encode(Math.random());
   // add the new secretKey together with the current timestamp and the action
   var obj = new HopObject();
      obj.timestamp = (new Date()).valueOf();
      obj.validFor = validFor;
   session.data.secretKeys.put(secretKey, obj);
   return secretKey;
}


/**
 * Renders a hidden input field containing a valid secretKey for that particular session
 * and the current action.
 *
 * @param validFor If this parameter is not specified, then the current
 *                 action is taken. Otherwise the format is e.g. 'Story:comments/create',
 *                 or 'Site:edit', that is 'PrototypeInPath:Path/To/Object/action'.
 *                 If 'response.action' is specified, then the value res.data.action is taken.
 */
function secretKey_macro(param) {
   if (param.validFor && param.validFor.indexOf("response.") == 0) {
      var validFor = res.data[param.validFor.substring(9)];
   } else if (param.validFor) {
      var vf = param.validFor;
      if (vf.indexOf(":") != -1) {
         var obj = path[vf.substring(0, vf.indexOf(":"))];
         if (!obj) return;
         vf = vf.substring(vf.indexOf(":") + 1);
      } else {
         var obj = path[path.length-1];
      }
      if (vf.indexOf("/") != -1) {
         var objpath = vf.split("/");
         for (var i=0; i<objpath.length - 1; i++) {
            obj = obj.get(objpath[i]);
            if (obj == null) return;
         }
         var action = objpath[objpath.length - 1];
      } else {
         var action = vf;
      }
      var validFor = obj.href(action);
   } else {
      var validFor = path[path.length-1].href(req.action);
   }
   var secretKey = createSecretKey(validFor);
   Html.hidden({name: "secretKey", value: secretKey});
   return;
}


/**
 * Renders a hidden input field containing a valid secretKey for that particular session
 * and for all rest actions.
 */
function secretRestKey_macro(param) {
   // Make sure that this macro is only called within the backend
   // if (!res.meta.isSiteMgr) return; // FIXME
   var secretKey = createSecretKey("rest");
   Html.hidden({name: "secretKey", value: secretKey});
   return;
}
