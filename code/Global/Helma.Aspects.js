
try {
   void(Helma);
} catch (err) {
   Helma = {
      toString: function() {
         return "[Helma JavaScript Library]";
      }
   }
}


/**
 * Object for adding Aspects (by roman porotnikov,
 * http://freeroller.net/comments/deep?anchor=aop_fun_with_javascript)
 *
 * Note: Each prototype that uses aspects must implement a method
 * onCodeUpdate() to prevent aspects being lost when the prototype
 * is re-compiled
 */
Helma.Aspects = {

   addBefore: function(obj, fname, before) {
      if (!before) {
         app.log("ASPECTS ERROR addBefore: tried to add function that does not exist");
         app.log(before.name); // throw error
      }
      // prevent that the same function is added twice
      if (!obj[fname + "_before"]) obj[fname + "_before"] = new Array();
      if (Array.contains(obj[fname + "_before"], before.name)) {
         app.log("ASPECTS ERROR addBefore: tried to add " + before.name + " twice");
         return;
      }
      if (obj[fname + "_before"].push(before.name));
      var oldFunc = obj[fname];
      obj[fname] = function() {
         return oldFunc.apply(this, before(arguments, oldFunc, this));
      }
      return;
   },

   addAfter: function(obj, fname, after) {
      if (!after) {
         app.log("ASPECTS ERROR addAfter: tried to add function that does not exist");
         return;
      }
      // prevent that the same function is added twice
      if (!obj[fname + "_after"]) obj[fname + "_after"] = new Array();
      if (Array.contains(obj[fname + "_after"], after.name)) {
         app.log("ASPECTS ERROR addAfter: tried to add " + after.name + " twice");
         return;
      }
      if (obj[fname + "_after"].push(after.name));
      var oldFunc = obj[fname];
      obj[fname] = function() {
         return after(oldFunc.apply(this, arguments), arguments, oldFunc, this);
      }
      return;
   },

   addAround: function(obj, fname, around) {
      if (!around) {
         app.log("ASPECTS ERROR addAround tried to add function that does not exist");
         return;
      }
      // prevent that the same function is added twice
      if (!obj[fname + "_around"]) obj[fname + "_around"] = new Array();
      if (Array.contains(obj[fname + "_around"], around.name)) {
         app.log("ASPECTS ERROR addAround: tried to add " + around.name + " twice");
         return;
      }
      if (obj[fname + "_around"].push(around.name));
      var oldFunc = obj[fname];
      obj[fname] = function() {
         return around(arguments, oldFunc, this);
      }
      return;
   }

}
