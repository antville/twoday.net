LAYOUT_ORIENTATIONS = [
         "2c_center_left",
         "2c_full_left",
         "2c_left_left",
         "2c_center_right",
         "2c_full_right",
         "2c_left_right",
         "3c_full",
         "1c_center",
         "1c_left"
         ];


// Don't forget to edit locale-files when changing, deleting or adding preset-titles
// "name" must not be "currentColorSettings"!
COLOR_PRESETS = [

      {  "name":              "antville",
         "title":             "Antville Colors",
         "bgcolor":           "ffffff",
         "titlecolor":        "d50000",
         "textcolor":         "000000",
         "smalltextcolor":    "959595",
         "linecolor":         "000000",
         "blockcolor":        "ffffff",
         "linkcolor":         "ff4040",
         "alinkcolor":        "ff4040",
         "vlinkcolor":        "ff4040"
      },

      {  "name":              "twodayclassic",
         "title":             "Twoday Classic Colors",
         "bgcolor":           "ffffff",
         "titlecolor":        "000000",
         "textcolor":         "000000",
         "smalltextcolor":    "666666",
         "linecolor":         "cccc66",
         "blockcolor":        "ffffff",
         "linkcolor":         "666633",
         "alinkcolor":        "a8a86e",
         "vlinkcolor":        "666633"
      },

      {  "name":              "green",
         "title":             "Green",
         "bgcolor":           "CCCC33",
         "titlecolor":        "333333",
         "textcolor":         "333333",
         "smalltextcolor":    "333333",
         "linecolor":         "CCCCCC",
         "blockcolor":        "ffffff",
         "linkcolor":         "CC6633",
         "alinkcolor":        "CC6633",
         "vlinkcolor":        "CC6633"
      },

      {  "name":              "winter",
         "title":             "Winter",
         "bgcolor":           "63615A",
         "titlecolor":        "4A7184",
         "textcolor":         "ffffff",
         "smalltextcolor":    "ffffff",
         "linecolor":         "73736B",
         "blockcolor":        "A5A694",
         "linkcolor":         "393C39",
         "alinkcolor":        "393C39",
         "vlinkcolor":        "393C39"
      },

      {  "name":              "grey",
         "title":             "Grey",
         "bgcolor":           "BBBBBB",
         "titlecolor":        "444444",
         "textcolor":         "333333",
         "smalltextcolor":    "999999",
         "linecolor":         "ffffff",
         "blockcolor":        "E6E6E6",
         "linkcolor":         "666666",
         "alinkcolor":        "666666",
         "vlinkcolor":        "666666"
      }

   ];


// Don't forget to edit locale-files when changing, deleting or adding preset-titles
// "name" must not be "currentFontSettings"!
FONT_PRESETS = [

      {  "name":              "sansserif",
         "title":             "Sans Serif",
         "titlefont":         "Verdana, Helvetica, Arial, sans-serif",
         "titlefontsize":     "15px",
         "textfont":          "Verdana, Helvetica, Arial, sans-serif",
         "textfontsize":      "13px",
         "smalltextfont":     "Verdana, Helvetica, Arial, sans-serif",
         "smalltextfontsize": "11px"
      },

      {  "name":              "typewriter",
         "title":             "Type Writer",
         "titlefont":         "Courier New, Verdana, Helvetica, Arial, sans-serif",
         "titlefontsize":     "15px",
         "textfont":          "Courier New, Verdana, Helvetica, Arial, sans-serif",
         "textfontsize":      "13px",
         "smalltextfont":     "Courier New, Verdana, Helvetica, Arial, sans-serif",
         "smalltextfontsize": "11px"
      },

      {  "name":              "smallgeorgia",
         "title":             "Small Georgia",
         "titlefont":         "Georgia, Arial, Helvetica, sans-serif",
         "titlefontsize":     "12px",
         "textfont":          "Georgia, Arial, Helvetica, sans-serif",
         "textfontsize":      "11px",
         "smalltextfont":     "Georgia, Arial, Helvetica, sans-serif",
         "smalltextfontsize": "10px"
      },

      {  "name":              "largegeorgia",
         "title":             "Large Georgia",
         "titlefont":         "Georgia, Arial, Helvetica, sans-serif",
         "titlefontsize":     "17px",
         "textfont":          "Verdana, Geneva, Arial, Helvetica, sans-serif",
         "textfontsize":      "12px",
         "smalltextfont":     "Georgia, Arial, Helvetica, sans-serif",
         "smalltextfontsize": "11px"
      },

      {  "name":              "twodayclassic",
         "title":             "Twoday Classic",
         "titlefont":         "Trebuchet MS, Verdana, Arial",
         "titlefontsize":     "16px",
         "textfont":          "Trebuchet MS, Verdana, Arial",
         "textfontsize":      "13px",
         "smalltextfont":     "Trebuchet MS, Verdana, Arial",
         "smalltextfontsize": "11px"
      }

   ];
