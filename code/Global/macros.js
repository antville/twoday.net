
/**
 * Renders the charset used for returning responses
 *
 * @doclevel public
 */
function charset_macro() {
   return app.properties.charset ? app.properties.charset : "ISO-8859-1";
}


/**
 * Renders the current timestamp by calling the function formatTimestamp.
 *
 * @param format possible values are "short", "long" or any custom format
 * @see Global.formatTimestamp
 * @doclevel public
 */
function now_macro(param) {
   return formatTimestamp(new Date(), param.format);
}


/**
 * Renders the antville-logos by calling DefaultImages.render function.
 *
 * @param name   By default the twoday credits button is rendered
 * @param width  Width of logo, takes width of the logo image by default
 * @param height Height of logo, takes height of the logo image by default
 * @param alt    Alttext
 * @param linkto Url to which the logo should link to
 * @doclevel public
 */
function logo_macro(param) {
   DefaultImages.render(param.name || "twoday1", param);
   return;
}


/**
 * Renders an image out of image-pool either as plain image, thumbnail, popup or url
 * param.name can contain a slash indicating that the image belongs to a different site or to root
 *
 * @param name           (mandatory) Name of the image to render
 * @param as             (mandatory) may contain "url", "thumbnail" or "popup"
 * @param preventCaching (mandatory) If true, the image-id is appended as urlparam
 * @param linkto         Url to which the image should link to
 * @param fallback       You can specify a fallback image which is rendered in case the image given by param.name was not found.
 *                       If param.fallback starts with "/" the fallback image will be taken from DefaultImages object.
 * @doclevel public
 */
function image_macro(param) {
   if (!param.name)
      return;
   if (param.name.startsWith("/")) {
      // standard images and logos are handled by constant IMAGES
      DefaultImages.render(param.name.substring(1), param);
      return;
   }
   var result = getPoolObj(param.name, "images");
   var fallback = param.fallback;
   delete(param.fallback);
   if (!result && fallback) {
      if (fallback.startsWith("/")) {
         // standard images and logos are handled by constant IMAGES
         DefaultImages.render(fallback.substring(1), param);
         return;
      }
      result = getPoolObj(fallback, "images");
   }
   if (!result)
      return;
   var img = result.obj;
   // return different display according to param.as
   var lightbox = false;
   switch (param.as) {
      case "url" :
         var url = img.getUrl();
         return url;
      case "thumbnail" :
         if (!param.linkto)
            param.linkto = img.getUrl();
         if (img.thumbnail)
            img = img.thumbnail;
         break;
      case "popup" :
         param.linkto = img.getUrl();
         param.onclick = img.getPopupUrl();
         if (img.thumbnail)
            img = img.thumbnail;
         break;
      case "lightbox":
         param.linkto = img.getUrl();
         if (img.thumbnail)
            img = img.thumbnail;
         lightbox = true;
   }
   delete(param.name);
   delete(param.as);
   if (lightbox) {
      Html.openLink({href: param.linkto, title: img.alttext, rel: "lightbox"});
      delete(param.linkto);
      renderImage(img, param);
      Html.closeLink();
      return;
   }
   // render image tag
   if (param.linkto) {
      Html.openLink({href: param.linkto});
      delete(param.linkto);
      renderImage(img, param);
      Html.closeLink();
   } else
      renderImage(img, param);
   return;
}


/**
 * Global link macro. In contrast to the hopobject link macro,
 * this reproduces the link target without further interpretation.
 *
 * @param to       (mandatory) must contain the link url
 * @param urlparam may contain one or more url parameters in the form "param1=value1&param2=value2&param3=value3"
 * @param anchor   may contain the name of an anchor to link to (is appended to param.to after a "#")
 * @param text     Linktext. If nothing is given the url will be used as linktext.
 * @doclevel public
 */
function link_macro(param) {
   if (!param.to)
      return;
   param.href = param.to;
   if (param.urlparam)
      param.href += "?" + param.urlparam;
   if (param.anchor)
      param.href += "#" + param.anchor;
   var content = param.text ? param.text : param.href;
   delete param.to;
   delete param.linkto;
   delete param.urlparam;
   delete param.anchor;
   delete param.text;
   Html.openTag("a", param);
   res.write(content);
   Html.closeTag("a");
   return;
}


/**
 * macro fetches a file-object and renders it's main skin or any given skin you specify with param.skin. The main.skin of a file-object contains
 * its link and some additional file info.
 *
 * @param name (mandatory) Name of the file to link to
 * @param skin Name of the skin that should be rendered for the file-object. default is main.skin
 * @param as   If you hand over "url" als param.as no link is rendered but only the url of the file
 * @param text Linktext. If nothing is given the file-url will be used as linktext.
 * @skin File.main
 * @doclevel public
 */
function file_macro(param) {
   if (!param.name)
      return;
   var p = getPoolObj(param.name, "files");
   if (!p)
      return;
   if (param.as == "url")
      res.write(p.obj.getUrl());
   else {
      if (!param.text)
         param.text = p.obj.alias;
      p.obj.renderSkin(param.skin ? param.skin : "main", param);
   }
   return;
}


/**
 * Creates a string representing the objects in the current request path, linked to their main action.
 *
 * @param Separator determines how the single objects in the request path will be separated, default is ":"
 * @doclevel public
 */
function linkedpath_macro (param) {
   if (!path.Site && !path.Mgr) return;
   var separator = param.separator;
   if (!separator)
      separator = " : ";
   var title;
   var start = (path.Site == null) ? 0 : 1;
   for (var i=start; i<path.length-1; i++) {
      title = path[i].getNavigationName();
      Html.link({href: path[i].href()}, title);
      res.write(separator);
   }
   title = path[i].getNavigationName();
   if (req.action != "main" && path[i].main_action)
      Html.link({href: path[i].href()}, title);
   else
      res.write(title);
   return;
}


/**
 * Renders the story with the specified id; uses embed.skin as default
 * but the skin to be rendered can be choosen with parameter skin="skinname".
 * The id parameter may match one of these patterns:
 *   id            ... a story within the current site
 *   siteName/id   ... a story within the specified site
 *   siteName/topicName/id   ... a story within the specified site, and topic
 * where id may be the numerical id of the story, or one of these keywords:
 *   first    ... the first story (by date) in the site / topic
 *   last     ... the last story (by date) in the site / topic
 *   random   ... a random story in the site / topic
 *
 * @param id       id of a story
 * @param exclude  optional comma separated list of storyID's which to exclude.
 *                 especially useful, if use random as the id
 * @param as       optional,
 *                   "link" ... will render the story as link (using the story.title as the text)
 *                   "url"  ... will just render an url to the story
 *                   default  ... use a skin to render the story
 * @param skin     default='embed', Site.skin to be used to render the story
 * @skin Site.embed   Default skin to be used to render the story
 * @doclevel public
 */
function story_macro(param) {
   if (!param.id) return;

   var storyPath = param.id.split('/');
   var site;
   var storyID;
   var story;
   var storyContainer;

   if (storyPath.length === 3) {
      site = root.get(storyPath[0]);
      if (!site || !site.online) return;

      var topic = site.topics.get(storyPath[1]);
      if (!topic) return getMessage("error", "storyNoExist", param.id);

      storyContainer = topic;
      storyID = storyPath[2];
   } else if (storyPath.length === 2) {
      site = root.get(storyPath[0]);
      if (!site || !site.online) return;

      storyContainer = site.allstories;
      storyID = storyPath[1];
   } else if (res.handlers.site) {
      site = res.handlers.site;
      storyID = param.id;
      storyContainer = site.allstories;
   } else return;

   if (!storyContainer || storyContainer.size() === 0)
      return getMessage("error", "storyNoExist", param.id);

   var excludeStoryIds = param.exclude ? param.exclude.split(',') : [];
   switch (storyID) {
      case 'first':
         story = storyContainer.get(storyContainer.size() - 1);
         break;
      case 'last':
         story = storyContainer.get(0);
         break;
      case 'random':
         var try10Times = 0;
         do {
            if (++try10Times > 10) return;
            story = storyContainer.get(Math.floor(Math.random() * storyContainer.size()));
         } while (Array.contains(excludeStoryIds, story._id.toString()));
         break;
      default:
         story = storyContainer.get(storyID);
         break;
   }

   if (Array.contains(excludeStoryIds, story._id.toString())) return;

   if (!story || !story.online) return getMessage('error', 'storyNoExist', param.id);

   switch (param.as) {
      case 'url':
         res.write(story.href());
         break;
      case 'link':
         var title = param.text || story.content.getProperty('title');
         Html.link({href: story.href()}, title || story._id);
         break;
      default:
         story.renderSkin(param.skin || 'embed');
   }
}


/**
 * Renders a poll (optionally as link or results)
 *
 * @param id (mandatory) Id of the poll to render
 * @param as "url", "link", "results"
 * @skin Poll.main
 * @doclevel public
 */
function poll_macro(param) {
   if (!param.id)
      return;
   // disable caching of any contentPart containing this macro
   res.meta.cachePart = false;
   var parts = param.id.split("/");
   if (parts.length == 2)
      var site = root.get(parts[0]);
   else
      var site = res.handlers.site;
   if (!site)
      return;
   var poll = site.polls.get(parts[1] ? parts[1] : param.id);
   if (!poll)
      return getMessage("error.pollNoExist", param.id);
   switch (param.as) {
      case "url":
         res.write(poll.href());
         break;
      case "link":
         Html.link({
            href: poll.href(poll.closed ? "results" : "")
         }, poll.question);
         break;
      default:
         if (poll.closed || param.as == "results")
            poll.renderSkin("results");
         else {
            res.data.action = poll.href();
            poll.renderSkin("main");
         }
   }
   return;
}


/**
 * Renders a list of sites calling root.renderSitelist function to do the real job
 *
 * @param limit  Determines how many sites should be rendered at once. Default: 10
 * @param scroll If set to "true", this parameter makes the sitelist scrollable
 * @see root.renderSitelist
 * @doclevel public
 */
function sitelist_macro(param) {
   if (res.handlers.site && !res.handlers.site.trusted)
      return;
   // setting some general limitations:
   var minDisplay = 10;
   var maxDisplay = 25;
   var max = Math.min((param.limit ? parseInt(param.limit, 10) : minDisplay), maxDisplay);
   root.renderSitelist(max, "show", param.scroll);
   if (res.data.prevpage) res.write(res.data.prevpage);
   res.write(res.data.sitelist);
   if (res.data.nextpage) res.write(res.data.nextpage);
   delete res.data.prevpage;
   delete res.data.sitelist;
   delete res.data.nextpage;
   return;
}


/**
 * Renders a list of images for a given site (or the site the user is currently on) using the renderImage function for each.
 *
 * @param of         Specifies the site (alias) of which to render the imagelist. If param.of is empty, the current site is used (res.handlers.site).
 * @param limit      Determines how many images should be rendered at once. Default is 5.
 * @param itemprefix Prefex that is rendered before every image in the list.
 * @param itemsuffix Suffix that is rendered after every image in the list.
 * @param linkto     Specifies where the images should linkt to. Default is the image object itself.
 * @param as         "thumbnail" or "popup".
 * @see Global.renderImage
 * @doclevel public
 */
function imagelist_macro(param) {
   var site = param.of ? root.get(param.of) : res.handlers.site;
   if (!site || !site.images.size()) return;
   delete param.of;

   var max = Math.min(param.limit || 5, site.images.size());
   delete param.limit;

   var idx = 0;
   var imgParam;
   var linkParam = {};

   while (idx < max) {
      var imgObj = site.images.get(idx++);

      imgParam = Object.clone(param);
      delete imgParam.itemprefix;
      delete imgParam.itemsuffix;
      delete imgParam.as;
      delete linkParam.href;
      delete linkParam.onclick;

      res.write(param.itemprefix);
      switch (param.as) {
         case 'url':
            res.write(imgObj.getUrl());
            break;
         case 'popup':
            linkParam.onclick = imgObj.getPopupUrl();
         case 'thumbnail':
            linkParam.href = param.linkto || imgObj.getUrl();
            if (imgObj.thumbnail) imgObj = imgObj.thumbnail;
         default:
            delete imgParam.linkto;
            if (linkParam.href) {
               Html.openLink(linkParam);
               renderImage(imgObj, imgParam);
               Html.closeLink();
            } else renderImage(imgObj, imgParam);
      }
      res.write(param.itemsuffix);
   }
}


/**
 * Renders a list of topics for a given site (or the site the user is currently on) using the Site.topiclist macro.
 *
 * @param of Specifies the site (alias) of which to render the topiclist. If param.of is empty, the current site is used (res.handlers.site).
 * @see Site.topiclist_macro
 * @doclevel public
 */
function topiclist_macro(param) {
   var site = param.of ? root.get(param.of) : res.handlers.site;
   if (!site)
      return;
   site.topics.topiclist_macro(param);
   return;
}


/**
 * Renders the name of the current topic, if it is present in the Url.
 *
 * @see Topic.name_macro
 * @doclevel public
 */
function topicname_macro(param) {
   if (!path.topic)
      return;
   path.topic.name_macro(param);
   return;
}


/**
 * Renders the imagetopic list for a given site (or the site the user is currently on) using the ImageTopicMgr.topiclist macro.
 *
 * @param of (optional) specifies the site (alias) of which to render the topiclist. If param.of is empty, the current site is used (res.handlers.site).
 * @see ImageTopicMgr.topiclist_macro
 * @doclevel public
 */
function imagetopiclist_macro(param) {
   var site = param.of ? root.get(param.of) : res.handlers.site;
   if (!site)
      return;
   site.images.topics.topiclist_macro(param);
   return;
}


/**
 * Checks if the current session is authenticated. If true it returns the username
 *
 * @param as Possible values: "link", "url"
 * @doclevel public
 */
function username_macro(param) {
   if (!session.user)
      return;
   if (session.user.url && param.as == "link") {
      Html.link({href: session.user.url}, session.user.name);
   } else if (session.user.url && param.as == "url") {
      res.write(session.user.url);
   } else {
      res.write(session.user.getDisplayName());
   }
   return;
}


/**
 * Renders a form input field
 *
 * @param type  (optional) "input", "button", "radio", "checkbox", "textarea", "submit", "hidden", "file".<br/>Default: "input"
 * @param value (optional)
 * @param name  (optional)
 * @see Html.textArea
 * @see Html.checkBox
 * @see Html.submit
 * @see Html.hidden
 * @see Html.radioButton
 * @see Html.file
 * @see Html.input
 * @doclevel public
 */
function input_macro(param) {
   switch (param.type) {
      case "button" :
         break;
      case "radio" :
         param.selectedValue = req.data[param.name + "_array"] ? req.data[param.name + "_array"] : req.data[param.name];
         break;
      case "checkbox" :
         if (param.value == null) param.value = "1";
         param.selectedValue = req.data[param.name + "_array"] ? req.data[param.name + "_array"] : req.data[param.name];
         break;
      default :
         param.value = param.name && req.data[param.name] ? req.data[param.name] : param.value;
   }
   param.value = h_filter(param.value);
   switch (param.type) {
      case "textarea" :
         Html.textArea(param);
         break;
      case "checkbox" :
         Html.checkBox(param);
         break;
      case "button" :
         // FIXME: this is left for backwards compatibility
         Html.submit(param);
         break;
      case "submit" :
         Html.submit(param);
         break;
      case "password" :
         param.value = "";
         Html.password(param);
         break;
      case "hidden" :
         Html.hidden(param);
         break;
      case "radio" :
         Html.radioButton(param);
         break;
      case "file" :
         Html.file(param);
         break;
      default :
         Html.input(param);
   }
   return;
}


/**
 * Renders a dropdown-list
 *
 * @param name   (mandatory) comma separated list of item names
 * @param values (mandatory) comma separated list of item values
 * @param labels comma separated list if item labels
 * @see Html.dropDown
 * @doclevel public
 */
function dropdown_macro(param) {
   if (!param.name || !param.values)
      return;
   if (param.labels) {
      var options = [];
      var labels = param.labels.split(",");
      var values = param.values.split(",");
      for (var i = 0; i < values.length; i++) {
         options[i] = {
            display:  labels[i] ? labels[i] : values[i],
            value:    values[i]
         };
      }
   } else {
      var options = param.values.split(",");
   }
   var selectedIndex = req.data[param.name];
   delete param.values;
   delete param.labels;
   Html.dropDown(param, options, selectedIndex, param.firstOption);
   return;
}


/**
 * Renders a list of stories either contained in a topic or from the story collection.
 *
 * param.sortby     (mandatory) Determines the sort criteria ("title", "createtime", "modifytime");
 * param.order      (optional) Determines the sort order (asc or desc)
 * param.show       (mandatory) Determines the text type (story, comment or all)
 * param.limit      Specifies a limit of stories to render on a single page
 * param.itemprefix Prefex to be rendered before a story in the list
 * param.suffix     Suffix to be rendered after a story in the list
 * @doclevel public
 */
function storylist_macro(param) {
   // disable caching of any contentPart containing this macro
   res.meta.cachePart = false;
   var site = param.of ? root.get(param.of) : res.handlers.site;
   if (!site)
      return;

   // untrusted sites are only allowed to use "light" version
   if (res.handlers.site && !res.handlers.site.trusted) {
      param.limit = param.limit ? Math.min(site.allstories.count(), parseInt(param.limit, 10), 50) : 25;
      for (var i=0; i<param.limit; i++) {
         var story = site.allcontent.get(i);
         if (!story)
            continue;
         res.write(param.itemprefix);
         Html.openLink({href: story.href()});
         var str = story.title;
         if (!str)
            str = story.getRenderedContentPart("text").clip(20).softwrap(30);
         res.write(str ? str : "...");
         Html.closeLink();
         res.write(param.itemsuffix);
      }
      return;
   }

   // this is db-heavy action available for trusted users only (yet?)
   if (param.sortby != "title" && param.sortby != "createtime" && param.sortby != "modifytime")
      param.sortby = "modifytime";
   if (param.order != "asc" && param.order != "desc")
      param.order = "asc";
   var order = " order by TEXT_" + param.sortby.toUpperCase() + " " + param.order;
   var rel = "";
   if (param.show == "stories")
      rel += " and TEXT_PROTOTYPE = 'story'";
   else if (param.show == "comments")
      rel += " and TEXT_PROTOTYPE = 'comment'";
   if (param.topic)
      rel += " and TEXT_TOPIC = '" + param.topic + "'";
   var query = "select TEXT_ID from AV_TEXT where TEXT_F_SITE = " + site._id + " and TEXT_ISONLINE > 0" + rel + order;
   var connex = getDBConnection("twoday");
   var rows = connex.executeRetrieval(query);

   if (rows) {
      var cnt = 0;
      param.limit = param.limit ? Math.min(parseInt(param.limit, 10), 100) : 25;
      while (rows.next() && (cnt < param.limit)) {
         cnt++;
         var id = rows.getColumnItem("TEXT_ID").toString();
         var story = site.allcontent.get(id);
         if (!story)
            continue;
         res.write(param.itemprefix);
         Html.openLink({href: story.href()});
         var str = story.title;
         if (!str)
            str = story.getRenderedContentPart("text").clip(20).softwrap(30);
         res.write(str ? str : "...");
         Html.closeLink();
         res.write(param.itemsuffix);
      }
   }
   rows.release();
   return;
}


/**
 * Display a colorpicker. This macro works in site prefs and story editors
 *
 * @param name  (mandatory) Name of the color
 * @param color preselected color in the colorpicker
 * @skin Global.colorpickerWidget
 * @doclevel public
 */
function colorpicker_macro(param) {
   if (!param || !param.name)
      return;

   var param2 = new Object();
   param2.as = "editor";
   param2["size"] = "10";
   param2.onchange = "Antville.ColorPicker.set('" + param.name + "', this.value);";
   param2.id = "Antville_ColorValue_" + param.name;
   if (!param.text)
      param.text = param.name;
   if (param.color)
      param.color = renderColorAsString(param.color);

   if (path.Story || path.StoryMgr) {
      var obj = path.Story ? path.Story : new Story();
      param2.part = param.name;
      // use res.push()/res.pop(), otherwise the macro
      // would directly write to response
      res.push();
      obj.content_macro(param2);
      param.editor = res.pop();
      param.color = renderColorAsString(obj.content.getProperty(param.name));
   } else if (path.Layout) {
      var obj = path.Layout;
      // use res.push()/res.pop(), otherwise the macro
      // would directly write to response
      res.push();
      obj[param.name + "_macro"](param2);
      param.editor = res.pop();
      param.color = renderColorAsString(obj.preferences.getProperty(param.name));
   } else
      return;
   param.text = getMessage("Layout.preferences.colors." + param.text);
   renderSkin("colorpickerWidget", param);
   return;
}


/**
 * fakemail macro <%fakemail number=%>
 * generates and renders faked email-addresses
 * param.number
 * (contributed by hr@conspirat)
 * @doclevel public
 */
function fakemail_macro(param) {
   var tldList = ["com", "net", "org", "mil", "edu", "de", "biz", "de", "ch", "at", "ru", "de", "tv", "com", "st", "br", "fr", "de", "nl", "dk", "ar", "jp", "eu", "it", "es", "com", "us", "ca", "pl"];
   var nOfMails = param.number ? (param.number <= 50 ? param.number : 50) : 20;
   for (var i=0;i<nOfMails;i++) {
      var tld = tldList[Math.floor(Math.random()*tldList.length)];
      var mailName = "";
      var serverName = "";
      var nameLength = Math.round(Math.random()*7) + 3;
      for (var j=0;j<nameLength;j++)
         mailName += String.fromCharCode(Math.round(Math.random()*25) + 97);
      var serverLength = Math.round(Math.random()*16) + 8;
      for (var j=0;j<serverLength;j++)
         serverName += String.fromCharCode(Math.round(Math.random()*25) + 97);
      var addr = mailName + "@" + serverName + "." + tld;
      Html.link({href: "mailto:" + addr}, addr);
      if (i+1 < nOfMails)
         res.write(param.delimiter ? param.delimiter : ", ");
   }
   return;
}


/**
 * Renders one of the PUBLIC_CONSTANT_NAMES
 *
 * @param name (mandatory) Name of that constant
 * @doclevel public
 */
function constant_macro(param) {
   if (!param.name) return;
   if (Array.contains(PUBLIC_CONSTANT_NAMES, param.name)) return eval(param.name);
   return "[ACCESS DENIED FOR \"" + param.name + "\" ]";
}


/**
 * Picks a random site, image or story by setting param.what to the corresponding prototype
 * by default, embed.skin will be rendered but this can be overriden using param.skin
 *
 * @param what (mandatory) You may specify "sites", "stories" or "images"
 * @param site If param.what is set to either "sites" or "images" you may specify a site from where to take the ramdom object.
 *             Per default the root object is used.
 * @param skin Specifies which skin of the random object should be rendered, default is the skin "embed".
 * @skin Site.embed
 * @skin Story.embed
 * @skin Image.embed
 * @doclevel public
 */
function randomize_macro(param) {
   if (!param)
      var param = new Object();
   if (!param.what || param.what == "sites") {
      var rnd = Math.floor(Math.random() * root.publicSites.size());
      var obj = root.publicSites.get(rnd);
   } else {
      if (param.site) {
         var parent = root.get(param.site);
         if (!parent.online)
            return;
      } else
         var parent = root;
      if (param.what == "stories")
         var coll = param.site ? parent.allstories : parent.storiesByID;
      else if (param.what == "images")
         var coll = parent.images;
      else
         return;
      var rnd = Math.floor(Math.random() * coll.size());
      var obj = coll.get(rnd);
   }
   obj.renderSkin(param.skin ? param.skin : "embed");
   return;
}


/**
 * Renders a random image that can be either specified directly via the images-attribute,
 * via their topic or via their site
 *
 * @param images column separated list of image aliases
 * @param topic  specifies from which topic the image should be taken
 * all other parameters are passed through to the global image macro<br />
 * this macro is *not* allowed in stories
 * @doclevel public
 */
function randomimage_macro(param) {
   var suffix = (param.as && param.as == "thumbnail") ? "_small" : "";
   if (param.images) {
      var items = new Array();
      var aliases = param.images.split(",");
      for (var i=0; i<aliases.length; i++) {
         aliases[i] = aliases[i].trim();
         var img = getPoolObj(aliases[i] + suffix, "images");
         if (img && img.obj) items[items.length] = img.obj;
      }
      var size = items.length;
      var idx = Math.floor(Math.random() * size);
      var img = items[idx];
   } else {
      var top = param.topic;
      if (top && top.indexOf("/") >= 0) {
         var objPath = top.split("/");
         var s = (!objPath[0] || objPath[0] == "root") ? root : root.get(objPath[0]);
         top = objPath[1];
      }
      if (s==null) var s = res.handlers.site;
      var pool = (top) ? s.images.topics.get(top) : s.images;
      if (pool==null) return;
      var size = pool.count();
      var idx = Math.floor(Math.random() * size);
      var img = pool.get(idx);
   }
   delete(param.topic);
   delete(param.images);
   if (img) {
      param.name = img.alias;
      if (param.linkto && !param.linkto.contains("/")) {
         param.linkto = img.href(param.linkto);
      }
      return image_macro(param);
   }
   return;
}


/**
 * Renders the most recently created image of a topic or site
 *
 * @param topic specifies from which topic the image should be taken
 * all other parameters are passed through to the global image macro
 * @doclevel public
 */
function imageoftheday_macro(param) {
   var s = res.handlers.site;
   var pool = (param.topic) ? s.images.topics.get(param.topic) : res.handlers.site.images;
   if (pool==null) return;
   delete(param.topic);
   var img = pool.get(0);
   param.name = img.alias;
   return image_macro(param);
}


/**
 * Renders images as a thumbnail gallery
 * images can be either specified directly via the images-attribute or via their topic
 *
 * @param images comma separated list of image aliases
 * @param topic  Specifies from which topic the image should be taken
 * @param as     Default: "popup"
 * @param cols   If 0 then no table is rendered<br />Default:4
 * @itemprefix
 * @itemsuffix
 * @table_params Default: class="gallery" align="center"
 * @tr_params
 * @td_params
 * all other parameters are passed through to the global image macro
 * @doclevel public
 */
function gallery_macro(param) {
   if (param.images) {
      var items = new Array();
      var aliases = param.images.split(",");
      for (var i=0; i<aliases.length; i++) {
         aliases[i] = aliases[i].trim();
         var img = getPoolObj(aliases[i], "images");
         if (img && img.obj) items[items.length] = img.obj;
      }
   } else if (param.topic == null && path.Topic) {
      var items = path.Topic.list();
   } else {
      var top = param.topic;
      if (top && top.indexOf("/") >= 0) {
         var objPath = top.split("/");
         var s = (!objPath[0] || objPath[0] == "root") ? root : root.get(objPath[0]);
         top = objPath[1];
      }
      if (s==null) var s = res.handlers.site;
      var pool = (top) ? s.images.topics.get(top) : s.images;
      if (pool==null) return;
      var items = pool.list();
   }
   var cols = param.cols ? parseInt(param.cols, 10) : 4;
   var table_params = param.table_params ? " "+param.table_params : " class=\"gallery\"";
   var tr_params = param.tr_params ? " "+param.tr_params : "";
   var td_params = param.td_params ? " "+param.td_params : "";
   var itemprefix = param.itemprefix ? param.itemprefix : "";
   var itemsuffix = param.itemsuffix ? param.itemsuffix : "";
   delete(param.topic);
   delete(param.images);
   delete(param.cols);
   delete(param.itemprefix);
   delete(param.itemsuffix);
   delete(param.table_params);
   delete(param.tr_params);
   delete(param.td_params);

   if (param.as==null) param.as = "popup";
   if (cols>0) res.write("<table"+table_params+">\n");
   if (cols==0 || items.length%cols==0)
      var max = items.length;
   else
      var max = items.length + (cols - items.length%cols);
   for (var i=0; i<max; i++) {
      var img = (i<items.length) ? items[i] : null;
      if (cols>0 && i%cols==0) res.write("<tr"+tr_params+">\n");
      if (cols>0) res.write("<td"+td_params+">");
      if (img) {
         res.write(itemprefix);
         var obj = new HopObject();
         for (var j in param) obj[j] = param[j];
         obj.name = img.site.alias + "/" + img.alias;
         image_macro(obj);
         res.write(itemsuffix);
      }
      if (cols>0) res.write("</td>\n");
      if (cols>0 && i%cols==cols-1) res.write("</tr>\n");
   }
   if (cols>0) res.write("</table>\n");
   return;
}


/**
 * Renders localized text strings<br />
 * All arguments will passed directly to the rendered Message,
 * and are available via sthg like <% param.anyArgument %>
 *
 * @param key
 * @doclevel public
 */
function message_macro(param) {
   return getMessage(param.key, param);
}


/**
 * Helper function for macros
 * Converts a boolean string into boolean value
 * accepts the following strings as true:
 *   "yes","true","1"
 *
 * @param paramValue
 * @return Boolean
 * @doclevel public
 */
function getBooleanInParam(paramValue) {
  return (paramValue == "1" || paramValue == "yes" || paramValue == "true");
}


/**
 * Helper function for macros
 * Converts a Date string into a Date Object
 * accepts the following patterns:
 *   "yyyy","yyyy-MM","yyyy-MM-dd","yyyy-MM-dd HH:mm","EE, d.MMM yyyy H:mm"
 *
 * @param dateString String
 * @return Date
 * @doclevel public
 */
function getDateInParam(dateString) {
   if (dateString == null) return null;
   if (typeof dateString != "string") return null;
   if (dateString == "") return null;

   timePatterns = new Array("yyyy", "yyyy-MM", "yyyy-MM-dd", "yyyy-MM-dd HH:mm", "EE, d.MMM yyyy H:mm");
   timePatternsRegExp = new Array("[0-9]{4}","[0-9]{4}-[0-9]{2}","[0-9]{4}-[0-9]{2}-[0-9]{2}","[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{1,2}:[0-9]{1,2}","[A-Za-z]{2}, [0-9]{1,2}\\.[A-Za-z]{3} [0-9]{4} [0-9]{1,2}:[0-9]{1,2}","[0-9]{1,4} year[s]?","[0-9]{1,2} month[s]?","[0-9]{1,2} day[s]?","[0-9]{1,2} hour[s]?","[0-9]{1,2} minute[s]?");

   for (var i in timePatternsRegExp) {
      var re = new RegExp ("^" + timePatternsRegExp[i]);
      if (dateString.match(re)) {
         var format = timePatterns[i];
      }
   }

   if (format) {
      var df = new java.text.SimpleDateFormat (format);
      try {
         return df.parse(dateString);
      } catch (e) {
         return null;
      }
   }

   agePatterns =   new Array("yyyy", "MM", "dd", "HH", "mm");
   agePatternsRegExp = new Array("[0-9]{1,4} year[s]?","[0-9]{1,2} month[s]?","[0-9]{1,2} day[s]?","[0-9]{1,2} hour[s]?","[0-9]{1,2} minute[s]?");

   for (var i=0; i < agePatternsRegExp.length; i++) {
      var now = new Date();
      var age = 0;
      var re = new RegExp ("^" + agePatternsRegExp[i]);
      if (dateString.match(re)) {
         if (i == 4) age = parseInt(dateString, 10) * 1000 * 60; // minutes
         else if (i == 3) age = parseInt(dateString, 10) * 1000 * 60 * 60; // hours
         else if (i == 2) age = parseInt(dateString, 10) * 1000 * 60 * 60 * 24; // days
         else if (i == 1) age = parseInt(dateString, 10) * 1000 * 60 * 60 * 24 * 31; // months
         else if (i == 0) age = parseInt(dateString, 10) * 1000 * 60 * 60 * 24 * 365; // years

         return new Date((now-0)-age);
      }
   }

}


/**
 * Renders the URL to the static system files (images)
 *
 * @doclevel public
 */
function staticURL_macro(param) {
   staticUrl();
   return;
}


/**
 * Renders the URL to a help website.
 * Location of website is specified in app.properties as 'helpURL'.
 *
 * @doclevel private
 */
function helpLink_macro(param) {
   if (app.properties.helpURL) {
     var text = param.text ? param.text : getMessage("admin.help");
     delete(param.text);
     param.href = app.properties.helpURL;
     Html.openTag("a", param);
     res.write(text);
     Html.closeTag("a");
   }
   return;
}


/**
 * Renders a link to the next story. Allows linear
 * browsing through all stories.
 *
 * @param text
 * @param skin
 */
function nextStory_macro(param) {
   var next;
   if (path.Story) {
      var idx = path.Site.allstories.contains(path.Story);
      if (idx < path.Site.allstories.count()) {
         next = path.Site.allstories.get(idx+1);
      }
   }
   if (next == null && path.Site.allstories.count() > 0 && param.wrap == "true") {
      next = path.Site.allstories.get(0);
   }
   if (!next) return;
   var text;
   if (param.skin) {
      text = next.renderSkinAsString(param.skin);
   } else if (param.text) {
      text = param.text;
   } else {
      text = next.title;
   }
   Html.link({href: next.href()}, text);
   return;
}


/**
 * Renders a link to the previous story. Allows linear
 * browsing through all stories.
 *
 * @param text
 * @param skin
 * @param wrap
 */
function previousStory_macro(param) {
   var prev;
   if (path.Story) {
      var idx = path.Site.allstories.contains(path.Story);
      if (idx > 0) {
         prev = path.Site.allstories.get(idx-1);
      }
   }
   if (prev == null && path.Site.allstories.count() > 0 && param.wrap == "true") {
      prev = path.Site.allstories.get(path.Site.allstories.count() - 1);
   }
   if (!prev) return;
   var text;
   if (param.skin) {
      text = prev.renderSkinAsString(param.skin);
   } else if (param.text) {
      text = param.text;
   } else {
      text = prev.title;
   }
   Html.link({href: prev.href()}, text);
   return;
}


/**
 * Renders a SVG icon path or an entire SVG element
 * 
 * Path only: <% svgIcon name="mdiCookieRemove" as="path" %>
 * Full SVG:  <% svgIcon name="mdiCookieRemove" size="2em" fill="#fff" style="background:#ffb000" %>
 *
 * @param param Object of attributes {name|as|size|width|height|fill|style|...}
 * @doclevel public
 */
function svgIcon_macro(param) {
   // check if icon name has been specified
   if (!param.name) {
      res.write(getMessage('error.missingIconName'));
      return;
   }

   // check if icon name is available in the SVG object
   if (!SVG_ICON_PATHS.hasOwnProperty(param.name)) {
      res.write(getMessage('error.unknownIconName', param));
      return;
   }

   // check if only the path string is desired
   if (param.as && param.as === 'path') {
      res.write(SVG_ICON_PATHS[param.name]);
      return;
   }

   // check if size attribute was specified
   if (param.size) {
      // then use as width if no width was specified
      if (!param.width) param.width = param.size;
      delete(param.size);
   }

   // use default 24px width if still no width param
   if (!param.width) param.width = '24px';

   // use width as height if no height was specified
   if (!param.height) param.height = param.width;

   // use default currentColor if no fill was specified
   if (!param.fill) param.fill = 'currentColor';

   // always add the standard viewBox attribute for Material Design icons
   param.viewBox = '0 0 24 24';

   // compile the inner path element
   var innerPath = Html.tagAsString('path', {d: SVG_ICON_PATHS[param.name]});

   // finalize and flush the svg output
   delete(param.name);
   Html.element('svg', innerPath, param);
   return;
}
