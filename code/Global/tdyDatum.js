
app.modules.tdyDatum = {
   title: "tdyDatum",
   description: "Special Actions &amp; Macros for datum.at",
   version: "0.2",
   state: "stable",
   url: "",
   author: "Franz Philipp Moser",
   authorEmail: "philipp.moser@knallgrau.at",
   authorUrl: "http://www.knallgrau.at",
   authorOrganisation: "knallgrau.at",
   copyright: "(c) 2005 knallgrau.at",
   license: "twoday",
   licenseUrl: "http://www.twoday.org",
   isSidebar: false,
   hasSiteSettings: false,
   weblogname: "datum",
   csvFileName: "trafikantenliste",
   contactEmail: "office@datum.at",
   aboEmail: "abo@datum.at",
   weblogIds:
           "58197,2290,11437,11438,11436,11439,21410,11440,11441,11442,11443,11445,11444,11446,11447,11448,12188,12220,12270,13887,15377,16525,16526,16527,21234,21411,21412,21413,27240,28935,31154,32515,33725,34549,35641,36510,37729,38881,39908,40851,42578,43460,44327,45323,46259,47625,48566,49271,49956,50508,51783,52637,53255,53834,54336,55327,56244,57035,57560,58733,60563,62205,62947,63490,64359,64917"
}


app.modules.tdyDatum.allowTextMacros = function (s) {
   s.allowMacro("site.tdyDatumSellers");
}


 
