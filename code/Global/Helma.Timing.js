
// FIXME: recursion problem

/**
  * simple Timing mechanism to clock processing time of code parts
  * Usage:
  *   Helma.timer.start("someName");
  *     ... some computation ...
  *   Helma.timer.stop("someName");
  */

try {
   void(Helma);
} catch (err) {
   Helma = {
      toString: function() {
         return "[Helma JavaScript Library]";
      }
   }
}


/**
 * Stores and prints timing information every 10 minutes to the log-file.
 * in addition, when the app.properties.HelmaTimingEnable is true, a more detailed
 * timing information is printed contiously to the log-file.
 *
 * @action String "start" or "stop"
 * @key String the identifier for the timing-information
 */
Helma.Timing = {

   getCurrentTime: function() {
      if (java.lang.System.getProperty("java.version") == "1.5.0") {
         return java.lang.System.nanoTime();
      } else {
         return java.lang.System.currentTimeMillis() * 1000;
      }
   },

   init: function() {
      // timing information is temporarily stored in memory
      app.data.HelmaTiming = {
         indent: 0,
         tasks: {}
      };
   },

   start: function(taskKey) {
      // setup HelmaTiming Object for this task if it does not exist yet
      var appTask = app.data.HelmaTiming.tasks[taskKey];
      if (appTask == null) {
         appTask = app.data.HelmaTiming.tasks[taskKey] = {
            totalTime: 0,
            count: 0,
            responseCount: 0,
            responses: {}
         };
      }

      // setup HelmaTiming Object for this response if it does not exist yet
      var resTask = app.data.HelmaTiming.tasks[taskKey].responses[res.hashCode()]
      if (resTask == null) {
         app.data.HelmaTiming.tasks[taskKey].responseCount++;
         resTask = app.data.HelmaTiming.tasks[taskKey].responses[res.hashCode()] = {
            totalTime: 0,
            count: 0
         }
      }

      // print information to logfile
      resTask.indent++;
      if (app.properties.HelmaTimingEnable) {
         var indent = "TIMING ";
         for (var i = 1; i < resTask.indent; i++) indent += "| ";
         app.log(indent + taskKey + ": start");
      }

      resTask.startTime = Helma.Timing.getCurrentTime();
   },

   stop: function(taskKey) {
      var now = Helma.Timing.getCurrentTime();

      // determine if this timing task already started
      try {
         var resTask = app.data.HelmaTiming.tasks[taskKey].responses[res.hashCode()];
         // calculate durationTime and update resTask
         var startTime    = resTask.startTime;
         var stopTime     = now;
         var durationTime = stopTime - startTime;
         resTask.totalTime += durationTime;
         resTask.count++;
         resTask.startTime = null;
      } catch (err) {
         app.log("TIMING ERROR 'stop' called, but 'start' was not called for taskKey '" + taskKey + "'");
         return;
      }

      var appTask = app.data.HelmaTiming.tasks[taskKey];
      appTask.count++;
      appTask.totalTime += durationTime;

      // print information to logfile
      if (app.properties.HelmaTimingEnable) {
         var indent = "TIMING ";
         for (var i =  1; i < resTask.indent; i++) indent += "| ";
         app.log(indent + taskKey + ": ends in " + durationTime/1000 + "ms");
      }
      if (resTask.indent > 0) {
         resTask.indent--;
      }

   },

   logReport: function() {
      app.log("TIMING +-----------------------------------------------------------------------------+");
      app.log("TIMING | task                                     |   total ms |    count |   avg ms |");
      app.log("TIMING +-----------------------------------------------------------------------------+");
      for (var i in app.data.HelmaTiming.tasks) {
         var logTask = app.data.HelmaTiming.tasks[i];
         var logKey = i.pad(" ", 40, 1);
         var logTotalSec = (" " + Math.round(logTask.totalTime / 10000)/100).pad(" ", 10, -1);
         var logCount = (" " + logTask.count).pad(" ", 8, -1);
         var logAvgSec = logTask.count
            ? (" " + Math.round(parseInt(logTask.totalTime / logTask.count) / 10000)/100).pad(" ", 8, -1)
            : ("0").pad("  ", 8, -1);
         app.log("TIMING | " + logKey + " | " + logTotalSec + " | " + logCount + " | " + logAvgSec + " |");
      }
      app.log("TIMING +-----------------------------------------------------------------------------+");

      // garbage collect
      Helma.Timing.init();
   },

};

function logReport() {
   Helma.Timing.logReport();
}

Helma.Timing.init();
