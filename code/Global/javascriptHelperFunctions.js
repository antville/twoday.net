
function getTypeOf(value) {
   if (isNull(value)) return "null";
   else if (isFunction(value)) return "function";
   else if (isObject(value)) return "object";
   else if (isArray(value)) return "array";
   else if (isNumber(value)) return "number";
   else return typeof value;
}

function joinValues(value1, value2) {
   var typeOfValue1 = getTypeOf(value1);
   var typeOfValue2 = getTypeOf(value2);
   var typeOfResult;

   // shortcut for empty values
   if ((typeOfValue1 == "undefined" || typeOfValue1 == "null") && (typeOfValue2 == "undefined" || typeOfValue2 == "null")) {
      return (typeOfValue1 == "null" || typeOfValue2 == "null") ? null : undefined;
   }

   // determine type of result
   if (typeOfValue1 == typeOfValue2) {
      typeOfResult = typeOfValue1;
   } else if (typeOfValue2 == "undefined" || typeOfValue2 == "null") {
      return value1;
   } else if (typeOfValue1 == "undefined" || typeOfValue1 == "null") {
      return value2;
   }

   // try to join the values
   switch (typeOfResult) {
      case "number" :
         if (typeOfValue1 == typeOfValue2) return value1 + value2;
         else return (typeOfValue1 == "number") ? value1 : value2;
         break;
      case "array" :
         if (typeOfValue1 == typeOfValue2) return value1.concat(value2);
         else return (typeOfValue1 == "array") ? value1 : value2;
         break;
      case "boolean" :
         if (typeOfValue1 == typeOfValue2) return (value1 && value2);
         else return (typeOfValue1 == "number") ? value1 : value2;
         break;
      case "string" :
         if (typeOfValue1 == typeOfValue2) return value1 + value2;
         else return (typeOfValue1 == "number") ? value1 : value2;
         break;
      case "object" :
         if (typeOfValue1 == typeOfValue2) {
            var obj = Object.clone(value1);
            return Object.clone(value2, obj);
         } else {
            return (typeOfValue1 == "number") ? value1 : value2;
         }
         break;
      default :
         return;
   }
}
