
/**
 * @fileoverview Roby on Rails style helper functions 
 */
 
 
/**
 * Functions for calculating with Byte values based on Numbers.
 * Based on RoR library.
 *
 * @example
 *   ( (2).hours() + (45).minutes() ).ago().format("dd.MM.yyyy HH:mm:ss");
 *   ( (10).minutes() ).since( ((1).hour()).ago() );
 *
 * @Author Matthias.Platzer@knallgrau.at
 */


/**
 * Returns the number of bytes for (Number) * bit(s).
 * That means it returns 1/8th for 1 byte.
 *
 * @return Number
 */
Number.prototype.bit =
Number.prototype.bits = function() {
  return this / 8;
}


/**
 * Returns the number of bytes for (Number) * byte(s).
 * This is the same as the number itself - i know.
 *
 * @return Number
 */
Number.prototype["byte"] =
Number.prototype.bytes = function() {
  return this;
}


/**
 * Returns the number of bytes for (Number) * kilobyte(s).
 *
 * @return Number
 */
Number.prototype.kilobyte =
Number.prototype.kilobytes = function() {
  return this * 1024;
}


/**
 * Returns the number of bytes for (Number) * megabyte(s).
 *
 * @return Number
 */
Number.prototype.megabyte =
Number.prototype.megabytes = function() {
  return this * 1024 * 1024;
}


/**
 * Returns the number of bytes for (Number) * gigabyte(s).
 *
 * @return Number
 */
Number.prototype.gigabyte =
Number.prototype.gigabytes = function() {
  return this * 1024 * 1024 * 1024;
}


/**
 * Returns the number of bytes for (Number) * terabyte(s).
 *
 * @return Number
 */
Number.prototype.terabyte =
Number.prototype.terabytes = function() {
  return this * 1024 * 1024 * 1024 * 1024;
}


/**
 * Returns the number of bytes for (Number) * petabyte(s).
 *
 * @return Number
 */
Number.prototype.petabyte =
Number.prototype.petabytes = function() {
  return this * 1024 * 1024 * 1024 * 1024 * 1024;
}


/**
 * Returns the number of bytes for (Number) * exabyte(s).
 *
 * @return Number
 */
Number.prototype.exabyte =
Number.prototype.exabytes = function() {
  return this * 1024 * 1024 * 1024 * 1024 * 1024 * 1024;
}


/**
 * Returns the number rounded
 *
 * @param fractionDigits number of decimal points
 * @return Number
 */
Number.prototype.round = function(fractionDigits) {
   if (!fractionDigits) fractionDigits = 0;
   var pow = Math.pow(10, fractionDigits)
   return Math.round(this * pow) / pow;
}


/**
 * Functions for calculating with Time values based in Numbers.
 * based in RoR library, but it is using milliseconds instead of seconds,
 * because this is the basic unit in Javascript for time values.
 *
 * @example
 *   ( (2).hours() + (45).minutes() ).ago().format("dd.MM.yyyy HH:mm:ss");
 *   ( (10).minutes() ).since( ((1).hour()).ago() );
 *
 * @Author Matthias.Platzer@knallgrau.at
 */


/**
 * Returns a date (Number) milliseconds until now.
 *
 * @param now   optional date for base of calculation.
 * @return Date
 */
Number.prototype.until =
Number.prototype.ago = function() {
  var now = new Date();
  return new Date(now - this);
}


/**
 * Returns a date (Number) milliseconds from now.
 *
 * @param now   optional date for base of calculation.
 * @return Date
 */
Number.prototype.fromNow =
Number.prototype.since = function(now) {
  var now = now ? (now - 0) : (new Date() - 0);
  var result = (now + this)
  return new Date(result);
}


/**
 * Returns the number value as Date, where the number is the time in milliseconds.
 *
 * @return Date
 */
Number.prototype.asDate =
Number.prototype.toDate = function() {
  return new Date(this);
}


/**
 * Returns the time in milliseconds for (Number).
 *
 * @return Number
 */
Number.prototype.millisecond =
Number.prototype.milliseconds = function() {
  return this;
}


/**
 * Returns the time in milliseconds for (Number) * second(s).
 *
 * @return Number
 */
Number.prototype.second =
Number.prototype.seconds = function() {
  return this * 1000;
}


/**
 * Returns the time in milliseconds for (Number) * minute(s).
 *
 * @return Number
 */
Number.prototype.minute =
Number.prototype.minutes = function() {
  return this * (60 * 1000);
}


/**
 * Returns the time in milliseconds for (Number) * hour(s).
 *
 * @return Number
 */
Number.prototype.hour =
Number.prototype.hours = function() {
  return this * (60 * 60 * 1000); // an hour has 60 minutes by 60 seconds
}


/**
 * Returns the time in milliseconds for (Number) * day(s).
 *
 * @return Number
 */
Number.prototype.day =
Number.prototype.days = function() {
  return this * (24 * 3600 * 1000); // a day has 24 hours by 3600 seconds
}


/**
 * Returns the time in milliseconds for (Number) * week(s).
 *
 * @return Number
 */
Number.prototype.week =
Number.prototype.weeks = function() {
  return this * (7).days();
}


/**
 * Returns the time in milliseconds for (Number) * 14 day(s).
 *
 * @return Number
 */
Number.prototype.fortnight =
Number.prototype.fortnights = function() {
  return this * (2).weeks();
}


/**
 * Returns the time in milliseconds for (Number) month(s) (from now).
 * If you don't want to use now as the base time, you may pass an optional
 * date value as the first parameter.
 *
 * @param time   optional Date, used as base to calculate the year.
 * @return Number
 */
Number.prototype.month =
Number.prototype.months = function(time) {
  var time = time || new Date();
  var now = new Date();
  return (time.setMonth(time.getMonth() + this) - now);
}


/**
 * Returns the time in milliseconds for (Number) year(s) (from now).
 * If you don't want to use now as the base time, you may pass an optional
 * date value as the first parameter.
 *
 * @param time   optional Date, used as base to calculate the year.
 * @return Number
 */
Number.prototype.year =
Number.prototype.years = function(time) {
  var time = time || new Date();
  var now = new Date();
  return (time.setFullYear(time.getFullYear() + this) - now);
}


/**
 * Returns if this Number is a multiple of number.
 * @param number   Number
 * @return Boolean
 */
Number.prototype.isMultipleOf = function(number) {
   return (this % number == 0);
}


/**
 * Returns if this Number is even.
 * @return Boolean
 */
Number.prototype.isEven = function() {
   return (this % 2 == 0);
}


/**
 * Returns if this Number is odd.
 * @return Boolean
 */
Number.prototype.isOdd = function() {
   return (this % 2 != 0);
}


/**
 * Returns a copy of this date (not a reference).
 *
 * @return Date
 */
Date.prototype.clone = function() {
  return new Date(this - 0);
}


/**
 * Returns a number representing the date.
 * Milliseconds ellapsed since Jan 1, 1970.
 *
 * @return Date
 */
Date.prototype.toNumber =
Date.prototype.asNumber = function() {
  return (this - 0);
}


/**
 * Returns the week-number within the current year according to the Gregorian Calendar.
 *
 * @return Number
 */
Date.prototype.getWeek = function() {
   // code taken from http://www.codeproject.com/csharp/gregorianwknum.asp
   var year = this.getFullYear();
   var month = this.getMonth() + 1;
   var day = this.getDate();
   var a = Math.floor((14-(month))/12);
   var y = year + 4800 - a;
   var m = month + (12*a) - 3;
   // (gregorian calendar)
   var jd = day + Math.floor(((153*m)+2)/5) + (365*y) + Math.floor(y/4) - Math.floor(y/100) + Math.floor(y/400) - 32045;
   //now calc weeknumber according to JD
   var d4 = (jd + 31741 - (jd%7)) % 146097 % 36524 % 1461;
   var L = Math.floor(d4/1460);
   var d1 = ((d4-L)%365)+L;
   return Math.floor(d1/7) + 1;
}


/**
 * Returns a date, representing now.
 * WARNIG: This can easily be overridden by a variable called now
 * so you should use Date.now().
 *
 * @return Date
 */
var now =
Date.now = function() {
  return new Date();
}

/**
 * Returns a date, representing today (at midnight).
 * WARNIG: This can easily be overridden by a variable called today.
 * so you should use Date.today().
 *
 * @return Date
 */
var today =
Date.today = function() {
  return new Date( new Date().setHours(0, 0, 0, 0) );
}

/**
 * Returns a date, representing yesterday (at midnight).
 * WARNIG: This can easily be overridden by a variable called yesterday.
 * so you should use Date.yesterday().
 *
 * @return Date
 */
var yesterday =
Date.yesterday = function() {
  return new Date(today() - (1).day());
}

/**
 * Returns a date, representing tomorrow (at midnight).
 * so you should use Date.tomorrow().
 *
 * @return Date
 */
var tomorrow =
Date.tomorrow = function() {
  return new Date((today() - 0) + (1).day());
}



/**
 * Returns a Date representing the start of the day (0:00).
 * @return Date
 */
Date.prototype.midnight =
Date.prototype.beginningOfDay = function() {
   return new Date(this.setHours(0,0,0,0));
}


/**
 * Returns a Date representing the start of the week Monday(0:00).
 * @return Date
 */
Date.prototype.monday =
Date.prototype.beginningOfWeek = function(firstDayInWeek) {
   var firstDayInWeek = (firstDayInWeek != null) ? firstDayInWeek : 1; // Monday=1; Sunday=0;
   return new Date( this.setDate( this.getDate() - (this.getDay() + 7 - firstDayInWeek) % 7 ) ).beginningOfDay();
}


/**
 * Returns a Date representing the start of the year 1st jan. of month(0:00).
 * @return Date
 */
Date.prototype.beginningOfMonth = function() {
  return new Date(this.setDate(1)).beginningOfDay();
}


/**
 * Returns a Date representing the start of the year 1st jan. of month(0:00).
 * @return Date
 */
Date.prototype.endOfMonth = function() {
  // go to the 1st of this month, than add 1 month,
  // and set the day to 0, which is the day of the last month
  return new Date( new Date( new Date(this.setDate(1)).setMonth(this.getMonth() + 1) ).setDate(0) ).beginningOfDay();
}


/**
 * Returns a Date representing the start of the year 1st jan. of month(0:00).
 * @return Date
 */
Date.prototype.beginningOfYear = function() {
   return new Date(this.getFullYear(), 0, 1);
}


/**
 * Returns a date exactly (months) months ago.
 * Will return the last day of the previous month, if the previous
 * month is shorter. (31st Jan.).monthSince(2) -> 30th Nov.
 * @return Date
 */
Date.prototype.monthsAgo = function(months) {
  var originalDate = this.clone();
  var result = new Date( this.setMonth(this.getMonth() - months) );
  if (result.getDate() != originalDate.getDate())  {
     result = new Date(result.setDate(0));
  }
  return result;
}


/**
 * Returns a date exactly (months) months ahead.
 * Will return the last day of the next month, if the next
 * month is shorter. (31st Jan.).monthSince(1) -> 28th Feb.
 *
 * @param month   Number of months to add
 * @return Date
 */
Date.prototype.monthsSince = function(months) {
  var originalDate = this.clone();
  var result = new Date( this.setMonth(this.getMonth() + months) );
  if (result.getDate() != originalDate.getDate())  {
     result = new Date(result.setDate(0));
  }
  return result;
}


/**
 * Returns a date exactly 1 month ahead.
 * @return Date
 */
Date.prototype.nextMonth = function() {
  return this.monthsSince(1);
}


/**
 * Returns a date exactly 1 month ago
 * @ return Date
 */
Date.prototype.lastMonth = function() {
  return this.monthsAgo(1);
}


/**
 * Returns the difference between 2 dates as a Date Object
 * @param date, the date to compare to
 * @retun Date
 */
Date.prototype.diff = function(date) {
   if (this > date) {
      var diff = this - date;
   } else {
      var diff = date - this;
   }
   return new Date(0, 0, Math.floor(diff / (1).day()), Math.floor((diff % (1).day()) / (1).hour()), Math.floor((diff % (1).hour()) / (1).minute()));
}
