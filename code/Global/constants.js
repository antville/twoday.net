
PUBLIC_CONSTANT_NAMES = [];


/**
 * constants specifying global user-rights; right is checked via User.may(RIGHTNAME)
 */
GLOBAL_MAY_COMMENT = 1;                  // right to post comments
GLOBAL_MAY_BESUBSCRIBER = 2;             // right to become a SUBSCRIBER for any site
GLOBAL_MAY_BECONTRIBUTOR = 4 | 2;        // right to become a CONTRIBUTOR for any site
GLOBAL_MAY_BECONTENTMANAGER = 8 | 4 | 2; // right to become a CONTENTMANAGER for any site
GLOBAL_MAY_BEADMIN = 16 | 8 | 4 | 2;     // right to become an ADMIN for any site
GLOBAL_MAY_CREATESITE = 32;              // right to create new sites

/**
 * Define for each USER_TYPE their global rights
 */
USER_LOCAL = GLOBAL_MAY_COMMENT | GLOBAL_MAY_BEADMIN | GLOBAL_MAY_CREATESITE;
USER_SORUA = GLOBAL_MAY_COMMENT | GLOBAL_MAY_BESUBSCRIBER;
// USER_XYZ = GLOBAL_MAY_COMMENT;


/**
 * constants specifying user-rights on the membership-level
 */
MEMBER_MAY_ADD_STORY = 1;
MEMBER_MAY_VIEW_ANYSTORY = 2;
MEMBER_MAY_EDIT_ANYSTORY = 4;
MEMBER_MAY_DELETE_ANYSTORY = 8;
MEMBER_MAY_NOTINUSE = 16; // kept for backwards-compatibility; used to be MEMBER_MAY_ADD_COMMENT
MEMBER_MAY_EDIT_ANYCOMMENT = 32;
MEMBER_MAY_DELETE_ANYCOMMENT = 64;
MEMBER_MAY_ADD_IMAGE = 128;
MEMBER_MAY_EDIT_ANYIMAGE = 256;
MEMBER_MAY_DELETE_ANYIMAGE = 512;
MEMBER_MAY_ADD_FILE = 1024;
MEMBER_MAY_EDIT_ANYFILE = 2048;
MEMBER_MAY_DELETE_ANYFILE= 4096;
MEMBER_MAY_VIEW_STATS = 8192;
MEMBER_MAY_EDIT_PREFS = 16384;
MEMBER_MAY_EDIT_LAYOUTS = 32768;
MEMBER_MAY_EDIT_MEMBERS = 65536;

/**
 * constant containing integer representing permission of subscribers
 */
SUBSCRIBER = 0;

/**
 * constant containing integer representing permission of contributors
 */
CONTRIBUTOR = SUBSCRIBER | MEMBER_MAY_ADD_STORY | MEMBER_MAY_NOTINUSE |
              MEMBER_MAY_ADD_IMAGE | MEMBER_MAY_ADD_FILE |
              MEMBER_MAY_VIEW_STATS;

/**
 * constant containing integer representing permission of content manager
 */
CONTENTMANAGER = CONTRIBUTOR | MEMBER_MAY_VIEW_ANYSTORY | MEMBER_MAY_EDIT_ANYSTORY |
                 MEMBER_MAY_DELETE_ANYSTORY |
                 MEMBER_MAY_DELETE_ANYCOMMENT | MEMBER_MAY_EDIT_ANYIMAGE |
                 MEMBER_MAY_DELETE_ANYIMAGE | MEMBER_MAY_EDIT_ANYFILE |
                 MEMBER_MAY_DELETE_ANYFILE;

/**
 * constant containing integer representing permission of admins
 */
ADMIN = CONTENTMANAGER | MEMBER_MAY_EDIT_PREFS | MEMBER_MAY_EDIT_LAYOUTS | MEMBER_MAY_EDIT_MEMBERS;

/**
 * constant object containing the values for editableby levels
 */
EDITABLEBY_ADMINS       = 0;
EDITABLEBY_CONTRIBUTORS = 1;
EDITABLEBY_SUBSCRIBERS  = 2;

/**
 * constant object containing the values for layout shareable levels
 */
SHAREABLE_FOR_NAMES = ["none", "all", "trusted", "admin"];
SHAREABLE_FOR_NONE = 0;
SHAREABLE_FOR_ALL = 1;
SHAREABLE_FOR_TRUSTED = 2;
SHAREABLE_FOR_ADMIN = 3;

/**
 * array containing short dateformats
 */

SHORTDATEFORMATS = new Array();
SHORTDATEFORMATS[SHORTDATEFORMATS.length] = "yyyy.MM.dd, HH:mm";
SHORTDATEFORMATS[SHORTDATEFORMATS.length] = "yyyy-MM-dd HH:mm";
SHORTDATEFORMATS[SHORTDATEFORMATS.length] = "yyyy/MM/dd HH:mm";
SHORTDATEFORMATS[SHORTDATEFORMATS.length] = "d. MMMM, HH:mm";
SHORTDATEFORMATS[SHORTDATEFORMATS.length] = "MMMM d, HH:mm";
SHORTDATEFORMATS[SHORTDATEFORMATS.length] = "d. MMM, HH:mm";
SHORTDATEFORMATS[SHORTDATEFORMATS.length] = "MMM d, HH:mm";
SHORTDATEFORMATS[SHORTDATEFORMATS.length] = "EEE, d. MMM, HH:mm";
SHORTDATEFORMATS[SHORTDATEFORMATS.length] = "EEE MMM d, HH:mm";
SHORTDATEFORMATS[SHORTDATEFORMATS.length] = "EEE, HH:mm";
SHORTDATEFORMATS[SHORTDATEFORMATS.length] = "EE, HH:mm";
SHORTDATEFORMATS[SHORTDATEFORMATS.length] = "HH:mm";

LONGDATEFORMATS = new Array();
LONGDATEFORMATS[LONGDATEFORMATS.length] = "EEEE, d. MMMM yyyy, HH:mm";
LONGDATEFORMATS[LONGDATEFORMATS.length] = "EEEE, MMMM dd, yyyy, HH:mm";
LONGDATEFORMATS[LONGDATEFORMATS.length] = "EE, d. MMM. yyyy, HH:mm";
LONGDATEFORMATS[LONGDATEFORMATS.length] = "EE MMM dd, yyyy, HH:mm";
LONGDATEFORMATS[LONGDATEFORMATS.length] = "EE yyyy-MM-dd HH:mm";
LONGDATEFORMATS[LONGDATEFORMATS.length] = "yyyy-MM-dd HH:mm";
LONGDATEFORMATS[LONGDATEFORMATS.length] = "d. MMMM yyyy, HH:mm";
LONGDATEFORMATS[LONGDATEFORMATS.length] = "MMMM d, yyyy, HH:mm";
LONGDATEFORMATS[LONGDATEFORMATS.length] = "d. MMM yyyy, HH:mm";
LONGDATEFORMATS[LONGDATEFORMATS.length] = "MMM d, yyyy, HH:mm";

AVAILABLELANGUAGES = new Array();
AVAILABLELANGUAGES[AVAILABLELANGUAGES.length] = "de";
// AVAILABLELANGUAGES[AVAILABLELANGUAGES.length] = "de_AT";
// AVAILABLELANGUAGES[AVAILABLELANGUAGES.length] = "de_CH";
// AVAILABLELANGUAGES[AVAILABLELANGUAGES.length] = "de_DE";
AVAILABLELANGUAGES[AVAILABLELANGUAGES.length] = "en";
// AVAILABLELANGUAGES[AVAILABLELANGUAGES.length] = "en_US";
// AVAILABLELANGUAGES[AVAILABLELANGUAGES.length] = "en_GB";
// AVAILABLELANGUAGES[AVAILABLELANGUAGES.length] = "it";
// AVAILABLELANGUAGES[AVAILABLELANGUAGES.length] = "it_CH";
// AVAILABLELANGUAGES[AVAILABLELANGUAGES.length] = "it_IT";
// AVAILABLELANGUAGES[AVAILABLELANGUAGES.length] = "nl";
// AVAILABLELANGUAGES[AVAILABLELANGUAGES.length] = "nl_BE";
// AVAILABLELANGUAGES[AVAILABLELANGUAGES.length] = "nl_NL";


/**
 * width and height of thumbnail images
 */
THUMBNAILWIDTH = 100;
THUMBNAILHEIGHT = 100;


/**
 * object containing the metadata of
 * standard antville images plus the method
 * for rendering them
 */
DefaultImages = {
   icon: {name: "icon.gif", width: 48, height: 48, alt: "icon"},
   sorua: {name: "sorua.gif", width: 54, height: 20, alt: "sorua enabled"},
   favicon: {name: "favicon.png", width: 16, height: 16, alt: "favicon"},
   agb: {name: "agb.png", width: 27, height: 15, alt: "AGB"},
   AGBs: {name: "AGBs.png", width: 27, height: 15, alt: "AGBs"},
   twoday1: {name: "twoday1.png", width: 80, height: 15, alt: "twoday.net"},
   twoday2: {name: "twoday2.png", width: 80, height: 15, alt: "twoday.net"},
   twodayorg: {name: "twodayorg.png", width: 80, height: 15, alt: "twoday.org"},
   resident_of_twoday: {name: "resident_of_twoday.gif", width: 107, height: 30, alt: "resident of twoday.net"},
   powered_by_antville: {name: "powered_by_antville.gif", width: 68, height: 24, alt: "powered by Antville"},
   powered_by_helma: {name: "powered_by_helma.gif", width: 41, height: 30, alt: "powered by Helma"},
   smallanim: {name: "smallanim.gif", width: 98, height: 30, alt: "made with antville"},
   smallchaos: {name: "smallchaos.gif", width: 107, height: 29, alt: "made with antville"},
   smallstraight: {name: "smallstraight.gif", width: 107, height: 24, alt: "made with antville"},
   smalltrans: {name: "smalltrans.gif", width: 98, height: 30, alt: "made with antville"},
   xmlbutton: {name: "xml.png", width: 27, height: 15, alt: "xml version of this page"},
   rss_topic: {name: "rss_topic.png", width: 80, height: 15, alt: "xml version of this topic"},
   rss_complete: {name: "rss_complete.png", width: 80, height: 15, alt: "xml version of this page"},
   rss_summary: {name: "rss_summary.png", width: 80, height: 15, alt: "xml version of this page (summary)"},
   rss_comments: {name: "rss_comments.png", width: 80, height: 15, alt: "xml version of this page (with comments)"},
   hop: {name: "hop.gif", width: 124, height: 25, alt: "helma object publisher", linkto: "http://helma.org"},
   marquee: {name: "marquee.gif", width: 15, height: 15, alt: "marquee"},
   pixel: {name: "pixel.gif", width: 1, height: 1, alt: ""},
   illustrateCrop: {name: "illustrate_crop.gif", width: 120, height: 90, alt: "crop"},
   illustrateExact: {name: "illustrate_exact.gif", width: 120, height: 90, alt: "exact"},
   illustrateMax: {name: "illustrate_max.gif", width: 120, height: 90, alt: "max"},
   illustrateNo: {name: "illustrate_no.gif", width: 120, height: 90, alt: "no"},
   illustrateScale: {name: "illustrate_scale.gif", width: 120, height: 90, alt: "scale"},
   illustrateScaleCrop: {name: "illustrate_scalecrop.gif", width: 120, height: 90, alt: "scalecrop"},
   dot: {name: "dot.gif", width: 30, height: 30, alt: ""},

   /**
    * render a standard image
    */
   render: function(name, param) {
      var imgParam = Object.clone(param);
      delete imgParam.preventCaching;
      delete imgParam.prefix;
      delete imgParam.suffix;
      if (!this[name])
         return;
      imgParam.src = getStaticUrl() + this[name].name;
      if (imgParam.as == "url") {
         res.write(imgParam.src);
         return;
      }
      delete imgParam.name;
      imgParam.border = 0;
      if (!imgParam.width)
         imgParam.width = this[name].width;
      if (!imgParam.height)
         imgParam.height = this[name].height;
      if (!imgParam.alt)
         imgParam.alt = this[name].alt;
      if (!imgParam.linkto && !this[name].linkto)
         Html.tag("img", imgParam);
      else {
         Html.openLink({href: imgParam.linkto ? imgParam.linkto : this[name].linkto});
         delete imgParam.linkto;
         Html.tag("img", imgParam);
         Html.closeLink();
      }
      return;
   }
}


/**
 * constructor function for Skinset objects
 * used to build the constan SKINS
 */
function Skinset(key, skins, context) {
   this.key = key;
   this.skins = skins;
   this.context = context;
   this.children = [];
   this.add = function(obj) {
      this.children.push(obj);
   }
   return this;
}


/**
 * constant that contains the menu structure of the skin manager
 * it is basically an Array containing Skinset objects (which themselves
 * can contain child objects too)
 */
SKINSETS = [];
var newSet;

newSet = new Skinset("Root", ["Root.page", "Root.main", "Root.javascript", "Root.sysmgrnavigation", "Root.new"], "Root");
newSet.add(new Skinset("Root.scripts", ["Root.systemscripts", "Global.colorpickerScripts"]));
newSet.add(new Skinset("Root.sitelist", ["Site.preview", "Root.list"]));
newSet.add(new Skinset("Root.rss", ["Root.rss", "Site.rssItem", "Site.rssResource", "Global.rssImage"]));
newSet.add(new Skinset("Root.colorpicker", ["Global.colorpicker", "Global.colorpickerExt", "Global.colorpickerWidget", "Global.colorpickerScripts"]));
newSet.add(new Skinset("Root.welcome", ["Site.welcome", "Site.welcomeowner", "Site.welcomesysadmin", "Root.welcome"]));
newSet.add(new Skinset("Root.various", ["Root.blocked", "Root.notfound", "Root.sysError"]));
SKINSETS.push(newSet);

newSet = new Skinset("RootModuleMgr", ["RootModuleMgr.main", "Module.mgrlistitem", "RootModuleMgr.import", "Module.initialize", "Module.setup"], "Root");
newSet.add(new Skinset("RootModuleMgr.setup", ["Module.setupheader", "Module.setupline", "Module.setupspacerline"]));
newSet.add(new Skinset("RootModuleMgr.site", ["ModuleMgr.main", "Module.sitemgrlistitem", "Module.preferences"]));
SKINSETS.push(newSet);

newSet = new Skinset("Site", ["Site.page", "Site.sidebar01", "Site.sidebar02", "Site.sidebarItem", "Site.style", "Site.adminStyle", "Site.javascript", "Site.main", "Day.main", "Story.dayheader", "Story.dayfooter"]);
newSet.add(new Skinset("Site.orientation", ["Site.page_3c", "Site.style_orientation_2c_center_left", "Site.style_orientation_2c_full_left", "Site.style_orientation_2c_left_left", "Site.style_orientation_2c_center_right", "Site.style_orientation_2c_full_right", "Site.style_orientation_2c_left_right", "Site.style_orientation_3c_full"]), "Root");
newSet.add(new Skinset("Site.navigation", ["Site.contribnavigation", "Site.adminnavigation", "Global.nextpagelink", "Global.prevpagelink", "Global.pagenavigation", "Global.pagenavigationitem", "MemberMgr.usernavigation", "MemberMgr.statusloggedin", "MemberMgr.statusloggedout"]));
newSet.add(new Skinset("Site.topics", ["TopicMgr.main", "Topic.main"]));
newSet.add(new Skinset("Site.calendar", ["Site.calendar", "Site.calendardayheader", "Site.calendarweek", "Site.calendarday", "Site.calendarselday"]));
newSet.add(new Skinset("Site.rss", ["Site.rss", "Story.rssItem", "Story.rssItemSummary", "Story.rssResource"]));
newSet.add(new Skinset("Site.search", ["Site.searchform", "Site.searchbox", "Story.searchview"]));
newSet.add(new Skinset("Site.referrers", ["Site.referrers", "Site.referrerItem"]));
newSet.add(new Skinset("Site.mostread", ["Site.mostread", "Story.mostread"]));
newSet.add(new Skinset("Site.mails", ["MemberMgr.mailregconfirm", "MemberMgr.mailpassword", "MemberMgr.mailnewmember", "Membership.mailstatuschange", "Membership.mailmessage"], "Root"));
newSet.add(new Skinset("Site.preferences", ["Site.edit"], "Root"));
newSet.add(new Skinset("Site.user", ["MemberMgr.login", "MemberMgr.register", "MemberMgr.sendpwd", "User.edit", "User.sitelist", "Membership.subscriptionlistitem"], "Root"));
newSet.add(new Skinset("Site.membermgr", ["MemberMgr.main", "MemberMgr.new", "MemberMgr.membergroup", "MemberMgr.searchresult", "MemberMgr.searchresultitem", "Membership.mgrlistitem", "Membership.edit"], "Root"));
newSet.add(new Skinset("Site.various", ["Site.robotsAllow", "Site.robotsBlock", "Site.weblogStatus"]));
SKINSETS.push(newSet);

newSet = new Skinset("Story", ["Story.display", "Story.main", "Story.preview", "Story.comment", "Story.historyview", "Story.embed", "Story.edit", "Story.editForm", "Story.editOptions"]);
newSet.add(new Skinset("Story.backlinks", ["Story.backlinks", "Story.backlinkItem"]));
newSet.add(new Skinset("Story.list", ["StoryMgr.main", "Story.mgrlistitem"]));
SKINSETS.push(newSet);

newSet = new Skinset("Comment", ["Comment.toplevel", "Comment.reply", "Comment.edit"]);
SKINSETS.push(newSet);

newSet = new Skinset("Image", ["Image.main", "Image.edit", "ImageMgr.new", "LayoutImage.edit", "ImageMgr.main", "Image.mgrlistitem", "Topic.imagetopic", "Image.preview"]);
SKINSETS.push(newSet);

newSet = new Skinset("File", ["File.main", "File.edit", "FileMgr.new", "FileMgr.main", "File.mgrlistitem"]);
SKINSETS.push(newSet);

newSet = new Skinset("Poll", ["Poll.main", "Poll.results", "Choice.main", "Choice.result", "Choice.graph"]);
newSet.add(new Skinset("Poll.editor", ["Poll.edit", "Choice.edit"]));
newSet.add(new Skinset("Poll.list", ["PollMgr.main", "Poll.mgrlistitem"]));
SKINSETS.push(newSet);

newSet = new Skinset("SysMgr", ["SysMgr.status", "SysMgr.list", "Site.sysmgr_listitem", "Site.sysmgr_edit", "Site.sysmgr_delete", "User.sysmgr_listitem", "User.sysmgr_edit", "SysLog.sysmgr_listitem"], "Root");
newSet.add(new Skinset("SysMgr.forms", ["SysMgr.setup", "SysMgr.sitesearchform", "SysMgr.usersearchform", "SysMgr.syslogsearchform"]));
newSet.add(new Skinset("SysMgr.mails", ["SysMgr.blockwarnmail", "SysMgr.deletewarnmail"]));
SKINSETS.push(newSet);

newSet = new Skinset("SkinMgr", ["SkinMgr.main", "SkinMgr.page", "SkinMgr.edit", "SkinMgr.treebranch", "SkinMgr.treeleaf", "Skin.status", "Skin.statuscustom", "SkinMgr.new", "Skin.diff", "Skin.diffline"], "Root");
SKINSETS.push(newSet);

newSet = new Skinset("LayoutMgr", ["LayoutMgr.main", "LayoutMgr.new", "LayoutMgr.import"], "Root");
newSet.add(new Skinset("LayoutMgr.layout", ["Layout.mgrlistitem", "Layout.main", "Layout.edit", "Layout.download", "Layout.chooserlistitem", "Layout.testdrive"]));
newSet.add(new Skinset("LayoutMgr.images", ["LayoutImageMgr.main", "LayoutImageMgr.navigation", "LayoutImageMgr.new"]));
SKINSETS.push(newSet);

newSet = new Skinset("various", ["HopObject.delete"], "Root");
SKINSETS.push(newSet);


/**
 * constant that contains all modules that should be initialized on setup of twoday
 */
INITMODULESONSETUP = [
   "modBetterEditor",
   "modArchive",
   "modLinklist",
   "modSoruaAuthServer",
   "modSoruaUserServer",
   "modUserStatus",
   "modWeblogStatus",
   "modSiteMenu",
   "modAdminMenu",
   "modContributorMenu",
   "modWeblogSearchbar",
   "modCalendar",
   "modCredits",
   "modRecentUpdates",
   "modTrackback",
   "modLinkList",
   "modFreeText01",
   "modFreeText02"
];

// type of sidebar modules
MODULE_TYPE_NAVIGATION = 1;
MODULE_TYPE_INFO = 2;
MODULE_TYPE_EXTERNALCONTENTS = 3;
MODULE_TYPE_LIST = 4;

/**
 * Array holding all currently defined flag type reasons for stories/comments
 * Related message texts can be found in /locale/xx/message.properties
 * under the respective key "warn.flag.{flagtypereason}", 
 * e.g. "warn.flag.Generic" 
 */

FLAGTYPEREASONS = [
   'Generic', // must remain the first array item
   'Corona',
   'AntiVax',
   'HateSpeech',
   'Commercial',
   'Brutality'
];