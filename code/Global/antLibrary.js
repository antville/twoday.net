
/**
 * Returns a clone of an Array.
 * it won't clone the array items
 *
 * @param arr Array   original Array
 * @return Array
 */
Array.clone = function(arr) {
   var result = [];
   for (var i = 0; i < arr.length; i++) {
      result.push(arr[i]);
   }
   return result;
}


/**
 * If the Array contains just Objects this function
 * will return an Object matching obj[keyName] == key.
 *
 * @param arr      Array[Object]
 * @param.key      Value which should match the keyValue
 * @param.keyName  optional keyName, default="key"
 *
 * @return Object
 */
Array.getElementByKey = function(arr, key, keyName) {
   var keyName = keyName ? keyName : "key"
   for (var i = 0; i < arr.length; i++) {
      if (arr[i][keyName] == key) return arr[i];
   }
   return null;
}


/**
 * helper function for sorting Arrays
*/
function by_name(a, b) {
   return by_property(a, b, "name");
}


/**
 * helper function for sorting Arrays
*/
function by_createtime(a, b) {
   return by_property(a, b, "createtime");
}


/**
 * helper function for sorting Arrays
*/
function by_modifytime(a, b) {
   return by_property(a, b, "modifytime");
}


/**
 * helper function for sorting Arrays
 * @param a Object
 * @param b Object
 * @param propName String property name for comparing
*/
function by_property(a, b, propName) {
   if (a == null && b != null) return 1;
   if (b == null && a != null) return -1;
   if (a == null && b == null) return 0;
   if (b[propName] == null && a[propName] != null) return 1;
   if (a[propName] == null && b[propName] != null) return -1;
   if (a[propName] == null && b[propName] == null) return 0;
   if (a[propName] < b[propName]) return -1;
   if (a[propName] > b[propName]) return 1;
   if (a[propName] + "" == b[propName] + "") return 0;  // note: date objects are never the same; therefore convert property to String first;
}


/**
 * helper function for sorting Arrays
*/
function by_random(a, b) {
   var r = Math.round(Math.random()*10);
   return (r%2==0) ? 1 : -1;
}


/**
 * this function will sort an Array
 * @param arr     Array to be sorted
 * @param order   String  accepts: name,createtime,modifytime,random
 *                you also may specify separated by a space DESC or ASC
 *                to specify sorting order
 * @return Array
 */
function arraySort(arr, order) {
   if (order==null || order=="")
      return arr;
   if (order.indexOf(" ")!=-1) {
      var prop = order.substring(0, order.indexOf(" "));
      var suff = order.substring(order.indexOf(" ")+1).toUpperCase();
   } else {
      var prop = order;
      var suff = null;
   }
   var reverse = false;
   if (prop=="name" && suff=="DESC") var reverse = true;
   else if ((prop=="createtime" || prop=="modifytime") && suff=="ASC") var reverse = true;
   else var reverse = false;
   if (prop=="createtime") arr.sort(by_createtime);
   else if (prop=="modifytime") arr.sort(by_modifytime);
   else if (prop=="name") arr.sort(by_name);
   else if (prop=="random") arr.sort(by_random);
   if (reverse) {
      var narr = new Array();
      for (var i=arr.length-1; i>=0; i--) narr[narr.length] = arr[i];
      arr = narr;
   }
   return arr;
}
