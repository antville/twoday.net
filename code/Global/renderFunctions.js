
/**
 * Renders image tag for a given image.
 *
 * @devnote why isn't this an image function?
 * @param img Image     which should be rendered
 * @param param Object  contains user-defined properties
 *   .preventCaching Boolean    if true then the image-last-modifytime is appended as urlparam
 *   .title String              (optional) if not present we will use img.alttext
 *   .width String              (optional) you may override the images width, but this will just affect the img tag and not the original image.
 *   .height String             (optional) you may override the images width, but this will just affect the img tag and not the original image.
 *   .alt String                (optional) you may override the images alttext
 */
function renderImage(img, param) {
   var url = img.getUrl();
   if (param.preventCaching == "true") {
      var time = img.modifytime ? img.modifytime : img.createtime;
      if (time) url += "?tmp=" + time.format("mmss");
   }
   delete param.preventCaching;
   param.src = url

   if (!param.title) {
      param.title = img.alttext ? encode(img.alttext) : "";
   }
   if (!param.width && !param.height) {
      param.width = img.width;
      param.height = img.height;
   } else if (!param.height && param.width) {
      // Avoid param.height="NaN" when param.width="100%"
      if (param.width.isNumeric()) {
         var scaleFactor = (param.width / img.width);
         param.height = parseInt(img.height * scaleFactor, 10);
      }
   } else if (!param.width && param.height) {
      var scaleFactor = (param.height / img.height);
      param.height = parseInt(img.width * scaleFactor, 10);
   }
   param.alt = encode(param.alt ? param.alt : (img.alttext ? img.alttext : img.alias));
   if (param.alt == null) param.alt = "";
   delete param.prefix;
   delete param.suffix;
   Html.tag("img", param);
   return;
}


/**
 * Check if the color contains just hex-characters.
 * If so, it returns the color-definition prefixed with a '#'
 * otherwise it assumes the color is a named one.
 *
 * @param c String    original color value
 * @return String
 */
function renderColorAsString(c) {
   if (c && c.isHexColor())
      return "#" + c;
   return c;
}


/**
 * Renders a color as hex or named string.
 *
 * @param c String    original color value
 * @see Global.renderColorAsString
 */
function renderColor(c) {
   res.write(renderColorAsString(c));
   return;
}


/**
 * Do Wiki style substitution, transforming
 * stuff contained between asterisks into links.
 *
 * @param src String   original text, to be wikified
 * @return String
 */
function doWikiStuff (src) {
   // robert, disabled: didn't get the reason for this:
   // var src= " "+src;
   if (src == null || !src.contains("<*"))
      return src;

   // do the Wiki link thing, <*asterisk style*>
   var regex = new RegExp ("<[*]([^*]+)[*]>");
   regex.ignoreCase=true;

   var text = "";
   var start = 0;
   while (true) {
      var found = regex.exec (src.substring(start));
      var to = found == null ? src.length : start + found.index;
      text += src.substring(start, to);
      if (found == null)
         break;
      var name = ""+(new java.lang.String (found[1])).trim();
      var item = res.handlers.site.topics.get (name);
      if (item == null && name.lastIndexOf("s") == name.length-1)
         item = res.handlers.site.topics.get (name.substring(0, name.length-1));
      if (item == null || !item.size())
         text += format(name)+" <small>[<a href=\""+res.handlers.site.stories.href("create")+"?topic="+escape(name)+"\">define "+format(name)+"</a>]</small>";
      else
         text += "<a href=\""+item.href()+"\">"+name+"</a>";
      start += found.index + found[1].length+4;
   }
   return text;
}


/**
 * Renders a dropdown-box containing all available locales which are stored in AVAILABLELANGUAGES
 *
 * @param loc java.util.Locale to preselect
 * @param exclude Array     not in use
 * @param language String
 * @param param Object  will be passed to Html.dropdown as parameters for rendering the select tag
 * @doclevel admin
 */
function renderLocaleChooser(loc, exclude, language, param) {
   var param = param ? param : {};
   var locs = java.util.Locale.getAvailableLocales();
   var options = [];
   // get the defined locale of this site for comparison
   for (var i in locs) {
      if (language && !locs[i].toString().startsWith(language)) continue;
      var j = Array.indexOf(AVAILABLELANGUAGES, locs[i].toString());
      if (j > -1) {
         options[j] = [locs[i], locs[i].getDisplayName()];
      }
   }
   param.name = "locale";
   Html.dropDown(param, options, loc ? loc.toString() : null, ("--- " + getMessage("Global.renderLocaleChooser.choose") + " ---"));
   return;
}


/**
 * Renders a dropdown-box containing all available Creative Commons licenses
 *
 * @param lic: license to preselect
 * @doclevel admin
 */
function renderLicenseChooser(lic) {
   var options = [
      {value: "cc-by", display: getMessage("Global.licenseChooser.option.cc-by")},
      {value: "cc-by-sa", display: getMessage("Global.licenseChooser.option.cc-by-sa")},
      {value: "cc-by-nd", display: getMessage("Global.licenseChooser.option.cc-by-nd")},
      {value: "cc-by-nc", display: getMessage("Global.licenseChooser.option.cc-by-nc")},
      {value: "cc-by-nc-sa", display: getMessage("Global.licenseChooser.option.cc-by-nc-sa")},
      {value: "cc-by-nc-nd", display: getMessage("Global.licenseChooser.option.cc-by-nc-nd")}
   ];

   Html.dropDown({ name: 'license' }, options, lic ? lic : null, (" ---- "));
   return;
}


/**
 * Renders a dropdown-box for choosing dateformats
 * @param version String indicating version of dateformat to use:
 *    <li> "shortdateformat" - short date format
 *    <li> "longdateformat" - long date format
 * @param locale java.util.Locale   to be used
 * @param selectedValue    preselected value in dropdown
 */
function renderDateformatChooser(version, locale, selectedValue) {
   var patterns = (version == "shortdateformat" ? SHORTDATEFORMATS : LONGDATEFORMATS);
   var now = new Date();
   var options = new Array();
   for (var i in patterns) {
      var sdf = new java.text.SimpleDateFormat(patterns[i], locale);
      options[i] = [encodeForm(patterns[i]), sdf.format(now)];
   }
   Html.dropDown({name: version}, options, selectedValue);
   return;
}


/**
 * Renders a dropdown-box for choosing timezones
 *
 * @param java.util.TimeZone preselected time zone
 */
function renderTimeZoneChooser(tz) {
   var zones = java.util.TimeZone.getAvailableIDs();
   var options = new Array();
   var format = new java.text.DecimalFormat ("-0;+0");
   for (var i in zones) {
      var zone = java.util.TimeZone.getTimeZone(zones[i]);
      options[i] = [zones[i], "GMT" + (format.format(zone.getRawOffset()/Date.ONEHOUR)) + " (" + zones[i] + ")"];
   }
   Html.dropDown({name: "timezone"}, options, tz ? tz.getID() : null);
   return;
}


/**
 * Generic list render function. if the argument
 * "itemsPerPage" is given it renders a pagelist, otherwise
 * the *whole* collection will be rendered
 *
 * @param collection Object collection to work on
 * @param funcOrSkin Object either a string which is interpreted as name of a skin
 *               or a function to call for each item (the item is passed
 *               as argument)
 * @param itemsPerPage Int Number of items per page
 * @param pageIdx Object String or Integer representing the currently viewed page
 * @return String rendered list
 */
function renderList(collection, funcOrSkin, itemsPerPage, pageIdx) {
   var currIdx = 0;
   var isArray = collection instanceof Array;
   var stop = size = isArray ? collection.length : collection.size();

   if (itemsPerPage) {
      var totalPages = Math.ceil(size/itemsPerPage);
      if (isNaN(pageIdx) || pageIdx > totalPages || pageIdx < 0)
         pageIdx = 0;
      currIdx = pageIdx * itemsPerPage;
      stop = Math.min(currIdx + itemsPerPage, size);
   }
   var isFunction = (funcOrSkin instanceof Function) ? true : false;
   if (currIdx >= stop) {
      return getMessage("Mgr.listIsEmpty");
   }
   res.push();
   while (currIdx < stop) {
      var item = isArray ? collection[currIdx] : collection.get(currIdx);
      isFunction ? funcOrSkin(item) : (item ? item.renderSkin(funcOrSkin) : "");
      currIdx++;
   }
   return res.pop();
}


/**
 * Returns a rendered pagewise-navigationbar
 *
 * @param collection Object      collection to work on (either HopObject or Array)
 * @param url String             url of action to link to
 * @param itemsPerPage String    Number of items on one page
 * @param pageIdx Int            currently viewed page index
 * @return String
 */
function renderPageNavigation(collection, url, itemsPerPage, pageIdx) {
   var maxItems = 10;
   var size = (collection instanceof Array) ? collection.length : collection.size();
   var lastPageIdx = Math.ceil(size/itemsPerPage)-1;
   // if we have just one page, there's no need for navigation
   if (lastPageIdx <= 0)
      return null;

   // init parameter object
   var param = new Object();
   var pageIdx = parseInt(pageIdx, 10);
   // check if the passed page-index is correct
   if (isNaN(pageIdx) || pageIdx > lastPageIdx || pageIdx < 0)
      pageIdx = 0;
   param.display = ((pageIdx*itemsPerPage) +1) + "-" + (Math.min((pageIdx*itemsPerPage)+itemsPerPage, size));
   param.total = size;

   // render the navigation-bar
   res.push();
   if (pageIdx > 0)
      renderPageNavItem(getMessage("Mgr.pageNavigation.prev"), "pageNavItem", url, pageIdx-1);
   var offset = Math.floor(pageIdx/maxItems)*maxItems;
   if (offset > 0)
      renderPageNavItem("[..]", "pageNavItem", url, offset-1);
   var currPage = offset;
   var stop = Math.min(currPage + maxItems, lastPageIdx+1);
   while (currPage < stop) {
      if (currPage == pageIdx)
         renderPageNavItem("[" + (currPage +1) + "]", "pageNavSelItem");
      else
         renderPageNavItem("[" + (currPage +1) + "]", "pageNavItem", url, currPage);
      currPage++;
   }
   if (currPage < lastPageIdx)
      renderPageNavItem("[..]", "pageNavItem", url, offset + maxItems);
   if (pageIdx < lastPageIdx)
      renderPageNavItem(getMessage("Mgr.pageNavigation.next"), "pageNavItem", url, pageIdx +1);
   param.pagenavigation = res.pop();
   return renderSkinAsString("pagenavigation", param);
}


/**
 * Render a single item for page-navigation bar
 */
function renderPageNavItem(text, cssClass, url, page) {
   var param = {"class": cssClass};
   if (!url)
      param.text = text;
   else
      param.text = Html.linkAsString({href: url + "?page=" + page}, text);
   renderSkin("pagenavigationitem", param);
   return;
}

/**
 * Checks the response buffer for any instances of 'x-data="' and, if found, complements the head
 * with an AlpineJS script-call (located in sys.files/js).
 */
function renderAlpineScript() {
   var resBuffer = res.getBuffer();
   var needsAlpine = /x-data="/.test(resBuffer);
   if (needsAlpine) {
      var scriptTag = renderSkinAsString('javascripttagdefer', {url: getSysFilesUrl() + 'js/alpine.min.js'});
      var headClose = '</head>'
      res.resetBuffer();
      res.write(resBuffer.replace(headClose, scriptTag + headClose));
   }
}
