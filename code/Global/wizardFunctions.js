
/**
 * This is a pretty poor implementation of a wizard framework, and shoul be redone in the future.
 * So you should not rely on this functions.
 */

/**
 * Returns a param-object for usage with closePopUp.skin
 *
 * @example
 *   renderSkin("closePopUp", getRenderRedirectParam(someURL))
 * @param to   String, URL
 * @return Object
 * @doclevel experimental
 */
function getRenderRedirectParam(to) {
  var param = new Object();
  param.js = "window.opener.location.replace('"+to+"')";
  param.url = to;
  return param;
}


/**
 * Returns a param-object for usage with closePopUp.skin
 *
 * @example
 *   renderSkin("closePopUp", getRenderRedirectParam(someURL))
 * @return Object
 * @doclevel experimental
 */
function getRenderReloadParam() {
  var param = new Object();
  param.js = "window.opener.location.reload()";
  param.url = "";
  return param;
}
