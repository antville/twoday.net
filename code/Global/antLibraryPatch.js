
// FIX: don't lowercase the tail of the string

/**
 * transforms the first n characters of a string to uppercase
 * @param limit Number   amount of characters to transform
 * @return String the resulting string
 */
String.prototype.capitalize = function(limit) {
   if (limit == null)
      limit = 1;
   var head = this.substring(0, limit);
   var tail = this.substring(limit, this.length);
   return head.toUpperCase() + tail;
}
