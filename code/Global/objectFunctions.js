
/**
 * Loads messages on startup.
 * Will be called by Helma on starting the Helma server.
 */
function onStart() {
   // load localized text strings
   loadLocalization();
   // initialize all modules
   loadModules();
   // perform synchronisation and merging tasks, useful for development
   if (app.properties.syncOnStartup == "true") {
      // merge static dirs
      mergeStaticDirs();
   }
   // build macro help
   app.data.macros = buildMacroHelp();
   //eval(macroHelpFile.readAll());
   app.log("loaded macro help file");
   // creating the vector for referrer-logging
   app.data.accessLog = new java.util.Vector();
   // creating the hashtable for storyread-counting
   app.data.readLog = new java.util.Hashtable();
   // define the global mail queue
   app.data.mailQueue = new Array();
   // call onStart methods of modules
   for (var i in app.modules) {
      if (app.modules[i].onStart)
         app.modules[i].onStart();
   }
   app.data.manageAddressFilter = root.manage.createAddressFilter();
   app.data.manageAddressString = getProperty("allowSysMgr");
   return;
}


/**
 * Scheduler performing auto-disposal of inactive sites
 * and auto-blocking of private sites
 */
function scheduler() {
   if (knallgrau.isHelmaClusterMaster()) {
      // call autocleanup
      root.manage.autoCleanUp();
      // notify updated sites
      pingUpdatedSites();
      // publish to time
      publishToTime();
   }

   // write the log-entries in app.data.accessLog into DB
   writeAccessLog();
   // store a timestamp in app.data indicating when last update
   // of accessLog was finished
   app.data.lastAccessLogUpdate = new Date();
   // store the readLog in app.data.readLog into DB
   writeReadLog();
   // loadLocalization
   loadLocalization();

   // hook for modules
   root.applyModuleMethods("scheduler", null);
   app.log("[sheduler] finished");
   return 30000;
}


/**
 * Check if email-adress is syntactically correct
 *
 * @throws Error
 * @param adress String
 */
function evalEmail(address) {
   try {
      var ia = new Packages.javax.mail.internet.InternetAddress(address);
      ia.validate(); // throws Exception if email seems to be invalid
   } catch (err) {
      throw new Exception("emailInvalid");
   }
   // check for spam mail hosts
   var spamMailHosts = [
      "nervmich.net",
      "wegwerfadresse.de",
      "spambog.com",
      "guerrillamail.com",
      "mailinator.com",
      "mailinator2.com",
      "sogetthis.com",
      "mailin8r.com",
      "mailinator.net",
      "spamherelots.com",
      "thisisnotmyrealemail.com",
      "mytrashmail.com",
      "yopmail.com",
      "nospamfor.us",
      "nospam4.us",
      "antireg.com",
      "tyldd.com",
      "trashmail.net",
      "126.com",
      "spambox.us",
      "qq.com",
      "trash-mail.com",
      "trash-mail.de",
      "awntireg.com",
      "antireg.com",
      "mailforspam.com",
      "antireg.ru",
      "yahoo.com.hk",
      "mail.ru",
      "yahoo.cn",
      "seomake.com",
      "trash2009.com",
      "thefirstmail.us"
   ];
   var mailHost = address.split("@")[1];
   var subdomains = mailHost.split(".");
   if (subdomains.length > 2)
      mailHost = subdomains[subdomains.length - 2] + "." + subdomains[subdomains.length -1];
   if (Array.contains(spamMailHosts, mailHost))
      throw new Exception("emailInvalid");
   return address;
}


/**
 * Check if email-adress is syntactically correct
 *
 * @param adress String
 * @return Boolean
 */
function isEmail(address) {
   try { evalEmail(address) }
   catch (err) {
      return false;
   }
   return true;
}


/**
 * Checks if url is correct
 * if not it assumes that http is the protocol
  *
 * @param adress String
 */
function evalURL(url) {
   if (!url || url.contains("://") || url.contains("mailto:"))
      return url;
   if (url.contains("@"))
      return "mailto:" + url;
   else
      return "http://" + url;
}


/**
 * function returns file-extension according to mimetype of raw-image
 * returns false if mimetype is unknown
 * @param String Mimetype of image
 * @return String File-Extension to use
 */
function evalImgType(ct) {
   switch (ct) {
      case "image/jpeg" :
      case "image/pjpeg" :
      case "image/jpg" :
         return "jpg";
         break;
      case "image/gif" :
         return "gif";
         break;
      case "image/x-png" :
      case "image/png" :
         return "png";
         break;
      case "image/x-icon" :
         return "ico";
   }
}


/**
 * function checks if user has permanent cookies
 * storing username and password
 */
function autoLogin() {
   if (session.user)
      return;
   var type = req.data.avType ? req.data.avType : "local";
   var name = req.data.avUsr;
   var pw = req.data.avPw;
   if (!name || !pw)
      return;
   var u = root.getUserByKey(name, type);
   if (!u) {
	  //app.log("[autoLogin] User not found name: " + name);
      return;
   }
   var hash = (u._id + u.registered.valueOf() + req.data.http_remotehost).md5();
   if (!pw.contains(hash)) {
	  //app.log("[autoLogin] hash not correct: " + req.data.http_remotehost + ":" + name);
	  //app.log("[autoLogin] " + pw + " != " + hash);
      return;
   }
   try {
      u.doLogin();
   } catch (err) {
	  //app.log("[autoLogin] Error logging in user '" + name + "', err: " + err);
      res.message = err;
      return;
   }
   res.message = getMessage("confirm.welcome", [(res.handlers.site ? res.handlers.site : root).getTitle(), session.user.name]);
   return;
}


/**
 * function checks if the name of the requested object has a slash in it
 * if true, it tries to fetch the appropriate parent-object (either site or root)
 * and to fetch the object with the requested name in the specified collection
 * @param String Name of the object to retrieve
 * @param String Name of the pool to search in
 * @return Obj Object with two properties: one containing the parent-object of the pool,
 *             the other containing the object itself;
 *             If parent or object is null, the function returns null.
 */
function getPoolObj(objName, pool) {
   var p = new Object();
   if (objName.contains("/")) {
      var objPath = objName.split("/");
      p.parent = root.get(objPath[0]);
      p.objName = objPath[1];
   } else {
      p.parent = res.handlers.site;
      p.objName = objName;
   }
   if (!p.parent)
      return null;
   p.obj = p.parent[pool].get(p.objName);
   if (!p.obj)
      return null;
   return p;
}


/**
 * This is a simple logger that creates a DB entry for
 * each request that contains an HTTP referrer.
 * due to performance-reasons this is not written directly
 * into database but instead written to app.data.accessLog (=Vector)
 * and written to database by the scheduler once a minute
 */
function logAccess() {
   // track and handle Referrer-Spam
   if (root.modSpamProtection && root.modSpamProtection.referrers) {
      root.modSpamProtection.referrers.handleSpam();
   }

   var type = (path.Site) ? path.Site.type : null;
   if (type == "free") return;

   if (!req.data.http_referer || req.data.ref == "0")
      return;

   var site = res.handlers.site ? res.handlers.site : root;
   var url = Http.evalUrl(req.data.http_referer);

   // no logging at all if the referrer comes from the same site
   // or is not a http-request
   if (!url)
      return;
   var referrer = url.toString();
   var siteHref = site.href().toLowerCase();
   if (referrer.toLowerCase().contains(siteHref.substring(0, siteHref.length-1)))
      return;
   var logObj = new Object();
   logObj.storyID = path.Story ? path.Story._id : null;
   logObj.siteID = site._id;
   logObj.referrer = referrer;
   logObj.remoteHost = req.data.http_remotehost;
   logObj.browser = req.data.http_browser;
   // log to app.data.accessLog
   app.data.accessLog.add(logObj);
}


/**
 * to register updates of a site at weblogs.com
 * (and probably other services, soon), this
 * function can be called via the scheduler.
 */
function pingUpdatedSites() {
   // if in devtest mode, do nothing
   if (knallgrau.isDevTestMode()) {
      return;
   }
   var c = getDBConnection("twoday");
   var dbError = c.getLastError();
   if (dbError) {
      app.log("Error establishing DB connection: " + dbError);
      return;
   }

   var query = "select SITE_ALIAS from AV_SITE where SITE_ISONLINE = 1 and SITE_ENABLEPING = 1 and  (SITE_LASTPOSTING > SITE_LASTPING or SITE_LASTPING is null) order by SITE_ALIAS";
   var rows = c.executeRetrieval(query);
   var dbError = c.getLastError();
   if (dbError) {
      app.log("Error executing SQL query: " + dbError);
      return;
   }

   while (rows.next()) {
      var site = root.get(rows.getColumnItem("SITE_ALIAS"));
      app.log("Notifying weblogs.com for updated site '" + site.alias + "' (id " + site._id + ")");
      site.ping();
   }

   rows.release();
   return;
}


/**
 * function formats a date to a string. It checks if a site object is
 * in the request path and if so uses its locale and timezone.
 *
 * @param Object Date to be formatted
 * @param String The format string
 * @return String The date formatted as string
 */
function formatTimestamp(ts, dformat) {
   var fmt, locale, timezone;
   switch (dformat) {
      case "rss10" :
         fmt = "yyyy-MM-dd'T'HH:mm:ss'Z'";
         var timezone = new java.util.SimpleTimeZone(0, "UTC");
         break;
      case "rfc822" :
      case "rss20" :
         fmt = "EEE, dd MMM yyyy HH:mm:ss Z";
         locale = new java.util.Locale("en");
         break;
      case "short" :
         fmt = res.handlers.site ?
               res.handlers.site.preferences.getProperty("shortdateformat") :
               root.preferences.getProperty("shortdateformat");
         break;
      case "long" :
         fmt = res.handlers.site ?
               res.handlers.site.preferences.getProperty("longdateformat") :
               root.preferences.getProperty("longdateformat");
         break;
      case "dayheader" :
         fmt = res.handlers.site ?
               res.handlers.site.preferences.getProperty("dayheaderdateformat") :
               root.preferences.getProperty("dayheaderdateformat");
         break;
      default :
         fmt = dformat;
   }
   var handler = res.handlers.site ? res.handlers.site : root;
   if (!fmt) var fmt = "yyyy-MM-dd HH:mm";
   if (!locale) var locale = handler.getLocale();
   if (!timezone) var timezone = handler.getTimeZone();

   if (fmt == "verbose")
      return getAge(ts);

   try {
      return ts.format(fmt, locale, timezone);
   } catch (err) {
      return "[" + getMessage("error.invalidDatePattern") + "]";
   }
}


/**
 * Parses a String to a Date.Object
 * @param str The string to be parsed
 * @param fmt The format of the string
 *
 * @return Date.Object
 */
function parseStringToDate(str, fmt) {
   try {
      var df = new java.text.SimpleDateFormat(fmt);
      var jdate = df.parse(str);
      return new Date(jdate.getTime());
   } catch (e) {
      return null;
   }
}


/**
 * constructor function for Message objects
 * @param String Name of the message
 * @param Obj String or Array of strings passed to message-skin
 * @param Obj Result Object (optional)
 */
function Message(name, value, obj) {
   this.name = name;
   this.value = value;
   this.obj = obj;
   this.toString = function() {
      return getMessage("confirm." + this.name, this.value);
   }
   return this;
}


/**
 * constructor function for Exception objects
 * @param String Name of the message
 * @param Obj String or Array of strings passed to message-skin
 */
function Exception(name, value) {
   this.name = name;
   this.value = value;
   this.toString = function() {
      return getMessage("error." + this.name, this.value);
   }
   return this;
}


/**
 * constructor function for MailException objects
 * @param String Name of the message
 */
function MailException(name) {
   this.name = name;
   this.toString = function() {
      return getMessage("error." + this.name);
   }
   return this;
}


/**
 * constructor function for DenyException objects
 *
 * @param name String Name of the message
 * @param value
 * @param obj
 */
function DenyException(name, value, obj) {
   this.name = name;
   this.toString = function() {
      return getMessage("deny." + this.name, value, obj);
   }
   return this;
}


/**
 * returns localized text strings
 *
 * @param key
 * @param value Either a String, an Array or an Object containing values for the Skin
 * @param obj Optional Object, for which renderSkinAsString is called
 * @param locale Optional
 */
function getMessage(key, value, obj, locale) {
   if (key == null) return null;
   var localeString = (locale) ? locale : getCurrentLocale().toString();
   localeString = app.data.fallbackLocales[localeString][0];
   if (app.data.text[localeString] && app.data.text[localeString].get(key) != null)
      var str = app.data.text[localeString].get(key);
   else
       return null;
   if (value == null && obj == null) return str;
   var param = new Object();
   if (value != null) {
      if (value instanceof Array) {
         for (var j=0; j<value.length; j++)
            param["value" + (j+1)] = value[j];
      } else if (typeof value == "object") {
         param = value;
      } else {
         param.value1 = value;
      }
   }
   if (obj)
      return obj.renderSkinAsString(createSkin(str), param);
   else
      return renderSkinAsString(createSkin(str), param);
}


/**
 * function gets a MimePart passed as argument and
 * constructs an object-alias based on the name of the uploaded file
 * @param Obj MimePart-Object
 * @param Obj Destination collection
 */
function buildAliasFromFile(uploadFile, collection) {
   var filename = uploadFile.getName().split("/").pop();
   var pos = filename.lastIndexOf(".");
   if (pos > 0)
      filename = filename.substring(0, pos);
   return buildAlias(filename, collection);
}


/**
 * function gets a String passed as argument and
 * constructs an object-alias which is unique in
 * a collection
 * @param String proposed alias for object
 * @param Obj Destination collection
 * @return String determined name
 */
function buildAlias(alias, collection) {
   // clean name from any invalid characters
   var newAlias = alias.toFileName();
   if (newAlias.startsWith("."))
      newAlias = newAlias.substring(1);
   if (collection && collection.get(newAlias)) {
      // alias is already existing in collection, so we append a number
      var nr = 1;
      while (collection.get(newAlias + nr.toString()))
         nr++;
      return newAlias + nr.toString();
   } else
      return newAlias;
}


/**
 * This function parses a string for <img> tags and turns them
 * into <a> tags.
 */
function fixRssText(str) {
   var re = new RegExp("<img src\\s*=\\s*\"?([^\\s\"]+)?\"?[^>]*?(alt\\s*=\\s*\"?([^\"]+)?\"?[^>]*?)?>", "gi");
   str = str.replace(re, "[<a href=\"$1\" title=\"$3\">Image</a>]");
   return str;
}


/**
 * function swaps app.data.accessLog, loops over the objects
 * contained in Vector and inserts records for every log-entry
 * in AV_ACCESSLOG
 */
function writeAccessLog() {
   if (app.data.accessLog.size() == 0)
      return;
   // first of all swap app.data.accessLog
   var size = app.data.accessLog.size();
   var log = app.data.accessLog;
   app.data.accessLog = new java.util.Vector(size);
   // open database-connection
   var c = getDBConnection("twoday");
   var dbError = c.getLastError();
   if (dbError) {
      app.log("Error establishing DB connection: " + dbError);
      return;
   }
   // loop over log-vector
   var query;
   var re = new RegExp ("[']", "g");
   for (var i=0;i<log.size();i++) {
      var logObj = log.get(i);
      if (logObj.referrer) logObj.referrer = logObj.referrer.replace(re, "\\'");
      if (logObj.remoteHost) logObj.remoteHost = logObj.remoteHost.replace(re, "\\'");
      if (logObj.browser) logObj.browser = logObj.browser.replace(re, "\\'");
      query = "insert into AV_ACCESSLOG (ACCESSLOG_F_SITE, ACCESSLOG_F_TEXT, ACCESSLOG_REFERRER) values (" +
         logObj.siteID + ", " + logObj.storyID + ", '" + logObj.referrer + "')";
      c.executeCommand(query);
      if (dbError) {
         app.log("Error executing SQL query: " + dbError);
         return;
      }
   }
   app.log("wrote " + i + " referrers into database");
   return;
}


/**
 * function swaps app.data.readLog, loops over the logObjects
 * contained in the Hashtable and updates the read-counter
 * of all stories
 */
function writeReadLog() {
   if (app.data.readLog.size() == 0)
      return;
   // first of all swap app.data.readLog
   var size = app.data.readLog.size();
   var log = app.data.readLog;
   app.data.readLog = new java.util.Hashtable(size);
   // loop over Hashtable
   var reads = log.elements();
   while (reads.hasMoreElements()) {
      var el = reads.nextElement();
      var story = Story.getById(String(el.story));
      if (!story || !el.newReads)
         continue;
      story.reads += el.newReads;
   }
   app.log("updated read-counter of " + log.size() + " stories in database");
   return;
}


/**
 * rescue story/comment by copying all necessary properties to
 * session.data.rescuedText. this will be copied back to
 * req.data by restoreRescuedText() after successful login
 * @param Object req.data
 */
function rescueText(param) {
   session.data.rescuedText = new Object();
   for (var i in param) {
      if (i.startsWith("content_"))
         session.data.rescuedText[i] = param[i];
   }
   session.data.rescuedText.discussions = param.discussions;
   session.data.rescuedText.topic = param.topic;
   session.data.rescuedText.discussions_array = param.discussions_array;
   session.data.rescuedText.topicidx = param.topicidx;
   session.data.rescuedText.online = param.online;
   session.data.rescuedText.editableby = param.editableby;
   session.data.rescuedText.createtime = param.createtime;
   return;
}


/**
 * restore rescued Text in session.data by copying
 * all properties back to req.data
 */
function restoreRescuedText() {
   // copy story-parameters back to req.data
   for (var i in session.data.rescuedText)
      req.data[i] = session.data.rescuedText[i];
   session.data.rescuedText = null;
   return;
}


/**
 * function returns the level of the membership in cleartext
 * according to passed level
 * @param level
 * @param returnVarName boolean
 */
function getRole(level, returnVarName) {
   switch (parseInt(level, 10)) {
      case CONTRIBUTOR :
         return (returnVarName) ? "CONTRIBUTOR" : getMessage("Membership.role.contributor");
         break;
      case CONTENTMANAGER :
         return (returnVarName) ? "CONTENTMANAGER" : getMessage("Membership.role.contentManager");
         break;
      case ADMIN :
         return (returnVarName) ? "ADMIN" : getMessage("Membership.role.admin");
         break;
      default :
         return (returnVarName) ? "SUBSCRIBER" : getMessage("Membership.role.subscriber");
   }
}


/**
 * extract content properties from the object containing
 * the submitted form values (req.data)
 * @param Obj Parameter object (usually req.data)
 * @param Obj HopObject containing any already existing content
 * @return Obj JS object containing the following properties:
 *             .value: HopObject() containing extracted content
 *             .exists: Boolean true in case content was found
 *             .isMajorUpdate: Boolean true in case one content property
 *                             differs for more than 50 characters
 */
function extractContent(param, origContent) {
   var result = {isMajorUpdate: false, exists: false, value: new HopObject()};
   for (var i in param) {
      if (i.startsWith("content_") && param[i] != null) {
         var partName = i.substring(8);
         var newContentPart = param[i].trim();
         // check if there's a difference between old and
         // new text of more than 50 characters:
         if (!result.isMajorUpdate && origContent) {
            var len1 = origContent[partName] ? origContent[partName].length : 0;
            var len2 = newContentPart.length;
            result.isMajorUpdate = Math.abs(len1 - len2) >= 50;
         }
         result.value[partName] = newContentPart;
         if (newContentPart)
            result.exists = true;
      }
   }
   return result;
}


/**
 * general mail-sending function
 * @param from String sending email address
 * @param to Obj String or Array of Strings containing recipient email addresses
 * @param subject String subject line
 * @param body String Body to use in email
 * @param bcc Obj String or Array of Strings containing BCC recipient email addresses
 * @return Obj Message object
 */
function sendMail(from, to, subject, body, bcc) {
   if (!from || !to || !body) {
      throw new MailException("mailMissingParameters");
   }

   if (!subject) {
      subject = "";
   }

   if (!isArray(bcc)) {
      bcc = bcc ? [bcc] : [];
   }

   var sysEmail = root.preferences.getProperty("sys_email");
   var sysTitle = root.preferences.getProperty("sys_title");

   var username = getProperty("mail.smtp.user", sysEmail);
   var password = getProperty("mail.smtp.secret", "");

   var props = app.__app__.getProperties();
   var mail = Packages.javax.mail;

   var authenticator = new mail.Authenticator({
      getPasswordAuthentication: function () {
         return new mail.PasswordAuthentication(username, password);
      }
   });

   var session = new mail.Session.getInstance(props, authenticator);
   var message = new mail.internet.MimeMessage(session);

   message.addHeader("content-type", "text/plain; charset=utf8");
   message.addHeader("format", "flowed");
   message.addHeader("content-transfer-encoding", "8bit");

   if (isObject(from)) {
      message.setFrom(mail.internet.InternetAddress(from.email, from.name));
   } else {
      message.setFrom(mail.internet.InternetAddress(from));
   }

   if (!isArray(to)) {
      to = [to];
   } else if (to.length > 1) {
      // Send to all recipients as BCC
      bcc.unshift.apply(bcc, to);
      to.length = 0;
   }

   to.forEach(function(recipient) {
      if (isObject(recipient)) {
         recipient = mail.internet.InternetAddress(recipient.email, recipient.name);
      }
      message.addRecipients(mail.Message.RecipientType.TO, recipient);
   });

   bcc.forEach(function(recipient) {
      if (isObject(recipient)) {
         recipient = mail.internet.InternetAddress(recipient.email, recipient.name)
      }
      message.addRecipients(mail.Message.RecipientType.BCC, recipient);
   });

   message.setSubject("[" + root.getTitle() + "] " + subject, "utf8");
   message.setText(body, "utf8");

   // Use for debugging
   //message.writeTo(res.servletResponse.getOutputStream());
   //mail.Transport.send(message);

   Mail.prototype.queue.call(message);
   return new Message("mailSend");
}


/**
 * extend the Mail prototype with a method
 * that simply adds a mail object to an
 * application-wide array (mail queue).
 */
Mail.prototype.queue = function() {
   app.data.mailQueue.push(this);
   return;
}


/**
 * send all mails contained in the
 * application-wide mail queue
 */
function flushMailQueue() {
   if (app.data.mailQueue.length > 0) {
      app.log("MAIL INFO " + app.data.mailQueue.length + " Mails");
      while (app.data.mailQueue.length) {
         var message = app.data.mailQueue.pop();
         message.setSentDate(new Date());
         try {
            Packages.javax.mail.Transport.send(message);
         } catch (error) {
            app.log("MAIL Error while sending e-mail: " + error);
         }
      }
   }
   return;
}


/**
 * build a more scripting-compatible object
 * structure of the HELP.macros
 * @return Object the resulting object tree
 */
function buildMacroHelp() {
   var sorter = function(a, b) {
      var str1 = a.name.toLowerCase();
      var str2 = b.name.toLowerCase();
      if (str1 > str2)
         return 1;
      else if (str1 < str2)
         return -1;
      return 0;
   }

   var macroHelp = {};
   var ref = macroHelp.Global = [];
   var macrolist = HELP.macros.Global;
   for (var i in macrolist)
      ref.push({name: i, storyid: macrolist[i]});
   ref.sort(sorter);

   var ref = macroHelp.HopObject = [];
   var macrolist = HELP.macros.HopObject;
   for (var i in macrolist)
      ref.push({name: i, storyid: macrolist[i]});
   ref.sort(sorter);

   for (var proto in HELP.macros) {
      if (proto.indexOf("_") == 0 || proto == "Global" || proto == "HopObject")
         continue;
      var macrolist = HELP.macros[proto];
      var ref = macroHelp[proto] = [];
      var keys = "";
      for (var i in macrolist) {
         ref.push({name: i, storyid: macrolist[i]});
         keys += i + ",";
      }
      for (var n in macroHelp.HopObject) {
         var shared = macroHelp.HopObject[n];
         if (keys.indexOf(shared.name + ",") < 0)
            ref.push(shared);
      }
      ref.sort(sorter);
   }
   return macroHelp;
}


/**
 * Loads localized text files into one Hashtable per Locale.
 * For faster lookup during runtime the fallback Text for a
 * missing key (for a certain language) is also already stored
 * in that language Hashtable.
 * app.data.text is then a named Array of Hashtables
 * app.data.fallbackLocales is then a named Array of Arrays holding
 *    the fallback Locales for each possible Locale
 * The complete list of all available messages is determined by the
 *    messages available for "en"
 */
function loadLocalization() {

   var localeDirs = getAllLocaleDirs();
   var REGEXP_PROPERTIES_FILENAME = /^(([a-zA-Z][a-zA-Z0-9_\-.]*)\.properties)$/;

   // check if we need to update locales
   var checkSum = 0;
   for (var i in localeDirs) {
      var localeDir = localeDirs[i];
      var files = localeDir.list();
      for (var j in files) {
         if (!files[j].match(REGEXP_PROPERTIES_FILENAME)) continue;
         var file = new java.io.File(localeDirs[i], files[j]);
         checkSum += file.lastModified();
      }
   }
   if (checkSum == app.data.i18nCheckSum) {
      return;
   }
   app.data.i18nCheckSum = checkSum;

   var text = new Array();
   // read all message files from the LocalizationDir
   for (var i in localeDirs) {
      var localeDir = localeDirs[i];
      var hash = text[localeDir.getName()] ? text[localeDir.getName()] : new java.util.Hashtable();
      var files = localeDir.list();
      for (var j in files) {
         if (!files[j].match(REGEXP_PROPERTIES_FILENAME)) continue;
         var props = new java.util.Properties();
         var bpin = new java.io.FileInputStream(new java.io.File(localeDir.getAbsolutePath(), files[j]));
         props.load(bpin);
         bpin.close();
         for (var k = props.keys(); k.hasMoreElements(); ) {
            var key = k.nextElement();
            hash.put(key, props.get(key));
         }
      }
      text[localeDir.getName()] = hash;
   }
   app.data.text = text;
   // now we collect for all possible Locales their available fallbacks
   var fallbackLocales = new Array();
   var availableLocales = java.util.Locale.getAvailableLocales();
   for (var i in availableLocales) {
      fallbackLocales[availableLocales[i].toString()] = getLocaleFallbacks(availableLocales[i].toString());
   }
   // now we fill up the missing key/value pairs with the values of the fallback locals
   var defHash = text["en"];
   for (var i in text) {
      var locale = convertStringToLocale(i);
      var hash = text[i];
      var fbacks = fallbackLocales[i];
      for (var k = defHash.keys(); k.hasMoreElements(); ) {
         var key = k.nextElement();
         if (!hash.containsKey(key)) {
            for (var j=0; j<fbacks.length; j++) {
               if (text[fbacks[j]] && text[fbacks[j]].containsKey(key)) {
                  hash.put(key, text[fbacks[j]].get(key));
                  break;
               }

            }
         }
      }
      text[locale.toString] = hash;
   }
   app.data.text = text;
   app.data.fallbackLocales = fallbackLocales;
   // import flagReasons overwrites and updates from www's Site.flagReasons skin
   importFlagReasonsFromTOMLSkin();
   return;
}


/**
 * Parses www's custom skin "Site.flagReasons" TOML code and inserts all flag reasons along
 * with their message texts into the already existing language properties hashtable. This 
 * way it overwrites (updates) existing keys and enhances (inserts) non-existing keys.
 * 
 * TOML parsing is done by code\Global\toml.min.js which is a minified adapted version of
 * https://github.com/jakwings/toml-j0.4. The build process for the Helma toml version is done
 * by the tool in /tools/tomlparser.
 */
function importFlagReasonsFromTOMLSkin() {
   try {
      var wwwSite = root.get('www');
      var skinMgr = wwwSite.layout.skins;
      var skin = skinMgr.getSkin('Site', 'flagReasons');
      // parse TOML code in www's Site.flagReasons raw unrendered skin to JSON
      var json = TOML().parse(skin.skin);
      // validate parsing result and consistence
      if (!json || !json.ValidReasonCodes || !json.de || !json.en || !isArray(json.ValidReasonCodes) || json.ValidReasonCodes.length < 1) {
         res.message = getMessage('Site.flagReasons.invalid');
         return;
      }
      // first update the global FLAGTYPEREASONS constant in Global\constants.js
      for (var r in json.ValidReasonCodes) {
         var reason = json.ValidReasonCodes[r];
         // don't rebuild completely however insert only the non-existing reasons
         if (FLAGTYPEREASONS.indexOf(reason) < 0) FLAGTYPEREASONS.push(reason);
      }
      // loop languages de/en; fix me for more languages coming to Twoday
      var languages = ['de', 'en'];
      for (var l in languages) {
         var locale = languages[l];
         // get the existing hashtable for this language
         var hash = app.data.text[locale];
         // loop through reasons and insert/overwrite the appropriate message key text
         for (var r in json.ValidReasonCodes) {
            var reason = json.ValidReasonCodes[r];
            var key = 'warn.flag.' + reason;
            hash.put(key, json[locale][reason]);
         }
      }
   } catch (err) {
      // in case this is a TOML error, use the additional error information
      if (err.hasOwnProperty('location')) {
         res.message = getMessage('Site.flagReasons.syntaxerror', {
            line: err.location.start.line,
            col: err.location.start.column,
            msg: err.message
         });
      } else {
         res.message = getMessage('error.error') + ' -> ' + err.message;
      }
   }
}


/**
 * Returns all relevant localization directories which contain the locale dirs.
 *
 * @return Helma.File[]
 */
function getAllLocalizationDirs() {
   var carr = [];
   // get locale files of modules
   carr = getRepositorySubDirs("locale");
   
   // core locale directory
   if (carr == []) carr.push(getLocalizationDir());
   return carr;
}


/**
 * get all repositories of application
 */
function getRepositorySubDirs(subdir) {
   var dirs = new Array();
   var reps = app.getRepositories();
   for (var i=0; i < reps.length; i++) {
      
      var repType = reps[i].getClass().getName();
      // we assume the subdir to be locate within a code-repository
      if (repType.endsWith("FileRepository")) {
         var dir = new Helma.File(reps[i].getDirectory(), subdir);
         if (dir.exists()) {
            dirs.push(dir);
         }
         // try parent's subdir
         dir = new Helma.File(reps[i].getDirectory().getParent(), subdir);
         if (dir.exists()) {
            dirs.push(dir);
         }

      } else if (repType.endsWith("MultiFileRepository")) {
         var subDirs = reps[i].getDirectory().list();
         for (var j=0; j<subDirs.length; j++) {
            var subDir = new Helma.File(reps[i].getDirectory(), subDirs[j]);
            var dir = new Helma.File(subDir, subdir);
            if (dir.exists()) {
               dirs.push(dir);
            }
         }
      }
   }
   return dirs;
}


/**
 * Returns all relevant locale directories which need to be loaded
 *
 * @return Helma.File[]
 */
function getAllLocaleDirs() {
   var carr = getAllLocalizationDirs();

   var arr = [];
   for (var i=0; i<carr.length; i++) {
      var localeDirs = carr[i].exists() ? carr[i].list() : [];
      for (var j=0; j<localeDirs.length; j++) {
         if (localeDirs[j].isLocaleFormat("_")) {
            arr.push(new Helma.File(carr[i], localeDirs[j]));
         }
      }
   }
   return arr;
}


/**
 * Returns a localization root directory
 *
 * @param create   Boolean  whether to create the directory
 * @return Helma.File
 */
function getLocalizationDir(create) {
   var dir = new Helma.File(getAppDir(), "locale");
   if (!dir.exists() && create) dir.mkdir();
   return dir;
}


/**
 * Returns a language directory within the localization root
 *
 * @param locale  String representation of Locale
 * @param create  Boolean  whether to create the directory
 * @return Helma.File
 */
function getLocaleDir(locale, create) {
   var dir = this.getLocalizationDir(create);
   dir = new Helma.File(dir.getAbsolutePath(), locale);
   if (!dir.exists() && create) dir.mkdir();
   return dir;
}


/**
 * Returns a path to a localized skinDir
 * This is basically a performance-optimized version of
 *   getLocaleSkinDir(locale).getAbsolutePath()
 *
 * @param local   String representation of Locale
 * @return String
 */
function getLocaleSkinDirPath(locale, proto, create) {
   var skinPath = app.dir + Helma.File.separator + ".." + Helma.File.separator + "locale" +
      Helma.File.separator + locale + Helma.File.separator + "skins";
   return skinPath;
}


/**
 * Returns the current Locale to be used
 * The order of precedence is:
 *  1. locale of Site
 *  2. locale of Session
 *  3. locale of User
 *  4. locale of Root
 *
 * FIXME: Session/User should (?) have higher preference for
 *   MemberMgr/subscriptions, MemberMgr/updated,
 *   MemberMgr/memberships, MemberMgr/edit, MemberMgr/register, MemberMgr/newSite,
 *   MemberMgr/sendpwd, MemberMgr/login than Site
 *
 * @return Locale
 */
function getCurrentLocale() {
   try {
      if (res.meta.locale) return res.meta.locale;
   }
   catch (e) { /* ignore */ }
   var locale;
   if (res.handlers.site && res.handlers.site != root.sys_frontSite) {
      locale = res.handlers.site.getLocale();
   } else {
      try {
         if (session && session.data.locale)
            locale = session.data.locale;
         else if (session.user)
            locale = session.user.getLocale();
      }
      catch (err) {
         // error is thrown if session is not available
         // e.g. onStart or in scheduler functions
         ;
      }
   }
   if (!locale) {
      try {
         locale = root.getLocale();
      } catch (err) {
         // HELMABUG weird error was thrown here when called via XML-RPC
         locale = java.util.Locale.getDefault();
      }
   }
   try {
      res.meta.locale = locale;
   }
   catch (e) { /* ignore */ }
   return locale;
}


/**
 * Returns array of Locale String Representations,
 * which are used as fallbacks.
 * Order of precedence for fallback:
 *   1. desiredLocal.language + "_" + desiredLocal.country + "_" + desiredLocal.variant
 *   2. desiredLocal.language + "_" + desiredLocal.country
 *   3. desiredLocal.language
 *   4. defaultLocal.language + "_" + defaultLocal.country + "_" + defaultLocal.variant
 *   5. defaultLocal.language + "_" + defaultLocal.country
 *   6. defaultLocal.language
 *   7. "en"
 *
 * @param str String representation of Locale
 */
function getLocaleFallbacks(str) {
   var arr = new Array();
   var locale = convertStringToLocale(str);
   if (locale == null) return new Array();
   var defLocale = root.getLocale();
   arr.push(str);
   if (locale.getVariant())
      arr.push(locale.getLanguage() + "_" + locale.getCountry());
   if (locale.getCountry())
      arr.push(locale.getLanguage());
   if (locale.getLanguage() != defLocale.getLanguage()) {
      if (defLocale.getVariant())
         arr.push(defLocale.getLanguage() + "_" + defLocale.getCountry() + "_" + defLocale.getVariant());
      if (defLocale.getCountry())
         arr.push(defLocale.getLanguage() + "_" + defLocale.getCountry());
      arr.push(defLocale.getLanguage());
   }
   // make sure that "en" is always as a fallback available
   if (locale.getLanguage() != "en" && defLocale.getLanguage() != "en")
      arr.push("en");
   var fbacks = new Array();
   for (var i in arr) {
      // just consider fallbacks that actually do exist
      if (app.data.text[arr[i]]) fbacks.push(arr[i]);
   }
   return fbacks;
}


/**
 * Helper function to convert string representation to a Locale
 * i.e. locale == convertStringToLocale(locale.toString());
 */
function convertStringToLocale(str) {
   if (!str || typeof str != "string") return null;
   var REGEXP_LOCAL = /(^[a-z]{2}$)|(^([a-z]{2})_([A-Z]{2})$)|(^([a-z]{2})_([A-Z]{2})_([a-zA-Z][a-zA-Z0-9_]*)$)/;
   var match = str.match(REGEXP_LOCAL);
   if (match == null) return null;
   var language = (match[6] || match[3] || match[1]);
   var country = (match[7] || match[4]);
   var variant = (match[8]);
   if (variant) {
      var locale = new java.util.Locale(language, country, variant);
   } else if (country) {
      var locale = new java.util.Locale(language, country);
   } else {
      var locale = new java.util.Locale(language);
   }
   return locale;
}


/**
 * overwrites Helma internal getAge function
 */
function getAge(ts) {
   var now = new Date();
   if (ts == null || ts > now)
      return "";
   var time = now - ts;
   var param = {seconds: Math.floor(time / (1000)),
               minutes: Math.floor(time / (60*1000)),
               hours: Math.floor(time / (60*60*1000)),
               days: Math.floor(time / (24*60*60*1000))};
   if (time < 60*1000)
      return getMessage("generic.getage_seconds", param);
   else if (time < 2*60*1000)
      return getMessage("generic.getage_oneminute", param);
   else if (time < 60*60*1000)
      return getMessage("generic.getage_minutes", param);
   else if (time < 2*60*60*1000)
      return getMessage("generic.getage_onehour", param);
   else if (time < 24*60*60*1000)
      return getMessage("generic.getage_hours", param);
   else if (time < 2*24*60*60*1000)
      return getMessage("generic.getage_oneday", param);
   else
      return getMessage("generic.getage_days", param);
}


/**
 * Returns timestamp of last code update.
 */
function getLastCodeUpdate() {
   return new Date(app.__app__.typemgr.getLastCodeUpdate());
}


/**
 * render the path to the static directory of this
 * layout object
 * @param String name of subdirectory (optional)
 */
function staticPath(subdir) {
   res.write(app.properties.staticPath);
   if (subdir)
      res.write(subdir);
   return;
}


/**
 * return the path to the static directory of this
 * layout object
 * @param String name of subdirectory (optional)
 * @return String path to static directory
 */
function getStaticPath(subdir) {
   res.push();
   this.staticPath(subdir);
   return res.pop();
}


/**
 * render the URL of the directory where images
 * of this layout are located
 */
function staticUrl() {
   res.write(getStaticUrl());
   return;
}


/**
 * return the URL of the directory where images
 * of this layout are located
 * @return String URL of the image directory
 */
function getStaticUrl() {
   res.push();
   this.staticUrl();
   return res.pop();
}


/**
 * return the directory where images of this layout
 * are stored
 * @return Object File Object representing the image
 *                directory on disk
 */
function getStaticDir(subdir) {
   var f = new Helma.File(this.getStaticPath(subdir));
   f.mkdir();
   return f;
}


/**
 * render the path to the protected static directory
 * @param String name of subdirectory (optional)
 */
function protectedStaticPath(subdir) {
   res.write(app.properties.protectedStaticPath);
   if (subdir)
      res.write(subdir);
   return;
}


/**
 * return the path to the static directory of this
 * layout object
 * @param String name of subdirectory (optional)
 * @return String path to static directory
 */
function getProtectedStaticPath(subdir) {
   res.push();
   this.protectedStaticPath(subdir);
   return res.pop();
}


/**
 * return the directory where images of this layout
 * are stored
 * @return Object File Object representing the image
 *                directory on disk
 */
function getProtectedStaticDir(subdir) {
   var f = new Helma.File(this.getProtectedStaticPath(subdir));
   f.mkdir();
   return f;
}


/**
 * Return the directory of this installation.
 *
 * @return Helma.File
 */
function getAppDir() {
   var repDir = new Helma.File(app.dir);
   return new Helma.File(repDir.getParent());
}


/**
 * Return the directory of this installation.
 *
 * @return Helma.File
 */
function getModuleDir() {
   if (app.properties.modulePath)
      return new Helma.File(app.properties.modulePath);

   var dir = getAppDir();
   return new Helma.File(dir, "modules");
}


/**
 * Return the custom module directory of this installation.
 *
 * @return Helma.File
 */
function getCustomModuleDir() {
   if (app.properties.customModulePath)
      return new Helma.File(app.properties.customModulePath);

   var dir = getAppDir();
   return new Helma.File(dir, "custom/modules");
}


/**
 * function returns mimetype according to file-extension
 * returns false if mimetype is unknown
 * @return String Mimetype to use
 */
function getMimeType(ext) {
   switch (ext) {
      case "jpg" :
      case "jpeg" :
         return "image/jpeg";
      case "gif" :
         return "image/gif";
      case "png" :
         return "image/png";
      case "ico" :
         return "image/x-icon";
      case "txt" :
         return "text/plain";
      case "css" :
         return "text/css";
      case "html" :
         return "text/html";
      case "xml" :
         return "text/xml";
      case "js" :
         return "application/x-javascript";
   }
   return false;
}


/**
 * calls AspectJS-code.
 * This function will be called everytime the code from this prototype is loaded.
 *
 * @hook Choice.onGlobalCodeUpdate
 * @doclevel public
 */
function onCodeUpdate() {
   for (var m in app.modules) {
      if (app.modules[m]["onGlobalCodeUpdate"])
         app.modules[m]["onGlobalCodeUpdate"].apply(this);
      if (app.modules[m]["setMgrMenu"])
         app.modules[m]["setMgrMenu"].apply(this);
   }
   return;
}

/**
 * publish to time: set stories online that have been created in the past with a createtime in the future
 *  publishToTimeState == 0: published
 *  publishToTimeState == 1: not published - offline after publishing
 *  publishToTimeState == 2: not published - online, but not on frontpage after publishing
 *  publishToTimeState == 3: not published - online after publishing
 */
function publishToTime() {
   var timeOfLastRun = app.data.publishTimeOfLastRun;
   if (timeOfLastRun && (new Date() - timeOfLastRun.valueOf() < 59 * 1000))
      return;
   var dbc = getDBConnection("twoday");
   var sql = "SELECT TEXT_ID FROM AV_TEXT WHERE TEXT_PROTOTYPE = 'Story' and TEXT_PUBLISH_TO_TIME_STATE > 0 and TEXT_CREATETIME < now()";
   var rows = dbc.executeRetrieval(sql);
   while (rows && rows.next()) {
      var story = Story.getById(rows.getColumnItem("TEXT_ID"));
      story.online = story.publishToTimeState - 1;
      story.publishToTimeState = 0;
      story.site.updateAffectedStaticRSSFeeds(story.topic);
      // hook for modules
      story.applyModuleMethods("onStoryPublishToTime", {});
   }
   if (rows) rows.release();
   dbc.release();
   app.data.publishTimeOfLastRun = new Date();
}


/**
 * load mudules, copy static files into static/[moduleName]
 */
function loadModules() {
   for (var i in app.modules) {
      var moduleName = i;
      root.modules.loadModule(moduleName);

      // core module static directory
      var dir = new Helma.File(getModuleDir(), moduleName);
      var moduleStaticDir = new Helma.File(dir, "install" + java.io.File.separator +  "static");
      if (moduleStaticDir.isDirectory()) {
         var staticDir = new Helma.File(getStaticDir(), moduleName);
         if (!staticDir.exists()) staticDir.mkdir();
         copyDir(moduleStaticDir, staticDir);
      }

      // load custom modules
      var dir = new Helma.File(getCustomModuleDir(), moduleName);
      var customModuleStaticDir = new Helma.File(dir, "install" + java.io.File.separator +  "static");
      if (customModuleStaticDir.isDirectory()) {
         var staticDir = new Helma.File(getStaticDir(), moduleName);
         if (!staticDir.exists()) staticDir.mkdir();
         copyDir(customModuleStaticDir, staticDir);
      }
   }
}


/**
 * helper function to copy dirs
 */
function copyDir(source, dest) {
   if (!source || !source.exists() || !dest) return;
   var source = new Helma.File(source.getAbsolutePath());
   var dest = new Helma.File(dest.getAbsolutePath());
   if (source.getAbsolutePath() == dest.getAbsolutePath()) return;
   if (!dest.getParent().exists()) dest.getParent().mkdir();
   if (!dest.exists()) dest.mkdir();
   var files = source.listRecursive();
   for (var i in files) {
      var f = new Helma.File(files[i]);
      var relpath = f.getAbsolutePath().substring(source.getAbsolutePath().length + 1);
      if (relpath.indexOf("CVS") != -1 || relpath.indexOf(".svn") != -1) continue;
      var tf = new Helma.File(dest, relpath);
      if (f.isDirectory() && !tf.exists()) {
         tf.mkdir();
      } else if (f.isFile() && (!tf.exists() || tf.getLength() != f.getLength())) {
         f.hardCopy(tf.getAbsolutePath());
      }
   }
}


/**
 * Merges the content of core/custom/modules static directories into the main static directory.
 */
function mergeStaticDirs() {
   app.log("START INFO mergeStaticDirs()");
   // merge static
   var staticDir = getStaticDir();
   if (!staticDir.exists()) staticDir.makeDirectory();
   var staticDirs = getRepositorySubDirs("static");
   for (var i=0; i<staticDirs.length; i++) {
      copyDir(staticDirs[i], staticDir);
   }
   // merge protectedStatic
   var protectedStaticDir = getProtectedStaticDir();
   var protectedStaticDirs = getRepositorySubDirs("protectedStatic");
   for (var i=0; i<protectedStaticDirs.length; i++) {
      copyDir(protectedStaticDirs[i], protectedStaticDir);
   }
}


/**
 * Returns current domain.
 * @return String
 */
function getCurrentDomain() {
   var url = path[path.length - 1].href(req.action);
   if (!url.startsWith("http://")) return;
   url = url.substring(7);
   if (url.indexOf("/")) url = url.substring(0, url.indexOf("/"));
   return url;
}


/**
 * Returns the apps.properties as helma.util.ResourceProperties-representation.
 * @return helma.util.ResourceProperties
 */
function getAppsProperties() {
   var file = new java.io.File(app.getServerDir(), "apps.properties");
   var appsProps = new Packages.helma.util.ResourceProperties();
   appsProps.setIgnoreCase(false);
   appsProps.addResource(new Packages.helma.framework.repository.FileResource(file));
   return appsProps;
}


/**
 * Returns the domain of the cookie according to apps.properties.
 * @return String
 */
function getCookieDomain() {
   var cookieDomain = (getAppsProperties()).getProperty(app.name + ".cookieDomain");
   var currDomain = this.getCurrentDomain();
   // cookieDomains must have at least two periods to be valid
   if (currDomain && (currDomain.indexOf(".") == currDomain.lastIndexOf(".")))
      currDomain = null;
   if (!cookieDomain || !currDomain || !(currDomain.endsWith(cookieDomain))) {
      return currDomain;
   } else {
      return cookieDomain;
   }
}

/**
 * Adds a cookie to the response header
 * @param name {String} Name of cookie
 * @param value {String} Value of cookie
 * @param days {Number|null} Expiration days of cookie (null/undefined = 365, 0 = session cookie)
 * @param httpOnly {true|false|null|undefined} HttpOnly-flag of cookie (will be added if param is truthy)
 * @return void
 */
function addHeaderCookie(name, value, days, httpOnly) {
   if (typeof(days) === 'undefined' || days === null) days = 365;

   var domain = (getAppsProperties()).getProperty(app.name + '.cookieDomain');

   var cookie = [
      name + '=' + value,
      'Path=/',
      'Domain=' + domain,
      'SameSite=none',
      'Secure',
      'Partitioned'
   ];
   if (days !== 0) cookie.push('Max-Age=' + days*24*60*60);
   if (!!httpOnly) cookie.push('HttpOnly');
   res.servletResponse.addHeader('Set-Cookie', cookie.join('; '));
}


/**
 * Does nothing, just to avoid error message in log
 * Will be called by Helma on stopping the Helma server.
 */
function onStop() {
}


/**
 * get the URL of the directory where images
 * of this layout are located
 */
function getStaticUrl() {
   return (res.handlers.site && res.handlers.site.url) ? res.handlers.site.href() + "static/" : app.properties.staticUrl;
}


/**
* get the URL of the sys.files folder
*/
function getSysFilesUrl() {
  return getStaticUrl() + "sys.files/";
}


var getCipher = function(password, salt, mode) {
   var ALGORITHM = 'PBEWithMD5AndDES';
   var ITERATIONS = 1000;

   var PBEKeySpec = javax.crypto.spec.PBEKeySpec;
   var PBEParameterSpec = javax.crypto.spec.PBEParameterSpec;
   var SecretKeyFactory = javax.crypto.SecretKeyFactory;
   
   var bytes = java.lang.String(salt).getBytes();
   var pbeParamSpec = new PBEParameterSpec(bytes, ITERATIONS);
   var pbeKeySpec = new PBEKeySpec(password);
   var keyFac = SecretKeyFactory.getInstance(ALGORITHM);
   var pbeKey = keyFac.generateSecret(pbeKeySpec);
   var pbeCipher = javax.crypto.Cipher.getInstance(ALGORITHM);

   if (mode !== 'decrypt' && mode !== 'encrypt') throw Exception('Unknown Cipher Mode');

   pbeCipher.init(javax.crypto.Cipher[mode.toUpperCase() + '_MODE'], pbeKey, pbeParamSpec);
   return pbeCipher;
}
