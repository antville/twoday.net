
/**
 * Function sets bulk mail of 100 per call
 */
function tdyMailingSend() {
  // update AV_USER set USER_TDY_MAILING=1 where USER_EMAIL_ISCONFIRMED=1 AND (TO_DAYS(NOW()) - TO_DAYS(USER_LASTVISIT) <= 180);
  // update AV_USER set USER_TDY_MAILING=1 where USER_EMAIL='m@knallgrau.at' or USER_EMAIL='michi@knallgrau.at';
  var sql = "select USER_NAME from AV_USER where USER_TDY_MAILING=1 limit 100";
  var dbc = getDBConnection("twoday");
  var rows = dbc.executeRetrieval(sql);

  while (rows.next()) {
    var username = rows.getColumnItem(1);
    var usr = root.users.get(username);
    if (usr) {
      var param = new Object();
      param.username = username;
      var body    = renderSkinAsString("tdyMailing", param);
      var subject = "Betreff";
      var sender  = "info@twoday.net";
      sendMail(sender, usr.email, subject, body);
      res.debug("INFO: send email to " + usr.email);
      usr.tdyMailing = null;
    } else {
      res.debug("ERROR: no user found " + username);
    }
  }
  return;
}
