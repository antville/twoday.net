
/**
 * string containing reserved site aliases
 */
var TDY_RESERVEDALIASES = ",ad,ae,af,ag,ai,al,am,an,ao,aq,ar,as,at,au,aw,az,ba,bb,bd,be," +
  "bf,bg,bh,bi,bj,bm,bn,bo,br,bs,bt,bv,bw,by,bz,ca,cc,cf,cg,ch,ci,ck,cl,cm," +
  "cn,co,cr,cu,cv,cx,cy,cz,de,dj,dk,dm,do,dz,ec,ee,eg,eh,er,es,et,eu,fi,fj," +
  "fk,fm,fo,fr,fx,ga,gb,gd,ge,gf,gh,gi,gl,gm,gn,gp,gq,gr,gs,gt,gu,gw,gy,hk," +
  "hm,hn,hr,ht,hu,id,ie,ii,il,in,io,iq,ir,is,it,jm,jo,jp,ke,kg,kh,ki,km,kn," +
  "kp,kr,kw,ky,kz,la,lb,lc,li,lk,lr,ls,lt,lu,lv,ly,ma,mc,md,mg,mh,mk,ml,mm," +
  "mn,mo,mp,mq,mr,ms,mt,mu,mv,mw,mx,my,mz,so,na,nc,ne,nf,ng,ni,nl,no,np,nr," +
  "nu,nz,om,pa,pe,pf,pg,ph,pk,pl,pm,pn,pr,pt,pw,py,qa,re,ro,ru,rw,sa,sb,sc," +
  "sd,se,sg,sh,si,sj,sk,sl,sm,sn,so,sr,st,sv,sy,sz,tc,td,tf,tg,th,tj,tk,tm," +
  "tn,to,tp,tr,tt,tv,tw,tz,ua,ug,um,us,uy,uz,va,vc,ve,vg,vi,vn,vu,wf,ws,ye," +
  "yt,yu,za,zm,zr,zw,wien,linz,graz,salzburg,innsbruck,bregenz,eisenstadt," +
  "stpoelten,klagenfurt,muenchen,berlin,bern,duesseldorf,frankfurt,hamburg," +
  "paris,newyork,london,madrid,barcelona,budapest,prag,lissabon,bern,rom," +
  "warsaw,istanbul,tokyo,losangeles,sanfrancisco,uk," +
  "zuerich,en,status,iedit,mpay24,twoday,knallgrau,server," +
  "web1,web2,web3,web4,web5,web6,db1,db2,db3,db4,db5,db6,static,";


/**
 * checks if a string is reserved as site alias
 * @param str String
 * @return Boolean
 */
function tdyReservedAlias(str) {
   if (TDY_RESERVEDALIASES.indexOf("," + str.toLowerCase() + ",") != -1) return true;
   return false;
}
