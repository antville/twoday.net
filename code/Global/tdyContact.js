
app.modules.tdyContact = {
   title: "tdyContact",
   description: "Contact Action",
   version: "0.2",
   state: "stable",
   url: "",
   author: "Franz Philipp Moser",
   authorEmail: "philipp.moser@knallgrau.at",
   authorUrl: "http://www.knallgrau.at",
   authorOrganisation: "knallgrau.at",
   copyright: "(c) 2005 knallgrau.at",
   license: "twoday",
   licenseUrl: "http://www.twoday.org",
   isSidebar: false,
   hasSiteSettings: false,
   // equipicture, chorherr
   weblogIds: "14780,13653"
}
