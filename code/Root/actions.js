
/**
 * main action
 */
function main_action() {
   res.data.title = root.getTitle();
   res.handlers.mgr = this.manage;
   // perform redirect to setupWizard if no user exists yet
   if (!root.users.size()) {
      res.redirect(root.href("sitesetupwizard"));
   }

   if (res.handlers.site && !res.data.alternativeRendering) {
      res.handlers.site.main_action();
   } else {
      res.data.body = this.renderSkinAsString("main");
      this.renderPage();
      logAccess();
   }
   return;
}


/**
 * wrapper to access colorpicker
 */
function colorpicker_action() {
   if (!req.data.skin)
      req.data.skin = "colorpicker";
   renderSkin(req.data.skin);
   return;
}


/**
 * rss action
 */
function rss_action() {
   res.contentType = "text/xml";

   var now = new Date();
   var systitle = root.getTitle();
   var sdf = new java.text.SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
   sdf.setTimeZone(new java.util.SimpleTimeZone(0, "UTC"));

   var size = this.publicSites.size();
   var max = req.data.max ? parseInt(req.data.max, 10) : 25;
   max = Math.min(max, size, 50);

   var param = new Object();
   var items = new java.lang.StringBuffer();
   var resources = new java.lang.StringBuffer();

   for (var i=0; i<max; i++) {
      var site = this.publicSites.get(i);
      if (site.online && site.lastposting) {
         param.title = site.title ? site.title : site.alias;
         param.publisher = systitle;
         param.creator = site.creator ? site.creator.name : "";
         param.email = "";
/*         if (site.email)
            param.email = site.email.entitize();
         else*/
         if (site.creator && site.creator.publishemail) {
            param.email = site.creator.email.entitize();
         }
         param.isodate = sdf.format(site.lastposting)
         param.date = site.preferences.getProperty("tagline") ? "" : param.isodate;
         param.year = site.lastposting.getFullYear();
         items.append(site.renderSkinAsString("rssItem", param));
         resources.append(site.renderSkinAsString("rssResource", param));
      }
   }
   param = new Object();
   param.title = systitle;
   param.email = root.preferences.getProperty("sys_email").entitize();
   param.year = now.getFullYear();
   param.lastupdate = sdf.format(now);
   param.items = items.toString();
   param.resources = resources.toString();
   this.renderSkin("rss", param);
   return;
}


/**
 * wrapper for rss feed
 */
function rss_xml_action() {
   res.redirect(this.href("index.rdf"));
   return;
}


/**
 * wrapper action for rss_action
 */
function index_rss_action() {
   res.redirect(this.href("index.rdf"));
   return;
}


/**
 * wrapper action for rss_action
 */
function index_rdf_action() {
   this.rss_action();
   return;
}


/**
 * wrapper action for rss_action
 */
function index_xml_action() {
   res.redirect(this.href("rss"));
   return;
}


/**
 * action called when a site has been blocked
 */
function blocked_action() {
   var s = root.get(req.data.site);
   var param = new Object();
   if (s) param.siteName = s.getName();
   res.data.title = root.getTitle() + " - 404 - blocked";
   res.data.body = root.renderSkinAsString("siteBlocked", param);
   var check = s.checkAccess("unblock", session.user, res.meta.memberlevel);
   if (check == null) { // check went OK
      param.unblockUrl = s.href("unblock");
      res.data.body += root.renderSkinAsString("siteUnblock", param);
   }
   root.renderPage();
   return;
}


/**
 * 404 action
 */
function notfound_action() {
   if (res.handlers.site) {
      var theSite = res.handlers.site;
      if (!theSite.online) {
         try {
            theSite.checkView(session.user, res.meta.memberlevel);
         } catch (e) {
            res.redirect(theSite.href());
            return;
         }
      }
      // get action from path because 
      var pathArray = req.path.split("/");
      // check if last item in array has a length
      // because of trailing slashes
      if (pathArray.length > 1 && pathArray.length <= 3) {
         var skinName = (pathArray[(pathArray.length - 1)].length == 0) ? pathArray[(pathArray.length - 2)].toLowerCase() : pathArray[(pathArray.length - 1)].toLowerCase();
         if (skinName) {
            // check if .
            var extention = null;
            if (skinName.indexOf(".") != -1) {
               var nameExt = skinName.split(".");
               extention = nameExt[1];
               skinName = nameExt[0] + extention.toUpperCase();
            }
            skinName = skinName.substring(0, 50);
            skinName = skinName + "Custom";
            var sk = res.handlers.layout ? res.handlers.layout.skins.getSkinSource("Site", skinName) : null;
            if (sk) {
               res.status = 200;
               if (extention && getMimeType(extention)) {
                  res.contentType = getMimeType(extention);
                  theSite.renderSkin(skinName);
               } else  {
                  res.data.title = theSite.title;
                  res.data.body = theSite.renderSkinAsString(skinName);
                  theSite.renderPage();
               }
               return;
            }
         }
      }
   }
   // return to normal flow of not found action
   res.data.title = root.getTitle() + " - 404 - not found";
   req.data.path = escape(req.path);
   res.data.body = root.renderSkinAsString("notfound");
   if (res.handlers.site && res.handlers.site.online) {
      res.handlers.site.renderPage();
   } else if (res.handlers.site) {
      if (session.user) {
         try {
            res.handlers.site.checkView(session.user, res.meta.memberlevel);
            res.handlers.site.renderPage();
         } catch (e) {
            root.renderPage();
         }
      } else {
         session.data.referrer = res.handlers.site.href();
         res.redirect(res.handlers.site.members.href("login"));
      }
   } else {
      root.renderPage();
   }
   return;
}

/**
 * error action
 */
function sys_error_action() {
   try {
      res.data.title = root.getTitle() + " - Error";
      res.data.body = root.renderSkinAsString("sysError");
      if (session.user && session.user.sysadmin)
         res.data.body += "<p>"+res.error+"</p>";
   } catch (err) {
      ;
   }
   if (res.handlers.site && res.handlers.site.online) {
      res.handlers.site.renderPage();
   } else {
      try {
         root.renderPage();
      } catch (err) {
         root.renderSkin("plain", {});
      }
   }
   return;
}


/**
 * action to render external stylesheet
 */
function main_css_action() {
   res.setLastModified(res.handlers.layout.skins.getSkinLastModified("Root", "style"));
   res.contentType = "text/css";
   this.renderSkin("style");
}


/**
 * action to render external javascript
 */
function main_js_action() {
   res.setLastModified(res.handlers.layout.skins.getSkinLastModified("Root", "javascript"));
   res.contentType = "text/javascript";
   res.handlers.context = root; // necessary to generate correct URLs
   root.renderSkin("javascript");
   root.renderSkin("systemscripts");
}


/**
 * The installation setup wizard.
 * Performs the following tasks:
 *  1. license agreement (step: 'license')
 *  2. register user (step: 'rootadmin')
 *  3. create a first site (step: 'rootsite')
 */
function sitesetupwizard_action() {
   // security check
   if (root.users.size() > 0 && (!session.user || !session.user.sysadmin)) {
      res.redirect(root.href());
   }

   // initialize
   res.data.action = root.href("sitesetupwizard");

   // first page
   if (!req.data.step) {
      req.data.step = (session.user) ? "rootsite" : "license";
   }

   // handle cancel|next|previous
   if (req.data.submit == "cancel" || req.data.cancel) {
   // cancel
      if (req.data.step == "license") {
         ;
      } else if (req.data.step == "rootadmin") {
         res.redirect(root.href("sitesetupwizard"));
      } else if (req.data.step == "rootsite") {
         res.redirect(root.href());
      }

   } else if (req.data.next) {
   // next
      if (req.data.step == "license") {
         if (req.data.acceptlicense) {
            root.manage.onInstallation();
            res.redirect(root.href("sitesetupwizard") + "?step=rootadmin");
         } else {
            res.message = getMessage("error.acceptLicense");
         }

      } else if (req.data.step == "rootadmin") {
         if (root.users.size() > 0) {
            res.redirect(root.href("sitesetupwizard") + "?step=rootsite");
         }
         try {
            req.data.terms = 1;
            var result = root.members.evalRegistration(req.data);
            var usr = result.obj;
            res.message = result.toString();
            // now we log in the user and send the confirmation mail
            usr.doLogin();
            session.user.emailIsConfirmed = 1;
            root.preferences.setProperty("sys_email", req.data.email);
            res.redirect(root.href("sitesetupwizard") + "?step=rootsite");
         } catch (err) {
            res.message = err.toString();
         }

      } else if (req.data.step == "rootsite") {
         try {
            var result = root.evalNewSite(req.data, session.user);
            res.message = result.toString();
            // session.user.url = result.obj.href();
            // root.sys_frontSite = result.obj;
            // root.preferences.setProperty("sys_title", req.data.title);
            res.redirect(root.href());
         } catch (err) {
            res.message = err.toString();
         }
      }
   }

   // compose page
   var skinName;
   if (req.data.step == "license") {
      res.data.title = getMessage("Root.siteSetupWizard.license.header");
      res.data.body = root.manage.renderSkinAsString("sitesetupwizard_license_body");
      res.data.sidebar01 = root.manage.renderSkinAsString("sitesetupwizard_sidebar");
   } else if (req.data.step == "rootadmin") {
      res.data.title = getMessage("Root.siteSetupWizard.rootAdmin.header");
      res.data.body = root.manage.renderSkinAsString("sitesetupwizard_rootadmin_body");
      res.data.sidebar01 = root.manage.renderSkinAsString("sitesetupwizard_sidebar");
   } else if (req.data.step == "rootsite") {
      res.data.title = getMessage("Root.siteSetupWizard.rootSite.header");
      res.data.body = root.manage.renderSkinAsString("sitesetupwizard_rootsite_body");
      res.data.sidebar01 = root.manage.renderSkinAsString("sitesetupwizard_sidebar");
   }

   // render wizard page
   root.manage.renderSkin("wizardSimplePage", {});
   return;
}


/**
 * bugreport action
 */
function bugreport_action() {
   var param = {};

   if (req.data.cancel) {
      res.redirect(req.data.http_referer);
   } else if (req.data.send) {
      try {
         checkSecretKey();
         res.message = this.evalBugReport(req.data, session.user);
         res.redirect(req.data.http_referer);
      } catch (err) {
         res.message = err.toString();
      }
   }

   req.data.bugreport_name = (session.user) ? session.user.name : "";
   req.data.bugreport_email = (session.user) ? session.user.email : "";
   res.data.action = this.href(req.action);
   res.data.title = "Bug Report";
   res.data.body = this.renderSkinAsString("bugreport");

   this.renderSkin("plain", param);
   return;
}

/**
 * just a wrapper function for rendering jdbc Statistics
 */
function knallgrau_jdbcStatistics_action() {
   if (global.fpmedv && global.fpmedv.JdbcStatistics) {
	  if (req.data.reset && session.user && session.user.sysadmin) {
		  fpmedv.JdbcStatistics.resetStatistics();
	  }
      fpmedv.JdbcStatistics.renderStatistics();
   } else {
      res.write("Not available");
   }
}
