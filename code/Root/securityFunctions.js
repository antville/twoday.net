
/**
 * Permission checks (called by hopobject.onRequest())
 *
 * @param action  String, name of action
 * @param usr     User, who wants to access an action
 * @param level   Int, Membership level
 * @return Object if permission was denied <br />
 *   .message  message, if something went wrong <br />
 *   .url      recommanded redirect URL
 * @doclevel public
 */
function checkAccess(action, usr, level) {
   if (usr && usr.sysadmin) return;
   var url;
   try {
      switch (action) {
         case "main" :
         case "colorpicker" :
         case "rss" :
         case "rss_xml" :
         case "index_rss" :
         case "index_rdf" :
         case "index_xml" :
         case "blocked" :
         case "notfound" :
         case "sys_error" :
         case "main_js" :
         case "main_css" :
         case "terms" :
         case "sitesetupwizard" :
         case "bugreport" :
            break;
         default :
            url = this.applyAllModuleMethods("checkRootAccess", [action, usr, level, url]);
            break;
      }
   } catch (deny) {
      return {message: deny.toString(), url: url ? url : root.href()};
   }
   return;
}


/**
 * function checks if user is allowed to create a new Site
 * @param Obj User-Object
 */
function checkAddNewSite(usr) {
   if (usr.sysadmin) {
      return null;
   }

   // Creation of new sites for non-admins is disabled
   throw new DenyException("globalMayCreateSite");

   if (usr.may("CREATESITE") == false) {
      throw new DenyException("globalMayCreateSite");
   }
   if (root.preferences.getProperty("sys_limitNewSites") == "2" && !usr.sysadmin) {
      throw new DenyException("siteCreateOnlyAdmins");
   }
   if (root.preferences.getProperty("sys_limitNewSites") == "1" && !usr.trusted) {
      throw new DenyException("siteCreateNotAllowed");
   }
   if (root.preferences.getProperty("sys_limitSitesPerUser") && usr.sites.count() >= root.preferences.getProperty("sys_limitSitesPerUser") && !usr.trusted && !usr.sysadmin) {
      // check if user has already the maximum number of allowed sites
      throw new DenyException("siteCreateTooManySites", {count: root.preferences.getProperty("sys_limitSitesPerUser")});
   }
   if (root.preferences.getProperty("sys_minMemberAge")) {
      // check if user has been a registered member for long enough
      var regTime = Math.floor((new Date() - session.user.registered) / Date.ONEDAY);
      if (regTime < root.preferences.getProperty("sys_minMemberAge"))
         throw new DenyException("siteCreateMinMemberAge", [regTime, root.preferences.getProperty("sys_minMemberAge")]);
   }
   if (root.preferences.getProperty("sys_minMemberSince")) {
      // check if user has registered before the defined timestamp
      if (session.user.registered > root.preferences.getProperty("sys_minMemberSince"))
         throw new DenyException("siteCreateMinMemberSince", formatTimestamp(root.preferences.getProperty("sys_minMemberSince")));
   }
   if (root.preferences.getProperty("sys_waitAfterNewSite") && usr.sites.count()) {
      // check if user has to wait some more time before creating a new Site
      var lastCreation = Math.floor((new Date() - usr.sites.get(0).createtime) / Date.ONEDAY);
      if (lastCreation < root.preferences.getProperty("sys_waitAfterNewSite"))
         throw new DenyException("siteCreateWait", [root.preferences.getProperty("sys_waitAfterNewSite"), root.preferences.getProperty("sys_waitAfterNewSite") - lastCreation]);
   }
   // email must be confirmed before site creation!
   // TODO should be an module like AntiSpam
   if (!usr.emailIsConfirmed) {
      throw new DenyException("siteCreateNotAllowed");
   }
   return;
}
