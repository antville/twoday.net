
/**
 * step01: convert preferences of Sites
 */
function tdyUpgrade01_action() {
   writeln("-------- Starting tdyUpgrade01 --------");
   var images = ["knallgrau", "agb", "xml", "resident_of_twoday", "AGBs", "favicon", "marquee", "xmlbutton", "pixel", "twoday1", "twoday2", "rss_complete", "rss_topic", "hop"];
   var layout = root.layouts.get("default");
   for (var i in images) {
      if (layout.images.get(images[i])) {
         layout.images.deleteImage(layout.images.get(images[i]));
         res.debug("removed " + images[i]);
      }
   }
   var dbCon = getDBConnection("twoday");
   var query = "select SITE_ALIAS, SITE_TAGLINE, SITE_BGCOLOR, SITE_TEXTFONT, SITE_TEXTCOLOR, SITE_TEXTSIZE, SITE_LINKCOLOR, SITE_ALINKCOLOR, SITE_VLINKCOLOR, SITE_LINECOLOR, SITE_BLOCKCOLOR, SITE_TITLEFONT, SITE_TITLECOLOR, SITE_TITLESIZE, SITE_SMALLFONT, SITE_SMALLCOLOR, SITE_SMALLSIZE, SITE_HASDISCUSSIONS, SITE_USERMAYCONTRIB, SITE_SHOWDAYS, SITE_SHOWARCHIVE, SITE_LANGUAGE, SITE_COUNTRY, SITE_TIMEZONE, SITE_LONGDATEFORMAT, SITE_SHORTDATEFORMAT from AV_SITE";
   var sites = dbCon.executeRetrieval(query);
   while (sites.next()) {
      var s = root.get(sites.getColumnItem("SITE_ALIAS"));
      writeln("converting site '" + s.alias + "' ...");
      var prefs = new HopObject();
      prefs.tagline = sites.getColumnItem("SITE_TAGLINE");
      prefs.bgcolor = sites.getColumnItem("SITE_BGCOLOR");
      prefs.textfont = sites.getColumnItem("SITE_TEXTFONT");
      prefs.textcolor = sites.getColumnItem("SITE_TEXTCOLOR");
      prefs.textsize = sites.getColumnItem("SITE_TEXTSIZE");
      prefs.linkcolor = sites.getColumnItem("SITE_LINKCOLOR");
      prefs.alinkcolor = sites.getColumnItem("SITE_ALINKCOLOR");
      prefs.vlinkcolor = sites.getColumnItem("SITE_VLINKCOLOR");
      prefs.linecolor = sites.getColumnItem("SITE_LINECOLOR");
      prefs.blockcolor = sites.getColumnItem("SITE_BLOCKCOLOR");
      prefs.titlefont = sites.getColumnItem("SITE_TITLEFONT");
      prefs.titlesize = sites.getColumnItem("SITE_TITLESIZE");
      prefs.titlecolor = sites.getColumnItem("SITE_TITLECOLOR");
      prefs.smallfont = sites.getColumnItem("SITE_SMALLFONT");
      prefs.smallsize = sites.getColumnItem("SITE_SMALLSIZE");
      prefs.smallcolor = sites.getColumnItem("SITE_SMALLCOLOR");
      prefs.discussions = sites.getColumnItem("SITE_HASDISCUSSIONS");
      prefs.usercontrib = sites.getColumnItem("SITE_USERMAYCONTRIB");
      prefs.archive = sites.getColumnItem("SITE_SHOWARCHIVE");
      prefs.days = sites.getColumnItem("SITE_SHOWDAYS");
      prefs.language = sites.getColumnItem("SITE_LANGUAGE");
      prefs.country = sites.getColumnItem("SITE_COUNTRY");
      prefs.timezone = sites.getColumnItem("SITE_TIMEZONE");
      prefs.longdateformat = sites.getColumnItem("SITE_LONGDATEFORMAT");
      prefs.shortdateformat = sites.getColumnItem("SITE_SHORTDATEFORMAT");
      prefs.use_global_spamfilter = 1;
      prefs.language = "de";
      prefs.country = null;
      prefs.modBetterEditorEditor = "bettereditor";
      prefs.usedayheader = 1;
      prefs.shortdateformat = "d. MMM, HH:mm";
      prefs.modToolbarShow = s.showtoolbarfor;
      prefs.sidebar01 = "modUserStatus,modSiteMenu,modContributorMenu,modAdminMenu,modWeblogSearchbar";
      prefs.sidebar02 = "modCalendar,modRecentUpdates,modWeblogStatus,modCredits";
      prefs.rssfeedcomplete = 1;
      prefs.rssfeedsummary = 0;
      prefs.rssfeedcomments = 0;
      prefs.rssfeedtopics = 1;
      prefs.mostReadIsPrivate = "1";
      prefs.referrersIsPrivate = "1";
      s.preferences.setAll(prefs);
      writeln(" done");
   }
   sites.release();
   for (var i=0; i<root.users.count(); i++) {
     var u = root.users.get(i);
     u.preferences.setProperty("sex", u.sex);
     u.preferences.setProperty("age", u.age);
     u.preferences.setProperty("city", u.city);
     u.preferences.setProperty("language", "de");
     u.preferences.setProperty("country", (u.country == "de" || u.country == "de" || u.country == "ch") ? u.country.toUpperCase() : null);
   }
   writeln("-------- Finished tdyUpgrade01 --------");
   res.debug("DONE");
   return;
}


/*
 * convert sites
 */
function tdyUpgrade02_action() {
   writeln("-------- Starting tdyUpgrade02 --------");
   var defaultLayout = root.layouts.get("twodayclassic");
   // set root layout to defaultLayout
   root.sys_layout = defaultLayout;
   // site conversion
   // 1) create an initial layout for all sites
   // 2) copy the prefs from site to layout
   var size = root.size();
   // properties that should be moved from
   // site preferences to layout preferences
   var arr = ["bgcolor", "titlecolor", "titlefont", "titlesize", "textcolor",
              "textfont", "textsize", "smallcolor", "smallfont", "smallsize",
              "linkcolor", "alinkcolor", "vlinkcolor", "blockcolor", "linecolor"];
   for (var i=0;i<size;i++) {
      var s = root.get(i);
      writeln("Converting site '" + s.alias + "'");
      // create a new initial layout object as a child
      // layout of the default system layout
      var newLayout = s.layout;
      var prefs = new HopObject();
      for (var prop in arr) {
         var val = s.preferences.getProperty(arr[prop]);
         prefs[arr[prop]] = val;
         s.preferences.deleteProperty(arr[prop]);
      }
      newLayout.preferences_xml = Xml.writeToString(prefs);
   }
   writeln("-------- Finished tdyUpgrade02 --------");
   res.debug("DONE");
   return true;
}


/**
 * convert skins
 */
function tdyUpgrade03_action() {
   writeln("-------- Starting tdyUpgrade03 --------");
   var arr = ["bgcolor", "titlecolor", "titlefont", "titlesize", "textcolor",
              "textfont", "textsize", "smallcolor", "smallfont", "smallsize",
              "linkcolor", "alinkcolor", "vlinkcolor", "blockcolor", "linecolor"];
   var pattern;
   var replaceStr;
   var exp;
   var size = root.size();
   for (var i=0;i<size;i++) {
      var currLayout = root.get(i).layout;
      writeln("Converting skins of layout '" + currLayout.alias + "'");
      var currSkinmgr = currLayout.skins;
      for (var j=0;j<currSkinmgr.size();j++) {
         var proto = currSkinmgr.get(j);
         for (var k=0;k<proto.size();k++) {
            var s = proto.get(k);
            writeln(proto.groupname + "." + s.name);
            var source = s.skin;
            // replace <% site.bgcolor %> with <% layout.bgcolor %>
            for (var l in arr) {
               pattern = "<%\\s*(?:site|this)\\." + arr[l] + "([^%]+)%>";
               replaceStr = "<% layout." + arr[l] + "$1%>";
               exp = new RegExp(pattern, "gi");
               source = source.replace(exp, replaceStr);
            }
            // replace any occurrence of root|this.systitle
            pattern = "<%\\s*(?:root|this)\\.systitle([^%]+)%>";
            replaceStr = "<% root.title$1%>";
            exp = new RegExp(pattern, "gi");
            source = source.replace(exp, replaceStr);
            // replace any occurrence of root|this.sysurl
            pattern = "<%\\s*(?:root|this)\\.sysurl([^%]+)%>";
            replaceStr = "<% root.url$1%>";
            exp = new RegExp(pattern, "gi");
            source = source.replace(exp, replaceStr);
            s.skin = source;
            // update the prototype name
            // to mixed case convention
            switch (s.proto) {
               case "global" :
                  var pname = "Global";
                  break;
               case "root" :
                  var pname = "Root";
                  break;
               case "user" :
                  var pname = "User";
                  break;
               case "hopobject" :
                  var pname = "HopObject";
                  break;
               default:
                  var pname = s.proto;
            }
            // if skin isn't a default one, mark it
            // as custom
            if (!app.skinfiles[pname][s.name])
               s.custom = 1;
         }

      }
      writeln("OK\n");
   }
   writeln("-------- Finished tdyUpgrade03 --------");
   res.debug("DONE");
   return true;
}


/**
 * Step04:
 *  + convertFallbacks
 *  + convertArchive
 */
function tdyUpgrade04_action() {
   // <% image name="header" fallback="/" %>   ->  <% layout.image name="header" %>
   // <% image name="footer" fallback="/" %>   ->  <% layout.image name="footer" %>
   // <% image name="ring" fallback="/" %>     ->  <% layout.image name="ring" %>
   // <% image name="icon" fallback="/" %>     ->  <% image name="icon" fallback="/icon" %>
   // <% image name="xyz" fallback="/" %>      ->  <% image name="xyz" fallback="/xyz" %>
   // <% image name="xyz" fallback="abc" %>    ->  <% image name="xyz" fallback="abc/xyz" %>

   var sql = "select SKIN_F_LAYOUT, LAYOUT_F_SITE, SKIN_PROTOTYPE, SKIN_NAME from AV_SKIN, AV_LAYOUT where (SKIN_SOURCE like '%<\% image %' or SKIN_SOURCE LIKE '%<\%image %') and SKIN_F_LAYOUT = LAYOUT_ID";
   dbc = getDBConnection("twoday");
   var rows = dbc.executeRetrieval(sql);
   var values = new Array();
   while (rows.next()) {
      var obj = {siteID: rows.getColumnItem(1),
                 layoutID: rows.getColumnItem(2),
                 proto: rows.getColumnItem(3),
                 name: rows.getColumnItem(4)}
      values.push(obj);
   }
   rows.release();

   for (var i=0; i<values.length; i++) {
      var obj = values[i];
      var s = root.getById(obj.siteID);
      if (!s) {
         res.debug("no site for " + obj.siteID);
         continue;
      }
      var l = s.layouts.getById(obj.layoutID);
      var sk = l.skins.get(obj.proto).get(obj.name).skin;
      var resultskin = new java.lang.String(sk);

      var reg01 = new RegExp('<%[ ]*image[ ]([ ]*[a-z][a-z0-9]*="[^"]*")*[ ]*%>', "gi"); // find image_macro
      var result01 = sk.match(reg01);
      if(!result01) continue;
      for (var j=0; j<result01.length; j++) {
         reg01 = new RegExp('[a-z][a-z0-9]*="[^"]*"', "gi"); // loop through params

         var nameParam;
         var fallbackParam;
         var paramNames = new Array();
         var paramValues = new Array();
         do {
            var result02 = reg01.exec(result01[j]);
            if (result02 != null) {
               var reg02 = new RegExp('(.*)="(.*)"', "gi");
               var result03 = reg02(result02[0]); // split parameter name and value
               var paramName = result03[1];
               var paramValue = result03[2];
               paramNames[paramNames.length] = paramName;
               paramValues[paramValues.length] = paramValue;
               if (paramName == "name") nameParam = paramValue;
               if (paramName == "fallback") fallbackParam = paramValue;
            }
         } while(result02 != null);

         if (nameParam && (nameParam=="header" || nameParam=="footer" || nameParam=="ring"))
            var replacement = "<% layout.image ";
         else
            var replacement = "<% image ";
         for (var k=0; k<paramNames.length; k++) {
            if (paramNames[k] == "fallback") {
               if (nameParam && (nameParam=="header" || nameParam=="footer" || nameParam=="ring")) {
                  continue;
               } else {
                 if (paramValues[k] == "/" || paramValues[k] == "/" + nameParam)
                    paramValues[k] = "/" + nameParam;
                 else
                    paramValues[k] += "/" + nameParam;
               }
            }
            replacement += paramNames[k] + "=\"" + paramValues[k] + "\" ";
         }
         replacement += "%>";

         res.writeln(result01[j]);
         res.writeln("<font color=\"0000dd\">" + replacement + "</font><br>");
         resultskin = new java.lang.String(resultskin.replaceAll(result01[j], replacement));
      }
      l.skins.get(obj.proto).get(obj.name).skin = resultskin;
   }

   // <% history ... show="X" ... %>           ->  <% history ... limit="X" %>
   var sql = "select SKIN_F_LAYOUT, LAYOUT_F_SITE, SKIN_PROTOTYPE, SKIN_NAME from AV_SKIN, AV_LAYOUT where (SKIN_SOURCE like '%<\% history %' or SKIN_SOURCE LIKE '%<\%history %') and SKIN_F_LAYOUT = LAYOUT_ID";
   dbc = getDBConnection("twoday");
   var rows = dbc.executeRetrieval(sql);
   var values = new Array();
   while (rows.next()) {
      var obj = {siteID: rows.getColumnItem(1),
                 layoutID: rows.getColumnItem(2),
                 proto: rows.getColumnItem(3),
                 name: rows.getColumnItem(4)}
      values.push(obj);
   }
   rows.release();

   for (var i=0; i<values.length; i++) {
      var obj = values[i];
      var s = root.getById(obj.siteID);
      if (!s) {
         res.debug("no site for " + obj.siteID);
         continue;
      }
      var l = s.layouts.getById(obj.layoutID);
      var sk = l.skins.get(obj.proto).get(obj.name).skin;
      var resultskin = new java.lang.String(sk);

      var reg01 = new RegExp('<%[ ]*history[ ]([ ]*[a-z][a-z0-9]*="[^"]*")*[ ]*%>', "gi"); // find history_macro
      var result01 = sk.match(reg01);
      if(!result01) continue;
      for (var j=0; j<result01.length; j++) {
         reg01 = new RegExp('[a-z][a-z0-9]*="[^"]*"', "gi"); // loop through params

         var paramNames = new Array();
         var paramValues = new Array();
         do {
            var result02 = reg01.exec(result01[j]);
            if (result02 != null) {
               var reg02 = new RegExp('(.*)="(.*)"', "gi");
               var result03 = reg02(result02[0]); // split parameter name and value
               var paramName = result03[1];
               var paramValue = result03[2];
               if (paramName == "show") paramName = "limit";
               paramNames[paramNames.length] = paramName;
               paramValues[paramValues.length] = paramValue;
            }
         } while(result02 != null);

         var replacement = "<% history ";
         for (var k=0; k<paramNames.length; k++) {
            replacement += paramNames[k] + "=\"" + paramValues[k] + "\" ";
         }
         replacement += "%>";

         res.writeln(result01[j]);
         res.writeln("<font color=\"0000dd\">" + replacement + "</font><br>");
         resultskin = new java.lang.String(resultskin.replaceAll(result01[j], replacement));
      }
      l.skins.get(obj.proto).get(obj.name).skin = resultskin;
   }

   res.debug("DONE");
   return;
}

function tdyUpgrade05_action() {
   // /static/images/ferromonte/  -> /static/ferromonte/images
   // /static/files/ferromonte/   -> /static/ferromonte/files
   // /static/images/SITEALIAS/   -> /static/SITEALIAS/images
   // /static/files/SITEALIAS/    -> /static/SITEALIAS/files
   var sql = "select SKIN_F_LAYOUT, LAYOUT_F_SITE, SKIN_PROTOTYPE, SKIN_NAME from AV_SKIN, AV_LAYOUT, AV_SITE where AV_LAYOUT.LAYOUT_ALIAS = AV_SKIN.SKIN_F_LAYOUT and AV_SITE.SITE_ID = AV_LAYOUT.LAYOUT_F_SITE and (AV_SKIN.SKIN_SOURCE like concat('%/static/images/', AV_SITE.SITE_ALIAS, '%') or AV_SKIN.SKIN_SOURCE like concat('%/static/files/', AV_SITE.SITE_ALIAS, '%'))";
   dbc = getDBConnection("twoday");
   var rows = dbc.executeRetrieval(sql);
   var values = new Array();
   while (rows.next()) {
      var obj = {siteID: rows.getColumnItem(1),
                 layoutID: rows.getColumnItem(2),
                 proto: rows.getColumnItem(3),
                 name: rows.getColumnItem(4)}
      values.push(obj);
   }
   rows.release();

   for (var i=0; i<values.length; i++) {
      var obj = values[i];
      var s = root.getById(obj.siteID);
      if (!s) {
         res.debug("no site for " + obj.siteID);
         continue;
      }
      var l = s.layouts.getById(obj.layoutID);
      var sk = l.skins.get(obj.proto).get(obj.name).skin;
      var reg01 = new RegExp('/static/(images|files)/([a-z0-9]*)/', "gi");
      sk = sk.replace(reg01, '/static/$2/$1/');
      l.skins.get(obj.proto).get(obj.name).skin = sk;
   }
   res.debug("DONE");
   return;
}

function tdyUpgrade06_action() {
   var staticPath = app.properties.staticpath;

   var sql = "select SITE_ID, SITE_ALIAS, IMAGE_FILENAME from AV_IMAGE, AV_LAYOUT, AV_SITE where (IMAGE_FILENAME = 'header' or IMAGE_FILENAME = 'footer' or IMAGE_FILENAME = 'header_small' or IMAGE_FILENAME = 'footer_small') and (AV_LAYOUT.LAYOUT_ID = AV_IMAGE.IMAGE_F_LAYOUT) and (AV_SITE.SITE_ID = AV_LAYOUT.LAYOUT_F_SITE)";
   dbc = getDBConnection("twoday");
   var rows = dbc.executeRetrieval(sql);
   var values = new Array();
   while(rows.next()) {
      var obj = {siteID: rows.getColumnItem(1),
                 siteAlias: rows.getColumnItem(2),
                 imageFilename: rows.getColumnItem(3)}
      values.push(obj);
   }
   rows.release();
   for (var i=0; i<values.length; i++) {
      var obj = values[i];
      var staticDir = new Helma.File(staticPath + obj.siteAlias + "/layouts/" + obj.siteID);
      staticDir.mkdir();

      var srcFile = staticPath + obj.siteAlias + "/images/" + obj.imageFilename + ".jpg";
      var dstFile = staticPath + obj.siteAlias + "/layouts/" + obj.siteID + "/" + obj.imageFilename + ".jpg";
      var f = new Helma.File(srcFile);
      if (f.exists())
         f.move(dstFile);
   }
   res.debug("DONE");
}
