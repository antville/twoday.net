
/**
 * macro rendering loginStatus of user
 * valid params:  -  loginSkin
 *                -  logoutSkin
 */
function loginstatus_macro(param) {
   if (session.user)
      this.members.renderSkin("statusloggedin");
   else if (req.action != "login")
      this.members.renderSkin("statusloggedout");
   return;
}


/**
 * macro renders the number of site (either all or just the public ones)
 */
function sitecounter_macro(param) {
   if (param.count == "all")
      var size = root.size();
   else
      var size = this.publicSites.size();
   if (size == 0)
      res.write(param.no ? param.no : size);
   else if (size == 1)
      res.write(param.one ? param.one : size);
   else
      res.write(size + (param.more ? param.more : ""));
   return;
}


/**
 * render the system title of this antville installation
 */
function title_macro() {
   res.write(this.getTitle());
   return;
}


/**
 * render the system-url of this antville installation
 */
function url_macro(param) {
   res.write(this.getUrl(param.action));
   return;
}


/**
 * render the system manager navigation if user is a system manager
 */
function sysmgrnavigation_macro(param) {
   if (session.user && session.user.sysadmin)
      this.renderSkin("sysmgrnavigation");
   return;
}


/**
 * proxy macro for LayoutMgr.layoutchooser
 */
function layoutchooser_macro(param) {
   if (root.sys_layout)
      param.selected = root.sys_layout.alias;
   root.layouts.layoutchooser_macro(param);
   return;
}


/**
 * renders an Url to Help
 */
function bugReportLink_macro(param) {
   param.to = root.href("bugreport");
   var param2 = this.createLinkParam(param);
   Html.openTag("a", param2);
   res.write(param.text ? param.text : getMessage("admin.bugReport"));
   Html.closeTag("a");
   return;
}


/**
 * Renders the Domain of this twoday installation.
 */
function domain_macro() {
   res.write(root.getDomain());
   return;
}


/**
 * Renders the link to create a new site.
 *
 * @doclevel private
 */
function newSiteLink_macro(param) {
   // Don’t run the macro for non-admins
   if (!session.user || !session.user.sysadmin) return;
   var text = param.text ? param.text : getMessage("Root.sidebar.createSite");
   linkParam = this.createLinkParam(param);
   linkParam.href = this.members.href("newsite");
   Html.openTag("a", linkParam);
   res.write(text);
   Html.closeTag("a");
   return;
}


function hookSysMgrNavigation_macro(param) {
   this.applyModuleMethods("renderSysMgrNavigation", param);
   return;
}

/**
 * render generic preference editor or value
 *
 * @deprecated Site.preference_macro
 */
function preferences_macro(param) {
   if (!session.user || !session.user.sysadmin) return;
   if (param.as == "editor") {
      if (param.type == "checkbox") {
         var inputParam = this.preferences.createCheckBoxParam(param.name, param);
         Html.checkBox(inputParam);
      } else {
         var inputParam = this.preferences.createInputParam(param.name, param);
         delete inputParam.part;
         if (param.cols || param.rows) {
            Html.textArea(inputParam);
         } else {
            Html.input(inputParam);
         }
      }
   } else {
      res.write(this.preferences.getProperty(param.name));
   }
   return;
}
