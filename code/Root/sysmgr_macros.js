
/**
 * render the system-title of this antville-installation
 */
function sys_title_macro(param) {
   if (param.as == "editor") {
      Html.input(root.preferences.createInputParam("sys_title", param));
   } else {
      res.write(root.getTitle());
   }
   return;
}


/**
 * render the system-title of this antville-installation
 */
function sys_tagline_macro(param) {
   if (param.as == "editor") {
      Html.input(root.preferences.createInputParam("sys_tagline", param));
   } else {
      res.write(root.getTagline());
   }
   return;
}


/**
 * macro rendering address used for sending mails
 */
function sys_email_macro(param) {
   // this macro is allowed just for sysadmins
   if (!session.user.sysadmin)
      return;
   if (param.as == "editor") {
      param["type"] = "text";
      var iParam = root.preferences.createInputParam("sys_email", param);
      // use the users email if sys_email is empty
      if (!iParam.value)
         iParam.value = session.user.email;
      Html.input(iParam);
   } else {
      res.write(root.preferences.getProperty("sys_email"));
   }
   return;
}


/**
 * macro rendering diskquota
 */
function sys_diskQuota_macro(param) {
   // this macro is allowed just for sysadmins
   if (!session.user.sysadmin)
      return;
   if (param.as == "editor") {
      param["type"] = "text";
      var iParam = root.preferences.createInputParam("sys_diskQuota", param);
      Html.input(iParam);
   } else {
      res.write(root.preferences.getProperty("sys_diskquota"));
   }
   return;
}


/**
 * macro rendering a dropdown for limiting the creation of new Sites
 */
function sys_limitNewSites_macro(param) {
   // this macro is allowed just for sysadmins
   if (!session.user.sysadmin)
      return;
   if (param.as == "editor") {
      if (root.preferences.getProperty("sys_limitNewSites") == null) root.preferences.setProperty("sys_limitNewSites", 2);
      var options = [getMessage("SysMgr.registeredUsers"), getMessage("SysMgr.trustedUsers"), "---------"];
      Html.dropDown({name: "preferences_sys_limitNewSites"}, options, root.preferences.getProperty("sys_limitNewSites"));
   } else {
      res.write(root.preferences.getProperty("sys_limitNewSites"));
   }
   return;
}


/**
 * macro renders a dropdown containing the minimal registration time
 */
function sys_minMemberAge_macro(param) {
   // this macro is allowed just for sysadmins
   if (!session.user.sysadmin)
      return;
   if (param.as == "editor") {
      var options = new Array();
      for (var i=1;i<92;i++) {
         if (i < 7 || (i % 7) == 0)
            options[options.length] = i.toString();
      }
      Html.dropDown({name: "preferences_sys_minMemberAge"}, options, root.preferences.getProperty("sys_minMemberAge"), "----");
   } else {
      res.write(root.preferences.getProperty("sys_minMemberAge"));
   }
   return;
}


/**
 * macro renders an input type text for editing the system-timestamp
 * that allows users who have registered before it to create a site
 */
function sys_minMemberSince_macro(param) {
   // this macro is allowed just for sysadmins
   if (!session.user.sysadmin)
      return;
   if (param.as == "editor") {
      if (root.preferences.getProperty("sys_minMemberSince"))
         param.value = formatTimestamp(root.preferences.getProperty("sys_minMemberSince"), "yyyy-MM-dd HH:mm");
      param.name = "preferences_sys_minMemberSince";
      Html.input(param);
   } else {
      res.write(root.preferences.getProperty("sys_minMemberSince"));
   }
   return;
}


/**
 * macro renders a dropdown containing the number of days a user has to wait
 * after having created a site before being allowed to create a new one
 */
function sys_waitAfterNewSite_macro(param) {
   // this macro is allowed just for sysadmins
   if (!session.user.sysadmin)
      return;
   if (param.as == "editor") {
      var options = new Array();
      for (var i=1;i<92;i++) {
         if (i < 7 || !(i%7))
            options[i] = i;
      }
      Html.dropDown({name: "preferences_sys_waitAfterNewSite"}, options, root.preferences.getProperty("sys_waitAfterNewSite"), "----");
   } else {
      res.write(root.preferences.getProperty("sys_waitAfterNewSite"));
   }
   return;
}


/**
 * macro renders a dropdown containing the maximum number of sites a user can
 * create
 */
function sys_limitSitesPerUser_macro(param) {
   // this macro is allowd just for sysadmins
   if (!session.user.sysadmin)
      return;
   if (param.as == "editor") {
      var options = [];
      var nr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 15, 20, 50, 100];
      for (var i in nr) {
         options.push([nr[i], nr[i]]);
      }
      Html.dropDown({name: "preferences_sys_limitSitesPerUser"}, options, root.preferences.getProperty("sys_limitSitesPerUser"), "----");
   } else {
      res.write(root.preferences.getProperty("sys_limitSitesPerUser"));
   }
   return;
}


/**
 * macro rendering autocleanup-flag
 */
function sys_enableAutoCleanup_macro(param) {
   // this macro is allowed just for sysadmins
   if (!session.user.sysadmin)
      return;
   if (param.as == "editor") {
      var inputParam = root.preferences.createCheckBoxParam("sys_enableAutoCleanup", param);
      if (req.data.save && !req.data.preferences_sys_enableAutoCleanup)
         delete inputParam.checked;
      Html.checkBox(inputParam);
   } else {
      res.write(root.preferences.getProperty("sys_enableAutoCleanup") ? getMessage("generic.yes") : getMessage("generic.no"));
   }
   return;
}


/**
 * macro rendering hour when automatic cleanup starts
 */
function sys_startAtHour_macro(param) {
   // this macro is allowed just for sysadmins
   if (!session.user.sysadmin)
      return;
   if (param.as == "editor") {
      var options = new Array();
      for (var i=0;i<24;i++)
         options[i] = (i < 10 ? "0" + i : i.toString());
      Html.dropDown({name: "preferences_sys_startAtHour"}, options, root.preferences.getProperty("sys_startAtHour"));
   } else {
      res.write(root.preferences.getProperty("sys_startAtHour"));
   }
   return;
}


/**
 * macro rendering sys_blockInactiveSites-flag
 */
function sys_blockInactiveSites_macro(param) {
   // this macro is allowed just for sysadmins
   if (!session.user.sysadmin)
      return;
   if (param.as == "editor") {
      var inputParam = root.preferences.createCheckBoxParam("sys_blockInactiveSites", param);
      if (req.data.save && !req.data.preferences_sys_blockInactiveSites)
         delete inputParam.checked;
      Html.checkBox(inputParam);
   } else {
      res.write(root.preferences.getProperty("sys_blockInactiveSites") ? getMessage("generic.yes") : getMessage("generic.no"));
   }
   return;
}


/**
 * macro rendering Number of days before blocking inactive Sites
 */
function sys_blockInactiveSitesAfterDays_macro(param) {
   // this macro is allowed just for sysadmins
   if (!session.user.sysadmin)
      return;
   if (param.as == "editor") {
      Html.input(root.preferences.createInputParam("sys_blockInactiveSitesAfterDays", param));
   } else {
      res.write(root.preferences.getProperty("sys_blockInactiveSitesAfterDays"));
   }
   return;
}


/**
 * macro rendering sys_blockInactiveSites-flag
 */
function sys_removeBlockedSites_macro(param) {
   // this macro is allowed just for sysadmins
   if (!session.user.sysadmin)
      return;
   if (param.as == "editor") {
      var inputParam = root.preferences.createCheckBoxParam("sys_removeBlockedSites", param);
      if (req.data.save && !req.data.preferences_sys_removeBlockedSites)
         delete inputParam.checked;
      Html.checkBox(inputParam);
   } else {
      res.write(root.preferences.getProperty("sys_removeBlockedSites") ? getMessage("generic.yes") : getMessage("generic.no"));
   }
   return;
}


/**
 * macro rendering Number of days before blocking inactive Sites
 */
function sys_removeBlockedSitesAfterDays_macro(param) {
   // this macro is allowed just for sysadmins
   if (!session.user.sysadmin)
      return;
   if (param.as == "editor") {
      Html.input(root.preferences.createInputParam("sys_removeBlockedSitesAfterDays", param));
   } else {
      res.write(root.preferences.getProperty("sys_removeBlockedSitesAfterDays"));
   }
   return;
}


/**
 * macro rendering sys_removeBlockedUsers-flag
 */
function sys_removeBlockedUsers_macro(param) {
   // this macro is allowed just for sysadmins
   if (!session.user.sysadmin)
      return;
   if (param.as == "editor") {
      var inputParam = root.preferences.createCheckBoxParam("sys_removeBlockedUsers", param);
      if (req.data.save && !req.data.preferences_sys_removeBlockedUsers)
         delete inputParam.checked;
      Html.checkBox(inputParam);
   } else {
      res.write(root.preferences.getProperty("sys_removeBlockedUsers") ? getMessage("generic.yes") : getMessage("generic.no"));
   }
   return;
}


/**
 * macro rendering Number of days before blocking inactive Sites
 */
function sys_removeBlockedUsersAfterDays_macro(param) {
   // this macro is allowed just for sysadmins
   if (!session.user.sysadmin)
      return;
   if (param.as == "editor") {
      Html.input(root.preferences.createInputParam("sys_removeBlockedUsersAfterDays", param));
   } else {
      res.write(root.preferences.getProperty("sys_removeBlockedUsersAfterDays"));
   }
   return;
}


/**
 * macro rendering sys_removeUnconfirmedUsers-flag
 */
function sys_removeUnconfirmedUsers_macro(param) {
   // this macro is allowed just for sysadmins
   if (!session.user.sysadmin)
      return;
   if (param.as == "editor") {
      var inputParam = root.preferences.createCheckBoxParam("sys_removeUnconfirmedUsers", param);
      if (req.data.save && !req.data.preferences_sys_removeUnconfirmedUsers)
         delete inputParam.checked;
      Html.checkBox(inputParam);
   } else {
      res.write(root.preferences.getProperty("sys_removeUnconfirmedUsers") ? getMessage("generic.yes") : getMessage("generic.no"));
   }
   return;
}


/**
 * macro rendering Number of days before blocking inactive Sites
 */
function sys_removeUnconfirmedUsersAfterDays_macro(param) {
   // this macro is allowed just for sysadmins
   if (!session.user.sysadmin)
      return;
   if (param.as == "editor") {
      Html.input(root.preferences.createInputParam("sys_removeUnconfirmedUsersAfterDays", param));
   } else {
      res.write(root.preferences.getProperty("sys_removeUnconfirmedUsersAfterDays"));
   }
   return;
}


/**
 * macro rendering a dropdown containing all available locales
 */
function localechooser_macro(param) {
   if (!session.user.sysadmin)
      return;
   renderLocaleChooser(root.getLocale());
   return;
}


/**
 * macro rendering a dropdown containing all available locales
 */
function timezonechooser_macro(param) {
   if (!session.user.sysadmin)
      return;
   renderTimeZoneChooser(root.getTimeZone());
   return;
}


/**
 * macro renders a chooser for the longdateformat
 */
function longdateformat_macro(param) {
   if (!session.user.sysadmin)
      return;
   renderDateformatChooser("longdateformat", root.getLocale(), root.preferences.getProperty("longdateformat"));
   return;
}


/**
 * macro renders a chooser for the shortdateformat
 */
function shortdateformat_macro(param) {
   if (!session.user.sysadmin)
      return;
   renderDateformatChooser("shortdateformat", root.getLocale(), root.preferences.getProperty("shortdateformat"));
   return;
}


/**
 * macro renders the alias of the frontpage site defined
 */
function sys_frontSite_macro(param) {
   if (!session.user.sysadmin)
      return;
   if (param.as == "editor") {
      var inputParam = new Object();
      inputParam.name = "sys_frontSite";
      inputParam.value = root.sys_frontSite ? root.sys_frontSite.alias : null;
      Html.input(inputParam);
   } else {
      res.write(root.sys_frontSite ? root.sys_frontSite.alias : "");
   }
   return;
}


/**
 * render sys_commentfilter
 */
function sys_ipfilter_macro(param) {
   if (param.as == "editor") {
      param.name = "preferences_sys_ipfilter";
      param.value = root.preferences.getProperty("sys_ipfilter");
      if (param.cols || param.rows)
         Html.textArea(param);
      else
         Html.input(param);
   } else {
      res.write(root.preferences.getProperty("sys_ipfilter"));
   }
   return;
}
