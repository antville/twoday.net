
/**
 * evaluating new Site
 */
function evalNewSite(param, creator) {
   knallgrau.Event.notifyObservers(this, "beforeEvalSite", [{"param": param, "creator": creator}]);
   this.applyModuleMethods("onBeforeEvalNewSite", [param]);
   // check alias
   if (!param.alias)
      throw new Exception("siteAliasMissing");
   else if (this.get(param.alias))
      throw new Exception("siteAliasExisting");
   else if (!param.alias.isClean())
      throw new Exception("aliasNoSpecialChars");
   else if (param.alias.length > 30)
      throw new Exception("aliasTooLong");
   else if (this[param.alias] || this[param.alias + "_action"] || (tdyReservedAlias(param.alias) && !creator.sysadmin))
      throw new Exception("aliasReserved");

   // check if title is missing
   if (!param.title)
      param.title = param.alias;
   // create new Site
   var newSite = new Site(param.title, param.alias, creator);
   // set type
   newSite.type = /*param.type || */"free";
   // create an initial layout object that is a child layout
   // of the currently active root layout
   var initLayout = new Layout(newSite, newSite.title, creator);
   initLayout.alias = newSite.alias;
   var parentLayout = (param.layout) ? root.layouts.get(param.layout) : res.handlers.layout;
   if (!parentLayout)
      throw new Exception("layoutParentNotFound");
   initLayout.setParentLayout(parentLayout);
   initLayout.description = getMessage("Layout.initialDescription", {siteTitle: newSite.title, parentLayoutTitle: parentLayout.title});
   if (!this.add(newSite))
      throw new Exception("siteCreate");
   newSite.layouts.add(initLayout);
   newSite.layouts.setDefaultLayout(initLayout.alias);
   // add the creator to the admins of the new Site
   newSite.members.add(new Membership(creator, ADMIN));
   this.applyModuleMethods("evalNewSite", [creator, newSite, param]);
   // send confirmation mail to creator
   newSite.sendNewSiteInfo(creator.email);
   knallgrau.Event.notifyObservers(this, "onAfterEvalNewSite", [creator, newSite, param]);
   root.manage.syslogs.add(new SysLog("site", newSite.alias, "added site", creator));
   var result = new Message("siteCreate", null, newSite);
   return result;
}

/**
 * Check authorization for functions deleteSite, blockSite, unblockSite
 * Conduct redirect, if authorization is not sufficient
 * @param {Object} site
 * @returns void
 */
function checkAccess(site) {
   // make sure, user is logged in; do immediate redirect if not
   checkIfLoggedIn(this.href(req.action));
   // memberlevel is being set in onRequest() for every request
   if (res.meta.memberlevel < ADMIN) {
      // issue message and redirect if auth level is less than ADMIN (see /Global/constants.js)
      res.message = getMessage('deny.general');
      res.redirect(site ? site.href() : root.href());
   }
}


/**
 * function removes a site completely
 * including stories, comments, memberships
 * @param Object site to remove
 */
function deleteSite(site) {
   // check authorization for delete
   this.checkAccess(site);
   // now run delete process
   site.deleteAll();
   site.remove();
   this.manage.syslogs.add(new SysLog('site', site.alias, 'removed site', session.user));
   return new Message('siteDelete', {siteAlias: site.alias});
}


/**
 * function blocks a site
 * @param Object site to block
 */
function blockSite(site) {
   // check authorization for block action
   this.checkAccess(site);
   // now run block process
   site.blocked = 1;
   site.blockuser = session.user;
   site.blocktime = new Date();
   this.manage.syslogs.add(new SysLog('site', site.alias, 'blocked site', session.user));
   return new Message('siteBlock', {siteAlias: site.alias});
}


/**
 * function unblocks a site
 * @param Object site to unblock
 */
function unblockSite(site) {
   // check authorization for unblock action
   this.checkAccess(site);
   // now run unblock process
   site.blocked = 0;
   site.blockuser = null;
   site.blocktime = null;
   site.lastdelwarn = null;
   this.manage.syslogs.add(new SysLog('site', site.alias, 'unblocked site', session.user));
   return new Message('siteUnblock', {siteAlias: site.alias});
}


/**
 * Returns a user by it's unique key.
 * This may be the username, or e-mail adress
 * @param key String, username or e-mail adress
 * @param type String, optional; represents the authType of that user
 * @return User
 */
function getUserByKey(key, type) {
   var usr;
   if (type && type != "local") {
      if (root.nonLocalUsers.get(type)) {
         usr = root.nonLocalUsers.get(type).get(key);
         if (!usr)
            usr = root.nonLocalUsersByEmail.get(type).get(key);
      }
   } else {
      var usr = root.users.get(key);
      if (!usr) {
         usr = root.usersByEmail.get(key);
      }
   }
   return usr;
}


/**
 * function removes a user
 * including stories, comments, memberships (and abandoned sites)
 * @param User to remove
 */
function deleteUser(user) {
   try { var cusr = session.user; }
   catch (err) { var cusr = null; }
   var userName = user.name;
   var userEmail = user.email;
   user.deleteAll();
   user.remove();
   this.manage.syslogs.add(new SysLog("user", userName, "removed user", cusr));
   return new Message("userDelete", {userName: userName, userEmail: userEmail});
}


/**
 * function blocks a user
 * @param User to block
 */
function blockUser(user) {
   try { var cusr = session.user; }
   catch (err) { var cusr = null; }
   user.blocked = 1;
   user.blockuser = cusr;
   user.blocktime = new Date();
   this.manage.syslogs.add(new SysLog("user", user.name, "blocked user", cusr));
   return new Message("userBlock", {userName: user.name, userEmail: user.email});
}


/**
 * function unblocks a user
 * @param Object user to unblock
 */
function unblockUser(user) {
   try { var cusr = session.user; }
   catch (err) { var cusr = null; }
   user.blocked = 0;
   user.blockuser = null;
   user.blocktime = null;
   this.manage.syslogs.add(new SysLog("user", user.name, "unblocked user", cusr));
   return new Message("userUnblock", {userName: user.name, userEmail: user.email});
}


/**
 *  Search one or more (public) sites. Returns an array containing site-aliases and
 *  story ids of matching items.
 *
 * @param query The unprocessed query string
 * @param sid ID of site in which to search, null searches all. This can also be a comma-separated list of IDs.
 * @return The result array
 */
function searchSites(query, sid) {
   // result array
   var result = new Array();

   // break up search string
   var unquote = new RegExp("\\\\", "g");
   query = query.replace(unquote, "\\\\");
   unquote = new RegExp("\'", "g");
   query = query.replace(unquote, "\'\'");
   var qarr = query.split(" ");

   // construct query
   var where = "select AV_TEXT.TEXT_ID, AV_SITE.SITE_ALIAS from AV_TEXT, AV_SITE where "+
               "AV_TEXT.TEXT_F_SITE = AV_SITE.SITE_ID and "+
               "AV_TEXT.TEXT_ISONLINE > 0 and ";
   for (var i in qarr) {
      where += "(AV_TEXT.TEXT_TEXT like '%" + qarr[i].toLowerCase() + "%' or AV_TEXT.TEXT_TITLE like '%" + qarr[i].toLowerCase() + "%') "
      if (i < qarr.length-1)
         where += "and ";
   }
   // search only in the specified sites
   if (sid)
      where += "and AV_SITE.SITE_ID IN (" + sid + ") ";
   else
      where += "and AV_SITE.SITE_ISONLINE > 0 ";
   where += "order by AV_TEXT.TEXT_CREATETIME desc";

   var dbcon = getDBConnection ("twoday");
   var dbres = dbcon.executeRetrieval(where);
   if (dbres) {
      while (dbres.next()) {
         var item = new Object();
         item.sid = dbres.getColumnItem(1).toString();
         item.sitealias = dbres.getColumnItem(2);
         result[result.length] = item;
      }
   }

   dbres.release();
   return result;
}


/**
 * function checks if language and country were specified
 * for root. if so, it returns the specified Locale-object
 * otherwise it returns the default locale of the JVM
 */
function getLocale() {
   var locale = this.cache.locale;
   if (locale) return locale;
   if (root.preferences.getProperty("sys_language")) {
      locale = new java.util.Locale(root.preferences.getProperty("sys_language"), root.preferences.getProperty("sys_country") ? root.preferences.getProperty("sys_country") : "");
   } else if (app.properties.defaultLocale) {
      locale = convertStringToLocale(app.properties.defaultLocale);
   } else {
      locale = java.util.Locale.getDefault();
   }
   this.cache.locale = locale;
   return locale;
}


/**
 * Sets preferred locale for Root
 *
 * @param locale String, ISO-639 language-code with optional ISO-3166 country-code; e.g. de_AT
 */
function setLocale(locale) {
   var prefs = root.preferences.getAll();
   // store selected locale
   if (locale) {
      var loc = locale.split("_");
      prefs.sys_language = loc[0];
      prefs.sys_country = loc.length == 2 ? loc[1] : null;
      this.cache.locale = null;
   }
   this.preferences.setAll(prefs);
   return;
}


/**
 * function checks if the system title of this installation
 * was defined in setup
 */
function getTitle() {
   if (!root.preferences.getProperty("sys_title"))
      return "twoday";
   return root.preferences.getProperty("sys_title");
}


/**
 * function checks if the system tagline of this installation
 * was defined in setup
 */
function getTagline() {
   if (!root.preferences.getProperty("sys_tagline"))
      return "";
   return root.preferences.getProperty("sys_tagline");
}


/**
 * function checks if the system url of this antville-installation
 * was defined in setup and returns it.
 * if not set, root.href() is returned.
 */
function getUrl(action) {
   return root.href(action);
}


/**
 *  href URL postprocessor. If a virtual host mapping is defined
 *  for this site's alias, use it. Otherwise, use normal site URL.
 */
function processHref(href) {
   return (req && req.servletRequest && req.servletRequest.isSecure() ? "https://" : "http://") + getProperty("defaultHost") + href;
}


/**
 * function returns the (already cached) TimeZone-Object
 */
function getTimeZone() {
   if (this.cache.timezone)
       return this.cache.timezone;
   if (root.preferences.getProperty("sys_timezone"))
       this.cache.timezone = java.util.TimeZone.getTimeZone(root.preferences.getProperty("sys_timezone"));
   else
       this.cache.timezone = java.util.TimeZone.getDefault();
   return this.cache.timezone;
}


/**
 * return the root layout
 * if no layout is activated, check if the default
 * layout is existing, otherwise return a transient
 * layout object
 * @return Object layout object
 */
function getLayout() {
   if (!root.sys_layout) {
      // no layout defined, so try to get default
      // layout. if that doesn't exist either
      // return a newly created layout object
      var defLayout = root.layouts.get("default");
      if (!defLayout)
         return new Layout();
      return defLayout;
   }
   return root.sys_layout;
}


/**
 * Send a bug report
 */
function evalBugReport(param, user) {

   var bugReport = "";
   bugReport += "URL: \n" + param.bugreport_referrer + "\n\n";
   bugReport += "Short Description: \n" + param.bugreport_shortdescription + "\n\n";
   bugReport += "What have you done: \n" + param.bugreport_what + "\n\n";
   bugReport += "Description of the problem: \n" + param.bugreport_description + "\n\n";
   bugReport += "Browser: \n" + param.bugreport_browser + "\n\n";
   bugReport += "\n -------- \n"
   bugReport += "Reported by: " + param.bugreport_name + " (" + param.bugreport_email + ")\n";
   var now = new Date();
   bugReport += "Date: " + now.format("dd.MM.yyyy HH:mm") + "\n\n";
   bugReport += "Remote Host: " + param.http_remotehost + "\n\n";

   app.log(bugReport);

   sendMail((session.user ? session.user : param.bugreport_email), root.preferences.getProperty("sys_email"), "Twoday Bugreport: " + param.bugreport_shortdescription, bugReport);

   return "Bug Report was sended successfully!";
}


/**
 * checks if an User or IP-address is on the blocklist
 *
 * @param obj User Object or a string with an IP-Address
 * @return boolean
 */
function isIpAddressBlocked(obj) {
   var ipAddressList = root.preferences.getProperty("sys_ipfilter")
      ? root.preferences.getProperty("sys_ipfilter").split("\n")
      : [];
   var ipAddress = (obj instanceof User)
      ? obj.ipAddress
      : obj.match(/\d+\.\d+\.\d+\.\d+/)
         ? obj
         : "invalid";
   if (ipAddress == "invalid")
      return true;
   for (var i in ipAddressList) {
      if (ipAddress.indexOf(ipAddressList[i]) == 0) {
         return true;
      }
   }
   return false;
}


/**
 * Returns the Domain of this twoday installation.
 */
function getDomain() {
   var url = root.href();
   if (url.startsWith("http://")) url = url.substring(7);
   if (url.indexOf("/")) url = url.substring(0, url.indexOf("/"));
   return url;
}
