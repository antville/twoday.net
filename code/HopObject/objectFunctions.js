
/**
 * Return a name for the object to be used in the
 * global linkedpath macro
 * this function is overwritten by day-objects!
 * @see day.getNavigationName()
 * @see story.getNavigationName()
 * @see topic.getNavigationName()
 */
function getNavigationName() {
   var msg = getMessage(this._prototype);
   if (msg)
      return msg;
   else
      return this.__name__;
}


/**
 * creates parameter object that will be passed to
 * function that renders the input element
 */
function createInputParam(propName, param) {
   param.name = propName;
   // submitted values override property value
   // but only if there were not multiple form elements
   // with the same name submitted
   if (!req.data[propName + "_array"] && req.data[propName] != null)
      param.value = req.data[propName];
   else
      param.value = this[propName];
   delete param.as;
   param.value = param.value == null ? null : h_filter(param.value);
   return param;
}


/**
 * create a parameter object for checkboxes
 */
function createCheckBoxParam(propName, param) {
   param.name = propName;
   param.id = param.id ? param.id : param.name;
   param.value = 1;
   if (req.data[propName] == 1 || this[propName] == 1)
      param.checked = "checked";
   delete param.as;
   return param;
}


/**
 * derives parameter object from an object that will
 * be passed to function that renders the link element
 */
function createLinkParam(param) {
   // clone the param object since known non-html
   // attributes are going to be deleted
   var linkParam = Object.clone(param);
   var url = param.to ? param.to : param.linkto;
   if (!url || url == "main") {
      if (this._prototype != "Comment")
         linkParam.href = this.href();
      else
         linkParam.href = this.story.href() + "#" + this._id;
   } else if (url.contains("://") || url.startsWith("javascript"))
      linkParam.href = url;
   else {
      // check if link points to a subcollection
      if (url.contains("/"))
         linkParam.href = this.href() + url;
      else
         linkParam.href = this.href(url);
   }
   if (param.urlparam)
      linkParam.href += "?" + param.urlparam;
   if (param.anchor)
      linkParam.href += "#" + param.anchor;
   delete linkParam.to;
   delete linkParam.linkto;
   delete linkParam.urlparam;
   delete linkParam.anchor;
   delete linkParam.text;
   delete linkParam.suffix;
   delete linkParam.prefix;
   delete linkParam.encoding;
   return linkParam;
}


/**
 * method for rendering any module navigation
 * by calling the module method renderSiteNavigation
 * just invokes modules that are activated
 * @param moduleName  String name of app.modules.object
 * @param funcName    String name of the function to apply
 * @param param       Object will be passed to the applied function
 * @param keepParam Boolean, whether to try to merge the funtion results
 *                  into the param passed to the function call
 * @param invokeInactive  Boolean  true will also call not activated modules
 * @return Value
 */
function applyModuleMethod(moduleName, funcName, param, keepParam, invokeInactive) {
   param = typeof param == "array" ? param : [param];
   if (app.modules[moduleName] && root.modules.get(moduleName) && (invokeInactive || root.modules.get(moduleName).isActivated) && app.modules[moduleName][funcName]) {
      if (keepParam) {
         return joinValues(param, app.modules[moduleName][funcName].apply(this, param));
      } else {
         return app.modules[moduleName][funcName].apply(this, param);
      }
   }
};


/**
 * method for calling a function for all modules
 * just invokes modules that are activated
 * @param funcName  String name of the function to apply
 * @param param     Object will be passed to the applied function
 * @param keepParam Boolean, whether to try to merge the function results
 *                  into the param passed to the function call
 * @param invokeInactive  Boolean  true will also call not activated modules
 * @return Value
 */
function applyModuleMethods(funcName, param, keepParam, invokeInactive) {
   param = typeof param == "array" ? param : [param];
   var result;
   for (var i in app.modules) {
      if (!app.modules[i][funcName]) continue;
      if (app.modules[i][funcName] != null) {
         if (keepParam) {
            param = joinValues(param, app.modules[i][funcName].apply(this, param));
         } else {
            result = joinValues(result, app.modules[i][funcName].apply(this, param));
         }
      }
   }
   return result;
};


/**
 * method for calling a function for all modules
 * invokes all modules, no matter if they are activated
 * @param funcName  String name of the function to apply
 * @param param     Object will be passed to the applied functi
 * @param keepParam Boolean, whether to try to merge the funtion results
 *                  into the param passed to the function call
 * @return Value
 * @see applyModuleMethods
 */
function applyAllModuleMethods(funcName, param, keepParam) {
   return this.applyModuleMethods(funcName, param, keepParam, true);
};


/**
 * method for rendering any module navigation
 * by calling the module method renderSiteNavigation
 * just invokes modules that are activated
 * @param moduleName  String name of app.modules.object
 * @param funcName    String name of the function to apply
 * @param param       Object will be passed to the applied function
 */
function applyAdminModuleMethod(moduleName, funcName, param) {
   param = typeof param == "array" ? param : [param];
   var module = app.modules[moduleName];
   if (module && module[funcName]) {
      module[funcName].apply(this, param);
   }
   return;
};


/**
 * method for calling a function for all modules
 * just invokes modules that are activated
 * @param funcName  String name of the function to apply
 * @param param     Object will be passed to the applied function
 */
function applyAdminModuleMethods(funcName, param) {
   param = typeof param == "array" ? param : [param];
   for (var i in app.modules) {
      if (app.modules[i][funcName]) {
         app.modules[i][funcName].apply(this, param);
      }
   }
   return;
};


/**
 * Helper function to fetch the Site of an Object
 */
function getSite() {
   var obj = this;
   var cnt = 0;
   while (!(obj instanceof Site) && obj != null) {
      if (obj.site) return obj.site;
      obj = obj._parent;
      if (cnt++ > 50) return; // catch infinite loop
   }
   return obj;
}
