
/**
 * user-friendly wrapper for href_macro
 */
function url_macro(param) {
   this.href_macro(param);
   return;
}


/**
 * macro creates an html link
 * @param href
 * @param as     if set to popup javascript for popup will be added
 * @param popupMenubar     no default
 * @param popupToolbar     no default
 * @param popupStatus      no default
 * @param popupScrollbars  default=yes
 * @param popupResizable   default=yes
 * @param popupWidth       default=570
 * @param popupHeight      default=570
 * @param target           will be set to "_blank" if as="popup"
 *
 */
function link_macro(param) {
   var param2 = this.createLinkParam(param);
   if (!param2.onclick && param.as == "popup") {
      param.onclick =
         "var popupWindow = window.open('" + param2.href + "', 'popup', '" +
         (param.popupMenubar ? ", menubar=" + param.popupMenubar : "") +
         (param.popupToolbar ? ", toolbar=" + param.popupToolbar : "") +
         (param.popupStatus ? ", status=" + param.popupStatus : "") +
         ", scrollbars=" + (param.popupScrollbars ? param.popupScrollbars : "yes") +
         ", resizable=" + (param.popupResizable ? param.popupResizable : "yes") +
         ", width=" + (param.popupWidth ? param.popupWidth : 570) +
         ", height=" + (param.popupHeight ? param.popupHeight : 570) +
         "'); popupWindow.focus(); return false;";
      param2.target = (param.target == null) ? "_blank" : param2.target;
      delete param2.popupMenubar;
      delete param2.popupToolbar;
      delete param2.popupStatus;
      delete param2.popupScrollbars;
      delete param2.popupResizable;
      delete param2.popupWidth;
      delete param2.popupHeight;
   }
   delete(param.as);
   delete(param2.as);
   Html.openTag("a", param2);
   var text = (getMessage(param.text) || param.text) || param.to;
   res.write(text);
   Html.closeTag("a");
   return;
}


/**
 * macro renders the time the object was created
 */
function createtime_macro(param) {
   if (this.createtime)
      res.write(formatTimestamp(this.createtime, param.format));
   return;
}


/**
 * macro rendering modifytime
 */
function modifytime_macro(param) {
   if (this.modifytime)
      res.write(formatTimestamp(this.modifytime, param.format));
   return;
}


/**
 * macro renders the name of the creator of an object
 * either as link or as plain text
 */
function creator_macro(param) {
   if (!this.creator)
      return;
   if (param.as == "link" && this.creator.url)
      Html.link({href: this.creator.url}, this.creator.name);
   else if (param.as == "email")
      res.write(this.creator.publishemail ? this.creator.email : "");
   else if (param.as == "url")
      res.write(this.creator.url);
   else
      res.write(this.creator.name);
   return;
}


/**
 * macro renders the name of the modifier
 */
function modifier_macro(param) {
   if (!this.modifier)
      return;
   if (param.as == "link" && this.modifier.url)
      Html.link({href: this.modifier.url}, this.modifier.name);
   else if (param.as == "url")
      res.write(this.modifier.url);
   else
      res.write(this.modifier.name);
   return;
}


/**
 * macro renders localized text strings
 * All arguments will passed directly to the rendered Message,
 * and are available via sthg like <% param.anyArgument %>
 *
 * @param key
 */
function message_macro(param) {
   return getMessage(param.key, param, this);
}


/**
 * macro calls
 *
 */
function renderModuleSetup_macro(param) {
   if (!param.name) return;
   if (!param.modules) param.modules = "all";

   var methodName = param.name;
   delete param.name;
   var mods = param.modules.split(",");

   if (mods.length == 1 && mods[0] == "all") {
      this.applyModuleMethods(methodName, param);
   } else {
      for (var i in mods) {
         this.applyModuleMethod(mods[i], methodName, param);
      }
   }
   return;
}


/**
 * Returns a href for an object.
 *
 * @param action
 * @param collection
 */
function href_macro(param) {
   var obj = this;
   if (param.collection) {
      var objpath = param.collection.trim().split("/");
      for (var i=0; i<objpath.length; i++) {
         if (objpath[i] == "") obj = root;
         else obj = obj.get(objpath[i]);
         if (obj == null) return;
      }
      if (res.handlers.site && !res.handlers.site.trusted && res.handlers.site != obj.getSite())
         return;
   }
   if (param.action)
      res.write(obj.href(param.action));
   else
      res.write(obj.href());
   return;
}


/**
 * macro rendering a skin
 *
 * @param name String
 * @param for String, user group for which this skin should be rendered
 * @param collection
 */
function skin_macro(param) {
   var obj = this;
   if (param.collection) {
      var objpath = param.collection.trim().split("/");
      for (var i=0; i<objpath.length; i++) {
         if (objpath[i] == "") obj = root;
         else obj = obj.get(objpath[i]);
         if (obj == null) return;
      }
      if (res.handlers.site && !res.handlers.site.trusted && res.handlers.site != obj.getSite())
         return;
   }
   if (!param.name)
      return;
   if (param["for"]=="anonymous" && session.user!=null)
      return;
   if (param["for"]=="users" && session.user==null)
      return;
   if (param["for"]=="contributors" && res.handlers.site && !res.handlers.site.usercontrib && res.meta.memberlevel < CONTRIBUTOR)
      return;
   if (param["for"]=="admins" && res.handlers.site && res.meta.memberlevel < ADMIN)
      return;
   obj.renderSkin(param.name, param);
   return;
}


/**
 * Will print the last update info in the form:
 * created by Matix on 22.06.2003 14:15
 * @param format   legal DateFormat Pattern, optional
 */
function createInfo_macro(param) {
   var format = param.format ? param.format : "dd.MM.yyyy HH:mm";
   res.push();
      this.creator_macro({as: "link"});
   var creator = res.pop();
   res.push();
      this.createtime_macro({format: format});
   var createTime = res.pop();
   res.write(getMessage("Mgr.createInfo", {"creator": creator, "createTime": createTime}));
}


/**
 * Wrapper macro for HopObject.updateInfo
 * @param format   legal DateFormat Pattern, optional
 * @see HopObject.updateInfo_macro
 */
function modifyInfo_macro(param) {
   this.updateInfo_macro(param);
   return;
}


/**
 * Will print the last update info in the form:
 * last update: 22.06.2003 14:15
 * @param format   legal DateFormat Pattern, optional
 */
function updateInfo_macro(param) {
   var format = param.format ? param.format : "dd.MM.yyyy HH:mm";
   if (this.lastupdate == null && this.modifytime == null) {
      res.write(getMessage("Mgr.updateInfo.noUpdatesSoFar"));
      return;
   }

   res.write(getMessage("Mgr.updateInfo.lastUpdate") + ":&nbsp;");
   if (this.lastupdate) {
      res.write(formatTimestamp(this.lastupdate, format));
   } else if (this.modifytime) {
      res.write(formatTimestamp(this.modifytime, format));
   }
   return;
}


/**
 * returns the size of a certain collection
 * @param collection String, name of the collection of this object to be used; if not specified, then the direct children are taken;
 */
function count_macro (param) {
   var obj = this;
   if (param.collection) {
      var objpath = param.collection.trim().split("/");
      for (var i=0; i<objpath.length; i++) {
         if (objpath[i] == "") obj = root;
         else obj = obj.get(objpath[i]);
         if (obj == null) break;
      }
      if (obj == null) return "Error: no such named collection";
      if (res.handlers.site && !res.handlers.site.trusted && res.handlers.site != obj.getSite())
         return;
   }
   var cnt = obj.size();
   if (cnt==0) res.write(param.no ? param.no : "0");
   else if (cnt==1) res.write(param.one ? param.one : "1");
   else if (cnt>1) res.write(cnt + (param.more ? param.more : ""));
   return;
}


/**
 * macro renders for each object of a certain collection a specific skin
 * @param collection String, name of the collection (resp. collection path) of this object to be used; if not specified, then the direct children are taken;
 *   e.g. "images/topics/info"
 * @param skin String, name of the skin to be rendered for each item
 * @param order String, specifies the attribute which will be used to order the list;
 *              this can currently take "createtime", "modifytime", "name", "random";
 *              if not specified, then the default ordering will be used;
 *              possible (optional) suffixes: ASC, DESC; e.g. "modifytime ASC"
 * @param max Integer, maximum number of items to be rendered
 * @param default String, rendered if the list is empty
 * @param itemprefix String
 * @param itemsuffix String
 * @param separator String
 * @param clipping String, to be rendered at the end if not all items are displayed
 * @param exclude String: comma-separated list of story IDs, if "this" then the current page is ignored
 * @param listall (optional) if set to true, objects with a createtime in the future are listed
 */
function list_macro(param, isTrusted) {
   res.meta.cachePart = false;
   var sk = param.skin ? param.skin : "listitem";
   var objs = new Array();
   if (param.collection) {
     var colls = param.collection.split(",");
     for (var j=0; j<colls.length; j++) {
        var obj = this;
        var coll = colls[j].trim();
        var objpath = coll.split("/");
        for (var i=0; i<objpath.length; i++) {
           if (objpath[i]=="") obj = root;
           else obj = obj.get(objpath[i]);
           if (obj==null) return;
        }
        objs[objs.length] = obj;
     }
   } else {
      objs[0] = this;
   }
   if (objs.length==0) return "Error: no such named collection";
   var max = param.max ? parseInt(param.max, 10) : null;
   var items = [];
   var objsLength = objs.length;
   
   // loop through collections
   for (var j=0; j < objsLength; j++) {
      var objsJSize = objs[j].size();
      for (var k=0; k < objsJSize; k++) {
         var obj = objs[j].get(k);
         if (!isTrusted && res.handlers.site && !res.handlers.site.trusted && res.handlers.site != obj.getSite())
            continue;

         items[items.length] = obj;
         if (max && !param.exclude && items.length > max) break;
      }
      if (max && !param.exclude && items.length > max) break;
   }
   if (param.exclude && param.exclude=="this" && path.story) {
      var excludes = new Array(path.story._id+"");
   } else if (param.exclude && param.exclude!="this") {
      var excludes = param.exclude.split(",");
   } else {
      var excludes = new Array();
   }
   if (excludes.length>0) {
      var tmp = new Array();
      for (var i=0; i<items.length; i++) {
         if (Array.contains(excludes, items[i]._id)==false) {
            tmp[tmp.length] = items[i];
         }
      }
      items = tmp;
   }
   var size = items.length;
   if (max && max<size) size = max;
   if (size==0)
      return param["default"];
   items = arraySort(items, param.order);
   for (var i = 0; i < size; i++) {
      if (param.itemprefix) res.write(param.itemprefix);
      items[i].renderSkin(sk);
      if (param.itemsuffix) res.write(param.itemsuffix);
      if (param.separator && i+1 < size) res.write(param.separator);
   }
   if (param.clipping && size<items.length) {
      if (param.itemprefix) res.write(param.itemprefix);
      res.write(param.clipping);
      if (param.itemsuffix) res.write(param.itemsuffix);
   }
   return;
}


/**
 * Renders a script HTML-tag to load a site javascript (ClientSide)
 * it's advantage is that it will keep track of loaded scripts for
 * a request and prevent double loading
 *
 * @example
 *   <% site.script name="/jsLib.js" %>
 *   <% site.script name="/somescript.js" load="async" %>
 *   <% site.script name="/someotherscript.js" load="defer" %>
 * @param name   name of the script (including file extension)
 * @param load   optional; valid values are: "async" | "defer"
 * @doclevel public
 */
function script_macro(param) {
   if (!param.name) return;
   if (res.data.loadedScripts && res.data.loadedScripts[param.name]) return;
   if (!res.data.loadedScripts) res.data.loadedScripts = {};

   res.data.loadedScripts[param.name] = true;

   var url = param.name.startsWith("/")
      ? getStaticUrl() + param.name.substring(1)
      : this.href(param.name);

   var load = param.load && ['async', 'defer'].indexOf(param.load) >= 0
      ? ' ' + param.load
      : '';

   res.write('<script src="' + url + '"' + load + '></script>');
   return;
}
