
/**
 * function checks if there's a site in path
 * if true it checks if the site or the user is blocked
 */
function onRequest() {

   knallgrau.Event.notifyObservers(this, "onBeforeOnRequest", []);

   // set JSESSIONID-cookie needed for clustering
   knallgrau.setClusterCookie();

   // add original ip if client has been forwarded
   if (req.servletRequest.getHeader("x-forwarded-for")) req.data.http_remotehost = req.servletRequest.getHeader("x-forwarded-for") + "-" + req.data.http_remotehost;

   autoLogin();

   // defining skinpath, membershipLevel
   res.meta.memberlevel = null;
   // if root.sys_frontSite is set and the site is online
   // we put it into res.handlers.site to ensure that the mirrored
   // site works as expected
   if (!path.Site && root.sys_frontSite && root.sys_frontSite.online)
      res.handlers.site = root.sys_frontSite;
   if (res.handlers.site) {
      if (res.handlers.site.blocked && req.action != "unblock" && req.action != "unsubscribe") {
         res.message = getMessage("Site.edit.block.isBlocked", {alias: res.handlers.site.alias, reactivateLink: res.handlers.site.href("unblock")});
         res.redirect(root.href(""));
      }
      if (session.user)
         res.meta.memberlevel = res.handlers.site.members.getMembershipLevel(session.user);
      // set a handler that contains the context
      res.handlers.context = res.handlers.site;
   } else {
      // set a handler that contains the context
      res.handlers.context = root;
   }
   // check whether request came for the correct host
   if (req.action != "ping" && req.action != "pingIsClusterMaster")
      checkHttpHost();
   // set res.handlers.layout
   var alternativeLayout = root.layouts.getAlternativeLayout();

   if(alternativeLayout) {
      res.contentType = alternativeLayout.contentType;
      res.handlers.layout = alternativeLayout.layout;
      res.data.alternativeRendering = true;
   } else if (session.data.layout && (session.data.layout.isShareable(session.user, res.meta.memberlevel) || (session.data.layout.site == res.handlers.site))) {
      // test drive a layout
      res.handlers.layout = session.data.layout;
      res.message = session.data.layout.getTestDriveMessage();
   } else {
      // define layout handler
      res.handlers.layout = res.handlers.context.getLayout();
   }

   // set skinpath
   var arr = res.handlers.layout.getSkinPath();
   var locale = getCurrentLocale();
   var fbacks = app.data.fallbackLocales[locale.toString()];
   for (var i=0; i<fbacks.length; i++) arr.push(getLocaleSkinDirPath(fbacks[i]));
   res.skinpath = arr;

   // set res.meta.adminMenuPath
   if (
      MGR_MENU_PATH[path[path.length - 1]._prototype] &&
      MGR_MENU_PATH[path[path.length - 1]._prototype][req.action]
   ) {
      res.meta.adminMenuPath = MGR_MENU_PATH[path[path.length - 1]._prototype][req.action];
   } else {
      res.meta.adminMenuPath = "public";
   }

   if (session.user && session.user.blocked) {
      // user was blocked recently, so log out
      session.logout();
      res.message = new Exception("accountBlocked");
      res.redirect(res.handlers.context.href());
   }

   var check = this.checkAccess(req.action, session.user, res.meta.memberlevel);
   if (check != null) {
     res.message = check.message;
     res.redirect(check.url);
   }
   knallgrau.Event.notifyObservers(this, "onAfterOnRequest", []);

   // Re-set session cookie with secure flag (Twoday’s Helma cannot do this)
   addHeaderCookie("HopSession", session.cookie, 0, true);
   return;
}


/**
 * Permission checks (called by hopobject.onRequest())
 * This method needs to be overwritten by each single HopObject, and just exists
 * to make sure that anonymous users don't get access to closed Sites.
 *
 * @param action  String, name of action
 * @param usr     User, who wants to access an action
 * @param level   Int, Membership level
 * @return Object if permission was denied <br />
 *   .message  message, if something went wrong <br />
 *   .url      recommanded redirect URL
 * @doclevel public
 */
function checkAccess(action, usr, level) {
   if (usr && usr.sysadmin) return;
   var url;
   var site = path.Site;
   if (!site) return;
   try {
      if (site && !site.online) {
         checkIfLoggedIn(this.href(action));
         site.checkView(usr, level);
      }
   } catch (deny) {
      return {message: deny.toString(), url: (url ? url : (site ? site.href() : root.href()))};
   }
}
