
/**
 * function checks if story fits to the minimal needs (must have at least a text ;-)
 * @param Obj Object containing the properties needed for creating a new Story
 * @param Obj User-Object creating this story
 * @return Obj Object containing three properties:
 *             - error (boolean): true if error happened, false if everything went fine
 *             - message (String): containing a message to user
 *             - story (Obj): story-object containing assigned form-values
 *             - id (Int): id of created story
 */
function evalNewStory(param, creator) {
   var s = new Story(creator, param.http_remotehost);
   // collect content
   var content = extractContent(param);
   // if all story parts are null, return with error-message
   if (!content.exists)
      throw new Exception("textMissing");
   s.checkContentSize(content.value);
   // sanitize user input only if site is untrusted or allows stories to be written by everyone
   var skipSanitize = this._parent.trusted || this._parent.preferences.getProperty('usercontrib') === 0;
   if (!skipSanitize) {
      for (var i in content.value) {
         content.value[i] = sanitize(content.value[i], app.properties.allowedTagsForStory.split(","));
      }
   }
   s.setContent(content.value);
   // check if the create date is set in the param object
   if (param.createtime) {
      try {
         var fmt = (param.createtime.indexOf(":") != param.createtime.lastIndexOf(":")) ? "yyyy-MM-dd HH:mm:ss" : "yyyy-MM-dd HH:mm";
         s.createtime = param.createtime.toDate(fmt, this._parent.getTimeZone());
      } catch (error) {
         throw new Exception("timestampParse", param.createtime);
      }
   }
   s.editableby = !isNaN(parseInt(param.editableby, 10)) ?
                  parseInt(param.editableby, 10) : EDITABLEBY_ADMINS;
   s.discussions = param.discussions ? 1 : 0;

   // check name of topic (if specified)
   if (param.topic) {
      param.topic = stripTags(param.topic);
       // FIXME: this should be solved more elegantly
      if (param.topic.contains("\\") || param.topic.contains("/") || param.topic.contains("?") || param.topic.contains("+"))
         throw new Exception("topicNoSpecialChars");
      if (this._parent.topics[param.topic] || this._parent.topics[param.topic + "_action"])
         throw new Exception("topicReservedWord");
      s.topic = param.topic;
   } else if (param.addToTopic) {
      s.topic = stripTags(param.addToTopic);
   }
   // check the online-status of the story
   if (param.publish)
      s.online = param.addToFront ? 2 : 1;
   else
      s.online = 0;

   if (s.createtime > new Date()) {
      s.publishToTimeState = s.online + 1;
      s.online = 0;
   }

   // store the story
   if (!this.add(s))
      throw new Exception("storyCreate");
   // process param.images
   // NOTYETIMPLEMENTED
   // process param.files
   // NOTYETIMPLEMENTED
   // update affected static RSS Feeds
   s.site.updateAffectedStaticRSSFeeds(s.topic);

   // hook for modules
   this.applyModuleMethods("onEvalNewStory", [s], false);

   var result = new Message("storyCreate", null, s);
   result.id = s._id;
   if (s.online) {
      s.site.lastposting = new Date();
      s.site.lastupdate = new Date();
      result.url = s.href();
   } else {
      result.url = this.href();
   }
   knallgrau.Event.notifyObservers(this, "afterEvalNewStory", {"story": s, "param": param, "creator": creator, "result": result});
   return result;
}


/**
 * delete a story
 * including all the comments
 * @param Obj Story-Object that should be deleted
 * @return Obj Object containing two properties:
 *             - error (boolean): true if error happened, false if everything went fine
 *             - message (String): containing a message to user
 */
function deleteStory(storyObj) {
   // update affected static RSS Feeds
   storyObj.site.updateAffectedStaticRSSFeeds(storyObj.topic);
   storyObj.deleteAll();
   this._parent.lastupdate = new Date();
   storyObj.remove();
   storyObj.deleteComments();

   // hook for modules
   knallgrau.Event.notifyObservers(this, "afterDeleteStory", {"story": storyObj});
   return new Message("storyDelete");
}


/**
 * function loops over all stories and removes them (including their comments!)
 * @return Boolean true in any case
 */
function deleteAll() {
   for (var i=this.size();i>0;i--)
      this.deleteStory(this.get(i-1));
   return true;
}

