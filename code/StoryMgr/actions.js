
/**
 * list all stories
 */
function main_action() {
   res.data.storylist = renderList(this, "mgrlistitem", 20, req.data.page);
   res.data.pagenavigation = renderPageNavigation(this, this.href(), 20, req.data.page);
   res.data.title = getMessage("StoryMgr.main.title", {siteTitle: this._parent.title});
   res.data.body = this.renderSkinAsString("main");
   this._parent.renderPage();
   return;
}


/**
 * list all offline stories
 */
function offline_action() {
   res.data.storylist = renderList(this.offline, "mgrlistitem", 20, req.data.page);
   res.data.pagenavigation = renderPageNavigation(this.offline, this.href(req.action), 20, req.data.page);
   res.data.title = getMessage("StoryMgr.offline.title", {siteTitle: this._parent.title});
   res.data.body = this.renderSkinAsString("main");
   this._parent.renderPage();
   return;
}


/**
 * list all stories of a user inside the site the
 * membership belongs to
 */
function mystories_action() {
   var ms = this._parent.members.get(session.user._id.toString());
   res.data.storylist = renderList(ms ? ms.stories : [], "mgrlistitem", 20, req.data.page);
   res.data.pagenavigation = renderPageNavigation(ms ? ms.stories : [], this.href(req.action), 20, req.data.page);
   res.data.title = getMessage("StoryMgr.mystories.title", {siteTitle: this._parent.title});
   res.data.body = this.renderSkinAsString("main");
   this._parent.renderPage();
   return;
}


/**
 * action for creating a new Story
 */
function create_action() {
   knallgrau.Event.notifyObservers(this, "beforeCreateAction", req.data);
   // restore any rescued text
   if (session.data.rescuedText)
      restoreRescuedText();

   var s = new Story();
   s.site = this.getSite();
   s.discussions = this._parent.preferences.getProperty("discussionsby") == "nobody" ? 0 : 1;
   // storing referrer in session-cache in case user clicks cancel later
   if (!session.data.referrer && req.data.http_referer)
      session.data.referrer = req.data.http_referer;

   if (req.data.cancel) {
      var url = session.data.referrer ? session.data.referrer : this.href();
      session.data.referrer = null;
      res.redirect(url);
   } else if (req.data.save || req.data.publish) {
      try {
         checkSecretKey();
         var result = this.evalNewStory(req.data, session.user);
         res.message = result.toString();
         session.data.referrer = null;
         res.redirect(result.url);
      } catch (err) {
         res.message = err.toString();
      }
   }

   res.data.title =  getMessage("StoryMgr.create.title", {siteTitle: this._parent.title});
   res.data.action = this.href("create");
   res.data.body = s.renderSkinAsString("edit");
   this._parent.renderPage();
   return;
}
