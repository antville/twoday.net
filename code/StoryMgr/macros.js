
/**
 * returns an Array of list navigation items
 * @param param Object   passed by Macro
 * @return Array
 */
function getListNavigationItems() {
   var items = [
      {
         text: getMessage("admin.menu.contribute.stories.all"),
         action: "StoryMgr/main"
      },
      {
         text: getMessage("admin.menu.contribute.stories.offline"),
         action: "StoryMgr/offline"
      },
      {
         text: getMessage("admin.menu.contribute.stories.mystories"),
         action: "StoryMgr/mystories"
      }
   ];
   return items;
}


/**
 * returns an Array of StoryManager tasks
 * @param param Object   passed by Macro
 * @return Array
 */
function getStoryManagerTasks(param) {
   var items = [
      {
         action: "StoryMgr/create"
      }
   ];
   return items;
}


/**
 * render list of StoryManager tasks
 */
function storyManagerTasks_macro(param) {
   var items = this.getStoryManagerTasks(param);
   this.renderTasks(items, param);
}
