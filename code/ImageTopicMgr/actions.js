
/**
 * main action
 */
function main_action() {
   res.data.title = getMessage("ImageTopicMgr.main.title", {siteTitle: this._parent._parent.title});
   res.data.body = this.renderSkinAsString ("main");
   res.handlers.site.renderPage();
   return;
}
