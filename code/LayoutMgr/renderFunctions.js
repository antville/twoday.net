/**
 * render a dropdown containing shareable system layouts
 * @param Object current layout
 */
function renderParentLayoutChooser(selLayout, firstOption) {
   var options = [];
   var shareable = root.layouts.getShareable(session.user, res.meta.memberlevel);
   var size = shareable.length;
   for (var i=0;i<size;i++) {
      var l = shareable[i];
      options.push({value: l.alias, display: l.title});
   }
   var selected = null;
   if (selLayout && selLayout.parent)
      selected = selLayout.parent.alias;
   Html.dropDown({name: "layout"}, options, selected, firstOption);
   return;
}


/**
 * function renders a table with all shareable layouts
 * rendering a preview image, a preview button and some basic information
 */
function renderLayoutChooser(param) {
   var shareable = root.layouts.getShareable(session.user, res.meta.memberlevel);
   shareable.sort(new Function("a", "b", "return b.sharedBy.count() - a.sharedBy.count()"));
   for (var i = 0; i < shareable.length; i++) {
      var item = shareable[i];
      var itemParam = {
         checked: ((req.data.layout && req.data.layout == item.alias) || i == 0) ? "checked" : null
      };
      item.renderSkin("chooserItem", itemParam)
   }
   res.write('<br clear="all" />');
   res.write('<div class="clearall"></div>');
}
