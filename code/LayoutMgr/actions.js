
/**
 * main action
 */
function main_action() {
   if (req.data["activate"]) {
      this.setDefaultLayout(req.data["activate"]);
      res.redirect(this.href());
   }
   res.data.title = getMessage("LayoutMgr.main.title", {siteTitle: res.handlers.context.getTitle()});
   res.data.action = this.href();
   res.data.layoutlist = renderList(this.originals, (this._parent._prototype == "Site") ? "mgrlistitem" : "rootmgrlistitem", 20, req.data.page);
   res.data.pagenavigation = renderPageNavigation(this.originals, this.href(), 20, req.data.page);
   res.data.body = this.renderSkinAsString("main");
   res.data.sidebar01 = this.renderSkinAsString("mainSidebar");
   this.renderMgrPage();
   return;
}


/**
 * choose a new root layout
 */
function create_action() {
   if (req.data.cancel)
      res.redirect(this.href());
   else if (req.data.create) {
      try {
         checkSecretKey();
         var result = this.evalNewLayout(req.data, session.user);
         res.message = result.toString();
         res.redirect(result.obj.href("edit"));
      } catch (err) {
         res.message = err.toString();
      }
   }

   // render a list of root layouts that are shareable
   var shareable = root.layouts.getShareable(session.user, res.meta.memberlevel);
   res.data.layoutlist = renderList(shareable, "chooserItem", 20, req.data.page);
   res.data.pagenavigation = renderPageNavigation(shareable, this.href(req.action), 20, req.data.page);

   res.data.title = getMessage("LayoutMgr.create.title", {siteTitle: res.handlers.context.getTitle()});
   res.data.action = this.href(req.action);
   res.data.body = this.renderSkinAsString("new");
   this.renderMgrPage();
   return;
}


/**
 * import action
 */
function import_action() {
   if (req.data.cancel)
      res.redirect(this.href());
   else if (req.data["import"]) {
      try {
         checkSecretKey();
         var result = this.evalImport(req.data, session.user);
         res.message = result.toString();
         res.redirect(this.href());
      } catch (err) {
         res.message = err.toString();
      }
   }
   // render a list of root layouts that are shareable
   var shareable = root.layouts.getShareable(session.user, res.meta.memberlevel);
   res.data.layoutlist = renderList(shareable, "chooserItem", 6, req.data.page);
   res.data.pagenavigation = renderPageNavigation(shareable, this.href(req.action), 6, req.data.page);

   res.data.title = getMessage("LayoutMgr.import.title", {siteTitle: res.handlers.context.getTitle()});
   res.data.action = this.href(req.action);
   res.data.body = this.renderSkinAsString("import");
   this.renderMgrPage();
   return;
}


/**
 * choose a new root layout
 */
function switch_action() {
   // this wizard is just available for sites
   if (!path.site) {
      res.message = getMessage("LayoutMgr.switch.notAvailable");
      res.redirect(this.href("main"));
   }
   // initialize
   res.data.action = this.href(req.action);
   res.data.title = getMessage("LayoutMgr.switch.title", {siteTitle: res.handlers.context.getTitle()});
   res.data.sidebar01 = this.renderSkinAsString("mainSidebar");
   var skinParam = {};

   if (req.data.cancel) {
      res.redirect(this.href());

   } else if (req.data.next) {
      switch (req.data.step) {
         case "template":
            req.data.laststep = "template";
            req.data.step = "template_done";
            break;
         case "template_done":
            if (req.data.create_option == "test") {
               session.data.layout = root.layouts.get(req.data.layout);
               res.redirect(path.site.href("main"));
            } else {
               try {
                  checkSecretKey();
                  var result = this.evalNewLayout(req.data, session.user);
                  res.message = result.toString();
                  var newLayout = result.obj;
                  if (req.data.create_option_activate == "yes") {
                     this.setDefaultLayout(newLayout.alias);
                     res.redirect(path.site.href("main"));
                  } else {
                     session.data.layout = newLayout;
                     res.redirect(newLayout.href("preferences"));
                  }
               } catch (err) {
                  res.message = err.toString();
               }
            }
            break;
         case "create":
            try {
               checkSecretKey();
               var result = this.evalNewLayout(req.data, session.user);
               res.message = result.toString();
               var newLayout = result.obj;
               session.data.layout = newLayout;
               res.redirect(newLayout.skins.href());
            } catch (err) {
               res.message = err.toString();
            }
            break;
         default:
            if (req.data.switch_option) {
               req.data.step = req.data.switch_option;
            } else {
               res.message = getMessage("LayoutMgr.switch.chooseOption");
            }
      }
   } else if (req.data.back) {
      switch (req.data.step) {
         case "template":
            req.data.step = null;
         case "template_done":
            req.data.step = req.data.lastpage;
         default:
            ;
      }
   }

   var shareable = root.layouts.getShareable(session.user, res.meta.memberlevel);
   switch (req.data.step) {
      case "template":
         if (!req.data.layout && shareable.length > 0) req.data.layout = shareable[0].alias;
         res.data.title = getMessage("LayoutMgr.switch.chooseLayout.header");
         res.data.body = this.renderSkinAsString("switch_template", {});
         break;
      case "template_done":
         var parentLayout = root.layouts.get(req.data.layout);
         if (!req.data.create_option) req.data.create_option = "create";
         if (!req.data.title) req.data.title = parentLayout.title;
         if (!req.data.alias) req.data.alias = buildAlias(parentLayout.alias, this);
         if (!req.data.create_option_activate) req.data.create_option_activate = "no";

         res.data.title = getMessage("LayoutMgr.switch.template_done.header");
         res.data.body = this.renderSkinAsString("switch_template_done", {});
         break;
      case "create":
         res.data.title = getMessage("LayoutMgr.switch.create.header");
         res.data.body = this.renderSkinAsString("switch_create", {});
         break;
      case "import":
         res.redirect(this.href("import"));
         break;
      case "own":
         res.message = getMessage("LayoutMgr.switch.own.hintMessage");
         res.redirect(this.href("main"));
         break;
      case "export":
         res.message = getMessage("LayoutMgr.switch.export.hintMessage");
         res.redirect(this.href("main"));
         break;
      default:
         req.data["switch_option"] = "template";
         res.data.body = this.renderSkinAsString("switch_options", {myLayoutCount: this.count(), shareableLayoutCount: shareable.length});
   }

   this.renderMgrPage();
   return;
}
