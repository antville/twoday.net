
/**
 * create a new Layout based on a chosen parent layout
 * @param Object Object containing the submitted form values
 * @param Object Creator of the layout object
 */
function evalNewLayout(param, creator) {
   // check alias -> ?? should there be always an alias
   /* if (!alias)
      throw new Exception("layoutAliasMissing");
   else  */
   if (path.Site && path.Site.type == "free" && this.size() == 2)
      throw new Exception("layoutAmountRestricted");
   if (param.alias) {
      if (this.get(param.alias))
         throw new Exception("layoutAliasExisting");
      else if (!param.alias.isClean())
         throw new Exception("aliasNoSpecialChars");
      else if (param.alias.length > 30)
         throw new Exception("aliasTooLong");
      else if (this[param.alias] || this[param.alias + "_action"])
         throw new Exception("aliasReserved");
   }
   // check if title is missing -> ?? should there be always an alias
   /*
   if (!title)
      throw new Exception("layoutTitleMissing");
      */
   var newLayout = new Layout(this._parent instanceof Site ? this._parent : null,
                              "untitled", creator);
   if (param.layout) {
      var parentLayout = root.layouts.get(param.layout);
      if (!parentLayout)
         throw new Exception("layoutParentNotFound");
      newLayout.setParentLayout(parentLayout);
      newLayout.title = (param.title) ? param.title : parentLayout.title;
      newLayout.alias = param.title ? buildAlias(param.title, this) : buildAlias(parentLayout.alias, this);
      newLayout.locale = parentLayout.locale;
   } else {
      if (param.title) newLayout.title = param.title;
      newLayout.alias = buildAlias(newLayout.title, this);
   }
   if (param.alias) newLayout.alias = param.alias;

   if (!this.add(newLayout))
      throw new Exception("layoutCreate");
   return new Message("layoutCreate", newLayout.title, newLayout);
}


/**
 * function loops over all layouts and removes them (including their skins!)
 * @return Boolean true in any case
 */
function deleteAll() {
   for (var i=this.size(); i>0; i--)
      this.deleteLayout(this.get(i-1));
   return true;
}


/**
 * function deletes a layout
 * @param Obj Layout-HopObject to delete
 */
function deleteLayout(layout) {
   layout.deleteAll();
   var title = layout.title;
   layout.remove();
   return new Message("layoutDelete", title);
}


/**
 * Set the layout with the alias passed as argument
 * to the default site layout
 */
function setDefaultLayout(alias) {
   var l = this.get(alias);
   if (l && this._parent.layout != l)
      this._parent.layout = l;
   return;
}


/**
 * import a new Layout that was uploaded as a zip file
 */
function evalImport(param, creator) {
   if (param.uploadError) {
      // looks like the file uploaded has exceeded uploadLimit ...
      throw new Exception("layoutImportTooBig");
   } else if (!param.zipfile || param.zipfile.contentLength == 0) {
      // looks like nothing was uploaded ...
      throw new Exception("layoutImportNoUpload");
   }
   try {
      var contents = Zip.extractData(param.zipfile.getContent());
      // check wether this is a translated archive
      var archiveData = contents.files["archiveinfo.xml"] ? contents.files["archiveinfo.xml"].data : null;
      if (archiveData) {
         // FIXME !
         var archiveInfo = Xml.readFromString(new java.lang.String(archiveData, 0, archiveData.length));
         var rootLocalization = archiveInfo.locale;
         // loop through the localized layouts
         for (var i in contents.files) {
            if (convertStringToLocale(i)) {
               newLayout.skins.evalImport(contents.files[i]['skins']);
               newLayout.images.evalImport(contents.files[i]['imagedata'], contents.files[i]['images']);
            }
         }

      } else {
         // first, check if there's a file called "preferences" in the archive
         // and convert it into a HopObject
         var data = contents.files["preferences.xml"].data;
         var importLayout = Xml.readFromString(new java.lang.String(data, 0, data.length));
         // start with the actual import
         var newLayout = new Layout(this._parent instanceof Site ? this._parent : null,
                                    importLayout.title, session.user);
         newLayout.parent = param.layout ? root.layouts.get(param.layout) : null;
         newLayout.preferences.setAll(importLayout.preferences);
         newLayout.shareable = 0;
         newLayout.imported = 1;
         newLayout.alias = buildAlias(importLayout.alias, this);
         newLayout.description = importLayout.description;
         newLayout.creator = session.user;
         // FIXME: this should be done after importing skins
         // and images, buf for some reasons skins then
         // won't be stored persistent
         this.add(newLayout);
         // import skins
         if (contents.files['skins']) {
            newLayout.skins.evalImport(contents.files.skins);
         }
         if (contents.files['imagedata'] && contents.files['images']) {
            newLayout.images.evalImport(contents.files['imagedata'], contents.files['images']);
         }
         // import images
         // newLayout.images.evalImport(contents.files.imagedata, contents.files.images);
         return new Message("layoutImport", newLayout.title);

      }

   } catch (err) {
      throw new Exception("layoutImportCorrupt");
   }
   return;
}


/**
 * Returns an Array of shareable layouts for a specific user
 * @param usr     User Object
 * @param level   Number, res.meta.memberlevel
 */
function getShareable(usr, level) {
   var result = [];
   for (var i = 0; i < this.shareable.size(); i++) {
      if (this.shareable.get(i).isShareable(usr, level)) {
         result.push(this.shareable.get(i));
      }
   }
   return result;
}


/**
 * This function tries to find a contentType and a layout for the given parameters.
 * It also checks if the layout exists in the current LayoutMgr.
 * Currently we support only json format but this might change in the future
 * 
 * @param contentType String valid contentType
 * @param format String valid format
 * @return Object containing contentType of response and layout to use or null if no layout/contentType exists
 */
function getAlternativeLayout(contentType, format) {
   contentType = contentType ? contentType : req.servletRequest.getHeader("Accept");
   format = format ? format : req.data.format;
   // TODO we should add a mapping for nicer lookup, problem is that contentType may contain more than one contentType
   // for now only by req.param
   // contentType.indexOf("application/json") != -1 || contentType.indexOf("application/javascript") != -1 || 
   if ("json".equals(format)) {
      var layout = this.get("json");
      if (layout)
         return {"contentType": "application/json", "layout": layout};
   }
   return null;
}
