/**
 * render a dropdown containing available layouts
 */
function layoutchooser_macro(param) {
   var options = [];
   var size = this.size();
   for (var i=0;i<size;i++) {
      var l = this.get(i);
      options.push({value: l.alias, display: l.title});
   }
   Html.dropDown({name: "layout"}, options, param.selected, param.firstOption);
   return;
}


/**
 * render main tasks for layout manager
 */
function layoutMgrMainTasks_macro(param) {

   var containerParam = Object.clone(param);
   var itemParam = Object.clone(param);

   var items = [
      {  action: "LayoutMgr/switch"  },
      {  action: "LayoutMgr/create"  },
      {  action: "LayoutMgr/import"  }
   ]
   res.push();
   this.renderTaskItems(items, itemParam);
   containerParam.items = res.pop();

   this.renderSkin("taskContainer", containerParam);
   return;
}


/**
 * renders the radio option 'own' just if there is more than one layout for this site
 */
function switchOptionOwn_macro(param) {
   var myLayoutCount = this.count();
   res.data.myLayoutCount = myLayoutCount;
   if (myLayoutCount > 1) {
      this.renderSkin("switch_options_own");
   }
}


/**
 * macro renders a table with all shareable layouts
 * rendering a preview image, a preview button and some basic information
 */
function visualLayoutChooser_macro(param) {
   this.renderLayoutChooser(param);
}
