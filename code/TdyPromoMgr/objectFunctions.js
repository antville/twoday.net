
/**
 * Creates Promo object and makes it persistent.
 * Generates random MD5 Hash of size 8.
 * Promos can be value-based or month-based. They can also
 * be restricted to a certain site or user.
 *
 * @param param Object
 * @param usr User
 * @return p Promo
 */
function createPromo(param, usr) {
  var p = new TdyPromo();
  p.code = Packages.helma.util.MD5Encoder.encode((new Date()).valueOf().toString()).substring(0, 8);
  p.type = param.type;
  p.months = param.months;
  p.value = param.value;
  p.description = param.description;
  p.used = 0;
  p.validuntil = param.validuntil ? param.validuntil :  new Date((new Date()).valueOf() + 1000*3600*24*30*6);
  p.createtime = new Date();
  p.site = param.site;
  p.user = param.user;
  p.creator = usr;
  this.add(p);
  return p;
}


/**
 * @param code String
 * @param site Site
 * @param user User
 *
 * @return Boolean
 */
function usePromo(code, site, user) {
  var p = this.get(code);
  if (p == null) throw new Exception("tdyPromoNotValid");
  if (p.used == 1) throw new Exception("tdyPromoAlreadyUsed");
  if (p.validuntil < (new Date())) throw new Exception("tdyPromoNotValidAnymore");
  if (site != null && p.site != null && (p.site != site)) throw new Exception("tdyPromoWrongSite");
  if (user != null && p.user != null && (p.user != user)) throw new Exception("tdyPromoWrongUser");
  site.tdyBillingExtendPaidUntil(p.value, p.months, p.type);
  p.used = 1;
  p.validuntil = new Date();
  p.site = site;
  p.user = user;
  return getMessage("confirm.tdyPromoOK");
}
