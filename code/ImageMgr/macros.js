
/**
 * macro renders a list of existing topics as dropdown
 */
function topicchooser_macro(param) {
   var size = path.Site.images.topics.size();
   var options = new Array();
   for (var i=0;i<size;i++) {
      var topic = path.Site.images.topics.get(i);
      if (topic.size()) {
         options[i] = {value: topic.groupname, display: topic.groupname};
         if (req.data.addToTopic)
            var selected = req.data.addToTopic;
         else if (path.Image && path.Image.topic == topic.groupname)
            var selected = topic.groupname;
      }
   }
   param.firstOption = param.firstOption ? param.firstOption : "--- " + getMessage("ImageMgr.new.topicChooserFirstOption") + " ---";
   param.name = param.name || "addToTopic";
   Html.dropDown(param, options, selected, param.firstOption);
   return;
}


/**
 * macro renders a dropdown chooser for ImageMgr/new.skin
 * to select a resize method
 */
function resizeMethodChooser_macro(param) {
   var values = ["max","crop","scale","exact","no"];
   var options = [];
   // get the defined locale of this site for comparison
   for (var i in values) {
      options[i] = {
         display:  getMessage("ImageMgr.new.resize." + values[i]),
         value:    values[i]
      };
   }
   param.name = param.name ? param.name : "resizeto";
   Html.dropDown(param, options, req.data[param.name]);
}


/**
 * returns an Array of list navigation items
 * @param param Object   passed by Macro
 * @return Array
 */
function getListNavigationItems() {
   var items = [
      {
         text: getMessage("admin.menu.contribute.images.all"),
         action: "ImageMgr/main"
      },
      {
         text: getMessage("admin.menu.contribute.images.myimages"),
         action: "ImageMgr/myimages"
      }
   ];
   return items;
}


/**
 * returns an Array of ImageManager tasks
 * @param param Object   passed by Macro
 * @return Array
 */
function getImageManagerTasks(param) {
   var items = [
      {
         action: "ImageMgr/create"
      }
   ];
   return items;
}


/**
 * render list of ImageManager tasks
 */
function imageManagerTasks_macro(param) {
   var items = this.getImageManagerTasks(param);
   this.renderTasks(items, param);
   return;
}
