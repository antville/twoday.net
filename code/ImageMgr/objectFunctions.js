
/**
 * Checks if image fits to the minimal needs and stores the passed image.
 *
 * @param param   Object containing the properties needed for creating a new Image
 *   .rawimage   ByteArray image data passed by a web form
 *   .alias      (optional) alias to be used. otherwise we will create the alias from the uploaded image name
 *   .alttext    an alt text (a description) for this image. this will be used as the alt text in the image tag.
 *   .topic      (optional) topic name. this is the value from the text input field. THIS IS SUBJECT TO CHANGE
 *   .addToTopic (optional) existing topic name. this is the value from the dropdown field. THIS IS SUBJECT TO CHANGE
 * @param creator User creating this image
 * @return Obj Object containing two properties:
 *             - error (boolean): true if error happened, false if everything went fine
 *             - message (String): containing a message to user
 * @throws Exception
 * @doclevel public
 */
function evalImg(param, creator) {
   if (param.uploadError) {
      // looks like the file uploaded has exceeded uploadLimit ...
      throw new Exception("imageFileTooBig");
   } else if (!param.rawimage || param.rawimage.contentLength == 0) {
      // looks like nothing was uploaded ...
      throw new Exception("imageNoUpload");
   } else if (param.rawimage && (!param.rawimage.contentType || !evalImgType(param.rawimage.contentType)) && !param.webpublishingwizard) {
      // whatever the user has uploaded wasn't recognized as an image
      throw new Exception("imageNoImage");
   } else if (param.webpublishingwizard) {
      var fileext = param.rawimage.getName().split(".").pop().toLowerCase();
      if (fileext != "jpg" && fileext != "gif" && fileext != "jpeg" && fileext != "png") {
         throw new Exception("imageNoImage");
      }
   }
   var filesize = Math.round(param.rawimage.contentLength / 1024);
   if (this._parent.getDiskUsage() > this._parent.getDiskQuota()) {
      // disk quota has already been exceeded
      throw new Exception("siteQuotaExceeded");
   }

   var newImg = new Image(creator);
   // if no alias given try to determine it
   if (!param.alias) {
      newImg.alias = buildAliasFromFile(param.rawimage, this);
   } else {
      if (!param.alias.isFileName())
         throw new Exception("noSpecialChars");
      // if the new Image should be the weblog icon, the old one has to be deleted
      if (param.alias == "icon") {
         var oldicon = this.get("icon");
         if (oldicon != null) this.deleteImage(oldicon);
         newImg.alias = "icon";
         param.resizeto = "crop";
         param.width = "48";
         param.height = "48";
      } else if (param.alias == "favicon") {
         var oldicon = this.get("favicon");
         if (oldicon != null) this.deleteImage(oldicon);
         newImg.alias = "favicon";
         param.resizeto = "crop";
         param.width = "16";
         param.height = "16";
      } else {
        newImg.alias = buildAlias(param.alias, this);
      }
   }
   var origname = param.rawimage.getName();
   var fileext = origname.contains(".") ? origname.substring(origname.lastIndexOf(".") + 1) : "";
   newImg.filename = newImg.alias + "." + fileext.toLowerCase();
   // check name of topic (if specified)
   var topicName = null;
   if (param.topic) {
      param.topic = stripTags(param.topic);
      if (param.topic.contains("\\") || param.topic.contains("/") || param.topic.contains("?") || param.topic.contains("+"))
         throw new Exception("topicNoSpecialChars");
      topicName = param.topic;
   } else if (param.addToTopic) {
      topicName = stripTags(param.addToTopic);
   }
   newImg.topic = topicName;
   // save/resize the image
   var dir = this._parent.getStaticDir("images");
   newImg.save(param.rawimage, dir, param);
   // the fullsize-image is on disk, so we add the image-object (and create the thumbnail-image too)
   newImg.alttext = stripTags(param.alttext);
   if (!this.add(newImg))
      throw new Exception("imageAdd");
   // the fullsize-image is stored, so we check if a thumbnail should be created too
   if (newImg.width > THUMBNAILWIDTH || newImg.height > THUMBNAILHEIGHT)
      newImg.createThumbnail();
   var result = new Message("imageCreate", newImg.alias, newImg);
   result.url = newImg.href();
   return result;
}


/**
 * delete an image
 * @param Obj Image-Object to delete
 * @return String Message indicating success or failure
 */
function deleteImage(imgObj) {
   // first remove the image from disk (and the thumbnail, if existing)
   var dir = imgObj.site ? imgObj.site.getStaticDir("images") : imgObj.layout.getStaticDir();
   var f = new Helma.File(dir, imgObj.filename);
   f.remove();
   if (imgObj.thumbnail) {
      var thumb = imgObj.thumbnail;
      f = new Helma.File(dir, thumb.filename);
      f.remove();
      thumb.remove();
   }
   // then, remove the image-object
   imgObj.remove();
   return new Message("imageDelete");
}


/**
 * function deletes all images
 */
function deleteAll() {
   for (var i=this.size();i>0;i--)
      this.deleteImage(this.get(i-1));
   return true;
}
