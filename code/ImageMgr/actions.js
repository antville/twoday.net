
/**
 * display all images of a site or layout
 */
function main_action() {
   res.data.imagelist = renderList(this, "mgrlistitem", 20, req.data.page);
   res.data.pagenavigation = renderPageNavigation(this, this.href(), 20, req.data.page);
   res.data.title = getMessage("ImageMgr.main.title", {parentTitle: this._parent.title});
   res.data.body = this.renderSkinAsString("main");
   res.handlers.context.renderPage()
   return;
}


/**
 * display my images
 */
function myimages_action() {
   var ms = this._parent.members.get(session.user._id.toString());
   res.data.imagelist = renderList(ms ? ms.images : [], "mgrlistitem", 20, req.data.page);
   res.data.pagenavigation = renderPageNavigation(ms ? ms.images : [], this.href(req.action), 20, req.data.page);
   res.data.title = getMessage("ImageMgr.myimage.title", {parentTitle: this._parent.title});
   res.data.body = this.renderSkinAsString("main");
   res.handlers.context.renderPage()
   return;
}


/**
 * action for creating new Image objects
 */
function create_action() {
   if (req.data.cancel) {
      res.redirect(this.href());
   } else if (req.data.save) {
      // check if a url has been passed instead of a file upload. hw
      if ((!req.data.rawimage || req.data.rawimage.contentLength == 0) && req.data.url)
          req.data.rawimage = getURL(req.data.url);
      try {
         checkSecretKey();
         var result = this.evalImg(req.data, session.user);
         res.message = result.toString();
         session.data.referrer = null;
         // XP webpublishingwizard would be confused if we redirect here and expects us just to return.
         if (req.data.webpublishingwizard) return;
         if (req.data.alias == "icon" || req.data.alias == "favicon")
            res.redirect(res.handlers.site.href("manage"));
         res.redirect(this.href());
      } catch (err) {
         res.message = err.toString();
         if (req.data.publishingwizard) res.redirect(this.href()); // don't just return but redirect anywhere else to signalize an occoured error to the wizard.
      }
   }

   res.data.action = this.href(req.action);
   res.data.title = getMessage("ImageMgr.create.title", {parentTitle: this._parent.title});
   res.data.body = this.renderSkinAsString("new");
   res.handlers.context.renderPage();
   return;
}


/**
 * action for changing the weblog icon
 */
function changeicon_action() {
   if (req.data.cancel) {
      res.redirect(this.href());
   }

   res.data.action = this.href("create");
   res.data.title = getMessage("ImageMgr.changeicon.title", {parentTitle: this._parent.title});
   res.data.body = this.renderSkinAsString("changeicon");
   res.data.sidebar02 = this.renderSkinAsString("changeiconSidebar");
   this.renderMgrPage();
   return;
}
