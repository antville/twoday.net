
/**
 * Renders alias of this file
 *
 * @param as        optional as="editor" will render an input field <br />
 *                  as="link" will render a link
 * @doclevel public
 */
function alias_macro(param) {
   if (param.as == "editor") {
      Html.input(this.createInputParam("alias", param));
   } else if (param.as == "link") {
      param.to = ""
      param.title = encodeForm(this.description);
      delete(param.as);
      Html.openTag("a", this.createLinkParam(param));
      res.write(this.alias);
      Html.closeTag("a");
   } else {
      res.write(this.alias);
   }
   return;
}


/**
 * Renders description of this file
 *
 * @param as        optional as="editor" will render a textarea
 * @doclevel public
 */
function description_macro(param) {
   if (param.as == "editor")
      Html.textArea(this.createInputParam("description", param));
   else if (this.description)
      res.write(this.description);
   return;
}


/**
 * Renders the URL of this file
 *
 * @doclevel public
 */
function url_macro() {
   return this.getUrl();
}


/**
 * Renders a link for editing a file
 *
 * @param text  optional link text, if you don't want to use the default ("locale.generic.edit")
 * @doclevel admin
 */
function editlink_macro(param) {
   if (session.user) {
      try {
         this.checkEdit(session.user, res.meta.memberlevel);
      } catch (deny) {
         return;
      }
      Html.link({href: this.href("edit")}, param.text ? param.text : getMessage("generic.edit"));
   }
   return;
}


/**
 * Renders a link to the delete action,
 * if user is allowed to delete this file (is creator of this file)
 *
 * @param text   optional link text, if you don't want to use the default ("locale.generic.delete")
 * @param image  DEPRECATED; Renders an site.image, if present, instead of the text
 * @doclevel admin
 */
function deletelink_macro(param) {
   if (session.user) {
      try {
         this.checkEdit(session.user, res.meta.memberlevel);
      } catch (deny) {
         return;
      }
      Html.openLink({href: this.href("delete")});
      if (param.image && this.site.images.get(param.image))
         renderImage(this.site.images.get(param.image), param);
      else
         res.write(param.text ? param.text : getMessage("generic.delete"));
      Html.closeLink();
   }
   return;
}


/**
 * macro rendering a link to view the file
 */
function viewlink_macro(param) {
   if (session.user) {
      param.to = ""
      param.title = encodeForm(this.description);
      Html.openTag("a", this.createLinkParam(param));
      res.write(param.text ? param.text : getMessage("generic.view"));
      Html.closeTag("a");
   }
   return;
}


/**
 * macro rendering filesize
 */
function filesize_macro(param) {
   if (param.as == "bytes") {
      res.write(this.filesize);
   } else if (!param.as || param.as == "kilobytes") {
      res.write((this.filesize / 1024).format("###,###") + " KB");
   }
   return;
}


/**
 * macro rendering the mimetype
 */
function mimetype_macro(param) {
   res.write(this.mimetype);
   return;
}


/**
 * macro rendering the file extension from the name
 */
function filetype_macro(param) {
   var i = this.filename.lastIndexOf(".");
   if (i > -1)
      res.write(this.filename.substring(i+1, this.filename.length));
   return;
}


/**
 * macro rendering the number of requests so far
 * for a file-object
 */
function clicks_macro(param) {
   if (!this.requestcnt)
      res.write(param.no ? param.no : getMessage("File.downloads.no"));
   else if (this.requestcnt == 1)
      res.write(param.one ? param.one : getMessage("File.downloads.one"));
   else {
      res.write(this.requestcnt);
      res.write(param.more ? param.more : " " + getMessage("File.downloads.more"));
   }
   return;
}


/**
 * renders Task Items
 */
function tasks_macro(param) {
   var items = [];

   // check rights
   var mayView = true;
   var mayEdit = true;
   var mayDelete = true;
   try { this.site.checkView(session.user, res.meta.memberlevel); } catch (deny) { mayView = false; }
   try { this.checkEdit(session.user, res.meta.memberlevel); } catch (deny) { mayEdit = false; }
   try { this.checkDelete(session.user, res.meta.memberlevel); } catch (deny) { mayDelete = false; }

   // build task List
   if (mayView) items.push({href: this.getUrl(), text: getMessage("generic.view")});
   if (mayEdit) items.push({href: this.href("edit"), text: getMessage("generic.edit")});
   if (mayDelete) items.push({href: this.href("delete"), text: getMessage("generic.delete")});
   var modItems = this.applyModuleMethods("renderFileTasks", [], false, false);
   if (modItems) items = items.concat(modItems);

   // render task list
   res.push();
   this._parent.renderTaskItems(items, param);
   param.items = res.pop();

   this._parent.renderSkin("taskContainer", param);
   return;
}
