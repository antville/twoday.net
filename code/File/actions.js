
/**
 * main action
 * directly writes File to the Response
 *
 * @doclevel public
 */
function main_action() {
   if (this.creator != session.user) this.requestcnt++;
   if (this.site.online) {
      res.redirect(this.site.getStaticUrl("files/") + this.filename);
   } else {
      res.contentType = this.mimetype;
      res.forward(this.site.alias + Helma.File.separator + "files" + Helma.File.separator + this.filename);
   }
   return;
}


/**
 * Renders an edit form, and saves its data
 *
 * @req cancel  Any value will cancel this action and redirect to this _parents main action. In most cases the name of the cancel button is set to "cancel".
 * @req save    Any value will cause this action to save the form data to this file. In most cases the name of the submit button is set to "save".
 * @skin File.edit      Form for adding a comment / reply
 * @see File.evalFile   This will handle the form data, and add a reply
 * @doclevel public
 */
function edit_action() {
   if (req.data.cancel)
      res.redirect(this.site.files.href());
   else if (req.data.save) {
      checkSecretKey();
      res.message = this.evalFile(req.data, session.user);
      res.redirect(this._parent.href());
   }

   res.data.action = this.href(req.action);
   res.data.title = getMessage("File.edit.title", {fileAlias: this.alias});
   res.data.body = this.renderSkinAsString("edit");
   this.site.renderPage();
   return;
}


/**
 * After asking the user, if she is sure, this action will delete this file.
 *
 * @req cancel  Any value will cancel this action and redirect to this comments story. In most cases the name of the cancel button is set to "cancel".
 * @req remove  Any value will cause this action to delete this comment. In most cases the name of the submit button is set to "remove".
 * @skin HopObject.delete
 * @see File.deleteFile
 * @doclevel public
 */
function delete_action() {
   if (req.data.cancel)
      res.redirect(this.site.files.href());
   else if (req.data.remove) {
      try {
         checkSecretKey();
         var url = this._parent.href();
         res.message = this.site.files.deleteFile(this);
         res.redirect(url);
      } catch (err) {
         res.message = err.toString();
      }
   }

   res.data.action = this.href(req.action);
   res.data.title = getMessage("File.delete.title", {fileAlias: this.alias});
   var skinParam = {
      description: getMessage("File.deleteDescription"),
      detail: this.alias
   };
   res.data.body = this.renderSkinAsString("delete", skinParam);
   this.site.renderPage();
   return;
}
