
/**
 * Permission checks (called by hopobject.onRequest())
 *
 * @param action  String, name of action
 * @param usr     User, who wants to access an action
 * @param level   Int, Membership level
 * @return Object if permission was denied <br />
 *   .message  message, if something went wrong <br />
 *   .url      recommanded redirect URL
 * @doclevel public
 */
function checkAccess(action, usr, level) {
   if (usr && usr.sysadmin) return;
   var url;
   var site = this.site;
   try {
      if (site && !site.online) {
         checkIfLoggedIn(this.href(action));
         site.checkView(usr, level);
      }
      switch (action) {
         case "main" :
            break;
         case "edit" :
            checkIfLoggedIn(this.href(req.action));
            this.checkEdit(usr, level);
            break;
         case "delete" :
            checkIfLoggedIn(this.href(req.action));
            this.checkDelete(usr, level);
            break;
         default :
            url = this.applyAllModuleMethods("checkFileAccess", [action, usr, level, url]);
            break;
      }
   } catch (deny) {
      return {message: deny.toString(), url: (url ? url : (site ? site.href() : root.href()))};
   }
   return;
}


/**
 * check if user is allowed to edit a file
 * @param Obj Userobject
 * @param Int Permission-Level
 * @return String Reason for denial (or null if allowed)
 */
function checkEdit(usr, level) {
   if (this.creator != usr && (level & MEMBER_MAY_EDIT_ANYFILE) == 0)
      throw new DenyException("fileEdit");
   return;
}


/**
 * check if user is allowed to delete a file
 * @param Obj Userobject
 * @param Int Permission-Level
 * @return String Reason for denial (or null if allowed)
 */
function checkDelete(usr, level) {
   if (this.creator != usr && (level & MEMBER_MAY_DELETE_ANYIMAGE) == 0)
      throw new DenyException("fileDelete");
   return;
}
