
/**
 * Permission checks (called by hopobject.onRequest())
 *
 * @param action  String, name of action
 * @param usr     User, who wants to access an action
 * @param level   Int, Membership level
 * @return Object if permission was denied <br />
 *   .message  message, if something went wrong <br />
 *   .url      recommanded redirect URL
 * @doclevel public
 */
function checkAccess(action, usr, level) {
   if (usr && usr.sysadmin) return;
   var url;
   var site = this.poll.site;
   try {
      if (site && !site.online) {
         checkIfLoggedIn(this.href(action));
         site.checkView(usr, level);
      }
      switch (action) {
         case "edit" :
            checkIfLoggedIn(this.href(action));
            this.checkEdit(usr, level);
            break;
         case "delete" :
            checkIfLoggedIn(this.href(action));
            this.checkDelete(usr, level);
            break;
         case "results" :
            this.site.checkView(usr, level);
            break;
         case "toggle" :
            checkIfLoggedIn(this.href(action));
            this.checkDelete(usr, level);
            break;
         default :
            url = this.applyAllModuleMethods("checkChoiceAccess", [action, usr, level, url]);
            break;
      }
   } catch (deny) {
      return {message: deny.toString(), url: (url ? url : (site ? site.href() : root.href()))};
   }
   return;
}
