
/**
 * Constructor function for Choice objects, which is an item of Poll.
 * This will create a Choice Object and set this.createtime to the current time
 * <code>Poll &lt;- Choice &lt;- Vote -&gt; User</code>
 *
 * @param title   String, the text of the poll's choice
 * @see Poll
 * @doclevel public
 */
function constructor(title) {
   this.title = title;
   this.createtime = this.modifytime = new Date();
}


/**
 * Removes all votes from a choice
 * @doclevel public
 */
function deleteAll() {
   for (var i=this.size();i>0;i--)
      this.get(i-1).remove();
   return true;
}
