/**
 * Renders the title of a choice, either as editor or as plain text
 *
 * @param as        optional as="editor" will render an iinput field
 * @doclevel public
 */
function title_macro(param) {
   if (param.as === "editor")
      Html.input(this.createInputParam("title", param));
   else
      res.write(this.title);
   return;
}
