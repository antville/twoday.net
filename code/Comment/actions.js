
/**
 * Since comments don't have their own page, we redirect to
 * story together with an anchor to this comment
 *
 * @overrides Story.main_action
 * @doclevel public
 */
function main_action() {
   // since comments don't have their own page, we redirect to
   // story together with an anchor to this comment
   res.redirect(this.story.href() + "#" + this._id);
   return;
}


/**
 * Renders a comment(=reply) form on a comment, and adds it as a reply to this comment.
 *
 * @req cancel  Any value will cancel this action and redirect to this comments story. In most cases the name of the cancel button is set to "cancel".
 * @req save    Any value will cause this action to save the form data, and add a reply. In most cases the name of the submit button is set to "save".
 * @skin Comment.edit         Form for adding a comment / reply
 * @see Story.evalComment   This will handle the form data, and add a reply
 * @overrides Story.comment_action
 * @doclevel public
 */
function comment_action() {
   // restore any rescued text
   if (session.data.rescuedText)
      restoreRescuedText();

   if (req.data.cancel)
      res.redirect(this.story.href());
   else if (req.data.save) {
      try {
         checkSecretKey();
         var result = this.evalComment(req.data, session.user);
         res.message = result.toString();
         res.redirect(this.story.href() + "#" + result.id);
      } catch (err) {
         res.message = err.toString();
         app.log("COMMENT BUG? " + uneval(err));
      }
   }

   res.data.action = this.href(req.action);
   res.data.title = this.site.title;
   if (this.story.title)
      res.data.title += " - " + encode(this.story.title);
   res.data.action = this.href("comment");
   res.data.body = this.renderSkinAsString("comment");
   this.site.renderPage();
   return;
}


/**
 * Renders a comment(=reply) form, and updates this comment.
 *
 * @req cancel  Any value will cancel this action and redirect to this comments story. In most cases the name of the cancel button is set to "cancel".
 * @req save    Any value will cause this action to save the form data, and add a reply. In most cases the name of the submit button is set to "save".
 * @skin Comment.edit          Form for editing a comment / reply
 * @see Comment.updateComment  This will store the form data
 * @overrides Story.edit_action
 * @doclevel public
 */
function edit_action() {
   // restore any rescued text
   if (session.data.rescuedText)
      restoreRescuedText();

   if (req.data.cancel)
      res.redirect(this.story.href());
   else if (req.data.save) {
      try {
         checkSecretKey();
         res.message = this.updateComment(req.data);
         res.redirect(this.story.href() + "#" + this._id);
      } catch (err) {
         res.message = err.toString();
      }
   }

   res.data.action = this.href(req.action);
   res.data.title = this.site.title;
   if (this.story.title)
      res.data.title += " - " + encode(this.story.title);
   res.data.body = this.renderSkinAsString("edit");
   this.site.renderPage();
   return;
}


/**
 * After asking the user, if she is sure, this action will delete this comment.
 *
 * @req cancel  Any value will cancel this action and redirect to this comments story. In most cases the name of the cancel button is set to "cancel".
 * @req remove  Any value will cause this action to delete this comment. In most cases the name of the submit button is set to "remove".
 * @skin HopObject.delete
 * @see Story.deleteComment
 * @overrides Story.delete_action
 * @doclevel public
 */
function delete_action() {
   if (req.data.cancel)
      res.redirect(this.story.href());
   else if (req.data.remove) {
      try {
         checkSecretKey();
         var url = this.story.href();
         res.message = this.story.deleteComment(this);
         res.redirect(url);
      } catch (err) {
         res.message = err.toString();
      }
   }

   res.data.action = this.href(req.action);
   res.data.title = this.site.title;
   var skinParam = {
      description: getMessage("Comment.deleteDescription"),
      detail: this.creator ? this.creator.name : this.creatorName
   };
   res.data.body = this.renderSkinAsString("delete", skinParam);
   this.site.renderPage();
   return;
}
