
/**
 * Permission checks (called by hopobject.onRequest())
 *
 * @param action  String, name of action
 * @param usr     User, who wants to access an action
 * @param level   Int, Membership level
 * @return Object if permission was denied <br />
 *   .message  message, if something went wrong <br />
 *   .url      recommanded redirect URL
 * @doclevel public
 */
function checkAccess(action, usr, level) {
   if (usr && usr.sysadmin) return;
   var url;
   var site = this.story.site;
   try {
      if (site && !site.online) {
         checkIfLoggedIn(this.href(action));
         site.checkView(usr, level);
      }
      switch (action) {
         case "main" :
            break;
         case "edit" :
            if (!usr && req.data.save)
               rescueText(req.data);
            if (this.story.discussions &&
                this.site.preferences.getProperty("discussionsby") != "anonymous")
               checkIfLoggedIn(this.href(req.action));
            this.checkEdit(usr, level);
            break;
         case "delete" :
            checkIfLoggedIn(this.href(action));
            this.checkDelete(usr, level);
            break;
         case "comment" :
            if (!usr && req.data.save)
               rescueText(req.data);
            if (this.site.preferences.getProperty("discussionsby") != "anonymous")
               checkIfLoggedIn(this.href(req.action));
            this.checkComment(usr, level);
            break;
         default :
            url = this.applyAllModuleMethods("checkCommentAccess", [action, usr, level, url]);
            break;
      }
   } catch (deny) {
      return {message: deny.toString(), url: (url ? url : (site ? site.href() : root.href()))};
   }
   return;
}


/**
 * check if user is allowed to edit a comment
 * @param usr     User, object
 * @param level   Int, Permission-Level
 * @return String Reason for denial (or null if allowed)
 * @throws DenyException
 * @doclevel public
 */
function checkEdit(usr, level) {
   if (usr == null || (this.creator != usr && (level & MEMBER_MAY_EDIT_ANYCOMMENT) == 0))
      throw new DenyException("commentEdit");
   return;
}


/**
 * check if user is allowed to post a comment to this comment
 * just a wrapper to the story checkComment
 * @param Obj Userobject
 * @param Int Permission-Level
 * @return String Reason for denial (or null if allowed)
 */
function checkComment(usr, level) {
   this.story.checkComment(usr, level);
   return;
}

