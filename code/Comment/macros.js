
/**
 * Renders a link to reply to a comment
 *
 * @example
 *   <!-- Comment Links -->
 *   <div class="commentLinks">
 *     <% comment.replylink anchor="form" %>
 *     <% comment.editlink prefix=" - " %>
 *     <% comment.deletelink prefix=" - " %>
 *   </div>
 * @param anchor  optional anchor, that will be added to the link
 * @param image   DEPRECATED, name a site image to be used instead of a text
 * @param text    you may override the default text (locale.Comment.reply) of the replylink
 * @doclevel public
 */
function replylink_macro(param) {
   if (!this.online || req.action != "main")
      return;
   try {
      this.story.checkComment(session.user, res.meta.memberlevel);
      var href = (this.parent) ? this.parent.href("comment") : this.href("comment");
      Html.openLink({href: href + (param.anchor ? "#" + param.anchor : "")});
      if (param.image && this.site.images.get(param.image))
         renderImage(this.site.images.get(param.image), param);
      else
      if (!param.image)
         res.write(param.text ? param.text : getMessage("Comment.reply"));
      Html.closeLink();
   } catch (err) {}
   return;
}


/**
 * Renders the url of this comment.
 * A comment doesn't have its own page, so it is just an anchor on its stories page.
 *
 * @doclevel public
 */
function url_macro() {
   res.write(this.story.href() + "#" + this._id);
   return;
}


/**
 * Overwrites existing Comment.creator_macro in order to display the Anonymous Creator
 */
function creator_macro(param) {
   if (this.creatorName || this.modMtImExportCreatorName) {
      var url = this.creatorUrl;
      var prefix = getMessage("Text.anonymousCreator.prefix");
      if (prefix) prefix = prefix + " ";
      var suffix = getMessage("Text.anonymousCreator.suffix");
      if (suffix) suffix = " " + suffix;
      var name = this.creatorName ? this.creatorName : this.modMtImExportCreatorName;
      var text = prefix + name + suffix;
      if (param.as == "link" && url)
         Html.link({href: url}, text);
      else if (param.as == "url")
         res.write(url);
      else
         res.write(text);
   } else if (this.creator) {
      HopObject.prototype.creator_macro.apply(this, [param]);
   }
   return;
}


/**
 * Macro Hook in Comment/edit.skin
 *
 * @hook renderCommentEdit
 */
function hookCommentEdit_macro(param) {
   this.applyModuleMethods("renderCommentEdit", param);
   return;
}


/**
 * Macro Hook in Comment/edit.skin
 *
 * @hook renderCommentEdit
 */
function hookCommentLinks_macro(param) {
   this.applyModuleMethods("renderCommentLinks", param);
   return;
}
