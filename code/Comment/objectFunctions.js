
/**
 * Constructor function for comment objects.
 * This will set this comment to online, editableby admins and the createtime and -user.
 *
 * @param site       Site, the comment belongs to
 * @param creator    User, who creates this comment
 * @doclevel public
 */
function constructor(site, creator) {
   this.site = site;
   this.online = 1;
   this.editableby = EDITABLEBY_ADMINS;
   this.creator = creator;
   this.createtime = this.modifytime = new Date();
}


/**
 * Evaluates changes to posting. This function will also update the RSS feed(s).
 *
 * @param param Object containing the properties needed for creating a reply
 *    .http_remotehost   IP adress of the editing client
 * @return Message update
 * @throws Exception textMissing, if the content couldn't be evaluated
 * @doclevel public
 */
function updateComment(param) {
   // collect content
   var origContent = this.content.getAll();
   origContent.title = this.title;
   origContent.text  = this.text;
   var content = extractContent(param, origContent);
   if (!content.exists)
      throw new Exception("textMissing");
   this.checkContentSize(content.value);
   // sanitize user input
   for (var i in content.value) {
      content.value[i] = sanitize(content.value[i], app.properties.allowedTagsForComment.split(","));
   }
   this.setContent(content.value);
   if (content.isMajorUpdate) {
      this.modifytime = new Date();
   }
   this.site.lastupdate = new Date();
   // update affected static RSS Feed
   this.site.updateStaticRSSFeed("comments");
   return new Message("update");
}

/**
 * additional functionality that is prototype specific
 */
function addToCommentCollection(c) {
   // also add to story.comments since it has
   // cachemode set to aggressive and wouldn't refetch
   // its child collection index otherwise
   var ret = this.story.comments.add(c);
}

