/**
 * render a link to image-edit
 * calls image.editlink_macro, but only
 * if the layout in path is the one this image
 * belongs to
 */
function editlink_macro(param) {
   if (path.Layout == this.layout)
      Image.prototype.editlink_macro.apply(this, [param]);
   return;
}


/**
 * render a link to delete action
 * calls image.deletelink_macro, but only
 * if the layout in path is the one this image
 * belongs to
 */
function deletelink_macro(param) {
   if (path.Layout == this.layout)
      Image.prototype.deletelink_macro.apply(this, [param]);
   return;
}


/**
 * render a link for replacing this image
 * if the image is ...
 */
function replacelink_macro(param) {
   if (path.layout != this.layout) {
      if (session.user) {
         try {
            path.layout.images.checkAdd(session.user, res.meta.memberlevel);
         } catch (deny) {
            return;
         }
         Html.openLink({href: path.layout.images.href("create") + "?alias=" + this.alias + "&width=" + this.width + "&height=" + this.height + "&resizeto=crop"});
         if (param.image && this.site.images.get(param.image))
            renderImage(this.site.images.get(param.image), param);
         else
            res.write(param.text ? param.text : getMessage("generic.replace"));
         Html.closeLink();
      }
      return;
   }
   return;
}


/**
 * renders Task Items
 */
function tasks_macro(param) {
   var items = [];

   var imgMgr = path.layout.images;
   // check rights
   var mayEdit = true;
   var mayDelete = true;
   var mayReplace = true;
   if (path.layout != this.layout)
      mayEdit = false;
   else
      try { this.checkEdit(session.user, res.meta.memberlevel); } catch (deny) { mayEdit = false; }
   if (path.layout != this.layout)
      mayDelete = false;
   else
      try { this.checkDelete(session.user, res.meta.memberlevel); } catch (deny) { mayDelete = false; }
   if (path.layout == this.layout)
      try { this.checkEdit(session.user, res.meta.memberlevel); } catch (deny) { mayReplace = false; }
   else
      try { path.layout.images.checkAdd(session.user, res.meta.memberlevel); } catch (deny) { mayReplace = false; }

   // build task List
   if (mayEdit) items.push({href: this.href("edit"), text: getMessage("generic.edit")});
   if (mayDelete) items.push({href: this.href("delete"), text: getMessage("generic.delete")});
   if (mayReplace) {
      if (path.layout != this.layout)
         items.push({href: path.layout.images.href("create") + "?alias=" + this.alias + "&width=" + this.width + "&height=" + this.height + "&resizeto=crop", text: getMessage("generic.replace")});
      else
         items.push({href: this.href("replace"), text: getMessage("generic.replace")});
   }

   var modItems = this.applyModuleMethods("renderLayoutImageTasks", [], false, false);
   if (modItems) items = items.concat(modItems);

   // render task list
   res.push();
   this._parent.renderTaskItems(items, param);
   param.items = res.pop();

   this._parent.renderSkin("taskContainer", param);
   return;
}
