
/**
 * Permission checks (called by hopobject.onRequest())
 *
 * @param action  String, name of action
 * @param usr     User, who wants to access an action
 * @param level   Int, Membership level
 * @return Object if permission was denied <br />
 *   .message  message, if something went wrong <br />
 *   .url      recommanded redirect URL
 * @doclevel public
 */
function checkAccess(action, usr, level) {
   if (usr && usr.sysadmin) return;
   var url;
   var site = this._parent._parent.site ? this._parent._parent.site : null;
   try {
      if (site && !site.online) {
         checkIfLoggedIn(this.href(action));
         site.checkView(usr, level);
      }
      switch (action) {
         case "main" :
         case "thumbnail" :
            checkIfLoggedIn(this.href(req.action));
            this.checkView(usr, level);
            break;
         case "info" :
         case "edit" :
            checkIfLoggedIn(this.href(req.action));
            this.checkEdit(usr, level);
            break;
         case "delete" :
         case "replace" :
            checkIfLoggedIn(this.href(action));
            this.checkDelete(usr, level);
            break;
         default :
            url = this.applyAllModuleMethods("checkLayoutImageAccess", [action, usr, level, url]);
            break;
      }
   } catch (deny) {
      return {message: deny.toString(), url: (url ? url : (site ? site.href() : root.href()))};
   }
   return;
}


/**
 * check if user is allowed to edit a layout image
 * (overwrites Image.checkEdit())
 * @see Image/securityFunctions.js
 * @param Obj Userobject
 * @param Int Permission-Level
 * @return Obj Exception or null (if allowed)
 */
function checkEdit(usr, level) {
   this.layout.images.checkAdd(usr, level);
   return;
}


/**
 * check if user is allowed to delete a Image
 * (overwrites Image.checkEdit())
 * @see Image/securityFunctions.js
 * @param Obj Userobject
 * @param Int Permission-Level
 * @return Obj Exception or null (if allowed)
 */
function checkDelete(usr, level) {
   this.layout.images.checkAdd(usr, level);
   return;
}
