
/**
 * return the url of the layout image (overwrites
 * getUrl() of image)
 * @see image/objectFunctions.js
 */
function getUrl() {
   res.push();
   if ((this.layout.site && this.layout.site.online) || (!this.layout.site)) {
      this.layout.staticUrl();
      res.write(this.filename);
   } else {
      res.write(this.layout.images.href() + this.alias);
   }
   return res.pop();
}


/**
 * return the image file on disk
 * @return Object File object
 */
function getFile() {
   return new Helma.File(this.layout.getStaticPath(), this.filename);
}
