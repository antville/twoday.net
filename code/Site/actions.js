
/**
 * main action
 */
function main_action() {
   if (this.allstories.size() == 0) {
      res.data.body = this.renderSkinAsString("welcome");
      if (session.user) {
         if (session.user == this.creator)
            res.data.body += this.renderSkinAsString("welcomeowner");
         if (session.user.sysadmin)
            res.data.body += this.renderSkinAsString("welcomesysadmin");
      }
   } else {
      this.renderStorylist(req.data.day);
      res.data.body = this.renderSkinAsString("main");
   }
   res.data.title = this.title;
   this.renderPage();
   logAccess();
}


/**
 * edit action
 */
function edit_action() {
   res.handlers.mgr = this.stories;
   if (req.data.cancel)
      res.redirect(this.href());
   else if (req.data.save) {
      try {
         checkSecretKey();
         res.message = this.evalPreferences(req.data, session.user);
         res.redirect(this.href("edit"));
      } catch (err) {
         res.message = err.toString();
      }
   }

   res.data.action = this.href(req.action);
   res.data.title = getMessage("Site.edit.title", {siteTitle: this.title});
   res.data.body = this.renderSkinAsString("edit");
   res.data.sidebar01 = this.renderSkinAsString("editSidebar");
   if (session.user.sysadmin || (session.user === this.creator))
      res.data.sidebar01 += this.renderSkinAsString("editSidebarCreator");
   this.stories.renderMgrPage("Site");
   return;
}


/**
 * spamfilter action
 */
function spamfilter_action() {
   res.handlers.mgr = this.stories;
   if (req.data.cancel)
      res.redirect(this.href());
   else if (req.data.save) {
      try {
         checkSecretKey();
         res.message = this.evalSpamfilter(req.data, session.user);
         res.redirect(this.href(req.action));
      } catch (err) {
         res.message = err.toString();
      }
   } else if (req.data.permanent || req.data.permanent_array) {
      var urls = req.data.permanent_array ?
                 req.data.permanent_array : [req.data.permanent];
      res.push();
      res.write(this.preferences.getProperty("spamfilter"));
      for (var i in urls) {
         res.write("\n");
         res.write(urls[i]);
      }
      this.preferences.setProperty("spamfilter", res.pop());
      res.redirect(this.href(req.action));
      return;
   }

   res.data.action = this.href(req.action);
   res.data.title = getMessage("Site.spamfilter.title", {siteTitle: this.title});
   res.data.body = this.renderSkinAsString("spamfilter");
   res.data.sidebar01 = this.renderSkinAsString("spamfilterSidebar");
   this.stories.renderMgrPage("Site");
   return;
}


/**
 * manage action
 * renders the admin manager start page
 */
function manage_action() {
   res.handlers.mgr = this.stories;
   res.data.title = getMessage("Site.manage.title", {siteTitle: this.title});
   res.data.body = this.renderSkinAsString("manage");
   res.data.sidebar01 = this.renderSkinAsString("manageSidebar");
   this.stories.renderMgrPage("Site");
   return;
}


/**
 * contribute action
 * renders the contribute manager start page
 */
function contribute_action() {
   res.handlers.mgr = this.stories;
   res.data.title = getMessage("Site.contribute.title", {siteTitle: this.title});
   res.data.body = this.renderSkinAsString("contribute");
   res.data.sidebar01 = this.renderSkinAsString("contributeSidebar");
   this.renderPage();
   return;
}


/**
 * wrapper to access colorpicker also from site
 */
function colorpicker_action() {
   res.handlers.site = this;
   root.colorpicker_action();
   return;
}


/**
 * wrapper to make style.skin public
 */
function main_css_action() {
   res.setLastModified(res.handlers.layout.skins.getSkinLastModified("Site", "style"));
   res.contentType = "text/css";
   this.renderSkin("style");
}


/**
 * wrapper to make javascript.skin public
 */
function main_js_action() {
   res.setLastModified(res.handlers.layout.skins.getSkinLastModified("Site", "javascript"));
   res.contentType = "text/javascript";
   this.renderSkin("javascript");
   root.renderSkin("systemscripts");
}


/**
 * RSS 1.0 Feed
 */
function rss_action() {
   this.writeRSSFeedToResponse();
   return;
}


/**
 * RSS 1.0 Feed
 */
function index_rdf_action() {
   this.writeRSSFeedToResponse();
   return;
}


/**
 * RSS 1.0 Summary Feed
 */
function summary_rdf_action() {
   this.writeRSSFeedToResponse("summary");
   return;
}


/**
 * RSS 1.0 Comments Feed
 * This one is broken because it serves comments and stories
 */
function comments_rdf_action() {
   this.writeRSSFeedToResponse("comments");
   return;
}

/**
 * RSS 1.0 latest Comments Feed
 */
function latestcomments_rdf_action() {
   this.writeRSSFeedToResponse("latestcomments");
   return;
}

/**
 * RSS 1.0 Last modified Feed
 */
function lastmodified_rdf_action() {
   this.writeRSSFeedToResponse("comments");
   return;
}

/**
 * Redirect to correct location of RSS Feed
 */
function rss_xml_action() {
   res.redirect(this.href("index.rdf"));
   return;
}


/**
 * Redirect to correct location of RSS Feed
 */
function index_xml_action() {
   res.redirect(this.href("index.rdf"));
   return;
}


/**
 * Style-sheet for Xml.
 */
function rss2html_xsl_action() {
   res.setLastModified(res.handlers.layout.skins.getSkinLastModified("Site", "rss2html.xsl"));
   res.contentType = "text/xsl";
   this.renderSkin("rss2html.xsl");
}


/**
 * this action redirects to File/main_action
 * kept for backwards compatibility
 * @deprecated 08.11.2004
 */
function getfile_action() {
   var f = this.files.get(req.data.name);
   if (f) res.redirect(f.href());
   return;
}


/**
 * most read stories of a site
 */
function mostread_action() {
   res.handlers.mgr = this.stories;
   var mostReadIsPrivate = this.preferences.getProperty("mostReadIsPrivate");
   res.data.title = getMessage("Site.mostRead.title");
   res.data.sidebar01 = this.renderSkinAsString("mostReadSidebar");
   res.data.body = this.renderSkinAsString("mostread");
   if (mostReadIsPrivate == "1") {
      this.stories.renderMgrPage("Site");
   } else {
      this.renderPage();
   }
   return;
}


/**
 * referrers of a site
 */
function referrers_action() {
   if (req.data.permanent && session.user) {
      try {
         checkSecretKey();
         // FIXME: unfortunately, the check* methods are
         // not very handy, anymore... (need try/catch block)
         this.checkEdit(session.user, res.meta.memberlevel);
      } catch (err) {
         res.message = err.toString();
         res.redirect(this.href());
         return;
      }
      var urls = req.data.permanent_array ?
                 req.data.permanent_array : [req.data.permanent];
      res.push();
      res.write(this.preferences.getProperty("spamfilter"));
      for (var i in urls) {
         res.write("\n");
         res.write(urls[i]);
      }
      this.preferences.setProperty("spamfilter", res.pop());
      res.redirect(this.href(req.action));
      return;
   }

   var referrersIsPrivate = this.preferences.getProperty("referrersIsPrivate");
   res.data.action = this.href("referrers");
   res.data.title = getMessage("Site.referrers.title");
   res.data.sidebar01 = getMessage("Site.referrers.sidebar01");
   res.data.body = this.renderSkinAsString("referrers");
   if (referrersIsPrivate == "1") {
      res.handlers.mgr = this.stories;
      this.stories.renderMgrPage("Site");
   } else {
      this.renderPage();
   }
   return;
}


/**
 * search action
 */
function search_action() {
   res.data.action = this.href(req.action);
   res.data.title = "Search " + this.title;
   res.data.body = this.renderSkinAsString("searchform");

   // do not allow Bots to perform (CPU intensive) search requests
   var userAgent = req.data.http_browser ? req.data.http_browser.toLowerCase() : "";
   var deniedAgents = ["google", "slurp"];
   if (req.data.q) {
      for (var i in deniedAgents) {
         if (userAgent.contains(deniedAgents[i])) req.data.q = null;
      }
   }

   if (req.data.q) {
      var query = stripTags(req.data.q);
      // array with sites to search
      var sites = new Array (this);
      var blogIDs = this.applyModuleMethods("searchblogIds", [], false, false);
      blogIDs = (blogIDs || this._id);
      var result = root.searchSites (query, blogIDs);
      var found = result.length;
      if (found == 0)
         res.data.body += getMessage("error.searchNothingFound", query);
      else {
         var start = 0;
         var end = found;
         var itemsPerPage = 10;

         if (found == 1)
            res.data.body += getMessage("confirm.resultOne", query);
         else if (found <= itemsPerPage)
            res.data.body += getMessage("confirm.resultMany", [encodeForm(query), found]);
         else {
            if (req.data.start)
               start = Math.min(found-1, parseInt (req.data.start));
            if (isNaN(start))
               start = 0;
            end = Math.min(found, start+itemsPerPage);
            res.data.body += getMessage("confirm.resultMany", [encodeForm(query), found]);
            res.data.body += " " + getMessage("confirm.resultDisplay", [start+1, end]);
         }

         res.data.body += "<br />";

         // note: I'm doing this without a "searchbody" skin, since
         // I think there's not much need to customize the body of
         // search results, esp. since its parts are fully customizable.
         // of course, I may be wrong about that.

         // render prev links, if necessary
         if (start > 0) {
            var sp = new Object();
            sp.url = this.href() + "search?q=" + (encodeURIComponent(query)) + "&start=" + Math.max(0, start-itemsPerPage);
            sp.text = "previous results";
            res.data.body += "<br /><br />" + renderSkinAsString("prevpagelink", sp);
         }

         // render result
         for (var i=start; i<end; i++) {
            var site = root.get(result[i].sitealias);
            var item = site.allcontent.get(result[i].sid);
            if (item)
               res.data.body += item.renderSkinAsString("searchview");
         }

         // render next links, if necessary
         if (end < found) {
            var sp = new Object();
            sp.url = this.href() + "search?q=" + (encodeURIComponent(query)) + "&start=" + Math.min(found-1, start+itemsPerPage);
            sp.text = "next results";
            res.data.body += renderSkinAsString("nextpagelink", sp);
         }
      }
   }
   this.renderPage();
   return;
}


/**
 * block this Site (GET)
 */
function block_action_get() {
   res.data.action = this.href(req.action);
   res.data.title = getMessage('Site.edit.block.header', {siteTitle: this.title});
   res.handlers.mgr = this.stories;
   res.data.param = {
      removeBlockedSitesAfterDays: root.preferences.getProperty('sys_removeBlockedSitesAfterDays'),
      purgeInDaysText: getMessage('Site.edit.block.purgeInDays'),
      purgeImmediateText: getMessage('Site.edit.block.purgeImmediate')
   };
   res.data.body = this.renderSkinAsString('block', res.data.param);      
   this.stories.renderMgrPage('Site');
}


/**
 * block this Site (POST)
 */
function block_action_post() {
   if (req.data.cancel) res.redirect(this.href('edit'));
   // deactivation confirmed by user and req.data.dayOption is a number
   if (req.data.confirmblock) {
      checkSecretKey();
      // sanitize dayOption in case POST is issued via external REST
      var maxOption = Number(root.preferences.getProperty('sys_removeBlockedSitesAfterDays'));
      var dayOption = Number(req.data.dayOption);
      // keep dayOption in the range of 1 to 30
      dayOption = isNaN(dayOption) ? maxOption : Math.min(Math.max(1, dayOption), maxOption);
      // now block the site and calculate date point of purge
      res.message = root.blockSite(this);
      // set purge date only if blocksite() has been processed without error
      if (this.blocked === 1 && this.blocktime !== null) {
         this.lastdelwarn = new Date(this.blocktime.getTime() + (dayOption).days());
      }
   }
   res.redirect(this.href());
}


/**
 * unblock this Site
 */
function unblock_action() {
   if (req.data.confirm) {
      checkSecretKey();
      res.message = root.unblockSite(this);
      res.redirect(this.href());
   } else {
      this.renderSkin("unblock");
   }
}


/**
 * subscribe action
 */
function subscribe_action() {
   // create a new member-object and add it to membership-mountpoint
   this.members.add(new Membership(session.user));
   res.message = new Message("subscriptionCreate", this.title);
   res.redirect(this.href());
   return;
}


/**
 * unsubscribe action
 */
function unsubscribe_action() {
   if (req.data.cancel)
      res.redirect(this.members.href("updated"));
   else if (req.data.remove) {
      try {
         checkSecretKey();
         res.message = this.members.deleteMembership(this.members.get(session.user._id.toString()), session.user);
         res.redirect(this.members.href("updated"));
      } catch (err) {
         res.message = err.toString();
      }
   }

   res.data.title = getMessage("Site.unsubscribe.title", {siteTitle: this.title});
   var skinParam = {
      description: getMessage("Site.subscription.deleteDescription"),
      detail: this.title
   };
   res.data.body = this.renderSkinAsString("delete", skinParam);
   res.handlers.mgr = this.members;
   this.members.renderMgrPage("Membership");
   return;
}


/**
 * context menu extension
 */
function menuext_action() {
   this.renderSkin("menuext");
   return;
}


/**
 * context menu extension (accessible as /menuext.reg)
 */
function menuext_reg_action() {
   res.contentType = "application/exe";
   this.renderSkin("menuext.reg");
   return;
}


/**
 * robots.txt action
 */
function robots_txt_action() {
   res.contentType = "text/plain";
   if (this.show)
      this.renderSkin("robotsAllow");
   else
      this.renderSkin("robotsBlock");
   return;
}


/**
 * redirect to static
 * kept for backwards compatibility
 */
function jsLib_js_action() {
   res.redirect(getStaticUrl() + "jsLib.js");
}


/**
 * action for displaying the webPublishingWizard
 */
function webPublishingWizard_action() {
   if (req.data.done) {
      try {
         checkSecretKey();
         req.data.name = req.data.user;
         this.members.evalLogin(req.data);
         res.message = null;
      } catch (err) {
         res.message = err.toString();
      }
   }

   if (session.user) {
      res.handlers.imagemgr = this.images;
      this.renderSkin("webPublishingWizard");
   } else {
      this.renderSkin("webPublishingWizardLogin"); // was not able to authenticate user, go back to login-form
   }
}


/**
 * action for downloading reg-file which alters user's registry in a way that this installation is listed as serviceprovider for webpublishing.
 */
function webPublishingWizard_reg_action() {
   res.contentType = "application/exe";
   this.renderSkin("webPublishingWizardRegistry");
}


/**
 * This is a first attempt at implementing RSD for Antville.
 * See http://archipelago.phrasewise.com/rsd for further
 *  enlightenment.
 *
 *  There are still is a difficult Problem: how do I find out if
 *  XML-RPC is enabled and where it is running?
 *
 * --md@hudora.de
 *
 */
function rsd_xml_action() {
   res.contentType = "text/xml";
   // try to get hostname from the root
   param = new Object();
   pattern = "http://([^:/]+).*";
   exp = new RegExp(pattern, "i");
   param.host = root.getUrl().replace(exp, "$1");
   this.renderSkin("rsd.xml", param);
}


/**
 * Custom action which renders the skin Site.custom.
 * Has the same security check as Site.main_action.
 */
function custom_action() {
   var sk = this.layout.skins.get("Site") ? this.layout.skins.get("Site").get("custom") : null;
   if (sk) {
      this.renderSkin("custom");
   } else {
      res.redirect(this.href());
   }
   return;
}


/**
 * list all stories for a month
 */
function month_action() {
   if (!req.data.date) res.redirect(this.href());
   var days = [];
   for (var i=0; i<this.count(); i++) {
      var day = this.get(i).groupname;
      if (day && day.substring(0, 6) == req.data.date) {
         days.push(day);
      }
   }
   res.push();
   var cnt = 0;
   for (var i=0; i<days.length; i++) {
      var day = this.get(days[i]);
      if (this.preferences.getProperty("usedayheader")) {
         day.get(0).renderSkin("dayheader");
      }
      for (var j=0;j<day.size();j++) {
         day.get(j).renderSkin("preview");
         cnt++;
         if (cnt > 30) break;
      }
      if (this.preferences.getProperty("usedayheader")) {
         day.get(0).renderSkin("dayfooter");
      }
      if (cnt > 30) break;
   }
   res.data.storylist = res.pop();
   res.data.body = this.renderSkinAsString("main");
   res.data.title = this.title;
   this.renderPage();
   logAccess();
   return;
}


/**
 * Uses the random text generator to display a random text.
 * @see http://thammerl.interlinked.org/th/index.php/2007/09/20/random-text-generator
 */
function randomText_action() {
   res.data.body = this.renderSkinAsString("randomText");
   res.data.title = this.title;
   this.renderPage();
}

/**
* search action
*/
function googlesearch_action() {
  res.data.action = this.href(req.action);
  res.data.title = "Search " + this.title;
  res.data.body = this.renderSkinAsString("googlesearch");
  this.renderPage();
  return;
}
