
/**
 * Renders a page with a search form and if a query has been submited 
 * the search results
 */
function tdyDatumSearch_action() {
   // Security Check
   try {
      this.tdyDatumSecurityCheck();
   } catch (deny) {
      res.redirect(this.href());
      return;
   }
   // do not allow Bots to perform (CPU intensive) search requests
   var userAgent = req.data.http_browser ? req.data.http_browser.toLowerCase() : "";
   var deniedAgents = ["google", "slurp"];
   if (req.data.q) {
      for (var i in deniedAgents) {
         if (userAgent.contains(deniedAgents[i])) req.data.q = null;
      }
   }

   res.data.action = this.href(req.action);
   res.data.title = "Search " + this.title;
   res.data.body = this.renderSkinAsString("searchform");

   if (req.data.q) {
      // capsuled this into a seperate function
      // not like Site/search
      this.tdyDatumRenderSearchResult(req.data.q);
   }

   this.renderPage();
   return;
}

/**
 * Handles the contact mails
 */
function tdyDatumContact_action() {
   try {
      this.tdyDatumSecurityCheck();
   } catch (deny) {
      res.redirect(this.href());
      return;
   }
   
   if (req.data.doit && !req.data.cancel) {
      // check for errors
      var emailParam = new Object();
      emailParam.firstname = stripTags(req.data.firstname);
      emailParam.lastname = stripTags(req.data.lastname);
      emailParam.email = stripTags(req.data.email);
      emailParam.text = stripTags(req.data.text);
      if (emailParam.firstname.length == 0 || emailParam.lastname.length == 0 ||
            emailParam.email.length == 0 || emailParam.text.length == 0) {
         res.message = "Bitte alle Felder ausf&uuml;llen";
      } else if (!isEmail(emailParam.email)) {
         res.message = "&quot;" + emailParam.email + "&quot; ist keine EmailAdresse";
      } else {
         // send mail with objectFunction
         try {
            var mailBody = this.renderSkinAsString("tdyDatumContactEmail", emailParam);
            // sendMail(from, to, subject, body, bcc)
            var result = sendMail({ name: root.preferences.getProperty("sys_title"), email: root.preferences.getProperty("sys_email")},
                                    { name: "datum.at", email: app.modules.tdyDatum.contactEmail },
                                    "Kontaktanfrage datum.at",
                                    mailBody);
            res.message = result.toString();
            res.redirect("http://www.datum.at/");
         } catch (err) {
            res.message = err.toString();
         }
      }
   } else if (req.data.cancel) {
      res.redirect("http://www.datum.at/");
      return;
   }
   res.data.body = this.renderSkinAsString("tdyDatumContactForm");
   this.renderPage();
   return;
}

/**
 * Handles the abos subscriptions
 */
function tdyDatumAbo_action() {
   try {
      this.tdyDatumSecurityCheck();
   } catch (deny) {
      res.redirect(this.href());
      return;
   }
   
   if (req.data.doit && !req.data.cancel) {
      // check for errors
      var emailParam = new Object();
      emailParam.abo = stripTags(req.data.abo);
      emailParam.number = stripTags(req.data.number);
      emailParam.bname = stripTags(req.data.bname);
      emailParam.baddress = stripTags(req.data.baddress);

      emailParam.firstname = stripTags(req.data.firstname);
      emailParam.lastname = stripTags(req.data.lastname);
      emailParam.email = stripTags(req.data.email);
      emailParam.company = stripTags(req.data.company);
      emailParam.age = stripTags(req.data.age);
      emailParam.occupation = stripTags(req.data.occupation);
      emailParam.street = stripTags(req.data.street);
      emailParam.streetnr = stripTags(req.data.streetnr);
      emailParam.postcode = stripTags(req.data.postcode);
      emailParam.city = stripTags(req.data.city);
      emailParam.country = stripTags(req.data.country);
      emailParam.telefon = stripTags(req.data.telefon);
      // new fields
      emailParam.agender = stripTags(req.data.gender); // Herr Frau
      emailParam.atitle = stripTags(req.data.title); // Dr. Bacc. ...
      
      // AngebotAbo
      emailParam.aabonr = stripTags(req.data.aabonr); // Abonummer
      emailParam.agift = stripTags(req.data.agift); // Abopraemie
      emailParam.abillingaddress = stripTags(req.data.abillingaddress); // Rechnungsempfaenger
      emailParam.aname = stripTags(req.data.aname); // neuer Abonnenten name
      emailParam.aaddress = stripTags(req.data.aaddress); // neue Abonnenten Adresse
      emailParam.adate = stripTags(req.data.adate);
      
      var numberCheck = parseInt(emailParam.number, 10);
      if (emailParam.abo.length == 0) {
         res.message = "Bitte w&auml;hlen Sie eine Abo Form aus und ab welcher Nummer Sie Datum beziehen wollen!";
      }
      /* else if (emailParam.abo == "Geschenks Abo" && (emailParam.bname.length == 0 || emailParam.baddress.length == 0)) {
         res.message = "F&uuml;r das &quot;Geschenks-Abo&quot; m&uuml;ssen Sie die Kontaktdaten des Beschenkten ausf&uuml;llen!";
         
      } 
      */
      else if (emailParam.firstname.length == 0 || emailParam.lastname.length == 0 || emailParam.email.length == 0 ||
            emailParam.street.length == 0 || emailParam.streetnr.length == 0 || emailParam.postcode.length == 0 ||
            emailParam.city.length == 0 || emailParam.country.length == 0 || emailParam.telefon.length == 0) {
         res.message = "Bitte f&uuml;llen Sie Ihre Kontaktdaten vollst&auml;ndig aus!";
      } else if (!isEmail(emailParam.email)) {
         res.message = "&quot;" + emailParam.email + "&quot; ist keine EmailAdresse";
      } else {
         try {
            var mailBody = this.renderSkinAsString("tdyDatumAboEmail", emailParam);
            // sendMail(from, to, subject, body, bcc)
            var result = sendMail({ name: "datum.at", email: root.preferences.getProperty("sys_email")},
                                    { name: "datum.at", email: app.modules.tdyDatum.aboEmail },
                                    "Abobestellung datum.at",
                                    mailBody);
            res.message = result.toString();
            res.redirect("http://www.datum.at/");
         } catch (err) {
            res.message = err.toString();
         }
      }
   } else if (req.data.cancel) {
      res.redirect("http://www.datum.at/");
      return;
   }
   
   //render abo form
   res.data.body = this.renderSkinAsString("tdyDatumAboForm");
   this.renderPage();
   return;
}


/**
 * Custom action which renders the skin Site.tdypreviouslypublished.
 * Has the same security check as every other tdy Action
 */
function tdyPreviouslyPublished_action() {
   try {
      this.tdyDatumSecurityCheck();
   } catch (deny) {
      res.redirect(this.href());
      return;
   }
   var sk = this.layout.skins.get("Site") ? this.layout.skins.get("Site").get("tdypreviouslypublished") : null;
   if (sk) {
      res.data.body = this.renderSkinAsString("tdypreviouslypublished");
      this.renderPage();
   } else {
      res.redirect(this.href());
   }
   return;
}
