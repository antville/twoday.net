/**
 * Handles the contact mails
 */
function tdyContact_action() {
   try {
      this.tdyContactSecurityCheck();
   } catch (deny) {
      res.redirect(this.href());
      return;
   }
   var settings = this.tdyContactGetContactSettings(this._id);
   if (!settings) {
      app.log("no settings found: " + this._id);
      res.redirect(this.href());
   }
   if (req.data.doit && !req.data.cancel) {
      // check for errors
      for (var ii in req.data) {
         req.data[ii] = stripTags(req.data[ii]);
      }
      if (!req.data.email || !req.data.text || req.data.email.length == 0 || req.data.text.length == 0) {
         // TODO i18n
         res.message = "Bitte Email und Nachricht eingeben";
      } else if (!isEmail(req.data.email)) {
         // TODO i18n
         res.message = "&quot;" + req.data.email + "&quot; ist keine EmailAdresse";
      } else {
         // send mail with objectFunction
         try {
            var mailBody = this.renderSkinAsString("tdyContactEmailBody", req.data);
            // sendMail(from, to, subject, body, bcc)
            var result = sendMail({ name: settings.contactEmailNameFrom , email: root.preferences.getProperty("sys_email")},
                                    { name: settings.contactEmailName, email: settings.contactEmail },
                                    "Kontaktanfrage von " + this.alias,
                                    mailBody);
            res.message = result.toString();
            res.redirect(this.href());
         } catch (err) {
            res.message = err.toString();
         }
      }
   } else if (req.data.cancel) {
      res.redirect(this.href());
      return;
   }
   res.data.body = this.renderSkinAsString("tdyContactForm");
   this.renderPage();
   return;
}

/*
 * Checks if action is called from privileged weblog
 */

function tdyContactSecurityCheck() {
   // primitive security check
   var ids = "," + app.modules.tdyContact.weblogIds + ",";
   if (ids.indexOf("," + this._id + ",") == -1) {
      app.log("tdyContactSecurityCheck failed: " + this._id);
      throw new DenyException("general");
   }
   return;
}

function tdyContactGetContactSettings(id) {
   var contactSettings = new HopObject();
   contactSettings["s14780"] = { contactEmailNameFrom: "equipicture.com",
                                contactEmailName: "Info",
                                contactEmail: "info@equipicture.com"};
   contactSettings["s13653"] = { contactEmailNameFrom: "chorherr.twoday.net",
                                contactEmailName: "Christoph Chorherr",
                                contactEmail: "Christoph.Chorherr@gruene.at"};
   return contactSettings["s" + id];
}
