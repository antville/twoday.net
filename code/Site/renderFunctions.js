
/**
 * check if there are any stories in the previous month
 */
function renderLinkToNextMonth(lastDayIndex, currentMonth, monthNames) {
   var l = this.size();
   if (l == 0 || l <= lastDayIndex)
      return "&nbsp;";

   var prevDay = this.get(lastDayIndex + 1);
   if (prevDay && prevDay.groupname > currentMonth) {
      var month = prevDay.groupname.toString().substring(4, 6) - 1;
      return Html.linkAsString({href: prevDay.href()}, monthNames[month]);
   } else {
      return "&nbsp;";
   }
}


/**
 * check if there are any stories in the previous month
 */
function renderLinkToPrevMonth(firstDayIndex, currentMonth, monthNames) {
   var l = this.size();
   if (l == 0 || firstDayIndex == 0)
      return "&nbsp;";

   var nextDay = this.get(firstDayIndex - 1);
   if (nextDay && nextDay.groupname < currentMonth) {
      var month = nextDay.groupname.toString().substring(4, 6) - 1;
      return Html.linkAsString({href: nextDay.href()}, monthNames[month]);
   } else {
      return "&nbsp;";
   }
}


/**
 * function renders the list of stories for site-(front-)pages
 * and assigns the rendered list to res.data.storylist
 * scrollnavigation-links to previous and next page(s) are also
 * assigned to res.data (res.data.prevpage, res.data.nextpage)
 * using this separate renderFunction instead of doing the stuff
 * in storylist_macro() was necessary for completely independent
 * placement of the prevpage- and nextpage-links
 * @param Int Index-position to start with
 */
function renderStorylist(day) {
   var size = this.size();
   if (size == 0) return;
   var idx = 0;

   // if no day is specified, start with today. we may need
   // to search for today's entries (or the latest entry
   // before today) because there may be stories posted for
   // future days. (HW)
   var startdayString = day;
   if (!startdayString)
      startdayString = formatTimestamp(new Date(), "yyyyMMdd");

   var startday = this.get(startdayString); // FIXME TEST
   if (startday && startday.size()>0) {
      idx = this.contains(startday);
   } else {
      // loop through days until we find a day less or equal than
      // the one we're looking for.
      for (var i=size-1; i>=0; i--) {
         if (startdayString >= this.get(i).groupname) {
            this.get(i).prefetchChildren();
            idx = i;
            break;
         }
      }
   }
   var days = this.preferences.getProperty("days") ? this.preferences.getProperty("days") : 2;
   days = Math.min(days, 14);  // render 14 days max
   this.prefetchChildren(Math.max(idx-days,0), idx);

   // only display "newer stories" if we are actually browsing the archive,
   // and the day parameter has been explicitly specified,
   // i.e. suppress the link if we are on the home page and there are
   // stories on future days. (HW)
   if (idx + 1 < size && day) {
      var sp = new Object();
      var prev = this.get(Math.min(idx+days, size-1));
      sp.url = this.href() + "?day=" + prev.groupname;
      sp.text = getMessage("Story.newerStories");
      res.data.prevpage = renderSkinAsString("prevpagelink", sp);
   }
   var endIdx = Math.max(idx - days + 1, 0);
   res.push();
   while (idx >= endIdx) {
      var day = this.get(idx--);
      if (day) {
      if (res.handlers.site.preferences.getProperty("usedayheader")) {
         day.get(0).renderSkin("dayheader");
      }
      for (var i=0;i<day.size();i++) {
         day.get(i).renderSkin("preview");
      }
      if (res.handlers.site.preferences.getProperty("usedayheader")) {
         day.get(0).renderSkin("dayfooter");
      }
      }
   }
   res.data.storylist = res.pop();
   if (idx >= 0) {
      var sp = new Object();
      var next = this.get(idx);
      sp.url = this.href() + "?day=" + next.groupname;
      sp.text = getMessage("Story.olderStories");
      res.data.nextpage = renderSkinAsString("nextpagelink", sp);
   }
   return;
}


/**
 * renders the page skin
 */
function renderPage(param) {
   knallgrau.Event.notifyObservers(this, "beforeRenderPage", param);
   
   if (!param) param = {};
   if (!req.data.header) req.data.header = "";
   
   // do the rendering
   if (!param.skin) { 
      param.skin = (this.layout.preferences.getProperty("orientation") &&
         this.layout.preferences.getProperty("orientation").startsWith("3c_")) ? "page_3c" : "page";
   }
   this.renderSkin(param.skin, param);
   renderAlpineScript();
}


/**
 * Helper function for sidebar modules to reder a sidebar item
 * expects the content set to param.body
 * @param body    should be set by module
 * @param header  may be overiden by user at macro call, or module settings
 */
function renderSidebarModuleItem(moduleName, param) {
   var mod = root.modules.get(moduleName);
   if (!mod) return;
   if (!param.header) param.header = mod.getHeader();
   if (param.editLink && res.meta.memberlevel >= ADMIN) {
      res.push();
      this.link_macro({to:"modules/preferences?mod=" + mod.name, text: getMessage("generic.edit"), style: "font-size:10px;"});
      param.editLink = res.pop();
   } else {
      param.editLink = "";
   }
   this.renderSkin(param.skin || "sidebarItem", param);
}
