
/**
 * Renders a list of search results.
 *
 * @param searchString the search string :) 
 */
function tdyDatumRenderSearchResult(searchString) {
  var query = stripTags(searchString);

  var result = root.searchSites (query, app.modules.tdyDatum.weblogIds);
  
  var found = result.length;
  if (found == 0)
     res.data.body += getMessage("error.searchNothingFound", query);
  else {
     var start = 0;
     var end = found;

     if (found == 1)
        res.data.body += getMessage("confirm.resultOne", query);
     else if (found <= 10)
        res.data.body += getMessage("confirm.resultMany", [encodeForm(query), found]);
     else {
        if (req.data.start)
           start = Math.min(found-1, parseInt (req.data.start));
        if (isNaN(start))
           start = 0;
        end = Math.min(found, start+10);
        res.data.body += getMessage("confirm.resultMany", [encodeForm(query), found]);
        res.data.body += " " + getMessage("confirm.resultDisplay", [start+1, end]);
     }

     res.data.body += "<br />";

     // render prev links, if necessary
     if (start > 0) {
        var sp = new Object();
        sp.url = this.href(req.action) + "?q=" + escape(query)+"&start=" + Math.max(0, start-10);
        sp.text = "previous results";
        res.data.body += "<br /><br />" + renderSkinAsString("prevpagelink", sp);
     }

     // backup Layout skinpath
     var skinpathBackup = res.skinpath;
     // render result
     for (var i=start; i<end; i++) {
        var site = root.get(result[i].sitealias);
        var item = site.allcontent.get(result[i].sid);
        if (item) {
           // so each search item is rendered in the skin of the site
           // so each result can look different :)
           res.skinpath = item.site.layout.getSkinPath();
           res.data.body += item.renderSkinAsString("searchview");
        }
     }
     // restore Layout skinpath to current site
     res.skinpath = skinpathBackup;

     // render next links, if necessary
     if (end < found) {
        var sp = new Object();
        sp.url = this.href(req.action) + "?q=" + escape(query) + "&start=" + Math.min(found-1, start+10);
        sp.text = "next results";
        res.data.body += renderSkinAsString("nextpagelink", sp);
     }
  }
  return;
}
