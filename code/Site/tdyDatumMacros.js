
/**
 * Renders a list of Datum sellers.
 *
 * @param itemSkin
 * The skin to user for one seller
 */
function tdyDatumSellers_macro(param) {
   // Security Check
   try {
      this.tdyDatumSecurityCheck();
   } catch (deny) {
      // nothing is rendered
      return;
   }

   // get sellers
   var error = this.tdyDatumReadInSellers();
   if(error < 0) {
      // or some error
      res.write("[tdyDatumSellers] please check Sellers file: " + error);
      return;
   }
   var itemSkin = param.itemSkin ? param.itemSkin : "tdyDatumSeller";
   var sellers = app.data.tdyDatum.sellers;
   for (var i in sellers) {
      var seller = sellers[i];
      this.renderSkin(itemSkin, seller);
   }
   return;
}

function tdyDatumAboDropdown_macro(param) {
   // Security Check
   try {
      this.tdyDatumSecurityCheck();
   } catch (deny) {
      // nothing is rendered
      return;
   }
   var options = [
      {value: "Geschenks Abo", display: "Geschenks-Abo"},
      {value: "Qualitäts-Abo", display: "Qualit&auml;ts-Abo"},
      {value: "Abo", display: "Abo"},
      {value: "Langzeit Abo", display: "Langzeit-Abo"},
      {value: "Business Abo", display: "Business-Abos"},
      {value: "Auslands Abo", display: "Auslands-Abo"}
   ];
   Html.dropDown({name: "abo"}, options, req.data["abo"], "--- Bitte w&auml;hlen sie aus ---");
   return;
}
