
/**
 * constructor function for site objects
 * @param String Title
 * @param String Alias
 * @param Object Creator
 */
function constructor(title, alias, creator) {
   this.title = title;
   this.alias = alias;
   this.creator = creator;
   this.createtime = this.lastoffline = new Date();
   this.email = creator ? creator.email : null;
   this.online = 1;
   this.blocked = 0;
   this.trusted = creator ? creator.trusted : null;
   this.enableping = 1;
   this.show = 1;

   // create initial preferences
   var prefs = new HopObject();
   prefs.tagline = null;
   prefs.discussionsby = "sorua";
   prefs.usercontrib = 0;
   prefs.archive = 1;
   prefs.days = 8;
   // retrieve locale-object from root
   var loc = root.getLocale();
   prefs.language = loc.getLanguage();
   prefs.country = loc.getCountry();
   prefs.timezone = root.getTimeZone().getID();
   prefs.longdateformat = "EEEE, dd. MMMM yyyy, h:mm a";
   prefs.shortdateformat = "d. MMM, HH:mm";
   prefs.use_global_spamfilter = 1;
   prefs.mostReadIsPrivate = 1;
   prefs.referrersIsPrivate = 1;
   prefs.sidebar01 = "modUserStatus,modSiteMenu,modContributorMenu,modAdminMenu,modRecentUpdates,modLinklist";
   prefs.sidebar02 = "modCalendar,modWeblogSearchbar,modWeblogStatus,modCredits";
   prefs.rssfeedcomplete = 1;
   prefs.rssfeedsummary = 0;
   prefs.rssfeedcomments = 1;
   prefs.rssfeedlatestcomments = 1;
   prefs.rssfeedtopics = 1;
   prefs.license = null;
   prefs.usedayheader = 1;
   this.preferences_xml = Xml.writeToString(prefs);
   knallgrau.Event.notifyObservers(this, "afterConstructor", [{"creator": creator}]);
   
}


/**
 * function saves new properties of site
 * @param Obj Object containing the form values
 * @param Obj User-Object modifying this site
 * @throws Exception
 */
function evalPreferences(param, modifier) {
/*   if (!evalEmail(param.email))
      throw new Exception("emailInvalid");
   this.email = param.email; */
   this.title = stripTags(param.title);
   this.show = param.show ? 1 : 0;
   this.enableping = this.show;

   // store new preferences
   var prefs = this.preferences.getAll();
   for (var i in param) {
      if (i.startsWith("checkbox_preferences_")) prefs[i.substring(21)] = req.data[req.data[i]];
      if (i.startsWith("preferences_")) {
         prefs[i.substring(12)] = param[i];
      }
   }
   prefs.days = Math.min(!isNaN(parseInt(param.preferences_days, 10)) ? parseInt(param.preferences_days, 10) : 3, 14);
   if (param.preferences_discussionsby) {
      prefs.discussionsby = param.preferences_discussionsby;
   }
   if (param.preferences_discussionsBlockList != null) {
      prefs.discussionsBlockList = param.preferences_discussionsBlockList.replace(/\s*[\,\;]\s*/g, ",").trim();
   }

   // prefs.usedayheader = param.preferences_usedayheader ? 1 : 0;
   prefs.usercontrib = param.preferences_usercontrib ? 1 : 0;
   prefs.archive = param.preferences_archive ? 1 : 0;
   // store selected locale
   if (param.locale) {
      var loc = param.locale.split("_");
      prefs.language = loc[0];
      prefs.country = loc.length == 2 ? loc[1] : null;
   }
   prefs.timezone = param.timezone;
   prefs.longdateformat = param.longdateformat;
   prefs.shortdateformat = param.shortdateformat;
   prefs.license = param.license;

   // layout
   if (param.layout) {
      this.layout = this.layouts.get(param.layout);
   }

   // store preferences
   this.preferences.setAll(prefs);
   // call the evalPreferences method of every module
   for (var i in app.modules)
      this.applyModuleMethod(i, "evalPreferences", param);


   // clears static Cache
   this.clearStaticCache();

   // reset cached locale, timezone and dateSymbols
   this.cache.locale = null;
   this.cache.timezone = null;
   this.cache.dateSymbols = null;

   this.modifytime = new Date();
   this.modifier = modifier;
   return new Message("update");
}


/**
 * Performs necessary activities if the Online Status of the Site is changed.
 * @param newStatus "offline" or "online"
 */
function toggleOnline(newStatus) {
   var usr;
   try {
      usr = session.user;
   } catch (err) {
      ;
   }
   var FS = Helma.File.separator;
   var staticDirs = [];
   staticDirs.push(new Helma.File(app.properties.staticPath + FS + this.alias + FS + "images"));
   staticDirs.push(new Helma.File(app.properties.staticPath + FS + this.alias + FS + "files"));
   for (var i=0; i<this.layouts.size(); i++) {
      staticDirs.push(new Helma.File(app.properties.staticPath + FS + this.alias + FS + "layouts" + FS + this.layouts.get(i).alias));
   }
   var protectDirs = [];
   protectDirs.push(new Helma.File(app.properties.protectedStaticPath + FS + this.alias + FS + "images"));
   protectDirs.push(new Helma.File(app.properties.protectedStaticPath + FS + this.alias + FS + "files"));
   for (var i=0; i<this.layouts.size(); i++) {
      protectDirs.push(new Helma.File(app.properties.protectedStaticPath + FS + this.alias + FS + "layouts" + FS + this.layouts.get(i).alias));
   }
   for (var i in staticDirs) staticDirs[i].mkdir();
   for (var i in protectDirs) protectDirs[i].mkdir();

   if (newStatus == "online") {
      this.online = 1;
      // move protectDirs to staticDirs
      for (var i=0; i<protectDirs.length; i++) {
         var files = protectDirs[i].exists() ? protectDirs[i].list() : [];
         for (var j=0; j<files.length; j++) {
            var f = new Helma.File(protectDirs[i], files[j]);
            if (f.exists() && f.isFile()) f.move(staticDirs[i].getAbsolutePath() + FS + files[j]);
         }
      }
      root.manage.syslogs.add(new SysLog("site", this.alias, "set online", usr));
   } else if (newStatus == "offline") {
      this.lastoffline = new Date();
      this.online = 0;
      // move staticDirs to protectDirs
      for (var i=0; i<staticDirs.length; i++) {
         var files = staticDirs[i].exists() ? staticDirs[i].list() : [];
         for (var j=0; j<files.length; j++) {
            var f = new Helma.File(staticDirs[i], files[j]);
            if (f.exists() && f.isFile()) f.move(protectDirs[i].getAbsolutePath() + FS + files[j]);
         }
      }
      root.manage.syslogs.add(new SysLog("site", this.alias, "set offline", usr));
   }
   return;
}


/**
 * function saves spamfilter settings for site
 * @param param     Object containing the form values
 *   .preferences_use_global_spamfilter  0|1 whether to include the server wide spamfilter list
 *   .preferences_spamfilter             list of RegExp patterns, separated by newlines
 * @param modifier  User-Object modifying this site
 * @throws Exception
 */
function evalSpamfilter(param, modifier) {
   // store preferences
   this.preferences.setProperty("use_global_spamfilter", param.preferences_use_global_spamfilter ? 1 : 0);
   this.preferences.setProperty("spamfilter", param.preferences_spamfilter);

   this.modifytime = new Date();
   this.modifier = modifier;
   return new Message("update");
}


/**
 * function checks if language and country were specified
 * for this site. if so, it returns the specified Locale-object
 * otherwise it calls getLocale() for root
 */
function getLocale() {
   var locale = this.cache.locale;
   if (locale)
       return locale;
   if (this.preferences.getProperty("language")) {
      if (this.preferences.getProperty("country"))
         locale = new java.util.Locale(this.preferences.getProperty("language"),
                                       this.preferences.getProperty("country"));
      else
         locale = new java.util.Locale(this.preferences.getProperty("language"));
   } else
      locale = root.getLocale();
   this.cache.locale =locale;
   return locale;
}


/**
 * function checks if license is set for this site. if so, it returns the
 * specified value, else it returns "by" (commercial use, allow modifications)
 */
function getLicense() {
   return this.preferences.getProperty("license");
}


/**
 * function returns the (already cached) DateFormatSymbols according
 * to the locale defined for a site
 */
function getDateSymbols() {
   var symbols = this.cache.dateSymbols;
   if (symbols)
      return symbols;
   this.cache.dateSymbols = new java.text.DateFormatSymbols(this.getLocale());
   return this.cache.dateSymbols;
}


/**
 * function returns the (already cached) TimeZone-Object
 * according to site-preferences
 */
function getTimeZone() {
   var tz = this.cache.timezone;
   if (tz)
       return tz;
   if (this.preferences.getProperty("timezone"))
       tz = java.util.TimeZone.getTimeZone(this.preferences.getProperty("timezone"));
   else
       tz = root.getTimeZone();
   this.cache.timezone = tz;
   return tz;
}


/**
 * function deletes all assets of a site (recursive!)
 */
function deleteAll() {
   this.images.deleteAll();
   this.files.deleteAll();
   this.layouts.deleteAll();
   this.stories.deleteAll();
   this.members.deleteAll();
   this.polls.deleteAll();
   return true;
}


/**
 * send notification to weblogs.com
 * that this site was updated
 * @return Object with properties error and message
 */
function ping() {
   this.lastping = new Date();
   // if in development mode, do nothing
   if ("development".equals(app.properties.environment)) {
      app.log("[Site.ping] development mode detected. EXITING.");
      return;
   }

   // REST style ping
   var url = "http://rpc.weblogs.com/pingSiteForm?name=" + escape(this.title || this.alias) + "&url=" + escape(this.href());
   if (this.isRSSFeedEnabled())
      url += "&changesURL=" + escape(this.href("index.rdf"));
   else if (this.isRSSFeedEnabled("summary"))
      url += "&changesURL=" + escape(this.href("summary.rdf"));
   try {
      var jurl = new java.net.URL(url);
      var conn = jurl.openConnection();
      conn.setAllowUserInteraction(false);
      conn.setRequestMethod("GET");
      conn.setRequestProperty("User-Agent", root.href() + " ping bot");
      conn.setConnectTimeout(500);
      conn.setReadTimeout(500);
      var responseCode = conn.getResponseCode();
      var responseLength = conn.getContentLength();
   } catch (err) {
      ;
   } finally {
      if (conn) conn.disconnect();
   }
   return;

   var params = new java.util.Vector();
      params.addElement(encode(this.title || this.alias));
      params.addElement(this.href());
   var meth = "executeAsync(java.lang.String,java.util.Vector,org.apache.xmlrpc.AsyncCallback)"; // method executeAsync is overloaded

   var xmlRpcClient01 = new Packages.org.apache.xmlrpc.XmlRpcClient("http://rpc.weblogs.com/RPC2");
   xmlRpcClient01[meth]("weblogUpdates.ping", params, null);

   if (this.isRSSFeedEnabled())
      params.addElement(this.href("index.rdf"));
   else if (this.isRSSFeedEnabled("summary"))
      params.addElement(this.href("summary.rdf"));
   var xmlRpcClient03 = new Packages.org.apache.xmlrpc.XmlRpcClient("http://66.94.237.139/"); // ping.blo.gs
   xmlRpcClient03[meth]("weblogUpdates.ping", params, null);
}


/**
 *  href URL postprocessor. If a virtual host mapping is defined
 *  for this site's alias, use it. Otherwise, use normal site URL.
 */
function processHref(href) {
   var vhost = app.properties["vhost." + this.alias];
   var protocol = req && req.servletRequest && req.servletRequest.isSecure() ? "https://" : "http://";
   if (vhost)
      return protocol + vhost + href;
   else if (this.url)
      return protocol + this.url + href;
   else if (app.properties.subdomains == "true")
      return protocol + this.alias.toLowerCase() + "." + app.properties.defaulthost + href
   else
      return protocol + app.properties.defaulthost + "/" + this.alias + href;
}


/**
 * returns a short name for this site
 * Currently this is just the Url without http://
 */
function getName() {
   var HTTPS = 'https://';
   var href = this.href();
   var start = href.indexOf(HTTPS) > -1 ? HTTPS.length : HTTPS.length - 1;
   return href.substring(start, href.length - 1);
}


/**
 * return the currently enabled layout object
 */
function getLayout() {
   if (this.layout)
      return this.layout;
   return root.getLayout();
}


/**
 * render the path to the static directory
 * of this site
 * @param String name of subdirectory (optional)
 */
function staticPath(subdir) {
   if (this.online) {
      res.write(app.properties.staticPath);
   } else {
      res.write(app.properties.protectedStaticPath);
   }
   res.write(this.alias);
   res.write(Helma.File.separator);
   if (subdir)
      res.write(subdir);
   return;
}


/**
 * return the path to the static directory
 * of this site
 * @param String name of subdirectory (optional)
 * @return String path to the static directory
 */
function getStaticPath(subdir) {
   res.push();
   this.staticPath(subdir);
   return res.pop();
}


/**
 * render the url of the static directory
 * of this site
 * @param String optional subdirectory
 */
function staticUrl(subdir) {
	var staticUrl = "";
   if (this.online) {
   	staticUrl = this.url ? (this.href() + "static/") : app.properties.staticUrl;
      staticUrl += this.alias + "/";
   } else {
      staticUrl = this.href();
   }
   if(req && req.servletRequest && req.servletRequest.isSecure())
   	staticUrl = staticUrl.replace("http://", "https://");
   res.write(staticUrl)
   if (subdir)
      res.write(subdir);
   return;
}


/**
 * return the url of the static directory
 * of this site
 * @param String optional subdirectory
 * @return String static url
 */
function getStaticUrl(subdir) {
   res.push();
   this.staticUrl(subdir);
   return res.pop();
}


/**
 * return the directory containing static contents
 * @param String subdirectory (optional)
 * @return Object File object
 */
function getStaticDir(subdir) {
   var f = new Helma.File(this.getStaticPath());
   f.mkdir();
   if (subdir) {
      f = new Helma.File(f, subdir);
      f.mkdir();
   }
   return f;
}


/**
 * return directory used for caching purposes
 * @return Object Helma.File object
 */
function getCacheDir() {
   var FS = Helma.File.separator;
   var f = new Helma.File(app.properties.protectedStaticPath + FS + this.alias + FS + "cache");
   f.mkdir();
   return f;
}


/**
 * function returns the title of a site
 */
function getTitle() {
   if (this.title && this.title.trim())
     return stripTags(this.title);
   else
     return "[" + getMessage("generic.untitled") + "]";
}


/**
 * function returns the used disk space for this site in Kilobyte
 */
function getDiskUsage() {
   var c = getDBConnection("twoday");

   var imageQuery = "select sum(IMAGE_FILESIZE) DISKUSAGE from AV_SITE, AV_IMAGE where " +
               "SITE_ID = IMAGE_F_SITE and IMAGE_F_IMAGE_PARENT is null and SITE_ID = " + this._id;
               
   var fileQuery = "select sum(FILE_FILESIZE) DISKUSAGE from AV_SITE, AV_FILE where " +
               "SITE_ID = FILE_F_SITE and SITE_ID = " + this._id;
               
   var layoutQuery = "select sum(IMAGE_FILESIZE) DISKUSAGE " +
                     "from AV_SITE, AV_LAYOUT, AV_IMAGE where " +
                     "SITE_ID = LAYOUT_F_SITE and LAYOUT_ID = IMAGE_F_LAYOUT and IMAGE_F_IMAGE_PARENT is null and " +
                     "SITE_ID = " + this._id;
                     
   var fileRows = c.executeRetrieval(fileQuery);
   fileRows.next();

   var imageRows = c.executeRetrieval(imageQuery);
   imageRows.next();

   var layoutRows = c.executeRetrieval(layoutQuery);
   layoutRows.next();
   
   var fileUsage = fileRows.getColumnItem("DISKUSAGE") || 0;
   var imageUsage = imageRows.getColumnItem("DISKUSAGE") || 0;
   var layoutUsage = layoutRows.getColumnItem("DISKUSAGE") || 0;

   fileRows.release();
   imageRows.release();
   layoutRows.release();
   return Math.round((fileUsage + imageUsage + layoutUsage) / 1024);
}


/**
 * function returns the disk quota in Kilobyte for this site
 */
function getDiskQuota() {
   var diskQuota = 0;
   if (this.trusted)
      diskQuota = Infinity;
   else
      diskQuota = TDY_BILLING_DISKQUOTA[this.type] * 1024;
   return diskQuota;
}


/**
 * function sends a mail with the infos of the newly created site
 */
function sendNewSiteInfo(email) {
   if (!email) return;
   var sp = {siteAlias: this.alias,
             siteName: this.getName(),
             siteUrl: this.href()};
   sendMail(root.preferences.getProperty("sys_email"),
            email,
            getMessage("mail.newsite", {siteName: this.getName()}),
            root.members.renderSkinAsString("mailnewsite", sp));
   return;
}


/**
 * returns all available modules for sidebars
 * @return Array[Module]
 */
function getSidebarModules() {
   var modules = [];
   for (var i = 0; i < root.modules.size(); i++) {
      var mod = root.modules.get(i);
      if (mod && mod.isActivated && mod.isSidebar) {
         if(!mod.isRestricted || (mod.isRestricted && session.user.sysadmin)) {
            //app.log("pushing: " + mod.title + " " + mod.isRestricted);
            modules.push(mod);
         }
      }
   }
   return modules.sort(new Function("a", "b", "return a.getTitle() > b.getTitle()"));
}


/**
 * returns all available sidebar modules, that are not in use
 * @return Array[Module]
 */
function getAvailableSidebarModules() {
   var sidebarModules = this.getSidebarModules();
   var sidebar01 = this.getModulesInSidebar("sidebar01");
   var sidebar02 = this.getModulesInSidebar("sidebar02");

   var modules = [];
   for (var i = 0; i < sidebarModules.length; i++) {
      if (Array.contains(sidebar01, sidebarModules[i])) continue;
      if (Array.contains(sidebar02, sidebarModules[i])) continue;
      modules.push(sidebarModules[i]);
   }
   return modules;
}


/**
 * returns all modules currently placed in one of the sidebars
 * @param id    String, sidebar01|sidebar02
 * @return Array[Module]
 */
function getModulesInSidebar(id) {
   var sidebarModulesAsString = this.preferences.getProperty(id);
   var sidebarModuleNames = (sidebarModulesAsString) ? sidebarModulesAsString.split(",") : [];
   var modules = [];
   for (var i in sidebarModuleNames) {
      var mod = root.modules.get(sidebarModuleNames[i]);
      if (mod && mod.isActivated && mod.isSidebar) {
         modules.push(mod);
      }
   }
   return modules;
}


/**
 * returns all available modules for this site
 * @return Array[Module]
 */
function getModules() {
   var modules = [];
   for (var i = 0; i < root.modules.size(); i++) {
      var mod = root.modules.get(i);
      if (mod && mod.isActivated && !mod.isSidebar && mod.hasSiteSettings) {
         if(!mod.isRestricted || (mod.isRestricted && session.user.sysadmin)) {
            //app.log("pushing: " + mod.title + " " + mod.isRestricted);
            modules.push(mod);
         }
      }
   }
   return modules;
}


/**
 * Removes all files in the cache directory
 */
function clearStaticCache() {
   var dir = this.getCacheDir();
   var files = dir.list();
   for (var i=0; files && i < files.length; i++) {
      var f = new Helma.File(dir, files[i]);
      f.remove();
   }
   return;
}


/**
 * Writes the requested RSS Feed to the Response
 * @param type String 'comments', 'summary', 'topic.TOPICNAME', null
 */
function writeRSSFeedToResponse(type) {
   if (!this.online) {
      // check if user is logged in
      var u = session.user ? session.user : HTTPAuth.authenticate("RSS Feed");
      if (!u)
        return;
      var lv = res.handlers.site.members.getMembershipLevel(u);
      try {
         this.checkView(u, lv);
      } catch (e) {
         res.redirect(res.handlers.site.href());
      }
   } 

   // determine whether requested feed is enabled or not
   if (!this.isRSSFeedEnabled(type)) return;

   // determine which feed to serve
   var file = this.getStaticRSSFeed(type);

   // if the static file does not exist yet, create it now
   if (!file.exists()) this.writeRSSFeedToFile(type);

   // res.forward static file
   res.contentType = "text/xml";
   res.forward(this.alias + java.io.File.separator + "cache" + java.io.File.separator + file.getName());

   return;
}


/**
 * Writes the requested RSS Feed to a static file
 * @param type String 'comments', 'summary', 'topic.TOPICNAME', null
 */
function writeRSSFeedToFile(type) {
   // determine whether requested feed is enabled or not
   if (!this.isRSSFeedEnabled(type)) return;

   // determine collection and subtitle
   var collection, subtitle;
   if (type == "comments") {
      collection = this.lastmod;
      // collection = this.onlineComments;
      subtitle = getMessage("RSS.comments");
   } else if (type == "latestcomments") {
      collection = this.onlineComments;
      subtitle = getMessage("RSS.comments");
   } else if (type == "summary") {
      collection = this.allstories;
      subtitle = getMessage("RSS.summary");
   } else if (type && type.indexOf("topic.") == 0) {
      collection = this.topics.get(type.substring(6));
      if (!collection) return;
      subtitle = getMessage("RSS.topic") + ":" + collection.groupname;
   } else {
      collection = this.allstories;
   }

   // iterate through collection and collect RSS Items and Ressources
   var items = new java.lang.StringBuffer();
   var resources = new java.lang.StringBuffer();
   var sdf = new java.text.SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
   sdf.setTimeZone(new java.util.SimpleTimeZone(0, "UTC"));
   var max = (type == "comments" || type == "latestcomments") ? 30 : 15;
   max = Math.min(max, collection.size());
   collection.prefetchChildren(0, max);
   for (var i=0; i<max; i++) {
      var story = collection.get(i);
      if (story.createtime == null) {
         app.log("[error] story createtime is null: story._id " + story._id);
      }
      var dateValue = (story.createtime || story.modifytime);
      var item = {
         url: story.href(),
         title: story.getRenderedContentPart("title").clip(80),
         text: story.getRenderedContentPart("text"),
         publisher: this.creator ? this.creator.name : "-",
         creator: story.creator ? story.creator.name : "-",
         date: sdf.format(dateValue),
         subject: story.topic ? story.topic : "",
         year: dateValue.getFullYear()
      };
      if (type == "summary")
         item.text = item.text.clip(500);
      item.email = "";
      if (!item.title) {
         if (!item.text)
            item.title = "...";
         else
            item.title = story.getRenderedContentPart("text").clip(25);
      }
      var skinName = (type == "summary") ? "rssItemSummary" : "rssItem";
      items.append(story.renderSkinAsString(skinName, item));
      resources.append(story.renderSkinAsString("rssResource", item));
   }

   var now = new Date();
   // build the final RSS String
   var param = {
      subtitle: subtitle,
      url: this.href(),
      title: this.creator ? this.creator.name : "-",
      creator: this.creator ? this.creator.name : "-",
      year: now.getFullYear(),
      lastupdate: max > 0 ? sdf.format(this.lastupdate): sdf.format(this.createtime),
      items: items.toString(),
      resources: resources.toString()
   };
   if (this.creator && this.creator.publishemail)
      param.email = this.creator.email.entitize();
   if (this.getLicense()) {
      var ccType = this.getLicense().substring(3);
      var ccCountry = (this.getLocale() && this.getLocale().getLanguage() == "de") ? "de/" : "";
      param.licensePart01 = "<cc:license rdf:resource=\"http://creativecommons.org/licenses/" +
                            ccType + "/2.0/" + ccCountry  + "\" />\n";
      param.licensePart02 = "<cc:License rdf:about=\"http://creativecommons.org/licenses/" +
                            ccType + "/2.0/" + ccCountry  + "\">\n";
      param.licensePart02 += this.renderSkinAsString(this.getLicense());
      param.licensePart02 += "</cc:License>";
   }
   var rss = this.renderSkinAsString("rss", param);
   // make sure that we use the correct encoding
   rss = new java.lang.String(new java.lang.String((rss)).getBytes(app.properties.charset),
               java.lang.System.getProperty("file.encoding"));

   // write the RSS Feed finally to the static file
   var f = this.getStaticRSSFeed(type);
   if (f.exists()) f.remove();
   f.open();
   f.write(rss);
   f.flush();
   f.close();

   return;
}


/**
 * Determines whether a certain RSS Feed is enabled for this Site
 * @param type String 'comments', 'summary', 'topic.TOPICNAME', null
 */
function isRSSFeedEnabled(type) {
   var prefs = this.preferences;
   if (type == "comments") {
      return prefs.getProperty("rssfeedcomments");
   } else if (type == "latestcomments") {
      return prefs.getProperty("rssfeedlatestcomments");
   } else if (type == "summary") {
      return prefs.getProperty("rssfeedsummary");
   } else if (type && type.indexOf("topic.") == 0) {
      var topic = this.topics.get(type.substring(6));
      if (!topic) return false;
      return prefs.getProperty("rssfeedtopics");
   } else {
      return prefs.getProperty("rssfeedcomplete");
   }
}


/**
 * Returns Helma.File representation of static RSS feed
 * @param type String 'comments', 'summary', 'topic.TOPICNAME', null
 */
function getStaticRSSFeed(type) {
   if (type == "comments") {
      return new Helma.File(this.getCacheDir(), "comments.rdf");
   } else if (type == "latestcomments") {
      return new Helma.File(this.getCacheDir(), "latestcomments.rdf");
   } else if (type == "summary") {
      return new Helma.File(this.getCacheDir(), "summary.rdf");
   } else if (type && type.indexOf("topic.") == 0) {
      return new Helma.File(this.getCacheDir(), "topic." + type.substring(6).clean() + ".rdf");
   } else {
      return new Helma.File(this.getCacheDir(), "index.rdf");
   }
}


/**
 * Updates static RSS Feed.
 * This function should be called whenever the static RSS Feeds gets outdated.
 * @param type String 'comments', 'summary', 'topic.TOPICNAME', null, 'all'
 */
function updateStaticRSSFeed(type) {
   var f = this.getStaticRSSFeed(type);
   if (f.exists()) f.remove();
// FIXME the RSS Feeds are not immediately updated;
// therfore we now just remove these Feeds, and they will
// be generated on demand
//   this.writeRSSFeedToFile(type);
   return;
}


/**
 * Calls Site.updateStaticRSSFeed() for all affected RSS Feeds
 * @param topic affected topic
 */
function updateAffectedStaticRSSFeeds(topic) {
   this.updateStaticRSSFeed("index");
   this.updateStaticRSSFeed("summary");
   this.updateStaticRSSFeed("comments");
   if (topic) {
      this.updateStaticRSSFeed("topic." + topic);
   }
   return;
}


/**
 * Checks if a user is on the blocklist
 *
 * @param usr User Object
 * @param type string with the blocklist Type, default is 'discussions'
 * @return boolean true if the user is on the blockList
 */
function isUserBlocked(usr, type) {
   if (!usr) return false;
   type = type ? type : "discussions";
   var blockList = this.preferences.getProperty(type + "BlockList")
      ? this.preferences.getProperty(type + "BlockList").split(",")
      : [];
   for (var i in blockList) {
      if (usr.name == blockList[i]) {
         return true;
      }
   }
   return false;
}


/**
 * returns all available navigation modules for sidebars
 * @return Array[Module]
 */
function getNavigationModules() {
   var modules = [];
   for (var i = 0; i < root.modules.size(); i++) {
      var mod = root.modules.get(i);
      if (mod && mod.isActivated && mod.isSidebar && mod.type == MODULE_TYPE_NAVIGATION) {
         modules.push(mod);
      }
   }
   return modules.sort(new Function("a", "b", "return a.getTitle() > b.getTitle()"));
}

function getInfoModules() {
   var modules = [];
   for (var i = 0; i < root.modules.size(); i++) {
      var mod = root.modules.get(i);
      if (mod && mod.isActivated && mod.isSidebar && mod.type == MODULE_TYPE_INFO) {
         modules.push(mod);
      }
   }
   return modules.sort(new Function("a", "b", "return a.getTitle() > b.getTitle()"));
}

function getExternalContentsModules() {
   var modules = [];
   for (var i = 0; i < root.modules.size(); i++) {
      var mod = root.modules.get(i);
      if (mod && mod.isActivated && mod.isSidebar && mod.type == MODULE_TYPE_EXTERNALCONTENTS) {
         modules.push(mod);
      }
   }
   return modules.sort(new Function("a", "b", "return a.getTitle() > b.getTitle()"));
}

function getListModules() {
   var modules = [];
   for (var i = 0; i < root.modules.size(); i++) {
      var mod = root.modules.get(i);
      if (mod && mod.isActivated && mod.isSidebar && mod.type == MODULE_TYPE_LIST) {
         modules.push(mod);
      }
   }
   return modules.sort(new Function("a", "b", "return a.getTitle() > b.getTitle()"));
}

function getOtherModules() {
   var modules = [];
   for (var i = 0; i < root.modules.size(); i++) {
      var mod = root.modules.get(i);
      if (mod && mod.isActivated && mod.isSidebar && 
			mod.type != MODULE_TYPE_NAVIGATION &&
            mod.type != MODULE_TYPE_INFO &&
            mod.type != MODULE_TYPE_EXTERNALCONTENTS &&
            mod.type != MODULE_TYPE_LIST
         ) {
         modules.push(mod);
      }
   }
   return modules.sort(new Function("a", "b", "return a.getTitle() > b.getTitle()"));
}
