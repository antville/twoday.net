
/**
 * Renders the months of the archive + The stories of the month !
 * FIXME: param.limit should specify the maximum number of months, instead of days
 *
 * @param limit       maximum amount of days to be included
 * @param itemprefix  prefix for each month's item
 * @param itemsuffix  suffix for each month's item
 * @param storylistclass The class for the storylistdiv
 * @param format   Java SimpleDateFormat pattern, default='MMMM yyyy'
 * @see http://java.sun.com/j2se/1.4.1/docs/api/java/text/SimpleDateFormat.html
 * @doclevel public
 */
function tdySigMonthlistWithStories_macro(param) {
   if (this.alias != "sig" || !this.stories.size() || !this.preferences.getProperty("archive")) {
      return;
   }
   res.meta.cachePart = false;
   var size = param.limit ? Math.min(this.size(), param.limit) : this.size();
   param.storylistclass = param.storylistclass ? param.storylistclass : "storylistarchive";
   var storiesOfAMonth = "";
   for (var i=0; i<size; i++) {
      var currentDay = this.get(i);
      for (var j=0; j<currentDay.size(); j++) {
         storiesOfAMonth += currentDay.get(j).renderSkinAsString("tdySigArchiveView");
      }
      var nextDay = this.get(i+1);
      if (!nextDay || nextDay.groupname.substring(0, 6) < currentDay.groupname.substring(0, 6)) {
         res.write(param.itemprefix || "");
         Html.openLink({href: this.href("month") + "?date=" + currentDay.groupname.substring(0,6)});
         var ts = currentDay.groupname.substring(0, 6).toDate("yyyyMM", this.getTimeZone());
         res.write(formatTimestamp(ts, param.format ? param.format : "MMMM yyyy"));
         Html.closeLink();
         res.write(param.itemsuffix || "");
         res.write("\n<div class=\"" + param.storylistclass + "\">\n");
         res.write(storiesOfAMonth);
         res.write("</div>\n");
         storiesOfAMonth = "";
      }
   }
}
