/**
 * This js file encompasses all necessary functions representing the 'Site.alpine' macro. This macro
 * generates (scoped) css and Alpine script (x-data) into a specified skin. Example: Assume there
 * is a skin file by the name of "flag.skin" located in the code/Story prototype folder. A separate
 * file with a structure like a vue SFC file (style and script), by the name of flag-flagForm.html is
 * in the same folder. It will be read by the Alpine macro and utilized to output the entire Alpine
 * construct to the response buffer.
 *
 * The example case; in flag.skin:
 *   <div class="someclass" x-data="<% site.alpine name=flagForm skin=story-flag style=scoped %>"
 *     ... Alpine html, usage of data and functions from the Alpine data scope
 *   </div>
 *   whereas
 *     name .... holds the target name of the data function (required)
 *     skin .... holds name/location of the target skin: hoptype + '-' + skinname
 *     style ... takes the attributes to shape the generated css, separated by comma
 *       scoped:   the generated css will have a scope attached
 *       minified: the generated css will be minified (no spaces or line breaks)
 *       sorted:   the css properties will be sorted ascending
 *
 * The macro will create a data function in "window.$" + name. It will add a x-init attribute
 * if this function contains an init-function (Alpine 3.x does this automatically). It will add
 * styles and script before the DIV.
 *
 */

/**
 * Parses a nested css string, e.g. like in @media
 * @param {string} css string of styles
 * @param {number} start position of the opening bracket of the nested css
 */
function getNestedRuleContent(css, start) {
  var len = css.length;
  // from nested css opening bracket
  var pos = start + 1;
  var openBrackets = 1;
  // find function ending bracket
  while (pos < len && openBrackets !== 0) {
    switch (css[pos]) {
      case '{':
        openBrackets++;
        break;
      case '}':
        openBrackets--;
        break;
    }
    pos++;
  }
  return {
    rule: css.slice(0, start).trim(),
    nested: css.slice(start + 1, pos - 1).trim(),
    ended: pos
  };
}

/**
 * Parses the css string between <style>...</style> into an array of unique rules
 *
 * interface rule {
 *   selectors: string[];
 *   properties: string[];
 *   nested?: rules;
 * }
 * interface rules: rule[];
 *
 * @param {string} css
 * @returns {Array} of rule
 */
function parseCSS(css) {
  var rules = [];
  var semi = css.indexOf(';');
  var open = css.indexOf('{');
  while (css.length && (semi >= 0 || open >= 0)) {
    if (semi < open) {
      // regular @-rule (no nesting)
      rules.push({
        selectors: [],
        properties: [css.slice(0, semi).trim()]
      });
      css = css.slice(++semi).trim();
    } else {
      if (css[0] === '@') {
        // nested @-rules
        var at = this.getNestedRuleContent(css, open);
        rules.push({
          selectors: [],
          properties: [at.rule],
          nested: parseCSS(at.nested)
        });
        css = css.slice(at.ended).trim();
      } else {
        // normal properties/values
        var m = css.match(/([\s\S]*?{[\s\S]*?})/);
        if (m) {
          var rule = m[1].split(/[{}]/);
          var selectors = rule[0]
            .trim()
            .split(',')
            .map(function (s) {
              return s.trim();
            });
          var properties = rule[1]
            .trim()
            .split(';')
            .map(function (s) {
              return s.trim();
            })
            .filter(function (s) {
              return s.length;
            });
          rules.push({ selectors: selectors, properties: properties });
          css = css.slice(m[1].length).trim();
        }
      }
    }
    if (css.length) {
      semi = css.indexOf(';');
      open = css.indexOf('{');
    }
  }
  return rules;
}

/**
 * Regenerates CSS from parsed rules
 * @param {Array} parsedRules returned rules (array or rule) from parseCSS
 * @param {Object} param macro params <% site.alpine name=func skin=hop-skin style=scoped,minified,sorted %>
 * @param {number} level indent level 1 (base css) or 2 (nested css)
 */
function generateCSS(parsedRules, param, level) {
  var styleAttr = (param.style || '').split(',');
  var scoped = Array.contains(styleAttr, 'scoped');
  var minify = Array.contains(styleAttr, 'minified');
  var sorted = Array.contains(styleAttr, 'sorted');

  param.scope = scoped ? 'data-' + Packages.helma.util.MD5Encoder.encode(Math.random()).slice(-8) : undefined;
  var spacer = minify ? '' : ' ';
  var breaker = minify ? '' : '\n';
  var indent = spacer.repeat(2);
  var indentSelectors = breaker + indent.repeat(level);
  var indentProps = breaker + indent.repeat(level + 1);
  var styles = '';

  for (var r in parsedRules) {
    var rule = parsedRules[r];
    if (scoped)
      rule.selectors = rule.selectors.map(function (selector) {
        return '[' + param.scope + '] ' + selector;
      });
    styles += indentSelectors + rule.selectors.join(',' + indentSelectors);
    if (!rule.selectors.length)
      styles +=
        rule.properties[0] + (rule.nested ? spacer + '{' + generateCSS(rule.nested, param, 2) + indent + '}' : ';');
    else {
      if (sorted) rule.properties = rule.properties.sort();
      styles += spacer + '{' + indentProps + rule.properties.join(';' + indentProps) + ';' + indentSelectors + '}';
    }
  }
  return minify ? styles.replace(/\: /g, ':').replace(/;}/g, '}') : styles + breaker;
}

/**
 *
 * @param {Object} param macro params <% site.alpine name=func skin=hop-skin style=scoped,minified,sorted %>
 * whereas "name" (required) holds the name of the Alpine x-data function
 *         "skin" (required) is a combination of the prototype/hoptype and the skin-name, e.g. story-flag
 *         "style" (optional) is a combination of 0 t0 3 attributes scoped,minify,sorted delimited by comma
 *            scoped .... the CSS in the Alpine file will be scoped (= is only valid for the Alpine DIV)
 *            minified .. the CSS will be minified before written to the response buffer
 *            sorted .... the CSS properties within the rules will be sorted ascending
 */
function alpine_macro(param) {
  try {
    var funcName = param.name;
    if (!funcName) throw new Error('Missing Alpine function name');

    var skinName = param.skin;
    if (!skinName) throw new Error('Missing Alpine skin reference');

    var parts = skinName.split('-');
    if (parts.length !== 2) throw new Error('Alpine skin reference must be "hoptype-skinname"');

    var hoptype = parts[0].toLowerCase().capitalize();
    var skin = parts[1];
    var protoDir = new Helma.File(app.dir, hoptype);
    if (!protoDir.exists()) throw new Error('Alpine hoptype folder «' + protoDir.getPath() + '» does not exist');

    var alpineFile = new Helma.File(protoDir, skin + '-' + funcName + '.html');
    if (!alpineFile.exists()) throw new Error('Alpine file «' + alpineFile.getName() + '» not found');

    var alpineContent = alpineFile.readAll();

    // Extract and parse styles from the Alpine file
    var c = alpineContent.match(/<style.*?>([\S\s]*?)<\/style>/i);
    var css = c ? c[1].replace(/\r?\n\s*/g, ' ') : '';
    var parsedCSS = this.parseCSS(css.trim());

    // Extract script from Alpine file
    var s = alpineContent.match(/<script.*?>([\s\S]*?)<\/script>/i);
    if (!s) throw new Error('Required script (js) not found in Alpine file: ' + alpineFile.getName());

    // Replace function name placeholder if present
    var windowFuncName = 'window.$' + funcName;
    var js = s[1].replace('$alpine = ', windowFuncName + ' = ');
    // prepare x-init statement in case there is an init-function on Alpine's data scope
    var xInit = /init\s*\:\s*function/.test(js) ? '" x-init="init()' : '';

    // render any macros on the data scope as they will not be replaced in the parent skin
    js = this.renderSkinAsString(createSkin(js), res.data.param || {});

    // store and adjust the response buffer (to rebuild latest DIV tag)
    var resBuf = res.getBuffer();
    var divStart = resBuf.slice(resBuf.lastIndexOf('<div'));
    var resBuf = resBuf.slice(0, resBuf.length - divStart.length);

    // re-generate the Alpine CSS if there is any
    var content = parsedCSS.length ? '<style>' + this.generateCSS(parsedCSS, param, 1) + '</style>\n' : '';
    // add CSS scope (created in generateCSS) to the DIV tag
    if (param.scope) divStart = divStart.replace('<div', '<div ' + param.scope);
    // generate script tag/content and the rebuilt DIV statement
    content += '<script>' + js + '</script>\n' + divStart + windowFuncName + '()' + xInit;

    // reset and rewrite the response buffer with styles, script and updated DIV
    res.resetBuffer();
    res.write(resBuf + content);
  } catch (err) {
    res.write('Error: ' + err.message);
  }
}
