
/**
 * macro rendering the title of a site
 * You may clip this title to a maximum count of characters
 *
 * @param as        optional as="editor" will render an input field
 * @param linkto    will cause the macro to render a link, linking to
 *                  the specified site action
 * @param limit     maximum count of characters
 * @param clipping  if the title had to be clipped, this string will be
 *                  appended, default = '...'
 * @doclevel public
 */
function title_macro(param) {
   if (param.as == "editor") {
      Html.input(this.createInputParam("title", param));
   } else {
      if (this.title == null) return;
      var title = this.title.trim();
      if (param.limit) {
         if (!param.clipping) param.clipping = "...";
         title = title.clip(param.limit, param.clipping);
      }
      if (param && param.linkto) {
         if (param.linkto == "main")
            param.linkto = "";
         Html.openLink({href: this.href(param.linkto)});
         res.write(stripTags(title));
         Html.closeLink();
      } else {
         res.write(title);
      }
   }
   return;
}


/**
 * macro rendering alias of a site
 *
 * @param as        optional as="editor" will render an input field
 * @doclevel public
 */
function alias_macro(param) {
   if (param.as == "editor")
      Html.input(this.createInputParam("alias", param));
   else
      res.write(this.alias);
   return;
}


/**
 * macro rendering tagline of a site
 *
 * @param as        optional as="editor" will render an input field
 * @doclevel public
 */
function tagline_macro(param) {
   if (param.as == "editor")
      Html.input(this.preferences.createInputParam("tagline", param));
   else if (this.preferences.getProperty("tagline"))
      res.write(stripTags(this.preferences.getProperty("tagline").trim()));
   return;
}


/**
 * macro rendering email
 * NOT IN USE YET
 */
function email_macro(param) {
   if (param.as == "editor")
      Html.input(this.createInputParam("email", param));
   else
      res.write(this.email);
   return;
}


/**
 * macro rendering the time of the last posting of this site
 *
 * @param format   Java SimpleDateFormat pattern, default='yyyy-MM-dd HH:mm'
 * @see Global.formatTimestamp
 * @doclevel public
 */
function lastposting_macro(param) {
   if (this.lastposting)
      res.write(formatTimestamp(this.lastposting, param.format));
   else
      res.write("--/--/--");
   return;
}


/**
 * macro rendering lastupdate time of this site
 * this is the time of the last story posted, the last comment created,
 * or the last major update to a story
 * for more options on the format parameter see Global.formatTimestamp
 *
 * @param format   Java SimpleDateFormat pattern, default='yyyy-MM-dd HH:mm'
 * @see http://java.sun.com/j2se/1.4.1/docs/api/java/text/SimpleDateFormat.html
 * @see Global.formatTimestamp
 * @doclevel public
 */
function lastupdate_macro(param) {
   if (this.lastupdate)
      res.write(formatTimestamp(this.lastupdate, param.format));
   else
      res.write("--/--/--");
   return;
}


/**
 * macro rendering online-status of this site
 *
 * @param as        optional as="editor" will render a checkbox
 * @param yes       string to be rendered, of site is offline
 *                  default = translated:generic.yes
 * @param no        string to be rendered, of site is online
 *                  default = translated:generic.no
 * @doclevel manage
 */
function online_macro(param) {
   if (param.as == "editor") {
      var inputParam = this.createCheckBoxParam("online", param);
      if (req.data.save && !req.data.online)
         delete inputParam.checked;
      Html.checkBox(inputParam);
   } else if (this.online) {
      res.write(param.yes ? param.yes : getMessage("generic.yes"));
   } else {
      res.write(param.no ? param.no : getMessage("generic.no"));
   }
   return;
}


/**
 * macro rendering nr. of days to show on site-frontpage
 *
 * @param as        optional as="editor" will render an input field
 * @doclevel manage
 */
function showdays_macro(param) {
   if (param.as == "editor")
      Html.input(this.preferences.createInputParam("days", param));
   else
      res.write(this.preferences.getProperty("days"));
   return;
}


/**
 * macro rendering enableping-flag for site
 *
 * @param as        optional as="editor" will render a checkbox
 * @doclevel manage
 */
function enableping_macro(param) {
   if (param.as == "editor") {
      var inputParam = this.createCheckBoxParam("enableping", param);
      if (req.data.save && !req.data.enableping)
         delete inputParam.checked;
      Html.checkBox(inputParam);
   } else
      res.write(this.enableping ? getMessage("generic.yes") : getMessage("generic.no"));
   return;
}


/**
 * macro rendering show-flag
 *
 * @param as        optional as="editor" will render a checkbox
 * @doclevel manage
 */
function show_macro(param) {
   if (param.as == "editor") {
      var inputParam = this.createCheckBoxParam("show", param);
      if (req.data.save && !req.data.show)
         delete inputParam.checked;
      Html.checkBox(inputParam);
   } else
      res.write(this.show ? getMessage("generic.yes") : getMessage("generic.no"));
   return;
}


/**
 * macro rendering default longdateformat
 *
 * @param as        as="chooser" will render a dropdown
 *                  with available longdate formats
 *                  as="editor" will render an input field
 * @doclevel manage
 */
function longdateformat_macro(param) {
   if (param.as == "chooser")
      renderDateformatChooser("longdateformat", this.getLocale(),
                              this.preferences.getProperty("longdateformat"));
   else if (param.as == "editor")
      Html.input(this.preferences.createInputParam("longdateformat", param));
   else
      res.write(this.preferences.getProperty("longdateformat"));
   return;
}


/**
 * macro rendering default shortdateformat
 *
 * @param as        as="chooser" will render a dropdown
 *                  with available shortdate formats
 *                  as="editor" will render an input field
 * @doclevel manage
 */
function shortdateformat_macro(param) {
   if (param.as == "chooser")
      renderDateformatChooser("shortdateformat", this.getLocale(),
                              this.preferences.getProperty("shortdateformat"));
   else if (param.as == "editor")
      Html.input(this.preferences.createInputParam("shortdateformat", param));
   else
      res.write(this.preferences.getProperty("shortdateformat"));
   return;
}


/**
 * macro rendering loginStatus of user
 *
 *
 * @param loggedInSkin    MemberMgr skin to be used, if the user is logged in
 *                        default = 'MemberMgr.statusloggedin'
 * @param loggedOutSkin   MemberMgr skin to be used, if the user is not logged in
 *                        default = 'MemberMgr.statusloggedout'
 * @doclevel public
 */
function loginstatus_macro(param) {
   if (session.user) {
      this.members.renderSkin(param.loggedInSkin ? param.loggedInSkin : "statusloggedin");
   } else if (req.action != "login") {
      this.members.renderSkin(param.loggedOutSkin ? param.loggedOutSkin : "statusloggedout");
   }
   return;
}


/**
 * Renders different navigation-skins
 * depending on user-status & rights and param.for
 *
 * @param for      users|contributors|admins
 * @param modules  'all', or a comma separated list of module names<br />
 *                 will call renderSiteNavigation for each module
 * @skin Site.usernavigation      Navigation for users
 * @skin Site.contribnavigation   Navigation for contributors
 * @skin Site.adminnavigation     Navigation for admins
 * @hook Site.renderSiteNavigation
 * @doclevel public
 */
function navigation_macro(param) {
   if (param["for"] == "users" && !param.modules) {
      this.renderSkin("usernavigation");
   }
   if (!session.user)
      return;
   switch (param["for"]) {
      case "contributors" :
         if (session.user.sysadmin ||
             this.preferences.getProperty("usercontrib") ||
             res.meta.memberlevel >= CONTRIBUTOR)
            this.renderSkin("contribnavigation");
         break;
      case "admins" :
         if (session.user.sysadmin || res.meta.memberlevel >= ADMIN)
            this.renderSkin("adminnavigation");
         break;
   }
   if (param.modules != null) {
      var mods = param.modules.split(",");
      if (mods.length == 1 && mods[0] == "all") {
         this.applyModuleMethods("renderSiteNavigation", param);
      } else {
         for (var i in mods)
            this.applyModuleMethod(mods[i], "renderSiteNavigation", param);
      }
   }
   return;
}


/**
 * call this sites navigation render method
 * of a module
 * NOT IN USE!
 *
 * @deprecated
 */
function moduleNavigation_macro(param) {
   if (!param.module)
      return;
   this.applyModuleMethod(param.module, "renderSiteNavigation", param);
   return;
}


/**
 * Renders a calendar
 * version 2
 * the archive option must be enabled for this site
 *
 * @skin Site.calendar
 * @skin Site.calendardayheader
 * @skin Site.calendarweek
 * @skin Site.calendarselday
 * @skin Site.calendarday
 * @doclevel public
 */
function calendar_macro(param) {
   // do nothing if there is not a single story :-))
   // or if archive of this site is disabled
   if (!this.allstories.size() || !this.preferences.getProperty("archive"))
      return;
   // define variables needed in this function
   var calParam = new Object();
   var dayParam = new Object();
   var weekParam = new Object();
   res.push();

   // create new calendar-object
   var cal = java.util.Calendar.getInstance(this.getTimeZone(), this.getLocale());
   var symbols = this.getDateSymbols();

   // render header-row of calendar
   var firstDayOfWeek = cal.getFirstDayOfWeek();
   var weekdays = symbols.getShortWeekdays();
   res.push();
   for (var i=0;i<7;i++) {
      dayParam.day = weekdays[(i+firstDayOfWeek-1)%7+1];
      this.renderSkin("calendardayheader", dayParam);
   }
   weekParam.week = res.pop();
   this.renderSkin("calendarweek", weekParam);

   cal.set(java.util.Calendar.DATE, 1);
   // check whether there's a day or a story in path
   // if so, use it to determine the month to render
   if (path.Story && path.Story.day)
      var today = path.Story.day.toString();
   else if (path.Day && path.Day._prototype == "Day")
      var today = path.Day.groupname.toString();
   if (today) {
      // instead of using String.toDate
      // we do it manually here to avoid that a day like 20021001
      // would be changed to 20020930 in some cases
      cal.set(java.util.Calendar.YEAR, parseInt(today.substring(0, 4), 10));
      cal.set(java.util.Calendar.MONTH, parseInt(today.substring(4, 6), 10)-1);
   }
   // nr. of empty days in rendered calendar before the first day of month appears
   var pre = (7-firstDayOfWeek+cal.get(java.util.Calendar.DAY_OF_WEEK)) % 7;
   var days = cal.getActualMaximum(java.util.Calendar.DATE);
   var weeks = Math.ceil((pre + days) / 7);
   var daycnt = 1;

   var monthNames = symbols.getMonths();
   calParam.month = monthNames[cal.get(java.util.Calendar.MONTH)];
   calParam.year =  cal.get(java.util.Calendar.YEAR);

   // pre-render the year and month so we only have to append the days as we loop
   var currMonth = formatTimestamp(new Date(cal.getTime().getTime()), "yyyyMM");
   // remember the index of the first and last days within this month.
   // this is needed to optimize previous and next month links.
   var firstDayIndex = 9999999;
   var lastDayIndex = -1;
   for (var i=0;i<weeks;i++) {
      res.push();
      for (var j=0;j<7;j++) {
         dayParam.skin = "calendarday";
         if ((i == 0 && j < pre) || daycnt > days)
            dayParam.day = "&nbsp;";
         else {
            var currGroupname = currMonth+daycnt.format("00");
            var linkText = daycnt < 10 ? "&nbsp;" + daycnt + "&nbsp;" : daycnt.toString();
            var currGroup = this.get(currGroupname);
            if (currGroup) {
               var idx = this.contains(currGroup);
               if (idx > -1) {
                  if  (idx > lastDayIndex)
                     lastDayIndex = idx;
                  if (idx < firstDayIndex)
                     firstDayIndex = idx;
               }
               dayParam.day = Html.linkAsString({href: currGroup.href()}, linkText);
            } else {
               dayParam.day = linkText;
            }
            if (currGroupname == today)
               dayParam.skin = "calendarselday";
            daycnt++;
         }
         this.renderSkin(dayParam.skin, dayParam);
      }
      weekParam.week = res.pop();
      this.renderSkin("calendarweek", weekParam);
   }
   // set day to last day of month and try to render next month
   // check what the last day of the month is
   calParam.back = this.renderLinkToPrevMonth(firstDayIndex, currMonth + "01", monthNames);
   calParam.forward = this.renderLinkToNextMonth(lastDayIndex, currMonth + "31", monthNames);
   calParam.calendar = res.pop();
   var str = this.renderSkinAsString("calendar", calParam);
   res.write(str);
}


/**
 * Renders age of site in days
 *
 * @example
 *    This weblog is online since <% site.age %> days.
 * @doclevel public
 */
function age_macro(param) {
   var age = Math.floor((new Date() - this.createtime) / Date.ONEDAY);
   res.write(age);
   return;
}


/**
 * Renders a list of recently added/updated stories/comments
 * of this site
 *
 * @param limit   maximum item count, default=5
 * @param show    comments|stories   you may limit the list to just show comments, or stories
 * @doclevel public
 */
function history_macro(param) {
   param.show = param.show || "all";
   try {
      this.checkView(session.user, res.meta.memberlevel);
   } catch (deny) {
      return;
   }

   var limit = param.limit ? parseInt(param.limit, 10) : 5;
   var i;
   var cnt = i = 0;
   var size = this.lastmod.size();
   var discussions = (this.preferences.getProperty("discussionsby") != "nobody");
   res.push();
   // store story handler
   var storedStoryHandler = res.handlers.story;
   while (cnt < limit && i < size) {
      if (i % limit == 0)
         this.lastmod.prefetchChildren(i, limit);
      var item = this.lastmod.get(i++);
      if (item) {
      switch (item._prototype) {
         case "Story":
            if (param.show == "comments")
               continue;
            break;
         case "Comment":
            if (param.show == "stories" || !item.story || !item.story.online ||
                  !item.story.discussions || !discussions)
               continue;
            break;
      }
      res.handlers.story = item;
      item.renderSkin("historyview");
      cnt++;
      }
   }
   var str = res.pop();
   res.handlers.story = storedStoryHandler;
   res.write(str);
}


/**
 * Renders a list of available localizations as dropdown
 *
 * @doclevel admin
 */
function localechooser_macro(param) {
   renderLocaleChooser(this.getLocale());
   return;
}


/**
 * Renders a list of available (Creative Commons) licenses as dropdown
 *
 * @doclevel admin
 */
function licensechooser_macro(param) {
   renderLicenseChooser(this.getLicense());
   return;
}


/**
 * Renders a list of available time zones as dropdown
 *
 * @doclevel admin
 */
function timezonechooser_macro(param) {
   renderTimeZoneChooser(this.getTimeZone());
   return;
}


/**
 * Renders a list of most read pages, ie. a link
 * to a story together with the read counter et al.
 *
 * @doclevel public
 */
function listMostRead_macro() {
   var param = new Object();
   var size = this.mostread.size();
   for (var i=0; i<size; i++) {
      var s = this.mostread.get(i);
      param.reads = s.reads;
      param.rank = i+1;
      s.renderSkin("mostread", param);
   }
   return;
}


/**
 * Renders a list of referrers, ie. a link
 * to a url together with the read counter et al.
 *
 * @doclevel public
 */
function listReferrers_macro() {
   var c = getDBConnection("twoday");
   var dbError = c.getLastError();
   if (dbError)
      return getMessage("error.database", dbError);
   // we're doing this with direct db access here
   // (there's no need to do it with prototypes):
   var d = new Date();
   d.setDate(d.getDate()-1); // 24 hours ago
   var query = "select ACCESSLOG_REFERRER, count(*) as \"COUNT\" from AV_ACCESSLOG " +
      "where ACCESSLOG_F_SITE = " + this._id + " and ACCESSLOG_DATE > {ts '" +
      d.format("yyyy-MM-dd HH:mm:ss") + "'} group by ACCESSLOG_REFERRER "+
      "order by \"COUNT\" desc, ACCESSLOG_REFERRER asc;";
   var rows = c.executeRetrieval(query);
   var dbError = c.getLastError();
   if (dbError)
      return getMessage("error.database", dbError);
   var skinParam = new Object();
   var referrer;
   while (rows.next()) {
      skinParam.count = rows.getColumnItem("COUNT");
      referrer = rows.getColumnItem("ACCESSLOG_REFERRER");
      skinParam.referrer = encode(referrer);
      skinParam.text = encode(referrer.length > 50 ? referrer.substring(0, 50) + "..." : referrer);
      this.renderSkin("referrerItem", skinParam);
   }
   rows.release();
   return;
}


/**
 * Renders the xml (RSS) buttons according to the RSS feeds published
 * You may place this macro somewhere in the sidebar, or the footer
 * All parameters specified (except separator will be passed to the
 * renderFunction for the image (DefaultImages.render())
 *
 * @example
 *   <% xmlbutton separator="<br />" %>
 * @separator   separator (HTML allowed)
 * @doclevel public
 */
function xmlbutton_macro(param) {
   var rssFeedComplete = this.preferences.getProperty("rssfeedcomplete") ? true : false;
   var rssFeedSummary = this.preferences.getProperty("rssfeedsummary") ? true : false;
   var rssFeedComments = this.preferences.getProperty("rssfeedcomments") ? true : false;
   var rssFeedLatestComments = this.preferences.getProperty("rssfeedlatestcomments") ? true : false;
   var rssFeedTopics = this.preferences.getProperty("rssfeedtopics") ? true : false;

   var sep = param.separator ? param.separator : " ";
   delete(param.separator);
   var strings = [];
   if (rssFeedComplete) {
      res.push();
      param.linkto = this.href("index.rdf");
      DefaultImages.render("rss_complete", param);
      strings.push(res.pop());
   }
   if (rssFeedSummary) {
      res.push();
      param.linkto = this.href("summary.rdf");
      DefaultImages.render("rss_summary", param);
      strings.push(res.pop());
   }
   if (!rssFeedComplete && rssFeedComments) {
      res.push();
      param.linkto = this.href("comments.rdf");
      DefaultImages.render("rss_complete", param);
      strings.push(res.pop());
   }
   if (rssFeedLatestComments) {
      res.push();
      param.linkto = this.href("latestcomments.rdf");
      DefaultImages.render("rss_comments", param);
      strings.push(res.pop());
   }
   if (path.topic && path.topicmgr && rssFeedTopics) {
      res.push();
      param.linkto = path.topic.href("index.rdf");
      DefaultImages.render("rss_topic", param);
      strings.push(res.pop());
   }
   if (strings.length > 0) {
      return strings.join(sep);
   }
   return;
}


/**
 * Renders a Creative Commons button, according to the License of this sites
 * including the recommanded rdf tags
 *
 * @skin Site.cc
 * @skin Site.cc-by
 * @skin Site.cc-by-nc
 * @skin Site.cc-by-nc-nd
 * @skin Site.cc-by-nc-sa
 * @skin Site.cc-by-nd
 * @skin Site.cc-by-sa
 * @doclevel public
 */
function ccbutton_macro(param) {
   var license = this.getLicense();
   if (!license) 
      return;
   param.ccType = license.substring(3);
   param.ccCountry = (this.getLocale() && this.getLocale().getLanguage() == "de") ? "de/" : "";
   param.ccPermitsRequires = this.renderSkinAsString(license);
   this.renderSkin("cc", param);
}


/**
 * Renders the searchbox
 *
 * @skin Site.searchbox
 * @doclevel public
 */
function searchbox_macro(param) {
   this.renderSkin("searchbox");
   return;
}


/**
 * Renders the months of the archive
 * FIXME: param.limit should specify the maximum number of moth, instead of days
 *
 * @param limit       maximum amount of days to be included
 * @param itemprefix  prefix for each month's item
 * @param itemsuffix  suffix for each month's item
 * @param format   Java SimpleDateFormat pattern, default='MMMM yyyy'
 * @see http://java.sun.com/j2se/1.4.1/docs/api/java/text/SimpleDateFormat.html
 * @doclevel public
 */
function monthlist_macro(param) {
   if (!this.stories.size() || !this.preferences.getProperty("archive"))
      return;

   var size = param.limit ? Math.min(this.size(), param.limit) : this.size();
   param.itemprefix = param.itemprefix ? param.itemprefix : "";
   param.itemsuffix = param.itemsuffix ? param.itemsuffix : "<br />";
   res.push();
   for (var i=size-1;i>=0;i--) {
      var curr = this.get(i);
      var next = i > 0 ? this.get(i-1) : null;
      if (!next || next.groupname.substring(0, 6) < curr.groupname.substring(0, 6)) {
         res.write(param.itemprefix);
         Html.openLink({href: this.href("month") + "?date=" + curr.groupname.substring(0,6)});
         var ts = curr.groupname.substring(0, 6).toDate("yyyyMM", this.getTimeZone());
         res.write(formatTimestamp(ts, param.format ? param.format : "MMMM yyyy"));
         Html.closeLink();
         res.write(param.itemsuffix);
      }
   }
   var str = res.pop();
   res.write(str);
}


/**
 * proxy-macro for layout chooser
 *
 * @see LayoutMgr.layoutchooser_macro
 * @doclevel admin
 */
function layoutchooser_macro(param) {
   if (this.layout)
      param.selected = this.layout.alias;
   this.layouts.layoutchooser_macro(param);
   return;
}


/**
 * render generic preference editor or value
 *
 * @deprecated Site.preference_macro
 */
function preferences_macro(param) {
   if (param.as == "editor") {
      if (param.type == "checkbox") {
         var inputParam = this.preferences.createCheckBoxParam(param.name, param);
         Html.checkBox(inputParam);
      } else {
         var inputParam = this.preferences.createInputParam(param.name, param);
         delete inputParam.part;
         if (param.cols || param.rows) {
            Html.textArea(inputParam);
         } else {
            Html.input(inputParam);
         }
      }
   } else {
      res.write(this.preferences.getProperty(param.name));
   }
   return;
}


/**
 * output spamfilter data appropriate
 * for client-side javascript code
 *
 * @doclevel public
 */
function spamfilter_macro(param) {
   if (this.preferences.getProperty("use_global_spamfilter") == "1") {
      var rootFilters = root.preferences.getProperty("sys_spamfilter") || "";
   } else {
      var rootFilters = "";
   }
   var siteFilters = this.preferences.getProperty("spamfilter");
   var allFilters = (rootFilters + "\n" + siteFilters).split("\n");
   var okFilters = [];
   for (var i in allFilters) {
      if (allFilters[i]) okFilters.push(allFilters[i].trim());
   }
   if (okFilters.length == 0)
      return "";
   else
      return "\"" + okFilters.join("\",\"") + "\"";
}


/**
 * Renders the used disk space for this site
 *
 * @param show   "quota" will show the diskusage in relation to the allowed
 *               maximum diskQuota '234 of 3082 KB'
 *               "diskusageinpercent" will show the diskusge in percent
 * @doclevel admin
 */
function diskusage_macro(param) {
   var diskUsage = this.getDiskUsage();
   var quota = "";
   if (param.show == "diskusageinpercent") {
      if (this.getDiskQuota() > 0 && this.getDiskQuota() < Infinity) {
         res.write(Math.round((diskUsage) / (this.getDiskQuota()) * 100));
         return;
      }
      res.write("0");
      return;
   }
   if (param.show == "quota" && this.getDiskQuota() > 0 && this.getDiskQuota() < Infinity) {
      quota = " " + getMessage("generic.of") + " " + this.getDiskQuota() + " KB";
   } else {
      quota = " KB";
   }
   res.write((diskUsage) + quota);
   return;
}


/**
 * Calls the macro checks if there are any modules present
 * and if they need to be included in the system setup page
 *
 * @hook Site.renderSitePreferences
 * @doclevel admin
 */
function modulePreferences_macro(param) {
   this.applyModuleMethods("renderSitePreferences", param);
   return;
}


/**
 * macro renders a site setting
 *
 * @deprecated Site.preference_macro
 */
function setting_macro(param) {
   return this.preferences.getProperty(param.key);
}


/**
 * Renders the short name of this site (which is part of the URL)
 *
 * @doclevel public
 */
function name_macro(param) {
   return this.getName();
}


/**
 * wrapper macro
 * calls global image_macro()
 *
 * @see Global.image_macro
 * @doclevel public
 */
function image_macro(param) {
   if (!param.name)
      return;
   if (param.name.indexOf("/") == -1)
      param.name = this.alias + "/" + param.name;
   image_macro(param);
}


/**
 * catch some special needs before passing the
 * macro call up to the HopObject prototype
 * FIXME: this is probably to hackish...
 */
function switch_macro(param) {
   if (param.name == "userMayEdit") {
      try {
         // FIXME: unfortunately, the check* methods are
         // not very handy, anymore... (need try/catch block)
         this.checkEdit(session.user, res.meta.memberlevel);
         res.write(param.on);
      } catch (err) {
         res.write(param.off);
         return;
      }
   } else
      HopObject.switch_macro.apply(this, [param]);
   return;
}


/**
 * Renders the number of members of this site
 *
 * @doclevel admin
 */
function membercounter_macro(param) {
   return this.members.size();
}


/**
 * Renders the sidebar01
 * calls the site.sidebar.skin and will pass any parameters to it
 * parameters are accesable via <% param.paramname %>
 *
 * @param any  any parameter will be passed to the sidebarskin
 * @docelevel public
 */
function sidebar01_macro(param) {
   this.renderSkin("sidebar01", param);
}


/**
 * Renders the sidebar02
 * calls the site.sidebar2.skin and will pass any parameters to it
 * parameters are accesable via <% param.paramname %>
 *
 * @param any  any parameter will be passed to the sidebarskin
 * @docelevel public
 */
function sidebar02_macro(param) {
   this.renderSkin("sidebar02", param);
}


/**
 * Renders a dropdown of existing sites where user's level >= CONTRIBUTOR
 *
 * @doclevel admin
 */
function siteChooser_macro(param) {
   var contributors = new Array;
   for (var i=0; i < session.user.memberships.size(); i++) {
      var m = session.user.memberships.get(i);
      if (m.level >= CONTRIBUTOR)
         contributors[i] = {value: m.site.alias, display: m.site.title};
   }

   if (!req.data.addToSite) req.data.addToSite = this.alias;
   Html.dropDown({name: "addToSite"}, contributors, req.data.addToSite);
   return;
}


/**
 * Renders navigation for listing stories
 *
 * @skin Site.listNavigationContainer
 * @doclevel public beta
 */
function adminNavigation_macro(param) {

   var items = this.getListNavigationItems();
   res.push();
   this.renderListNavigationItems(items, param);
   param.items = res.pop();

   this.renderSkin("listNavigationContainer", param);
   return;
}


/**
 * Renders a value from the Site.preferences, or an input field
 *
 * @param name      name of the preference
 * @param as        optional as="editor" will render an input field
 *                  according to param.type
 * @param type      HTML input type, will be used together with as="editor"
 *                  allowed values: button,radio,cehckbox,textarea,submit,hidden,file
 *                  default = text
 * @param linkto    will cause the macro to render a link, linking to
 *                  the specified site action
 * @param stripTags default = no, (except if you specify linkto)
 * @param limit     maximum count of characters
 * @param clipping  if the value had to be clipped, this string will be
 *                  appended, default = '...'
 * @doclevel public
 */
function preference_macro(param) {
   var value = this.preferences.getProperty(param.name);
   param.name = this.preferences.__name__ + "_" + param.name;

   if (!req.data[param.name] && !req.data[param.name + "_array"]) {
      req.data[param.name] = value;
   }

   if (param.as == "editor") {
      switch (param.type) {
         case "button" :
            break;
         case "radio" :
            param.selectedValue = req.data[param.name + "_array"] ? req.data[param.name + "_array"] : req.data[param.name];
            break;
         case "checkbox" :
            param.selectedValue = req.data[param.name + "_array"] ? req.data[param.name + "_array"] : req.data[param.name];
            break;
         default :
            param.value = param.name && req.data[param.name] ? req.data[param.name] : param.value;
      }
      switch (param.type) {
         case "textarea" :
            Html.textArea(param);
            break;
         case "checkbox" :
            Html.hidden({"name": "checkbox_" + param.name, "value": param.name});
            Html.checkBox(param);
            break;
         case "button" :
            // FIXME: this is left for backwards compatibility
            Html.submit(param);
            break;
         case "submit" :
            Html.submit(param);
            break;
         case "password" :
            param.value = "";
            Html.password(param);
            break;
         case "hidden" :
            Html.hidden(param);
            break;
         case "radio" :
            Html.radioButton(param);
            break;
         case "file" :
            Html.file(param);
            break;
         default :
            Html.input(param);
      }
      return;
   } else {
      var value = this.preferences.getProperty(param.name) || value;
      if (value == null) return;
      if (typeof value == "string") value = value.trim();
      if (param.limit && typeof value == "string") {
         value.clip(param.limit, param.clipping || "...");
      }
      if (param && param.linkto) {
         if (param.linkto == "main") param.linkto = "";
         Html.openLink({href: this.href(param.linkto)});
         res.write(stripTags(value));
         Html.closeLink();
      } else {
         if (param.stripTags) {
            res.write(stripTags(value));
         } else {
            res.write(value);
         }
      }
   }
}


/**
 * Renders sidebar modules for the sidebars 01 and 02 according to the order
 * specified at <i>admin &gt; modules &gt; order sidebar modules</i>
 * There is no default value for param.for, so you have to specify it!
 *
 * @param for    sidebar01|sidebar02
 * @doclevel public
 */
function sidebarItems_macro(param) {
   if (param["for"] && (param["for"] == "sidebar01" || param["for"] == "sidebar02")) {
      var modules = this.getModulesInSidebar(param["for"]);
      for (var i in modules) {
         this.applyModuleMethod(modules[i].name, "renderSidebarItem", param);
      }

      for (var i in modules) {
         this.applyModuleMethod(modules[i].name, "onAfterRenderSidebarItem", param);
      }
   }
}


/**
 * Renders a specific sidebar module item, defined by param.module
 * The module has to be installed and activated
 *
 * @param module  name of the module to render
 * @doclevel public
 */
function sidebarItem_macro(param)  {
   if (param.module) {
      this.applyModuleMethod(param.module, "renderSidebarItem", param);
   }
}


/**
 * Renders HTML Meta-Tag to prevent Robots from crawling this Site
 * Depending on this site settings (Site.show), we allow robots
 * to index this site or not
 * You should place this macro into the &lt;HEAD&gt; section of your template
 *
 * @doclevel public
 */
function metaRobots_macro(param) {
   if (!this.show)
      res.write("<meta name=\"robots\" content=\"noindex, nofollow\" />");
   else
      res.write("<meta name=\"robots\" content=\"all\" />");
   return;
}


/**
 * Renders HTML Link-Tag for RSS Feeds, if site.preferences.rssFeed is activated
 * You should place this macro into the &lt;HEAD&gt; section of your template
 * RSS Browsers will use these tags to detect the feeds this site provides
 *
 * @doclevel public
 */
function metaAlternate_macro(param) {
   var rssFeedComplete = this.preferences.getProperty("rssfeedcomplete") ? true : false;
   var rssFeedSummary = this.preferences.getProperty("rssfeedsummary") ? true : false;
   var rssFeedComments = this.preferences.getProperty("rssfeedcomments") ? true : false;
   var rssFeedTopics = this.preferences.getProperty("rssfeedtopics") ? true : false;
   if (path.topic && rssFeedTopics) {
      res.write('<link rel="alternate" type="application/rss+xml" title="RSS 1.0 (' + getMessage("RSS.topic") + ':' + path.topic.groupname + ')" href="' + path.topic.href("index.rdf") + '" />');
   } else if (rssFeedComplete) {
      res.write('<link rel="alternate" type="application/rss+xml" title="RSS 1.0" href="' + this.href("index.rdf") + '" />');
   } else if (rssFeedSummary) {
      res.write('<link rel="alternate" type="application/rss+xml" title="RSS 1.0 (' + getMessage("RSS.summary") + ')" href="' + this.href("summary.rdf") + '" />');
   } else if (rssFeedComments) {
      res.write('<link rel="alternate" type="application/rss+xml" title="RSS 1.0 (' + getMessage("RSS.comments") + ')" href="' + this.href("comments.rdf") + '" />');
   }
   res.write('<link rel="EditURI" type="application/rsd+xml" title="RSD" href="' + this.href("rsd.xml") + '" />');
   return;
}


/**
 * Renders a list of the most recent stories in this site
 * will use the Site._children collection as default and
 * the Story.display.skin to render each story
 *
 * @param max    maximum number of stories that should be listed
 *               default = 3
 * @collection   you may specify "allstories" as the collection to be used.
 *               this will also included stories, which are not on the
 *               frontpage; default = _children
 * @param skin   Story skin to be used for rendering a strory
 *               default = display
 * @see Site.type.properties
 * @skin Story.display
 * @doclevel public
 */
function stories_macro(param) {
   var collection = param.collection === 'allstories' ? this.allstories : this;
   var max = Math.min(collection.count(), param.max || 3);
   var skinName = param.skin || 'display';
   
   this.allstories.prefetchChildren(0, max);

   for (var i=0; i<max; i++) {
      this.allstories.get(i).renderSkin(skinName);
   }
}


/**
 * hook into mostread_action
 */
function hookMostRead_macro(param) {
   this.applyModuleMethods("renderMostRead", param);
   return;
}


/**
 * hook into mostread_action
 */
function hookMostReadSidebar_macro(param) {
   this.applyModuleMethods("renderMostReadSidebar", param);
   return;
}


/**
 * Macro Hook in Site/manage.skin
 *
 * @hook renderSiteManage
 */
function hookSiteManage_macro(param) {
   this.applyModuleMethods("renderSiteManage", param);
   return;
}


/**
 * Uses the random text generator to display a random text, based
 * on the already published stories.
 *
 * @see http://thammerl.interlinked.org/th/index.php/2007/09/20/random-text-generator
 */
function randomText_macro(param) {
   var cnt = 0;
   var part = param.part == "title" ? "title" : "text";
   if (part == "title")
      var limiter = new Packages.at.knallgrau.randomtext.LengthLimiter(40);
   else
      var limiter = new Packages.at.knallgrau.randomtext.LengthLimiter(400);
   var chain = Packages.at.knallgrau.randomtext.MarkovChain(2, limiter);
   for (var i=0; i<Math.min(1000, this.allstories.count()); i++) {
      var str = (stripTags(this.allstories.get(i)[part] || "")).trim();
      if (str && str.split(" ").length > 2 && str.length > 10) {
         cnt++;
         chain.update(str);
      }
   }
   if (cnt == 0)
      return;
   var text = chain.generateRandomText();
   if (part == "text") {
      text = text.replace(". ", ".<br />", "g");
      text = text.replace("! ", "!<br />", "g");
      text = text.replace("? ", "?<br />", "g");
   }
   res.write(text);
   return;
}
