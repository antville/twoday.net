/**
 * Reads in the csv file where datum sellers are stored
 *
 * @return Boolean
 * Or -1 if now file has been uploaded, -2 if the file can't be opened
 */
function tdyDatumReadInSellers() {
    if (!app.data.tdyDatum)
       app.data.tdyDatum = new Object();
    var s = root.get(app.modules.tdyDatum.weblogname);
    //check if file is uploaded
    var csvFileObj = s.files.get(app.modules.tdyDatum.csvFileName);
    if (!csvFileObj)
       return -1;
    // check if data is old?
    if (!app.data.tdyDatum.lastUpdate 
        || app.data.tdyDatum.lastUpdate.getTime() < csvFileObj.createtime.getTime()) {
       // set first because if file has errors it will be parsed every time macro is called!
       app.data.tdyDatum.lastUpdate = csvFileObj.createtime;
       // open file
       var csvFile = new Helma.File(s.getStaticPath("files"), csvFileObj.filename);
       
       //try to open it
       if(!csvFile.open())
           return -2;
       
       // read first line
       var csvLine = csvFile.readln();
       // store fields in dataobject
       app.data.tdyDatum.sellers = new Array();
       
       
       // read lines
       while (csvLine = csvFile.readln()) {
          // split
          var csvValues = csvLine.split(";");
          // csvValues should contain 6 fields
          // 0: name
          // 1: plz
          // 2: ort
          // 3: strasse
          // 4: bezug (not needed)
          // 5: bundesland (not needed)
          var seller = new Object();
          seller.name = csvValues[0];
          seller.plz = csvValues[1];
          seller.ort = csvValues[2];
          seller.strasse = csvValues[3];
          app.data.tdyDatum.sellers[app.data.tdyDatum.sellers.length] = seller;
       }
       csvFile.close();
       // Everything ok?
    }
    return true;
}

