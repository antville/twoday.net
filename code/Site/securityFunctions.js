
/**
 * Permission checks (called by hopobject.onRequest())
 *
 * @param action  String, name of action
 * @param usr     User, who wants to access an action
 * @param level   Int, Membership level
 * @return Object if permission was denied <br />
 *   .message  message, if something went wrong <br />
 *   .url      recommanded redirect URL
 * @doclevel public
 */
function checkAccess(action, usr, level) {
   var url = this.members.href("login");
   var site = this;
   try {
      if (site && !site.online && action != "index.rdf") {
         checkIfLoggedIn(this.href(action));
         site.checkView(usr, level);
      }
      switch (action) {
         case "main" :
         case "custom" :
         case "month" :
            url = root.href();
            this.checkView(usr, level);
            break;
         case "edit" :
         case "manage" :
            checkIfLoggedIn(this.href(action));
            url = this.href();
            this.checkEdit(usr, level);
            break;
         case "contribute" :
            checkIfLoggedIn(this.href(action));
            url = this.href();
            //this.tdyBillingCheckPaidUntil();
            this.checkContribute(usr, level);
            break;
         case "delete" :
            checkIfLoggedIn(this.href(action));
            url = this.href();
            this.checkDelete(usr, level);
            break;
         case "getfile" :
            url = root.href();
            this.checkView(usr, level);
            break;
         case "mostread" :
            url = this.href("manage");
            this.checkMostRead(usr, level);
            break;
         case "referrers" :
            url = this.href("manage");
            this.checkReferrers(usr, level);
            break;
         case "spamfilter" :
            url = root.href();
            this.checkEdit(usr, level);
            break;
         case "search" :
            url = root.href();
            this.checkView(usr, level);
            break;
         case "subscribe" :
            checkIfLoggedIn(this.href(action));
            url = this.href();
            this.checkSubscribe(usr, level);
            break;
         case "unsubscribe" :
            checkIfLoggedIn(this.href(action));
            this.checkUnsubscribe(usr, level);
            break;
         case "block" :
            checkIfLoggedIn(this.href(action));
            this.checkBlock(usr, level);
            break;
         case "unblock" :
            checkIfLoggedIn(this.href(action));
            this.checkUnblock(usr, level);
            break;
         case "randomText" :
            checkIfLoggedIn(this.href(action));
            url = this.href();
            this.checkContribute(usr, level);
            break;
         default :
            url = this.applyAllModuleMethods("checkSiteAccess", [action, usr, level, url]);
            break;
      }
   } catch (deny) {
      return {message: deny.toString(), url: (url ? url : (site ? site.href() : root.href()))};
   }
   return;
}


/**
 * check if a user is allowed to view a site
 * @param Obj Userobject
 * @param Int Permission-Level
 */
function checkView(usr, level) {
   if (!this.online) {
      // if not logged in or not logged in with sufficient permissions
      if (!usr || (level == null && !usr.sysadmin))
         throw new DenyException("siteView");
   }
   return;
}


/**
 * check if user is allowed to show the referrers page
 * @param Obj Userobject
 * @param Int Permission-Level
 * @return String Reason for denial (or null if allowed)
 */
function checkContribute(usr, level) {
   // if not logged in or not logged in with sufficient permissions
   if (!usr || (level < CONTRIBUTOR && !usr.sysadmin))
      throw new DenyException("siteContribute");
   return;
}


/**
 * check if user is allowed to show the referrers page
 * @param Obj Userobject
 * @param Int Permission-Level
 * @return String Reason for denial (or null if allowed)
 */
function checkReferrers(usr, level) {
   var referrersIsPrivate = this.preferences.getProperty("referrersIsPrivate");
   if (!this.online || referrersIsPrivate == "1") {
      // if not logged in or not logged in with sufficient permissions
      if (!usr || (level < CONTENTMANAGER && !usr.sysadmin))
         throw new DenyException("siteReferrers");
   }
}


/**
 * check if user is allowed to show the referrers page
 * @param Obj Userobject
 * @param Int Permission-Level
 * @return String Reason for denial (or null if allowed)
 */
function checkMostRead(usr, level) {
   var mostReadIsPrivate = this.preferences.getProperty("mostReadIsPrivate");
   if (!this.online || mostReadIsPrivate == "1") {
      // if not logged in or not logged in with sufficient permissions
      if (!usr || (level < CONTENTMANAGER && !usr.sysadmin))
         throw new DenyException("siteMostRead");
   }
   return;
}


/**
 * check if user is allowed to edit the preferences of this site
 * @param Obj Userobject
 * @param Int Permission-Level
 * @return String Reason for denial (or null if allowed)
 */
function checkEdit(usr, level) {
   if ((level & MEMBER_MAY_EDIT_PREFS) == 0)
      throw new DenyException("siteManage");
   return null;
}


/**
 * check if user is allowed to delete the site
 * (only SysAdmins or the creator of a site are allowed to delete it!)
 * @param Obj Userobject
 * @return String Reason for denial (or null if allowed)
 */
function checkDelete(usr) {
   if (!usr.sysadmin && usr != this.creator)
      throw new DenyException("siteDelete");
   return;
}


/**
 * function checks if user is allowed to sign up
 * @param Obj Userobject
 * @param Int Permission-Level
 * @return String Reason for denial (or null if allowed)
 */
function checkSubscribe(usr, level) {
   if (level != null && !usr.sysadmin)
      throw new Exception("subscriptionExist");
   if (!this.online && !usr.sysadmin)
      throw new DenyException("siteView");
   if (usr.may("BESUBSCRIBER") == false)
      throw new DenyException("globalMayBeSubscriber");
   return;
}


/**
 * check if user is allowed to unsubscribe
 * @param Obj Userobject
 * @return String Reason for denial (or null if allowed)
 */
function checkUnsubscribe(usr, level) {
   if (level == null) {
      throw new Exception("subscriptionNoExist");
   } else if (usr == this.creator) {
      // Creators / Owners of that Site are not allowed to remove their subscription
      throw new DenyException("unsubscribe");
   }
   return;
}


/**
 * check if user is allowed to block this site
 * @param Obj Userobject
 * @param Int Permission-Level
 * @return String Reason for denial (or null if allowed)
 */
function checkBlock(usr, level) {
   // allready blocked don't do this again
   if (this.blocked)
      throw new DenyException("general");
   if (level < ADMIN)
      throw new DenyException("general");
   return;
}


/**
 * check if user is allowed to unblock this site
 * @param Obj Userobject
 * @param Int Permission-Level
 * @return String Reason for denial (or null if allowed)
 */
function checkUnblock(usr, level) {
   if (usr.sysadmin)
      return;
   // blog is not blocked
   if (!this.blocked)
      throw new DenyException("general");
   // this may never happen because every block must be issued by a user so deny
   if (!this.blockuser)
      throw new DenyException("general");
   // only unblock if blocker is same as unblocker
   if (this.blockuser != usr)
      throw new DenyException("general");
   return;
}
