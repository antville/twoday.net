
/**
 * returns an Array of Admin Manager tasks
 * @param param Object   passed by Macro
 * @return Array
 */
function getManageTasks(param) {
   var items = [
      {
         href: this.href("main"),
         action: "Site/main"
      },
      {
         href: this.images.href("changeicon"),
         action: "ImageMgr/changeicon"
      },
      {
         href: res.handlers.layout.href("preferences"),
         action: "Layout/preferences"
      },
      {
         href: this.href("edit"),
         action: "Site/edit"
      }
   ];
   return items;
}


/**
 * Renders a list of manage site tasks
 *
 * @see Site.getManageTasks
 * @see Mgr.renderTasks
 */
function manageTasks_macro(param) {
   var items = this.getManageTasks(param);
   Mgr.prototype.renderTasks(items, param);
}


/**
 * covers MemberMgr.memberTasks_macro
 */
function memberTasks_macro(param) {
   this.members.memberTasks_macro(param);
}


/**
 * returns an Array of Contributor tasks
 * @param param Object   passed by Macro
 * @return Array
 */
function getContributorTasks(param) {
   var items = [
      {
         href: this.stories.href("create"),
         action: "StoryMgr/create"
      },
      {
         href: this.stories.href("main"),
         action: "StoryMgr/main"
      },
      {
         href: this.images.href("main"),
         action: "ImageMgr/main"
      },
      {
         href: this.files.href("main"),
         action: "FileMgr/main"
      },
      {
         href: this.polls.href("main"),
         action: "PollMgr/main"
      }
   ];
   this.applyModuleMethods("getContributorTasks", [items]);
   return items;
}


/**
 * Renders a list of contributor site tasks
 *
 * @see Site.getContributorTasks
 * @see Mgr.renderTasks
 */
function contributorTasks_macro(param) {
   var items = this.getContributorTasks(param);
   Mgr.prototype.renderTasks(items, param);
}


/**
 * returns an Array of Admin tasks
 * @param param Object   passed by Macro
 * @return Array
 */
function getAdminTasks(param) {
   var items = [
      {
         action: "Site/manage"
      },
      {
         href: res.handlers.site.members.href(),
         action: "MemberMgr/main"
      },
      {
         href: res.handlers.site.href("layout"),
         action: "Layout/main"
      },
      {
         href: res.handlers.site.modules.href(),
         action: "ModuleMgr/main"
      }
   ];

   var mayReferrer = true;
   var mayMostRead = true;
   try { this.checkReferrers(session.user, res.meta.memberlevel); } catch (deny) { mayReferrer = false; }
   try { this.checkMostRead(session.user, res.meta.memberlevel); } catch (deny) { mayMostRead = false; }
   if (mayReferrer) {
      items.push({
         action: "Site/referrers"
      });
   }
   if (mayMostRead) {
      items.push({
         href: res.handlers.site.href("mostread"),
         action: "Site/mostRead"
      });
   }
   return items;
}


/**
 * Renders a list of admin site tasks
 *
 * @see Site.getAdminTasks
 * @see Mgr.renderTasks
 */
function adminTasks_macro(param) {
   var items = this.getAdminTasks(param);
   Mgr.prototype.renderTasks(items, param);
}


/**
 * returns an Array of Owner tasks
 * @param param Object   passed by Macro
 * @return Array
 */
function getOwnerTasks(param) {
   var items = [
      {
         action: "Site/manage",
         href: this.href("manage")
      },
      {
         action: "Site/contribute",
         href: this.href("contribute")
      },
      {
         action: "Site/main",
         href: this.href()
      }
   ];
   return items;
}


/**
 * Renders a list of site tasks for the owner
 *
 * @see Site.getOwenerTasks
 * @see Mgr.renderTasks
 */
function ownerTasks_macro(param) {
   var items = this.getOwnerTasks(param);
   Mgr.prototype.renderTasks(items, param);
}
