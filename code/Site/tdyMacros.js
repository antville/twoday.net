
function tdyTopicNavigation_macro(param) {
   if (param.topic) {
      var topic = this.topics.get(param.topic);
   } else {
      var topic = path.Story && this.topics.get(path.Story.topic);
   }
   if (!topic) return;
   var stories = topic.list();
   stories = stories.reverse();
   var currStoryPos = path.Story ? topic.count() - 1 - topic.contains(path.Story) : 0;
   var currTopicPos = this.topics.contains(topic);
   if (currStoryPos > 0) {
      var previous = stories[currStoryPos - 1];
   } else {
      previousTopic = currTopicPos > 0 ? this.topics.get(currTopicPos - 1) : this.topics.get(this.topics.count() - 1);
      var previous = previousTopic.get(0);
   }
   Html.link({href: previous.href()}, "&lt;&lt;");
   res.write(" ");
   for (var i=0; i<stories.length; i++) {
      Html.link({href: stories[i].href()}, (currStoryPos==i) ? "<u>" + (i+1) + "</u>" : i+1);
      if (i < stories.length - 1) res.write(param.separator || " | ");
   }
   res.write(" ");
   if (currStoryPos < stories.length - 1) {
      var next = stories[currStoryPos + 1];
   } else {
      nextTopic = (currTopicPos < this.topics.count() - 1) ? this.topics.get(currTopicPos + 1) : this.topics.get(0);
      var next = nextTopic.get(nextTopic.count() - 1);
   }
   Html.link({href: next.href()}, "&gt;&gt;");
}
