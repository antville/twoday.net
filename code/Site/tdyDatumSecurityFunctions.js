/*
 * Checks if macros, actions are called within a Datum Weblog
 */

function tdyDatumSecurityCheck() {
   // primitive security check
   var ids = "," + app.modules.tdyDatum.weblogIds + ",";
   if (ids.indexOf("," + this._id + ",") == -1) {
      app.log("tdyDatumSecurityCheck failed: " + this._id);
      throw new DenyException("general");
   }
   return;
}
