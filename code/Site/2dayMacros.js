
/**
 * macro for saibel
 * DEPRECATED
 */
function topicstorylist_macro(param) {

  if (path.Story==null) return;
  if (path.Story.topic != param.topic) return;

  var items = this.allstories.list();

  var allstories = arraySort(items, param.order);
  var itemprefix = param.itemprefix ? param.itemprefix : "";
  var itemsuffix = param.itemsuffix ? param.itemsuffix : "";

  for (var i=0; i<allstories.length; i++) {
    if (param.as=="link") {
      itemprefix = itemprefix + '<a href="'+allstories[i].href()+'">';
      itemsuffix = '</a>' + itemsuffix;
    }
    if (allstories[i].topic == param.topic) res.write( itemprefix + allstories[i].title + itemsuffix );
  }
  return;
}
