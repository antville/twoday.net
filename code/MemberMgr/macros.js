
/**
 * macro renders a link to signup if user is not member of this site
 * if user is member, it displays the level of membership
 */
function membership_macro(param) {
   if (res.meta.memberlevel == null)
      return;
   res.write(getRole(res.meta.memberlevel));
   return;
}


/**
 * macro renders a link to signup-action
 * but only if user is not a member of this site
 * and the site is public
 */
function subscribelink_macro(param) {
   if (this._parent.online && res.meta.memberlevel == null)
      Html.link({href: this._parent.href("subscribe")},
                (getMessage(param.text) || param.text) || getMessage("MemberMgr.signUp"));
   return;
}


/**
 * macro renders a link to signup-action
 * but only if user is not a member of this site
 */
function subscriptionslink_macro(param) {
   if (session.user.size())
      Html.link({href: this.href("updated")},
                param.text ? param.text : getMessage("MemberMgr.subscriptions"));
   return;
}


/**
 * returns an Array of Member tasks
 * @param param Object   passed by Macro
 * @return Array
 */
function getMemberTasks(param) {
   var items = [
      {
         href: this.href("create"),
         action: "MemberMgr/create"
      }
   ];
   return items;
}


/**
 * render list of member tasks
 */
function memberTasks_macro(param) {
   var items = this.getMemberTasks(param);
   this.renderTasks(items, param);
}


/**
 * returns an Array of list navigation items
 * @param param Object   passed by Macro
 * @return Array
 */
function getListNavigationItems() {
   var items = [
      {
         text: getMessage("admin.menu.admin.members.all"),
         action: "MemberMgr/main"
      },
      {
         text: getMessage("admin.menu.admin.members.admins"),
         action: "MemberMgr/admins"
      },
      {
         text: getMessage("admin.menu.admin.members.managers"),
         action: "MemberMgr/managers"
      },
      {
         text: getMessage("admin.menu.admin.members.contributors"),
         action: "MemberMgr/contributors"
      },
      {
         text: getMessage("admin.menu.admin.members.subscribers"),
         action: "MemberMgr/subscribers"
      }
   ];
   return items;
}


/**
 * macro renders a table with all shareable layouts
 * rendering a preview image, a preview button and some basic information
 */
function visualLayoutChooser_macro(param) {
   root.layouts.renderLayoutChooser(param);
}


/**
 * returns an Array of Account tasks
 * @param param Object   passed by Macro
 * @return Array
 */
function getAccountTasks(param) {
   var items = [
      {
         action: "MemberMgr/edit"
      },
      {
         action: "MemberMgr/updated"
      },
      {
         action: "MemberMgr/memberships"
      },
      {
         action: "MemberMgr/newsite"
      },
   ];
   return items;
}


/**
 * Renders a list of site tasks for the owner
 *
 * @see Site.getAccountTasks
 * @see Mgr.renderTasks
 */
function accountTasks_macro(param) {
   var items = this.getAccountTasks(param);
   Mgr.prototype.renderTasks(items, param);
}


/**
 * Macro Hook in MemberMgr/register_step01_body.skin
 *
 * @hook renderMemberMgrRegisterStep01
 */
function hookMemberMgrRegisterStep01_macro(param) {
   this.applyModuleMethods("renderMemberMgrRegisterStep01", param);
   return;
}


/**
 * Macro Hook in MemberMgr/newsite_choosename_body.skin
 *
 * @hook renderMemberMgrNewSite
 */
function hookMemberMgrNewSiteChooseName_macro(param) {
   this.applyModuleMethods("renderMemberMgrNewSite", param);
   return;
}


/**
 * Macro Hook in MemberMgr/login_body.skin
 *
 * @hook renderCommentEdit
 */
function hookMemberMgrLogin_macro(param) {
   this.applyModuleMethods("renderMemberMgrLogin", param);
   return;
}
