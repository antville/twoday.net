
/**
 * main action, lists all members in alpabetical order
 */
function main_action() {
   this.renderView(this, getMessage("Membership.role.members"));
   return;
}


/**
 * list all subscribers of a site
 */
function subscribers_action() {
   this.renderView(this.subscribers, getMessage("Membership.role.subscribers"));
   return;
}


/**
 * list all contributors of a site
 */
function contributors_action() {
   this.renderView(this.contributors, getMessage("Membership.role.contributors"));
   return;
}


/**
 * list all content managers of a site
 */
function managers_action() {
   this.renderView(this.managers, getMessage("Membership.role.contentManagers"));
   return;
}


/**
 * list all admins of a site
 */
function admins_action() {
   this.renderView(this.admins, getMessage("Membership.role.administrators"));
   return;
}


/**
 * action for creating a new Membership
 */
function create_action() {
   if (req.data.cancel) {
      res.redirect(this.href());
   } else if (req.data.keyword) {
      try {
         var result = this.searchUser(req.data.keyword);
         res.message = result.toString();
         res.data.searchresult = this.renderSkinAsString("searchresult", {result: result.obj});
      } catch (err) {
         res.message = err.toString();
      }
   } else if (req.data.add && req.data.username) {
      try {
         checkSecretKey();
         var uname = req.data.username;
         var usertype = uname.substring(0, uname.indexOf(":"));
         var username = uname.substring(uname.indexOf(":") + 1);
         var result = this.evalNewMembership(usertype, username, session.user);
         res.message = result.toString();
         // send confirmation mail
         var sp = new Object();
         sp.site = result.obj.site.title;
         sp.creator = session.user.name;
         sp.url = result.obj.site.href();
         sp.account = result.obj.user.name;
         var mailbody = this.renderSkinAsString("mailnewmember", sp);
         sendMail(root.preferences.getProperty("sys_email"),
                  result.obj.user.email,
                  getMessage("mail.newMember", result.obj.site.title),
                  mailbody);
         res.redirect(result.obj.href("edit"));
      } catch (err) {
         res.message = err.toString();
         if (err instanceof MailException)
            res.redirect(result.obj.href("edit"));
         res.redirect(this.href());
      }
   }
   res.data.action = this.href(req.action);
   res.data.title = getMessage("MemberMgr.create.title");
   res.data.body = this.renderSkinAsString("new");
   res.data.sidebar01 = this.renderSkinAsString("new_sidebar");
   this.renderMgrPage();
   return;
}


/**
 * action for displaying the last updated
 * site list of a user's subscriptions
 */
function updated_action() {
   this.forceSafeDomain();
   var sitelist = session.user.lastUpdatedMemberships;
   res.data.title = getMessage("admin.menu.account.updated");
   var itemsPerPage = 32;
   var currPage = parseInt(req.data.page || 0, 10);
   res.push();
   Html.link({href: this.href("updated") + "?page=" + (currPage + 1)}, getMessage("generic.button.Forward"));
   var nextLink = res.pop();
   res.data.body = this.renderSkinAsString("updated", {
         list: renderList(sitelist, "subscriptionlistitem", itemsPerPage, currPage),
         nextLink: ((currPage+1) * itemsPerPage < sitelist.count() ? nextLink : "")
      });
   res.data.sidebar02 = this.renderSkinAsString("updatedSidebar");
   this.renderMgrPage();
}


/**
 * action for displaying subscriptions of a user
 */
function subscriptions_action() {
   this.forceSafeDomain();
   this.renderSubscriptionView(session.user.subscriptions, getMessage("admin.menu.account.subscriptions"));
   return;
}


/**
 * action for displaying memberships of a user
 */
function memberships_action() {
   this.forceSafeDomain();
   res.data.sidebar02 = this.renderSkinAsString("membershipsSidebar");
   res.data.sidebar02 += this.renderSkinAsString("mainSidebar02");
   this.renderSubscriptionView(session.user.memberships, getMessage("admin.menu.account.memberships"));
   return;
}


/**
 * edit actions for user profiles
 */
function edit_action() {
   this.forceSafeDomain();
   if (req.data.cancel)
      res.redirect(this._parent.href());
   else if (req.data.save) {
      try {
         checkSecretKey();
         res.message = this.updateUser(req.data);
         res.redirect(this.href("edit"));
      } catch (err) {
         res.message = err.toString();
      }
   }

   res.data.title = getMessage("MemberMgr.edit.title", {userName: session.user.name});
   res.data.body = session.user.renderSkinAsString("edit");
   if (session.user.authType == "local")
      res.data.sidebar02 = session.user.renderSkinAsString("editSidebar");
   this.renderMgrPage();
   return;
}


/**
 * edit actions for user profiles
 */
function account_action() {
   this.forceSafeDomain();
   res.data.title = getMessage("MemberMgr.account.title", {userName: session.user.name});
   res.data.body = session.user.renderSkinAsString("account");
   res.data.sidebar02 = session.user.renderSkinAsString("accountSidebar");
   this.renderMgrPage();
   return;
}


/**
 * twoday registration wizard
 */
function register_action() {
   this.forceSingleDomain();
   var step = req.data.step ? req.data.step : 1;
   var doClose = false;
   if (req.data.submit == "cancel" || req.data.cancel) {
      doClose = true;
   } else if (step == "1" && req.data.register) {
      try {
         var result = this.evalRegistration(req.data);
         res.message = result.toString();
         // now we log in the user and send the confirmation mail
         var usr = result.obj;
         usr.doLogin();
         if (root.preferences.getProperty("sys_email")) {
            var sp = {
               name: usr.name,
               password: usr.password,
               activationid: usr.getActivationHash(),
               activationlink: usr.getActivationLink()
            };
            var maildata = {};
            maildata.from = root.preferences.getProperty("sys_email");
            maildata.to = usr.email;
            maildata.subject = getMessage("mail.registration", root.getTitle());
            maildata.body =  this.renderSkinAsString("mailregconfirm", sp);
            knallgrau.Event.notifyObservers(this, "beforeRegistrationConfirmationMail", [maildata]);
            sendMail(maildata.from,
                     maildata.to,
                     maildata.subject,
                     maildata.body
                  );            
         }
         res.redirect(this.href("register") + "?step=2");
      } catch (err) {
         res.message = err.toString();
         // if we got a mail exception redirect back
         if (err instanceof MailException) {
            res.redirect(this.href("register") + "?step=2");
         }
      }

   } else if (req.data.act == "t" || (step == "2" && req.data.register)) {
      var code = (req.data.act) ? req.data.id : req.data.activationCode;
      var usr = (req.data.uid) ? root.users.getById(req.data.uid) : session.user;
      try {
         res.message = this.evalEmailAddressActivation({userID: usr._id, code: code});
         if (session.user == null) usr.doLogin();
         this.closeAndRedirect(true);
      } catch (err) {
         res.message = err.toString();
         res.redirect(this.href("activateExpiredEmail"));
      }
   } else if (step == "3" && req.data.register) {
      doClose = true;
   } else if (step == "3" && req.data.newsite) {
      res.redirect(this.href("newsite"));
   }

   this.closeAndRedirect(doClose);

   if (!req.data.step) req.data.step = "1";

   res.data.action = this.href(req.action);
   res.data.title = path.site ? path.site.title : root.getTitle();
   res.handlers.user = new User();
   res.data.body = this.renderSkinAsString("register_step0" + step);

   var param = new Object();
   var skinName = "register_step0" + step;
   res.data.header = getMessage("MemberMgr.register.title");
   res.data.sidebar01 = this.renderSkinAsString(skinName + "_sidebar");
   res.data.body = this.renderSkinAsString(skinName + "_body");

   this.renderSkin("wizardSimplePage", param);
}

/**
 * twoday login wizard
 */
function login_action() {
   // perform redirect to setupWizard if no user exists yet
   if (!root.users.count()) {
      res.redirect(root.href("sitesetupwizard"));
   }

   this.forceSingleDomain();

   var doClose = false;
   var termsRevision = getProperty('terms.revision');
   var alreadyAgreed = req.cookies.agreed === termsRevision;

   if (req.data.cancel) {
      doClose = true;

   } /* else if (req.data.isuser == "0") {
      var redUrl = this.href("register") + "?";
      if (req.data.name) redUrl += "name=" + encodeURIComponent(req.data.name) + "&";
      if (req.data.email) redUrl += "email=" + encodeURIComponent(req.data.email);
      res.redirect(redUrl);

   } */ else if (req.data.isuser == "2") {
      var redUrl = this.href("sendpwd");
      var name = req.data.name ? req.data.name : req.data.email;
      if (name) redUrl += "?name=" + encodeURIComponent(name);
      res.redirect(redUrl);

   } /* else if (req.data.isuser == "sorua") {
      var redUrl = this.href("modSoruaUserServer") + "?submit=1";
      if (req.data.name) redUrl += "&name=" + encodeURIComponent(req.data.name);
      redUrl += "&authuri=" + escape(req.data.modSoruaAuthServerAuthUri);
      if (req.data.remember || req.data.remembersorua) redUrl += "&remember=1";
      res.redirect(redUrl);

   } */ else if ((req.data.submit === 'login' || req.data.login) && alreadyAgreed) {
      try {
         res.message = this.evalLogin(req.data);
         doClose = true;
         // Successful login
         knallgrau.Event.notifyObservers(this, "afterSuccessfulLoginAction", req.data);
      } catch (err) {
         res.message = err.toString();
      }

   }

   if (!session.data.referrer && req.data.http_referer) {
      session.data.referrer = req.data.http_referer;
   }

   this.closeAndRedirect(doClose);
   res.data.action = this.href(req.action);
   res.data.title = getMessage('User.login.title');
   if (!req.data.isuser) req.data.isuser = '1';

   var skinToRender = alreadyAgreed ? 'login_body' : 'login_body_consent';
   var consentPrompt = getMessage('MemberMgr.login.consentPrompt', { 
      infoStoryUrl: root.get('info').get('stories').href()
   });

   res.data.body = this.renderSkinAsString(skinToRender, {
      prompt: consentPrompt,
      revision: termsRevision,
      validfor: 1001
   });
   
   res.data.header = getMessage('MemberMgr.login.header');
   res.data.sidebar01 = this.renderSkinAsString('login_sidebar');
   knallgrau.Event.notifyObservers(this, 'beforeLoginRenderPage', req.data);
   this.renderSkin('wizardSimplePage');
   return;
}


/**
 * logout action
 */
function logout_action() {
   if (session.user) {
      var authType = session.user.authType;
      var authData = session.user.authData;
      res.message = new Message("logout", session.user.name);
      session.logout();
      session.data.referrer = null;
      addHeaderCookie('avType', '');
      addHeaderCookie('avUsr', '');
      addHeaderCookie('avPw', '');
      addHeaderCookie('avLoggedIn', '', 0);
      /* if (authType == "sorua") {
         res.redirect(authData + "?sorua-action=logout&sorua-return-url=" + escape(this._parent.href()));
      } */
   }
   res.redirect(this._parent.href());
   return;
}


/**
 * password reset action
 */
function sendpwd_action() {
   this.forceSafeDomain();
   if (req.data.cancel)
      res.redirect(this._parent.href());
   else if (req.data.send) {
      try {
         checkSecretKey();
         res.message = this.sendPwd(req.data.email);
         res.redirect(this._parent.href());
      } catch (err) {
         res.message = err.toString();
      }
   }

   var param = new Object();
   res.data.body = this.renderSkinAsString("sendpwd");
   res.data.header = getMessage("MemberMgr.sendpwd.title");
   this.renderSkin("wizardSimplePage", param);
   return;
}


/**
 * Renders the wizard for creating a new site
 */
function newsite_action() {
   this.forceSafeDomain();
   // initialize
   var doClose = false;
   if (!req.data.step) {
      session.data.newsite = {type: "free", layout: "default", alias: buildAlias(session.user.name, root), setpersonalurl: session.user.sites.count() > 0 ? 0 : 1};
   };
   res.data.action = this.href(req.action);

   // handle cancel|next|previous
   if (req.data.submit == "cancel" || req.data.cancel) {
   // cancel
      doClose = true;

   } else if (req.data.next) {
   // next
      if (req.data.step == "chooseType") {
         session.data.newsite.type = req.data.type;
         req.data.step = "choosename";

      } else if (req.data.step == "choosename") {
         try {
            session.data.newsite.title = req.data.title;
            session.data.newsite.alias = req.data.alias;
            session.data.newsite.setpersonalurl = req.data.setpersonalurl;
            session.data.newsite.modJCaptchaAnswer = req.data.modJCaptchaAnswer;
            var param = req.data;
            if (!param.alias) {
               throw new Exception("siteAliasMissing");
            } else if (root.get(param.alias)) {
               session.data.newsite.alias = buildAlias(param.alias, root);
               throw new Exception("siteAliasExisting");
            } else if (!param.alias.isClean()) {
               session.data.newsite.alias = buildAlias(param.alias, root);
               throw new Exception("aliasNoSpecialChars");
            } else if (param.alias.length > 30) {
               session.data.newsite.alias = param.alias.substring(0, 30);
               throw new Exception("aliasTooLong");
            } else if (root[param.alias] || root[param.alias + "_action"]) {
               throw new Exception("aliasReserved");
            }
            req.data.step = "chooselayout";
         } catch (err) {
            res.message = err.toString();
         }

      } else if (req.data.step == "chooselayout") {
         session.data.newsite.layout = req.data.layout;
         try {
            var result = root.evalNewSite(session.data.newsite, session.user);
            res.message = result.toString();
            if (req.data.setpersonalurl) session.user.url = result.obj.href();
            session.data.newsite.href = result.obj.href();
            req.data.step = "done";
         } catch (err) {
            res.message = err.toString();
         }

      }

   } else if (req.data.back) {
   // back
      if (req.data.step == "choosename") {
         session.data.newsite.title = req.data.title;
         session.data.newsite.alias = req.data.alias;
         session.data.newsite.modJCaptchaAnswer = req.data.modJCaptchaAnswer;
         res.data.freeChecked = "";
         res.data.basicChecked = "";
         res.data.advancedChecked = "";
         switch (session.data.newsite.type) {
            case "free": res.data.freeChecked = "checked"; break;
            case "basic": res.data.basicChecked = "checked"; break;
            case "advanced": res.data.advancedChecked = "checked"; break;
         }
         req.data.step = "chooseType";
      } else if (req.data.step == "chooselayout") {
         session.data.newsite.layout = req.data.layout;
         req.data.step = "choosename";
      }
   }

   // compose page

   if (req.data.step == "chooseType" || req.data.step == null) {
      if (req.data.step == null) res.data.freeChecked = "checked"; // initial value
      req.data.step = "chooseType";
      res.data.body = this.renderSkinAsString("newsite_choosetype_body");
      res.data.sidebar01 = this.renderSkinAsString("newsite_choosetype_sidebar");
   } else if (req.data.step == "choosename") {
      req.data.title = session.data.newsite.title;
      req.data.alias = session.data.newsite.alias;
      req.data.setpersonalurl = session.data.newsite.setpersonalurl;
      res.data.title = getMessage("Root.newSite.header");
      res.data.body = this.renderSkinAsString("newsite_choosename_body");
      res.data.sidebar01 = this.renderSkinAsString("newsite_choosename_sidebar");
   } else if (req.data.step == "chooselayout") {
      req.data.layout = session.data.newsite.layout;
      res.data.title = getMessage("Root.newSite.chooseLayout.header");
      res.data.body = this.renderSkinAsString("newsite_chooselayout_body", {});
      res.data.sidebar01 = "";
   } else if (req.data.step == "done") {
      var href = session.data.newsite.href;
      session.data.newsite = null;
      res.redirect(href);
   }

   // check if we will close this wizard
   this.closeAndRedirect(doClose);
   knallgrau.Event.notifyObservers(this, "beforeNewSiteRenderPage", req.data);
   // rende wizard page
   this.renderSkin("wizardSimplePage", {});
}


/**
 * prints the client-side password security check javascript
 */
function passwordSecurityCheck_js_action() {
   res.contentType = "text/javascript";
   this.renderSkin("passwordSecurityCheckJS");
}


/**
 * activates an acount with an expired email address
 *
 */
function activateExpiredEmail_action() {
   if (!session.data.activationUserID || !session.data.activationUserPassword) {
      res.redirect(this.href("login"));
   }
   var u = User.getById(session.data.activationUserID);
   if (u.digest !== session.data.activationUserPassword) {
      res.redirect(this.href("login"));
   }

   if (req.data.sendpwd) {  //send password again to address
      try {
         u.sendRegistrationInfo();
         res.message = getMessage("confirm.mailSendActivation");
      } catch (err) {
         res.message = err.toString();
      }
      res.redirect(this.href("activateExpiredEmail"));
   } else if (req.data.changeMail && req.data.email1) {  // change email
      try {
         checkSecretKey();
         if (req.data.email1 != req.data.email2) {
            throw new Exception("accountEmailNotMatch");
         }
         u.changeEmail(req.data.email1);
         res.message = getMessage("confirm.mailSendActivation");
      } catch (err) {
         res.message = err.toString();
      }
      res.redirect(this.href("activateExpiredEmail"));
   } else if (req.data.activate) { // activation code was entered
      try {
         checkSecretKey();
         res.message = this.evalEmailAddressActivation({userID: session.data.activationUserID, code: req.data.activationHash});
         //unset session variables
         session.data.activationUserPassword = null;
         session.data.activationUserID = null;
         u.doLogin();
         this.closeAndRedirect(true);

      } catch (err) {
         res.message = err.toString();
      }
   }

   res.data.action = "activateExpiredEmail";
   res.data.email = u.email;
   res.data.body = this.renderSkinAsString("activate_expired", { sendAgainLink: this.href("activateExpiredEmail") + "?sendpwd=true" });
   this.renderSkin("wizardSimplePage");
}


function resetpwd_action() {
   this.forceSingleDomain();

   var Hex = Packages.org.apache.commons.codec.binary.Hex;

   try {
      if (!req.data.id || !req.data.key) throw '';

      var user = User.getById(req.data.id);

      if (!user) throw new Exception('resultNoUser');

      var hash = user.getDigest(user.digest).substr(-8);
      var salt = String(user.registered - 0).substring(0, 8)
      var password = java.lang.String(user.digest).toCharArray();
   
      var cipher = getCipher(password, salt, 'decrypt');
      var key = new java.lang.String(decodeURIComponent(req.data.key));
      var ciphertext = Hex.decodeHex(key.toCharArray());
      var cleartext = cipher.doFinal(ciphertext);
      var payload = (new java.lang.String(cleartext) + '').split(',');
   
      if (Date.now() - payload[1] > 0) throw Exception('accountActivationFailed');  
      if (payload[0] !== hash) throw Exception('accountActivationFailed');
   } catch (ex) {
      res.message = ex.name === 'JavaException' ? getMessage('error.accountActivationFailed') : ex.toString();
      res.redirect(this._parent.href());
   }

   if (req.data.cancel)
      res.redirect(this._parent.href());
   else if (req.data.save) {
      try {
         checkSecretKey();
         var param = req.data;
         if (!param.password1 || !param.password2)
            throw new Exception("accountNewPwdMissing");
         else if (param.password1 != param.password2)
            throw new Exception("passwordNoMatch");
         user.digest = user.getDigest(param.password1);
         res.message = new Message("update");
         res.redirect(this.href("login"));
      } catch (err) {
         res.message = err.toString();
      }
   }

   res.data.title = getMessage("MemberMgr.sendpwd.title");
   res.data.body = user.renderSkinAsString("reset");
   this.renderSkin("wizardSimplePage", new Object());
   return;
}
