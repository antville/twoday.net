
/**
 * Permission checks (called by hopobject.onRequest())
 *
 * @param action  String, name of action
 * @param usr     User, who wants to access an action
 * @param level   Int, Membership level
 * @return Object if permission was denied <br />
 *   .message  message, if something went wrong <br />
 *   .url      recommanded redirect URL
 * @doclevel public
 */
function checkAccess(action, usr, level) {
   if (usr && usr.sysadmin) return;
   var url;
   var site = this._parent._prototype == "Site" ? this._parent : null;
   try {
      if (site && !site.online && (action != "login" && action != "register" && action != "sendpwd" && action != "passwordSecurityCheck.js"
            && action != "wizardsimple.css" && action != "manage.css" && action != "modJCaptcha")) {
         checkIfLoggedIn(this.href(action));
         site.checkView(usr, level);
      }
      switch (action) {
         case "newsite" :
         case "register" :
            throw DenyException('general');
            break;
         /*case "newsite" :
            if (!usr) {
               res.message = getMessage("error.userNotLoggedInCreateSiteFailed");
               session.data.referrer = this.href(action);
               var loginUrl = site ? site.members.href("login") : root.members.href("login");
               res.redirect(loginUrl);
            }
            root.checkAddNewSite(usr, level);
            break;*/
         case "main" :
         case "admins" :
         case "managers" :
         case "contributors" :
         case "subscribers" :
         case "create" :
            checkIfLoggedIn(this.href(action));
            this.checkEditMembers(usr, level);
            break;
         case "account" :
         case "edit" :
         case "updated" :
         case "subscriptions" :
         case "memberships" :
         case "logout" :
            checkIfLoggedIn(this.href(action));
            break;
         case "login" :
         case "reset" :
         case "sendpwd" :
         case "passwordSecurityCheck.js" :
         case "activateExpiredEmail" :
            break;
         default :
            url = this.applyAllModuleMethods("checkMemberMgrAccess", [action, usr, level, url]);
            break;
      }
   } catch (deny) {
      return {message: deny.toString(), url: (url ? url : (site ? site.href() : root.href()))};
   }
   return;
}


/**
 * check if user is allowed to edit the memberlist of this site
 * @param Obj Userobject
 * @param Int Permission-Level
 * @return String Reason for denial (or null if allowed)
 */
function checkEditMembers(usr, level) {
   if ((level & MEMBER_MAY_EDIT_MEMBERS) == 0)
      throw new DenyException("memberEdit");
   return;
}
