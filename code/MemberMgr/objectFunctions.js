
/**
 * Check if a login for a USER_LOCAL is ok. Throws an
 * Exception if login is not successful. If req.data.remember
 * is set, then session.user.setAutoLoginCookie is called.
 *
 * @param param Object
 *         .name String, either username or email-address.
 *         .password
 * @return Object containing two properties:
 *             - error (boolean): true if error happened, false if everything went fine
 *             - message (String): containing a message to user
 */
function evalLogin(param) {
   var username = param.name;
   var password = param.password;
   this.applyModuleMethods("onBeforeEvalLogin", [param]);
   if (!username || username.contains("\\")) {
      throw new Exception("loginTypo");
   }
   var u = root.getUserByKey(username, "local");
   if (!u) {
      throw new Exception("loginTypo");
   }
   var digest = u.getDigest(password);
   if (digest !== u.digest) {
      u.failedLoginCount = (u.failedLoginCount != null) ? (u.failedLoginCount + 1) : 1;
      throw new Exception("loginTypo");
   }
   if (u.blocked) {
      throw new Exception("accountBlocked");
   }
   u.doLogin();
   if (req.data.remember) session.user.setAutoLoginCookie();
   return new Message("welcome", [res.handlers.context.getTitle(), session.user.name]);
}


/**
 * Check if a registration attempt is ok. And if so, a new
 * User is registered.
 *
 * @param param Obj Object containing form-values needed for registration
 *   .terms      accept terms of use, must not be null
 *   .email      valid email adress
 *   .password1  must not be null
 *   .password2  has to match password1
 *   .name       username
 * @return ResultObject|ErrorObj
 *         ResultObject:
 *           .error (boolean): true if error happened, false if everything went fine
 *           .message (String): containing a message to user
 *           .username: username of registered user
 *           .password: password of registered user
 * @throws Exception
 */
function evalRegistration(param) {
   this.applyModuleMethods("onBeforeEvalRegistration", [param]);
   // terms of use accepted?
   if (!param.terms) {
      throw new Exception("termsNotAccepted");
   }

   // check if email-address is valid
   if (!param.email) {
      throw new Exception("emailMissing");
   } else if (!isEmail(param.email)) {
      throw new Exception("emailInvalid");
   } else if (root.usersByEmail.get(param.email)) {
      throw new Exception("emailExisting");
   }

   // check if passwords match
   if (!param.password1 || !param.password2) {
      throw new Exception("passwordTwice");
   } else if (param.password1 != param.password2) {
      throw new Exception("passwordNoMatch");
   }

   // check if username is existing
   var invalidChar = new RegExp("[^a-zA-Z0-9äöüÄÖÜß\\.\\-_ ]");
   if (!param.name) {
      throw new Exception("usernameMissing");
   } else if (invalidChar.exec(param.name)) {
      throw new Exception("usernameNoSpecialChars");
   } else if (param.name.length>30) {
      throw new Exception("usernameTooLong");
   } else if (root.users.get(param.name)) {
      throw new Exception("usernameExisting");
   }

   var newUser = new User();
   root.users.add(newUser);
   newUser.type = "USER_LOCAL";
   newUser.authType = "local";
   newUser.name = param.name;
   newUser.email = param.email ? param.email.trim() : param.email;
   newUser.publishemail = param.publishemail;
   newUser.url = evalURL(param.url);
   newUser.registered = new Date();
   newUser.digest = newUser.getDigest(param.password1);
   newUser.blocked = 0;
   // grant trust and sysadmin-rights if there's no sysadmin 'til now
   if (root.manage.sysadmins.size() == 0) {
      newUser.sysadmin = newUser.trusted = 1;
      newUser.emailIsConfirmed = 1;
   } else {
      newUser.sysadmin = newUser.trusted = 0;
   }
   var welcomeWhere = root.getTitle();
   this.applyModuleMethods("evalRegistration", [newUser, param]);
   root.manage.syslogs.add(new SysLog("user", newUser.name, "added user", null));
   return new Message("welcome", [welcomeWhere, newUser.name], newUser);
}


/**
 * evaluates the activation of an email address
 * @param Obj Object containing two properties:
 *             -userID  id of the user
 *             -code    hashcode the user entered
 */
function evalEmailAddressActivation(param) {
   var usr = root.users.getById(param.userID);
   if (usr != null && usr.getActivationHash() == param.code) {
      usr.emailLastConfirmed = new Date();
      usr.emailIsConfirmed = 1;
      return new Message("accountActivationSucceeded");
   } else {
      throw new Exception("accountActivationFailed");
   }
   return;
}


/**
 * update user-profile
 * @param Obj Object containing form values
 * @return Obj Object containing two properties:
 *             - error (boolean): true if error happened, false if everything went fine
 *             - message (String): containing a message to user
 */
function updateUser(param) {
   if (param.oldpwd) {
      if (session.user.digest != session.user.getDigest(param.oldpwd))
         throw new Exception("accountOldPwd");
      if (!param.password1 || !param.password2)
         throw new Exception("accountNewPwdMissing");
      else if (param.password1 != param.password2)
         throw new Exception("passwordNoMatch");
      session.user.digest = session.user.getDigest(param.password1);
   }
   session.user.url = evalURL(param.url);
   var emailIsChanged = (session.user.email != param.email) ? true : false;
   session.user.changeEmail(param.email);
   session.user.publishemail = param.publishemail;
   session.user.setLocale(param.locale);
   var prefs = session.user.preferences.getAll();
   session.user.preferences.setAll(prefs);
   if (emailIsChanged) {
      return new Message("mailSendActivation");
   } else {
      return new Message("update");
   }
}


/**
 * Sends a Password-Reminder via E-Mail to a user.
 *
 * @param userKey
 * @return Obj Object containing two properties:
 *             - error (boolean): true if error happened, false if everything went fine
 *             - message (String): containing a message to user
 */
function sendPwd(userKey) {
   if (!userKey) {
      throw new Exception("emailMissing");
   }
   var usr = root.getUserByKey(userKey);
   if (!usr) {
      throw new Exception("emailNoAccounts")
   }
   if (usr.authType != "local") {
      throw new Exception("sendPwdAuthTypeNotLocal");
   }
   // now we send the mail containing all accounts for this email-address
   var mailbody = this.renderSkinAsString("mailpassword", {name: usr.name,
                                                           resetLink: usr.getResetLink(),
                                                           activationlink: usr.getActivationLink(),
                                                           activationid: usr.getActivationHash()});
   sendMail(root.preferences.getProperty("sys_email"), usr.email, getMessage("mail.sendPwd"), mailbody);
   return new Message("mailSendPassword");
}


/**
 * function searches for users using part of username
 * @param String Part of username or email-address
 * @return Obj Object containing four properties:
 *             - error (boolean): true if error happened, false if everything went fine
 *             - message (String): containing a message to user
 *             - found (Int): number of users found
 *             - list (String): rendered list of users found
 */
function searchUser(key) {
   var dbConn = getDBConnection("twoday");
   var dbError = dbConn.getLastError();
   if (dbError)
      throw new Exception("database", dbError);
   var query = "select USER_ID, USER_AUTH_TYPE, USER_NAME, USER_URL from AV_USER ";
   query += "where USER_NAME like '%" + key + "%' order by length(USER_NAME) asc, USER_NAME";
   var searchResult = dbConn.executeRetrieval(query);
   var dbError = dbConn.getLastError();
   if (dbError)
      throw new Exception("database", dbError);
   var found = 0;
   res.push();
   while (searchResult.next() && found < 100) {
      var userId = searchResult.getColumnItem("USER_ID");
      var authtype = searchResult.getColumnItem("USER_AUTH_TYPE");
      var name = searchResult.getColumnItem("USER_NAME");
      var url = searchResult.getColumnItem("USER_URL");
      // do nothing if the user is already a member
      if (this.get(userId.toString()))
         continue;
      var sp = {authtype: authtype, name: name,
                description: (url ? Html.linkAsString({href: url}, url) : null)};
      this.renderSkin("searchresultitem", sp);
      found++;
   }
   dbConn.release();
   switch (found) {
      case 0:
         throw new Exception("resultNoUser");
      case 1:
         return new Message("resultOneUser", null, res.pop());
      case 100:
         return new Message("resultTooManyUsers", null, res.pop());
      default:
         return new Message("resultManyUsers", found, res.pop());
   }
}


/**
 * function adds a user with a given username to the list of members
 * of this site
 * @param authtype String
 * @param username String
 * @param creator User
 * @return Obj Object containing two properties:
 *             - error (boolean): true if error happened, false if everything went fine
 *             - message (String): containing a message to user
 */
function evalNewMembership(authtype, username, creator) {
   if (!authtype || authtype == "local") {
      var newMember = root.users.get(username);
   } else {
      var newMember = root.nonLocalUsers.get(authtype) ?
         root.nonLocalUsers.get(authtype).get(username) : null;
   }
   if (!newMember)
      throw new Exception("resultNoUser");
   if (this.get(newMember._id.toString()))
      throw new Exception("userAlreadyMember");
   if (newMember.may("BESUBSCRIBER") == false)
      throw new DenyException("globalMayBeSubscriber2");
   try {
      var ms = new Membership(newMember);
      this.add(ms);
      return new Message("memberCreate", ms.user.name, ms);
   } catch (err) {
      throw new Exception("memberCreate", username);
   }
   return;
}


/**
 * function deletes a member
 * @param Obj Membership-Object to delete
 * @param Obj User-Object about to delete membership
 * @return Obj Object containing two properties:
 *             - error (boolean): true if error happened, false if everything went fine
 *             - message (String): containing a message to user
 */
function deleteMembership(membership) {
   if (!membership) {
      throw new Exception("memberDelete");
   } else if (membership.user == this._parent.creator) {
      if (!session.user || !session.user.sysadmin)
         // deleting the owner is denied
         throw new DenyException("memberDeleteOwner");
   }
   membership.remove();
   return new Message("memberDelete");
}


/**
 * function deletes all members
 */
function deleteAll() {
   for (var i=this.size();i>0;i--)
      this.get(i-1).remove();
   return true;
}


/**
 * function retrieves the level of a users membership
 */
function getMembershipLevel(usr) {
   if (!usr)
      return null;
   if (usr.sysadmin) return ADMIN;
   var ms = this.get(usr._id.toString());
   if (!ms)
      return null;
   return ms.level;
}


/**
 * Performs a redirect to a single subdomain. This is useful, so that browsers just store
 * form values (e.g. username + password) for a single domain. For sites with their own
 * URL, no redirect will take place.
 *
 */
function forceSingleDomain() {
   // Perform redirect to a (constant) action.
   if (path.site && path.site != root.sys_frontSite &&
         !path.site.url && !app.properties["vhost." + path.site.alias]) {
      var obj = (root.sys_frontSite && root.sys_frontSite.online) ? root.sys_frontSite : root;
      if (!session.data.referrer) {
         session.data.referrer = req.data.http_referer ? req.data.http_referer : path.site.href();
      }
      if (!session.user && !session.data.locale && path.site) session.data.locale = path.site.getLocale();
      res.redirect(obj.members.href(req.action));
   }
   return;
}


/**
 * Performs a redirect to the MemberMgr of a single 'safe' (sub)domain, that can not be injected with
 * JavaScript. The sys_frontSite, resp. root is considered to be such a safe domain. But if any other
 * (non-trusted) site also runs under that domain, then its not safe anymore. This mechanism is necessary
 * to prevent XSS- and CSRF-type attacks (if users are allowed to insert un-sanitized strings).
 * The function should be called for any MemberMgr-action, that displays sensitive user-information.
 *
 */
function forceSafeDomain() {
   if (path.site != root.sys_frontSite) {
      var obj = (root.sys_frontSite && root.sys_frontSite.online) ? root.sys_frontSite : root;
      if (!session.data.referrer) {
         session.data.referrer = req.data.http_referer ? req.data.http_referer : (path.site ? path.site.href() : root.href());
      }
      if (!session.user && !session.data.locale && path.site) session.data.locale = path.site.getLocale();
      res.redirect(obj.members.href(req.action));
   }
   return;
}

 /**
  * Decides whether to close the current wizard. This may be a popUp,
  * or an inline wizard.
  *
  * @skin Global.closePopup
  * @param doClose  Boolean
  */
 function closeAndRedirect(doClose) {
    if (doClose) {
       if (session.data.referrer && !session.data.referrer.match(/members\/login/)) {
          var url = session.data.referrer;
          session.data.referrer = null;
       } else {
          var url = (res.handlers.site ? res.handlers.site : root).href();
       }
       if (req.data.popup) {
          renderSkin("closePopup", getRenderRedirectParam(url));
       } else {
          res.redirect(url);
       }
       return;
    }
 }
