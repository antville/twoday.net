app.modules.modjQuery = {
   title: "JQuery Integration",
   description: "adds jQuery to the html head",
   version: "1.0",
   state: "stable",
   url: "",
   author: "Franz Philipp Moser",
   authorEmail: "philipp.moser@knallgrau.at",
   authorUrl: "http://www.knallgrau.at",
   authorOrganisation: "knallgrau.at",
   copyright: "(c) forever knallgrau.at",
   license: "twoday",
   licenseUrl: "http://www.twoday.org",
   hasSiteSettings: true
};


/**
 * Renders the module's preferences.
 */
app.modules.modjQuery.renderPreferences = function() {
   Module.prototype.renderSetupSpacerLine();
   Module.prototype.renderSetupHeader({header: getMessage("modjQuery.wizard.admin.settingsTitle")});
   var options = [
      {value: "false", display: getMessage("generic.no")},
      {value: "true", display: getMessage("generic.yes")},
   ];
   var param = {
      label: getMessage("modjQuery.wizard.admin.editor"),
      field: Html.dropDownAsString({name: "modjQueryActive"}, options, this.preferences.getProperty("modjQueryActive"))
   };
   Module.prototype.renderSetupLine(param);
};


/**
 * Stores the modjQuery state
 */
app.modules.modjQuery.evalModulePreferences = function() {
   this.preferences.setProperty("modjQueryActive", "true".equals(req.data.modjQueryActive) ? "true" : "false");
};


app.modules.modjQuery.beforeRenderPageAddResponseHandlerAndHeader = function() {
   if (res.handlers.site && "true".equals(res.handlers.site.preferences.getProperty("modjQueryActive"))) {
      res.handlers.jquery = new JQuery();
      knallgrau.addToResponseHead(renderSkinAsString('javascripttag', {url: getSysFilesUrl() + 'js/jquery/jquery.min.js'}));
   }
};
knallgrau.Event.registerObserver("Site", "beforeRenderPage", app.modules.modjQuery.beforeRenderPageAddResponseHandlerAndHeader, null, "app.modules.modjQuery.beforeRenderPageAddResponseHandlerAndHeader");
