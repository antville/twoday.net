app.modules.modTwitter = {
   title: "Twitter Connector",
   description: "posts title and url to twitter",
   version: "1.0",
   state: "stable",
   url: "",
   author: "Franz Philipp Moser",
   authorEmail: "philipp.moser@knallgrau.at",
   authorUrl: "http://www.knallgrau.at",
   authorOrganisation: "knallgrau.at",
   copyright: "(c) 2009 knallgrau.at",
   license: "twoday",
   licenseUrl: "http://www.twoday.org",
   hasSiteSettings: true,
   isRestricted: true
};

/**
 * Renders the module's preferences.
 */
app.modules.modTwitter.renderPreferences = function() {
   Module.prototype.renderSetupSpacerLine();
   Module.prototype.renderSetupHeader({header: getMessage("modTwitter.wizard.admin.settingsTitle")});
   var options = [
      {value: "false", display: getMessage("generic.no")},
      {value: "true", display: getMessage("generic.yes")},
   ];
   var param = {
      label: getMessage("modTwitter.wizard.admin.editor"),
      field: Html.dropDownAsString({name: "modTwitterActive"}, options, this.preferences.getProperty("modTwitterActive"))
   };
   Module.prototype.renderSetupLine(param);
   param = {
         label: getMessage("modTwitterUserName.wizard.admin.settingsText"),
         field: Html.input({name: "modTwitterUserName", id: "modTwitterUserName", value: this.preferences.getProperty("modTwitterUserName") ? this.preferences.getProperty("modTwitterUserName") : ""})
      };
   Module.prototype.renderSetupLine(param);
};

/**
 * Stores the twitter username
 */
app.modules.modTwitter.evalModulePreferences = function() {
   this.preferences.setProperty("modTwitterActive", req.data.modTwitterActive);
   this.preferences.setProperty("modTwitterUserName", req.data.modTwitterUserName);
};

/**
 * renders edit story hook skin
 */
app.modules.modTwitter.renderStoryEdit = function(params) {
   var site = this.getSite();
   if (site.preferences.getProperty("modTwitterActive") == "true")
      this.renderSkin("twitterEditStory");
};

app.modules.modTwitter.sendToTwitter = function(story, user, passwd) {
   var twitterer = new knallgrau.Twitter({userId: user, userPassword: passwd});
   try {
      twitterer.updateStatus(story.renderSkinAsString("twitterStorytext"));
   } catch (e) {
      // do nothing for now
   }   
};

/**
 * only send to twitter when eval new Story
 */
app.modules.modTwitter.onEvalNewStory = function(params) {
   if (req.data && req.data.twitterSendTo != "1")
      return;
   // edit or new Story, only when new Story
   // try to post
   var site = this.getSite();
   if (!site)
      return;
   var user = site.preferences.getProperty("modTwitterUserName");
   var passwd = req.data.twitterpasswd;
   if (!user || !passwd)
      return;
   app.modules.modTwitter.sendToTwitter(params[0], user, passwd);
};

app.modules.modTwitter.onEvalStory = function(params) {
   if (req.data && req.data.twitterSendTo != "1")
      return;
   // edit or new Story, only when new Story
   // try to post
   var site = this.getSite();
   if (!site)
      return;
   var user = site.preferences.getProperty("modTwitterUserName");
   var passwd = req.data.twitterpasswd;
   if (!user || !passwd)
      return;
   app.modules.modTwitter.sendToTwitter(params[0], user, passwd);
};
