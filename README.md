# About Twoday

Twoday is the software running at Twoday.net, a german-speaking weblog hosting platform.

Twoday.net was originally hosted and developed by [Knallgrau](http://www.knallgrau.at). Since 2018 it is hosted and maintained by Antville.org.

The software is in large part based on Antville and powered by Helma, a lightning fast, highly scalable Java web application server.

## About Antville

Antville is an open source project aimed at the development of a high performance, feature rich weblog hosting software. It can easily host up to several thousands of sites (the number of weblogs is rather limited by the installation owner’s choice and server power than by software limitations).

Antville is entirely written in JavaScript (ECMAScript, to be precise) and based on Helma Object Publisher, a powerful and fast scriptable open source web application server (which itself is written in Java). Antville works with a relational database in the backend.

[Check out the project site for more information.](http://project.antville.org)

## About Helma Object Publisher

[Helma Object Publisher](http://helma.org) is a web application server driven by Java and programmed with JavaScript.

With Helma Object Publisher (sometimes simply referred to as Helma or Hop) you can define Objects and map them to a relational database table. These so-called HopObjects can be created, modified and deleted using a comfortable object/container model. Hence, no manual fiddling around with database code is necessary.

HopObjects are extended JavaScript objects which can be scripted using server-side JavaScript. Beyond the common JavaScript features, Helma provides special “skin” and template functionalities which facilitate the rendering of objects via a web interface.

Thanks to Helma’s relational database mapping technology, HopObjects create a hierarchical structure, the URL space of a Helma site. The parts between slashes in a Helma URL represent HopObjects (similar to the document tree in static sites). The Helma URL space can be thought of as an analogy to the Document Object Model (DOM) in client-side JavaScript.
