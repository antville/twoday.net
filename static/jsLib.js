/**
 * A compact JavaScript-Helper Libary for Cross Browser Scripting
 * Used for twoday / antville
 *
 * Written by Matthias Platzer at http://knallgrau.at
 * inspired by code found at various places in the net
 * especially by HTMLArea by Mihai Bazon http://students.infoiasi.ro/~mishoo
 *
 * use it as you need it
 * 
 * Nov 2023: updated to reflect current web standard levels (NeonWilderness)
 * 
 */

// used in: modBetterEditorJS.skin, mod(Listening|Reading|Watching)Setup.skin, modToolbarDropDowns.skin
addEvent = function (el, evname, func) {
  el.addEventListener(evname, func, true);
};

// used in: modBetterEditorJS.skin, mod(Listening|Reading|Watching)Setup.skin
stopEvent = function (evt) {
  evt.preventDefault();
  evt.stopPropagation();
};

// NOT used
addEvents = function (el, evs, func) {
  for (var i in evs) addEvent(el, evs[i], func);
};

// NOT used
removeEvent = function (el, evname, func) {
  el.removeEventListener(evname, func, true);
};

// NOT used
removeEvents = function (el, evs, func) {
  for (var i in evs) removeEvent(el, evs[i], func);
};

// used in: modBetterEditorJS.skin
removeClass = function (el, className) {
  el.classList.remove(className);
};

// used in: modBetterEditorJS.skin
addClass = function (el, className) {
  el.classList.add(className);
};

// NOT used
hasClass = function (el, className) {
  return el.classList.contains(className);
};

// NOT used
hitTest = function (ele1, ele2) {
  if (getAbsOffsetLeft(ele1) + ele1.offsetWidth < getAbsOffsetLeft(ele2)) return false;
  if (getAbsOffsetTop(ele1) + ele1.offsetHeight < getAbsOffsetTop(ele2)) return false;
  if (getAbsOffsetLeft(ele2) + ele2.offsetWidth < getAbsOffsetLeft(ele1)) return false;
  if (getAbsOffsetTop(ele2) + ele2.offsetHeight < getAbsOffsetTop(ele1)) return false;
  return true;
};

// NOT used
isUnderMouse = function (evt, ele) {
  var sl = window.scrollX ? window.scrollX : document.body.scrollLeft;
  var st = window.scrollY ? window.scrollY : document.body.scrollTop;

  var x = evt.clientX + sl;
  var y = evt.clientY + st;
  if (x < getAbsOffsetLeft(ele)) return false;
  if (y < getAbsOffsetTop(ele)) return false;
  if (x > getAbsOffsetLeft(ele) + ele.offsetWidth) return false;
  if (y > getAbsOffsetTop(ele) + ele.offsetHeight) return false;
  return true;
};

function getAbsOffsetTop(ele) {
  var offset = 0;
  var st = window.scrollY ? window.scrollY : document.body.scrollTop;
  do {
    offset += ele.offsetTop;
    if (window.getComputedStyle && window.getComputedStyle(ele, '').getPropertyValue('position') === 'fixed') {
      offset += st;
    }
    ele = ele.offsetParent;
  } while (ele != null);
  return offset;
}

function getAbsOffsetLeft(ele) {
  var offset = 0;
  var sl = window.scrollX ? window.scrollX : document.body.scrollLeft;
  do {
    offset += ele.offsetLeft;
    if (window.getComputedStyle && window.getComputedStyle(ele, '').getPropertyValue('position') == 'fixed') {
      offset += sl;
    }
    ele = ele.offsetParent;
  } while (ele != null);
  return offset;
}

// NOT used
// Cookie Functions
function ClientCookie() {
  if (document.cookie.length) {
    this.cookies = ' ' + document.cookie;
  }
}

ClientCookie.prototype.setCookie = function (key, value) {
  document.cookie = key + '=' + escape(value);
};

ClientCookie.prototype.getCookie = function (key) {
  if (this.cookies) {
    var start = this.cookies.indexOf(' ' + key + '=');
    if (start == -1) {
      return null;
    }
    var end = this.cookies.indexOf(';', start);
    if (end == -1) {
      end = this.cookies.length;
    }
    end -= start;
    var cookie = this.cookies.substr(start, end);
    return unescape(cookie.substr(cookie.indexOf('=') + 1, cookie.length - cookie.indexOf('=') + 1));
  } else {
    return null;
  }
};

// partially used in: modBetterEditorJS.skin
userAgent = navigator.userAgent.toLowerCase();
isIE = userAgent.includes('msie') && !userAgent.includes('opera');
isOpera = userAgent.includes('opera');
isMac = userAgent.includes('mac');
isMacIE = isIE && isMac;
isWinIE = isIE && !isMac;
isSafari = userAgent.includes('safari');
// navigator.product is now deprecated and always equals "Gecko" in all browsers
isGecko = /* navigator.product == 'Gecko' && */ !isSafari;

// partially used in: modBetterEditorJS.skin
KEY_TAB = 9;
KEY_REFRESH = 116; // F5
KEY_ENTER = 13;
TAB_INDENT = 2;
KEY_SPACE = 32;

// used in: mod(Listening|Reading|Watching)Setup.skin
function newXmlRequest() {
  return new XMLHttpRequest();
}
