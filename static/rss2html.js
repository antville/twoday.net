// A workaround for XSL-to-XHTML systems that don't
//  implement XSL 'disable-output-escaping="yes"'.
//
// sburke@cpan.org, Sean M. Burke.
//  - I hereby release this JavaScript code into the public domain.

var is_decoding;

function check_decoding () {
  var d = document.getElementById('cometestme');
  if(!d) {
    throw complaining("Can't find an id='cometestme' element?");
  } else if(!('textContent' in d)) {
    // It's a browser with a halfassed DOM implementation (like IE6)
    // that doesn't implement textContent!  Assume that if it's that
    // dumb, it probably doesn't implement disable-content-encoding.
  } else {
    ampy = d.textContent;

    if(ampy == undefined) throw complaining("'cometestme' element has undefined text content?!");
    if(ampy == ''       ) throw complaining("'cometestme' element has empty text content?!"    );

    if      (ampy == "\x26"	) { is_decoding =  true; }
    else if (ampy == "\x26amp;" ) { is_decoding = false; }
    else		     { throw complaining('Insane value: "' + ampy + '"!'); }
  }

}

function go_decoding () {

  check_decoding();

  if(is_decoding) {
    alert("No work needs doing -- already decoded!");
    return;
  }

  var to_decode = document.getElementsByName('decodeable');
  if(!( to_decode && to_decode.length )) {
    //alert("No work needs doing -- no elements to decode!");
    return;
  }

  var s;
  for(var i = to_decode.length - 1; i >= 0; i--) { 
    s = to_decode[i].textContent;

    if(
      s == undefined ||
      (s.indexOf('&') == -1 && s.indexOf('<') == -1)
    ) {
      // the null or markupless element needs no reworking
    } else {
      to_decode[i].innerHTML = s;  // that's the magic
    }
  }

  return;
}
