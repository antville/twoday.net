// FIXME: Entire modToolbar js/skin logic needs refactoring!!!
var openMenuDiv = null;
var doTryToCloseMenu = false;
var dropDownTimer;

function openMenu(id) {
  if (openMenuDiv) {
    openMenuDiv.style.visibility = 'hidden';
    openMenuDiv = null;
  }
  var dd = document.getElementById('modToolbar-dropdown-' + id);
  var te = document.getElementById('modToolbar-' + id);
  var tbSpacer = document.getElementById('modToolbar-spacer');
  dd.style.visibility = 'visible';
  dd.style.display = 'block';
  if (tbSpacer.offsetWidth < getAbsOffsetLeft(te) + 160) dd.style.left = tbSpacer.offsetWidth - 160 + 'px';
  else dd.style.left = getAbsOffsetLeft(te) - 5 + 'px';
  dd.style.top = getAbsOffsetTop(te) + te.offsetHeight + 2 + 'px';
  openMenuDiv = dd;
  window.clearTimeout(dropDownTimer);
  dropDownTimer = window.setTimeout('doTryToCloseMenu = true', 2500);
}

function tryToCloseMenu(evt) {
  try {
    if (openMenuDiv && !isUnderMouse(evt, openMenuDiv)) {
      openMenuDiv.style.visibility = 'hidden';
      openMenuDiv = null;
      window.clearTimeout(dropDownTimer);
    }
  } catch (e) {}
}

function tryToCloseMenuOnMove(evt) {
  try {
    if (doTryToCloseMenu) {
      doTryToCloseMenu = false;
      if (openMenuDiv && !isUnderMouse(evt, openMenuDiv)) {
        openMenuDiv.style.visibility = 'hidden';
        openMenuDiv.style.display = 'none';
        openMenuDiv = null;
        window.clearTimeout(dropDownTimer);
      }
    }
  } catch (e) {}
}

function repositionToolbar() {
  var tb = document.getElementById('modToolbar');
  tb.style.top = document.body.scrollTop + 'px';
}

function hideToolbar(open) {
  var tb = document.getElementById('modToolbar');
  var tbCollapsed = document.getElementById('modToolbar-collapsed');
  var tbSpacer = document.getElementById('modToolbar-spacer');
  tb.style.display = open ? 'block' : 'none';
  tbSpacer.style.display = open ? 'block' : 'none';
  tbCollapsed.style.display = open ? 'none' : 'block';
  // setCookie("tbisopen", open ? "1" : "-1");
}
