
// create namespace
var twodayNet = {};

// all functions that use JQuery go in here
try {
   /** the Comment extension
    * see http://plugins.jquery.com/project/htmlcomments
    */
   jQuery.fn.comments = function(i) {
      if (this.html() != null && this.html() != '') {
          var searchString = this.html().split("<!--");
          var commentArray = new Array();
          for (j in searchString) {
              if(typeof(searchString[j]) == 'string'){
                  if(searchString[j].indexOf("-->") != -1) {
                      commentArray.push(searchString[j].split("-->")[0].replace(/^\s+/,'').replace(/\s+$/,''));
                  }
              }
          }
          if (isNaN(Number(i)) || i == null) {
              return commentArray;
          } else {
              return commentArray[i];
          }
      } else {
          return null;
      }
   };
   /** the Comment extension
    * see http://plugins.jquery.com/project/htmlcomments
    */
   jQuery.fn.twodayFadeIn = function(speed, func) {
      speed = (speed || "normal")
      function _fadeIn() {
         $("table:first", this).animate({"opacity": 1}, speed, func);
      };
      this.slideDown(speed, _fadeIn);

      return this;
   };
   
   
   twodayNet.Skins = function () {
      // try to read skins from current html
      var rawSkinArray = $('#twodaynetJSkins').comments();
      var skins = {};
      if (rawSkinArray != null) {
         $.each(rawSkinArray, function(i, rawSkin) {
            // get Skinname
            var skinName = rawSkin.match(/^§§§\s*([\w\d-]+)\s*§§§/);
            if (skinName && skinName[1]) {
               skins[skinName[1]] = rawSkin.replace(/^§§§\s*([\w\d-]+)\s*§§§\s*/, ""); 
            }
         });
      }
      
      var that = this;
      
      this.getSkins = function() {
         return skins;
      };
      
      this.renderSkin = function(name, params) {
         renderedSkin = "";
         params = (params || {})
         if (skins[name]) {
            renderedSkin = skins[name];
            $.each(params, function(i, param) {
               // ###      test ###
               var regex = new RegExp("### " + i + " ###", "g");
               renderedSkin = renderedSkin.replace(regex, param);
            });
         }
         return renderedSkin;
      };
   };
} catch(e) {};

// everything else goes here