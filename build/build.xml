<?xml version="1.0"?>

<project name="twoday" default="usage" basedir=".">

   <!-- =================================================================== -->
   <!-- help on usage                                                       -->
   <!-- =================================================================== -->
   <target name="help" depends="usage"/>
   <target name="usage">
      <echo/>
      <echo/>
      <echo message="Twoday build instructions"/>
      <echo message="-------------------------------------------------------------"/>
      <echo/>
      <echo message=" available targets are:"/>
      <echo/>
      <echo message=" all        - generates the twoday and twoclick packages"/>
      <echo message=" twoday     - generates the twoday packages"/>
      <echo message=" twoclick   - generates the twoclick packages"/>
      <echo message=" transfer   - copy packages to download server"/>
      <echo message=" docs       - generates the API helmaDocs"/>
      <echo/>
      <echo message=" usage      - provides help on using the build tool (default)"/>
      <echo/>
      <echo message=" See comments inside the build.xml file for more details."/>
      <echo message="-------------------------------------------------------------"/>
      <echo/>
      <echo/>
   </target>


   <!-- =================================================================== -->
   <!-- initialize some variables                                           -->
   <!-- =================================================================== -->
   <target name="init">
      <property name="name" value="Twoday"/>
      <property name="year" value="2001-${year}"/>
      <property name="version" value="1.9.0"/>
      <property name="project" value="Twoday"/>
      <property name="build.compiler" value="classic"/>

      <property name="cvs.hop.root"
         value=":pserver:anonymous@194.232.104.95:/opt/cvs/hop"/>
      <property name="cvs.hop.tag" value="HEAD"/>

      <property name="home.dir" value=".."/>
      <property name="build.dir" value="${home.dir}/build"/>
      <property name="build.code" value="${home.dir}/code"/>
      <property name="build.static" value="${home.dir}/static"/>
      <property name="build.docs" value="${home.dir}/docs"/>

      <property name="build.work" value="${build.dir}/work"/>
      <property name="build.dist" value="${build.dir}/dist"/>
      <property name="build.extra" value="${build.dir}/extra"/>
      <property name="build.lib" value="${build.dir}/lib"/>

      <property name="package.name" value="${project}-${version}"/>
      <property name="twoclick.name" value="TwoClick-${version}"/>

      <property name="scp.server" value="212.41.237.58"/>
      <property name="scp.path" value="/home/twoday_net"/>

      <property name="debug" value="on"/>
      <property name="optimize" value="on"/>
      <property name="deprecation" value="off"/>

      <tstamp/>

      <filter token="year" value="${year}"/>
      <filter token="version" value="${version}"/>
      <filter token="date" value="${TODAY}"/>

   </target>


   <!-- =================================================================== -->
   <!-- generate both, the twoday and twoclick packages                   -->
   <!-- =================================================================== -->
   <target name="all">
      <property name="nocleanup" value="true"/>
      <antcall target="docs"/>
      <antcall target="twoday"/>
      <antcall target="twoclick"/>
      <antcall target="cleanup"/>
      <!-- FIXME: currently don't know about a way to call target cleanup here -->
      <delete dir="${build.work}"/>
   </target>


   <!-- =================================================================== -->
   <!-- generate the twoday distribution packages                         -->
   <!-- =================================================================== -->
   <target name="twoday" depends="docs">
      <antcall target="package-zip">
         <param name="filename" value="${package.name}"/>
         <param name="path" value="${home.dir}"/>
      </antcall>
      <antcall target="package-tgz">
         <param name="filename" value="${package.name}"/>
         <param name="path" value="${home.dir}"/>
      </antcall>
      <antcall target="cleanup"/>
   </target>


   <!-- =================================================================== -->
   <!-- generate the twoclick distribution packages                         -->
   <!-- =================================================================== -->
   <target name="twoclick" depends="docs">
      <copy todir="${build.work}/">
         <fileset dir="${build.extra}"
            includes="twoclick.sh twoclick.bat apps.properties
                      launcher.jar server.properties"/>
      </copy>
      <copy file="${build.extra}/db.properties"
         todir="${build.work}/apps/${project}/code"
         overwrite="true"/>

      <cvs cvsRoot="${cvs.hop.root}"
         command="export"
         tag="${cvs.hop.tag}"
         package="helma/lib helma/licenses helma/license.txt"
         dest="${build.work}"/>

      <copy todir="${build.work}/lib">
         <fileset dir="${build.work}/helma/lib" includes="*.jar"/>
      </copy>
      <copy file="${build.extra}/helma.jar" todir="${build.work}/lib"/>

      <copy todir="${build.work}/licenses">
         <fileset dir="${build.work}/helma/licenses"/>
      </copy>
      <copy file="${build.work}/helma/license.txt"
         tofile="${build.work}/licenses/helma.txt"/>
      <move file="${build.work}/apps/${project}/license.txt"
         tofile="${build.work}/licenses/twoday.txt"/>

      <delete dir="${build.work}/helma"/>

      <antcall target="package-zip">
         <param name="filename" value="${twoclick.name}"/>
         <param name="path" value="${build.work}"/>
      </antcall>
      <antcall target="package-tgz">
         <param name="filename" value="${twoclick.name}"/>
         <param name="path" value="${build.work}"/>
      </antcall>
      <antcall target="cleanup"/>
   </target>


   <!-- =================================================================== -->
   <!-- copy packages to download server                                    -->
   <!-- =================================================================== -->
   <target name="transfer" depends="init">
      <input message="User: " addproperty="scp.user"/>
      <input message="Password: " addproperty="scp.pass"/>
      <scp trust="yes" todir="${scp.user}:${scp.pass}@${scp.server}:${scp.path}">
         <fileset dir="${build.dist}" includes="${project}-${version}.*"/>
      </scp>
      <scp trust="yes" todir="${scp.user}:${scp.pass}@${scp.server}:${scp.path}/twoclick">
         <fileset dir="${build.dist}" includes="twoclick-${version}.*"/>
      </scp>
   </target>


   <!-- =================================================================== -->
   <!-- generate docs                                                       -->
   <!-- =================================================================== -->
   <target name="docs" depends="init">
   </target>


   <!-- =================================================================== -->
   <!-- Packages the work directory with TAR-GZIP                           -->
   <!-- needs parameter ${filename} for final dist-file                     -->
   <!-- =================================================================== -->
   <target name="package-tgz" depends="init">
      <mkdir dir="${build.dist}" />
      <fixcrlf srcdir="${path}" eol="lf" eof="remove"
         includes="**/*.txt, **/*.properties, **/*.hac, **/*.js, **/*.skin, **/.xml, **/.sh" />
      <tar tarfile="${build.dist}/${filename}.tar" basedir="${path}" excludes="**">
         <tarfileset prefix="${filename}" dir="${path}" mode="755">
            <include name="**/*.sh"/>
         </tarfileset>
         <tarfileset prefix="${filename}" dir="${path}">
            <include name="**"/>
            <exclude name="**/*.sh"/>
         </tarfileset>
      </tar>
      <gzip zipfile="${build.dist}/${filename}.tgz" src="${build.dist}/${filename}.tar"/>
      <delete file="${build.dist}/${filename}.tar"/>
   </target>


   <!-- =================================================================== -->
   <!-- Packages the work directory with ZIP                                -->
   <!-- needs parameter ${filename} for final dist-file                     -->
   <!-- =================================================================== -->
   <target name="package-zip" depends="init">
      <mkdir dir="${build.dist}" />
      <fixcrlf srcdir="${path}" eol="crlf"
         includes="**/*.txt, **/*.properties, **/*.hac, **/*.js, **/*.skin, **/*.xml, **/.bat" />
      <zip zipfile="${build.dist}/${filename}.zip">
         <zipfileset dir="${path}" prefix="${filename}" includes="**" />
      </zip>
   </target>


   <!-- =================================================================== -->
   <!-- clean up the mess                                                   -->
   <!-- =================================================================== -->
   <target name="cleanup" depends="init" unless="nocleanup">
      <delete dir="${build.work}"/>
   </target>

</project>
