
#---------------------
# Create the database
#---------------------

#CREATE DATABASE db_twoday_net;
#USE db_twoday_net;


#-----------------------------------------
# Create a user with necessary privileges
#-----------------------------------------

#GRANT SELECT,INSERT,UPDATE,DELETE ON db_twoday_net.* TO twoday@localhost IDENTIFIED BY 'twoday';



#-----------------------------
# Table structure for AV_ROOT
#-----------------------------

create table AV_ROOT (
   ROOT_ID int unsigned not null,
   ROOT_F_SITE mediumint(10),
   ROOT_F_LAYOUT mediumint(10),
   ROOT_PREFERENCES mediumtext,
   primary key (ROOT_ID)
);

#----------------------------
# records for table AV_ROOT
#----------------------------

insert into AV_ROOT (ROOT_ID, ROOT_F_SITE, ROOT_F_LAYOUT, ROOT_PREFERENCES) values (0, null, null, null);


#------------------------------
# Table structure for AV_MODULE
#------------------------------

create table AV_MODULE (
   MODULE_ID int unsigned not null auto_increment,
   MODULE_NAME tinytext,
   MODULE_TITLE tinytext,
   MODULE_DESCRIPTION text,
   MODULE_URL tinytext,
   MODULE_VERSION tinytext,
   MODULE_STATE tinytext,
   MODULE_COPYRIGHT tinytext,
   MODULE_LICENSE tinytext,
   MODULE_LICENSEURL tinytext,
   MODULE_LICENSETEXT text,
   MODULE_ISLICENSEACCEPTED tinyint,
   MODULE_AUTHOR tinytext,
   MODULE_AUTHORURL tinytext,
   MODULE_AUTHOREMAIL tinytext,
   MODULE_AUTHORORGANISATION tinytext,
   MODULE_ISSIDEBAR tinyint,
   MODULE_TYPE tinyint,
   MODULE_ISACTIVATED tinyint,
   MODULE_ISINITIALIZED tinyint,
   MODULE_ISRESTRICTED tinyint,
   MODULE_HASSITESETTINGS tinyint,
   MODULE_INSTALLEDFILES text,
   MODULE_CREATETIME datetime,
   MODULE_F_USER_MODIFIER mediumint(10),
   MODULE_MODIFYTIME datetime,
   MODULE_PREFERENCES text,
   primary key (MODULE_ID)
);


#------------------------------
# Table structure for AV_ACCESSLOG
#------------------------------

create table AV_ACCESSLOG (
   ACCESSLOG_ID int unsigned not null auto_increment,
   ACCESSLOG_F_SITE mediumint(10),
   ACCESSLOG_F_TEXT mediumint(10),
   ACCESSLOG_REFERRER text,
   ACCESSLOG_IP varchar(255),
   ACCESSLOG_BROWSER varchar(255),
   ACCESSLOG_DATE timestamp,
   primary key (ACCESSLOG_ID)
);

#---------------------------
# Indexes on table AV_ACCESSLOG
#---------------------------

create index IDX_ACCESSLOG_F_TEXT on AV_ACCESSLOG (ACCESSLOG_F_TEXT);
create index IDX_ACCESSLOG_MIXED on AV_ACCESSLOG (ACCESSLOG_F_SITE,ACCESSLOG_DATE);

#----------------------------
# Table structure for AV_CHOICE
#----------------------------

create table AV_CHOICE (
   CHOICE_ID mediumint(10) not null,
   CHOICE_F_POLL mediumint(10),
   CHOICE_TITLE varchar(255),
   CHOICE_CREATETIME datetime,
   CHOICE_MODIFYTIME datetime,
   primary key (CHOICE_ID)
);

#----------------------------
# Indexes on table AV_CHOICE
#----------------------------

CREATE INDEX IDX_CHOICE_F_POLL ON AV_CHOICE (CHOICE_F_POLL);

#----------------------------
# Table structure for AV_FILE
#----------------------------
create table AV_FILE (
  FILE_ID mediumint(10) not null,
  FILE_F_SITE mediumint(10),
  FILE_ALIAS tinytext,
  FILE_MIMETYPE tinytext,
  FILE_FILENAME tinytext,
  FILE_FILESIZE mediumint(10),
  FILE_DESCRIPTION text,
  FILE_REQUESTCNT mediumint(10),
  FILE_CREATETIME datetime,
  FILE_F_USER_CREATOR mediumint(10),
  FILE_MODIFYTIME datetime,
  FILE_F_USER_MODIFIER mediumint(10),
  primary key (FILE_ID)
);

#----------------------------
# Indexes on table AV_FILE
#----------------------------

CREATE INDEX IDX_FILE_F_SITE ON AV_FILE (FILE_F_SITE);
CREATE INDEX IDX_FILE_ALIAS ON AV_FILE (FILE_ALIAS(20));
CREATE INDEX IDX_FILE_F_USER_CREATOR ON AV_FILE (FILE_F_USER_CREATOR);

#----------------------------
# Table structure for AV_IMAGE
#----------------------------

create table AV_IMAGE (
   IMAGE_ID mediumint(10) not null,
   IMAGE_F_SITE mediumint(10),
   IMAGE_F_LAYOUT mediumint(10),
   IMAGE_F_IMAGE_PARENT mediumint(10),
   IMAGE_F_IMAGE_THUMB mediumint(10),
   IMAGE_PROTOTYPE varchar(20),
   IMAGE_ALIAS tinytext,
   IMAGE_MIMETYPE tinytext,
   IMAGE_FILENAME tinytext,
   IMAGE_FILESIZE mediumint(10),
   IMAGE_TOPIC varchar(255),
   IMAGE_WIDTH mediumint(4),
   IMAGE_HEIGHT mediumint(4),
   IMAGE_ALTTEXT text,
   IMAGE_CREATETIME datetime,
   IMAGE_F_USER_CREATOR mediumint(10),
   IMAGE_MODIFYTIME datetime,
   IMAGE_F_USER_MODIFIER mediumint(10),
   primary key (IMAGE_ID)
);

#----------------------------
# Indexes on table AV_IMAGE
#----------------------------

CREATE INDEX IDX_IMAGE_F_USER_CREATOR ON AV_IMAGE (IMAGE_F_USER_CREATOR);
CREATE INDEX IDX_IMAGE_MIXED ON AV_IMAGE (IMAGE_F_SITE,IMAGE_ALIAS(20),IMAGE_F_IMAGE_PARENT);
CREATE INDEX IDX_IMAGE_MIXED_LAYOUT ON AV_IMAGE (IMAGE_F_LAYOUT,IMAGE_ALIAS(20),IMAGE_F_IMAGE_PARENT);

#----------------------------
# records for table AV_IMAGE
#----------------------------

alter table AV_IMAGE change column IMAGE_ID IMAGE_ID mediumint(10) not null auto_increment;
insert into AV_IMAGE (IMAGE_PROTOTYPE,IMAGE_ALIAS,IMAGE_FILENAME,IMAGE_MIMETYPE,IMAGE_WIDTH,IMAGE_HEIGHT,IMAGE_ALTTEXT,IMAGE_CREATETIME) values ('LayoutImage','big','big.gif','image/gif',404,53,'antville.org',NOW());
insert into AV_IMAGE (IMAGE_PROTOTYPE,IMAGE_ALIAS,IMAGE_FILENAME,IMAGE_MIMETYPE,IMAGE_WIDTH,IMAGE_HEIGHT,IMAGE_CREATETIME) values ('LayoutImage','headbg','headbg.gif','image/gif',3,52,NOW());
insert into AV_IMAGE (IMAGE_PROTOTYPE,IMAGE_ALIAS,IMAGE_FILENAME,IMAGE_MIMETYPE,IMAGE_WIDTH,IMAGE_HEIGHT,IMAGE_ALTTEXT,IMAGE_CREATETIME) values ('LayoutImage','dot','dot.gif','image/gif',30,30,'dots',NOW());
insert into AV_IMAGE (IMAGE_PROTOTYPE,IMAGE_ALIAS,IMAGE_FILENAME,IMAGE_MIMETYPE,IMAGE_WIDTH,IMAGE_HEIGHT,IMAGE_ALTTEXT,IMAGE_CREATETIME) values ('LayoutImage','bullet','bullet.gif','image/gif',3,10,'bullet',NOW());
insert into AV_IMAGE (IMAGE_PROTOTYPE,IMAGE_ALIAS,IMAGE_FILENAME,IMAGE_MIMETYPE,IMAGE_WIDTH,IMAGE_HEIGHT,IMAGE_ALTTEXT,IMAGE_CREATETIME) values ('LayoutImage','webloghead','webloghead.gif','image/gif',404,53,'head',NOW());
insert into AV_IMAGE (IMAGE_PROTOTYPE,IMAGE_ALIAS,IMAGE_FILENAME,IMAGE_MIMETYPE,IMAGE_WIDTH,IMAGE_HEIGHT,IMAGE_CREATETIME) values ('LayoutImage','preview200','preview200.gif','image/gif',200,150,NOW());
select @imageID:=IMAGE_ID from AV_IMAGE where IMAGE_ALIAS = 'preview200';
insert into AV_IMAGE (IMAGE_PROTOTYPE,IMAGE_ALIAS,IMAGE_FILENAME,IMAGE_MIMETYPE,IMAGE_WIDTH,IMAGE_HEIGHT,IMAGE_CREATETIME,IMAGE_F_IMAGE_PARENT) values ('LayoutImage','preview200','preview200_small.gif','image/gif',100,75, NOW(),@imageID);
insert into AV_IMAGE (IMAGE_PROTOTYPE,IMAGE_ALIAS,IMAGE_FILENAME,IMAGE_MIMETYPE,IMAGE_WIDTH,IMAGE_HEIGHT,IMAGE_CREATETIME) values ('LayoutImage','preview_full','preview_full.gif','image/gif',750,650,NOW());
select @imageID:=IMAGE_ID from AV_IMAGE where IMAGE_ALIAS = 'preview_full';
insert into AV_IMAGE (IMAGE_PROTOTYPE,IMAGE_ALIAS,IMAGE_FILENAME,IMAGE_MIMETYPE,IMAGE_WIDTH,IMAGE_HEIGHT,IMAGE_CREATETIME,IMAGE_F_IMAGE_PARENT) values ('LayoutImage','preview_full','preview_full_small.gif','image/gif',100,87, NOW(),@imageID);
alter table AV_IMAGE change column IMAGE_ID IMAGE_ID mediumint(10) not null;

#-------------------------------
# Table structure for AV_MEMBERSHIP
#-------------------------------
create table AV_MEMBERSHIP (
   MEMBERSHIP_ID mediumint(10) not null,
   MEMBERSHIP_F_SITE mediumint(10),
   MEMBERSHIP_F_USER mediumint(10),
   MEMBERSHIP_LEVEL mediumint(10),
   MEMBERSHIP_CREATETIME datetime,
   MEMBERSHIP_MODIFYTIME datetime,
   MEMBERSHIP_F_USER_MODIFIER mediumint(10),
   primary key (MEMBERSHIP_ID)
);

#----------------------------
# Indexes on table AV_MEMBERSHIP
#----------------------------

CREATE INDEX IDX_MEMBERSHIP_F_SITE ON AV_MEMBERSHIP (MEMBERSHIP_F_SITE);
CREATE INDEX IDX_MEMBERSHIP_F_USER ON AV_MEMBERSHIP (MEMBERSHIP_F_USER);
CREATE INDEX IDX_MEMBERSHIP_LEVEL ON AV_MEMBERSHIP (MEMBERSHIP_LEVEL);

#----------------------------
# Table structure for AV_POLL
#----------------------------

create table AV_POLL (
   POLL_ID mediumint(10) not null,
   POLL_F_SITE mediumint(10),
   POLL_TITLE varchar(255),
   POLL_QUESTION text,
   POLL_ISONLINE tinyint(1),
   POLL_CLOSED tinyint(4),
   POLL_CLOSETIME datetime,
   POLL_CREATETIME datetime,
   POLL_F_USER_CREATOR mediumint(10),
   POLL_MODIFYTIME datetime,
   POLL_F_USER_MODIFIER mediumint(10),
   primary key (POLL_ID)
);

#----------------------------
# Indexes on table AV_POLL
#----------------------------

CREATE INDEX IDX_POLL_F_SITE ON AV_POLL (POLL_F_SITE);
CREATE INDEX IDX_POLL_F_USER_CREATOR ON AV_POLL (POLL_F_USER_CREATOR);

#------------------------------
# Table structure for AV_LAYOUT
#------------------------------

create table AV_LAYOUT (
   LAYOUT_ID mediumint(10) not null,
   LAYOUT_ALIAS varchar(128),
   LAYOUT_TITLE varchar(128),
   LAYOUT_LOCALE varchar(64) null,
   LAYOUT_F_SITE mediumint(10),
   LAYOUT_F_LAYOUT_PARENT mediumint(10),
   LAYOUT_F_LAYOUT_LOCALEPARENT mediumint(10),
   LAYOUT_PREFERENCES mediumtext,
   LAYOUT_MESSAGES mediumtext,
   LAYOUT_DESCRIPTION text,
   LAYOUT_CREATETIME datetime,
   LAYOUT_MODIFYTIME datetime,
   LAYOUT_F_USER_CREATOR mediumint(10),
   LAYOUT_F_USER_MODIFIER mediumint(10),
   LAYOUT_SHAREABLE tinyint(8),
   LAYOUT_ISIMPORT tinyint(1),
   primary key (LAYOUT_ID)
);

# create an initial layout object
alter table AV_LAYOUT change column LAYOUT_ID LAYOUT_ID mediumint(10) not null auto_increment;
insert into AV_LAYOUT (LAYOUT_ALIAS, LAYOUT_TITLE, LAYOUT_PREFERENCES, LAYOUT_DESCRIPTION, LAYOUT_SHAREABLE)
values ('default', 'default layout', '<?xml version="1.0" encoding="UTF-8"?>\r\n<xmlroot xmlns:hop="http://www.helma.org/docs/guide/features/database">\r\n  <hopobject id="t234" name="HopObject" prototype="HopObject" created="1069430202375" lastModified="1069430202375">\r\n    <smallcolor>959595</smallcolor>\r\n    <textcolor>000000</textcolor>\r\n    <vlinkcolor>ff4040</vlinkcolor>\r\n    <titlecolor>d50000</titlecolor>\r\n    <smallsize>11px</smallsize>\r\n    <alinkcolor>ff4040</alinkcolor>\r\n    <textsize>13px</textsize>\r\n    <titlesize>15px</titlesize>\r\n    <linkcolor>ff4040</linkcolor>\r\n    <smallfont>Verdana, Arial, Helvetica, sans-serif</smallfont>\r\n    <textfont>Verdana, Helvetica, Arial, sans-serif</textfont>\r\n    <titlefont>Verdana, Helvetica, Arial, sans-serif</titlefont>\r\n    <bgcolor>ffffff</bgcolor>\r\n  </hopobject>\r\n</xmlroot>', 'The default layout of twoday, which just uses the file-based skins.', 1);
alter table AV_LAYOUT change column LAYOUT_ID LAYOUT_ID mediumint(10) not null;
# mark all default images as layout images of default layout
select @layoutID:=LAYOUT_ID from AV_LAYOUT where LAYOUT_ALIAS = 'default' and LAYOUT_F_SITE is null and LAYOUT_F_LAYOUT_PARENT is null;
update AV_IMAGE set IMAGE_F_LAYOUT = @layoutID where IMAGE_F_SITE is null;

#----------------------------
# Indexes on table AV_LAYOUT
#----------------------------

CREATE INDEX IDX_LAYOUT_ALIAS ON AV_LAYOUT (LAYOUT_ALIAS);
CREATE INDEX IDX_LAYOUT_F_SITE ON AV_LAYOUT (LAYOUT_F_SITE);
CREATE INDEX IDX_LAYOUT_F_LAYOUT_PARENT ON AV_LAYOUT (LAYOUT_F_LAYOUT_PARENT);

#----------------------------
# Table structure for AV_SKIN
#----------------------------

create table AV_SKIN (
   SKIN_ID mediumint(10) not null,
   SKIN_F_LAYOUT mediumint(10),
   SKIN_PROTOTYPE tinytext,
   SKIN_NAME tinytext,
   SKIN_TITLE varchar(128),
   SKIN_DESCRIPTION varchar(255),
   SKIN_NEEDSTRANSLATION tinyint(1),
   SKIN_ISCUSTOM tinyint(1),
   SKIN_MODULE tinytext,
   SKIN_SOURCE mediumtext,
   SKIN_CREATETIME datetime,
   SKIN_F_USER_CREATOR mediumint(10),
   SKIN_MODIFYTIME datetime,
   SKIN_F_USER_MODIFIER mediumint(10),
   primary key (SKIN_ID)
);

#----------------------------
# Indexes on table AV_SKIN
#----------------------------

CREATE INDEX IDX_SKIN_MIXED ON AV_SKIN (SKIN_F_LAYOUT,SKIN_PROTOTYPE(10),SKIN_NAME(10));

#----------------------------
# Table structure for AV_SYSLOG
#----------------------------

create table AV_SYSLOG (
  SYSLOG_ID mediumint(10) not null,
  SYSLOG_TYPE tinytext null,
  SYSLOG_OBJECT tinytext null,
  SYSLOG_ENTRY text null,
  SYSLOG_CREATETIME datetime null,
  SYSLOG_F_USER_CREATOR mediumint(10) null,
  primary key (SYSLOG_ID)
);

CREATE INDEX IDX_SYSLOG_TYPE ON AV_SYSLOG (SYSLOG_TYPE(10));
CREATE INDEX IDX_SYSLOG_OBJECT ON AV_SYSLOG (SYSLOG_OBJECT(10));

#----------------------------
# Table structure for AV_TEXT
#----------------------------

create table AV_TEXT (
   TEXT_ID mediumint(10) not null,
   TEXT_F_SITE mediumint(10),
   TEXT_DAY varchar(10),
   TEXT_TOPIC varchar(255),
   TEXT_PROTOTYPE varchar(20),
   TEXT_F_TEXT_STORY mediumint(10),
   TEXT_F_TEXT_PARENT mediumint(10),
   TEXT_ALIAS varchar(255),
   TEXT_TITLE text,
   TEXT_URL_ID varchar(70) null default null,
   TEXT_TEXT text,
   TEXT_CONTENT text,
   TEXT_ISONLINE tinyint(4),
   TEXT_PUBLISH_TO_TIME_STATE tinyint,
   TEXT_EDITABLEBY tinyint(4),
   TEXT_HASDISCUSSIONS tinyint(1),
   TEXT_PREFERENCES text,
   TEXT_CREATETIME datetime,
   TEXT_F_USER_CREATOR mediumint(10),
   TEXT_MODIFYTIME datetime,
   TEXT_F_USER_MODIFIER mediumint(10),
   TEXT_CREATOR_NAME tinytext,
   TEXT_CREATOR_EMAIL tinytext,
   TEXT_CREATOR_URL tinytext,
   TEXT_READS mediumint(10),
   TEXT_IPADDRESS varchar(255),
   TEXT_WAITING_FOR_MODERATION TINYINT(1) UNSIGNED NULL,
   primary key (TEXT_ID)
);

#----------------------------
# Indexes on table AV_TEXT
#----------------------------

CREATE INDEX IDX_TEXT_F_TEXT_STORY ON AV_TEXT (TEXT_F_TEXT_STORY);
CREATE INDEX IDX_TEXT_F_TEXT_PARENT ON AV_TEXT (TEXT_F_TEXT_PARENT);
CREATE INDEX IDX_TEXT_F_USER_CREATOR ON AV_TEXT (TEXT_F_USER_CREATOR);
CREATE INDEX IDX_TEXT_MIXED_ALL ON AV_TEXT (TEXT_F_SITE,TEXT_MODIFYTIME,TEXT_ISONLINE,TEXT_PROTOTYPE,TEXT_ID);
CREATE INDEX IDX_TEXT_MIXED_TOPIC ON AV_TEXT (TEXT_F_SITE,TEXT_TOPIC);
CREATE INDEX IDX_TEXT_MIXED_DAY ON AV_TEXT (TEXT_F_SITE,TEXT_DAY);
CREATE INDEX IDX_TEXT_MIXED_STORIES ON AV_TEXT (TEXT_F_SITE,TEXT_PROTOTYPE,TEXT_ISONLINE,TEXT_CREATETIME,TEXT_ID,TEXT_DAY);
CREATE INDEX IDX_TEXT_PUBLISH_TO_TIME_STATE ON AV_TEXT (TEXT_PUBLISH_TO_TIME_STATE);
ALTER TABLE AV_TEXT ADD INDEX IDX_TEXT_URL_ID ( TEXT_F_SITE , TEXT_URL_ID ( 10 ) );
#----------------------------
# Table structure for AV_USER
#----------------------------

create table AV_USER (
   USER_ID mediumint(10) not null,
   USER_TYPE tinytext,
   USER_NAME tinytext,
   USER_PASSWORD tinytext,
   USER_EMAIL tinytext,
   USER_EMAIL_ISPUBLIC tinyint(1),
   USER_EMAIL_ISCONFIRMED tinyint(1),
   USER_EMAIL_LASTCONFIRMED datetime,
   USER_EMAIL_CHANGED datetime,
   USER_EMAIL_HISTORY tinytext,
   USER_URL tinytext,
   USER_PREFERENCES mediumtext,
   USER_REGISTERED datetime,
   USER_LASTVISIT datetime,
   USER_IPADDRESS tinytext,
   USER_ISBLOCKED tinyint(1),
   USER_BLOCKUSER mediumint(10),
   USER_BLOCKTIME datetime,
   USER_ISTRUSTED tinyint(1),
   USER_ISSYSADMIN tinyint(1),
   USER_AUTH_TYPE tinytext,
   USER_AUTH_USER tinytext,
   USER_AUTH_DATA tinytext,
   USER_FAILED_LOGIN_COUNT tinyint unsigned,
   primary key (USER_ID)
);

#----------------------------
# Indexes on table AV_USER
#----------------------------

CREATE INDEX IDX_USER_NAME ON AV_USER (USER_NAME(20));
CREATE INDEX IDX_USER_ISBLOCKED ON AV_USER (USER_ISBLOCKED);
CREATE INDEX IDX_USER_ISTRUSTED ON AV_USER (USER_ISTRUSTED);
CREATE INDEX IDX_USER_ISSYSADMIN ON AV_USER (USER_ISSYSADMIN);

#----------------------------
# Table structure for AV_VOTE
#----------------------------

create table AV_VOTE (
   VOTE_ID mediumint(10) not null,
   VOTE_F_POLL mediumint(10),
   VOTE_F_USER mediumint(10),
   VOTE_F_CHOICE mediumint(10),
   VOTE_CREATETIME datetime,
   VOTE_MODIFYTIME datetime,
   primary key (VOTE_ID)
);

#----------------------------
# Indexes on table AV_VOTE
#----------------------------

CREATE INDEX IDX_VOTE_F_POLL ON AV_VOTE (VOTE_F_POLL);
CREATE INDEX IDX_VOTE_F_USER ON AV_VOTE (VOTE_F_USER);
CREATE INDEX IDX_VOTE_F_CHOICE ON AV_VOTE (VOTE_F_CHOICE);

#----------------------------
# Table structure for AV_SITE
#----------------------------
create table AV_SITE (
   SITE_ID mediumint(10) not null,
   SITE_TITLE tinytext,
   SITE_ALIAS tinytext,
   SITE_URL tinytext,
   SITE_EMAIL tinytext,
   SITE_F_LAYOUT mediumint(10),
   SITE_ISONLINE tinyint(1),
   SITE_ISBLOCKED tinyint(1),
   SITE_BLOCKUSER mediumint(10),
   SITE_BLOCKTIME datetime,
   SITE_ISTRUSTED tinyint(1),
   SITE_LASTUPDATE datetime,
   SITE_LASTPOSTING datetime,
   SITE_LASTOFFLINE datetime,
   SITE_LASTBLOCKWARN datetime,
   SITE_LASTDELWARN datetime,
   SITE_LASTPING datetime,
   SITE_ENABLEPING tinyint(1),
   SITE_SHOW tinyint(1),
   SITE_PREFERENCES mediumtext,
   SITE_DISKUSAGE int unsigned,
   SITE_CREATETIME datetime,
   SITE_F_USER_CREATOR mediumint(10),
   SITE_MODIFYTIME datetime,
   SITE_F_USER_MODIFIER mediumint(10),
   primary key (SITE_ID)
);

#----------------------------
# Indexes on table AV_SITE
#----------------------------

CREATE INDEX IDX_SITE_ALIAS ON AV_SITE (SITE_ALIAS(20));
CREATE INDEX IDX_SITE_ISONLINE ON AV_SITE (SITE_ISONLINE);
CREATE INDEX IDX_SITE_ISBLOCKED ON AV_SITE (SITE_ISBLOCKED);
CREATE INDEX IDX_SITE_ENABLEPING ON AV_SITE (SITE_ENABLEPING);
CREATE INDEX IDX_SITE_LASTPING ON AV_SITE (SITE_LASTPING);
CREATE INDEX IDX_SITE_F_USER_CREATOR ON AV_SITE (SITE_F_USER_CREATOR);

#----------------------------
# Table structure for AV_LOOKUP
#----------------------------
create table AV_LOOKUP (
   LOOKUP_ID mediumint(10) unsigned not null auto_increment,
   LOOKUP_TYPE tinytext,
   LOOKUP_KEY tinytext,
   LOOKUP_VALUE tinytext,
   LOOKUP_CREATETIME datetime,
   primary key (LOOKUP_ID)
);

#----------------------------
# Indexes on table AV_LOOKUP
#----------------------------

CREATE INDEX IDX_LOOKUP_KEY ON AV_LOOKUP (LOOKUP_TYPE(6), LOOKUP_KEY(6));


#---------------------------#
#                           #
#      CUSTOM SECTION       #
#                           #
#---------------------------#

alter table AV_SITE add column SITE_TYPE tinytext;
alter table AV_SITE add column SITE_TDY_BILLING_PAIDUNTIL datetime;

CREATE TABLE TDY_BILLING (
   BILLING_ID mediumint(10) NOT NULL default '0',
   BILLING_TYPE tinytext default NULL,
   BILLING_INVOICEID int unsigned default NULL,
   BILLING_TID int unsigned default NULL,
   BILLING_MPAYTID int unsigned default NULL,
   BILLING_PACKAGE tinytext default NULL,
   BILLING_AMOUNT float default NULL,
   BILLING_CURRENCY tinytext default NULL,
   BILLING_PAYMENTTYPE tinytext default NULL,
   BILLING_CCBRAND tinytext default NULL,
   BILLING_DESCRIPTION text default NULL,
   BILLING_F_SITE mediumint(10) default NULL,
   BILLING_F_CREATOR mediumint(10) default NULL,
   BILLING_CREATETIME datetime default NULL,
   BILLING_PREFERENCES mediumtext null,
   PRIMARY KEY(BILLING_ID)
);
CREATE INDEX IDX_TDY_BILLING_F_SITE ON TDY_BILLING (BILLING_F_SITE);
CREATE INDEX IDX_TDY_BILLING_F_CREATOR ON TDY_BILLING (BILLING_F_CREATOR);

CREATE TABLE TDY_PROMO (
   PROMO_ID mediumint(10) NOT NULL default '0',
   PROMO_CODE varchar(255) default NULL,
   PROMO_TYPE varchar(255) default NULL,
   PROMO_MONTHS mediumint(10) default NULL,
   PROMO_VALUE float default NULL,
   PROMO_DESCRIPTION mediumtext default NULL,
   PROMO_USED tinyint(1) default NULL,
   PROMO_VALIDUNTIL datetime default NULL,
   PROMO_F_SITE mediumint(10) default NULL,
   PROMO_F_USER mediumint(10) default NULL,
   PROMO_F_CREATOR mediumint(10) default NULL,
   PROMO_CREATETIME datetime default NULL,
   PRIMARY KEY(PROMO_ID)
);
CREATE INDEX IDX_TDY_PROMO_CODE ON TDY_PROMO (PROMO_CODE);
CREATE INDEX IDX_TDY_PROMO_F_SITE ON TDY_PROMO (PROMO_F_SITE);
CREATE INDEX IDX_TDY_PROMO_F_USER ON TDY_PROMO (PROMO_F_USER);
CREATE INDEX IDX_TDY_PROMO_F_CREATOR ON TDY_PROMO (PROMO_F_CREATOR);

alter table AV_USER add column USER_MODMOBLOG_ID tinytext;
alter table AV_USER add column USER_MODMOBLOG_F_SITE mediumint(10);


alter table AV_SITE add column SITE_MOD_ENOTIFY tinyint null;
alter table AV_SITE add column SITE_MOD_ENOTIFY_TEXT text null;
alter table AV_MEMBERSHIP add column MEMBERSHIP_MOD_ENOTIFY varchar(20);

alter table AV_TEXT add column TEXT_MOD_CALENDAR_STARTTIME datetime;
alter table AV_TEXT add column TEXT_MOD_CALENDAR_ENDTIME datetime;
alter table AV_TEXT add column TEXT_MOD_CALENDAR_UNTIL datetime;
alter table AV_TEXT add column TEXT_MOD_CALENDAR_ISWHOLEDAY tinyint(1);
alter table AV_TEXT modify column TEXT_PROTOTYPE varchar(30);

