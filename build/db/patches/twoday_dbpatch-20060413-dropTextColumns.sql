
update AV_TEXT set TEXT_CREATOR_EMAIL = TEXT_MOD_MTIMEXPORT_AUTHOR_EMAIL where TEXT_MOD_MTIMEXPORT_AUTHOR_EMAIL IS NOT NULL and TEXT_CREATOR_EMAIL IS NULL;
update AV_TEXT set TEXT_CREATOR_NAME = TEXT_MOD_MTIMEXPORT_AUTHOR_NAME where TEXT_MOD_MTIMEXPORT_AUTHOR_NAME IS NOT NULL and TEXT_CREATOR_NAME IS NULL;

alter table AV_TEXT drop column TEXT_MOD_MTIMEXPORT_AUTHOR_NAME;
alter table AV_TEXT drop column TEXT_MOD_MTIMEXPORT_AUTHOR_EMAIL;

alter table AV_TEXT drop column TEXT_RAWCONTENT;
alter table AV_TEXT drop column TEXT_ALIAS;
