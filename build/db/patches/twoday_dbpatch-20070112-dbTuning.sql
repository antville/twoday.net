
optimize table AV_TEXT;
optimize table AV_IMAGE;


alter table AV_SYSLOG MODIFY SYSLOG_OBJECT varchar(255) CHARACTER SET latin1;
alter table AV_SYSLOG modify SYSLOG_TYPE enum('site', 'system', 'user') character set latin1;

alter table AV_SYSLOG drop index IDX_SYSLOG_TYPE;
CREATE INDEX IDX_SYSLOG_TYPE ON AV_SYSLOG (SYSLOG_TYPE(2));

optimize table AV_SYSLOG;
