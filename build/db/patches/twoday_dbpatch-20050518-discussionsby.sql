
alter table AV_TEXT ADD column TEXT_CREATOR_URL tinytext after TEXT_F_USER_MODIFIER;
alter table AV_TEXT ADD column TEXT_CREATOR_EMAIL tinytext after TEXT_F_USER_MODIFIER;
alter table AV_TEXT ADD column TEXT_CREATOR_NAME tinytext after TEXT_F_USER_MODIFIER;

update AV_SITE set SITE_PREFERENCES = REPLACE(SITE_PREFERENCES, '<discussions type="float">1.0</discussions>', '<discussionsby>sorua</discussionsby>');
update AV_SITE set SITE_PREFERENCES = REPLACE(SITE_PREFERENCES, '<discussions type="float">0.0</discussions>', '<discussionsby>nobody</discussionsby>');

update AV_SITE set SITE_PREFERENCES = REPLACE(SITE_PREFERENCES, '<discussionsby>sorua</discussionsby>', '<discussionsby>anonymous</discussionsby>') where SITE_ALIAS='bmworacleracing';
update AV_SITE set SITE_PREFERENCES = REPLACE(SITE_PREFERENCES, '<discussionsby>sorua</discussionsby>', '<discussionsby>anonymous</discussionsby>') where SITE_ALIAS='michi';
