
# IMPORTANT UPGRADE INFORMATION !!!
# stop Helma
# patch DB with this patch
# backup your [HopHome]/db/twoday directory
# call patch20050330 with 'java -cp launcher.jar helma.main.launcher.Commandline twoday.manage.patch20050330', resp. change 'twoday' to the name of your application
# important: you just may run this script once!
# start Helma
# stop & start Helma again, before accessing any page (necessary for the 0.xml + 1.xml to be created again) !!
# check that sys_frontSite and sys_layout are set correctly

create table AV_ROOT (
   ROOT_ID int unsigned not null,
   ROOT_F_SITE mediumint(10),
   ROOT_F_LAYOUT mediumint(10),
   ROOT_PREFERENCES text,
   primary key (ROOT_ID)
);

insert into AV_ROOT (ROOT_ID, ROOT_F_SITE, ROOT_F_LAYOUT, ROOT_PREFERENCES) values (0, null, null, null);

create table AV_MODULE (
   MODULE_ID int unsigned not null auto_increment,
   MODULE_NAME tinytext,
   MODULE_TITLE tinytext,
   MODULE_DESCRIPTION text,
   MODULE_URL tinytext,
   MODULE_VERSION tinytext,
   MODULE_STATE tinytext,
   MODULE_COPYRIGHT tinytext,
   MODULE_LICENSE tinytext,
   MODULE_LICENSEURL tinytext,
   MODULE_LICENSETEXT text,
   MODULE_ISLICENSEACCEPTED tinyint,
   MODULE_AUTHOR tinytext,
   MODULE_AUTHORURL tinytext,
   MODULE_AUTHOREMAIL tinytext,
   MODULE_AUTHORORGANISATION tinytext,
   MODULE_ISSIDEBAR tinyint,
   MODULE_ISACTIVATED tinyint,
   MODULE_ISINITIALIZED tinyint,
   MODULE_HASSITESETTINGS tinyint,
   MODULE_INSTALLEDFILES text,
   MODULE_CREATETIME datetime,
   MODULE_F_USER_MODIFIER mediumint(10),
   MODULE_MODIFYTIME datetime,
   MODULE_PREFERENCES text,
   primary key (MODULE_ID)
);
