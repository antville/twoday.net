
alter table AV_LAYOUT add column LAYOUT_F_LAYOUT_LOCALEPARENT mediumint(10) null after LAYOUT_F_LAYOUT_PARENT;
alter table AV_LAYOUT add column LAYOUT_LOCALE varchar(64) null after LAYOUT_TITLE;
alter table AV_LAYOUT add column LAYOUT_MESSAGES mediumtext null after LAYOUT_PREFERENCES;

alter table AV_SKIN add column SKIN_TITLE varchar(128) after SKIN_NAME;
alter table AV_SKIN add column SKIN_DESCRIPTION varchar(255) after SKIN_TITLE;
alter table AV_SKIN add column SKIN_NEEDSTRANSLATION tinyint(1) after SKIN_DESCRIPTION;
