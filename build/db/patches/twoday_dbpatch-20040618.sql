

alter table AV_SITE add column SITE_BLOCKUSER mediumint(10) unsigned NULL after SITE_ISBLOCKED;
alter table AV_SITE add column SITE_BLOCKTIME datetime NULL after SITE_BLOCKUSER;
alter table AV_USER add column USER_BLOCKUSER mediumint(10) unsigned NULL after USER_ISBLOCKED;
alter table AV_USER add column USER_BLOCKTIME datetime NULL after USER_BLOCKUSER;
