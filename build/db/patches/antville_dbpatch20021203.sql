

DROP INDEX IDX_ACCESSLOG_F_SITE ON AV_ACCESSLOG;
DROP INDEX IDX_ACCESSLOG_DATE ON AV_ACCESSLOG;
CREATE INDEX IDX_ACCESSLOG_MIXED ON AV_ACCESSLOG (ACCESSLOG_F_SITE,ACCESSLOG_DATE);

DROP INDEX IDX_IMAGE_F_SITE ON AV_IMAGE;
DROP INDEX IDX_IMAGE_ALIAS ON AV_IMAGE;
DROP INDEX IDX_IMAGE_F_IMAGE_PARENT ON AV_IMAGE;
DROP INDEX IDX_IMAGE_F_IMAGE_THUMB ON AV_IMAGE;
CREATE INDEX IDX_IMAGE_MIXED ON AV_IMAGE (IMAGE_F_SITE,IMAGE_ALIAS(20),IMAGE_F_IMAGE_PARENT);

DROP INDEX IDX_SKIN_F_SITE ON AV_SKIN;
DROP INDEX IDX_SKIN_PROTOTYPE ON AV_SKIN;
DROP INDEX IDX_SKIN_NAME ON AV_SKIN;
CREATE INDEX IDX_SKIN_MIXED ON AV_SKIN (SKIN_F_SITE,SKIN_PROTOTYPE(10),SKIN_NAME(10));

DROP INDEX IDX_TEXT_F_SITE ON AV_TEXT;
DROP INDEX IDX_TEXT_TOPIC ON AV_TEXT;
DROP INDEX IDX_TEXT_DAY ON AV_TEXT;
DROP INDEX IDX_TEXT_PROTOTYPE ON AV_TEXT;
DROP INDEX IDX_TEXT_ISONLINE ON AV_TEXT;
CREATE INDEX IDX_TEXT_MIXED_ALL ON AV_TEXT (TEXT_F_SITE,TEXT_MODIFYTIME,TEXT_ISONLINE,TEXT_PROTOTYPE,TEXT_ID);
CREATE INDEX IDX_TEXT_MIXED_TOPIC ON AV_TEXT (TEXT_F_SITE,TEXT_TOPIC);
CREATE INDEX IDX_TEXT_MIXED_DAY ON AV_TEXT (TEXT_F_SITE,TEXT_DAY);
CREATE INDEX IDX_TEXT_MIXED_STORIES ON AV_TEXT (TEXT_F_SITE,TEXT_PROTOTYPE,TEXT_ISONLINE,TEXT_CREATETIME,TEXT_ID,TEXT_DAY);
