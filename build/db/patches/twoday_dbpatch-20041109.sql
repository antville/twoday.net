
# alter table AV_TEXT modify column TEXT_TITLE text;
# alter table AV_TEXT modify column TEXT_TEXT text;
# alter table AV_TEXT modify column TEXT_CONTENT text;
# alter table AV_TEXT modify column TEXT_RAWCONTENT text;

alter table AV_FILE modify column FILE_DESCRIPTION text;
alter table AV_IMAGE modify column IMAGE_ALTTEXT text;
alter table AV_POLL modify column POLL_QUESTION text;

alter table AV_LAYOUT modify column LAYOUT_DESCRIPTION text;
alter table AV_SYSLOG modify column SYSLOG_ENTRY text;
