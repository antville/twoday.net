use antville;

alter table AV_SITE add column SITE_TDY_BILLING_TYPE tinytext;
alter table AV_SITE add column SITE_TDY_BILLING_PAIDUNTIL datetime;
# alter table AV_SITE change column SITE_PAIDUNTIL SITE_TDY_BILLING_PAIDUNTIL datetime;
# alter table AV_SITE change column SITE_TYPE SITE_TDY_BILLING_TYPE tinytext;

CREATE TABLE TDY_BILLING (
   BILLING_ID mediumint(10) NOT NULL default '0',
   BILLING_TYPE tinytext default NULL,
   BILLING_INVOICEID int unsigned default NULL,
   BILLING_TID int unsigned default NULL,
   BILLING_MPAYTID int unsigned default NULL,
   BILLING_PACKAGE tinytext default NULL,
   BILLING_AMOUNT float default NULL,
   BILLING_CURRENCY tinytext default NULL,
   BILLING_PAYMENTTYPE tinytext default NULL,
   BILLING_CCBRAND tinytext default NULL,
   BILLING_DESCRIPTION text default NULL,
   BILLING_F_SITE mediumint(10) default NULL,
   BILLING_F_CREATOR mediumint(10) default NULL,
   BILLING_CREATETIME datetime default NULL,
   PRIMARY KEY(BILLING_ID)
);
CREATE INDEX IDX_TDY_BILLING_F_SITE ON TDY_BILLING (BILLING_F_SITE);
CREATE INDEX IDX_TDY_BILLING_F_CREATOR ON TDY_BILLING (BILLING_F_CREATOR);
