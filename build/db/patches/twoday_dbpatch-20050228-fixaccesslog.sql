
# mediumint (8mio) might become too small for large sites
ALTER TABLE AV_ACCESSLOG MODIFY COLUMN ACCESSLOG_ID int unsigned auto_increment;
