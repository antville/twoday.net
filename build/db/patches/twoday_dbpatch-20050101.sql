
alter table AV_USER add column USER_TYPE tinytext after USER_ID;
alter table AV_USER add column USER_AUTH_TYPE tinytext;
alter table AV_USER add column USER_AUTH_USER tinytext;
alter table AV_USER add column USER_AUTH_DATA tinytext;
update AV_USER set USER_TYPE = 'USER_LOCAL';
update AV_USER set USER_AUTH_TYPE = 'local';
