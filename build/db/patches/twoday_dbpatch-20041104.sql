
alter table AV_IMAGE add column IMAGE_MIMETYPE tinytext after IMAGE_ALIAS;
update AV_IMAGE set IMAGE_FILENAME = concat(IMAGE_FILENAME, '.', IMAGE_FILEEXT);
update AV_IMAGE set IMAGE_MIMETYPE = 'image/jpeg' where IMAGE_FILEEXT = 'jpg';
update AV_IMAGE set IMAGE_MIMETYPE = 'image/gif' where IMAGE_FILEEXT = 'gif';
update AV_IMAGE set IMAGE_MIMETYPE = 'image/png' where IMAGE_FILEEXT = 'png';
update AV_IMAGE set IMAGE_MIMETYPE = 'image/x-icon' where IMAGE_FILEEXT = 'ico';
alter table AV_IMAGE drop column IMAGE_FILEEXT;

alter table AV_FILE change column FILE_NAME FILE_FILENAME tinytext;
alter table AV_FILE change column FILE_SIZE FILE_FILESIZE mediumint(10);

update AV_SKIN set SKIN_NAME = 'info' where SKIN_PROTOTYPE = 'Image' and SKIN_NAME='main';
