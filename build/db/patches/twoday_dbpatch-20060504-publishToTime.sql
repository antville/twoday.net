
# publishToTimeState == 0: published
# publishToTimeState == 1: not published - offline after publishing
# publishToTimeState == 2: not published - online, but not on frontpage after publishing
# publishToTimeState == 3: not published - online after publishing
ALTER TABLE AV_TEXT ADD TEXT_PUBLISH_TO_TIME_STATE tinyint after TEXT_ISONLINE;

UPDATE AV_TEXT SET TEXT_PUBLISH_TO_TIME_STATE = 0;
UPDATE AV_TEXT SET TEXT_PUBLISH_TO_TIME_STATE = 3 WHERE TEXT_ISONLINE = 2 and TEXT_CREATETIME > now();
UPDATE AV_TEXT SET TEXT_PUBLISH_TO_TIME_STATE = 2 WHERE TEXT_ISONLINE = 1 and TEXT_CREATETIME > now();
UPDATE AV_TEXT SET TEXT_PUBLISH_TO_TIME_STATE = 1 WHERE TEXT_ISONLINE = 0 and TEXT_CREATETIME > now();

CREATE INDEX IDX_TEXT_PUBLISH_TO_TIME_STATE ON AV_TEXT (TEXT_PUBLISH_TO_TIME_STATE);
