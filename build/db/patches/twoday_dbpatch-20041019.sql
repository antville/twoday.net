
# important fix to TEXT_ISONLINE column; tinyint(1) caused troubles, so we rather use tinyint(4);
alter table AV_TEXT modify column TEXT_ISONLINE tinyint(4);
