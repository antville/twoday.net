
alter table AV_IMAGE change column IMAGE_ID IMAGE_ID mediumint(9) not null auto_increment;
insert into AV_IMAGE (IMAGE_ALIAS,IMAGE_FILENAME,IMAGE_FILEEXT,IMAGE_WIDTH,IMAGE_HEIGHT,IMAGE_ALTTEXT) 
   values ('manage','manage','gif',50,13,'manage');
alter table AV_IMAGE change column IMAGE_ID IMAGE_ID mediumint(9) not null;

alter table AV_TEXT add column TEXT_ALIAS varchar(128) after TEXT_F_TEXT_PARENT;
alter table AV_SITE add column  SITE_PREFERENCES mediumtext null after SITE_SHORTDATEFORMAT;

ALTER TABLE AV_SITE ADD COLUMN SITE_VLINKCOLOR varchar(6);
create table AV_SKINSET (
   SKINSET_ID mediumint(10) not null,
   SKINSET_NAME varchar(128),
   SKINSET_F_SITE mediumint(10),
   SKINSET_F_SKINSET_PARENT mediumint(10),
   SKINSET_CREATETIME datetime,
   SKINSET_MODIFYTIME datetime,
   SKINSET_F_USER_CREATOR mediumint(10),
   SKINSET_F_USER_MODIFIER mediumint(10),
   SKINSET_SHARED tinyint(1),
   primary key (SKINSET_ID)
);
# Create a skinset for each existing site with the same primary key
# use direct DB (and *not* MySQL CC)
insert into AV_SKINSET (
  SKINSET_ID, 
  SKINSET_NAME, 
  SKINSET_F_SITE, 
  SKINSET_F_SKINSET_PARENT, 
  SKINSET_CREATETIME, 
  SKINSET_MODIFYTIME, 
  SKINSET_F_USER_CREATOR, 
  SKINSET_F_USER_MODIFIER,
  SKINSET_SHARED) 
select 
  SITE_ID, 
  SITE_TITLE, 
  SITE_ID, 
  null, 
  NOW(), 
  null, 
  SITE_F_USER_CREATOR, 
  null,
  0
from AV_SITE;
# Alter skin table site reference to skinset
alter table AV_SKIN 
   change column SKIN_F_SITE 
   SKIN_F_SKINSET mediumint(10);
alter table AV_SKINSET
   change column SKINSET_ID LAYOUT_ID mediumint(10) not null default '0',
   change column SKINSET_NAME LAYOUT_TITLE varchar(128) null,
   change column SKINSET_F_SITE LAYOUT_F_SITE mediumint(10) null,
   change column SKINSET_F_SKINSET_PARENT LAYOUT_F_LAYOUT_PARENT mediumint(10) null,
   change column SKINSET_CREATETIME LAYOUT_CREATETIME datetime null,
   change column SKINSET_MODIFYTIME LAYOUT_MODIFYTIME datetime null,
   change column SKINSET_F_USER_CREATOR LAYOUT_F_USER_CREATOR mediumint(10) null,
   change column SKINSET_F_USER_MODIFIER LAYOUT_F_USER_MODIFIER mediumint(10) null,
   change column SKINSET_SHARED LAYOUT_SHARED tinyint(1) null;
alter table AV_SKINSET rename AV_LAYOUT;
alter table AV_LAYOUT add column LAYOUT_ALIAS varchar(128) null after LAYOUT_ID;
alter table AV_LAYOUT add column LAYOUT_PREFERENCES mediumtext null after LAYOUT_F_LAYOUT_PARENT;
alter table AV_LAYOUT add column LAYOUT_DESCRIPTION mediumtext null after LAYOUT_PREFERENCES;
alter table AV_LAYOUT change column LAYOUT_SHARED LAYOUT_SHAREABLE tinyint(1) null;
alter table AV_SKIN change column SKIN_F_SKINSET SKIN_F_LAYOUT mediumint(10) null;
alter table AV_IMAGE add column IMAGE_F_LAYOUT mediumint(10) null after IMAGE_F_SITE;
alter table AV_IMAGE add column IMAGE_PROTOTYPE varchar(20) null after IMAGE_F_IMAGE_THUMB;
# create an initial layout object
alter table AV_LAYOUT change column LAYOUT_ID LAYOUT_ID mediumint(10) not null auto_increment;
insert into AV_LAYOUT (LAYOUT_ALIAS, LAYOUT_TITLE, LAYOUT_PREFERENCES, LAYOUT_DESCRIPTION, LAYOUT_SHAREABLE)
values ('default', 'Default Layout', '<?xml version="1.0" encoding="UTF-8"?>\r\n<xmlroot xmlns:hop="http://www.helma.org/docs/guide/features/database">\r\n  <hopobject id="t234" name="HopObject" prototype="HopObject" created="1069430202375" lastModified="1069430202375">\r\n    <smallcolor>666666</smallcolor>\r\n    <textcolor>000000</textcolor>\r\n    <vlinkcolor>ff3300</vlinkcolor>\r\n    <titlecolor>cc0000</titlecolor>\r\n    <smallsize>11px</smallsize>\r\n    <alinkcolor>ff0000</alinkcolor>\r\n    <textsize>13px</textsize>\r\n    <titlesize>15px</titlesize>\r\n    <linkcolor>ff3300</linkcolor>\r\n    <smallfont>Verdana, Arial, Helvetica, sans-serif</smallfont>\r\n    <textfont>Verdana, Helvetica, Arial, sans-serif</textfont>\r\n    <titlefont>Verdana, Helvetica, Arial, sans-serif</titlefont>\r\n    <bgcolor>ffffff</bgcolor>\r\n  </hopobject>\r\n</xmlroot>', 'The default Layout', 1);
update AV_LAYOUT set LAYOUT_ALIAS = LAYOUT_ID where LAYOUT_ALIAS is null;
alter table AV_LAYOUT change column LAYOUT_ID LAYOUT_ID mediumint(10) not null;
# updata all default images so that they're part of the above created layout
select @layoutID:=LAYOUT_ID from AV_LAYOUT where LAYOUT_ALIAS = 'default' and LAYOUT_F_SITE is null and LAYOUT_F_LAYOUT_PARENT is null;
update AV_IMAGE set IMAGE_F_LAYOUT = @layoutID where IMAGE_F_SITE is null;
update AV_IMAGE set IMAGE_PROTOTYPE = 'image' where IMAGE_F_SITE is not null;
update AV_IMAGE set IMAGE_PROTOTYPE = 'layoutimage' where IMAGE_F_LAYOUT is not null;
alter table AV_SITE add column SITE_F_LAYOUT mediumint(10) null after SITE_EMAIL;
alter table AV_LAYOUT add column LAYOUT_ISIMPORT tinyint(1) null after LAYOUT_SHAREABLE;
alter table AV_SKIN add column SKIN_ISCUSTOM tinyint(1) null after SKIN_NAME;
update AV_SKIN set SKIN_ISCUSTOM = 0;
update AV_SKIN set SKIN_NAME = "mgrlistitem" where SKIN_PROTOTYPE = "file" and SKIN_NAME = "preview";
update AV_SKIN set SKIN_NAME = "mgrlistitem" where SKIN_PROTOTYPE = "image" and SKIN_NAME = "preview";
update AV_SKIN set SKIN_NAME = "mgrlistitem" where SKIN_PROTOTYPE = "poll" and SKIN_NAME = "listitem";
update AV_SKIN set SKIN_NAME = "mgrlistitem" where SKIN_PROTOTYPE = "layout" and SKIN_NAME = "listitem";
update AV_SKIN set SKIN_NAME = "mgrlistitem" where SKIN_PROTOTYPE = "story" and SKIN_NAME = "listitem";
update AV_SKIN set SKIN_NAME = "mgrlistitem" where SKIN_PROTOTYPE = "membership" and SKIN_NAME = "preview";
update AV_SKIN set SKIN_NAME = "mailregconfirm" where SKIN_PROTOTYPE = "membermgr" and SKIN_NAME = "mailbody";
update AV_SKIN set SKIN_NAME = "mailpassword" where SKIN_PROTOTYPE = "membermgr" and SKIN_NAME = "pwdmail";
update AV_SKIN set SKIN_NAME = "mailstatuschange" where SKIN_PROTOTYPE = "membership" and SKIN_NAME = "mailbody";
update AV_TEXT set TEXT_EDITABLEBY = 2 where TEXT_EDITABLEBY = 0;
update AV_TEXT set TEXT_EDITABLEBY = 0 where TEXT_EDITABLEBY is null;
## rename skin prototypes since they're mixed case now
update AV_SKIN set SKIN_PROTOTYPE = "Global" where SKIN_PROTOTYPE = "global";
update AV_SKIN set SKIN_PROTOTYPE = "User" where SKIN_PROTOTYPE = "user";
update AV_SKIN set SKIN_PROTOTYPE = "Root" where SKIN_PROTOTYPE = "root";
alter table AV_SITE add column SITE_DISKUSAGE mediumint(10) null after SITE_PREFERENCES;
update AV_SITE set SITE_DISKUSAGE = SITE_FILESIZEUSAGE + SITE_IMAGESIZEUSAGE;
UPDATE AV_IMAGE SET IMAGE_PROTOTYPE = "LayoutImage" where IMAGE_PROTOTYPE = "layoutimage";
UPDATE AV_IMAGE SET IMAGE_PROTOTYPE = "Image" where IMAGE_PROTOTYPE = "image";
UPDATE AV_TEXT SET TEXT_PROTOTYPE = "Story" where TEXT_PROTOTYPE = "story";
UPDATE AV_TEXT SET TEXT_PROTOTYPE = "Comment" where TEXT_PROTOTYPE = "comment";
UPDATE AV_SKIN SET SKIN_PROTOTYPE = "Comment" where SKIN_PROTOTYPE = "comment";
UPDATE AV_SKIN SET SKIN_PROTOTYPE = "BloggerApi" where SKIN_PROTOTYPE = "blogger";
UPDATE AV_SKIN SET SKIN_PROTOTYPE = "Choice" where SKIN_PROTOTYPE = "choice";
UPDATE AV_SKIN SET SKIN_PROTOTYPE = "Comment" where SKIN_PROTOTYPE = "comment";
UPDATE AV_SKIN SET SKIN_PROTOTYPE = "Day" where SKIN_PROTOTYPE = "day";
UPDATE AV_SKIN SET SKIN_PROTOTYPE = "File" where SKIN_PROTOTYPE = "file";
UPDATE AV_SKIN SET SKIN_PROTOTYPE = "FileMgr" where SKIN_PROTOTYPE = "filemgr";
UPDATE AV_SKIN SET SKIN_PROTOTYPE = "Folder" where SKIN_PROTOTYPE = "folder";
UPDATE AV_SKIN SET SKIN_PROTOTYPE = "Global" where SKIN_PROTOTYPE = "global";
UPDATE AV_SKIN SET SKIN_PROTOTYPE = "HopObject" where SKIN_PROTOTYPE = "hopobject";
UPDATE AV_SKIN SET SKIN_PROTOTYPE = "Image" where SKIN_PROTOTYPE = "image";
UPDATE AV_SKIN SET SKIN_PROTOTYPE = "ImageMgr" where SKIN_PROTOTYPE = "imagemgr";
UPDATE AV_SKIN SET SKIN_PROTOTYPE = "ImageTopicMgr" where SKIN_PROTOTYPE = "imagetopicmgr";
UPDATE AV_SKIN SET SKIN_PROTOTYPE = "Layout" where SKIN_PROTOTYPE = "layout";
UPDATE AV_SKIN SET SKIN_PROTOTYPE = "LayoutImage" where SKIN_PROTOTYPE = "layoutimage";
UPDATE AV_SKIN SET SKIN_PROTOTYPE = "LayoutImageMgr" where SKIN_PROTOTYPE = "layoutimagemgr";
UPDATE AV_SKIN SET SKIN_PROTOTYPE = "LayoutMgr" where SKIN_PROTOTYPE = "layoutmgr";
UPDATE AV_SKIN SET SKIN_PROTOTYPE = "MemberMgr" where SKIN_PROTOTYPE = "membermgr";
UPDATE AV_SKIN SET SKIN_PROTOTYPE = "Membership" where SKIN_PROTOTYPE = "membership";
UPDATE AV_SKIN SET SKIN_PROTOTYPE = "MetaWeblogApi" where SKIN_PROTOTYPE = "metaWeblog";
UPDATE AV_SKIN SET SKIN_PROTOTYPE = "MtApi" where SKIN_PROTOTYPE = "mt";
UPDATE AV_SKIN SET SKIN_PROTOTYPE = "Poll" where SKIN_PROTOTYPE = "poll";
UPDATE AV_SKIN SET SKIN_PROTOTYPE = "PollMgr" where SKIN_PROTOTYPE = "pollmgr";
UPDATE AV_SKIN SET SKIN_PROTOTYPE = "PropertyMgr" where SKIN_PROTOTYPE = "propertymgr";
UPDATE AV_SKIN SET SKIN_PROTOTYPE = "Root" where SKIN_PROTOTYPE = "root";
UPDATE AV_SKIN SET SKIN_PROTOTYPE = "RootLayoutMgr" where SKIN_PROTOTYPE = "rootlayoutmgr";
UPDATE AV_SKIN SET SKIN_PROTOTYPE = "Site" where SKIN_PROTOTYPE = "site";
UPDATE AV_SKIN SET SKIN_PROTOTYPE = "Skin" where SKIN_PROTOTYPE = "skin";
UPDATE AV_SKIN SET SKIN_PROTOTYPE = "SkinMgr" where SKIN_PROTOTYPE = "skinmgr";
UPDATE AV_SKIN SET SKIN_PROTOTYPE = "Story" where SKIN_PROTOTYPE = "story";
UPDATE AV_SKIN SET SKIN_PROTOTYPE = "StoryMgr" where SKIN_PROTOTYPE = "storymgr";
UPDATE AV_SKIN SET SKIN_PROTOTYPE = "SysLog" where SKIN_PROTOTYPE = "syslog";
UPDATE AV_SKIN SET SKIN_PROTOTYPE = "SysMgr" where SKIN_PROTOTYPE = "sysmgr";
UPDATE AV_SKIN SET SKIN_PROTOTYPE = "Topic" where SKIN_PROTOTYPE = "topic";
UPDATE AV_SKIN SET SKIN_PROTOTYPE = "TopicMgr" where SKIN_PROTOTYPE = "topicmgr";
UPDATE AV_SKIN SET SKIN_PROTOTYPE = "User" where SKIN_PROTOTYPE = "user";
UPDATE AV_SKIN SET SKIN_PROTOTYPE = "Vote" where SKIN_PROTOTYPE = "vote";
alter table AV_USER add column USER_PREFERENCES mediumtext null after USER_URL;

CREATE TABLE TDY_PROMO (
   PROMO_ID mediumint(10) NOT NULL default '0',
   PROMO_CODE varchar(255) default NULL,
   PROMO_TYPE varchar(255) default NULL,
   PROMO_MONTHS mediumint(10) default NULL,
   PROMO_VALUE float default NULL,
   PROMO_DESCRIPTION mediumtext default NULL,
   PROMO_USED tinyint(1) default NULL,
   PROMO_VALIDUNTIL datetime default NULL,
   PROMO_F_SITE mediumint(10) default NULL,
   PROMO_F_USER mediumint(10) default NULL,
   PROMO_F_CREATOR mediumint(10) default NULL,
   PROMO_CREATETIME datetime default NULL,
   PRIMARY KEY(PROMO_ID)
);
CREATE INDEX IDX_TDY_PROMO_CODE ON TDY_PROMO (PROMO_CODE);
CREATE INDEX IDX_TDY_PROMO_F_SITE ON TDY_PROMO (PROMO_F_SITE);
CREATE INDEX IDX_TDY_PROMO_F_USER ON TDY_PROMO (PROMO_F_USER);
CREATE INDEX IDX_TDY_PROMO_F_CREATOR ON TDY_PROMO (PROMO_F_CREATOR);
alter table AV_USER change column USER_MAILING USER_TDY_MAILING tinyint;
alter table AV_SITE change column SITE_PAIDUNTIL SITE_TDY_BILLING_PAIDUNTIL datetime;
alter table AV_SITE change column SITE_TYPE SITE_TDY_BILLING_TYPE tinytext;

CREATE TABLE TDY_BILLING (
   BILLING_ID mediumint(10) NOT NULL default '0',
   BILLING_TYPE tinytext default NULL,
   BILLING_INVOICEID int unsigned default NULL,
   BILLING_TID int unsigned default NULL,
   BILLING_MPAYTID int unsigned default NULL,
   BILLING_PACKAGE tinytext default NULL,
   BILLING_AMOUNT float default NULL,
   BILLING_CURRENCY tinytext default NULL,
   BILLING_PAYMENTTYPE tinytext default NULL,
   BILLING_CCBRAND tinytext default NULL,
   BILLING_DESCRIPTION text default NULL,
   BILLING_F_SITE mediumint(10) default NULL,
   BILLING_F_CREATOR mediumint(10) default NULL,
   BILLING_CREATETIME datetime default NULL,
   PRIMARY KEY(BILLING_ID)
);
CREATE INDEX IDX_TDY_BILLING_F_SITE ON TDY_BILLING (BILLING_F_SITE);
CREATE INDEX IDX_TDY_BILLING_F_CREATOR ON TDY_BILLING (BILLING_F_CREATOR);

alter table AV_IMAGE change column IMAGE_ALTTEXT IMAGE_ALTTEXT mediumtext;
alter table AV_LAYOUT add column LAYOUT_F_LAYOUT_LOCALEPARENT mediumint(10) null after LAYOUT_F_LAYOUT_PARENT;
alter table AV_LAYOUT add column LAYOUT_LOCALE varchar(64) null after LAYOUT_TITLE;
alter table AV_LAYOUT add column LAYOUT_MESSAGES mediumtext null after LAYOUT_PREFERENCES;
alter table AV_SKIN add column SKIN_TITLE varchar(128) after SKIN_NAME;
alter table AV_SKIN add column SKIN_DESCRIPTION varchar(255) after SKIN_TITLE;
alter table AV_SKIN add column SKIN_NEEDSTRANSLATION tinyint(1) after SKIN_DESCRIPTION;
alter table AV_MEMBERSHIP change column MEMBERSHIP_ENOTIFY MEMBERSHIP_MOD_ENOTIFY varchar(20);
alter table AV_SITE change column SITE_ENOTIFY SITE_MOD_ENOTIFY tinyint(4);
alter table AV_SITE add column SITE_BLOCKUSER mediumint(10) unsigned NULL after SITE_ISBLOCKED;
alter table AV_SITE add column SITE_BLOCKTIME datetime NULL after SITE_BLOCKUSER;
alter table AV_USER add column USER_BLOCKUSER mediumint(10) unsigned NULL after USER_ISBLOCKED;
alter table AV_USER add column USER_BLOCKTIME datetime NULL after USER_BLOCKUSER;
alter table AV_SKIN add column SKIN_MODULE tinytext after SKIN_ISCUSTOM;

update AV_SITE set SITE_F_LAYOUT = SITE_ID;
update AV_IMAGE set IMAGE_PROTOTYPE='LayoutImage' where IMAGE_ALIAS='header' or IMAGE_ALIAS='footer' or IMAGE_ALIAS='ring';
update AV_IMAGE set IMAGE_F_LAYOUT=IMAGE_F_SITE where IMAGE_ALIAS='header' and IMAGE_F_SITE IS NOT NULL;
update AV_IMAGE set IMAGE_F_SITE=NULL where IMAGE_ALIAS='header' and IMAGE_F_SITE IS NOT NULL;
update AV_IMAGE set IMAGE_F_LAYOUT=IMAGE_F_SITE where IMAGE_ALIAS='footer' and IMAGE_F_SITE IS NOT NULL;
update AV_IMAGE set IMAGE_F_SITE=NULL where IMAGE_ALIAS='footer' and IMAGE_F_SITE IS NOT NULL;
update AV_IMAGE set IMAGE_F_LAYOUT=IMAGE_F_SITE where IMAGE_ALIAS='ring' and IMAGE_F_SITE IS NOT NULL;
update AV_IMAGE set IMAGE_F_SITE=NULL where IMAGE_ALIAS='ring' and IMAGE_F_SITE IS NOT NULL;

update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, ".tdyEnotifyLink", ".modEnotifyLink");
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, ".enotifylink", ".modEnotifyLink");
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, "<% site.storylist ", "<% response.storylist ") where SKIN_SOURCE like "%<\% site.storylist %";
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, "<% topic.storylist ", "<% response.storylist ") where SKIN_SOURCE like "%<\% topic.storylist %";
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, "<% day.storylist ", "<% response.storylist ") where SKIN_SOURCE like "%<\% day.storylist %";
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, "<% this.storylist ", "<% response.storylist ") where SKIN_SOURCE like "%<\% this.storylist %";
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, "<%site.storylist","<%response.storylist") where SKIN_SOURCE like "%<\%site.storylist%";
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, "<%topic.storylist","<%response.storylist") where SKIN_SOURCE like "%<\%topic.storylist%";
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, "<%day.storylist","<%response.storylist") where SKIN_SOURCE like "%<\%day.storylist%";
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, "<%this.storylist ", "<% response.storylist ") where SKIN_SOURCE like "%<\%this.storylist %";
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, "<%storymgr.storylist ", "<% response.storylist ") where SKIN_SOURCE like "%<\%storymgr.storylist %";
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, "<% storymgr.storylist ", "<% response.storylist ") where SKIN_SOURCE like "%<\% storymgr.storylist %";
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, "<% site.topiclist ", "<% topiclist ") where SKIN_SOURCE like "%<\% site.topiclist %";
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, "<% this.topiclist ", "<% topiclist ") where SKIN_SOURCE like "%<\% this.topiclist %";
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, "<% site.imagetopiclist ", "<% imagetopiclist ") where SKIN_SOURCE like "%<\% site.imagetopiclist %";
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, "<% this.imagetopiclist ", "<% imagetopiclist ") where SKIN_SOURCE like "%<\% this.imagetopiclist %";
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, "<%site.topiclist", "<%topiclist") where SKIN_SOURCE like "%<\%site.topiclist%";
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, "<%this.topiclist", "<%topiclist") where SKIN_SOURCE like "%<\%this.topiclist%";
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, "<%site.imagetopiclist", "<%imagetopiclist") where SKIN_SOURCE like "%<\%site.imagetopiclist%";
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, "<%this.imagetopiclist", "<%imagetopiclist") where SKIN_SOURCE like "%<\%this.imagetopiclist%";
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, "<% story.title ", "<% story.content part=\"title\" ") where SKIN_SOURCE like "%<\% story.title %";
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, "<% this.title ", "<% this.content part=\"title\" ") where SKIN_SOURCE like "%<\% this.title %" and SKIN_PROTOTYPE = "Story";
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, "<% comment.title ", "<% comment.content part=\"title\" ") where SKIN_SOURCE like "%<\% comment.title %";
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, "<% this.title ", "<% this.content part=\"title\" ") where SKIN_SOURCE like "%<\% this.title %" and SKIN_PROTOTYPE = "Comment";
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, "<% story.text ", "<% story.content part=\"text\" ") where SKIN_SOURCE like "%<\% story.text %";
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, "<% this.text ", "<% this.content part=\"text\" ") where SKIN_SOURCE like "%<\% this.text %" and SKIN_PROTOTYPE = "Story";
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, "%>stylesheet.css", "%>main.css") where SKIN_SOURCE like "%\%>stylesheet.css%";
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, "%>stylesheet", "%>main.css") where SKIN_SOURCE like "%\%>stylesheet%";
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, "%>javascript.js", "%>main.js") where SKIN_SOURCE like "%\%>javascript.js%";
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, "%>javascript", "%>main.js") where SKIN_SOURCE like "%\%>javascript%";
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, "net/stylesheet.css", "net/main.css") where SKIN_SOURCE like "%net/stylesheet.css%";
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, "net/stylesheet", "net/main.css") where SKIN_SOURCE like "%net/stylesheet%";
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, "net/javascript.js", "net/main.js") where SKIN_SOURCE like "%net/javascript.js%";
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, "net/javascript", "net/main.js") where SKIN_SOURCE like "%net/javascript%";
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, "<% site.link to=\"skins\" ", "<% site.link to=\"layouts\" ") where SKIN_SOURCE like "%<\% site.link to=\"skins\" %";
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, "<script type=\"text/javascript\" src=\"<% site.url %>safescripts.js\"></script>", "") where SKIN_SOURCE like "%<script type=\"text/javascript\" src=\"<\% site.url \%>safescripts.js\"></script>%";
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, "<% site.referrerslink ", "<% site.link to=\"referrers\" ") where SKIN_SOURCE like "%<\% site.referrerslink %";
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, "<% site.mostreadlink ", "<% site.link to=\"mostread\" ") where SKIN_SOURCE like "%<\% site.mostreadlink %";
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, "<% site.billinglink ", "<% site.link to=\"tdyBillingType\" ") where SKIN_SOURCE like "%<\% site.billinglink %";
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, "TWODAY_PRICE_ADVANCED", "TDY_BILLING_PRICE_ADVANCED") where SKIN_SOURCE like "%TWODAY_PRICE_ADVANCED%";
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, "TWODAY_PRICE_BASIC", "TDY_BILLING_PRICE_BASIC") where SKIN_SOURCE like "%TWODAY_PRICE_BASIC%";
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, "TWODAY_PRICE_BETA", "TDY_BILLING_PRICE_BETA") where SKIN_SOURCE like "%TWODAY_PRICE_BETA%";
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, "TWODAY_PRICE_FREE", "TDY_BILLING_PRICE_FREE") where SKIN_SOURCE like "%TWODAY_PRICE_FREE%";
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, "TWODAY_DISKQUOTA_ADVANCED", "TDY_BILLING_DISKQUOTA_ADVANCED") where SKIN_SOURCE like "%TWODAY_DISKQUOTA_ADVANCED%";
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, "TWODAY_DISKQUOTA_BASIC", "TDY_BILLING_DISKQUOTA_BASIC") where SKIN_SOURCE like "%TWODAY_DISKQUOTA_BASIC%";
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, "TWODAY_DISKQUOTA_BETA", "TDY_BILLING_DISKQUOTA_BETA") where SKIN_SOURCE like "%TWODAY_DISKQUOTA_BETA%";
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, "TWODAY_DISKQUOTA_FREE", "TDY_BILLING_DISKQUOTA_FREE") where SKIN_SOURCE like "%TWODAY_DISKQUOTA_FREE%";
update AV_TEXT set TEXT_TEXT = replace(TEXT_TEXT, "TWODAY_PRICE_ADVANCED", "TDY_BILLING_PRICE_ADVANCED") where TEXT_TEXT like "%TWODAY_PRICE_ADVANCED%";
update AV_TEXT set TEXT_TEXT = replace(TEXT_TEXT, "TWODAY_PRICE_BASIC", "TDY_BILLING_PRICE_BASIC") where TEXT_TEXT like "%TWODAY_PRICE_BASIC%";
update AV_TEXT set TEXT_TEXT = replace(TEXT_TEXT, "TWODAY_PRICE_BETA", "TDY_BILLING_PRICE_BETA") where TEXT_TEXT like "%TWODAY_PRICE_BETA%";
update AV_TEXT set TEXT_TEXT = replace(TEXT_TEXT, "TWODAY_PRICE_FREE", "TDY_BILLING_PRICE_FREE") where TEXT_TEXT like "%TWODAY_PRICE_FREE%";
update AV_TEXT set TEXT_TEXT = replace(TEXT_TEXT, "TWODAY_DISKQUOTA_ADVANCED", "TDY_BILLING_DISKQUOTA_ADVANCED") where TEXT_TEXT like "%TWODAY_DISKQUOTA_ADVANCED%";
update AV_TEXT set TEXT_TEXT = replace(TEXT_TEXT, "TWODAY_DISKQUOTA_BASIC", "TDY_BILLING_DISKQUOTA_BASIC") where TEXT_TEXT like "%TWODAY_DISKQUOTA_BASIC%";
update AV_TEXT set TEXT_TEXT = replace(TEXT_TEXT, "TWODAY_DISKQUOTA_BETA", "TDY_BILLING_DISKQUOTA_BETA") where TEXT_TEXT like "%TWODAY_DISKQUOTA_BETA%";
update AV_TEXT set TEXT_TEXT = replace(TEXT_TEXT, "TWODAY_DISKQUOTA_FREE", "TDY_BILLING_DISKQUOTA_FREE") where TEXT_TEXT like "%TWODAY_DISKQUOTA_FREE%";
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, "render2dayWhatsHot", "tdyWhatsHot") where SKIN_SOURCE like "%render2dayWhatsHot%";
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, "name=2dayWhatsHot", "name=tdyWhatsHot") where SKIN_SOURCE like "%name=2dayWhatsHot%";
update AV_SKIN set SKIN_NAME="tdyWhatsHot" where SKIN_NAME="2dayWhatsHot";
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, 'width="30" height="12"', 'cols="30" rows="12"') where SKIN_NAME='edit' and (SKIN_PROTOTYPE='Story' or SKIN_PROTOTYPE='Comment');
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, 'width="30" height="15"', 'cols="30" rows="15"') where SKIN_NAME='edit' and (SKIN_PROTOTYPE='Story' or SKIN_PROTOTYPE='Comment');
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, 'width="40" height="12"', 'cols="40" rows="12"') where SKIN_NAME='edit' and (SKIN_PROTOTYPE='Story' or SKIN_PROTOTYPE='Comment');
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, 'width="30" height="1"', 'cols="30" rows="1"') where SKIN_NAME='edit' and (SKIN_PROTOTYPE='Story' or SKIN_PROTOTYPE='Comment');
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, 'width="30" height="24"', 'cols="30" rows="24"') where SKIN_NAME='edit' and (SKIN_PROTOTYPE='Story' or SKIN_PROTOTYPE='Comment');
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, 'width="30" height="20"', 'cols="30" rows="20"') where SKIN_NAME='edit' and (SKIN_PROTOTYPE='Story' or SKIN_PROTOTYPE='Comment');
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, 'width="60" height="12"', 'cols="60" rows="12"') where SKIN_NAME='edit' and (SKIN_PROTOTYPE='Story' or SKIN_PROTOTYPE='Comment');
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, 'width="24" height="5"', 'cols="24" rows="5"') where SKIN_NAME='edit' and (SKIN_PROTOTYPE='Story' or SKIN_PROTOTYPE='Comment');
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, 'width="30" height="5"', 'cols="30" rows="5"') where SKIN_NAME='edit' and (SKIN_PROTOTYPE='Story' or SKIN_PROTOTYPE='Comment');
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, 'width="30" height="3"', 'cols="30" rows="3"') where SKIN_NAME='edit' and (SKIN_PROTOTYPE='Story' or SKIN_PROTOTYPE='Comment');
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, 'width="50" height="12"', 'cols="50" rows="12"') where SKIN_NAME='edit' and (SKIN_PROTOTYPE='Story' or SKIN_PROTOTYPE='Comment');
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, 'width="31" height="10"', 'cols="31" rows="10"') where SKIN_NAME='edit' and (SKIN_PROTOTYPE='Story' or SKIN_PROTOTYPE='Comment');
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, 'height="5" width="30"', 'rows="5" cols="30"') where SKIN_NAME='edit' and (SKIN_PROTOTYPE='Story' or SKIN_PROTOTYPE='Comment');


# update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, "twoday.net", "towday.net");

update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, 'to="loginPopUp"', 'to="login"');
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, 'history show="', 'history limit="');
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, 'response.prevpage', 'response.pagenavigation') where SKIN_PROTOTYPE<>'Site' and SKIN_PROTOTYPE<>'Day' and SKIN_PROTOTYPE<>'Topic';
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, 'response.nextpage', 'response.pagenavigation') where SKIN_PROTOTYPE<>'Site' and SKIN_PROTOTYPE<>'Day' and SKIN_PROTOTYPE<>'Topic';
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, '.justintopic', '.addtofront checked="checked"');
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, 'Beitrag nur in der Rubrik anzeigen lassen', 'Diesen Beitrag auf der Titelseite anzeigen');
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, 'Beitrag nur f�r Mitglieder anzeigen', 'Diesen Beitrag auf der Titelseite anzeigen');
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, 'Beitrag nur f�r Mitglieder anzeigen', 'Diesen Beitrag auf der Titelseite anzeigen');
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, 'Beitrag nur in dem Sternzeichen anzeigen lassen', 'Diesen Beitrag auf der Titelseite anzeigen');
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, 'Beitrag nur in dem Themenbereich anzeigen lassen', 'Diesen Beitrag auf der Titelseite anzeigen');
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, 'Beitrag nur innerhalb des Themas anzeigen lassen', 'Diesen Beitrag auf der Titelseite anzeigen');
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, 'Beitrag nur in ausgew�hlter Rubrik anzeigen', 'Diesen Beitrag auf der Titelseite anzeigen');
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, '<% story.topic %>', '<% story.topic as="link" %>');

alter table AV_SITE change column SITE_SHOWONFRONTPAGE SITE_SHOW tinyint default NULL;

alter table AV_SKIN change column SKIN_ID SKIN_ID mediumint(10) not null auto_increment;
insert into AV_SKIN (SKIN_PROTOTYPE, SKIN_NAME, SKIN_SOURCE, SKIN_ISCUSTOM, SKIN_CREATETIME, SKIN_MODIFYTIME, SKIN_F_LAYOUT, SKIN_F_USER_CREATOR, SKIN_F_USER_MODIFIER) select "Story", "dayheader", SKIN_SOURCE, SKIN_ISCUSTOM, SKIN_CREATETIME, SKIN_MODIFYTIME, SKIN_F_LAYOUT, SKIN_F_USER_CREATOR, SKIN_F_USER_MODIFIER from AV_SKIN where SKIN_PROTOTYPE="Day" and SKIN_NAME="header" and SKIN_F_LAYOUT<>3434 and SKIN_F_LAYOUT<>3488;
alter table AV_SKIN change column SKIN_ID SKIN_ID mediumint(10) not null;
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, "<% day.timestamp", "<% story.createtime") where SKIN_NAME="dayheader";
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, "<%day.timestamp", "<% story.createtime") where SKIN_NAME="dayheader";
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, "<% day.timestamp", "<% day.date");
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, "<%day.timestamp", "<% day.date");

delete from AV_SKIN where SKIN_PROTOTYPE="Day" and SKIN_NAME="header";
alter table AV_USER change column USER_MOBLOG_ID USER_MODMOBLOG_ID tinytext;
alter table AV_USER change column USER_MOBLOG_F_SITE USER_MODMOBLOG_F_SITE mediumint(10);

update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, '<p><% story.discussions as="editor" %> Kommentare erlauben</p>', '<% story.discussions as="editor" %>') where SKIN_NAME='edit' and SKIN_PROTOTYPE='Story';
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, '<% story.discussions as="editor" prefix="<p>" suffix=" Kommentare erlauben</p>" %>', '<% story.discussions as="editor" %>') where SKIN_NAME='edit' and SKIN_PROTOTYPE='Story';

update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, 'site.bgcolor', 'layout.bgcolor') where SKIN_NAME='style' and SKIN_PROTOTYPE='Site';
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, 'site.titlecolor', 'layout.titlecolor') where SKIN_NAME='style' and SKIN_PROTOTYPE='Site';
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, 'site.textcolor', 'layout.textcolor') where SKIN_NAME='style' and SKIN_PROTOTYPE='Site';
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, 'site.smallcolor', 'layout.smallcolor') where SKIN_NAME='style' and SKIN_PROTOTYPE='Site';
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, 'site.linkcolor', 'layout.linkcolor') where SKIN_NAME='style' and SKIN_PROTOTYPE='Site';
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, 'site.alinkcolor', 'layout.alinkcolor') where SKIN_NAME='style' and SKIN_PROTOTYPE='Site';
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, 'site.vlinkcolor', 'layout.vlinkcolor') where SKIN_NAME='style' and SKIN_PROTOTYPE='Site';
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, 'site.blockcolor', 'layout.blockcolor') where SKIN_NAME='style' and SKIN_PROTOTYPE='Site';
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, 'site.linecolor', 'layout.linecolor') where SKIN_NAME='style' and SKIN_PROTOTYPE='Site';
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, 'site.titlefont', 'layout.titlefont') where SKIN_NAME='style' and SKIN_PROTOTYPE='Site';
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, 'site.textfont', 'layout.textfont') where SKIN_NAME='style' and SKIN_PROTOTYPE='Site';
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, 'site.smallfont', 'layout.smallfont') where SKIN_NAME='style' and SKIN_PROTOTYPE='Site';
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, '<script type="text/javascript" src="<% site.url %>safescripts"></script>', '');

update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, '<% twoday_toolbar %>', '<% modToolbar %>');

update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, '<META', '<meta');
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, 'NAME=', 'name=');
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, 'CONTENT=', 'content=');
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, '<INPUT', '<input');
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, 'type=submit', 'type="submit"');

update AV_SKIN set SKIN_SOURCE = CONCAT(SKIN_SOURCE, '\n<% site.skin name="adminStyle" %>\n') WHERE SKIN_NAME='style' and SKIN_PROTOTYPE='Site';

update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, '<% twoday_ad01 prefix="<br /><div align=\"center\">" suffix="</div><br />" %>', '');
update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, '<% twoday_ad02 prefix="<p>" suffix="</p>" %>', '');

update AV_TEXT set TEXT_TEXT = replace(TEXT_TEXT, "<% thumbnail ", "<% image as=\"popup\"") where TEXT_TEXT like "%<\% thumbnail %";
update AV_TEXT set TEXT_TEXT = replace(TEXT_TEXT, "<%thumbnail ", "<% image as=\"popup\"") where TEXT_TEXT like "%<\%thumbnail %";

alter table AV_SITE add column SITE_MOD_ENOTIFY_TEXT text null;
update AV_SITE, AV_SKIN set SITE_MOD_ENOTIFY_TEXT = SKIN_SOURCE where SITE_ID = SKIN_F_LAYOUT and SKIN_PROTOTYPE='Site' and SKIN_NAME='enotifyMail';

update AV_SKIN set SKIN_SOURCE = replace(SKIN_SOURCE, 'layout.image name="icon"', 'image name="icon" fallback="/icon"');

alter table AV_LAYOUT change column LAYOUT_SHAREABLE LAYOUT_SHAREABLE tinyint(8);
