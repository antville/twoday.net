
alter table AV_SITE
	drop column SITE_BGCOLOR,
	drop column SITE_TEXTFONT,
	drop column SITE_TEXTCOLOR,
	drop column SITE_TEXTSIZE,
	drop column SITE_LINKCOLOR,
	drop column SITE_ALINKCOLOR,
	drop column SITE_VLINKCOLOR,
	drop column SITE_LINECOLOR,
 	drop column SITE_BLOCKCOLOR,
	drop column SITE_TITLEFONT,
	drop column SITE_TITLECOLOR,
	drop column SITE_TITLESIZE,
	drop column SITE_SMALLFONT,
	drop column SITE_SMALLCOLOR,
	drop column SITE_SMALLSIZE,
	drop column SITE_HASDISCUSSIONS,
	drop column SITE_USERMAYCONTRIB,
	drop column SITE_SHOWDAYS,
	drop column SITE_SHOWARCHIVE,
	drop column SITE_LANGUAGE,
	drop column SITE_COUNTRY,
	drop column SITE_TIMEZONE,
	drop column SITE_LONGDATEFORMAT,
	drop column SITE_SHORTDATEFORMAT,
	drop column SITE_TAGLINE;
alter table AV_SITE drop column SITE_FILESIZEUSAGE;
alter table AV_SITE drop column SITE_IMAGESIZEUSAGE;
alter table AV_TEXT drop TEXT_COMMENTSCOUNT;

alter table AV_USER drop column USER_SEX;
alter table AV_USER drop column USER_AGE;
alter table AV_USER drop column USER_COUNTRY;
alter table AV_USER drop column USER_CITY;

alter table AV_SITE drop column SITE_RSSFEED;
alter table AV_SITE drop column SITE_RSSFEEDTIME;

# drop site.rssfeed, site.rssfeedtime
