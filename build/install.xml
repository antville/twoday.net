<?xml version="1.0"?>

<project name="twoday" default="usage" basedir=".">

  <!-- =================================================================== -->
  <!-- initialize some parameters                                          -->
  <!-- =================================================================== -->
  <property file="install.properties"/>

  <property name="name" value="twoday"/>
  <property name="version" value="0.0.0"/>

  <property name="build.dir" value="."/>
  <property name="build.work" value="${build.dir}/work"/>
  <property name="build.dist" value="${build.dir}/dist"/>
  <property name="build.extra" value="${build.dir}/extra"/>
  <property name="build.docs" value="${build.dir}/docs"/>

  <property name="mysql.lib" value="${build.extra}/mysql-connector-java-3.1.12-bin.jar"/>

  <property name="debug" value="on"/>
  <property name="optimize" value="on"/>
  <property name="deprecation" value="off"/>

  <tstamp/>

  <filter token="year" value="${year}"/>
  <filter token="version" value="${version}"/>
  <filter token="date" value="${TODAY}"/>

  <!-- =================================================================== -->
  <!-- help on usage                                                       -->
  <!-- =================================================================== -->
  <target name="help" depends="usage"/>
  <target name="usage">
      <echo>
Installation Tasks
-------------------------------------------------------------
Make sure that the predefined settings in 
${name}/build/install.properties will work for you,
adapt these settings if you want, and then call one of the 
following available targets:

 testdb        - tests the current db settings
 initdb        - initializes the mysql database
 force-initdb  - same as initdb, but drops the database before 
                 re-creating it
 filldata      - recreates the db and fills it with mysql-data.sql
 configure     - configures apps.properties, app.properties + 
                 db.properties according to install.properties.
 install       - calls initdb + configure
 force-install - calls initdb + configure, but drops any 
                 previously existing database
 force-all     - calls force-install + filldata
-------------------------------------------------------------
      </echo>
  </target>

  <!-- =================================================================== -->
  <!-- test whether the root db user has privileges to create db           -->
  <!-- =================================================================== -->
  <target name="testdb">
     <sql
      driver="com.mysql.jdbc.Driver"
      url="jdbc:mysql://${db.host}/"
      userid="${db.root.user}"
      password="${db.root.pass}"
      rdbms="mysql">
      <classpath>
      	<pathelement location="${mysql.lib}"/>
      </classpath>
      create database justATestPleaseIgnore;
      drop database justATestPleaseIgnore;
     </sql>
     <echo>Database Connection: OK</echo>
  </target>


  <!-- =================================================================== -->
  <!-- setup the database according to all *.sql files                     -->
  <!-- =================================================================== -->
  <target name="initdb" depends="testdb">
     <sql
      driver="com.mysql.jdbc.Driver"
      url="jdbc:mysql://${db.host}/"
      userid="${db.root.user}"
      password="${db.root.pass}"
      rdbms="mysql">
      <classpath>
      	<pathelement location="${mysql.lib}"/>
      </classpath>
      CREATE DATABASE ${db.name};
      USE ${db.name};
      GRANT ALL ON ${db.name}.* TO ${db.user}@'localhost' IDENTIFIED BY '${db.pass}';
      GRANT ALL ON ${db.name}.* TO ${db.user}@'localhost.localdomain' IDENTIFIED BY '${db.pass}';
     </sql>
     <sql
      driver="com.mysql.jdbc.Driver"
      url="jdbc:mysql://${db.host}/${db.name}"
      userid="${db.user}"
      password="${db.pass}"
      rdbms="mysql"
      src="${twoday.dir}/db/mysql-complete.sql">
      <classpath>
      	<pathelement location="${mysql.lib}"/>
      </classpath>
     </sql>
  </target>

  <target name="force-initdb" depends="testdb">
     <sql
      driver="com.mysql.jdbc.Driver"
      url="jdbc:mysql://${db.host}/"
      userid="${db.root.user}"
      password="${db.root.pass}"
      rdbms="mysql">
      <classpath>
      	<pathelement location="${mysql.lib}"/>
      </classpath>
      DROP DATABASE IF EXISTS ${db.name};
     </sql>
    <antcall target="initdb" />
  </target>

  <target name="filldata" depends="force-initdb">
     <sql
      driver="com.mysql.jdbc.Driver"
      url="jdbc:mysql://${db.host}/${db.name}"
      userid="${db.user}"
      password="${db.pass}"
      rdbms="mysql">
      <fileset dir="${twoday.dir}">
        <include name="**/mysql-data.sql"/>
      </fileset>
      <classpath>
      	<pathelement location="${mysql.lib}"/>
      </classpath>
     </sql>
  </target>

  <target name="configure">
    <copy file="${build.extra}/db.properties" todir="${twoday.dir}/code" overwrite="true">
      <filterchain><expandproperties/></filterchain>
    </copy>
    <copy file="${build.extra}/app.properties" todir="${twoday.dir}/code" overwrite="true">
      <filterchain><expandproperties/></filterchain>
    </copy>
    <move file="${helma.dir}/apps.properties" tofile="${helma.dir}/apps.properties.tmp" overwrite="true">
      <filterchain>
        <linecontainsregexp>
          <regexp pattern="^(?!${name}\b)"/>
          <regexp pattern="^#(?!${name}\b)"/>
        </linecontainsregexp>
      </filterchain>
    </move>
    <move file="${helma.dir}/apps.properties.tmp" tofile="${helma.dir}/apps.properties" overwrite="true" />
    <concat destfile="${helma.dir}/apps.properties" append="true" fixlastline="true">
      <filelist dir="${build.extra}" files="apps.properties"/>
      <filterchain><expandproperties/></filterchain>
    </concat>
  </target>

  <target name="install">
    <antcall target="initdb" />
    <antcall target="configure" />
  </target>

  <target name="force-install">
    <antcall target="force-initdb" />
    <antcall target="configure" />
  </target>

  <target name="force-all">
    <antcall target="force-install" />
    <antcall target="filldata" />
  </target>

</project>
