#!/bin/sh

## uncomment the following line to set JAVA_HOME
#export JAVA_HOME=/usr/lib/jdk1.5.0_02

##--------------------------------------------
## No need to edit anything past here
##--------------------------------------------

if [ "$JAVA_HOME" ]; then
   JAVACMD="$JAVA_HOME/bin/java"
else
   JAVACMD=java
fi

BUILDFILE=install.xml

ANTLIBDIR=lib
CP="$CLASSPATH:$ANTLIBDIR/ant.jar:$ANTLIBDIR/ant-launcher.jar:$ANTLIBDIR/ant-nodeps.jar"

$JAVACMD -cp $CP org.apache.tools.ant.launch.Launcher -buildfile $BUILDFILE $1
