@echo off

:## uncomment the following line to set JAVA_HOME:
rem set JAVA_HOME=c:\programme\java\jdk1.5.0_02

:## --------------------------------------------
:## No need to edit anything past here
:## --------------------------------------------

set TARGET=%1%

:: Using JAVA_HOME variable if defined. Otherwise,
:: Java executable must be contained in PATH variable
if "%JAVA_HOME%"=="" goto default
   set JAVACMD=%JAVA_HOME%\bin\java
   goto end
:default
   set JAVACMD=java
:end

set BUILDFILE=install.xml

set ANTLIBDIR=lib
set CP="%CLASSPATH%;%ANTLIBDIR%/ant.jar;%ANTLIBDIR%/ant-launcher.jar;%ANTLIBDIR%/ant-nodeps.jar"

"%JAVACMD%" -cp %CP% org.apache.tools.ant.launch.Launcher -buildfile %BUILDFILE% %TARGET%

goto end

:## -----------ERROR-------------
:javahomeerror
echo "ERROR: JAVA_HOME not found in your environment."
echo "Please, set the JAVA_HOME variable in your environment to match the"
echo "location of the Java Virtual Machine you want to use."

:end

