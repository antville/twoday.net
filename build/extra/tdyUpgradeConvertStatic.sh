#!/bin/sh
# convert old antville static folder
# is supposed to run in "static"

# create "antville" folder etc.
mkdir antville
mkdir antville/layouts
mkdir antville/layouts/default

# go to "images" directory
cd images

# loop over all files
for file in *
do
        if [ -d $file ]
        then
# if file is directory then move content
# into new subdirectory "images"
# and move this folder to "antville"
                cd $file
                mkdir ../../antville/$file/
                mkdir ../../antville/$file/images/
                mv * ../../antville/$file/images/
        cd ..
        else
# move other content to "layouts"
                mv $file ../antville/layouts/default/
        fi
done
cd ..

# same with "files" folder
cd files

for file in *
do
        if [ -d $file ]
        then
                cd $file
                mkdir ../../antville/$file/
                mkdir ../../antville/$file/files/
                mv * ../../antville/$file/files/
        cd ..
        fi
done
cd ..

# delete old folders
rm -r files
rm -r images
