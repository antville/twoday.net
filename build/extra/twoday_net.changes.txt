
15.05.2006 barbara
rename Site.tdyBillingType to Site.type and move into core

08.05.2006 barbara
extract billing and move into own module, use qenta as billing provider instead of mpay24

13.06.2005 michi
PRE-MERGE of discussionsby to allow anonymous user comments
check-in of modJCaptcha (for registration)

08.05.2005 michi
MERGE till twoday_20050508

26.04.2005 michi
removed line-ending white-spaces (for smoother merge)

22.02.2005 michi
implemented XmlRpc-UserFunctions (for moday)

11.01.2005 michi
fixed checking for isPaid()

08.01.2005 michi
added blo.gs and blogg.de as ping-destinations

08.09.2004 michi
added /manage/tdyBilling/invoices to print list of invoices

10.08.2004 michi
added Global/tdyInode.skin and included it into Site/credits.skin

22.07.2004 wolfgang
limited certain functionality for certain tdyBillingTypes

17.06.2004 (michi)

implementation of tdyBilling
* added properties Site/tdyBillingType and Site/tdyBillingPaidUntil
* modified Site/sysmgr_list.skin, SysMgr/updateSite(): view and edit new properties in SysMgr
* modified Site/sysmgr_statusflags_macro(): show tdyBillingType for Sites
* modified SysMgr/searchSites() and SysMgr/sitesearchform.skin: allow filtering of Sites dependent on tdyBillingType and tdyBillingPaidUntil
* modified Root/evalNewSite(): function now accepts fourth parameter tdyBillingType
* modified Root/new.skin and Root/new_action() to pass tdyBillingType to Root/evalNewSite()
* added Site/tdyBilling_action()
* modified Site/checkAccess(): just allow access to "tdyBilling" for users which may Edit
* added public constants: TDY_BILLING_PRICE_ADVANCED, TDY_BILLING_PRICE_BASIC, TDY_BILLING_PRICE_BETA, TDY_BILLING_PRICE_FREE, TDY_BILLING_DISKQUOTA_ADVANCED, TDY_BILLING_DISKQUOTA_BASIC, TDY_BILLING_DISKQUOTA_BETA, TDY_BILLING_DISKQUOTA_FREE
* added prototypes tdyBillingMgr and tdyBilling
* added Site/tdyBillingPayment_action(), Site/tdyBillingType_action(),Site/tdyBillingIsPaid(), Site/tdyBillingProcessMpay24Confirmation(), Site/tdyBillingSetParamObjectForSkin(), Site/tdyBillingExtendPaidUntil(), Site/tdyBillingGetPricePerMonth(), Site/tdyBillingPaymentLink_macro(), Site/tdyBillingSwitchType(), Site/tdyBilling_sysmgr_PaidUntil_macro(), Site/tdyBilling_sysmgr_Type_macro(), Site/tdyBilling_sysmgr_link_macro(), Site/tdyBillingRecordPayment(), Site/tdyBillingPayment.skin, Site/tdyBillingTypeFree.skin, Site/tdyBillingTypeBasic.skin, Site/tdyBillingTypeBeta.skin, Site/tdyBillingTypeAdvanced.skin

implementation of tdyPromo (depends on tdyBilling)
* added prototypes TdyPromoMgr and TdyPromo
* added DB Table TDY_PROMO
* added collection Root/tdyPromos
* added TdyPromoMgr/createPromo() and TdyPromoMgr/usePromo()
* added Site/tdyPromo.skin

implementation of tdyWhatsHot
* added Site/tdyWhatsHot_macro and Site/tdyWhatsHot.skin

implementation of tdyReservedAliases
* added constant Global/TDY_RESERVEDALIASES
* added Global/tdyReservedAliases()
* modified Root/evalNewSite(): added check for reserved alias name

implementation of tdyMailing
* added Global/tdyMailing() and Global/tdyMailing.skin
* added property User.tdyMailing
* added DB Column USER_TDY_MAILING
