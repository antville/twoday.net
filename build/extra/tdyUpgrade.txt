
====== merge TWODAY with TWODAY_NET =======
c:\helma\apps\twoday> cvs update
c:\helma\apps> c:\windows\system32\unxutils\diff -Nur -x CVS -x modules twoday_net.twoday twoday > patch.twoday.diff
c:\helma\apps\twoday_net.twoday> c:\windows\system32\unxutils\patch -p1 < ..\patch.twoday.diff
-> also copy modified binaries ! (c:\windows\system32\unxutils\diff -r -x CVS -x modules twoday_net.twoday twoday)
c:\helma\apps\twoday_net.twoday> cvs add ....
c:\helma\apps\twoday_net.twoday> cvs commit -m "update twoday to 20041125"
c:\helma\apps\twoday_net.twoday> cvs tag -c twoday_20041125
c:\helma\apps\twoday_net> cvs update -d -P -j twoday_20040826 -j twoday_20041001
c:\helma\apps\twoday_net> cvs commit -m "merge from twoday_20041125 to twoday_20041207"

====== merge TWODAY with TWODAY_VFL =======
c:\helma\apps\twoday> cvs update
c:\helma\apps> c:\windows\system32\unxutils\diff -Nur -x CVS -x modules twoday_vfl.twoday twoday > patch.twoday.diff
c:\helma\apps\twoday_vfl.twoday> c:\windows\system32\unxutils\patch -p1 < ..\patch.twoday.diff
-> also copy modified binaries ! (c:\windows\system32\unxutils\diff -r -x CVS -x modules twoday_vfl.twoday twoday)
c:\helma\apps\twoday_vfl.twoday> cvs add ....
c:\helma\apps\twoday_vfl.twoday> cvs commit -m "update twoday to 20041125"
c:\helma\apps\twoday_vfl.twoday> cvs tag -c twoday_20041125
c:\helma\apps\twoday_vfl> cvs update -d -P -j twoday_20040826 -j twoday_20041001
c:\helma\apps\twoday_vfl> cvs commit -m "merge from twoday_20041125 to twoday_20041207"

====== merge TWODAY with TWODAY_TU =======
c:\helma\apps\twoday> cvs update
c:\helma\apps> c:\windows\system32\unxutils\diff -Nur -x CVS -x modules twoday_tu.twoday twoday > patch.twoday.diff
c:\helma\apps\twoday_tu.twoday> c:\windows\system32\unxutils\patch -p1 < ..\patch.twoday.diff
-> also copy modified binaries ! (c:\helma\apps>c:\windows\system32\unxutils\diff -r -x CVS -x modules twoday_tu.twoday twoday)
c:\helma\apps\twoday_tu.twoday> cvs add ....
c:\helma\apps\twoday_tu.twoday> cvs commit -m "update twoday to 20041207"
c:\helma\apps\twoday_tu.twoday> cvs tag -c twoday_20040615
c:\helma\apps\twoday_tu> cvs update -d -P -j twoday_20040610 -j twoday_20040615
c:\helma\apps\twoday_tu> cvs commit -m "merge from twoday_20040908 to twoday_20041202"


HELMABUG FIXME
  root is not available for aspectJS stuff

TODO:
* mikro-portale (RSS-Feeds subclassing site)
* multiServer Konzept
* automatisierte Makro-Dokumenation
* SORUA


################# Antville 1.1 #################

make sure that Antville Bug #341 & #342 is fixed: http://helma.org/bugs/show_bug.cgi?id=341 http://helma.org/bugs/show_bug.cgi?id=342


################ How to upgrade #####################

1. cp -rp /home/twoday /home/twoday_net  ( + 10min )
   cd /home/twoday_net/static
   cp /usr/local/helma2/apps/twoday_net/tdyUpgradeConvertStatic.sh .
   chmod +x tdyUpgradeConvertStatic.sh
   ./tdyUpgradeConvertStatic.sh
   mv antville anttmp
   mv anttmp/* .
   rmdir anttmp
   chown -R helma:helma /home/twoday_net
   cp /usr/local/helma2/apps/twoday_net/static/*.png /home/twoday_net/static
   cp /usr/local/helma2/apps/twoday_net/static/*.gif /home/twoday_net/static
   cp /usr/local/helma2/apps/twoday_net/static/*.jpg /home/twoday_net/static

2. cd /var/lib/mysql
   mysqldump db_twoday -p > /home/backup/db_twoday.sql  (~2min)
   (drop database db_twoday_net)
   mysqlhotcopy db_twoday db_twoday_net (~1min)
   chown -R mysql:users db_twoday_net

3. cd /usr/local/helma2/apps/twoday_net
   cvs update -dP
   
3a. Patch the database with twoday.upgrade.01.sql (~5min)  (with MySQL CC)
  ACHTUNG: MySQL CC begrenzt selects auf 1000 Entries; daher INSERT INTO.. statement direkt ausführen !!!

4. set requestTimeout=18000 in app.properties

4a. /etc/init.d/helma2 start -> http://www.towday.net

4b. import layout twodayClassic.zip
select LAYOUT_ID FROM AV_LAYOUT WHERE LAYOUT_ALIAS='twodayclassic';
update AV_LAYOUT set LAYOUT_F_LAYOUT_PARENT=3737 where LAYOUT_F_SITE IS NOT NULL;

4c. /etc/init.d/helma2 restart

5. call http://towday.net/tdyUpgrade01 ~2min

6. call http://towday.net/tdyUpgrade02 ~2min

7. call http://towday.net/tdyUpgrade03 ~1min

8. call http://towday.net/tdyUpgrade04 ~3min

9. call http://towday.net/tdyUpgrade05 ~10sec

10. Set requestTimeOut in app.properties to normal value

11. http://www.towday.net
11a. http://www.twoday.net/layouts/1/skins/edit?key=Site.preview fix fallback="/icon"
11b. http://www.twoday.net/layouts/1/skins/edit?key=Site.page fix knallgrau-image
11c. http://twoday.net/manage/setup set locale to "German"

13. If everything looks fine then patch the database with twoday.upgrade.02.sql



 #### open Issues ####

"icon" -> "Icon" ???

 *) membermrg_link as="popup" implementieren und im User STATUS + Toolbar einbauen
 *) justintopic -> addtofront: Wording passt nun nicht mehr in paar Skins (screenshots,sab,emm)
 *) getCurrentLocale -> geoIP
 *) Payment übersetzen; MPAY24 de/en
 *) nicht bezahlte Weblogs sperren

http://smi.towday.net/modules/order - Sidebar Text rechts
layoutbild hochladen - redirect zu eigene bilder


Renamce CREDITS


 #### other Changes ####
# notify MPay24 of new Confirmation-URL; test Billing
# deprecated: Global/favicon_macro(), Global/trackhref_macro(), Site/redir_action()
