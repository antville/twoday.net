
@echo off

set APP=%1%
set STEP=%2%

set CVS_CMD=cvs
set DIFF_CMD=diff
set PATCH_CMD=c:\windows\system32\unxutils\patch.exe
set APPS_HOME=c:\helma\apps
set YEAR=%DATE:~6,4%
set MONTH=%DATE:~3,2%
set DAY=%DATE:~0,2%
set TSTAMP=%YEAR%%MONTH%%DAY%

if "%APP%" == "" goto errorarg
if "%STEP%" == "1" goto stepone
if "%STEP%" == "2" goto steptwo
goto errorarg
goto end

:stepone
cd %APPS_HOME%\twoday_clean
%CVS_CMD% -q update -CdP
cd %APPS_HOME%
%DIFF_CMD% -Nr --unified=5 -x CVS %APP%.twoday twoday_clean > patch.twoday.diff
cd %APPS_HOME%\%APP%.twoday
%PATCH_CMD% -p1 -F 7 < ..\patch.twoday.diff
cd %APPS_HOME%
cp %APPS_HOME%\twoday_clean\code\*.zip %APPS_HOME%\%APP%.twoday\code
echo ---- List of differing Binary Files -------------
%DIFF_CMD% -q -r -x CVS %APP%.twoday twoday_clean
echo -------------------------------------------------
echo ---------------- DONE ---------------------------
echo Copy all differing Binary Files, then call 
echo 'cvs add', resp. 'cvs remove' from SmartCVS, and
echo call 'merge %APP% 2' afterwards.
echo -------------------------------------------------
goto end

:steptwo
cd %APPS_HOME%\%APP%.twoday
cvs commit -m "update twoday to %TSTAMP%"
cvs tag -c twoday_%TSTAMP%
echo ----------------- DONE -------------
echo Now you need to merge %APP%.twoday 
echo into %APP%. Study twoday.change.log
echo for that, and double-check the 
echo modifications of the skin files.
echo ------------------------------------
goto end

:errorarg
echo ------------ ERROR ----------------------------------------
echo ERROR: 'merge twoday_xyz 1', resp 'merge twoday_xyz 2', and
echo call 'cvs add ...' manually in between, and perform the
echo merge afterwards
echo -----------------------------------------------------------

:end
cd %APPS_HOME%
